Imports ISC.LibrerieComuni.ManageSessione.Modulo

Public Class CLS_export

    Private _FileNameExport As String
    Private _GestData As GestioneDataset
    Private _GestModu As GestioneModulo
    Private _OnlySelected As Boolean

    Public Sub New()

        _OnlySelected = False

    End Sub

    WriteOnly Property OnlySelected() As Boolean

        Set(ByVal value As Boolean)
            _OnlySelected = value
        End Set

    End Property

    WriteOnly Property FileNameExport() As String

        Set(ByVal value As String)
            _FileNameExport = value
        End Set

    End Property

    WriteOnly Property DataToExport() As GestioneDataset

        Set(ByVal value As GestioneDataset)
            _GestData = value
        End Set

    End Property

    WriteOnly Property GestioneModulo() As GestioneModulo

        Set(ByVal value As GestioneModulo)
            _GestModu = value
        End Set

    End Property

    Public Sub Esportazione()
        Dim exp_csv As CLS_export_CSV

        exp_csv = New CLS_export_CSV
        exp_csv.FileNameExport = _FileNameExport
        exp_csv.DataToExport = _GestData
        exp_csv.GestioneModulo = _GestModu
        exp_csv.OnlySelected = _OnlySelected
        exp_csv.Esportazione()

    End Sub

End Class
