Imports ISC.LibrerieComuni.ManageSessione.Modulo

Public Class CLS_export_CSV

    Private _FileNameExport As String
    Private _GestData As GestioneDataset
    Private _GestModu As GestioneModulo
    Private _OnlySelected As Boolean

    WriteOnly Property FileNameExport() As String

        Set(ByVal value As String)
            _FileNameExport = value
        End Set

    End Property

    WriteOnly Property DataToExport() As GestioneDataset

        Set(ByVal value As GestioneDataset)
            _GestData = value
        End Set

    End Property

    WriteOnly Property GestioneModulo() As GestioneModulo

        Set(ByVal value As GestioneModulo)
            _GestModu = value
        End Set

    End Property

    Public Sub Esportazione()
        Dim sw_exp As System.IO.StreamWriter
        Dim cLineaTipo As String
        Dim cTabella As String
        Dim dr_rowA As DataRow
        Dim dr_rowB As DataRow
        Dim cValue As String
        Dim lExit As Boolean

        sw_exp = New System.IO.StreamWriter(_FileNameExport)
        ' Intestazione
        If _GestModu.EsportaModulo.SelectSingleNode("Esporta/intestazione/value") IsNot Nothing Then
            sw_exp.WriteLine(_GestModu.EsportaModulo.SelectSingleNode("Esporta/intestazione/value").InnerText)
        End If
        cTabella = ""
        cLineaTipo = ""
        If _GestModu.EsportaModulo.SelectSingleNode("Esporta/dati/tabella") IsNot Nothing Then
            cTabella = _GestModu.EsportaModulo.SelectSingleNode("Esporta/dati/tabella").InnerText
        End If
        If _GestModu.EsportaModulo.SelectSingleNode("Esporta/dati/value") IsNot Nothing Then
            cLineaTipo = _GestModu.EsportaModulo.SelectSingleNode("Esporta/dati/value").InnerText
        End If
        If _GestData.MainTableName = cTabella Then
            _GestData.SetupMasterRecord(0)
            lExit = False
            While Not lExit
                cValue = _GestData.ExtractValue(_GestModu.EsportaModulo.SelectSingleNode("Esporta/dati/value").InnerText, "", "0", "")
                sw_exp.WriteLine(cValue)
                If Not (_GestData.RecordNumber(cTabella) + 1 = _GestData.MainTable.Rows.Count) Then
                    _GestData.MoveRecordNumber(cTabella)
                Else
                    lExit = True
                End If
            End While
        Else
            _GestData.SetUpRecordNumber(cTabella)
            For Each dr_rowA In _GestData.MainTable.Rows
                While _GestData.RecordNumber(cTabella) <= dr_rowA.GetChildRows(_GestData.GetRelationName(cTabella)).Length - 1
                    dr_rowB = CType(dr_rowA.GetChildRows(_GestData.GetRelationName(cTabella))(_GestData.RecordNumber(cTabella)), DataRow)
                    If dr_rowB("DEF_toprint") And _OnlySelected Or Not _OnlySelected Then
                        cValue = _GestData.ExtractValue(_GestModu.EsportaModulo.SelectSingleNode("Esporta/dati/value").InnerText, "", "0", "")
                        sw_exp.WriteLine(cValue)
                    End If
                    _GestData.MoveRecordNumber(cTabella)
                End While
            Next
        End If
        sw_exp.Close()

    End Sub

    WriteOnly Property OnlySelected() As Boolean

        Set(ByVal value As Boolean)
            _OnlySelected = value
        End Set

    End Property

End Class
