Imports System.Windows.Forms

Public Class GestioneExport

    Private _OnlySelected As Boolean
    'Private _Anteprima As Boolean
    'Private _ArchiviazioneOttica As Boolean
    'Private _CassettoDefault As Integer
    'Private _DatiStatisticheStampa As Dictionary(Of Integer, Integer)
    Private _Esegui_Operazioni_PostExport As Boolean
    Private _FirstRecordExported As Integer
    Private _NoSelectionNeeded As Boolean
    'Private _ForceStampaCartaceo As Boolean
    'Private _FronteRetro As Boolean
    'Private _GruppiDaStampare As Integer
    'Private _GruppoInstampa As Integer
    'Private _InserisciFincatura As Boolean
    Private _LastRecordExported As Integer
    'Private _NomeStampante As String
    'Private _NoSplitAllegati As Boolean
    'Private _NumeroModuli As Integer
    'Private _NumeroPagineGlobali As Integer
    'Private _NumeroPagineParziali As Integer
    'Private _NumeroPaginePDF As Integer
    'Private _NumeroPartiDocumento As Integer
    'Private _PaginePerParte As Integer
    'Private _ParteInStampa As Integer
    'Private _PrinterTray As ArrayList
    'Private _PrnPDFCreator As String
    'Private _ResetWrapRound As Boolean
    'Private _StampaDaSplittare As Boolean
    Private _EsportazioneEseguita As Boolean
    'Private _SuffissoParte As String
    'Private _WrapRound As Decimal

    Property Esegui_Operazioni_PostExport() As Boolean

        Get
            Return _Esegui_Operazioni_PostExport
        End Get
        Set(ByVal value As Boolean)
            _Esegui_Operazioni_PostExport = value
        End Set

    End Property

    Public Sub New()


    End Sub



    Property LastRecordExported() As Integer

        Get
            Return _LastRecordExported
        End Get
        Set(ByVal value As Integer)
            _LastRecordExported = value
        End Set

    End Property

    Property NoSelectionNeeded() As Boolean

        Get
            Return _NoSelectionNeeded
        End Get
        Set(ByVal value As Boolean)
            _NoSelectionNeeded = value
        End Set

    End Property

    Property EsportazioneEseguita() As Boolean

        Get
            Return _EsportazioneEseguita
        End Get
        Set(ByVal value As Boolean)
            _EsportazioneEseguita = value
        End Set

    End Property

    WriteOnly Property DataToExport() As GestioneDataset

        Set(ByVal value As GestioneDataset)

        End Set

    End Property

    WriteOnly Property OnlySelected() As Boolean

        Set(ByVal value As Boolean)
            _OnlySelected = value
        End Set

    End Property

End Class
