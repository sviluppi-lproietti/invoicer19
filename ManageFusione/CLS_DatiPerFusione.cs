﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace ISC.Invoicer.ManageFusione
{
    public class CLS_DatiPerFusione
    {
        public ArrayList DocumentiDaFondere;

        public CLS_DatiPerFusione()
        {
            DocumentiDaFondere = new ArrayList();
        }

        public void ResetDocumentiFondere()
        {
            DocumentiDaFondere = new ArrayList();
        }

        public string SOrientamento
        {
            set
            {
                if (value == "M")
                    Orientamento = eTipoOrientamento.TOR_master;
                if (value == "L")
                    Orientamento = eTipoOrientamento.TOR_landscape;
                if (value == "P")
                    Orientamento = eTipoOrientamento.TOR_portrait;
            }
        }

        public string NomeDocumentoFuso;
        public eTipoOrientamento Orientamento;
        public int Rotation;

        public Boolean PaginePariSingoloDocumento;

        public Boolean PaginePariDocumentoGlobale;

    }
}
