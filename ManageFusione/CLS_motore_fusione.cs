﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace ISC.Invoicer.ManageFusione
{
    public class CLS_motore_fusione
    {
        public CLS_DatiPerFusione DatiFusione;

        public void FondiDocumenti()
        {
            Document DocumentoFuso;
            PdfDictionary pdfdict;
            int nPagineDocumento;
            PdfReader reader;
            PdfCopy copier;
            int nPagine;
         
            try
            {
                //
                // determina l'orientamento del documento Master (il primo)
                //
                if (DatiFusione.Orientamento == eTipoOrientamento.TOR_master)
                    DeterminaOrientamentoPagina();

                //
                // Preparo il nuovo documento
                //
                DocumentoFuso = new Document();
                copier = new PdfCopy(DocumentoFuso, new System.IO.FileStream(DatiFusione.NomeDocumentoFuso, System.IO.FileMode.OpenOrCreate));

                DocumentoFuso.Open();
                nPagineDocumento = 0;
                //DatiFusione.DocumentiDaFondere.RemoveAt(0);
                foreach (string cFilename in DatiFusione.DocumentiDaFondere)
                {
                    reader = new PdfReader(System.IO.File.ReadAllBytes(cFilename));
                    nPagine = reader.NumberOfPages;
                    for (int nPagina = 1; nPagina <= nPagine; nPagina++)
                    {
                        pdfdict = reader.GetPageN(nPagina);
                        pdfdict.Put(PdfName.ROTATE, new PdfNumber(DatiFusione.Rotation));
                        copier.AddPage(copier.GetImportedPage(reader, nPagina));
                        nPagineDocumento++;
                    }
                    if (DatiFusione.PaginePariSingoloDocumento && ((nPagine % 2) == 1))
                    {
                        copier.AddPage(PageSize.A4, DatiFusione.Rotation);
                        nPagineDocumento++;
                    }
                }
                if (DatiFusione.PaginePariDocumentoGlobale && ((nPagineDocumento % 2) == 1))
                    copier.AddPage(PageSize.A4, DatiFusione.Rotation);
                DocumentoFuso.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeterminaOrientamentoPagina()
        {
            iTextSharp.text.Rectangle Rect;
            PdfReader reader;
            string cFileName;

            cFileName = (string)DatiFusione.DocumentiDaFondere[0];

            //
            // Legge la prima pagina del documento maste
            //
            reader = new PdfReader(cFileName);

            //
            // Recupera la prima pagina. La numerazione per iTextSharp inizia da 1
            //
            Rect = reader.GetPageSizeWithRotation(1);

            //
            // Determina quale è l'horiemtamento della pagina.
            //
            if (Rect.Height >= Rect.Width)
                DatiFusione.Orientamento = eTipoOrientamento.TOR_portrait;
            else
                DatiFusione.Orientamento = eTipoOrientamento.TOR_landscape;
            DatiFusione.Rotation = Rect.Rotation;
            reader.Close();
        }
    }
}
