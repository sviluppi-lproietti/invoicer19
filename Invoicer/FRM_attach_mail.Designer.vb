<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_attach_mail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.TXB_attach_fil = New System.Windows.Forms.TextBox
        Me.BTN_attach_fil = New System.Windows.Forms.Button
        Me.BTN_add_attach = New System.Windows.Forms.Button
        Me.OFD_new_file = New System.Windows.Forms.OpenFileDialog
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "FIle da allegare"
        '
        'TXB_attach_fil
        '
        Me.TXB_attach_fil.Location = New System.Drawing.Point(88, 5)
        Me.TXB_attach_fil.Name = "TXB_attach_fil"
        Me.TXB_attach_fil.Size = New System.Drawing.Size(502, 20)
        Me.TXB_attach_fil.TabIndex = 15
        '
        'BTN_attach_fil
        '
        Me.BTN_attach_fil.Image = Global.Invoicer.My.Resources.Resources.PNG_file
        Me.BTN_attach_fil.Location = New System.Drawing.Point(596, 3)
        Me.BTN_attach_fil.Name = "BTN_attach_fil"
        Me.BTN_attach_fil.Size = New System.Drawing.Size(27, 23)
        Me.BTN_attach_fil.TabIndex = 16
        Me.BTN_attach_fil.UseVisualStyleBackColor = True
        '
        'BTN_add_attach
        '
        Me.BTN_add_attach.Location = New System.Drawing.Point(629, 3)
        Me.BTN_add_attach.Name = "BTN_add_attach"
        Me.BTN_add_attach.Size = New System.Drawing.Size(75, 23)
        Me.BTN_add_attach.TabIndex = 18
        Me.BTN_add_attach.Text = "Aggiungi"
        Me.BTN_add_attach.UseVisualStyleBackColor = True
        '
        'OFD_new_file
        '
        Me.OFD_new_file.FileName = "OpenFileDialog1"
        '
        'FRM_attach_mail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(716, 260)
        Me.Controls.Add(Me.BTN_add_attach)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXB_attach_fil)
        Me.Controls.Add(Me.BTN_attach_fil)
        Me.Name = "FRM_attach_mail"
        Me.Text = "Allegati da inviare via mail"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXB_attach_fil As System.Windows.Forms.TextBox
    Friend WithEvents BTN_attach_fil As System.Windows.Forms.Button
    Friend WithEvents BTN_add_attach As System.Windows.Forms.Button
    Friend WithEvents OFD_new_file As System.Windows.Forms.OpenFileDialog
End Class
