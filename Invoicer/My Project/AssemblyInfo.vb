﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Invoicer")> 
<Assembly: AssemblyDescription("Programma per la stampa di documenti")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("Invoicer")>
<Assembly: AssemblyCopyright("Copyright ©  2019")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("e0386f3a-61a9-4fa4-8489-3e02d97c45b2")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("19.10.30.01")>
<Assembly: AssemblyFileVersion("19.10.30.01")>
<Assembly: log4net.Config.XMLConfigurator(ConfigFile:="Log4NetAssembly1.exe.log4net", Watch:=True)> 
<Assembly: NeutralResourcesLanguageAttribute("it-IT")> 