﻿Public Class StampantiEntity

    Private mSTA_codice As Integer
    Private mSTA_nome As String

    Public Sub New(ByVal id As Integer, ByVal name As String)

        mSTA_codice = id
        mSTA_nome = name

    End Sub

    Public Property STA_codice() As Integer

        Get
            Return mSTA_codice
        End Get
        Set(ByVal value As Integer)
            mSTA_codice = value
        End Set

    End Property

    Public Property STA_nome() As String

        Get
            Return mSTA_nome
        End Get
        Set(ByVal value As String)
            mSTA_nome = value
        End Set

    End Property

End Class

