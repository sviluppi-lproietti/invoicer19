Imports ManageStampa
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita

Public Class FRM_exporting

    Private _ArchOttFileName As String
    Private _AttivitaEsecuzione As StepProc
    Private _AttivitaEsecDes As String
    Private _dTimeStart As DateTime
    ' Private _GestData As ManageExport.GestioneDataset
    Private _GestModu As GestioneModulo
    Private _GestSess As GestioneSessione
    Private _OldAutoSaveDirectory As String
    Private _OldAutoSaveFileName As String
    Private _OldAutosaveFormat As Integer
    Private _OldFontsEmbedded As Integer
    Private _OldDefaultPrinter As String
    Private _OldUseAutoSave As Integer
    Private _OldUseAutosaveDirectory As Integer
    Private _PagePartStr As String
    'Private _PRNArchOtt As TipiComuni.CLS_PDFCreator
    Private _RestorePDFPrinter As Boolean
    Private _Thr2Exec As System.Threading.Thread
    Private _UltimoGruppoStampato As Integer
    Private _ExportFileName As String
    Private _OnySelected As Boolean

    Enum StepProc
        Iniz_Procedura_export = 1
        Iniz_LoopExport = 2
        CaricaDatabase = 3
        Check_ConsistenzaDB_1 = 4
        Goto_FirstRecordDS_1 = 5
        EsportaDati = 6
        CLEAN_CartellaWork = 7
        Last_LoopExport = 8
        Exit_Procedura_export = 9
        Exit_Forced = 100
    End Enum

#Region "Propriet� Pubbliche"

    Property ExportFileNameFN() As String

        Get
            Return _ExportFileName
        End Get
        Set(ByVal value As String)
            _ExportFileName = value
        End Set

    End Property

    WriteOnly Property GestioneModulo() As GestioneModulo

        Set(ByVal value As GestioneModulo)
            _GestModu = value
        End Set

    End Property

    WriteOnly Property GestioneSessione() As GestioneSessione

        Set(ByVal value As GestioneSessione)
            _GestSess = value
        End Set

    End Property

#End Region

#Region "Propriet� Private"

    Private Property AttivitaEsecuzione() As StepProc

        Get
            Return _AttivitaEsecuzione
        End Get
        Set(ByVal value As StepProc)
            ' ********************************************************************* '
            ' In questa porzione di codice si controlla invece la progressione del  '
            ' codice dell'attivit� da svolgere.                                     '
            ' ********************************************************************* '
            If _AttivitaEsecuzione <> StepProc.Exit_Forced Then
                _AttivitaEsecuzione = value

                ' ********************************************************************* '
                ' Se non si sta svolgendo la stampa per l'archiviazione ottica allora   '
                ' si saltano dei passi.                                                 '
                ' ********************************************************************* '
            End If

            ' ********************************************************************* '
            ' In questa porzione di codice si imposta ecluisivamente il valore del- '
            ' la descrizione del testo presentato nella maschera.                   '
            ' ********************************************************************* '
            Select Case _AttivitaEsecuzione
                Case Is = StepProc.Iniz_Procedura_export
                    _AttivitaEsecDes = "Inizio attivit� di export"
                Case Is = StepProc.CaricaDatabase
                    _AttivitaEsecDes = "Caricamento dati per stampa..."
                Case Is = StepProc.Goto_FirstRecordDS_1
                    _AttivitaEsecDes = "Riposizionamento sul primo record da stampare..."
                Case Is = StepProc.EsportaDati
                    _AttivitaEsecDes = "Esportazione dati..."
                
                Case Is = StepProc.CLEAN_CartellaWork
                    _AttivitaEsecDes = "Pulizia cartella work..."
                Case Is = StepProc.Exit_Forced
                    _AttivitaEsecDes = "Uscita forzata in corso..."
            End Select
        End Set

    End Property

    Private ReadOnly Property ExitForced() As Boolean

        Get
            Return AttivitaEsecuzione = StepProc.Exit_Forced
        End Get

    End Property

#End Region

    Private Sub FRM_exporting_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        PGB_dett_grp.Maximum = StepProc.Last_LoopExport
        AttivitaEsecuzione = StepProc.Iniz_Procedura_export
        TMR_check.Enabled = True

    End Sub

    Private Sub FRM_printing_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        If Not _Thr2Exec Is Nothing Then
            AttivitaEsecuzione = StepProc.Exit_Forced
            While _Thr2Exec.IsAlive
                System.Threading.Thread.Sleep(200)
            End While
        End If

    End Sub

    Private Sub BTN_annulla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_annulla.Click

        AttivitaEsecuzione = StepProc.Exit_Forced

    End Sub

    Private Sub TMR_check_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMR_check.Tick

        If _Thr2Exec Is Nothing Then
            _dTimeStart = Now
            _UltimoGruppoStampato = 0
            _Thr2Exec = New System.Threading.Thread(New System.Threading.ThreadStart(AddressOf ProceduraExport))
            _Thr2Exec.Start()
        Else
            If _Thr2Exec.IsAlive Then
                LBL_tim_from_start.Text = FormattaOrario(Now - _dTimeStart)
                'If _UltimoGruppoStampato <> _GestStam.GruppoInStampa Then
                '    LBL_tim_to_end.Text = FC_utilita.FormattaOrario(TimeSpan.FromSeconds((Now - _dTimeStart).TotalSeconds / _GestStam.GruppoInStampa * (_GestStam.GruppiDaStampare - _GestStam.GruppoInStampa)))
                '    _UltimoGruppoStampato = _GestStam.GruppoInStampa
                'End If
                'If GRP_printing.Visible Then
                '    LBL_exp_run.Text = _GestStam.GruppoInStampa + 1
                '    PGB_stampa.Value = _GestStam.GruppoInStampa
                'End If
                'If GRP_act_in_prg.Visible Then
                '    LBL_act_runn.Text = _AttivitaEsecDes
                '    If PGB_dett_grp.Minimum <= AttivitaEsecuzione And AttivitaEsecuzione <= PGB_dett_grp.Maximum Then
                '        PGB_dett_grp.Value = AttivitaEsecuzione
                '    End If
                'End If
            Else
                If AttivitaEsecuzione = StepProc.Exit_Forced Then
                    Me.DialogResult = Windows.Forms.DialogResult.Cancel
                Else
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                End If
                Me.Close()
            End If
        End If

    End Sub

    ' ********************************************************************* '
    ' Procedura di Export dei moduli.                                       '
    ' ********************************************************************* '
    Private Sub ProceduraExport()

        ' ********************************************************************* '
        ' Procedo con la stampa dei moduli richiesti.                           '
        ' ********************************************************************* '
        AttivitaEsecuzione = StepProc.Iniz_LoopExport
        While _AttivitaEsecuzione < StepProc.Exit_Procedura_export And Not ExitForced
            Select Case AttivitaEsecuzione
                Case Is = StepProc.CaricaDatabase
                    CaricaDatabase()
                Case Is = StepProc.Check_ConsistenzaDB_1
                    CheckConsistenzaDB()
                Case Is = StepProc.Goto_FirstRecordDS_1
                    CreaDSToPrint()
                Case Is = StepProc.EsportaDati
                    EsportaDati()
                Case Is = StepProc.CLEAN_CartellaWork
                    Mantieni_CartellaWork()
            End Select

            ' ********************************************************************* '
            ' Imposta il prossimo passo da eseguire.                                '
            ' ********************************************************************* '
            AttivitaEsecuzione += 1
        End While
        '_GestStam.ExportEseguita = AttivitaEsecuzione <> StepProc.Exit_Forced

    End Sub

    Private Sub EsportaDati()
        'Dim expDati As ManageExport.CLS_export

        'expDati = New ManageExport.CLS_export
        'expDati.FileNameExport = ExportFileNameFN
        'expDati.DataToExport = _GestData
        'expDati.GestioneModulo = _GestModu
        'expDati.OnlySelected = _OnySelected
        'expDati.Esportazione()

    End Sub

    ' ********************************************************************* '
    ' Caricamento dei dati che andranno stampati.                           '
    ' ********************************************************************* '
    Private Sub CaricaDatabase()
        Dim nRecordToLoad As Integer

        ' _GestStam.FirstRecordPrinted = _GestSess.CurrentRecordNumber
        If _GestModu.ModuloSenzaSelezioneNecessaria Then
            nRecordToLoad = 1
        Else
            nRecordToLoad = 9999999
        End If

        _GestSess.Carica_FullDataset(nRecordToLoad)
        ' _GestStam.LastRecordPrinted = _GestSess.CurrentRecordNumber

    End Sub

    ' ********************************************************************* '
    ' Cancellazione dei documenti ottici precedentemente archiviati.        '
    ' ********************************************************************* '
    Private Sub SetUpError2QuickDataset(ByVal _StatusPrn As Dictionary(Of Integer, String()))

        'For nRec = _GestStam.FirstRecordPrinted To _GestStam.LastRecordPrinted - 1
        '    dr = _GestSess.QuickDataTable.Rows(nRec)
        '    nRecCode = dr(_GestSess.QuickDataset_MainTableKey)
        '    If _StatusPrn.ContainsKey(nRecCode) Then
        '        If (dr("DEF_errcode") = 0) And (_StatusPrn(nRecCode)(0) < 0) Then
        '            dr("DEF_errcode") = _StatusPrn(nRecCode)(0)
        '            dr("DEF_errdesc") = _StatusPrn(nRecCode)(1)
        '        End If
        '    End If
        'Next

    End Sub

    ' ********************************************************************* '
    ' Crea la classe per la gestione dell'oggetto per il database di stampa '
    ' ********************************************************************* '
    Private Sub CreaDSToPrint()

        '_GestData = New ManageExport.GestioneDataset(_GestSess.FullDataset, _GestSess.FullDataset_MainTableName)
        '_GestData.DatiFissiFileName = _GestSess.Files("PlugInDatiFissiFIL")
        '_GestData.MainLinkToQD = _GestSess.LinkField_QuickDS_FullDS

    End Sub

    Private Sub CheckConsistenzaDB()

        _GestSess.AggiornaQuickDataset()
        _GestSess.RemovePRNDSErrorRecord()

        ' ********************************************************************* '
        ' Se non ci sono pi� righe da stampare nel database di stampa provvedo  '
        ' a saltare al gruppo successivo.                                       '
        ' ********************************************************************* '
        If (_GestSess.FullDataSet_MainTable.Rows.Count = 0) Then
            AttivitaEsecuzione = StepProc.Last_LoopExport
        End If

    End Sub

    Private Sub Mantieni_CartellaWork()
        Dim aFileList As System.IO.FileInfo()
        Dim nDirSize As Long
        Dim i As Integer

        nDirSize = 0
        aFileList = New System.IO.DirectoryInfo(_GestSess.WorkFolder).GetFiles
        i = aFileList.GetUpperBound(0)
        While Not (i = -1)
            If nDirSize > 10000000 Then
                Try
                    System.IO.File.Delete(aFileList(i).FullName)
                Catch ex As Exception

                End Try
            Else
                nDirSize += aFileList(i).Length
            End If
            i -= 1
        End While

    End Sub

    WriteOnly Property OnlySelected() As Boolean

        Set(ByVal value As Boolean)
            _OnySelected = value
        End Set

    End Property

End Class
