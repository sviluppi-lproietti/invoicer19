<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_unlock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.TXB_password = New System.Windows.Forms.TextBox
        Me.BTN_sblocca = New System.Windows.Forms.Button
        Me.BTN_ignora = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Password"
        '
        'TXB_password
        '
        Me.TXB_password.Location = New System.Drawing.Point(71, 6)
        Me.TXB_password.Name = "TXB_password"
        Me.TXB_password.Size = New System.Drawing.Size(147, 20)
        Me.TXB_password.TabIndex = 1
        Me.TXB_password.UseSystemPasswordChar = True
        '
        'BTN_sblocca
        '
        Me.BTN_sblocca.Location = New System.Drawing.Point(45, 39)
        Me.BTN_sblocca.Name = "BTN_sblocca"
        Me.BTN_sblocca.Size = New System.Drawing.Size(75, 23)
        Me.BTN_sblocca.TabIndex = 2
        Me.BTN_sblocca.Text = "Sblocca"
        Me.BTN_sblocca.UseVisualStyleBackColor = True
        '
        'BTN_ignora
        '
        Me.BTN_ignora.Location = New System.Drawing.Point(126, 39)
        Me.BTN_ignora.Name = "BTN_ignora"
        Me.BTN_ignora.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignora.TabIndex = 3
        Me.BTN_ignora.Text = "Annulla"
        Me.BTN_ignora.UseVisualStyleBackColor = True
        '
        'FRM_unlock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(234, 74)
        Me.Controls.Add(Me.BTN_ignora)
        Me.Controls.Add(Me.BTN_sblocca)
        Me.Controls.Add(Me.TXB_password)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FRM_unlock"
        Me.Text = "Sblocca men�"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXB_password As System.Windows.Forms.TextBox
    Friend WithEvents BTN_sblocca As System.Windows.Forms.Button
    Friend WithEvents BTN_ignora As System.Windows.Forms.Button
End Class
