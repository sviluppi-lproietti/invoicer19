﻿Imports System.Reflection
Imports ISC.LibrerieComuni.InvoicerDomain.Impl
Imports ISC.Invoicer.ManageStampa
Imports ISC.LibrerieComuni.ManageSessione
Imports ISC.LibrerieComuni.ManageSessione.Entity
Imports ISC.LibrerieComuni.ManageSessione.MSE_Costanti
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML
Imports ISC.LibrerieComuni.OggettiComuniForm

Public Class FRM_Invoicer

    Private _RowColor As System.Drawing.Color
    Private _RowSelectedIndex As Integer
    Private _SelectA As Integer
    Private _SelectDa As Integer
    Private _SelectFromPlugInType As String
    Private _SelectMainta As Boolean
    Private _SelecRic As Boolean
    Private _TipoRic As String
    Private _TipoSelezione As TipoSelezione
    Private _ValueRic As String

    Private Property _SessioniMasterDisponibili As Dictionary(Of Integer, SessioneEntity)

    ' ********************************************************************* '
    ' Elenca le tipologie di selezione che possono essere fatte.            '
    ' ********************************************************************* '
    Enum TipoSelezione

        SEL_noselezioneeffettuata = 1

        SEL_tutti = 101
        SEL_nessuno = 102
        SEL_inverti = 103
        SEL_intervallo = 104
        SEL_intervallo_sel = 105
        SEL_intervallo_des = 106
        SEL_da_plugin = 107
        SEL_caricamodulo = 108
        SEL_con_err_gr = 109
        SEL_send_by_mail = 110
        SEL_cell_click = 201
        SEL_cm_seleziona = 202
        SEL_cm_deseleziona = 203
        SEL_da_ricerca = 204

    End Enum

    Public Sub New()
        'AddHandler Application.ThreadException, AddressOf OnThreadException

        'Added this
        AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledExceptionEventRaised

        InitializeComponent()

    End Sub

    'Added this
    Sub UnhandledExceptionEventRaised(ByVal sender As Object, ByVal e As UnhandledExceptionEventArgs)

        If e.IsTerminating Then
            Dim o As Object = e.ExceptionObject
            MessageBox.Show(o.ToString) ' use EventLog instead
        End If

    End Sub

    ' ********************************************************************* '
    ' Procedura per la preparazione della maschera principale dell'applica- '
    ' zione.                                                                '
    ' ********************************************************************* '
    Private Sub Invoicer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim drv As System.IO.DriveInfo
        Dim cFolderLimit As String
        Dim cSviluppoFLD As String
        Dim f As New FRM_start

        Try
            cSviluppoFLD = "D:\Invoicer_svi"
            ' ********************************************************************* '
            ' Caricamento della configurazione dell'applicazione.                   '
            ' ********************************************************************* '
            ObjGlobali.CFG = New CLS_cfg_inv(System.IO.Path.GetDirectoryName([Assembly].GetExecutingAssembly().Location))
            If ObjGlobali.CFG.SwitchPrdSvi = eSwitchPrdSvi.SPS_sviluppo Then
                drv = New System.IO.DriveInfo(cSviluppoFLD.Substring(0, 1))
                If drv.IsReady Then
                    ObjGlobali.CFG = New CLS_cfg_inv(cSviluppoFLD)
                    ObjGlobali.CFG.ApplicazioneAvviabile = True
                End If
            End If

            '
            ' Pulizia della cartella di LOG per le cartelle più vecchie di una settimana.
            '
            cFolderLimit = ConcatenaFolderValue(ObjGlobali.CFG.Folders("LogsFLD"), Now.AddDays(-7).ToString("yyyyMMdd"))
            For Each cFolder As String In System.IO.Directory.GetDirectories(ObjGlobali.CFG.Folders("LogsFLD"))
                If cFolder < cFolderLimit Then
                    System.IO.Directory.Delete(cFolder, True)
                End If
            Next

            '
            ' Creazione degli oggetti globali dell'applicazione
            '
            ObjGlobali.Logger = New CLS_logger(ObjGlobali.Folders("LogsFLD"), ObjGlobali.CFG.LogLevel, ObjGlobali.CFG.LogFileSize, "Invoicer")
            ObjGlobali.Logger.ScriviMessaggioLog(eLogLevel.LL_informational, 1, "", "Invoicer_Load", "Sistema operativo: " + CFG.VersioneSistemaOperativo)
            ObjGlobali.GestioneProcessoStampa = New CLS_GestioneProcessoStampa
            ObjGlobali.GestioneModuliStampare = New Dictionary(Of Integer, CLS_GestioneModuloStampare)
            ObjGlobali.GestioneModuliEsportare = New Dictionary(Of Integer, CLS_GestioneModuloStampare)
            ObjGlobali.GestioneSessioneStampa = New GestioneSessione(eTipoApplicazione.TAP_Invoicer, ObjGlobali.Folders, ObjGlobali.CFG.VersioneSistemaOperativo, CFG.OLEDB_ConnString)

            TAC_modulistampa.TabPages.RemoveAt(0)
            Imposta_TOS_menu()

            If Not ObjGlobali.CFG.IsLoaded Then
                MessageBox.Show("Attenzione! Non è presente il file di configurazione. Procedere alla configurazione utilizzando il menu Utilità>Configurazione", "File di configurazione mancante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
            f.ShowDialog()
            ObjGlobali.CFG.ApplicazioneAvviabile = True

            If ObjGlobali.CFG.ApplicazioneAvviabile Then
                If Not ObjGlobali.CFG.StampaCartaceaAblitata Then
                    MessageBox.Show("Attenzione! La stampante per la generazione dei file cartacei non è presente o non è stata configurata. Procedere alla configurazione utilizzando il menu Utilità>Configurazione", "Stampante ottica non presente", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
                If Not ObjGlobali.CFG.StampaOtticaAblitata Then
                    MessageBox.Show("Attenzione! La stampante per la generazione dei file pdf non è presente o non è stata configurata. Procedere alla configurazione utilizzando il menu Utilità>Configurazione", "Stampante ottica non presente", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
                If Not ObjGlobali.CFG.SuddivisioneModuli Then
                    MessageBox.Show("Attenzione! Non sono presenti i componenti per la suddivisione degli allegati. Questa funzione sarà disabilitata.", "Disabilita stampa ottica", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
                If Not ObjGlobali.CFG.IsPDFCreatorCorretto Then
                    MessageBox.Show("Attenzione! La versione di PDF Creator richiesta e quella installata sono differenti. Potrebbero verificarsi degli errori in caso di utilizzo.", "Librerie non corrette", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If

                ' ********************************************************************* '
                ' Rimuove tutte la TAB PAGE.                                            '
                ' ********************************************************************* '
                Imposta_TACPrincipale(-1)

                ' ********************************************************************* '
                ' Rimuove l'opzione per la generazione automatica delle colonne.        '
                ' ********************************************************************* '
                DGV_dati.AutoGenerateColumns = False
                DGV_stat_glob.AutoGenerateColumns = False
                DGV_stat_parz.AutoGenerateColumns = False
                DGV_stat_glob.Visible = ObjGlobali.CFG.LicenseSwitch
                DGV_stat_parz.Visible = ObjGlobali.CFG.LicenseSwitch

                _TipoSelezione = TipoSelezione.SEL_noselezioneeffettuata
                SetStatusBar()

                TSM_sessioni_recenti.Visible = ObjGlobali.CFG.LicenseSwitch
                If ObjGlobali.CFG.LicenseSwitch Then
                    SetMenuSessioniRecenti()
                Else
                    ClearSessioniRecenti()
                End If

                ' ********************************************************************* '
                ' Alcuni menu debbono essere visualizzati solo nella versione completa. '
                ' ********************************************************************* '
                TSM_Selezione.Visible = ObjGlobali.CFG.LicenseSwitch
                TSM_visualizza.Visible = ObjGlobali.CFG.LicenseSwitch
                TSM_gestione_giri.Visible = ObjGlobali.CFG.LicenseSwitch
                ToolStripMenuItem6.Visible = ObjGlobali.CFG.LicenseSwitch
                TSM_reset_prn_stat.Visible = ObjGlobali.CFG.LicenseSwitch
                TSM_EsportaFileDiLog.Visible = ObjGlobali.CFG.LicenseSwitch
                TSM_puliscicartellawork.Visible = ObjGlobali.CFG.LicenseSwitch

                TMR_sched_op.Start()
            Else
                MessageBox.Show("Attenzione! Non è stato possibile caricare il file di configurazione. L'applicazione non può essere avviata.", "Impossibile aprire l'applicazinoe", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

#Region "Procedure per la gestione del menu Sessione Stampa"

    Private Sub TSM_nuovo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_nuovo.Click
        Dim s As SessioneEntity
        Dim frm_new_sce As FRM_nuova_sessione
        Dim frm_new_wai As FRM_nuova_attesa

        Try
            ' ********************************************************************* '
            ' Inizio la raccolata dei dati per la creazione della nuova sessione.   '
            ' Controlla che esista la directory delle sessioni di stampa definite.  '
            ' ********************************************************************* '
            If System.IO.Directory.Exists(ObjGlobali.Folders("SessioniDefaultFLD")) Then
                ' ********************************************************************* '
                ' Recupera l'elenco delle sessioni contenute nella cartella.            '
                ' ********************************************************************* '
                _SessioniMasterDisponibili = New Dictionary(Of Integer, SessioneEntity)
                For Each cTmp As String In System.IO.Directory.GetDirectories(ObjGlobali.Folders("SessioniDefaultFLD"))
                    s = New SessioneEntity(cTmp.Replace(ObjGlobali.Folders("SessioniDefaultFLD"), ""), cTmp)
                    If s.SessioneValida Then
                        s.SES_codice = _SessioniMasterDisponibili.Count + 1
                        _SessioniMasterDisponibili.Add(s.SES_codice, s)
                    End If
                Next

                If _SessioniMasterDisponibili.Count > 0 Then
                    frm_new_sce = FC_forms.BuildForm(New FRM_nuova_sessione)
                    frm_new_sce.ListaSessioniDefault = Trasformazioni.TrasformaInLista(_SessioniMasterDisponibili)
                    If frm_new_sce.ShowDialog = Windows.Forms.DialogResult.OK Then
                        '
                        ' Procedo con la chiusura della sessione 
                        '
                        ChiusuraSessione()

                        '
                        ' Recupero i parametri che mi occorrono dalla sessione creata
                        '
                        ObjGlobali.GestioneSessioneStampa.Apertura(frm_new_sce.NuovaSessioneFLD, False)
                        ObjGlobali.GestioneSessioneStampa.DBGiriConnectionString = ObjGlobali.CFG.DBGiriConsegna_CS
                        ObjGlobali.GestioneSessioneStampa.DatiSessione = frm_new_sce.ListaDatiNuovaSessione
                        ObjGlobali.GestioneSessioneStampa.ListaSortGiri = frm_new_sce.ListaSortGiri

                        '
                        ' Procedo con la creazione della sessione con i nuovi dati.
                        '
                        frm_new_wai = FC_forms.BuildForm(New FRM_nuova_attesa)
                        If frm_new_wai.ShowDialog() = Windows.Forms.DialogResult.OK Then
                            If ObjGlobali.GestioneSessioneStampa.NuoviToponimi Then
                                MessageBox.Show("Attenzione sono stati recuperati dei toponimi nuovi per cui è possibile che l'ordinamento sia errato rispetto a qualche toponimo", "Nuovi toponimi trovati", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            End If
                            '
                            ' Procedo con il caricamento della sessione 
                            '
                            CaricamentoSessione(frm_new_sce.NuovaSessioneFLD)
                        End If
                        FC_forms.DestroyForm(frm_new_wai)
                    End If
                    FC_forms.DestroyForm(frm_new_sce)
                Else
                    MessageBox.Show("Attenzione! Non esistono sessioni di default all'interno del percorso specificato.", "Sessioni - Elenco sessioni di default", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            Else
                MessageBox.Show(String.Concat("Attenzione! Il percorso dove sono archiviati i modelli di stampa non esiste.", vbLf, "Non è possibile procedere con la creazione di una nuova sessione."), "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MessageBox.Show(String.Concat("Errore durante la creazione della sessione.", vbLf, "Descrizione dell'errore:", vbLf, ex.Message), "Errore durante la creazione sessione", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ObjGlobali.GestioneSessioneStampa = New GestioneSessione(eTipoApplicazione.TAP_Invoicer, ObjGlobali.Folders, ObjGlobali.CFG.VersioneSistemaOperativo, CFG.OLEDB_ConnString)
        End Try

    End Sub

    Private Sub TSM_apri_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_apri.Click
        Dim frm_open As FRM_apri
        Dim cFolder As String

        frm_open = FC_forms.BuildForm(New FRM_apri)
        cFolder = ObjGlobali.Folders.Item("SessioniCreateFLD", , , False)
        If Not (System.IO.Directory.Exists(cFolder)) Then
            MessageBox.Show("Attenzione! La cartella delle sessioni create (" + cFolder + ") non è stata trovata.")
            cFolder = ObjGlobali.Folders("RootApplicationFLD")
        End If

        frm_open.TXB_dest_folder.Text = cFolder
        If frm_open.ShowDialog = Windows.Forms.DialogResult.OK Then
            '
            ' Apro la sessione selezionata
            '
            CaricamentoSessione(frm_open.TXB_dest_folder.Text)
        End If
        FC_forms.DestroyForm(frm_open)

    End Sub

    Private Sub TSM_chiudi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_chiudi.Click

        '
        ' Procedo con la chiusura della sessione
        '
        ChiusuraSessione()
        SetQuadroStatistiche()

    End Sub

    Private Sub TSM_esci_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_esci.Click

        ChiusuraSessione()
        Me.Close()

    End Sub

    ' ********************************************************************* '
    ' Riempe il menu delle sessioni recenti.                                '
    ' ********************************************************************* '
    Private Sub SetMenuSessioniRecenti()
        Dim miTemp As ToolStripItem

        TSM_sessioni_recenti.DropDownItems.Clear()
        For Each s As SessioniRecentiEntity In GetSessioniRecenti()
            miTemp = New ToolStripMenuItem(String.Concat(s.SER_codice, ") ", s.SER_nome))
            miTemp.Tag = s.SER_percorso
            miTemp.ToolTipText = s.SER_percorso
            AddHandler miTemp.Click, AddressOf TSM_Sessioni_Recenti_Click
            TSM_sessioni_recenti.DropDownItems.Add(miTemp)
        Next
        TSM_sessioni_recenti.Enabled = TSM_sessioni_recenti.DropDownItems.Count > 0

    End Sub

#End Region

#Region "Procedure per la gestione del menu Ricerca"

    Private Sub TSM_ric_progressivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_ric_progressivo.Click
        Dim frm_ric_progr As FRM_ric_progressivo
        Dim lRicerca As Boolean

        _TipoRic = CType(sender, ToolStripMenuItem).Name.ToLower
        lRicerca = False
        Select Case _TipoRic
            Case Is = "tsm_ric_progressivo"
                frm_ric_progr = FC_forms.BuildForm(New FRM_ric_progressivo)
                If frm_ric_progr.ShowDialog = Windows.Forms.DialogResult.OK Then
                    _ValueRic = frm_ric_progr.TXB_progressivo.Text
                    _SelecRic = frm_ric_progr.CHB_ric_select.Checked
                    lRicerca = True
                End If
                FC_forms.DestroyForm(frm_ric_progr)
        End Select

        If lRicerca Then
            If FC_forms.EseguiOperazioneInThread("Ricerca elemento richiesto in corso...", New Threading.Thread(AddressOf RicercaRigaSelezionata)) Then
                If _RowSelectedIndex > -1 Then
                    DGV_dati.FirstDisplayedScrollingRowIndex = _RowSelectedIndex
                    If _SelecRic Then
                        SelezioneGenerica("sel_da_ricerca")
                    End If
                Else
                    MessageBox.Show("Attenzione! La ricerca non ha trovato nessun record con il criterio selezionato.", "Ricerca senza esito", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
        End If

    End Sub

#End Region

#Region "Procedure per la gestione del menu Seleziona"

    Private Sub TSM_SelezioneGenerica_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_sel_tutti.Click, TSM_sel_nessuno.Click, TSM_sel_inverti.Click, TSM_con_err_gr.Click

        SelezioneGenerica(CType(sender, ToolStripMenuItem).Name.ToLower)

    End Sub

    Private Sub TSM_send_by_mail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_send_by_mail.Click

        SelezioneGenerica(CType(sender, ToolStripMenuItem).Name.ToLower)

    End Sub

#End Region

#Region "Procedure per la gestione del menu Visualizza"

    Private Sub TSM_visualizzagenerica_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_vis_error.Click, TSM_vis_tutti.Click, TSM_vis_noerror.Click, TSM_vis_err_load_gravi.Click, TSM_vis_err_load_lievi.Click, TSM_vis_err_prn.Click
        Dim dv_dati As DataView

        dv_dati = DGV_dati.DataSource
        Select Case CType(sender, ToolStripMenuItem).Name.ToLower
            Case Is = "tsm_vis_tutti"
                dv_dati.RowFilter = ""
            Case Is = "tsm_vis_noerror"
                dv_dati.RowFilter = "DEF_errcode = 0"
            Case Is = "tsm_vis_error"
                dv_dati.RowFilter = "DEF_errcode < 0"
            Case Is = "tsm_vis_err_load_gravi"
                dv_dati.RowFilter = "DEF_errcode = -1"
            Case Is = "tsm_vis_err_load_lievi"
                dv_dati.RowFilter = "DEF_errcode = -2"
            Case Is = "tsm_vis_err_prn"
                dv_dati.RowFilter = "DEF_errcode = -10"
        End Select

    End Sub

#End Region

#Region "Procedure per la gestione del menu Utilità"

    Private Sub TSM_configurazioni_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_configurazioni.Click
        Dim frm_opt As FRM_opzioni

        frm_opt = FC_forms.BuildForm(New FRM_opzioni)
        frm_opt.ShowDialog()
        FC_forms.DestroyForm(frm_opt)
        SetStatusBar()

    End Sub

    Private Sub TSM_reset_prn_stat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_reset_prn_stat.Click

        If MessageBox.Show("Attenzione! Azzerando le statistiche di stampa queste non saranno più disponibili. Procedo?", "Azzeramento statistiche", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            ObjGlobali.GestioneSessioneStampa.ResetStatisticheStampe()
            DGV_stat_glob.DataSource = ObjGlobali.GestioneSessioneStampa.ListaStatisticheStampaGlobali
            DGV_stat_parz.DataSource = ObjGlobali.GestioneSessioneStampa.ListaStatisticheStampaParziali
        End If

    End Sub

    Private Sub TSM_pulisci_modulo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_pulisci_modulo.Click
        Dim frm_clean_modulo As FRM_pulisci_modulo

        frm_clean_modulo = FC_forms.BuildForm(New FRM_pulisci_modulo)
        frm_clean_modulo.ShowDialog()
        FC_forms.DestroyForm(frm_clean_modulo)

    End Sub

    'Private Sub TSM_puliscicartellawork_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_puliscicartellawork.Click

    '    EseguiPuliziaFldWork()

    'End Sub

    Private Sub TSM_EsportaFileDiLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_EsportaFileDiLog.Click
        Dim sw As System.IO.StreamWriter
        Dim dr_log As DS_log.TBL_logRow
        Dim cFileName As String

        cFileName = String.Concat(ObjGlobali.Folders("LogsFLD"), Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute, Now.Second, "_log_sess_", ObjGlobali.GestioneSessioneStampa.Nome, ".txt")
        sw = New System.IO.StreamWriter(cFileName)
        sw.WriteLine("--- INIZIO ---")
        sw.WriteLine(String.Concat("Log del ", Now.ToString))
        For Each dr_log In CType(ObjGlobali.GestioneSessioneStampa.LogDSStampa, DS_log).TBL_log.Rows
            sw.WriteLine(String.Concat("Record: ", dr_log.LOG_desc_rec_1))
            Select Case dr_log.LOG_stato
                Case Is = 0
                    sw.WriteLine("Stato: OK")
                Case Is = -1
                    sw.WriteLine("Stato: Errore Caricamento dati dal db: GRAVE")
                Case Is = -2
                    sw.WriteLine("Stato: Errore Caricamento dati dal db: LIEVE")
                Case Is = -10
                    sw.WriteLine("Stato: Errore Stampa")
                Case Is = -11
                    sw.WriteLine("Stato: Stampa non effettuabile. Numero di fogli superiore al consentito.")
                Case Else
                    sw.WriteLine(String.Concat("Stato: ", dr_log.LOG_stato.ToString, " NON IDENTIFICATO"))
            End Select
            If dr_log.LOG_stato < 0 Then
                sw.WriteLine("Descrizione dell'errore ")
                sw.WriteLine(dr_log.LOG_desc_err)
            End If
            sw.WriteLine("-------------------------------------------------------------------------------------------------")
        Next
        sw.WriteLine("--- FINE ---")
        sw.Close()

    End Sub

#End Region

    Private Sub CSM_Selezionegenerica_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CSM_seleziona.Click, CSM_deseleziona.Click

        SelezioneGenerica(CType(sender, ToolStripMenuItem).Name.ToLower)
        DGV_dati.Refresh()

    End Sub

    Private Sub SelezioneRange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_sel_range.Click, TSM_sel_norange.Click
        Dim frm_sr As FRM_select_range

        frm_sr = FC_forms.BuildForm(New FRM_select_range)
        If CType(sender, ToolStripMenuItem).Name.ToLower = "tsm_sel_range" Then
            frm_sr.Text = "Seleziona intervallo"
            _TipoSelezione = TipoSelezione.SEL_intervallo_sel
        Else
            frm_sr.Text = "Deseleziona intervallo"
            _TipoSelezione = TipoSelezione.SEL_intervallo_des
        End If
        frm_sr.CHB_hold_sel.Visible = _TipoSelezione = TipoSelezione.SEL_intervallo_sel
        If frm_sr.ShowDialog = Windows.Forms.DialogResult.OK Then
            _SelectDa = frm_sr.TXB_da.Text
            _SelectA = frm_sr.TXB_a.Text
            _SelectMainta = frm_sr.CHB_hold_sel.Checked
            SelezioneGenerica("")
        End If
        FC_forms.DestroyForm(frm_sr)

    End Sub

    Private Sub SelezioneGenerica(ByVal cNameCtr As String)
        Dim nOldRecordSelezionati As Integer

        If _TipoSelezione = TipoSelezione.SEL_noselezioneeffettuata Then
            Select Case cNameCtr
                Case Is = "tsm_sel_tutti"
                    _TipoSelezione = TipoSelezione.SEL_tutti
                Case Is = "tsm_sel_nessuno"
                    _TipoSelezione = TipoSelezione.SEL_nessuno
                Case Is = "tsm_con_err_gr"
                    _TipoSelezione = TipoSelezione.SEL_con_err_gr
                Case Is = "tsm_sel_inverti"
                    _TipoSelezione = TipoSelezione.SEL_inverti
                Case Is = "tsm_send_by_mail"
                    _TipoSelezione = TipoSelezione.SEL_send_by_mail
                Case Is = "caricamodulo"
                    _TipoSelezione = TipoSelezione.SEL_caricamodulo
                Case Is = "csm_seleziona"
                    _TipoSelezione = TipoSelezione.SEL_cm_seleziona
                Case Is = "csm_deseleziona"
                    _TipoSelezione = TipoSelezione.SEL_cm_deseleziona
                Case Is = "selezionacellclick"
                    _TipoSelezione = TipoSelezione.SEL_cell_click
                Case Is = "sel_da_ricerca"
                    _TipoSelezione = TipoSelezione.SEL_da_ricerca
                Case Else
                    _TipoSelezione = TipoSelezione.SEL_da_plugin
                    _SelectFromPlugInType = cNameCtr
            End Select
        End If
        nOldRecordSelezionati = ObjGlobali.GestioneSessioneStampa.RecordSelezionati
        If FC_forms.EseguiOperazioneInThread("Modifica elementi selezionati in corso...", New Threading.Thread(AddressOf SelectItemToPrint)) Then
            If (ObjGlobali.GestioneSessioneStampa.RecordSelezionati <> nOldRecordSelezionati) And (ObjGlobali.GestioneSessioneStampa.RecordSelezionati = 0 Or nOldRecordSelezionati = 0) Then
                AbilitaBottoniAvvioAttivita()
            End If
            SetQuadroStatistiche()
        End If
        _TipoSelezione = TipoSelezione.SEL_noselezioneeffettuata

    End Sub

    ' ********************************************************************* '
    ' Questa procedura si occupa di selezionare i record del QuickDataset   ' 
    ' in base alle condizioni indicate.                                     '
    ' ********************************************************************* '
    Private Sub SelectItemToPrint()
        Dim nOldTipoSelezione As TipoSelezione
        Dim dgvr As DataGridViewRow
        Dim dr As DataRow

        If Not ObjGlobali.CFG.LicenseSwitch Then
            nOldTipoSelezione = _TipoSelezione
            _TipoSelezione = TipoSelezione.SEL_nessuno
            For Each dr In ObjGlobali.GestioneSessioneStampa.QuickDataTable.Rows
                SetSelezioneRecord(dr)
            Next
            _TipoSelezione = nOldTipoSelezione
        End If

        If CInt(_TipoSelezione / 100) = 2 Then
            For Each dgvr In DGV_dati.SelectedRows
                SetSelezioneRecord(CType(dgvr.DataBoundItem, DataRowView).Row)
            Next
        End If

        ObjGlobali.GestioneSessioneStampa.RecordSelezionati = 0
        For Each dr In ObjGlobali.GestioneSessioneStampa.QuickDataTable.Rows
            If CInt(_TipoSelezione / 100) = 1 Then
                If _TipoSelezione = TipoSelezione.SEL_caricamodulo Then
                    ObjGlobali.GestioneSessioneStampa.SetSelectable(dr)
                    dr.AcceptChanges()
                Else
                    SetSelezioneRecord(dr)
                End If
            End If
            ObjGlobali.GestioneSessioneStampa.ContaSelezionati(dr)
        Next

    End Sub

    Private Sub SetSelezioneRecord(ByVal dr As DataRow)

        If dr("DEF_selectable") Then
            Select Case _TipoSelezione
                Case Is = TipoSelezione.SEL_tutti, TipoSelezione.SEL_cm_seleziona, TipoSelezione.SEL_da_ricerca
                    dr("DEF_toprint") = True
                Case Is = TipoSelezione.SEL_nessuno, TipoSelezione.SEL_cm_deseleziona
                    dr("DEF_toprint") = False
                Case Is = TipoSelezione.SEL_inverti, TipoSelezione.SEL_cell_click
                    dr("DEF_toprint") = Not dr("DEF_toprint")
                Case Is = TipoSelezione.SEL_intervallo_sel, TipoSelezione.SEL_intervallo_des
                    If _SelectDa <= dr(ObjGlobali.GestioneSessioneStampa.QuickDataset_MainTableKey) And dr(ObjGlobali.GestioneSessioneStampa.QuickDataset_MainTableKey) <= _SelectA Then
                        dr("DEF_toprint") = _TipoSelezione = TipoSelezione.SEL_intervallo_sel
                    Else
                        If Not _SelectMainta And (_TipoSelezione = TipoSelezione.SEL_intervallo_sel) Then
                            dr("DEF_toprint") = False
                        End If
                    End If
                Case Is = TipoSelezione.SEL_da_plugin
                    ObjGlobali.GestioneSessioneStampa.SetPrintable(dr, _SelectFromPlugInType, "")
                Case Is = TipoSelezione.SEL_send_by_mail
                    dr("DEF_toprint") = dr("DEF_sendbymail")
            End Select
        Else
            Select Case _TipoSelezione
                Case Is = TipoSelezione.SEL_con_err_gr
                    dr("DEF_toprint") = dr("DEF_errcode") = -1
                Case Else
                    dr("DEF_toprint") = False
            End Select
        End If
        dr.AcceptChanges()

    End Sub

#Region "Procedure per la gestione della tab page Dati da stampare"

    Private Sub DGV_dati_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_dati.CellContentClick

        If e.ColumnIndex = 0 Then
            SelezioneGenerica("selezionacellclick")
        End If

    End Sub

    Private Sub DGV_dati_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DGV_dati.CellFormatting
        Dim dr As DataGridViewRow

        dr = DGV_dati.Rows(e.RowIndex)
        If Not DGV_dati.Columns.GetFirstColumn(DataGridViewElementStates.Displayed) Is Nothing Then
            If e.ColumnIndex = DGV_dati.Columns.GetFirstColumn(DataGridViewElementStates.Displayed).Index Then
                ' ********************************************************************* '
                ' In base al valore della cella dell'errore coloro la casella.          '
                ' ********************************************************************* '
                Select Case dr.Cells(1).Value
                    Case Is = 0
                        If dr.DataBoundItem("DEF_selectable") Then
                            If dr.Cells(0).Value Then
                                _RowColor = Color.Yellow
                            Else
                                _RowColor = Color.White
                            End If
                        Else
                            _RowColor = Color.BurlyWood
                        End If
                    Case Is = -1
                        _RowColor = Color.Orange
                    Case Is = -2
                        _RowColor = Color.Pink
                    Case Is = -3
                        _RowColor = Color.Bisque
                    Case Is = -10
                        _RowColor = Color.OrangeRed
                    Case Is = -201, -202, -203
                        _RowColor = Color.IndianRed
                    Case Is = -11
                        _RowColor = Color.Olive
                    Case Else
                        _RowColor = Color.Gray
                End Select
                If dr.Selected Then
                    SetBoxMsgErrore()
                End If
            End If
            If DGV_dati.Columns(e.ColumnIndex).Tag = "boolean" Then
                If dr.DataBoundItem(DGV_dati.Columns(e.ColumnIndex).DataPropertyName) Then
                    e.Value = "Si"
                Else
                    e.Value = "No"
                End If
            ElseIf DGV_dati.Columns(e.ColumnIndex).Tag > "" Then
                If dr.DataBoundItem(DGV_dati.Columns(e.ColumnIndex).DataPropertyName) Then
                    e.Value = DGV_dati.Columns(e.ColumnIndex).Tag.ToString.Split(";")(1)
                Else
                    e.Value = DGV_dati.Columns(e.ColumnIndex).Tag.ToString.Split(";")(0)
                End If
            End If
            dr.DefaultCellStyle.BackColor = _RowColor
        End If

    End Sub

    Private Sub CaricamentoModulo1(ByVal lReload As Boolean)

        'With ObjGlobali.GestioneModuliStampare(1).Modulo
        '    If .IsOkModulo Then
        '        ObjGlobali.GestioneModuliStampare(1).FattoreCorrezioneX = ObjGlobali.GestioneModuliStampare(1).Modulo.FattoreCorrezioneX
        '        ObjGlobali.GestioneModuliStampare(1).FattoreCorrezioneY = ObjGlobali.GestioneModuliStampare(1).Modulo.FattoreCorrezioneY
        '        If lReload Then
        '            If (CHB_fronte_retro_00.Checked <> .FronteRetro) Then
        '                If MessageBox.Show(String.Concat("Attenzione!! L'impostazione del fronte retro per il modulo è diversa da quella impostata.", vbLf, "La sostituisco con quella del modulo?"), "Impostazione Fronte retro", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No Then

        '                End If
        '            End If
        '        Else
        '            CHB_fronte_retro_00.Checked = .FronteRetro
        '            SelezioneGenerica("caricamodulo")
        '        End If
        '        ' SetImpostazioniStampante()
        '        DGV_dati.Refresh()
        '        ' SetDSChooseTray()
        '    End If
        'End With

    End Sub

#End Region

#Region "Procedure relative alla gestione del TAB Page denominata stampa"

    Private Sub BTN_stampa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_stampa.Click
        Dim frm_esecatt As FRM_EsecuzioneAttivita
        Dim m As CLS_GestioneModuloStampare
        Dim lPossoStampare As Boolean
        Dim f As FlowLayoutPanel
        Dim g As GroupBox

        lPossoStampare = True

        '
        ' Verifica che i dati opzionali inseriti siano stati realmente inseriti.
        '
        If (ObjGlobali.GestioneSessioneStampa.MessaggioAvviso > "") And (Not ObjGlobali.GestioneSessioneStampa.CheckDatiVar Or ObjGlobali.CFG.CheckDatiVar) Then
            ObjGlobali.GestioneSessioneStampa.CheckDatiVar = (MessageBox.Show(ObjGlobali.GestioneSessioneStampa.MessaggioAvviso, "Verifica", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes)
            lPossoStampare = ObjGlobali.GestioneSessioneStampa.CheckDatiVar
        End If

        '
        ' Avvertenza che qualora si proceda con la stampa ottica i file precedenti saranno cancellati.
        '
        If (lPossoStampare And CHB_prn_arch_ott.Checked) And System.IO.Directory.GetFiles(ObjGlobali.GestioneSessioneStampa.Folders("ArchivioOtticoFLD")).Length > 0 Then
            lPossoStampare = MessageBox.Show("Attenzione! Durante la generazione dei file ottici verranno, eventualmente, rimossi quelli precedentemente generati. Procedo?", "Verifica", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes
        End If

        If lPossoStampare And (ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 1) Then
            For Each k As KeyValuePair(Of Integer, CLS_GestioneModuloStampare) In ObjGlobali.GestioneModuliStampare
                m = CType(k.Value, CLS_GestioneModuloStampare)
                If lPossoStampare And m.DaStampare Then
                    Try
                        If m.Modulo.CheckModuloModified Then
                            If MessageBox.Show("Attenzione il modulo risulta modificato. Carico la nuova versione o proseguo con la vecchia?", "Modulo Modificato", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                                m.Modulo.Carica_Modulo()
                            Else
                                lPossoStampare = False
                            End If
                        End If
                    Catch ex As Exception
                        MessageBox.Show("Attenzione! Errore caricando il modulo aggiornato. Verrà stampato comunque utilizzando il vecchio modulo.", "Modulo Modificato", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                End If
            Next
        End If

        If lPossoStampare Then
            ObjGlobali.GestioneSessioneStampa.GoToRecordNumber(0)
            With ObjGlobali.GestioneProcessoStampa
                .FirstRecord2Manage = 0
                .NumeroDocumenti = NUD_documenti_gruppo.Value
                .GruppiDaStampare = ObjGlobali.GestioneSessioneStampa.RecordSelezionati
                .PulisciFolderElaborati = CHB_clean_FLD_elab.Checked
                .FondiModuli = CHB_fusione_moduli.Checked
                .ForceStampaCartaceo = CHB_force_paper.Checked
                .ArchiviazioneOttica = CHB_prn_arch_ott.Checked
                .FastPrintEnabled = CHB_fast_print.Checked
            End With
            For Each k As KeyValuePair(Of Integer, CLS_GestioneModuloStampare) In ObjGlobali.GestioneModuliStampare
                m = CType(k.Value, CLS_GestioneModuloStampare)
                If m.DaStampare Then
                    m.StampaForzataDettaglio = CType(_PannellogestioneModulo(m.CodiceModulo).Controls(BuildControlName("CHB_stampa_dettaglio_", m.CodiceModulo)), CheckBox).Checked
                    f = _PannellogestioneModulo(m.CodiceModulo).Controls(BuildControlName("FLP_moduli_", m.CodiceModulo))
                    If ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 1 Then
                        m.ArchiviazioneOttica = CHB_prn_arch_ott.Checked
                        If m.ArchiviazioneOttica Then
                            '
                            ' Impostazioni per la stampa in modalità elettronica
                            '
                            g = f.Controls(BuildControlName("GRB_stampante_ele_", m.CodiceModulo))
                            m.DatiPerStampa.FattoreCorrezione.FCS_coord_x = CType(g.Controls(BuildControlName("TXB_fat_cor_ele_x_", m.CodiceModulo)), TextBox).Text
                            m.DatiPerStampa.FattoreCorrezione.FCS_coord_y = CType(g.Controls(BuildControlName("TXB_fat_cor_ele_y_", m.CodiceModulo)), TextBox).Text
                            m.FronteRetro = False
                            m.ResetWrapRound = CType(g.Controls(BuildControlName("CHB_reset_cod_wr_ele_", m.CodiceModulo)), CheckBox).Checked
                            m.WrapRound = CType(g.Controls(BuildControlName("TXB_cod_wr_ele_", m.CodiceModulo)), TextBox).Text
                            m.RendiPaginePari = CType(g.Controls(BuildControlName("CHB_rendi_pari_", m.CodiceModulo)), CheckBox).Checked
                        Else
                            '
                            ' Impostazioni per la stampa in modalità cartacea
                            '
                            g = f.Controls(BuildControlName("GRB_stampante_car_", m.CodiceModulo))
                            m.DatiPerStampa.FattoreCorrezione.FCS_coord_x = CType(g.Controls(BuildControlName("TXB_fat_cor_car_x_", m.CodiceModulo)), TextBox).Text
                            m.DatiPerStampa.FattoreCorrezione.FCS_coord_y = CType(g.Controls(BuildControlName("TXB_fat_cor_car_y_", m.CodiceModulo)), TextBox).Text
                            m.FronteRetro = CType(g.Controls(BuildControlName("CHB_fronte_retro_", m.CodiceModulo)), CheckBox).Checked
                            m.ResetWrapRound = CType(g.Controls(BuildControlName("CHB_reset_cod_wr_car_", m.CodiceModulo)), CheckBox).Checked
                            m.WrapRound = CType(g.Controls(BuildControlName("TXB_cod_wr_car_", m.CodiceModulo)), TextBox).Text
                            m.RendiPaginePari = False
                        End If
                        m.DatiPerStampa.MaxNumeroFogli = ObjGlobali.CFG.MaxNumeroFogli
                        m.NoSplitAllegati = Not ObjGlobali.CFG.SuddivisioneModuli
                        m.DatiPerStampa.PaginePerParte = ObjGlobali.CFG.PaginePerParte
                        m.SuffissoParte = ObjGlobali.CFG.SuffissoParte
                        m.PreparaPerStampa()
                    ElseIf ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 2 Then
                        g = f.Controls(BuildControlName("GRB_generazione_", m.CodiceModulo))
                        'm.BarcodeImbX = CType(g.Controls(BuildControlName("TXB_Coordinata_X_", m.CodiceModulo)), TextBox).Text
                        'm.BarcodeImbY = CType(g.Controls(BuildControlName("TXB_Coordinata_Y_", m.CodiceModulo)), TextBox).Text
                        'm.AltezzaBarCode = CType(g.Controls(BuildControlName("TXB_Altezza_Barcode_", m.CodiceModulo)), TextBox).Text
                    End If
                End If
            Next
            frm_esecatt = FC_forms.BuildForm(New FRM_EsecuzioneAttivita)
            frm_esecatt.TipoAttivita = FRM_EsecuzioneAttivita.eTipoAttivita.Stampa
            If frm_esecatt.ShowDialog() = Windows.Forms.DialogResult.OK Then

            End If
            DGV_stat_glob.DataSource = ObjGlobali.GestioneSessioneStampa.ListaStatisticheStampaGlobali
            DGV_stat_parz.DataSource = ObjGlobali.GestioneSessioneStampa.ListaStatisticheStampaParziali
            FC_forms.DestroyForm(frm_esecatt)
        End If

    End Sub

#End Region

#Region "Procedure di utilità"

    ' ********************************************************************* '
    ' Procedura per la gestione della visualizzazione delle Tab Page Princi '
    ' pale.                                                                 '
    ' ********************************************************************* '
    Private Sub Imposta_TACPrincipale(ByVal nTypeOperation As Integer)

        If nTypeOperation = -1 Then
            TAC_principale.TabPages.Remove(TAP_dati)
            TAC_principale.TabPages.Remove(TAP_stampa)
            TAC_principale.TabPages.Remove(TAP_export)
        ElseIf nTypeOperation = 1 Then
            TAC_principale.TabPages.Add(TAP_dati)
            TAC_principale.TabPages.Add(TAP_stampa)
        ElseIf nTypeOperation = 2 Then
            TAC_principale.TabPages.Add(TAP_dati)
            TAC_principale.TabPages.Add(TAP_export)
        ElseIf nTypeOperation = 3 Then
            TAC_principale.TabPages.Add(TAP_dati)
            TAC_principale.TabPages.Add(TAP_stampa)
            TAC_principale.TabPages.Add(TAP_export)
        End If

    End Sub

#End Region

    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean
        Dim frm As FRM_unlock

        Select Case keyData
            Case Keys.Control Or Keys.Alt Or Keys.Shift Or Keys.F12
                If ObjGlobali.GestioneSessioneStampa.IsLoaded Then
                    frm = FC_forms.BuildForm(New FRM_unlock)
                    If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                        ToolStripMenuItem8.Visible = True
                        TSM_pulisci_modulo.Visible = True
                    End If
                    FC_forms.DestroyForm(frm)
                End If
            Case Else
                MyBase.ProcessCmdKey(msg, keyData)
        End Select

    End Function

    Sub RicercaRigaSelezionata()
        Dim dr_qd As DataGridViewRow
        Dim aResult As String()

        _RowSelectedIndex = -1
        aResult = ObjGlobali.GestioneSessioneStampa.GetSelectedRow(_TipoRic, _ValueRic)
        If aResult IsNot Nothing Then
            For Each dr_qd In DGV_dati.Rows
                dr_qd.Selected = dr_qd.DataBoundItem(aResult(0)) = aResult(1)
                If dr_qd.Selected Then
                    _RowSelectedIndex = DGV_dati.Rows.IndexOf(dr_qd)
                End If
            Next
        End If

    End Sub

    Private Sub ImpostaFattoreCorrezioneToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImpostaFattoreCorrezioneToolStripMenuItem.Click
        Dim cCtrlNameA As String
        Dim cCtrlNameB As String
        Dim f As FlowLayoutPanel
        Dim tmpTab As TabPage

        For Each nCodiceModulo As Integer In ObjGlobali.GestioneSessioneStampa.ElencoModuli.Keys
            If ObjGlobali.GestioneSessioneStampa.ElencoModuli(nCodiceModulo).DatiModulo.DaStampare Then
                tmpTab = Me.Controls("TAC_principale").Controls("TAP_stampa").Controls("TAC_modulistampa").Controls(BuildControlName("TAP_modulo_", nCodiceModulo))
                f = tmpTab.Controls(BuildControlName("FLP_moduli_", nCodiceModulo))
                cCtrlNameA = BuildControlName("GRB_stampante_car_", nCodiceModulo)
                cCtrlNameB = BuildControlName("TXB_fat_cor_car_x_", nCodiceModulo)
                ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.FattoreCorrezione_Cartaceo.FCS_coord_x = Integer.Parse(CType(CType(f.Controls(cCtrlNameA), GroupBox).Controls(cCtrlNameB), TextBox).Text)

                cCtrlNameB = BuildControlName("TXB_fat_cor_car_y_", nCodiceModulo)
                ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.FattoreCorrezione_Cartaceo.FCS_coord_y = Integer.Parse(CType(CType(f.Controls(cCtrlNameA), GroupBox).Controls(cCtrlNameB), TextBox).Text)

                cCtrlNameA = BuildControlName("GRB_stampante_ele_", nCodiceModulo)
                cCtrlNameB = BuildControlName("TXB_fat_cor_ele_x_", nCodiceModulo)
                ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.FattoreCorrezione_Ottico.FCS_coord_x = Integer.Parse(CType(CType(f.Controls(cCtrlNameA), GroupBox).Controls(cCtrlNameB), TextBox).Text)

                cCtrlNameB = BuildControlName("TXB_fat_cor_ele_y_", nCodiceModulo)
                ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.FattoreCorrezione_Ottico.FCS_coord_y = Integer.Parse(CType(CType(f.Controls(cCtrlNameA), GroupBox).Controls(cCtrlNameB), TextBox).Text)


                ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.Salva_FattoriCorrezione()
            End If
        Next

    End Sub

    Private Sub TSM_Sessioni_Recenti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim nCodeSess As Integer

        If TypeOf sender Is ToolStripItem Then
            ChiusuraSessione()

            '
            ' Procedo con il caricamento di una sessione.
            '
            CaricamentoSessione(CType(sender, ToolStripItem).Tag)

            '
            ' Se la sessione non è stata caricata allora la rimuovo dalla lista delle sessioni recenti attive.
            '
            If Not ObjGlobali.GestioneSessioneStampa.IsLoaded Then
                nCodeSess = CType(sender, ToolStripItem).Text.Substring(0, CType(sender, ToolStripItem).Text.IndexOf(")"))
                SetSessioniRecenti("", "", nCodeSess)
                SetMenuSessioniRecenti()
            End If
        End If

    End Sub

    Private Sub DGV_dati_CellToolTipTextNeeded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellToolTipTextNeededEventArgs) Handles DGV_dati.CellToolTipTextNeeded
        Dim dr As DataRow
        Dim cError As String

        If e.RowIndex > -1 Then
            dr = CType(DGV_dati.Rows(e.RowIndex).DataBoundItem, DataRowView).Row
            Select Case dr("DEF_errcode")
                Case Is = 0
                    cError = "Nessun Errore."
                    LBL_err_desc.Text = "N/D"
                Case Is = -1, -2
                    cError = "Errore in fase di caricamento dei dati."
                Case Is = -201, -202, -203
                    cError = "Errore in fase di gestione del file per l'achiviazione ottica"
                Case Is = -301
                    cError = "Errore in fase di stampa dei dati."
                Case Else
                    cError = "Errore sconosciuto."
            End Select
            e.ToolTipText = String.Concat("Errore: ", cError, vbCr, "Descrizione: ", dr("DEF_errdesc"))
        End If

    End Sub

    Private Sub ResetStatoErroreToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResetStatoErroreToolStripMenuItem.Click
        Dim dgvr As DataGridViewRow
        Dim dr As DataRow

        For Each dgvr In DGV_dati.SelectedRows
            dr = CType(dgvr.DataBoundItem, DataRowView).Row
            dr("DEF_errcode") = 0
            dr("DEF_errdesc") = ""
            dr("DEF_selectable") = True
            dr.AcceptChanges()
        Next

    End Sub

    Private Sub AggiungiAllegatiDaInviareToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AggiungiAllegatiDaInviareToolStripMenuItem.Click
        Dim frm_att As FRM_attach_mail

        frm_att = FC_forms.BuildForm(New FRM_attach_mail)
        frm_att.GestSess = ObjGlobali.GestioneSessioneStampa
        If frm_att.ShowDialog = Windows.Forms.DialogResult.OK Then

        End If
        FC_forms.DestroyForm(frm_att)

    End Sub

    Private Sub BTN_stampa_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'If BTN_stampa.Enabled And _cfg.StampaCartaceaAbilitata And _cfg.StampaOtticaAbilitata Then
        '    CHB_prn_arch_ott.Checked = False
        '    CHB_prn_arch_ott.Enabled = True
        'ElseIf BTN_stampa.Enabled And _cfg.StampaOtticaAbilitata Then
        '    CHB_prn_arch_ott.Checked = True
        '    CHB_prn_arch_ott.Enabled = False
        'ElseIf BTN_stampa.Enabled And _cfg.StampaCartaceaAbilitata Then
        '    CHB_prn_arch_ott.Checked = False
        '    CHB_prn_arch_ott.Enabled = False
        'ElseIf Not BTN_stampa.Enabled Then
        '    CHB_prn_arch_ott.Checked = False
        '    CHB_prn_arch_ott.Enabled = False
        'End If

    End Sub

    '
    ' Procedure da posizionare.
    '
    Private Sub SetQuadroStatistiche()

        If ObjGlobali.GestioneSessioneStampa.IsLoaded Then
            LBL_rec_load.Text = ObjGlobali.GestioneSessioneStampa.RecordCaricati
            LBL_rec_sel.Text = ObjGlobali.GestioneSessioneStampa.RecordSelezionati
            LBL_err_car_grav.Text = ObjGlobali.GestioneSessioneStampa.RecordErrCaricamentoGravi
            LBL_err_car_liev.Text = ObjGlobali.GestioneSessioneStampa.RecordErrCaricamentoLievi
            LBL_err_prn.Text = ObjGlobali.GestioneSessioneStampa.RecordErroreStampa
            If ObjGlobali.GestioneSessioneStampa.MessaggioAvviso > "" Then
                If ObjGlobali.GestioneSessioneStampa.CheckDatiVar Then
                    LBL_dati_var.Text = "Si"
                Else
                    LBL_dati_var.Text = "No"
                End If
            Else
                LBL_dati_var.Text = "Non nec."
            End If
            DGV_stat_glob.DataSource = ObjGlobali.GestioneSessioneStampa.ListaStatisticheStampaGlobali
            DGV_stat_parz.DataSource = ObjGlobali.GestioneSessioneStampa.ListaStatisticheStampaParziali
        Else
            LBL_rec_load.Text = "N/A"
            LBL_rec_sel.Text = "N/A"
            LBL_err_car_grav.Text = "N/A"
            LBL_err_car_liev.Text = "N/A"
            LBL_err_prn.Text = "N/A"
            'LBL_can_print.Text = "N/A"
            LBL_dati_var.Text = "N/A"
            'TSM_EsportaFileDiLog.Enabled = False
            'DGV_dati.Columns.Clear()
            DGV_stat_glob.DataSource = Nothing
            DGV_stat_parz.DataSource = Nothing
        End If

    End Sub

    ' Imposta i valori dei campi nella status bar.
    Private Sub SetStatusBar()

        TSSL_versione_software.Text = String.Concat("Versione: ", My.Application.Info.Version.ToString, " - ")
        If ObjGlobali.CFG.LicenseSwitch Then
            TSSL_versione_software.Text = String.Concat(TSSL_versione_software.Text, "L")
        Else
            TSSL_versione_software.Text = String.Concat(TSSL_versione_software.Text, "F")
        End If
        TSSL_stato_prn_pap.Text = "Stampa cartacea: "
        If ObjGlobali.CFG.StampaCartaceaAblitata Then
            TSSL_stato_prn_pap.Text = String.Concat(TSSL_stato_prn_pap.Text, "ABILITATA")
        Else
            TSSL_stato_prn_pap.Text = String.Concat(TSSL_stato_prn_pap.Text, "DISABILITATA")
        End If
        TSSL_versione_pdfcreator.Text = String.Concat("Versione PDFCreator: ", ObjGlobali.CFG.VersionePDFCreator)
        If ObjGlobali.CFG.IsPDFCreatorCorretto Then
            TSSL_versione_pdfcreator.Text = String.Concat(TSSL_versione_pdfcreator.Text, " CORRETTA")
        Else
            TSSL_versione_pdfcreator.Text = String.Concat(TSSL_versione_pdfcreator.Text, " Richiesta versione ", ISC.Invoicer.ManageStampa.MST_costanti.Versione_PDFCreator_Corretta)
        End If
        TSSL_stato_prn_ott.Text = "Stampa ottica: "
        If ObjGlobali.CFG.StampaOtticaAblitata Then
            TSSL_stato_prn_ott.Text = String.Concat(TSSL_stato_prn_ott.Text, "ABILITATA")
        Else
            TSSL_stato_prn_ott.Text = String.Concat(TSSL_stato_prn_ott.Text, "DISABILITATA")
        End If

    End Sub

    Private Sub TSM_gest_topo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_gest_topo.Click
        Dim frm_mng_topo As FRM_mng_toponimi

        frm_mng_topo = FC_forms.BuildForm(New FRM_mng_toponimi)
        frm_mng_topo.ShowDialog()
        FC_forms.DestroyForm(frm_mng_topo)

    End Sub

    Private Sub GestioneCittàToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_gest_citta.Click
        Dim frm_mng_citt As FRM_mng_citta

        frm_mng_citt = FC_forms.BuildForm(New FRM_mng_citta)
        frm_mng_citt.ShowDialog()
        FC_forms.DestroyForm(frm_mng_citt)

    End Sub

    Private Sub AssociaCittàToponimoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_ass_cit_top.Click
        Dim frm_citt_topo As FRM_citta_x_toponimi

        frm_citt_topo = FC_forms.BuildForm(New FRM_citta_x_toponimi)
        frm_citt_topo.ShowDialog()
        FC_forms.DestroyForm(frm_citt_topo)

    End Sub

    Private Sub DefinisciGiroConsegnaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_gest_giro.Click
        Dim frm_giri_cxt As FRM_giro_x_cxt

        frm_giri_cxt = FC_forms.BuildForm(New FRM_giro_x_cxt)
        frm_giri_cxt.ShowDialog()
        FC_forms.DestroyForm(frm_giri_cxt)

    End Sub

    Private Sub TSM_insert_bulk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_insert_bulk.Click
        Dim frm_ins_bulk As FRM_insert_bulk

        frm_ins_bulk = FC_forms.BuildForm(New FRM_insert_bulk)
        frm_ins_bulk.TXB_file_import.Text = ObjGlobali.Folders("WorkFLD")
        frm_ins_bulk.ShowDialog()
        FC_forms.DestroyForm(frm_ins_bulk)

    End Sub

    Private Sub RaggruppamentoGiriConsegnaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RaggruppamentoGiriConsegnaToolStripMenuItem.Click
        Dim frm_giri_gir As FRM_giro_x_gir

        frm_giri_gir = FC_forms.BuildForm(New FRM_giro_x_gir)
        frm_giri_gir.ShowDialog()
        FC_forms.DestroyForm(frm_giri_gir)

    End Sub

    Private Sub BTN_esporta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_esporta.Click
        Dim lPrintRecord As Boolean
        Dim frm_exp As FRM_exporting

        lPrintRecord = False
        If TXB_nomefile_export.Text > "" Then
            lPrintRecord = MessageBox.Show("Proseguo con l'esportazione dei dati?", "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes
        Else
            MessageBox.Show("Attenzione il nome del file non è stato specificato. Non posso proseguire", "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Error)
        End If

        If lPrintRecord Then
            ObjGlobali.GestioneSessioneStampa.GoToRecordNumber(0)
            frm_exp = FC_forms.BuildForm(New FRM_exporting)
            frm_exp.GestioneSessione = ObjGlobali.GestioneSessioneStampa
            frm_exp.GestioneModulo = ObjGlobali.GestioneModuliStampare(1).Modulo
            frm_exp.OnlySelected = CHB_onlyselected.Checked
            frm_exp.ExportFileNameFN = String.Concat(ObjGlobali.GestioneSessioneStampa.Folders("EsportaFLD"), TXB_nomefile_export.Text, ".csv")
            If frm_exp.ShowDialog() = Windows.Forms.DialogResult.OK Then

            End If
        End If

    End Sub

    Private Sub GestioneAzioniPostStampaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSM_manage_azioni_ps.Click
        Dim frm_azioni_ps As FRM_azione_poststampa

        frm_azioni_ps = FC_forms.BuildForm(New FRM_azione_poststampa)
        frm_azioni_ps.ShowDialog()
        FC_forms.DestroyForm(frm_azioni_ps)

    End Sub

    '
    '
    '
    ' Le procedure qui sotto sono tutte nuove
    '
    '
    '

    '
    ' Gestice il caricamento di una sessione e la conseguente preparazione delle maschere
    '
    Private Sub CaricamentoSessione(ByVal cFolder As String)

        '
        ' Prima di caricare la nuova sessione, qualora ne fosse già aperta una procedo con la sua chiusura
        '
        ChiusuraSessione()

        '
        ' Procedo con il caricamento della sessione.
        '
        ObjGlobali.GestioneSessioneStampa.Apertura(cFolder, False)
        If ObjGlobali.GestioneSessioneStampa.IsLoaded Then
            '
            ' Procedo con il caricamento dei dati del quickdataset
            '
            FC_forms.EseguiOperazioneInThread("Caricamento dati della sessione in corso...", New Threading.Thread(AddressOf ObjGlobali.GestioneSessioneStampa.Carica_QuickDataset))

            '
            ' Aggiunge i menu necessari
            '
            FC_menu.BuildMenu_FromPlugIN(MES_principale, TOS_menu, ObjGlobali.GestioneSessioneStampa.ElementiMenuDedicati, AddressOf TSM_SelezioneGenerica_Click)

            '
            ' Procedo con il caricamento dei moduli da gestire
            '
            Caricamento_Moduli()

            '
            ' Procedo con la sistemazione del TabControl Principale
            '
            Imposta_TACPrincipale(1)

            '
            ' Imposta la ToolStripBar principale
            '
            Imposta_TOS_menu()

            '
            ' Procedo con il setting della visibilità dei controlli
            '
            For Each nCodiceModulo As Integer In ObjGlobali.GestioneSessioneStampa.ElencoModuli.Keys
                If ObjGlobali.GestioneSessioneStampa.ElencoModuli(nCodiceModulo).DatiModulo.DaStampare Then
                    Imposta_VisibilitaControlliGestioneModulo(nCodiceModulo, Nothing)
                End If
            Next

            '
            ' Abilita pulsanti per esecuzione delle attività
            '
            AbilitaBottoniAvvioAttivita()

            '
            ' Imposta valori TabPage "Dati sessione stampa"
            '
            SetDatiSessioneStampa()

            '
            ' Abilito il menu di gestione dei moduli
            '
            TSM_gestione_modulo.Enabled = True

            '
            ' Se l'avvio è di con la modalità di sviluppo allora procedo a disbilitare in patenza il fast print.
            '
            If (ObjGlobali.CFG.SwitchPrdSvi = OGC_enumeratori.eSwitchPrdSvi.SPS_sviluppo) Then
                CHB_fast_print.Checked = False
            End If
        ElseIf Not ObjGlobali.GestioneSessioneStampa.IsLoaded Then
            MessageBox.Show("Problemi nel caricamento della sessione.", "Errore caricamento sessione", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ObjGlobali.GestioneSessioneStampa = New GestioneSessione(eTipoApplicazione.TAP_Invoicer, ObjGlobali.Folders, ObjGlobali.CFG.VersioneSistemaOperativo, CFG.OLEDB_ConnString)
        End If
        '
        ' Imposta valori pannello statistiche
        '
        SetQuadroStatistiche()

    End Sub

    '
    ' Gestisce la chiuura della sessione caricata
    '
    Private Sub ChiusuraSessione()

        If ObjGlobali.GestioneSessioneStampa.IsLoaded Then
            '
            ' Memorizzo la sessione tra quelle recenti
            '
            SetSessioniRecenti(ObjGlobali.GestioneSessioneStampa.Nome, ObjGlobali.GestioneSessioneStampa.Folders("SessioneRootFLD"), -1)
            SetMenuSessioniRecenti()

            '
            ' Richea la 
            '
            ObjGlobali.GestioneSessioneStampa = New GestioneSessione(eTipoApplicazione.TAP_Invoicer, ObjGlobali.Folders, ObjGlobali.CFG.VersioneSistemaOperativo, CFG.OLEDB_ConnString)
            FC_menu.PulisciMenu(MES_principale, TOS_menu)

            ' 
            ' pulizia della griglia di visualizzazione dei dati.
            ' 
            DGV_dati.DataSource = Nothing
            DGV_dati.Columns.Clear()

            '
            ' Imposta la visualizzazione dei controlli nella Tool bar
            '
            Imposta_TOS_menu()

            '
            ' Imposta la visualizzazione nulla del quadro descrittivo della sessione
            '
            SetDatiSessioneStampa()

            '
            ' Rimuovi le TabPage del Tabcontrol TAC_Principale
            '
            Imposta_TACPrincipale(-1)

            '
            ' Rimuovi le TabPage dal TabControl TAC_modulistampa
            '
            TAC_modulistampa.TabPages.Clear()

            '
            ' Lista moduli da stampare
            '
            LSB_moduli_stampare.Items.Clear()
            CHB_fusione_moduli.Checked = False

            '
            ' Pulisco i contenitori dei moduli
            '
            ObjGlobali.GestioneModuliStampare.Clear()
            ObjGlobali.GestioneModuliEsportare.Clear()
            ObjGlobali.GestioneProcessoStampa = New CLS_GestioneProcessoStampa

            '
            ' 
            '
            ObjGlobali.GestioneSessioneStampa.ChiudiSessione()

            '
            ' Disabilito il menu di gestione dei moduli
            '
            TSM_gestione_modulo.Enabled = False
        End If

    End Sub

    '
    ' Popola l'elenco dei moduli da stampare
    '
    Private Sub Caricamento_Moduli()

        ObjGlobali.GestioneModuliStampare.Clear()
        ObjGlobali.GestioneProcessoStampa.TipologiaStampa = ObjGlobali.GestioneSessioneStampa.TipologiaStampa
        For Each nCodiceModulo As Integer In ObjGlobali.GestioneSessioneStampa.ElencoModuli.Keys
            ObjGlobali.GestioneSessioneStampa.AttivaModulo(nCodiceModulo)
            ObjGlobali.GestioneModuliStampare.Add(nCodiceModulo, New CLS_GestioneModuloStampare(nCodiceModulo))

            With ObjGlobali.GestioneModuliStampare(nCodiceModulo)
                .NomeModulo = ObjGlobali.GestioneSessioneStampa.ElencoModuli(nCodiceModulo).DatiModulo.Nome
                .Modulo = ObjGlobali.GestioneSessioneStampa.ElencoModuli(nCodiceModulo)
                .Folders.Add("SessioneRootFLD", ObjGlobali.GestioneSessioneStampa.Folders("SessioneRootFLD"))
                .Folders.Add("ModuliFLD", ObjGlobali.GestioneSessioneStampa.Folders("ModuliFLD"))
                .Folders.Add("ArchivioOtticoFLD", ObjGlobali.GestioneSessioneStampa.Folders("ArchivioOtticoFLD"))
                .Folders.Add("RistampeFLD", ObjGlobali.GestioneSessioneStampa.Folders("RistampeFLD"))
            End With
        Next

    End Sub

    '
    ' Popola il TabControl con le TabPage necessarie.
    ' 
    Private Sub Costruisci_TAP_Stampa_modulo_XX()
        Dim m As GestioneModulo
        Dim ctrlD As CheckBox
        Dim tmpTab As TabPage
        Dim frm As FRM_fake

        For Each nCodiceModulo As Integer In ObjGlobali.GestioneSessioneStampa.ElencoModuli.Keys
            m = ObjGlobali.GestioneSessioneStampa.ElencoModuli(nCodiceModulo)
            If m.DatiModulo.DaStampare Then
                frm = New FRM_fake
                tmpTab = frm.TAP_modulo_00
                tmpTab.Text = ObjGlobali.GestioneModuliStampare(nCodiceModulo).NomeModulo
                tmpTab.Name = tmpTab.Name.Replace("_00", String.Concat("_", nCodiceModulo.ToString().PadLeft(2, "0")))
                For Each ctrlA As Control In tmpTab.Controls
                    ctrlA.Name = ctrlA.Name.Replace("_00", String.Concat("_", nCodiceModulo.ToString().PadLeft(2, "0")))
                    If ctrlA.Name.StartsWith("CHB_modulo_da_stampare_") Then
                        AddHandler CType(ctrlA, CheckBox).CheckedChanged, AddressOf CHB_modulo_da_stampare_00_CheckedChanged
                        ctrlD = ctrlA
                    ElseIf ctrlA.Name.StartsWith("CHB_stampa_dettaglio_") Then
                        ctrlA.Visible = m.ConDettaglio
                    ElseIf ctrlA.Name.StartsWith("FLP_moduli_") Then
                        For Each ctrlB As Control In ctrlA.Controls
                            ctrlB.Name = ctrlB.Name.Replace("_00", String.Concat("_", nCodiceModulo.ToString().PadLeft(2, "0")))
                            If ctrlB.Name.StartsWith("GRB_stampante_car_") Then
                                For Each ctrlC As Control In ctrlB.Controls
                                    ctrlC.Name = ctrlC.Name.Replace("_00", String.Concat("_", nCodiceModulo.ToString().PadLeft(2, "0")))
                                    If ctrlC.Name.StartsWith("CMB_stampanti_") Then
                                        AddHandler CType(ctrlC, ComboBox).SelectedIndexChanged, AddressOf CMB_stampanti_00_SelectedIndexChanged
                                    ElseIf ctrlC.Name.StartsWith("RDB_default_") Then
                                        AddHandler CType(ctrlC, RadioButton).CheckedChanged, AddressOf RDB_default_00_CheckedChanged
                                        CType(ctrlC, RadioButton).Checked = True
                                    ElseIf ctrlC.Name.StartsWith("RDB_cassetti_") Then
                                        AddHandler CType(ctrlC, RadioButton).CheckedChanged, AddressOf RDB_default_00_CheckedChanged
                                    ElseIf ctrlC.Name.StartsWith("CMB_cassetto_def_") Then
                                        AddHandler CType(ctrlC, ComboBox).SelectedIndexChanged, AddressOf CMB_cassetto_def_00_SelectedIndexChanged
                                    ElseIf ctrlC.Name.StartsWith("DGV_ass_cassetti_") Then
                                        AddHandler CType(ctrlC, DataGridView).CurrentCellDirtyStateChanged, AddressOf DGV_ass_cassetti_00_CurrentCellDirtyStateChanged
                                    ElseIf ctrlC.Name.StartsWith("CHB_fronte_retro_") Then
                                        CType(ctrlC, CheckBox).Checked = m.DatiModulo.StampaFronteRetro
                                        CType(ctrlC, CheckBox).Enabled = Not m.DatiModulo.DisabilitaFronteRetro
                                        AddHandler CType(ctrlC, CheckBox).CheckedChanged, AddressOf CHB_fronte_retro_00_CheckedChanged
                                    ElseIf ctrlC.Name.StartsWith("CHB_insert_fincatura_") Then
                                        AddHandler CType(ctrlC, CheckBox).CheckedChanged, AddressOf CHB_insert_fincatura_00_CheckedChanged
                                    End If
                                Next
                            ElseIf ctrlB.Name.StartsWith("GRB_stampante_ele_") Then
                                For Each ctrlC As Control In ctrlB.Controls
                                    ctrlC.Name = ctrlC.Name.Replace("_00", String.Concat("_", nCodiceModulo.ToString().PadLeft(2, "0")))
                                    If ctrlC.Name.StartsWith("CMB_file_arch_ott_") Then
                                        AddHandler CType(ctrlC, ComboBox).SelectedIndexChanged, AddressOf CMB_file_arch_ott_00_SelectedIndexChanged
                                    End If
                                Next
                            ElseIf ctrlB.Name.StartsWith("GRB_generazione_") Then
                                For Each ctrlC As Control In ctrlB.Controls
                                    ctrlC.Name = ctrlC.Name.Replace("_00", String.Concat("_", nCodiceModulo.ToString().PadLeft(2, "0")))
                                Next
                            End If
                        Next
                    End If

                Next
                TAC_modulistampa.TabPages.Add(tmpTab)
            End If
        Next

    End Sub

    '
    ' Questa procedura si attiva quando viene aggiunto un tab ai moduli di cui si procederà alla stampa
    '
    Private Sub TAC_modulistampa_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles TAC_modulistampa.ControlAdded
        Dim nCodiceModulo As Integer
        Dim cCtrlNameA As String
        Dim cCtrlNameB As String
        Dim f As FlowLayoutPanel

        nCodiceModulo = RecuperaNumeroModulo(e.Control.Name)
        If nCodiceModulo > 0 Then
            '
            ' Imposta il modulo come non selezionato
            '
            CType(e.Control.Controls(BuildControlName("CHB_modulo_da_stampare_", nCodiceModulo)), CheckBox).Checked = (ObjGlobali.GestioneModuliStampare.Count = 1)
            If Not (ObjGlobali.GestioneModuliStampare.Count = 1) Then
                CHB_modulo_da_stampare_00_CheckedChanged(CType(e.Control.Controls(BuildControlName("CHB_modulo_da_stampare_", nCodiceModulo)), CheckBox), Nothing)
            End If
            f = e.Control.Controls(BuildControlName("FLP_moduli_", nCodiceModulo))

            If ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 1 Then
                cCtrlNameA = BuildControlName("GRB_stampante_car_", nCodiceModulo)
                With CType(f.Controls(cCtrlNameA), GroupBox)
                    '
                    ' Aggiunge la lista stampanti
                    '
                    cCtrlNameB = BuildControlName("CMB_stampanti_", nCodiceModulo)
                    CType(.Controls(cCtrlNameB), ComboBox).DataSource = Trasformazioni.TrasformaInLista(ObjGlobali.CFG.Stampanti)
                    CType(.Controls(cCtrlNameB), ComboBox).ValueMember = "STA_codice"
                    CType(.Controls(cCtrlNameB), ComboBox).DisplayMember = "STA_nome"
                    CType(.Controls(cCtrlNameB), ComboBox).SelectedIndex = Trasformazioni.TrasformaInLista(ObjGlobali.CFG.Stampanti).IndexOf(ObjGlobali.CFG.StampanteCartaceaDefault)
                End With

                cCtrlNameA = BuildControlName("GRB_stampante_ele_", nCodiceModulo)
                With CType(f.Controls(cCtrlNameA), GroupBox)
                    '
                    ' Aggiunge la lista delle 
                    '
                    cCtrlNameB = BuildControlName("CMB_file_arch_ott_", nCodiceModulo)
                    CType(.Controls(cCtrlNameB), ComboBox).DataSource = ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.ListaNomiArchivioOttico
                    CType(.Controls(cCtrlNameB), ComboBox).ValueMember = "AOF_codice"
                    CType(.Controls(cCtrlNameB), ComboBox).DisplayMember = "AOF_descrizione"
                    If CType(.Controls(cCtrlNameB), ComboBox).Items.Count = 2 Then
                        CType(.Controls(cCtrlNameB), ComboBox).SelectedIndex = 1
                    End If

                    ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.NomeStampanteOttica = ObjGlobali.CFG.StampanteOtticaDefault.STA_nome
                    CType(.Controls(BuildControlName("TXB_fat_cor_ele_x_", nCodiceModulo)), TextBox).Text = ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.FattoreCorrezione_Ottico.FCS_coord_x
                    CType(.Controls(BuildControlName("TXB_fat_cor_ele_y_", nCodiceModulo)), TextBox).Text = ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.FattoreCorrezione_Ottico.FCS_coord_y
                End With
            ElseIf ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 2 Then
                cCtrlNameA = BuildControlName("GRB_generazione_", nCodiceModulo)
                With CType(f.Controls(cCtrlNameA), GroupBox)
                    cCtrlNameB = BuildControlName("TXB_Coordinata_X_", nCodiceModulo)
                    CType(.Controls(cCtrlNameB), TextBox).Text = ObjGlobali.GestioneModuliStampare(nCodiceModulo).BarcodeCodImbX

                    cCtrlNameB = BuildControlName("TXB_Coordinata_Y_", nCodiceModulo)
                    CType(.Controls(cCtrlNameB), TextBox).Text = ObjGlobali.GestioneModuliStampare(nCodiceModulo).BarcodeCodImbY
                End With
            End If
        End If

    End Sub

    Public Sub Imposta_VisibilitaControlliGestioneModulo(ByVal nCodiceModulo As Integer, ByVal tmpTab As TabPage)
        Dim cCtrlName As String
        Dim lVisible As Boolean
        Dim f As FlowLayoutPanel

        If tmpTab Is Nothing Then
            f = _PannellogestioneModulo(nCodiceModulo).Controls(BuildControlName("FLP_moduli_", nCodiceModulo))
        Else
            f = tmpTab.Controls(BuildControlName("FLP_moduli_", nCodiceModulo))
        End If

        '
        ' Gestisce la visibilità del pannello per la stampante cartacea
        '
        cCtrlName = BuildControlName("GRB_stampante_car_", nCodiceModulo)
        lVisible = (ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 1) And Not CHB_prn_arch_ott.Checked
        f.Controls(cCtrlName).Visible = lVisible

        '
        ' Gestisce la visibilità del pannello per la stampante elettronica
        '
        cCtrlName = BuildControlName("GRB_stampante_ele_", nCodiceModulo)
        lVisible = (ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 1) And CHB_prn_arch_ott.Checked
        f.Controls(cCtrlName).Visible = lVisible

        '
        ' Gestisce la visibilità del pannello per i documenti pregenerati.
        '
        cCtrlName = BuildControlName("GRB_generazione_", nCodiceModulo)
        lVisible = (ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 2) And False     ' L'immissione dei dati per la generazione del barcode non è più necessaria. Procedo con quella del modulo.
        f.Controls(cCtrlName).Visible = lVisible

    End Sub

    Private Function BuildControlName(ByVal cControlPrefix As String, ByVal nCodiceModulo As Integer) As String

        Return String.Concat(cControlPrefix, nCodiceModulo.ToString().PadLeft(2, "0"))

    End Function

    Private Function RecuperaNumeroModulo(ByVal cValue As String) As Integer
        Dim nTmp As Integer

        nTmp = 0
        If cValue > "" Then
            nTmp = CInt(cValue.Substring(cValue.Length - 2))
        End If
        Return nTmp

    End Function

    Private Property _PannellogestioneModulo(ByVal nCodiceModulo As Integer) As TabPage

        Get
            Dim p As TabPage

            p = CType(Me.Controls("TAC_principale"), TabControl).TabPages("TAP_stampa")
            Return CType(p.Controls("TAC_modulistampa"), TabControl).TabPages(BuildControlName("TAP_modulo_", nCodiceModulo))
        End Get
        Set(ByVal value As TabPage)

        End Set

    End Property

    Private Sub NUD_numero_moduli_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NUD_documenti_gruppo.ValueChanged

        If NUD_documenti_gruppo.Value < 100000000 Then
            ObjGlobali.GestioneProcessoStampa.NumeroDocumenti = NUD_documenti_gruppo.Value
            For Each p As KeyValuePair(Of Integer, CLS_GestioneModuloStampare) In ObjGlobali.GestioneModuliStampare
                CType(p.Value, CLS_GestioneModuloStampare).DatiPerStampa.DocumentiDaStampare = NUD_documenti_gruppo.Value
            Next
        End If

    End Sub

    Private Sub LSB_moduli_stampare_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles LSB_moduli_stampare.DoubleClick
        Dim nCodiceModulo As Integer
        Dim tmpTab As TabPage

        nCodiceModulo = LSB_moduli_stampare.IndexFromPoint(CType(e, MouseEventArgs).Location)
        If nCodiceModulo <> System.Windows.Forms.ListBox.NoMatches Then
            nCodiceModulo = LSB_moduli_stampare.SelectedItem.ToString.Substring(0, 2)
            tmpTab = TAC_modulistampa.TabPages(BuildControlName("TAP_modulo_", nCodiceModulo))
            CType(tmpTab.Controls(BuildControlName("CHB_modulo_da_stampare_", nCodiceModulo)), CheckBox).Checked = False
        End If

    End Sub

    Private Sub LSB_moduli_stampare_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles LSB_moduli_stampare.DrawItem
        Dim clrSelectedText As Color = Color.Red 'Our color for selected text 
        Dim clrHighlight As Color = Color.Yellow    'Our background for selected items

        If e.State = DrawItemState.Selected Then
            'This item is selected
            'e.DrawBackground()
            e.Graphics.FillRectangle(New SolidBrush(clrHighlight), e.Bounds)    'Fill the item's rectangle with our highlight
            e.Graphics.DrawString(LSB_moduli_stampare.Items.Item(e.Index), e.Font, New SolidBrush(clrSelectedText), e.Bounds)  'Draw the text for the item
        ElseIf e.State = DrawItemState.None Then
            'This item has no state
            e.DrawBackground()  'Draw our regular background
            e.Graphics.DrawString(LSB_moduli_stampare.Items.Item(e.Index), e.Font, Brushes.Black, e.Bounds)    'Draw the item text in its regular color
        End If
        'Draws a focus rectangle around the item if it has focus
        e.DrawFocusRectangle()

    End Sub

    Private Sub LSB_moduli_stampare_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LSB_moduli_stampare.SelectedIndexChanged
        Dim nCodiceModulo As Integer
        Dim tmpTab As TabPage

        If LSB_moduli_stampare.SelectedIndex > -1 Then
            nCodiceModulo = LSB_moduli_stampare.SelectedItem.ToString.Substring(0, 2)
            tmpTab = TAC_modulistampa.TabPages(BuildControlName("TAP_modulo_", nCodiceModulo))
            TAC_modulistampa.SelectedTab = tmpTab
        End If

    End Sub

    Private Sub TAC_modulistampa_Selected(ByVal sender As Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles TAC_modulistampa.Selected
        Dim nCodiceModulo As Integer
        Dim cModuloName As String
        Dim cCtrlNameA As String
        Dim cTmp As String

        If e.TabPage IsNot Nothing Then
            cTmp = e.TabPage.Name
            nCodiceModulo = cTmp.Substring(cTmp.Length - 2)

            cModuloName = String.Concat(nCodiceModulo.ToString().PadLeft(2, "0"), ") ", GestioneModuliStampare(nCodiceModulo).NomeModulo)
            cCtrlNameA = BuildControlName("CHB_modulo_da_stampare_", nCodiceModulo)
            If CType(e.TabPage.Controls(cCtrlNameA), CheckBox).Checked Then
                LSB_moduli_stampare.SelectedIndex = LSB_moduli_stampare.Items.IndexOf(cModuloName)
            End If
        End If

    End Sub

#Region "Procedure legate alla selezione dei dati per la stampa"

    '
    ' Aggiunge o toglie il modulo da quelli da stampare
    '
    Private Sub CHB_modulo_da_stampare_00_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim nCodiceModulo As Integer
        Dim cModuloName As String
        Dim cTmp As String
        Dim f As FlowLayoutPanel

        With CType(sender, CheckBox)
            cTmp = .Name
            nCodiceModulo = cTmp.Substring(cTmp.Length - 2)

            '
            ' Imposto nella lista dei moduli gestiti se il modulo è da stampare o no.
            '
            ObjGlobali.GestioneModuliStampare(nCodiceModulo).DaStampare = .Checked

            '
            ' Imposto la visualizzazione dei dati nella maschera di gestione del modulo.
            '
            cModuloName = String.Concat(nCodiceModulo.ToString().PadLeft(2, "0"), ") ", GestioneModuliStampare(nCodiceModulo).NomeModulo)
            If .Checked Then
                LSB_moduli_stampare.Items.Add(cModuloName)
                LSB_moduli_stampare.SelectedIndex = LSB_moduli_stampare.Items.IndexOf(cModuloName)
                ObjGlobali.GestioneProcessoStampa.CodiciModuliDaStampare.Add(nCodiceModulo)
            Else
                If LSB_moduli_stampare.Items.IndexOf(cModuloName) > -1 Then
                    LSB_moduli_stampare.Items.RemoveAt(LSB_moduli_stampare.Items.IndexOf(cModuloName))
                    ObjGlobali.GestioneProcessoStampa.CodiciModuliDaStampare.Remove(nCodiceModulo)
                End If
            End If

            '
            ' Visualizza descrizione stato modulo
            '
            ObjGlobali.GestioneModuliStampare(nCodiceModulo).VerificaProcedoConStampa(CHB_prn_arch_ott.Checked)
            SetUpModuloStampabile(nCodiceModulo)

            '
            ' Abilita o disbilita il gruppo 
            '
            f = .Parent.Controls(BuildControlName("FLP_moduli_", nCodiceModulo))
            CType(f.Controls(BuildControlName("GRB_stampante_car_", nCodiceModulo)), GroupBox).Enabled = .Checked
            CType(f.Controls(BuildControlName("GRB_stampante_ele_", nCodiceModulo)), GroupBox).Enabled = .Checked
            CType(f.Controls(BuildControlName("GRB_generazione_", nCodiceModulo)), GroupBox).Enabled = .Checked

            '
            ' Abilita pulsante di stampa
            '
            AbilitaBottoniAvvioAttivita()
        End With

    End Sub

    '
    ' Imposto la parte relativa hai cassetti di stampa se una stampante va- ' 
    ' lida è stata selezionata.                                             '
    '
    Private Sub CMB_stampanti_00_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim nCodiceModulo As Integer
        Dim ctrlA As ComboBox
        Dim ctrlB As TextBox
        Dim ctrlD As Control
        Dim ctrlE As Control
        Dim cTmp As String

        ctrlA = CType(sender, ComboBox)
        nCodiceModulo = RecuperaNumeroModulo(ctrlA.Name)
        If (ctrlA.SelectedIndex > 0) Then
            With ObjGlobali.GestioneModuliStampare(nCodiceModulo)
                .NomeStampanteCartacea = CType(ctrlA.SelectedItem, StampantiEntity).STA_nome
                .NomeStampanteOttica = ObjGlobali.CFG.StampanteOtticaDefault.STA_nome
                FC_forms.EseguiOperazioneInThread("Acquisizione dati dalla stampante...", New Threading.Thread(AddressOf .GetDatiStampante))

                ObjGlobali.Logger.ScriviMessaggioLog(eLogLevel.LL_debugging, 0, "", "CMB_stampanti_00_SelectedIndexChanged", String.Concat("INIZIO caricamento lista cassetti: ", .NomeModulo))
                For Each k As KeyValuePair(Of Integer, CassettoEntity) In .CassettiStampante
                    If (k.Key > 0) Then
                        ObjGlobali.Logger.ScriviMessaggioLog(eLogLevel.LL_debugging, 1, "", "CMB_stampanti_00_SelectedIndexChanged", String.Concat("Cassetto: ", k.Value.PRT_sourcename, " ", k.Value.PRT_kind, " ", k.Value.PRT_rawkind))
                    End If
                Next
                ObjGlobali.Logger.ScriviMessaggioLog(eLogLevel.LL_debugging, 0, "", "CMB_stampanti_00_SelectedIndexChanged", String.Concat("FINE caricamento lista cassetti: ", .NomeModulo))
                .FronteRetro = True

                '
                ' Imposta il controllo per il cassetto di default
                '
                With CType(ctrlA.Parent.Controls(BuildControlName("CMB_cassetto_def_", nCodiceModulo)), ComboBox)
                    .DataSource = ObjGlobali.GestioneModuliStampare(nCodiceModulo).ListaCassetti
                    .ValueMember = "PRT_codice"
                    .DisplayMember = "PRT_sourcename"
                    .SelectedIndex = ObjGlobali.GestioneModuliStampare(nCodiceModulo).ListaCassetti.IndexOf(ObjGlobali.GestioneModuliStampare(nCodiceModulo).CassettoDefault)
                End With

                ctrlD = ctrlA.Parent.Controls(BuildControlName("RDB_cassetti_", nCodiceModulo))
                ctrlD.Enabled = (.CassettiStampante.Count > 2) And ObjGlobali.CFG.LicenseSwitch

                ctrlE = ctrlA.Parent.Controls(BuildControlName("DGV_ass_cassetti_", nCodiceModulo))
                ctrlE.Enabled = CType(ctrlD, RadioButton).Checked
                CType(ctrlE, DataGridView).AutoGenerateColumns = False
                With CType(CType(ctrlE, DataGridView).Columns(1), System.Windows.Forms.DataGridViewComboBoxColumn)
                    .DataSource = ObjGlobali.GestioneModuliStampare(nCodiceModulo).ListaCassetti
                    .DisplayMember = "PRT_sourcename"
                    .ValueMember = "PRT_codice"
                End With
                CType(ctrlE, DataGridView).DataSource = ObjGlobali.GestioneModuliStampare(nCodiceModulo).ListaCassettiModuli

                '
                ' Impostazione dei valori del fattore di correzione
                '
                cTmp = BuildControlName("TXB_fat_cor_car_x_", nCodiceModulo)
                ctrlB = ctrlA.Parent.Controls(cTmp)
                ctrlB.Text = .FattoreCorrezione_Cartaceo.FCS_coord_x
                cTmp = BuildControlName("TXB_fat_cor_car_y_", nCodiceModulo)
                ctrlB = ctrlA.Parent.Controls(cTmp)
                ctrlB.Text = .FattoreCorrezione_Cartaceo.FCS_coord_y
            End With
        End If

    End Sub

    '
    ' Procedura per la gestione della selezione del cassetto.               
    ' 
    Private Sub RDB_default_00_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim nCodiceModulo As Integer
        Dim g As GroupBox

        nCodiceModulo = RecuperaNumeroModulo(sender.name)
        g = CType(sender, RadioButton).Parent
        If (CType(sender, RadioButton).Name.Contains("RDB_default") And CType(sender, RadioButton).Checked) Then
            CType(g.Controls(BuildControlName("CMB_cassetto_def_", nCodiceModulo)), ComboBox).Enabled = CType(sender, RadioButton).Checked
            CType(g.Controls(BuildControlName("DGV_ass_cassetti_", nCodiceModulo)), DataGridView).Enabled = False
        ElseIf (CType(sender, RadioButton).Name.Contains("RDB_cassetti") And CType(sender, RadioButton).Checked) Then
            CType(g.Controls(BuildControlName("CMB_cassetto_def_", nCodiceModulo)), ComboBox).Enabled = False
            CType(g.Controls(BuildControlName("DGV_ass_cassetti_", nCodiceModulo)), DataGridView).Enabled = CType(sender, RadioButton).Checked
        End If

    End Sub

    Private Sub DGV_ass_cassetti_00_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim nCodiceModulo As Integer

        nCodiceModulo = RecuperaNumeroModulo(sender.name)
        CType(sender, DataGridView).CommitEdit(DataGridViewDataErrorContexts.Commit)
        CType(sender, DataGridView).EndEdit()
        ObjGlobali.GestioneModuliStampare(nCodiceModulo).Imposta_cassettiUtilizzati(False, Nothing)
        ObjGlobali.GestioneModuliStampare(nCodiceModulo).VerificaProcedoConStampa(CHB_prn_arch_ott.Checked)
        SetUpModuloStampabile(nCodiceModulo)

    End Sub

    '
    ' Imposto la necessità di inserire la fincatura in caso di sola stampa cartacea. In caso di stampa elettronica 
    ' la fincatura va comunqe inserita.
    '
    Private Sub CHB_insert_fincatura_00_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim nCodiceModulo As Integer
        Dim cTmp As String

        With CType(sender, CheckBox)
            cTmp = .Name
            nCodiceModulo = cTmp.Substring(cTmp.Length - 2)

            '
            ' Imposto nella lista dei moduli gestiti se il modulo è da stampare o no.
            '
            ObjGlobali.GestioneModuliStampare(nCodiceModulo).InserisciFincatura = .Checked
        End With

    End Sub

    Private Sub CHB_fronte_retro_00_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim nCodiceModulo As Integer

        nCodiceModulo = RecuperaNumeroModulo(sender.name)
        ObjGlobali.GestioneModuliStampare(nCodiceModulo).FronteRetro = CType(sender, CheckBox).Checked

    End Sub

#End Region

    Private Sub SetDatiSessioneStampa()

        If ObjGlobali.GestioneSessioneStampa.IsLoaded Then
            LBL_sessione_nome.Text = ObjGlobali.GestioneSessioneStampa.Nome
            LBL_sessione_cartella.Text = ObjGlobali.GestioneSessioneStampa.Folders(FLD_sessioneroot)
            LBL_sessione_Descrizione.Text = ObjGlobali.GestioneSessioneStampa.Descrizione
            LBL_plugin_name.Text = ObjGlobali.GestioneSessioneStampa.PlugInName
            LBL_plugin_versione.Text = ObjGlobali.GestioneSessioneStampa.PlugInVersion
            LBL_plugin_filename.Text = ObjGlobali.GestioneSessioneStampa.Files(FIL_plugin)
        Else
            LBL_sessione_nome.Text = "N/A"
            LBL_sessione_cartella.Text = "N/A"
            LBL_sessione_Descrizione.Text = "N/A"
            LBL_plugin_name.Text = "N/A"
            LBL_plugin_versione.Text = "N/A"
            LBL_plugin_filename.Text = "N/A"
        End If

    End Sub

#Region "Codice di gestione del TABControl TAC_principale"

    '
    ' Gestisce le procedure per l'aggiunta al controllo TAC_principale
    '
    Private Sub TAC_principale_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles TAC_principale.ControlAdded

        If e.Control.Name.ToLower = "tap_dati" Then
            Setup_TAP_dati()
        ElseIf e.Control.Name.ToLower = "tap_stampa" Then
            Setup_TAP_stampa()
        End If

    End Sub

    '
    ' Imposta la TAB_page dei dati da stampare con la griglia dei dati correttamente settata.
    '
    Private Sub Setup_TAP_dati()
        Dim dgvc As System.Windows.Forms.DataGridViewColumn
        Dim nColumn As Integer
        Dim nWidth As Integer
        Dim nGap As Integer

        ObjGlobali.GestioneSessioneStampa.ImpostaDGVDati(DGV_dati)

        ' *****
        ' Ridimensiona DGV_dati
        ' *****
        nWidth = 0
        For Each dgvc In DGV_dati.Columns
            nWidth += dgvc.Width
        Next
        nColumn = 0
        While nWidth < DGV_dati.Width
            dgvc = DGV_dati.Columns(nColumn)
            If Not dgvc.DataPropertyName.StartsWith("DEF_") Then
                dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                nGap = Math.Min(100, DGV_dati.Width - nWidth)
                dgvc.Width += nGap
                nWidth += nGap
            End If
            nColumn += 1
            If nColumn = DGV_dati.Columns.Count Then
                nColumn = 0
            End If
        End While
        DGV_dati.DataSource = New DataView(ObjGlobali.GestioneSessioneStampa.QuickDataTable)
        DGV_dati.MultiSelect = ObjGlobali.CFG.LicenseSwitch

    End Sub

    '
    ' Imposta la TabPage della stampa dei moduli.
    '
    Private Sub Setup_TAP_stampa()

        '
        ' Procedo con la costruzione della TabPage relativa al modulo
        '
        Costruisci_TAP_Stampa_modulo_XX()

        '
        ' Procedo con la popolazione dei controlli con i valori necessari
        '
        SetVisibilitaParamStampa()

        '
        ' Visualizza controlli per stampa documenti pregenerati
        '
        CHB_prn_arch_ott.Visible = ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 1
        CHB_eseg_poststampa.Visible = ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 1
        CHB_force_paper.Visible = ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 1

        '
        ' Visualizza controlli per stampa documenti pregenerati
        '
        CHB_clean_FLD_elab.Visible = ObjGlobali.GestioneProcessoStampa.TipologiaStampa = 2

    End Sub

#End Region

#Region "Codici della TabPage TAP_dati"

    Private Sub SetBoxMsgErrore()
        Dim dr As DataRow
        Dim cError As String

        dr = CType(DGV_dati.SelectedRows(0).DataBoundItem, DataRowView).Row
        If dr("DEF_selectable") Then
            LBL_rec_selz.Text = "Selezionabile"
        Else
            LBL_rec_selz.Text = "Non selezionabile"
        End If

        If dr("DEF_toprint") Then
            LBL_stato_rec.Text = "Selezionato"
        Else
            LBL_stato_rec.Text = "Non selezionato"
        End If

        LBL_err_desc.Text = dr("DEF_errdesc")
        Select Case dr("DEF_errcode")
            Case Is = 0
                cError = "Nessun Errore."
                LBL_err_desc.Text = "N/D"
            Case Is = -1, -2
                cError = "Errore in fase di caricamento dei dati."
            Case Is = -201, -202, -203
                cError = "Errore in fase di gestione del file per l'achiviazione ottica"
            Case Is = -301
                cError = "Errore in fase di stampa dei dati."
            Case Else
                cError = "Errore sconosciuto."
        End Select
        LBL_err_fase.Text = cError

    End Sub

#End Region

#Region "Codici legati al TAB_stampa"

    Sub AbilitaBottoniAvvioAttivita()
        Dim lModuliOk As Boolean

        '
        ' Verifico che i moduli selezionabili siano stampabili
        '
        lModuliOk = ObjGlobali.GestioneProcessoStampa.NumeroModuliDaStampare > 0
        For Each nCodiceModulo As Integer In ObjGlobali.GestioneModuliStampare.Keys
            If ObjGlobali.GestioneModuliStampare(nCodiceModulo).DaStampare Then
                lModuliOk = lModuliOk And ObjGlobali.GestioneModuliStampare(nCodiceModulo).ProcedoConStampa
            End If
        Next
        BTN_stampa.Enabled = (ObjGlobali.GestioneSessioneStampa.RecordSelezionati > 0) And lModuliOk

    End Sub

    ' ********************************************************************* '
    ' Procedura per la gestione della stampa dei documenti ottici.          '
    ' ********************************************************************* '
    Private Sub CHB_prn_arch_ott_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CHB_prn_arch_ott.CheckedChanged

        With CType(sender, CheckBox)
            If .Checked Then
                NUD_documenti_gruppo.Value = 1
            Else
                NUD_documenti_gruppo.Value = ObjGlobali.CFG.DocumentiContemporanei
            End If
            For Each nCodiceModulo As Integer In ObjGlobali.GestioneModuliStampare.Keys
                If ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.DatiModulo.DaStampare Then
                    Imposta_VisibilitaControlliGestioneModulo(nCodiceModulo, Nothing)
                    ObjGlobali.GestioneModuliStampare(nCodiceModulo).VerificaProcedoConStampa(.Checked)
                    SetUpModuloStampabile(nCodiceModulo)
                End If
            Next
            SetVisibilitaParamStampa()
        End With

    End Sub

    Private Sub CMB_file_arch_ott_00_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim nCodiceModulo As Integer

        nCodiceModulo = RecuperaNumeroModulo(sender.name)
        With _PannellogestioneModulo(nCodiceModulo).Controls(BuildControlName("FLP_moduli_", nCodiceModulo)).Controls(BuildControlName("GRB_stampante_ele_", nCodiceModulo))
            If CType(sender, ComboBox).SelectedIndex = 0 Then
                ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.NomeFileArchivioOtticoSelect = Nothing
            Else
                ObjGlobali.GestioneModuliStampare(nCodiceModulo).Modulo.NomeFileArchivioOtticoSelect = CType(CType(.Controls(BuildControlName("CMB_file_arch_ott_", nCodiceModulo)), ComboBox).SelectedItem, NomeArchiviazioneOtticaEntity)
            End If
        End With
        ObjGlobali.GestioneModuliStampare(nCodiceModulo).VerificaProcedoConStampa(CHB_prn_arch_ott.Checked)
        SetUpModuloStampabile(nCodiceModulo)

    End Sub

#End Region

    Private Sub SetUpModuloStampabile(ByVal nCodiceModulo As Integer)

        With _PannellogestioneModulo(nCodiceModulo).Controls(BuildControlName("LBL_stato_modulo_", nCodiceModulo))
            .Text = ObjGlobali.GestioneModuliStampare(nCodiceModulo).DescrizionePossoProcedere
            If ObjGlobali.GestioneModuliStampare(nCodiceModulo).ProcedoConStampa Then
                .ForeColor = Color.Green
            Else
                .ForeColor = Color.Red
            End If
        End With
        AbilitaBottoniAvvioAttivita()

    End Sub

#Region "Gestione Della ToolBar"

    Private Sub Imposta_TOS_menu()

        If ObjGlobali.GestioneSessioneStampa Is Nothing Then
            TSB_refresh.Visible = False
            TSB_open_arcott_fld.Visible = False
            TSB_open_documenti_unificati_fld.Visible = False
            TSB_open_elaborati_fld.Visible = False
            TSB_export_fld.Visible = False
        Else
            If ObjGlobali.GestioneSessioneStampa.IsLoaded Then
                TSB_refresh.Visible = (ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 1) And (ObjGlobali.GestioneModuliStampare.Count > 0)
                TSB_open_arcott_fld.Visible = (ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 1) And (ObjGlobali.GestioneModuliStampare.Count > 0)
                TSB_open_documenti_unificati_fld.Visible = (ObjGlobali.GestioneSessioneStampa.ModuloUnificato IsNot Nothing)
                TSB_open_elaborati_fld.Visible = ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 2
                TSB_export_fld.Visible = (ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 1) And (ObjGlobali.GestioneModuliEsportare.Count > 0)
            Else
                TSB_refresh.Visible = False
                TSB_open_arcott_fld.Visible = False
                TSB_open_documenti_unificati_fld.Visible = False
                TSB_open_elaborati_fld.Visible = False
                TSB_export_fld.Visible = False
            End If
        End If
        ToolStripSeparator4.Visible = (TSB_open_arcott_fld.Visible Or TSB_open_elaborati_fld.Visible)
        ToolStripSeparator2.Visible = TSB_export_fld.Visible

    End Sub

    Private Sub TSB_refresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_refresh.Click

        If FC_forms.EseguiOperazioneInThread("Refresh dati della sessione in corso...", New Threading.Thread(AddressOf ObjGlobali.GestioneSessioneStampa.Carica_QuickDataset)) Then
            '     ManageTACPrincipale(1)
        End If

    End Sub

    Private Sub TSB_open_arcott_fld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_open_arcott_fld.Click

        ApriFolderExplorer(ObjGlobali.GestioneSessioneStampa.Folders("ArchivioOtticoFLD"))

    End Sub

    Private Sub TSB_open_documenti_unificati_fld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_open_documenti_unificati_fld.Click

        ApriFolderExplorer(ObjGlobali.GestioneSessioneStampa.Folders("DocumentiUnificatiFLD"))

    End Sub

    Private Sub TSB_open_elaborati_fld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_open_elaborati_fld.Click

        ApriFolderExplorer(ObjGlobali.GestioneSessioneStampa.Folders("ElaboratiFLD"))

    End Sub

    Private Sub TSB_export_fld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_export_fld.Click

        ApriFolderExplorer(ObjGlobali.GestioneSessioneStampa.Folders("EsportaFLD"))

    End Sub

#End Region

#Region "Gestione della StatusStrip STS_bottom"

    Private Sub TSSL_prnwrk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSSL_prnwrk.Click

        ApriFolderExplorer(ObjGlobali.Folders("PrnWrkFLD"))

    End Sub

#End Region

#Region "Gestione delle Sessioni Recenti"

    '
    ' Recupero delle sessioni recenti e passaggio alla procedura per l'inserimento nel menu.
    '
    Private Function GetSessioniRecenti() As List(Of SessioniRecentiEntity)
        Dim l As List(Of SessioniRecentiEntity)
        Dim xnSessioniRecenti As Xml.XmlNode
        Dim xnSessione As Xml.XmlNode
        Dim cSessioneNome As String

        l = New List(Of SessioniRecentiEntity)
        xnSessioniRecenti = ObjGlobali.CFG.FileCFGXml.SelectSingleNode("configurazione/sessioni_recenti")
        If NodeNull(xnSessioniRecenti) Then
            xnSessioniRecenti = ObjGlobali.CFG.FileCFGXml.CreateNode(Xml.XmlNodeType.Element, "sessioni_recenti", "")
            ObjGlobali.CFG.FileCFGXml.SelectSingleNode("configurazione").AppendChild(xnSessioniRecenti)
        End If
        For Each xnSessione In ObjGlobali.CFG.FileCFGXml.SelectSingleNode("configurazione/sessioni_recenti")
            If xnSessione.Attributes("nome") Is Nothing Then
                cSessioneNome = xnSessione.InnerText
            Else
                cSessioneNome = xnSessione.Attributes("nome").Value
            End If
            l.Add(New SessioniRecentiEntity(l.Count + 1, cSessioneNome, xnSessione.InnerText))
        Next
        Return l

    End Function

    '
    ' Gestisce la parte di configurazione relativa al salvataggio delle sessioni recenti
    '
    Sub SetSessioniRecenti(ByVal cSessioneNome As String, ByVal cSessioneFile As String, ByVal nValue2Remove As Integer)
        Dim xnSessioniRecenti As Xml.XmlNode
        Dim xaSessNome As Xml.XmlAttribute
        Dim xnSessione As Xml.XmlNode

        If (cSessioneFile > "") And ObjGlobali.CFG.LicenseSwitch Then
            xnSessioniRecenti = ObjGlobali.CFG.FileCFGXml.SelectSingleNode("configurazione/sessioni_recenti")
            If NodeNull(xnSessioniRecenti) Then
                xnSessioniRecenti = ObjGlobali.CFG.FileCFGXml.CreateNode(Xml.XmlNodeType.Element, "sessioni_recenti", "")
                ObjGlobali.CFG.FileCFGXml.SelectSingleNode("configurazione").AppendChild(xnSessioniRecenti)
            End If

            xnSessione = xnSessioniRecenti.SelectSingleNode(String.Concat("sessione_row[.='", cSessioneFile, "']"))
            If NodeNotNull(xnSessione) Then
                xaSessNome = ObjGlobali.CFG.FileCFGXml.CreateAttribute("nome")
                xaSessNome.Value = cSessioneNome
                xnSessione.Attributes.Append(xaSessNome)

                '
                ' Se la sessione è già presente allora provvedo a rimuoverla e spostarla in testa.
                '
                xnSessioniRecenti.RemoveChild(xnSessione)
            Else
                xnSessione = ObjGlobali.CFG.FileCFGXml.CreateNode(Xml.XmlNodeType.Element, "sessione_row", "")
                xnSessione.InnerText = cSessioneFile

                xaSessNome = ObjGlobali.CFG.FileCFGXml.CreateAttribute("nome")
                xaSessNome.Value = cSessioneNome
                xnSessione.Attributes.Append(xaSessNome)
            End If
            xnSessioniRecenti.InsertBefore(xnSessione, xnSessioniRecenti.ChildNodes(0))
            If xnSessioniRecenti.ChildNodes.Count > 10 Then
                xnSessione = xnSessioniRecenti.LastChild
                xnSessioniRecenti.RemoveChild(xnSessione)
            End If
            ObjGlobali.CFG.SalvaFileConfigurazione()
        End If
        If nValue2Remove > -1 Then
            xnSessioniRecenti = ObjGlobali.CFG.FileCFGXml.SelectSingleNode("configurazione/sessioni_recenti")
            xnSessione = xnSessioniRecenti.ChildNodes(nValue2Remove - 1)
            xnSessioniRecenti.RemoveChild(xnSessione)
            ObjGlobali.CFG.SalvaFileConfigurazione()
        End If

    End Sub

    '
    ' Pulizia delle sessioni recenti.
    '
    Private Sub ClearSessioniRecenti()
        Dim xnWork1 As Xml.XmlNode
        Dim xnWork2 As Xml.XmlNode

        xnWork1 = ObjGlobali.CFG.FileCFGXml.SelectSingleNode("configurazione/sessioni_recenti")
        If NodeNull(xnWork1) Then
            xnWork1 = ObjGlobali.CFG.FileCFGXml.CreateNode(Xml.XmlNodeType.Element, "sessioni_recenti", "")
            ObjGlobali.CFG.FileCFGXml.SelectSingleNode("configurazione").AppendChild(xnWork1)
        End If

        xnWork2 = xnWork1.FirstChild
        While NodeNotNull(xnWork2)
            xnWork1.RemoveChild(xnWork2)
            xnWork2 = xnWork1.FirstChild
        End While
        ObjGlobali.CFG.SalvaFileConfigurazione()

    End Sub

#End Region

#Region "Procedure comuni"

    Private Sub ApriFolderExplorer(ByVal cFolder As String)

        Shell(String.Concat("explorer ", cFolder), AppWinStyle.NormalFocus)

    End Sub

#End Region

    Private Sub TMR_sched_op_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMR_sched_op.Tick
        Dim aFileList As System.IO.FileInfo()
        Dim nPrnWorkFLDSize As Long
        Dim lFileLocked As Boolean
        Dim i As Integer
        Dim c As Process

        '
        ' Questa operazione manuetiene:
        ' - la dimensione della cartella temporanea <rootapplication>\PrnWrkFLD se supera la dimensione massima consentita
        ' - la dimensione della memoria utilizzata collezionando la garbage collection e pulendola.
        '
        nPrnWorkFLDSize = 0
        aFileList = New System.IO.DirectoryInfo(ObjGlobali.Folders("PrnWrkFLD")).GetFiles
        i = aFileList.GetUpperBound(0)
        While Not (i = -1)
            nPrnWorkFLDSize += aFileList(i).Length
            If (nPrnWorkFLDSize > ObjGlobali.CFG.PrnWrkFLDSize) Or (CDate(aFileList(i).LastWriteTime) < Now.AddMinutes(-10)) Then
                Try
                    System.IO.File.Delete(aFileList(i).FullName)
                    nPrnWorkFLDSize -= aFileList(i).Length
                Catch ex As Exception
                    lFileLocked = True
                End Try
            End If
            i -= 1
        End While

        TSSL_prnwrk.Text = String.Concat("Cartella temporanea stampe: ", CDec(nPrnWorkFLDSize / 1024).ToString("#,##0"), " KB")
        If lFileLocked Then
            TSSL_prnwrk.Text = String.Concat(TSSL_prnwrk.Text, " (canc. sospesa, file locked)")
        End If

        If Now.Second <= 10 Then
            GC.Collect()
        End If
        c = Process.GetCurrentProcess()
        TSSL_memorysize.Text = String.Concat("Dati applicazione (GC Time - Attuale/Picco - Thread): ", Now.Second.ToString("00"), " - ", CDec(c.WorkingSet64 / 1024).ToString("#,##0.000"), "/", CDec(c.PeakWorkingSet64 / 1024).ToString("#,##0.000"), " KB - ", c.Threads.Count.ToString("#,##0"))
        c.Dispose()
        c = Nothing

    End Sub

    Private Sub CMB_cassetto_def_00_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim nCodiceModulo As Integer

        nCodiceModulo = RecuperaNumeroModulo(sender.name)
        ObjGlobali.GestioneModuliStampare(nCodiceModulo).Imposta_cassettiUtilizzati(True, CType(sender, ComboBox).SelectedItem)
        ObjGlobali.GestioneModuliStampare(nCodiceModulo).VerificaProcedoConStampa(CHB_prn_arch_ott.Checked)
        SetUpModuloStampabile(nCodiceModulo)

    End Sub

    Private Sub SetVisibilitaParamStampa()
        Dim lVisible As Boolean

        lVisible = ObjGlobali.CFG.LicenseSwitch And ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 1 And CHB_prn_arch_ott.Checked
        CHB_fusione_moduli.Visible = lVisible
        If lVisible Then
            CHB_fusione_moduli.Enabled = GestioneSessioneStampa.ModuloUnificato IsNot Nothing
        End If

        lVisible = ObjGlobali.CFG.LicenseSwitch And ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 1 And Not CHB_prn_arch_ott.Checked
        PNL_documenti_cont.Visible = lVisible
        If lVisible Or ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 2 Then
            NUD_documenti_gruppo.Value = ObjGlobali.CFG.DocumentiContemporanei
        End If

        lVisible = ObjGlobali.CFG.LicenseSwitch And ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 1 And CHB_prn_arch_ott.Checked
        CHB_eseg_poststampa.Visible = lVisible

        lVisible = ObjGlobali.CFG.LicenseSwitch And ObjGlobali.GestioneSessioneStampa.TipologiaStampa = 1 And Not CHB_prn_arch_ott.Checked
        CHB_force_paper.Visible = lVisible

    End Sub

End Class