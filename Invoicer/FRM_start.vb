Public Class FRM_start

    Private nControllo As Integer

    Private Sub FRM_start_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If My.Application.Info.Title <> "" Then
            LBL_app_name.Text = My.Application.Info.Title
        Else
            '
            ' If the application title is missing, use the application name, without the extension
            '
            LBL_app_name.Text = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If
        LBL_version.Text = String.Concat(LBL_version.Text, My.Application.Info.Version.Major, ".", My.Application.Info.Version.Minor, ".", My.Application.Info.Version.Build)
        nControllo = 2
        TMR_controlli.Enabled = True

    End Sub

    Private Sub TMR_controlli_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMR_controlli.Tick

        If nControllo = 1 Then
            'LBL_controllo_runn.Text = "Verifica versione sistema operativo"
            'Me.Refresh()
            'ObjGlobali.CFG.ControllaVersioneSistemaOperativo()
        ElseIf nControllo = 2 Then
            LBL_controllo_runn.Text = "Verifica esistenza file applicazione"
            Me.Refresh()
            ObjGlobali.Logger.ScriviMessaggioLog(ISC.LibrerieComuni.OggettiComuni.LogSystem.eLogLevel.LL_debugging, 0, "FRM_start", "TMR_controlli", "Verifica esistenza file applicazione")
            ObjGlobali.CFG.ControllaFileApplicazione()
        ElseIf nControllo = 3 Then
            'LBL_controllo_runn.Text = "Presenza cartella appoggio stampe temporanee"
            'Me.Refresh()
            'If Not System.IO.Directory.Exists(ObjGlobali.CFG.Folders("PrnWrkFLD")) Then
            '    System.IO.Directory.CreateDirectory(_cfg.PrnWrkFLD)
            'End If
        ElseIf nControllo = 4 Then
            LBL_controllo_runn.Text = "Verifica stampanti configurate"
            Me.Refresh()
            ObjGlobali.Logger.ScriviMessaggioLog(ISC.LibrerieComuni.OggettiComuni.LogSystem.eLogLevel.LL_debugging, 0, "FRM_start", "TMR_controlli", "Verifica stampanti configurate")
            ObjGlobali.CFG.VerificaStampantiConfigurate()
        ElseIf nControllo = 5 Then
            LBL_controllo_runn.Text = "Verifica versione PDFCreator"
            Me.Refresh()
            ObjGlobali.Logger.ScriviMessaggioLog(ISC.LibrerieComuni.OggettiComuni.LogSystem.eLogLevel.LL_debugging, 0, "FRM_start", "TMR_controlli", "Verifica versione PDFCreator")
            ObjGlobali.CFG.ControllaVersioneLibrerie()
        ElseIf nControllo = 6 Then
            Me.Close()
        End If
        nControllo += 1

    End Sub

End Class