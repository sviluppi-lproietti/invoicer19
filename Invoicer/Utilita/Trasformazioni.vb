﻿Imports ISC.LibrerieComuni.ManageSessione.Entity

Public Class Trasformazioni

    Public Shared Function TrasformaInLista(ByVal s As Dictionary(Of Integer, StampantiEntity)) As List(Of StampantiEntity)
        Dim l As List(Of StampantiEntity)

        l = New List(Of StampantiEntity)
        For Each k As KeyValuePair(Of Integer, StampantiEntity) In s
            l.Add(k.Value)
        Next
        Return l

    End Function

    Public Shared Function TrasformaInLista(ByVal s As Dictionary(Of Integer, SessioneEntity)) As List(Of SessioneEntity)
        Dim l As List(Of SessioneEntity)

        l = New List(Of SessioneEntity)
        For Each k As KeyValuePair(Of Integer, SessioneEntity) In s
            l.Add(k.Value)
        Next
        Return l

    End Function

End Class
