Imports ISC.LibrerieComuni.ManageSessione
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori

Public Class FRM_apri

    Private Sub BTN_ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ok.Click

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub BTN_ignore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ignore.Click

        Me.DialogResult = Windows.Forms.DialogResult.Ignore
        Me.Close()

    End Sub

    Private Sub BTN_select_folder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_select_folder.Click

        FBD_select_folder.Description = "Seleziona il percorso dove � contenuta una sessione di stampa....."
        FBD_select_folder.SelectedPath = TXB_dest_folder.Text
        If FBD_select_folder.ShowDialog = Windows.Forms.DialogResult.OK Then
            TXB_dest_folder.Text = FBD_select_folder.SelectedPath
        End If

    End Sub

    Private Sub TXB_dest_folder_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TXB_dest_folder.TextChanged
        Dim gs As GestioneSessione

        gs = New GestioneSessione(eTipoApplicazione.TAP_Invoicer, ObjGlobali.Folders, ObjGlobali.CFG.VersioneSistemaOperativo, CFG.OLEDB_ConnString)
        gs.Anteprima(TXB_dest_folder.Text)
        LBL_descr_sessione.Text = gs.Descrizione
        BTN_ok.Enabled = gs.IsLoaded

    End Sub

    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean

        Select Case keyData
            Case Keys.Return
                If BTN_ok.Enabled Then
                    BTN_ok_Click(Nothing, Nothing)
                End If
            Case Keys.Escape
                BTN_ignore_Click(Nothing, Nothing)
        End Select

    End Function

End Class