Imports System.Threading
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita

Public Class FRM_nuova_attesa

    Private _ForceClose As Boolean
    Private _TmrElapsedStart As DateTime
    Private _thr2exec As Thread

    Private Sub StartTmr_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMR_check.Tick

        If _thr2exec Is Nothing Then
            _thr2exec = New Thread(New ThreadStart(AddressOf ObjGlobali.GestioneSessioneStampa.CreaNuovaSessione))
            _thr2exec.Start()
            _TmrElapsedStart = Now
        Else
            If _thr2exec.IsAlive Then
                LBL_time_elapsed.Text = FormattaOrario((Now - _TmrElapsedStart))

                With ObjGlobali.GestioneSessioneStampa
                    LBL_progr_att.Text = String.Concat(.MessaggiProgress.ProgressivoPrinc, "/", .MessaggiProgress.MassimoPrinc)
                    LBL_descr_att.Text = .MessaggiProgress.TipoOperazionePrinc
                    If .MessaggiProgress.MassimoPrinc > -1 Then
                        PGB_att.Maximum = .MessaggiProgress.MassimoPrinc
                        PGB_att.Value = .MessaggiProgress.ProgressivoPrinc
                    End If

                    LBL_progr_dett.Text = String.Concat(.MessaggiProgress.ProgressivoSecon, "/", .MessaggiProgress.MassimoSecon)
                    LBL_descr_dett.Text = .MessaggiProgress.TipoOperazioneSecon
                    If .MessaggiProgress.MassimoSecon > -1 Then
                        PGB_dett.Maximum = .MessaggiProgress.MassimoSecon
                        PGB_dett.Value = .MessaggiProgress.ProgressivoSecon
                    End If
                End With
                Me.Refresh()
            Else
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        End If

    End Sub

    Private Sub BTN_annulla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_annulla.Click

        TMR_check.Enabled = False
        If MessageBox.Show("Sei sicuro di voler interrompere la procedura di creazione della sessione?", "Interrompere creazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            _thr2exec.Abort()
            ObjGlobali.GestioneSessioneStampa.RimuoviSessioneCreazione()
            If ObjGlobali.GestioneSessioneStampa.Errore Is Nothing Then
                MessageBox.Show("Attenzione! Non � stato possibile rimuovere completamente la sessione in creazione.", "Errore in fase di rimozione", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Else
            TMR_check.Enabled = True
        End If
        Me.Close()

    End Sub

End Class