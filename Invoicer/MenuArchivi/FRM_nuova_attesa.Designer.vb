<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_nuova_attesa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LBL_descr_att = New System.Windows.Forms.Label()
        Me.TMR_check = New System.Windows.Forms.Timer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PGB_att = New System.Windows.Forms.ProgressBar()
        Me.LBL_progr_att = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LBL_time_elapsed = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PGB_dett = New System.Windows.Forms.ProgressBar()
        Me.LBL_progr_dett = New System.Windows.Forms.Label()
        Me.LBL_descr_dett = New System.Windows.Forms.Label()
        Me.BTN_annulla = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LBL_descr_att
        '
        Me.LBL_descr_att.Location = New System.Drawing.Point(9, 75)
        Me.LBL_descr_att.Name = "LBL_descr_att"
        Me.LBL_descr_att.Size = New System.Drawing.Size(429, 13)
        Me.LBL_descr_att.TabIndex = 0
        Me.LBL_descr_att.Text = "N/D"
        Me.LBL_descr_att.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TMR_check
        '
        Me.TMR_check.Enabled = True
        Me.TMR_check.Interval = 500
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Attivit�"
        '
        'PGB_att
        '
        Me.PGB_att.Location = New System.Drawing.Point(12, 49)
        Me.PGB_att.Name = "PGB_att"
        Me.PGB_att.Size = New System.Drawing.Size(426, 23)
        Me.PGB_att.TabIndex = 2
        '
        'LBL_progr_att
        '
        Me.LBL_progr_att.Location = New System.Drawing.Point(343, 33)
        Me.LBL_progr_att.Name = "LBL_progr_att"
        Me.LBL_progr_att.Size = New System.Drawing.Size(95, 13)
        Me.LBL_progr_att.TabIndex = 3
        Me.LBL_progr_att.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Tempo trascorso"
        '
        'LBL_time_elapsed
        '
        Me.LBL_time_elapsed.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_time_elapsed.Location = New System.Drawing.Point(337, 9)
        Me.LBL_time_elapsed.Name = "LBL_time_elapsed"
        Me.LBL_time_elapsed.Size = New System.Drawing.Size(101, 13)
        Me.LBL_time_elapsed.TabIndex = 5
        Me.LBL_time_elapsed.Text = "N/D"
        Me.LBL_time_elapsed.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(101, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Dettaglio attivit�"
        '
        'PGB_dett
        '
        Me.PGB_dett.Location = New System.Drawing.Point(12, 110)
        Me.PGB_dett.Name = "PGB_dett"
        Me.PGB_dett.Size = New System.Drawing.Size(426, 23)
        Me.PGB_dett.TabIndex = 7
        '
        'LBL_progr_dett
        '
        Me.LBL_progr_dett.Location = New System.Drawing.Point(340, 94)
        Me.LBL_progr_dett.Name = "LBL_progr_dett"
        Me.LBL_progr_dett.Size = New System.Drawing.Size(98, 13)
        Me.LBL_progr_dett.TabIndex = 8
        Me.LBL_progr_dett.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_descr_dett
        '
        Me.LBL_descr_dett.Location = New System.Drawing.Point(9, 136)
        Me.LBL_descr_dett.Name = "LBL_descr_dett"
        Me.LBL_descr_dett.Size = New System.Drawing.Size(429, 13)
        Me.LBL_descr_dett.TabIndex = 9
        Me.LBL_descr_dett.Text = "N/D"
        Me.LBL_descr_dett.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BTN_annulla
        '
        Me.BTN_annulla.Location = New System.Drawing.Point(184, 160)
        Me.BTN_annulla.Name = "BTN_annulla"
        Me.BTN_annulla.Size = New System.Drawing.Size(75, 23)
        Me.BTN_annulla.TabIndex = 10
        Me.BTN_annulla.Text = "Annulla"
        Me.BTN_annulla.UseVisualStyleBackColor = True
        '
        'FRM_nuova_attesa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 195)
        Me.ControlBox = False
        Me.Controls.Add(Me.BTN_annulla)
        Me.Controls.Add(Me.LBL_descr_dett)
        Me.Controls.Add(Me.LBL_progr_dett)
        Me.Controls.Add(Me.PGB_dett)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.LBL_time_elapsed)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LBL_progr_att)
        Me.Controls.Add(Me.PGB_att)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LBL_descr_att)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FRM_nuova_attesa"
        Me.Text = "Attesa"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LBL_descr_att As System.Windows.Forms.Label
    Friend WithEvents TMR_check As System.Windows.Forms.Timer
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PGB_att As System.Windows.Forms.ProgressBar
    Friend WithEvents LBL_progr_att As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LBL_time_elapsed As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PGB_dett As System.Windows.Forms.ProgressBar
    Friend WithEvents LBL_progr_dett As System.Windows.Forms.Label
    Friend WithEvents LBL_descr_dett As System.Windows.Forms.Label
    Friend WithEvents BTN_annulla As System.Windows.Forms.Button
End Class
