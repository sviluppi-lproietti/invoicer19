<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_apri
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BTN_ok = New System.Windows.Forms.Button()
        Me.TXB_dest_folder = New System.Windows.Forms.TextBox()
        Me.FBD_select_folder = New System.Windows.Forms.FolderBrowserDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BTN_ignore = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LBL_descr_sessione = New System.Windows.Forms.Label()
        Me.BTN_select_folder = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BTN_ok
        '
        Me.BTN_ok.Enabled = False
        Me.BTN_ok.Location = New System.Drawing.Point(235, 131)
        Me.BTN_ok.Name = "BTN_ok"
        Me.BTN_ok.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ok.TabIndex = 5
        Me.BTN_ok.Text = "OK"
        Me.BTN_ok.UseVisualStyleBackColor = True
        '
        'TXB_dest_folder
        '
        Me.TXB_dest_folder.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.TXB_dest_folder.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystemDirectories
        Me.TXB_dest_folder.Location = New System.Drawing.Point(146, 12)
        Me.TXB_dest_folder.Name = "TXB_dest_folder"
        Me.TXB_dest_folder.Size = New System.Drawing.Size(453, 20)
        Me.TXB_dest_folder.TabIndex = 1
        '
        'FBD_select_folder
        '
        Me.FBD_select_folder.ShowNewFolderButton = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Sessione da caricare"
        '
        'BTN_ignore
        '
        Me.BTN_ignore.Location = New System.Drawing.Point(349, 131)
        Me.BTN_ignore.Name = "BTN_ignore"
        Me.BTN_ignore.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignore.TabIndex = 6
        Me.BTN_ignore.Text = "ANNULLA"
        Me.BTN_ignore.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(131, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Descrizione della sessione"
        '
        'LBL_descr_sessione
        '
        Me.LBL_descr_sessione.Location = New System.Drawing.Point(143, 43)
        Me.LBL_descr_sessione.Name = "LBL_descr_sessione"
        Me.LBL_descr_sessione.Size = New System.Drawing.Size(487, 73)
        Me.LBL_descr_sessione.TabIndex = 4
        '
        'BTN_select_folder
        '
        Me.BTN_select_folder.Image = Global.Invoicer.My.Resources.Resources.PNG_folder
        Me.BTN_select_folder.Location = New System.Drawing.Point(605, 10)
        Me.BTN_select_folder.Name = "BTN_select_folder"
        Me.BTN_select_folder.Size = New System.Drawing.Size(27, 23)
        Me.BTN_select_folder.TabIndex = 2
        Me.BTN_select_folder.UseVisualStyleBackColor = True
        '
        'FRM_apri
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 166)
        Me.Controls.Add(Me.BTN_select_folder)
        Me.Controls.Add(Me.LBL_descr_sessione)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.BTN_ignore)
        Me.Controls.Add(Me.TXB_dest_folder)
        Me.Controls.Add(Me.BTN_ok)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FRM_apri"
        Me.Text = "Apri sessione stampa"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BTN_ok As System.Windows.Forms.Button
    Friend WithEvents TXB_dest_folder As System.Windows.Forms.TextBox
    Friend WithEvents FBD_select_folder As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BTN_ignore As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LBL_descr_sessione As System.Windows.Forms.Label
    Friend WithEvents BTN_select_folder As System.Windows.Forms.Button
End Class
