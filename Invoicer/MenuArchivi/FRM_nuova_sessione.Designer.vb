﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_nuova_sessione
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BTN_ignore = New System.Windows.Forms.Button()
        Me.BTN_ok = New System.Windows.Forms.Button()
        Me.CMB_sessioni = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GRP_sort_opz = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.BTN_deseleziona = New System.Windows.Forms.Button()
        Me.BTN_seleziona = New System.Windows.Forms.Button()
        Me.BTN_sposta_giu = New System.Windows.Forms.Button()
        Me.BTN_sposta_su = New System.Windows.Forms.Button()
        Me.DGV_giri_nosel = New System.Windows.Forms.DataGridView()
        Me.GIR_nome = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGV_giri_sel = New System.Windows.Forms.DataGridView()
        Me.GIR_nome_ass = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BTN_sess_seleziona = New System.Windows.Forms.Button()
        Me.GRP_dati_sessione = New System.Windows.Forms.GroupBox()
        Me.OFD_new_file = New System.Windows.Forms.OpenFileDialog()
        Me.FBD_select_folder = New System.Windows.Forms.FolderBrowserDialog()
        Me.GRP_progress_build = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TMR_operation = New System.Windows.Forms.Timer(Me.components)
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GRP_sort_opz.SuspendLayout()
        CType(Me.DGV_giri_nosel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_giri_sel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRP_dati_sessione.SuspendLayout()
        Me.GRP_progress_build.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BTN_ignore
        '
        Me.BTN_ignore.Location = New System.Drawing.Point(441, 12)
        Me.BTN_ignore.Name = "BTN_ignore"
        Me.BTN_ignore.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignore.TabIndex = 5
        Me.BTN_ignore.Text = "ANNULLA"
        Me.BTN_ignore.UseVisualStyleBackColor = True
        '
        'BTN_ok
        '
        Me.BTN_ok.Enabled = False
        Me.BTN_ok.Location = New System.Drawing.Point(291, 12)
        Me.BTN_ok.Name = "BTN_ok"
        Me.BTN_ok.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ok.TabIndex = 4
        Me.BTN_ok.Text = "OK"
        Me.BTN_ok.UseVisualStyleBackColor = True
        '
        'CMB_sessioni
        '
        Me.CMB_sessioni.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_sessioni.FormattingEnabled = True
        Me.CMB_sessioni.Location = New System.Drawing.Point(155, 6)
        Me.CMB_sessioni.Name = "CMB_sessioni"
        Me.CMB_sessioni.Size = New System.Drawing.Size(580, 21)
        Me.CMB_sessioni.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Elenco sessioni predisposte"
        '
        'GRP_sort_opz
        '
        Me.GRP_sort_opz.Controls.Add(Me.Label2)
        Me.GRP_sort_opz.Controls.Add(Me.Label3)
        Me.GRP_sort_opz.Controls.Add(Me.BTN_deseleziona)
        Me.GRP_sort_opz.Controls.Add(Me.BTN_seleziona)
        Me.GRP_sort_opz.Controls.Add(Me.BTN_sposta_giu)
        Me.GRP_sort_opz.Controls.Add(Me.BTN_sposta_su)
        Me.GRP_sort_opz.Controls.Add(Me.DGV_giri_nosel)
        Me.GRP_sort_opz.Controls.Add(Me.DGV_giri_sel)
        Me.GRP_sort_opz.Location = New System.Drawing.Point(6, 21)
        Me.GRP_sort_opz.Name = "GRP_sort_opz"
        Me.GRP_sort_opz.Size = New System.Drawing.Size(795, 252)
        Me.GRP_sort_opz.TabIndex = 9
        Me.GRP_sort_opz.TabStop = False
        Me.GRP_sort_opz.Text = "Opzioni ordinamento "
        Me.GRP_sort_opz.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(219, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Ordine dei giri di consegna disponibili"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(435, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(159, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Giri di consegna disponibili"
        '
        'BTN_deseleziona
        '
        Me.BTN_deseleziona.Location = New System.Drawing.Point(367, 94)
        Me.BTN_deseleziona.Name = "BTN_deseleziona"
        Me.BTN_deseleziona.Size = New System.Drawing.Size(64, 23)
        Me.BTN_deseleziona.TabIndex = 5
        Me.BTN_deseleziona.Text = ">>"
        Me.BTN_deseleziona.UseVisualStyleBackColor = True
        '
        'BTN_seleziona
        '
        Me.BTN_seleziona.Location = New System.Drawing.Point(366, 65)
        Me.BTN_seleziona.Name = "BTN_seleziona"
        Me.BTN_seleziona.Size = New System.Drawing.Size(64, 23)
        Me.BTN_seleziona.TabIndex = 4
        Me.BTN_seleziona.Text = "<<"
        Me.BTN_seleziona.UseVisualStyleBackColor = True
        '
        'BTN_sposta_giu
        '
        Me.BTN_sposta_giu.Location = New System.Drawing.Point(214, 218)
        Me.BTN_sposta_giu.Name = "BTN_sposta_giu"
        Me.BTN_sposta_giu.Size = New System.Drawing.Size(64, 23)
        Me.BTN_sposta_giu.TabIndex = 3
        Me.BTN_sposta_giu.Text = "Giù"
        Me.BTN_sposta_giu.UseVisualStyleBackColor = True
        '
        'BTN_sposta_su
        '
        Me.BTN_sposta_su.Location = New System.Drawing.Point(81, 218)
        Me.BTN_sposta_su.Name = "BTN_sposta_su"
        Me.BTN_sposta_su.Size = New System.Drawing.Size(64, 23)
        Me.BTN_sposta_su.TabIndex = 2
        Me.BTN_sposta_su.Text = "Su"
        Me.BTN_sposta_su.UseVisualStyleBackColor = True
        '
        'DGV_giri_nosel
        '
        Me.DGV_giri_nosel.AllowUserToAddRows = False
        Me.DGV_giri_nosel.AllowUserToDeleteRows = False
        Me.DGV_giri_nosel.AllowUserToOrderColumns = True
        Me.DGV_giri_nosel.AllowUserToResizeColumns = False
        Me.DGV_giri_nosel.AllowUserToResizeRows = False
        Me.DGV_giri_nosel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_giri_nosel.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GIR_nome})
        Me.DGV_giri_nosel.Location = New System.Drawing.Point(435, 32)
        Me.DGV_giri_nosel.Name = "DGV_giri_nosel"
        Me.DGV_giri_nosel.RowTemplate.Height = 20
        Me.DGV_giri_nosel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_giri_nosel.Size = New System.Drawing.Size(354, 203)
        Me.DGV_giri_nosel.TabIndex = 1
        '
        'GIR_nome
        '
        Me.GIR_nome.DataPropertyName = "GIR_nome"
        Me.GIR_nome.HeaderText = "Giro di consegna"
        Me.GIR_nome.Name = "GIR_nome"
        Me.GIR_nome.ReadOnly = True
        Me.GIR_nome.Width = 294
        '
        'DGV_giri_sel
        '
        Me.DGV_giri_sel.AllowUserToAddRows = False
        Me.DGV_giri_sel.AllowUserToDeleteRows = False
        Me.DGV_giri_sel.AllowUserToResizeColumns = False
        Me.DGV_giri_sel.AllowUserToResizeRows = False
        Me.DGV_giri_sel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_giri_sel.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GIR_nome_ass})
        Me.DGV_giri_sel.Location = New System.Drawing.Point(6, 32)
        Me.DGV_giri_sel.MultiSelect = False
        Me.DGV_giri_sel.Name = "DGV_giri_sel"
        Me.DGV_giri_sel.RowTemplate.Height = 20
        Me.DGV_giri_sel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_giri_sel.Size = New System.Drawing.Size(354, 183)
        Me.DGV_giri_sel.TabIndex = 0
        '
        'GIR_nome_ass
        '
        Me.GIR_nome_ass.DataPropertyName = "GIR_nome"
        Me.GIR_nome_ass.HeaderText = "Giro di Consegna"
        Me.GIR_nome_ass.Name = "GIR_nome_ass"
        Me.GIR_nome_ass.ReadOnly = True
        Me.GIR_nome_ass.Width = 294
        '
        'BTN_sess_seleziona
        '
        Me.BTN_sess_seleziona.Location = New System.Drawing.Point(741, 4)
        Me.BTN_sess_seleziona.Name = "BTN_sess_seleziona"
        Me.BTN_sess_seleziona.Size = New System.Drawing.Size(75, 23)
        Me.BTN_sess_seleziona.TabIndex = 2
        Me.BTN_sess_seleziona.Text = "Seleziona"
        Me.BTN_sess_seleziona.UseVisualStyleBackColor = True
        '
        'GRP_dati_sessione
        '
        Me.GRP_dati_sessione.AutoSize = True
        Me.GRP_dati_sessione.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.GRP_dati_sessione.Controls.Add(Me.GRP_sort_opz)
        Me.GRP_dati_sessione.Location = New System.Drawing.Point(3, 3)
        Me.GRP_dati_sessione.Name = "GRP_dati_sessione"
        Me.GRP_dati_sessione.Size = New System.Drawing.Size(807, 292)
        Me.GRP_dati_sessione.TabIndex = 3
        Me.GRP_dati_sessione.TabStop = False
        Me.GRP_dati_sessione.Text = "Dati sessione di stampa"
        Me.GRP_dati_sessione.Visible = False
        '
        'OFD_new_file
        '
        Me.OFD_new_file.FileName = "OpenFileDialog1"
        '
        'GRP_progress_build
        '
        Me.GRP_progress_build.Controls.Add(Me.Label5)
        Me.GRP_progress_build.Controls.Add(Me.ProgressBar1)
        Me.GRP_progress_build.Controls.Add(Me.Label4)
        Me.GRP_progress_build.Location = New System.Drawing.Point(3, 301)
        Me.GRP_progress_build.Name = "GRP_progress_build"
        Me.GRP_progress_build.Size = New System.Drawing.Size(801, 80)
        Me.GRP_progress_build.TabIndex = 13
        Me.GRP_progress_build.TabStop = False
        Me.GRP_progress_build.Text = "Preparazione Cartella nuova sessione"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(756, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Label5"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(9, 41)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(680, 23)
        Me.ProgressBar1.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 21)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Label4"
        '
        'TMR_operation
        '
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.GRP_dati_sessione)
        Me.FlowLayoutPanel1.Controls.Add(Me.GRP_progress_build)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 33)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(813, 432)
        Me.FlowLayoutPanel1.TabIndex = 14
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.BTN_ok)
        Me.Panel1.Controls.Add(Me.BTN_ignore)
        Me.Panel1.Location = New System.Drawing.Point(3, 387)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(801, 42)
        Me.Panel1.TabIndex = 15
        '
        'FRM_nuova_sessione
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(822, 472)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.BTN_sess_seleziona)
        Me.Controls.Add(Me.CMB_sessioni)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FRM_nuova_sessione"
        Me.Text = "Scegli la sessione da utilizzare e i dati per crearla"
        Me.GRP_sort_opz.ResumeLayout(False)
        Me.GRP_sort_opz.PerformLayout()
        CType(Me.DGV_giri_nosel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_giri_sel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRP_dati_sessione.ResumeLayout(False)
        Me.GRP_progress_build.ResumeLayout(False)
        Me.GRP_progress_build.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BTN_ignore As System.Windows.Forms.Button
    Friend WithEvents BTN_ok As System.Windows.Forms.Button
    Friend WithEvents CMB_sessioni As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GRP_sort_opz As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents BTN_deseleziona As System.Windows.Forms.Button
    Friend WithEvents BTN_seleziona As System.Windows.Forms.Button
    Friend WithEvents BTN_sposta_giu As System.Windows.Forms.Button
    Friend WithEvents BTN_sposta_su As System.Windows.Forms.Button
    Friend WithEvents DGV_giri_nosel As System.Windows.Forms.DataGridView
    Friend WithEvents GIR_nome As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DGV_giri_sel As System.Windows.Forms.DataGridView
    Friend WithEvents GIR_nome_ass As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BTN_sess_seleziona As System.Windows.Forms.Button
    Friend WithEvents GRP_dati_sessione As System.Windows.Forms.GroupBox
    Friend WithEvents OFD_new_file As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FBD_select_folder As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents GRP_progress_build As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TMR_operation As System.Windows.Forms.Timer
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
