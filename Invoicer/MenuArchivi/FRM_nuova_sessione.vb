﻿Imports ISC.LibrerieComuni.ManageSessione
Imports ISC.LibrerieComuni.ManageSessione.Entity
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori
Imports ISC.LibrerieComuni.OggettiComuniForm

Public Class FRM_nuova_sessione

    Public Property NuovaSessioneFLD As String
    Public Property ListaSessioniDefault As List(Of SessioneEntity)
    Private _thr2exec As System.Threading.Thread
    Private _GestSess As GestioneSessione
    Private _data_giri_sel As DS_giri_ass
    Private _ListaDatiNuovaSessione As ArrayList
    Private _ListaSortGiri As ArrayList

    Private Sub FRM_nuovo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        _GestSess = Nothing
        CMB_sessioni.Items.Add(New SessioneEntity("----- Seleziona una sessione da stampare -----"))
        For Each s As SessioneEntity In ListaSessioniDefault
            CMB_sessioni.Items.Add(s)
        Next
        CMB_sessioni.ValueMember = "SES_codice"
        CMB_sessioni.DisplayMember = "SES_nome"
        CMB_sessioni.SelectedIndex = 0

        CMB_sessioni.Enabled = CMB_sessioni.Items.Count > 1
        GRP_progress_build.Visible = False

    End Sub

    Private Sub CMB_moduli_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CMB_sessioni.SelectedIndexChanged

        BTN_sess_seleziona.Enabled = CMB_sessioni.SelectedIndex > 0

    End Sub

    Private Sub BTN_sess_seleziona_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_sess_seleziona.Click
        Dim s As SessioneEntity
        Dim xPos As Integer
        Dim yPos As Integer
        Dim wSize As Integer
        Dim hSize As Integer
        Dim lMultiline As Boolean
        Dim nMaxY As Integer
        Dim lda As Layer_DataAccess
        Dim dv_gir_ass As DataView
        Dim dv_gir_nas As DataView
        Dim cKind As String
        Dim ctrlA As Control
        Dim ctrlB As Control
        Dim cTmp As String
        Dim g As GroupBox

        '
        ' Individua la sessione selezionata
        '
        s = CMB_sessioni.SelectedItem

        '
        ' Carica la sessione base 
        '
        _GestSess = New GestioneSessione(eTipoApplicazione.TAP_Invoicer, ObjGlobali.Folders, ObjGlobali.CFG.VersioneSistemaOperativo, CFG.OLEDB_ConnString)
        _GestSess.Apertura(s.SES_folder, True)
        If _GestSess.IsLoaded Then
            '
            ' Passa il percorso dove è memorizzato il database dei giri.
            '
            _GestSess.DBGiriConnectionString = ObjGlobali.CFG.DBGiriConsegna_CS

            '
            ' Rimuovo i vecchi controlli
            '
            g = GRP_dati_sessione
            While g.Controls.Count > 1
                g.Controls.Remove(g.Controls(g.Controls.Count - 1))
            End While

            '
            ' Se il caricamento a avuto esito positivo allora procedo con la richiesta dei dati.
            '
            nMaxY = 0
            GRP_sort_opz.Visible = False
            For Each aParam As String() In _GestSess.ListaControlliFormNuovaSessione
                ctrlA = Nothing
                '
                ' Recupera i parametri di posizione e dimensione del controllo
                '
                cKind = aParam(0).Substring(0, 3)
                If cKind = "SRT" Then
                    If ObjGlobali.CFG.DBGiriConsegna_CS = "" Then
                        MessageBox.Show("Attenzione! Il percorso per il database dei Giri di Consegna non esiste o non è stato specificato. Non verrà mostrata la selezione dei giri di consegna.", "Visualizzazione giri di consegna", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        cKind = ""
                    End If
                    yPos = nMaxY + 10
                    hSize = GRP_sort_opz.Height
                Else
                    xPos = aParam(2).Substring(0, aParam(2).IndexOf(";"))
                    yPos = aParam(2).Substring(aParam(2).IndexOf(";") + 1)
                    lMultiline = aParam(3).StartsWith("M;")
                    If lMultiline Then
                        aParam(3) = aParam(3).Substring(2)
                    End If
                    wSize = aParam(3).Substring(0, aParam(3).IndexOf(";"))
                    hSize = aParam(3).Substring(aParam(3).IndexOf(";") + 1)
                    If nMaxY < yPos + hSize Then
                        nMaxY = yPos + hSize
                    End If
                End If
                Select Case cKind
                    Case Is = "LBL"
                        ctrlA = New Label()
                        ctrlA.Text = aParam(1)
                        ctrlA.Font = New Font(ctrlA.Font, FontStyle.Bold)
                    Case Is = "TXB"
                        ctrlA = New TextBox()
                        cTmp = aParam(1).Replace("&", "")
                        If cTmp = "1" Then
                            ctrlA.Text = ObjGlobali.Folders("WorkFLD")
                        ElseIf cTmp = "998" Then
                            ctrlA.Text = ObjGlobali.Folders("SessioniCreateFLD")
                        Else
                            ctrlA.Text = aParam(1)
                        End If
                        AddHandler ctrlA.LostFocus, AddressOf TXB_LostFocus
                        CType(ctrlA, TextBox).Multiline = lMultiline
                    Case Is = "BTN"
                        ctrlA = New Button
                        If aParam(0).Substring(4, 3) = "fld" Then
                            CType(ctrlA, Button).Image = Global.Invoicer.My.Resources.Resources.PNG_folder
                            AddHandler CType(ctrlA, Button).Click, AddressOf BTN_select_folder_Click
                            ctrlB = Me.Controls.Find(aParam(0).Replace("BTN_fld", "TXB"), True)(0)
                            CType(ctrlB, TextBox).AutoCompleteMode = AutoCompleteMode.SuggestAppend
                            CType(ctrlB, TextBox).AutoCompleteSource = AutoCompleteSource.FileSystemDirectories
                        ElseIf aParam(0).Substring(4, 3) = "fil" Then
                            ctrlB = Me.Controls.Find(aParam(0).Replace("BTN_fil", "TXB"), True)(0)
                            CType(ctrlA, Button).Image = Global.Invoicer.My.Resources.Resources.PNG_file
                            AddHandler CType(ctrlA, Button).Click, AddressOf BTN_select_file_Click
                            CType(ctrlB, TextBox).AutoCompleteMode = AutoCompleteMode.SuggestAppend
                            CType(ctrlB, TextBox).AutoCompleteSource = AutoCompleteSource.FileSystem
                        End If
                    Case Is = "SRT"
                        GRP_sort_opz.Visible = True
                        GRP_sort_opz.Top = yPos

                        _data_giri_sel = New DS_giri_ass
                        lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri_sel)
                        lda.CaricaDataSet("TBL_giri")
                        _data_giri_sel.TBL_giri.Columns.Add(New DataColumn("GIR_sortcode", System.Type.GetType("System.Int32")))
                        For Each dr_gir As DataRow In _data_giri_sel.TBL_giri.Rows
                            dr_gir.Item("GIR_sortcode") = 0
                        Next
                        dv_gir_ass = New DataView(_data_giri_sel.TBL_giri)
                        dv_gir_ass.RowFilter = "GIR_sortcode > 0"
                        dv_gir_ass.Sort = "GIR_sortcode"
                        dv_gir_nas = New DataView(_data_giri_sel.TBL_giri)
                        dv_gir_nas.RowFilter = "GIR_sortcode = 0"

                        DGV_giri_sel.AutoGenerateColumns = False
                        DGV_giri_sel.DataSource = dv_gir_ass
                        DGV_giri_nosel.AutoGenerateColumns = False
                        DGV_giri_nosel.DataSource = dv_gir_nas
                        SetGiriOpzOrdinam()
                End Select
                If ctrlA IsNot Nothing Then
                    ctrlA.Name = aParam(0)
                    ctrlA.Location = New System.Drawing.Point(xPos, yPos)
                    ctrlA.Size = New Size(wSize, hSize)
                    g.Controls.Add(ctrlA)
                End If
            Next
            GRP_dati_sessione.Visible = True
            GRP_progress_build.Visible = False
        End If

    End Sub

    Private Sub BTN_seleziona_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_seleziona.Click
        Dim bm_gir_nas As BindingManagerBase
        Dim dr As DataRow

        bm_gir_nas = DGV_giri_nosel.BindingContext(DGV_giri_nosel.DataSource, DGV_giri_nosel.DataMember)
        dr = CType(bm_gir_nas.Current, DataRowView).Row
        dr.Item("GIR_sortcode") = 100 + (CType(DGV_giri_sel.DataSource, DataView).Count + 1) * 10
        SetGiriOpzOrdinam()

    End Sub

    Private Sub BTN_deseleziona_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_deseleziona.Click
        Dim bm_gir_ass As BindingManagerBase
        Dim dr As DataRow

        bm_gir_ass = DGV_giri_nosel.BindingContext(DGV_giri_sel.DataSource, DGV_giri_sel.DataMember)
        dr = CType(bm_gir_ass.Current, DataRowView).Row
        dr.Item("GIR_sortcode") = 0
        SetGiriOpzOrdinam()

    End Sub

    Private Sub BTN_sposta_su_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_sposta_su.Click
        Dim bm_gir_ass As BindingManagerBase
        Dim dv_gir_ass As DataView
        Dim nSortCode As Integer
        Dim dr As DataRow

        bm_gir_ass = DGV_giri_nosel.BindingContext(DGV_giri_sel.DataSource, DGV_giri_sel.DataMember)
        dr = CType(bm_gir_ass.Current, DataRowView).Row
        dr.Item("GIR_sortcode") -= 15
        dr.AcceptChanges()
        dv_gir_ass = DGV_giri_sel.DataSource
        nSortCode = 100 + dv_gir_ass.Count * 10
        For i As Integer = dv_gir_ass.Count - 1 To 0 Step -1
            dr = dv_gir_ass(i).Row
            dr("GIR_sortcode") = nSortCode
            nSortCode -= 10
        Next
        _data_giri_sel.TBL_giri.AcceptChanges()

    End Sub

    Private Sub BTN_sposta_giu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_sposta_giu.Click
        Dim bm_gir_ass As BindingManagerBase
        Dim dv_gir_ass As DataView
        Dim nSortCode As Integer
        Dim dr As DataRow

        bm_gir_ass = DGV_giri_nosel.BindingContext(DGV_giri_sel.DataSource, DGV_giri_sel.DataMember)
        dr = CType(bm_gir_ass.Current, DataRowView).Row
        dr.Item("GIR_sortcode") += 15
        dr.AcceptChanges()
        dv_gir_ass = DGV_giri_sel.DataSource
        nSortCode = 110
        For i As Integer = 0 To dv_gir_ass.Count - 1
            dr = dv_gir_ass(i).Row
            dr("GIR_sortcode") = nSortCode
            nSortCode += 10
        Next
        _data_giri_sel.TBL_giri.AcceptChanges()

    End Sub

    Private Sub SetGiriOpzOrdinam()
        Dim dv_gir_ass As DataView
        Dim dv_gir_nas As DataView

        dv_gir_ass = DGV_giri_sel.DataSource
        dv_gir_nas = DGV_giri_nosel.DataSource

        BTN_seleziona.Enabled = dv_gir_nas.Count > 0
        BTN_deseleziona.Enabled = dv_gir_ass.Count > 0
        BTN_sposta_su.Enabled = dv_gir_ass.Count > 0
        BTN_sposta_giu.Enabled = dv_gir_ass.Count > 0
        If DGV_giri_sel.SelectedRows.Count = 1 Then
            BTN_sposta_su.Enabled = DGV_giri_sel.SelectedRows(0).Index > 0
            BTN_sposta_giu.Enabled = DGV_giri_sel.SelectedRows(0).Index < DGV_giri_sel.RowCount - 1
        End If

    End Sub

    Private Sub DGV_giri_sel_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGV_giri_sel.SelectionChanged
        Dim dv_gir_ass As DataView

        If DGV_giri_sel.SelectedRows.Count = 1 Then
            dv_gir_ass = DGV_giri_sel.DataSource
            BTN_sposta_su.Enabled = DGV_giri_sel.SelectedRows(0).Index > 0
            BTN_sposta_giu.Enabled = DGV_giri_sel.SelectedRows(0).Index < dv_gir_ass.Count - 1
        End If

    End Sub

    Private Sub BTN_ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ok.Click
        Dim dv_gir_ass As DataView
        Dim cTxb As TextBox
        Dim dr As DataRow

        CMB_sessioni.Enabled = False
        BTN_sess_seleziona.Enabled = False
        GRP_dati_sessione.Enabled = False
        BTN_ok.Enabled = False
        _ListaSortGiri = New ArrayList
        If MessageBox.Show("Sei sicuro di voler procedere con la creazione di una nuova sessione di stampa?", "Conferma", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            CMB_sessioni.Enabled = False
            BTN_sess_seleziona.Enabled = False
            GRP_dati_sessione.Enabled = False
            BTN_ok.Enabled = False
            NuovaSessioneFLD = ""
            _ListaDatiNuovaSessione = New ArrayList
            For Each aParam As String() In _GestSess.ListaControlliFormNuovaSessione
                If aParam(0).StartsWith("TXB_") Then
                    '
                    ' indica che il valor contenuto è una cartella. 
                    ' Estraggo il contenuto della stessa e memorizzo i valori nel file dei risultati
                    ' con il codice dopo l'asterisco
                    '
                    cTxb = CType(Controls.Find(aParam(0), True)(0), TextBox)
                    If aParam(4).StartsWith("*") Then
                        For Each cfile As String In System.IO.Directory.GetFiles(cTxb.Text)
                            _ListaDatiNuovaSessione.Add(New String() {aParam(4).Replace("*", ""), cfile})
                        Next
                    Else
                        Select Case aParam(4)
                            Case Is = 201
                                NuovaSessioneFLD = StandardFolderValue(cTxb.Text)
                                _ListaDatiNuovaSessione.Add(New String() {aParam(4), StandardFolderValue(cTxb.Text)})
                            Case Is = 202
                                _ListaDatiNuovaSessione.Add(New String() {aParam(4), cTxb.Text})
                            Case Else
                                If System.IO.File.Exists(cTxb.Text) Then
                                    _ListaDatiNuovaSessione.Add(New String() {aParam(4), cTxb.Text})
                                End If
                        End Select
                    End If
                End If
            Next
            If NuovaSessioneFLD > "" Then
                If System.IO.Directory.Exists(NuovaSessioneFLD) Then
                    MessageBox.Show("La cartella indicata per la sessione è già esistente non posso proseguire.", "Cartella sessione esistente", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Me.DialogResult = Windows.Forms.DialogResult.None
                Else
                    If GRP_sort_opz.Visible Then
                        dv_gir_ass = DGV_giri_sel.DataSource
                        For i As Integer = 0 To dv_gir_ass.Count - 1
                            dr = dv_gir_ass(i).Row
                            _ListaSortGiri.Add(dr("GIR_codice"))
                        Next
                    End If
                    GRP_progress_build.Visible = True
                    TMR_operation.Start()
                    '_GestSess.CreaFolderNuovaSessione(NuovaSessioneFLD)
                End If
            End If
        End If

    End Sub

    Private Sub BTN_ignore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ignore.Click

        If _GestSess IsNot Nothing Then
            If MessageBox.Show("Sei sicuro di voler interrompere la procedura di creazione della sessione?", "Interrompere creazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                _thr2exec.Abort()
                _GestSess.RimuoviSessioneCreazione()
                If ObjGlobali.GestioneSessioneStampa.Errore IsNot Nothing Then
                    MessageBox.Show("Attenzione! Non è stato possibile rimuovere completamente la sessione in creazione.", "Errore in fase di rimozione", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
                Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Else
                TMR_operation.Enabled = True
            End If
        End If
        Me.DialogResult = Windows.Forms.DialogResult.Ignore
        Me.Close()

    End Sub

    Public ReadOnly Property ListaDatiNuovaSessione() As ArrayList

        Get
            Return _ListaDatiNuovaSessione
        End Get

    End Property

    Public ReadOnly Property ListaSortGiri() As ArrayList

        Get
            Return _ListaSortGiri
        End Get

    End Property

    Private Sub BTN_select_file_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim cPath As String = ""
        Dim cFile As String = ""
        Dim cName As String
        Dim txb As TextBox

        cName = CType(sender, Button).Name
        txb = Me.Controls.Find(cName.Replace("BTN_fil", "TXB"), True)(0)
        ' OFD_new_file.Multiselect = False
        cFile = ""
        If txb.Text.EndsWith("\") Then
            cPath = txb.Text
        Else
            If System.IO.File.Exists(txb.Text) Then
                cFile = System.IO.Path.GetFileName(txb.Text)
                cPath = txb.Text.Replace(cFile, "")
            Else
                cPath = System.IO.Path.GetDirectoryName(txb.Text)
            End If
        End If
        OFD_new_file.InitialDirectory = cPath
        OFD_new_file.FileName = cFile

        If OFD_new_file.ShowDialog = Windows.Forms.DialogResult.OK Then
            txb.Text = OFD_new_file.FileName
        End If

    End Sub

    Private Sub BTN_select_folder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim cName As String
        Dim txb As TextBox

        cName = CType(sender, Button).Name
        txb = Me.Controls.Find(cName.Replace("BTN_fld", "TXB"), True)(0)
        FBD_select_folder.SelectedPath = txb.Text
        FBD_select_folder.ShowNewFolderButton = cName.ToLower.Contains("output")
        If FBD_select_folder.ShowDialog = Windows.Forms.DialogResult.OK Then
            txb.Text = StandardFolderValue(FBD_select_folder.SelectedPath)
        End If

    End Sub

    Private Sub TXB_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lRtn As Boolean

        'lRtn = (TXB_file_to_load.Text > "") And (System.IO.File.Exists(TXB_file_to_load.Text))
        'If nTest > 1 Then
        '    If Not System.IO.File.Exists(TXB_file_to_load.Text) Then
        '        MessageBox.Show("Attenzione! File da importare on esistente.")
        '    End If
        'End If
        'lRtn = lRtn And (TXB_dest_folder.Text > "") And (TXB_dest_folder.Text <> DatiPath)
        lRtn = True
        BTN_ok.Enabled = lRtn

    End Sub

    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean

        Select Case keyData
            'Case Keys.Return
            '    If BTN_ok.Enabled Then
            '        BTN_ok_Click(Nothing, Nothing)
            '    End If
            Case Keys.Escape
                BTN_ignore_Click(Nothing, Nothing)
        End Select

    End Function

    Private Sub TMR_operation_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMR_operation.Tick

        If _thr2exec Is Nothing Then
            _thr2exec = New System.Threading.Thread(Sub() _GestSess.CreaFolderNuovaSessione(NuovaSessioneFLD))
            _thr2exec.Start()
        Else
            If _thr2exec.IsAlive Then
                If _GestSess.MessaggiProgress.Abilitati Then
                    Label4.Text = _GestSess.MessaggiProgress.TipoOperazionePrinc
                    ProgressBar1.Maximum = _GestSess.MessaggiProgress.MassimoPrinc
                    ProgressBar1.Value = _GestSess.MessaggiProgress.ProgressivoPrinc

                    Label5.Text = String.Concat(_GestSess.MessaggiProgress.ProgressivoPrinc, "/", _GestSess.MessaggiProgress.MassimoPrinc)
                End If
                Me.Refresh()
            Else
                TMR_operation.Enabled = False
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        End If

    End Sub

End Class