<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_select_range
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.TXB_da = New System.Windows.Forms.TextBox
        Me.TXB_a = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.CHB_hold_sel = New System.Windows.Forms.CheckBox
        Me.BTN_ok = New System.Windows.Forms.Button
        Me.BTN_ignore = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Da"
        '
        'TXB_da
        '
        Me.TXB_da.Location = New System.Drawing.Point(39, 9)
        Me.TXB_da.Name = "TXB_da"
        Me.TXB_da.Size = New System.Drawing.Size(100, 20)
        Me.TXB_da.TabIndex = 1
        '
        'TXB_a
        '
        Me.TXB_a.Location = New System.Drawing.Point(172, 9)
        Me.TXB_a.Name = "TXB_a"
        Me.TXB_a.Size = New System.Drawing.Size(100, 20)
        Me.TXB_a.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(145, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(14, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "A"
        '
        'CHB_hold_sel
        '
        Me.CHB_hold_sel.AutoSize = True
        Me.CHB_hold_sel.Location = New System.Drawing.Point(48, 35)
        Me.CHB_hold_sel.Name = "CHB_hold_sel"
        Me.CHB_hold_sel.Size = New System.Drawing.Size(162, 17)
        Me.CHB_hold_sel.TabIndex = 4
        Me.CHB_hold_sel.Text = "Mantieni precedenti selezioni"
        Me.CHB_hold_sel.UseVisualStyleBackColor = True
        '
        'BTN_ok
        '
        Me.BTN_ok.Enabled = False
        Me.BTN_ok.Location = New System.Drawing.Point(48, 72)
        Me.BTN_ok.Name = "BTN_ok"
        Me.BTN_ok.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ok.TabIndex = 6
        Me.BTN_ok.Text = "OK"
        Me.BTN_ok.UseVisualStyleBackColor = True
        '
        'BTN_ignore
        '
        Me.BTN_ignore.Location = New System.Drawing.Point(148, 72)
        Me.BTN_ignore.Name = "BTN_ignore"
        Me.BTN_ignore.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignore.TabIndex = 7
        Me.BTN_ignore.Text = "Annulla"
        Me.BTN_ignore.UseVisualStyleBackColor = True
        '
        'FRM_select_range
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(280, 107)
        Me.Controls.Add(Me.BTN_ignore)
        Me.Controls.Add(Me.BTN_ok)
        Me.Controls.Add(Me.CHB_hold_sel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TXB_a)
        Me.Controls.Add(Me.TXB_da)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FRM_select_range"
        Me.Text = "Seleziona intervallo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXB_da As System.Windows.Forms.TextBox
    Friend WithEvents TXB_a As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CHB_hold_sel As System.Windows.Forms.CheckBox
    Friend WithEvents BTN_ok As System.Windows.Forms.Button
    Friend WithEvents BTN_ignore As System.Windows.Forms.Button
End Class
