Public Class FRM_select_range

    Private Sub BTN_ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ok.Click

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub BTN_ignore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ignore.Click

        Me.DialogResult = Windows.Forms.DialogResult.Ignore
        Me.Close()

    End Sub

    Private Sub TXB_da_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXB_da.TextChanged

        BTN_ok.Enabled = IsNumeric(TXB_da.Text) And IsNumeric(TXB_a.Text)
        TXB_a.Text = TXB_da.Text

    End Sub

    Private Sub TXB_a_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXB_a.TextChanged

        BTN_ok.Enabled = IsNumeric(TXB_da.Text) And IsNumeric(TXB_a.Text)

    End Sub

    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean

        Select Case keyData
            Case Keys.Return
                If BTN_ok.Enabled Then
                    BTN_ok_Click(Nothing, Nothing)
                End If
            Case Keys.Escape
                BTN_ignore_Click(Nothing, Nothing)
        End Select

    End Function

End Class