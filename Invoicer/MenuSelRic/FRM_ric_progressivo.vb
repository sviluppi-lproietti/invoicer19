Public Class FRM_ric_progressivo

    Private Sub BTN_ricerca_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ricerca.Click

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub BTN_ignora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ignora.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()

    End Sub

    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean

        Select Case keyData
            Case Keys.Return
                BTN_ricerca_Click(Nothing, Nothing)
        End Select

    End Function

    Private Sub TXB_progressivo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXB_progressivo.TextChanged

        BTN_ricerca.Enabled = TXB_progressivo.Text > ""

    End Sub

End Class