<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_ric_progressivo
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TXB_progressivo = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.BTN_ricerca = New System.Windows.Forms.Button
        Me.BTN_ignora = New System.Windows.Forms.Button
        Me.CHB_ric_select = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'TXB_progressivo
        '
        Me.TXB_progressivo.Location = New System.Drawing.Point(172, 6)
        Me.TXB_progressivo.Name = "TXB_progressivo"
        Me.TXB_progressivo.Size = New System.Drawing.Size(100, 20)
        Me.TXB_progressivo.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(121, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Progressivo da ricercare"
        '
        'BTN_ricerca
        '
        Me.BTN_ricerca.Enabled = False
        Me.BTN_ricerca.Location = New System.Drawing.Point(58, 55)
        Me.BTN_ricerca.Name = "BTN_ricerca"
        Me.BTN_ricerca.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ricerca.TabIndex = 2
        Me.BTN_ricerca.Text = "Ricerca"
        Me.BTN_ricerca.UseVisualStyleBackColor = True
        '
        'BTN_ignora
        '
        Me.BTN_ignora.Location = New System.Drawing.Point(157, 55)
        Me.BTN_ignora.Name = "BTN_ignora"
        Me.BTN_ignora.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignora.TabIndex = 3
        Me.BTN_ignora.Text = "Ignora"
        Me.BTN_ignora.UseVisualStyleBackColor = True
        '
        'CHB_ric_select
        '
        Me.CHB_ric_select.AutoSize = True
        Me.CHB_ric_select.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CHB_ric_select.Location = New System.Drawing.Point(12, 32)
        Me.CHB_ric_select.Name = "CHB_ric_select"
        Me.CHB_ric_select.Size = New System.Drawing.Size(174, 17)
        Me.CHB_ric_select.TabIndex = 1
        Me.CHB_ric_select.Text = "Seleziona elemento se trovato?"
        Me.CHB_ric_select.UseVisualStyleBackColor = True
        '
        'FRM_ric_progressivo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 86)
        Me.Controls.Add(Me.CHB_ric_select)
        Me.Controls.Add(Me.BTN_ignora)
        Me.Controls.Add(Me.BTN_ricerca)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXB_progressivo)
        Me.Name = "FRM_ric_progressivo"
        Me.Text = "Ricerca per progressivo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TXB_progressivo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BTN_ricerca As System.Windows.Forms.Button
    Friend WithEvents BTN_ignora As System.Windows.Forms.Button
    Friend WithEvents CHB_ric_select As System.Windows.Forms.CheckBox
End Class
