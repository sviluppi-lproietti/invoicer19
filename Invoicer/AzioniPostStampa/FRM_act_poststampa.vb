Imports System.Windows.Forms

Public Class FRM_azione_poststampa

    Private _data_tab_serv As DS_act_ps_serv
    Private _data_Action As DS_poststampa_azioni
    Private _bind As BindingSource
    Private _InsertRow As Boolean
    'Private _GestSess As ManageSessione.GestioneSessione

    'Public WriteOnly Property GestSess() As ManageSessione.GestioneSessione

    '    Set(ByVal value As ManageSessione.GestioneSessione)
    '        _GestSess = value
    '    End Set

    'End Property


    Private ReadOnly Property AzioniPostStampa_filename() As String

        Get
            Return String.Concat(ObjGlobali.GestioneSessioneStampa.Folders("DatiFLD"), "azioni_poststampa.xml")
        End Get

    End Property

    Private Sub FRM_azione_poststampa_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        _data_Action = New DS_poststampa_azioni
        If System.IO.File.Exists(AzioniPostStampa_filename) Then
            _data_Action.ReadXml(AzioniPostStampa_filename)
            _data_Action.AcceptChanges()
            For Each dr_psa As DS_poststampa_azioni.TBL_PostStampa_AzioniRow In _data_Action.TBL_PostStampa_Azioni.Rows
                If dr_psa.PSA_cod_cond = 1 Then
                    dr_psa.PSA_cod_cond = 1001
                End If
                If dr_psa.PSA_cod_act = 1 Then
                    dr_psa.PSA_cod_act = 101
                End If
            Next
        End If
        PreparaTabelleServizio()

        _bind = New BindingSource
        _bind.DataSource = _data_Action
        _bind.DataMember = "TBL_PostStampa_Azioni"

        CMB_cod_cond.DataSource = _data_tab_serv.TBL_serv1
        CMB_cod_cond.DisplayMember = "SE1_descrizione"
        CMB_cod_cond.ValueMember = "SE1_chiave"

        CMB_cod_action.DataSource = _data_tab_serv.TBL_serv2
        CMB_cod_action.DisplayMember = "SE2_descrizione"
        CMB_cod_action.ValueMember = "SE2_chiave"

        CMB_cod_cond.DataBindings.Clear()
        CMB_cod_cond.DataBindings.Add(New Binding("SelectedValue", _bind, "PSA_cod_cond", True))

        TXB_val_cond.DataBindings.Clear()
        TXB_val_cond.DataBindings.Add(New Binding("Text", _bind, "PSA_val_cond", True))

        CMB_cod_action.DataBindings.Clear()
        CMB_cod_action.DataBindings.Add(New Binding("SelectedValue", _bind, "PSA_cod_act", True))

        TXB_val_act.DataBindings.Clear()
        TXB_val_act.DataBindings.Add(New Binding("Text", _bind, "PSA_val_act", True))

        DataGridView1.AutoGenerateColumns = False
        DataGridView1.Columns.Add(COL_cod_condizione())
        DataGridView1.Columns.Add(COL_val_condizione())
        DataGridView1.Columns.Add(COL_cod_azione())
        DataGridView1.Columns.Add(COL_val_azione())
        DataGridView1.DataSource = _bind

    End Sub

    Private Sub DGV_dati_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DataGridView1.CellFormatting
        Dim dr As DataGridViewRow
        Dim nValue As Integer

        dr = DataGridView1.Rows(e.RowIndex)
        If DataGridView1.Columns(e.ColumnIndex).DataPropertyName = "PSA_cod_cond" Then
            nValue = dr.DataBoundItem(DataGridView1.Columns(e.ColumnIndex).DataPropertyName)
            e.Value = ""
            If nValue > 0 Then
                e.Value = CType(_data_tab_serv.TBL_serv1.FindBySE1_chiave(nValue), DS_act_ps_serv.TBL_serv1Row).SE1_descrizione
            End If
        End If
            If DataGridView1.Columns(e.ColumnIndex).DataPropertyName = "PSA_cod_act" Then
            nValue = dr.DataBoundItem(DataGridView1.Columns(e.ColumnIndex).DataPropertyName)
            e.Value = ""
            If nValue > 0 Then
                e.Value = CType(_data_tab_serv.TBL_serv2.FindBySE2_chiave(nValue), DS_act_ps_serv.TBL_serv2Row).SE2_descrizione
            End If
        End If

    End Sub

    Private Function COL_cod_condizione() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Condizione"
            .DataPropertyName = "PSA_cod_cond"
            .Width = 150
        End With
        Return col

    End Function

    Private Function COL_val_condizione() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Valore condizione"
            .DataPropertyName = "PSA_val_cond"
            .Width = 300
        End With
        Return col

    End Function

    Private Function COL_cod_azione() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Azione"
            .DataPropertyName = "PSA_cod_act"
            .Width = 200
        End With
        Return col

    End Function

    Private Function COL_val_azione() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Valore azione"
            .DataPropertyName = "PSA_val_act"
            .Width = 300
        End With
        Return col

    End Function

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        ' bottone per aggiungere
        Dim dr_act As DS_poststampa_azioni.TBL_PostStampa_AzioniRow

        Me.ActiveControl = CMB_cod_cond
        dr_act = CType(_bind.AddNew.Row, DS_poststampa_azioni.TBL_PostStampa_AzioniRow)
        dr_act.PSA_cod_cond = 0
        dr_act.PSA_val_cond = ""
        dr_act.PSA_cod_act = 0
        dr_act.PSA_val_act = ""
        _InsertRow = True

    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating

        If _InsertRow Then
            e.Cancel = MessageBox.Show("L'azione inserita non � stata salvata. Spostandosi su un altro record si perderanno le modifiche effettuate. Procedo comunque?", "Nuova azione", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No
            If Not e.Cancel Then
                DataGridView1.BindingContext(DataGridView1.DataSource, DataGridView1.DataMember).CancelCurrentEdit()
                _InsertRow = False
            End If
        End If

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Dim bm As BindingManagerBase

        bm = DataGridView1.BindingContext(DataGridView1.DataSource, DataGridView1.DataMember)
        bm.EndCurrentEdit()
        _InsertRow = False
        _data_Action.WriteXml(AzioniPostStampa_filename)

    End Sub

    Private Sub CMB_cod_cond_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CMB_cod_cond.SelectedIndexChanged

        TXB_val_cond.Text = ""

    End Sub

    Private Sub CMB_cod_action_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CMB_cod_action.SelectedIndexChanged

        Button1.Visible = CMB_cod_action.SelectedIndex = 1
        TXB_val_act.Text = ""

    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Dim dr_act As DS_poststampa_azioni.TBL_PostStampa_AzioniRow

        dr_act = CType(CType(DataGridView1.BindingContext(DataGridView1.DataSource, DataGridView1.DataMember).Current, DataRowView).Row, DS_poststampa_azioni.TBL_PostStampa_AzioniRow)
        dr_act.Delete()
        _data_Action.AcceptChanges()
        _data_Action.WriteXml(AzioniPostStampa_filename)

    End Sub

    Private Sub PreparaTabelleServizio()
        Dim dr_se1 As DS_act_ps_serv.TBL_serv1Row
        Dim dr_se2 As DS_act_ps_serv.TBL_serv2Row

        _data_tab_serv = New DS_act_ps_serv
        dr_se1 = _data_tab_serv.TBL_serv1.NewRow
        dr_se1.SE1_chiave = -1
        dr_se1.SE1_descrizione = "--- Seleziona condizione ---"
        _data_tab_serv.TBL_serv1.Rows.Add(dr_se1)

        dr_se2 = _data_tab_serv.TBL_serv2.NewRow
        dr_se2.SE2_chiave = -1
        dr_se2.SE2_descrizione = "--- Seleziona condizione ---"
        _data_tab_serv.TBL_serv2.Rows.Add(dr_se2)

        dr_se2 = _data_tab_serv.TBL_serv2.NewRow
        dr_se2.SE2_chiave = 101
        dr_se2.SE2_descrizione = "Copia file"
        _data_tab_serv.TBL_serv2.Rows.Add(dr_se2)

        For Each aValue As String() In ObjGlobali.GestioneSessioneStampa.DatiAzioniPostStampa
            If aValue(0) = "C" Then
                dr_se1 = _data_tab_serv.TBL_serv1.NewRow
                dr_se1.SE1_chiave = aValue(1)
                dr_se1.SE1_descrizione = aValue(2)
                _data_tab_serv.TBL_serv1.Rows.Add(dr_se1)
            End If
            If aValue(0) = "A" Then
                dr_se2 = _data_tab_serv.TBL_serv2.NewRow
                dr_se2.SE2_chiave = aValue(1)
                dr_se2.SE2_descrizione = aValue(2)
                _data_tab_serv.TBL_serv2.Rows.Add(dr_se2)
            End If
        Next
        _data_tab_serv.AcceptChanges()

    End Sub


    Private Sub DataGridView1_RowValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles DataGridView1.RowValidating
        Dim dr_psa As DS_poststampa_azioni.TBL_PostStampa_AzioniRow

        dr_psa = CType(_bind.Current, DataRowView).Row
        If dr_psa.RowState = DataRowState.Added Or dr_psa.RowState = DataRowState.Modified Then
            e.Cancel = MessageBox.Show("L'azione inserita non � stata salvata. Spostandosi su un altro record si perderanno le modifiche effettuate. Procedo comunque?", "Nuova azione", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No
            If Not e.Cancel Then
                DataGridView1.BindingContext(DataGridView1.DataSource, DataGridView1.DataMember).CancelCurrentEdit()
                _InsertRow = False
            End If
        End If

    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged


    End Sub
End Class

'Public Class ValueDescriptionPair

'    Public Value As Object
'    Public Description As String

'    Public Sub New(ByVal NewValue As Object, ByVal NewDescription As String)
'        Value = NewValue
'        Description = NewDescription
'    End Sub

'    Public Overrides Function ToString() As String
'        Return Description
'    End Function

'End Class
