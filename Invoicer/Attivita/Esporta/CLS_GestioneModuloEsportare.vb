﻿Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.Invoicer.ManageStampa
Imports ISC.LibrerieComuni.InvoicerDomain.Impl
Imports ISC.LibrerieComuni.OggettiComuni

Public Class CLS_GestioneModuloEsportare

    Public Property BarcodeCodImbX As Decimal
    Public Property BarcodeCodImbY As Decimal
    Public Property CassettiStampante As Dictionary(Of Integer, CassettoEntity)
    Public Property CassettiModuli As Dictionary(Of Integer, CassettiPagineEntity)

    '
    ' Indica se il modulo è da stampare o meno
    '
    Public Property DaStampare As Boolean
    Public Property DatiPerStampa As CLS_DatiPerStampa
    'Public Property FileNameArchiviazioneOttica As Dictionary(Of Integer, NomeArchiviazioneOtticaEntity)

    '
    '
    '
    Public Property DescrizionePossoProcedere As String

    Public ReadOnly Property ListaCassettiModuli As List(Of CassettiPagineEntity)

        Get
            Dim l As List(Of CassettiPagineEntity)

            l = New List(Of CassettiPagineEntity)
            For Each k As KeyValuePair(Of Integer, CassettiPagineEntity) In CassettiModuli
                l.Add(k.Value)
            Next
            Return l
        End Get

    End Property

    Public ReadOnly Property ListaCassetti As List(Of CassettoEntity)

        Get
            Dim l As List(Of CassettoEntity)

            l = New List(Of CassettoEntity)
            For Each k As KeyValuePair(Of Integer, CassettoEntity) In CassettiStampante
                l.Add(k.Value)
            Next
            Return l
        End Get

    End Property

    'Public ReadOnly Property ListaFileNameArchiviazione As List(Of NomeArchiviazioneOtticaEntity)

    '    Get
    '        Dim l As List(Of NomeArchiviazioneOtticaEntity)

    '        l = New List(Of NomeArchiviazioneOtticaEntity)
    '        For Each k As KeyValuePair(Of Integer, NomeArchiviazioneOtticaEntity) In FileNameArchiviazioneOttica
    '            l.Add(k.Value)
    '        Next
    '        Return l
    '    End Get

    'End Property

    Public Property ArchiviazioneOttica() As Boolean

        Get
            Return DatiPerStampa.ArchiviazioneOttica
        End Get
        Set(ByVal value As Boolean)
            DatiPerStampa.ArchiviazioneOttica = value
        End Set

    End Property

    'Public ReadOnly Property CanPrintPDF As Boolean

    '    Get
    '        Return FileNameArchiviazioneOttica.Count > 1
    '    End Get

    'End Property

    Public Property CassettoDefault As CassettoEntity
    Public Property DatiStatisticheStampa As Dictionary(Of Integer, Integer)

    Public ReadOnly Property EsistonoPagineGenerare As Boolean

        Get
            Return Not (Modulo.AllegatoDaDividere = "")
        End Get

    End Property

    Public Property Folders As CLS_FileFolderDic

        Get
            Return DatiPerStampa.Folders
        End Get
        Set(ByVal value As CLS_FileFolderDic)
            DatiPerStampa.Folders = value
        End Set

    End Property

    Public Property Modulo As GestioneModulo
    Public Property NomeModulo As String
    Public Property NumeroModulo As Integer

    '
    ' Indica il nome della stampante con la quale si stamperà il modulo.
    '
    Public Property NomeStampante As String
    Public Property NoSplitAllegati() As Boolean

    Public Property ResetWrapRound As Boolean

    Public Property StampaEseguita() As Boolean

        Get
            Return DatiPerStampa.StampaEseguita
        End Get
        Set(ByVal value As Boolean)
            DatiPerStampa.StampaEseguita = value
        End Set

    End Property

    Public Property SuffissoParte As String

        Get
            Return DatiPerStampa.SuffissoParte
        End Get
        Set(ByVal value As String)
            DatiPerStampa.SuffissoParte = value
        End Set

    End Property

    'Public Property Stampa As GestioneStampa

    Public Property WrapRound() As Decimal

        Get
            Return DatiPerStampa.WrapRound
        End Get
        Set(ByVal value As Decimal)
            DatiPerStampa.WrapRound = value
        End Set

    End Property

    Public Sub New(ByVal nCodice As Integer)

        NumeroModulo = nCodice
        DatiPerStampa = New CLS_DatiPerStampa
        '  FileNameArchiviazioneOttica = New Dictionary(Of Integer, NomeArchiviazioneOtticaEntity)()
        ' Stampa = New GestioneStampa

    End Sub

    Public Sub SistemaGMS()

        '
        ' Carica l'elenco dei Nomi per l'archiviazione ottica
        '
        'FileNameArchiviazioneOttica.Add(FileNameArchiviazioneOttica.Count, New NomeArchiviazioneOtticaEntity(FileNameArchiviazioneOttica.Count, "--- Seleziona un formato per il nome per l'archiviazione ottica ---"))
        'For Each xnTmp As Xml.XmlNode In ObjGlobali.GestioneModuliStampare(1).Modulo.ArchOttFileNameList.SelectNodes("ArchOttItem")
        '    FileNameArchiviazioneOttica.Add(FileNameArchiviazioneOttica.Count, New NomeArchiviazioneOtticaEntity(FileNameArchiviazioneOttica.Count, xnTmp.SelectSingleNode("Descrizione").InnerText))
        'Next

    End Sub

    Public Sub VerificaProcedoConStampa(ByVal lPrnOttica As Boolean)

        ' ProcedoConStampa = True
        If DaStampare Then
            If lPrnOttica Then
                '
                ' Se la stampa è ottica allora verifico che sia presente almeno un tipo di nome relativo al file 
                '
                'ProcedoConStampa = Modulo.ArchOttFileNameSelect > ""
                'If Not ProcedoConStampa Then
                '    DescrizionePossoProcedere = "Errore, occorre selezionare almeno un nome per i file PDF."
                'End If
            End If
            'If ProcedoConStampa Then
            '    DescrizionePossoProcedere = "Posso procedere con la stampa del presente modulo."
            'End If
        Else
            DescrizionePossoProcedere = "Il presente modulo non è selezionato."
        End If

    End Sub

End Class
