<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_EsecuzioneAttivita
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BTN_annulla = New System.Windows.Forms.Button()
        Me.GRP_attivita = New System.Windows.Forms.GroupBox()
        Me.LBL_desc_attivita = New System.Windows.Forms.Label()
        Me.TMR_check = New System.Windows.Forms.Timer(Me.components)
        Me.GRB_tempi = New System.Windows.Forms.GroupBox()
        Me.LBL_tim_from_start = New System.Windows.Forms.Label()
        Me.LBL_tim_to_end = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GRB_spedizione = New System.Windows.Forms.GroupBox()
        Me.LBL_count_tot = New System.Windows.Forms.Label()
        Me.LBL_desc_parz = New System.Windows.Forms.Label()
        Me.LBL_desc_tot = New System.Windows.Forms.Label()
        Me.LBL_count_parz = New System.Windows.Forms.Label()
        Me.PGB_acc_inv = New System.Windows.Forms.ProgressBar()
        Me.GRB_stampa = New System.Windows.Forms.GroupBox()
        Me.LBL_modulo_elab = New System.Windows.Forms.Label()
        Me.PGB_doc_parti = New System.Windows.Forms.ProgressBar()
        Me.LBL_modulo_elab_desc = New System.Windows.Forms.Label()
        Me.LBL_par_ela = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.LBL_par_pro = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.PGB_pagine = New System.Windows.Forms.ProgressBar()
        Me.LBL_pag_ela = New System.Windows.Forms.Label()
        Me.LBL_pag_pro_desc = New System.Windows.Forms.Label()
        Me.LBL_pag_pro = New System.Windows.Forms.Label()
        Me.LBL_pag_ela_desc = New System.Windows.Forms.Label()
        Me.LBL_grp_pro_desc = New System.Windows.Forms.Label()
        Me.LBL_grp_pro = New System.Windows.Forms.Label()
        Me.LBL_grp_ela_desc = New System.Windows.Forms.Label()
        Me.PGB_gruppi = New System.Windows.Forms.ProgressBar()
        Me.LBL_grp_ela = New System.Windows.Forms.Label()
        Me.LogAttivita = New System.Windows.Forms.ListBox()
        Me.GRP_attivita.SuspendLayout()
        Me.GRB_tempi.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GRB_spedizione.SuspendLayout()
        Me.GRB_stampa.SuspendLayout()
        Me.SuspendLayout()
        '
        'BTN_annulla
        '
        Me.BTN_annulla.AutoSize = True
        Me.BTN_annulla.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_annulla.Location = New System.Drawing.Point(471, 178)
        Me.BTN_annulla.Name = "BTN_annulla"
        Me.BTN_annulla.Size = New System.Drawing.Size(462, 66)
        Me.BTN_annulla.TabIndex = 6
        Me.BTN_annulla.Text = "ANNULLA"
        Me.BTN_annulla.UseVisualStyleBackColor = True
        '
        'GRP_attivita
        '
        Me.GRP_attivita.Controls.Add(Me.LBL_desc_attivita)
        Me.GRP_attivita.Dock = System.Windows.Forms.DockStyle.Top
        Me.GRP_attivita.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GRP_attivita.Location = New System.Drawing.Point(3, 70)
        Me.GRP_attivita.Name = "GRP_attivita"
        Me.GRP_attivita.Size = New System.Drawing.Size(462, 84)
        Me.GRP_attivita.TabIndex = 6
        Me.GRP_attivita.TabStop = False
        Me.GRP_attivita.Text = "Attivit� in corso"
        '
        'LBL_desc_attivita
        '
        Me.LBL_desc_attivita.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LBL_desc_attivita.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_desc_attivita.Location = New System.Drawing.Point(3, 19)
        Me.LBL_desc_attivita.Name = "LBL_desc_attivita"
        Me.LBL_desc_attivita.Size = New System.Drawing.Size(456, 62)
        Me.LBL_desc_attivita.TabIndex = 0
        Me.LBL_desc_attivita.Text = "N/A"
        '
        'TMR_check
        '
        '
        'GRB_tempi
        '
        Me.GRB_tempi.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.GRB_tempi.Controls.Add(Me.LBL_tim_from_start)
        Me.GRB_tempi.Controls.Add(Me.LBL_tim_to_end)
        Me.GRB_tempi.Controls.Add(Me.Label4)
        Me.GRB_tempi.Controls.Add(Me.Label3)
        Me.GRB_tempi.Dock = System.Windows.Forms.DockStyle.Top
        Me.GRB_tempi.Location = New System.Drawing.Point(3, 3)
        Me.GRB_tempi.Name = "GRB_tempi"
        Me.GRB_tempi.Size = New System.Drawing.Size(462, 61)
        Me.GRB_tempi.TabIndex = 12
        Me.GRB_tempi.TabStop = False
        Me.GRB_tempi.Text = "Tempo"
        '
        'LBL_tim_from_start
        '
        Me.LBL_tim_from_start.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_tim_from_start.Location = New System.Drawing.Point(205, 16)
        Me.LBL_tim_from_start.Name = "LBL_tim_from_start"
        Me.LBL_tim_from_start.Size = New System.Drawing.Size(118, 13)
        Me.LBL_tim_from_start.TabIndex = 9
        Me.LBL_tim_from_start.Text = "N/A"
        Me.LBL_tim_from_start.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_tim_to_end
        '
        Me.LBL_tim_to_end.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_tim_to_end.Location = New System.Drawing.Point(208, 32)
        Me.LBL_tim_to_end.Name = "LBL_tim_to_end"
        Me.LBL_tim_to_end.Size = New System.Drawing.Size(115, 13)
        Me.LBL_tim_to_end.TabIndex = 10
        Me.LBL_tim_to_end.Text = "N/A"
        Me.LBL_tim_to_end.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Rimanente:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Trascorso:"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.GRB_tempi)
        Me.FlowLayoutPanel1.Controls.Add(Me.GRP_attivita)
        Me.FlowLayoutPanel1.Controls.Add(Me.GRB_spedizione)
        Me.FlowLayoutPanel1.Controls.Add(Me.GRB_stampa)
        Me.FlowLayoutPanel1.Controls.Add(Me.LogAttivita)
        Me.FlowLayoutPanel1.Controls.Add(Me.BTN_annulla)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(946, 569)
        Me.FlowLayoutPanel1.TabIndex = 13
        '
        'GRB_spedizione
        '
        Me.GRB_spedizione.Controls.Add(Me.LBL_count_tot)
        Me.GRB_spedizione.Controls.Add(Me.LBL_desc_parz)
        Me.GRB_spedizione.Controls.Add(Me.LBL_desc_tot)
        Me.GRB_spedizione.Controls.Add(Me.LBL_count_parz)
        Me.GRB_spedizione.Controls.Add(Me.PGB_acc_inv)
        Me.GRB_spedizione.Dock = System.Windows.Forms.DockStyle.Top
        Me.GRB_spedizione.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GRB_spedizione.Location = New System.Drawing.Point(3, 160)
        Me.GRB_spedizione.Name = "GRB_spedizione"
        Me.GRB_spedizione.Size = New System.Drawing.Size(462, 83)
        Me.GRB_spedizione.TabIndex = 15
        Me.GRB_spedizione.TabStop = False
        Me.GRB_spedizione.Text = "Dati invio"
        '
        'LBL_count_tot
        '
        Me.LBL_count_tot.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_count_tot.Location = New System.Drawing.Point(234, 38)
        Me.LBL_count_tot.Name = "LBL_count_tot"
        Me.LBL_count_tot.Size = New System.Drawing.Size(89, 20)
        Me.LBL_count_tot.TabIndex = 6
        Me.LBL_count_tot.Text = "0"
        Me.LBL_count_tot.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_desc_parz
        '
        Me.LBL_desc_parz.AutoSize = True
        Me.LBL_desc_parz.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_desc_parz.Location = New System.Drawing.Point(6, 19)
        Me.LBL_desc_parz.Name = "LBL_desc_parz"
        Me.LBL_desc_parz.Size = New System.Drawing.Size(200, 17)
        Me.LBL_desc_parz.TabIndex = 5
        Me.LBL_desc_parz.Text = "Email inviate ..................:"
        '
        'LBL_desc_tot
        '
        Me.LBL_desc_tot.AutoSize = True
        Me.LBL_desc_tot.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_desc_tot.Location = New System.Drawing.Point(6, 38)
        Me.LBL_desc_tot.Name = "LBL_desc_tot"
        Me.LBL_desc_tot.Size = New System.Drawing.Size(199, 17)
        Me.LBL_desc_tot.TabIndex = 0
        Me.LBL_desc_tot.Text = "Email da inviare .............:"
        '
        'LBL_count_parz
        '
        Me.LBL_count_parz.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_count_parz.Location = New System.Drawing.Point(234, 16)
        Me.LBL_count_parz.Name = "LBL_count_parz"
        Me.LBL_count_parz.Size = New System.Drawing.Size(89, 20)
        Me.LBL_count_parz.TabIndex = 3
        Me.LBL_count_parz.Text = "0"
        Me.LBL_count_parz.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'PGB_acc_inv
        '
        Me.PGB_acc_inv.Location = New System.Drawing.Point(6, 61)
        Me.PGB_acc_inv.Name = "PGB_acc_inv"
        Me.PGB_acc_inv.Size = New System.Drawing.Size(317, 10)
        Me.PGB_acc_inv.TabIndex = 4
        '
        'GRB_stampa
        '
        Me.GRB_stampa.AutoSize = True
        Me.GRB_stampa.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.GRB_stampa.Controls.Add(Me.LBL_modulo_elab)
        Me.GRB_stampa.Controls.Add(Me.PGB_doc_parti)
        Me.GRB_stampa.Controls.Add(Me.LBL_modulo_elab_desc)
        Me.GRB_stampa.Controls.Add(Me.LBL_par_ela)
        Me.GRB_stampa.Controls.Add(Me.Label11)
        Me.GRB_stampa.Controls.Add(Me.LBL_par_pro)
        Me.GRB_stampa.Controls.Add(Me.Label9)
        Me.GRB_stampa.Controls.Add(Me.PGB_pagine)
        Me.GRB_stampa.Controls.Add(Me.LBL_pag_ela)
        Me.GRB_stampa.Controls.Add(Me.LBL_pag_pro_desc)
        Me.GRB_stampa.Controls.Add(Me.LBL_pag_pro)
        Me.GRB_stampa.Controls.Add(Me.LBL_pag_ela_desc)
        Me.GRB_stampa.Controls.Add(Me.LBL_grp_pro_desc)
        Me.GRB_stampa.Controls.Add(Me.LBL_grp_pro)
        Me.GRB_stampa.Controls.Add(Me.LBL_grp_ela_desc)
        Me.GRB_stampa.Controls.Add(Me.PGB_gruppi)
        Me.GRB_stampa.Controls.Add(Me.LBL_grp_ela)
        Me.GRB_stampa.Dock = System.Windows.Forms.DockStyle.Top
        Me.GRB_stampa.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GRB_stampa.Location = New System.Drawing.Point(3, 249)
        Me.GRB_stampa.Name = "GRB_stampa"
        Me.GRB_stampa.Size = New System.Drawing.Size(462, 269)
        Me.GRB_stampa.TabIndex = 17
        Me.GRB_stampa.TabStop = False
        Me.GRB_stampa.Text = "Dati stampa"
        '
        'LBL_modulo_elab
        '
        Me.LBL_modulo_elab.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LBL_modulo_elab.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_modulo_elab.Location = New System.Drawing.Point(217, 90)
        Me.LBL_modulo_elab.Name = "LBL_modulo_elab"
        Me.LBL_modulo_elab.Size = New System.Drawing.Size(239, 17)
        Me.LBL_modulo_elab.TabIndex = 15
        Me.LBL_modulo_elab.Text = "N/A"
        '
        'PGB_doc_parti
        '
        Me.PGB_doc_parti.Location = New System.Drawing.Point(9, 224)
        Me.PGB_doc_parti.Maximum = 4
        Me.PGB_doc_parti.Name = "PGB_doc_parti"
        Me.PGB_doc_parti.Size = New System.Drawing.Size(447, 23)
        Me.PGB_doc_parti.Step = 1
        Me.PGB_doc_parti.TabIndex = 5
        '
        'LBL_modulo_elab_desc
        '
        Me.LBL_modulo_elab_desc.AutoSize = True
        Me.LBL_modulo_elab_desc.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_modulo_elab_desc.Location = New System.Drawing.Point(6, 90)
        Me.LBL_modulo_elab_desc.Name = "LBL_modulo_elab_desc"
        Me.LBL_modulo_elab_desc.Size = New System.Drawing.Size(207, 17)
        Me.LBL_modulo_elab_desc.TabIndex = 14
        Me.LBL_modulo_elab_desc.Text = "Modulo in elaborazione ....:"
        '
        'LBL_par_ela
        '
        Me.LBL_par_ela.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_par_ela.Location = New System.Drawing.Point(367, 183)
        Me.LBL_par_ela.Name = "LBL_par_ela"
        Me.LBL_par_ela.Size = New System.Drawing.Size(89, 17)
        Me.LBL_par_ela.TabIndex = 13
        Me.LBL_par_ela.Text = "N/A"
        Me.LBL_par_ela.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(6, 201)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(202, 17)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Parte in stampa ..............:"
        '
        'LBL_par_pro
        '
        Me.LBL_par_pro.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_par_pro.Location = New System.Drawing.Point(367, 201)
        Me.LBL_par_pro.Name = "LBL_par_pro"
        Me.LBL_par_pro.Size = New System.Drawing.Size(89, 17)
        Me.LBL_par_pro.TabIndex = 11
        Me.LBL_par_pro.Text = "N/A"
        Me.LBL_par_pro.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 183)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(203, 17)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Parti del documento ........:"
        '
        'PGB_pagine
        '
        Me.PGB_pagine.Location = New System.Drawing.Point(9, 155)
        Me.PGB_pagine.Name = "PGB_pagine"
        Me.PGB_pagine.Size = New System.Drawing.Size(447, 23)
        Me.PGB_pagine.TabIndex = 9
        '
        'LBL_pag_ela
        '
        Me.LBL_pag_ela.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_pag_ela.Location = New System.Drawing.Point(367, 110)
        Me.LBL_pag_ela.Name = "LBL_pag_ela"
        Me.LBL_pag_ela.Size = New System.Drawing.Size(89, 17)
        Me.LBL_pag_ela.TabIndex = 8
        Me.LBL_pag_ela.Text = "N/A"
        Me.LBL_pag_ela.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_pag_pro_desc
        '
        Me.LBL_pag_pro_desc.AutoSize = True
        Me.LBL_pag_pro_desc.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_pag_pro_desc.Location = New System.Drawing.Point(7, 131)
        Me.LBL_pag_pro_desc.Name = "LBL_pag_pro_desc"
        Me.LBL_pag_pro_desc.Size = New System.Drawing.Size(204, 17)
        Me.LBL_pag_pro_desc.TabIndex = 7
        Me.LBL_pag_pro_desc.Text = "Numero pagine stampate .:"
        '
        'LBL_pag_pro
        '
        Me.LBL_pag_pro.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_pag_pro.Location = New System.Drawing.Point(367, 130)
        Me.LBL_pag_pro.Name = "LBL_pag_pro"
        Me.LBL_pag_pro.Size = New System.Drawing.Size(89, 17)
        Me.LBL_pag_pro.TabIndex = 6
        Me.LBL_pag_pro.Text = "N/A"
        Me.LBL_pag_pro.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_pag_ela_desc
        '
        Me.LBL_pag_ela_desc.AutoSize = True
        Me.LBL_pag_ela_desc.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_pag_ela_desc.Location = New System.Drawing.Point(6, 110)
        Me.LBL_pag_ela_desc.Name = "LBL_pag_ela_desc"
        Me.LBL_pag_ela_desc.Size = New System.Drawing.Size(204, 17)
        Me.LBL_pag_ela_desc.TabIndex = 5
        Me.LBL_pag_ela_desc.Text = "Numero pagine gruppo ....:"
        '
        'LBL_grp_pro_desc
        '
        Me.LBL_grp_pro_desc.AutoSize = True
        Me.LBL_grp_pro_desc.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_grp_pro_desc.Location = New System.Drawing.Point(6, 44)
        Me.LBL_grp_pro_desc.Name = "LBL_grp_pro_desc"
        Me.LBL_grp_pro_desc.Size = New System.Drawing.Size(204, 17)
        Me.LBL_grp_pro_desc.TabIndex = 0
        Me.LBL_grp_pro_desc.Text = "Gruppo in elaborazione ...:"
        '
        'LBL_grp_pro
        '
        Me.LBL_grp_pro.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LBL_grp_pro.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_grp_pro.Location = New System.Drawing.Point(367, 42)
        Me.LBL_grp_pro.Name = "LBL_grp_pro"
        Me.LBL_grp_pro.Size = New System.Drawing.Size(89, 17)
        Me.LBL_grp_pro.TabIndex = 3
        Me.LBL_grp_pro.Text = "N/A"
        Me.LBL_grp_pro.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_grp_ela_desc
        '
        Me.LBL_grp_ela_desc.AutoSize = True
        Me.LBL_grp_ela_desc.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_grp_ela_desc.Location = New System.Drawing.Point(6, 22)
        Me.LBL_grp_ela_desc.Name = "LBL_grp_ela_desc"
        Me.LBL_grp_ela_desc.Size = New System.Drawing.Size(205, 17)
        Me.LBL_grp_ela_desc.TabIndex = 1
        Me.LBL_grp_ela_desc.Text = "Gruppi da elaborare ........:"
        '
        'PGB_gruppi
        '
        Me.PGB_gruppi.Location = New System.Drawing.Point(9, 64)
        Me.PGB_gruppi.Name = "PGB_gruppi"
        Me.PGB_gruppi.Size = New System.Drawing.Size(447, 23)
        Me.PGB_gruppi.TabIndex = 4
        '
        'LBL_grp_ela
        '
        Me.LBL_grp_ela.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LBL_grp_ela.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_grp_ela.Location = New System.Drawing.Point(367, 22)
        Me.LBL_grp_ela.Name = "LBL_grp_ela"
        Me.LBL_grp_ela.Size = New System.Drawing.Size(89, 17)
        Me.LBL_grp_ela.TabIndex = 2
        Me.LBL_grp_ela.Text = "N/A"
        Me.LBL_grp_ela.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LogAttivita
        '
        Me.LogAttivita.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.LogAttivita.FormattingEnabled = True
        Me.LogAttivita.IntegralHeight = False
        Me.LogAttivita.Location = New System.Drawing.Point(471, 3)
        Me.LogAttivita.Name = "LogAttivita"
        Me.LogAttivita.Size = New System.Drawing.Size(462, 169)
        Me.LogAttivita.TabIndex = 16
        '
        'FRM_EsecuzioneAttivita
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(946, 569)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FRM_EsecuzioneAttivita"
        Me.Text = "Stampa in corso"
        Me.GRP_attivita.ResumeLayout(False)
        Me.GRB_tempi.ResumeLayout(False)
        Me.GRB_tempi.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.GRB_spedizione.ResumeLayout(False)
        Me.GRB_spedizione.PerformLayout()
        Me.GRB_stampa.ResumeLayout(False)
        Me.GRB_stampa.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BTN_annulla As System.Windows.Forms.Button
    Friend WithEvents GRP_attivita As System.Windows.Forms.GroupBox
    Friend WithEvents LBL_desc_attivita As System.Windows.Forms.Label
    Friend WithEvents TMR_check As System.Windows.Forms.Timer
    Friend WithEvents GRB_tempi As System.Windows.Forms.GroupBox
    Friend WithEvents LBL_tim_from_start As System.Windows.Forms.Label
    Friend WithEvents LBL_tim_to_end As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents GRB_spedizione As System.Windows.Forms.GroupBox
    Friend WithEvents LBL_count_tot As System.Windows.Forms.Label
    Friend WithEvents LBL_desc_parz As System.Windows.Forms.Label
    Friend WithEvents LBL_desc_tot As System.Windows.Forms.Label
    Friend WithEvents LBL_count_parz As System.Windows.Forms.Label
    Friend WithEvents PGB_acc_inv As System.Windows.Forms.ProgressBar
    Friend WithEvents LogAttivita As System.Windows.Forms.ListBox
    Friend WithEvents GRB_stampa As System.Windows.Forms.GroupBox
    Friend WithEvents LBL_modulo_elab As System.Windows.Forms.Label
    Friend WithEvents PGB_doc_parti As System.Windows.Forms.ProgressBar
    Friend WithEvents LBL_modulo_elab_desc As System.Windows.Forms.Label
    Friend WithEvents LBL_par_ela As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents LBL_par_pro As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents PGB_pagine As System.Windows.Forms.ProgressBar
    Friend WithEvents LBL_pag_ela As System.Windows.Forms.Label
    Friend WithEvents LBL_pag_pro_desc As System.Windows.Forms.Label
    Friend WithEvents LBL_pag_pro As System.Windows.Forms.Label
    Friend WithEvents LBL_pag_ela_desc As System.Windows.Forms.Label
    Friend WithEvents LBL_grp_pro_desc As System.Windows.Forms.Label
    Friend WithEvents LBL_grp_pro As System.Windows.Forms.Label
    Friend WithEvents LBL_grp_ela_desc As System.Windows.Forms.Label
    Friend WithEvents PGB_gruppi As System.Windows.Forms.ProgressBar
    Friend WithEvents LBL_grp_ela As System.Windows.Forms.Label
End Class
