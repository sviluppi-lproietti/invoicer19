﻿Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita

Public Class CLS_GestioneProcessoStampa

    Public Property Anteprima As Boolean
    Public Property ArchiviazioneOttica As Boolean
    Public Property CodiciModuliDaStampare As ArrayList
    Public Property CodiciModuliDaEsportare As ArrayList
    Public Property FastPrintEnabled As Boolean
    Public Property FirstRecordPrinted() As Integer
    Public Property FondiModuli As Boolean
    Public Property ForceStampaCartaceo As Boolean
    Public Property MaxNumFogli As Integer

    '
    ' Indica il numero del gruppo dei documenti in stampare
    '
    Public Property GruppoInStampa As Integer

    '
    ' Indica il numero di gruppi di documenti da stampare
    '
    Public Property GruppiDaStampare As Integer

        Get
            Return _GruppiDaStampare
        End Get
        Set(ByVal value As Integer)
            _GruppiDaStampare = value
            AggiornaDatiDiStampa()
        End Set

    End Property

    Public Property LastRecordPrinted() As Integer

    '
    ' Indica il numero di documenti per giascun gruppo
    '
    Public Property NumeroDocumenti As Integer

        Get
            Return _NumeroDocumenti
        End Get
        Set(ByVal value As Integer)
            _NumeroDocumenti = value
            AggiornaDatiDiStampa()
        End Set

    End Property

    Public ReadOnly Property NumeroModuliDaStampare As Integer

        Get
            Return CodiciModuliDaStampare.Count
        End Get

    End Property

    Public ReadOnly Property NumeroModuliDaEsportare As Integer

        Get
            Return CodiciModuliDaEsportare.Count
        End Get

    End Property

    Public Property PulisciFolderElaborati As Boolean
    Public Property TipologiaStampa As Integer
    Public Property TokenElaborazione As String

    Private _GruppiDaStampare As Integer
    Private _NumeroDocumenti As Integer

    Public Sub New()

        GruppiDaStampare = -1
        NumeroDocumenti = -1
        CodiciModuliDaStampare = New ArrayList
        CodiciModuliDaEsportare = New ArrayList

    End Sub

    Private Sub AggiornaDatiDiStampa()

        If GruppiDaStampare > -1 And NumeroDocumenti > -1 Then
            _GruppoInStampa = 0
            If (_GruppiDaStampare Mod _NumeroDocumenti) = 0 Then
                _GruppiDaStampare = Int(_GruppiDaStampare / _NumeroDocumenti)
            Else
                _GruppiDaStampare = Int(_GruppiDaStampare / _NumeroDocumenti) + 1
            End If
        End If

    End Sub

    Public Sub GeneraTokenElaborazione()

        TokenElaborazione = Timestamp()

    End Sub

    Public Property FirstRecord2Manage As Integer

End Class
