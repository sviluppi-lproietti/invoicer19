Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.xml
Imports iTextSharp.text.pdf.parser
Imports System.Xml
Imports System.Linq
Imports ISC.Invoicer.ManageStampa
Imports ISC.Invoicer.ManageStampa.MST_enumeratori
Imports ISC.LibrerieComuni.ManageAccessoDati
Imports ISC.Invoicer.ManageFusione
Imports ISC.LibrerieComuni.InvoicerDomain.Impl
Imports ISC.LibrerieComuni.ManageSessione
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Public Class FRM_EsecuzioneAttivita

    Public Shared logger As log4net.ILog

    Public Enum eStepAttivita
        STA_0_00_NUL_attivita = 0
        STA_0_01_INI_Attivita = 1
        STA_0_01_CHK_tipo_ope = 2

        '
        ' Attivita per la stampa in proprio
        '
        STA_1_01_INI_stampa_P = 3
        STA_2_01_INI_oper_pre = 4
        STA_2_02_RST_cont_mod = 5
        STA_2_03_NXT_cont_mod = 6
        STA_2_04_EXT_cont_mod = 7
        STA_2_05_CHK_pag_gene = 8
        STA_2_06_COU_paggener = 9
        STA_2_07_SPL_allegato = 10
        STA_2_08_BUI_prntrans = 11
        STA_2_09_TRA_allegato = 12
        STA_2_10_DES_prntrans = 13
        STA_2_11_BUI_pagineal = 14
        STA_2_12_LOO_cont_mod = 15
        STA_2_13_FIN_oper_pre = 16

        STA_3_01_INI_prstampa = 17
        STA_3_02_RST_cont_grp = 18
        STA_3_03_NXT_cont_grp = 19
        STA_3_04_EXT_cont_grp = 20
        STA_3_05_RST_cont_mod = 21
        STA_3_06_NXT_cont_mod = 22
        STA_3_07_EXT_cont_mod = 23
        STA_3_08_LOA_database = 24
        STA_3_09_CHK_consisDB = 25
        STA_3_10_ONL_stampaca = 26
        STA_3_11_CHK_consisDB = 27


        STA_3_22_RST_SettaSintetico_Dettaglio = 28
        STA_3_23_NXT_SettaSintetico_Dettaglio = 29
        STA_3_24_EXT_SettaSintetico_Dettaglio = 30
        STA_3_18_REM_data_sbm = 31
        STA_3_12_PRE_stampa = 32
        STA_3_13_GET_previewD = 33
        STA_3_14_ANT_document = 34
        STA_3_15_SAV_previewD = 35
        STA_3_16_CHK_consisDB = 36
        STA_3_17_PostAnteprima = 37
        STA_3_19_CHK_consisDB = 38

        STA_3_20_PRE_stampa = 39
        STA_3_22_RST_cont_par = 40
        STA_3_23_NXT_cont_par = 41
        STA_3_24_EXT_cont_par = 42
        STA_3_25_CHK_pdf_prin = 43
        STA_3_26_SET_pdf_prin = 44
        STA_3_28_StampaModulo = 45
        STA_3_29_CHK_consisDB = 46
        STA_3_30_CHK_pdf_prin = 47
        STA_3_31_WAI_pdf_prin = 48
        STA_3_32_CHK_paginepa = 49
        STA_3_32_BUI_paginepa = 50
        STA_3_33_RES_pdf_prin = 51
        'STA_3_34_SetUpMascheraDoc_out = 49
        STA_3_35_SaveSendByMail = 52
        STA_3_36_PostStampa = 53
        STA_3_39_LOO_cont_par = 54
        STA_3_40_MER_parti = 55
        STA_3_XX_LOO_SettaSintetico_Dettaglio = 56

        STA_3_40_LOO_cont_mod = 57


        STA_4_10_CHK_fusi_nee = 58
        STA_4_11_INI_pfusione = 59
        STA_4_12_FUS_fondidoc = 60
        STA_4_13_SaveSendByMail = 61
        STA_4_14_FIN_pfusione = 62
        STA_3_41_LOO_cont_grp = 63
        STA_1_15_FIN_stampa_P = 64

        '
        ' Attivita per la stampa con documenti pregenerati
        '
        STA_5_01_INI_stampa_G = 65
        STA_5_02_CLE_folderEl = 66
        STA_5_03_RST_cont_grp = 67
        STA_5_04_NXT_cont_grp = 68
        STA_5_05_EXT_cont_grp = 69
        STA_5_06_LOA_database = 70
        STA_5_07_CHK_consisDB = 71
        STA_5_08_BUI_tokenCod = 72
        STA_5_09_INS_BarcodeI = 73
        STA_5_10_BUI_fileStam = 74
        STA_5_11_LOO_cont_grp = 75
        STA_5_12_FIN_stampa_G = 76

        '
        ' Attivita per l'invio dei documenti tramite mail
        '
        STA_6_01_INI_send_mai = 77
        STA_6_02_OPERAZIONI_INVIO = 78
        STA_6_03_FIN_send_mai = 79

        '
        ' Attivit� di chiusura.
        '
        STA_0_02_FFO_Attivita = 80
        STA_0_03_FIN_Attivita = 81
    End Enum

    Public Enum eTipoAttivita
        Stampa = 1
        InvioMail = 2
    End Enum

    Public Property ForcedExit As Boolean
    Public Property TipoAttivita As eTipoAttivita

    Public Property DatiFusione As ISC.Invoicer.ManageFusione.CLS_DatiPerFusione

    Private Fusione As CLS_motore_fusione

    'Private Property _StampaSinteticoDettaglio As Boolean
    'Private Property _LoopSintDett As Integer
    '
    ' Variabili da sostituire progressivamente integrandoli in CLS_GestioneStampaModuli
    '
    Public Property GMS As CLS_GestioneModuloStampare

        Get
            Dim m As CLS_GestioneModuloStampare

            If (0 <= _NumModulo) And (_NumModulo < GPS.CodiciModuliDaStampare.Count) Then
                m = ObjGlobali.GestioneModuliStampare(GPS.CodiciModuliDaStampare(_NumModulo))
            Else
                m = Nothing
            End If
            Return m
        End Get
        Set(ByVal value As CLS_GestioneModuloStampare)
            ObjGlobali.GestioneModuliStampare(GPS.CodiciModuliDaStampare(_NumModulo)) = value
        End Set

    End Property

    Public Property GPS As CLS_GestioneProcessoStampa

        Get
            Return ObjGlobali.GestioneProcessoStampa
        End Get
        Set(ByVal value As CLS_GestioneProcessoStampa)
            ObjGlobali.GestioneProcessoStampa = value
        End Set

    End Property

    Public Property GSS As GestioneSessione

        Get
            Return ObjGlobali.GestioneSessioneStampa
        End Get
        Set(ByVal value As GestioneSessione)
            ObjGlobali.GestioneSessioneStampa = value
        End Set

    End Property

    'Private _FastPrintEnabled As Boolean
    Private _NumModulo As Integer
    Private _dTimeStart As DateTime
    Private _Thr2Exec As System.Threading.Thread
    Private _log_ope As CLS_logger
    Private _prnPDF As CLS_PDFCreator
    Private _documentiDaFondere1 As ArrayList
    Private _AttivitaEsecuzione As eStepAttivita
    Private _LastAttivitaEsecuzione As eStepAttivita
    'Private _FileNameUnificato As String

    Private ReadOnly Property _DescrizioneAttivitaEsecuzione

        Get
            Dim cRtn As String

            cRtn = ""
            Select Case _AttivitaEsecuzione
                Case Is = eStepAttivita.STA_0_01_INI_Attivita
                    cRtn = "Inizializzazione attivit� prescelta"
                Case Is = eStepAttivita.STA_0_01_CHK_tipo_ope
                    cRtn = "Preparazione percorso da svolgere"
                    '
                    ' Attivita per la stampa in proprio
                    '
                Case Is = eStepAttivita.STA_1_01_INI_stampa_P
                    cRtn = "Inizio procedura di stampa documenti generati in proprio"
                Case Is = eStepAttivita.STA_2_01_INI_oper_pre
                    cRtn = "Inizio attivit� preliminari"
                Case Is = eStepAttivita.STA_2_02_RST_cont_mod, Is = eStepAttivita.STA_3_05_RST_cont_mod
                    cRtn = "Reset contatore moduli"
                Case Is = eStepAttivita.STA_2_03_NXT_cont_mod, Is = eStepAttivita.STA_3_06_NXT_cont_mod
                    cRtn = "Avanzamento contatore moduli"
                Case Is = eStepAttivita.STA_2_04_EXT_cont_mod, Is = eStepAttivita.STA_3_07_EXT_cont_mod
                    cRtn = "Verifica esistenza modulo"
                Case Is = eStepAttivita.STA_2_05_CHK_pag_gene
                    cRtn = "Verifica presenza pagine da generare"
                Case Is = eStepAttivita.STA_2_06_COU_paggener
                Case Is = eStepAttivita.STA_2_07_SPL_allegato
                Case Is = eStepAttivita.STA_2_08_BUI_prntrans
                Case Is = eStepAttivita.STA_2_09_TRA_allegato
                Case Is = eStepAttivita.STA_2_10_DES_prntrans
                Case Is = eStepAttivita.STA_2_11_BUI_pagineal
                Case Is = eStepAttivita.STA_2_12_LOO_cont_mod, Is = eStepAttivita.STA_3_40_LOO_cont_mod
                    cRtn = "Fine ciclo elaborazione moduli"
                Case Is = eStepAttivita.STA_2_13_FIN_oper_pre
                    cRtn = "Fine attivit� preliminari"
                Case Is = eStepAttivita.STA_3_01_INI_prstampa
                    cRtn = "Inizio attivit� di stampa"
                Case Is = eStepAttivita.STA_3_08_LOA_database, Is = eStepAttivita.STA_5_06_LOA_database
                    cRtn = "Caricamento dati da utilizzare per la stampa"
                Case Is = eStepAttivita.STA_3_09_CHK_consisDB, Is = eStepAttivita.STA_3_11_CHK_consisDB, Is = eStepAttivita.STA_3_16_CHK_consisDB, _
                     Is = eStepAttivita.STA_3_19_CHK_consisDB, Is = eStepAttivita.STA_3_29_CHK_consisDB, Is = eStepAttivita.STA_5_07_CHK_consisDB
                    cRtn = "Verifica presenza record su gruppo stampa"
                Case Is = eStepAttivita.STA_3_10_ONL_stampaca
                    cRtn = "Rimozione record da non stampare in formato cartaceo."
                Case Is = eStepAttivita.STA_3_13_GET_previewD
                    cRtn = "Recupero dati di anteprima."
                Case Is = eStepAttivita.STA_3_12_PRE_stampa
                    cRtn = "Prepara ambiente di stampa per la stampa (Anteprima)"
                Case Is = eStepAttivita.STA_3_20_PRE_stampa
                    cRtn = "Prepara ambiente di stampa per la stampa (Effettiva)"
                Case Is = eStepAttivita.STA_3_14_ANT_document
                    cRtn = "Creazione anteprima modulo"
                Case Is = eStepAttivita.STA_3_17_PostAnteprima
                    cRtn = "Esecuzione operazioni post anteprima"
                Case Is = eStepAttivita.STA_3_18_REM_data_sbm
                    cRtn = "Rimuovi dati per invio via mail"
                Case Is = eStepAttivita.STA_3_25_CHK_pdf_prin, Is = eStepAttivita.STA_3_30_CHK_pdf_prin
                    cRtn = "verifica necesit� di stampa PDF"
                Case Is = eStepAttivita.STA_3_22_RST_cont_par
                    If GMS.DatiPerStampa.ContatoriPagine.PartiDocumento = 1 Then
                        cRtn = LBL_desc_attivita.Text
                        cRtn = cRtn.Substring(0, cRtn.LastIndexOf("(") - 1)
                    Else
                        cRtn = "Reset contatore parti in stampa"
                    End If
                Case Is = eStepAttivita.STA_3_23_NXT_cont_par
                    If GMS.DatiPerStampa.ContatoriPagine.PartiDocumento = 1 Then
                        cRtn = LBL_desc_attivita.Text
                        cRtn = cRtn.Substring(0, cRtn.LastIndexOf("(") - 1)
                    Else
                        cRtn = "Avanzamento contatore parte in stampa"
                    End If
                Case Is = eStepAttivita.STA_3_24_EXT_cont_par
                    If GMS.DatiPerStampa.ContatoriPagine.PartiDocumento = 1 Then
                        cRtn = LBL_desc_attivita.Text
                        cRtn = cRtn.Substring(0, cRtn.LastIndexOf("(") - 1)
                    Else
                        cRtn = "Controllo esistenza parte in stampa"
                    End If
                Case Is = eStepAttivita.STA_3_26_SET_pdf_prin
                    cRtn = "Predisposizione per stampante ottica"
                Case Is = eStepAttivita.STA_3_15_SAV_previewD
                    cRtn = "Salvataggio dati di anteprima"
                    ' Case Is = eStepAttivita.STA_3_27_BuildUpPrinter
                Case Is = eStepAttivita.STA_3_28_StampaModulo

                Case Is = eStepAttivita.STA_3_31_WAI_pdf_prin
                    cRtn = "Attesa produzine documento elettronico"
                Case Is = eStepAttivita.STA_3_32_CHK_paginepa
                    cRtn = "Verifica necessit� di rendere pari le pagine"
                Case Is = eStepAttivita.STA_3_32_BUI_paginepa
                    cRtn = "Verifica che le pagine del documento prodotto siano pari, altrimente le fa diventare pari"
                Case Is = eStepAttivita.STA_3_33_RES_pdf_prin
                    cRtn = "Restore parametri stampante ottica."
                Case Is = eStepAttivita.STA_3_35_SaveSendByMail, Is = eStepAttivita.STA_4_13_SaveSendByMail
                    cRtn = "Salva dati per invio tramite mail."
                Case Is = eStepAttivita.STA_3_36_PostStampa
                    'Case Is = eStepAttivita.STA_3_38_Check_FineStampaGruppo
                Case Is = eStepAttivita.STA_3_39_LOO_cont_par
                    If GMS.DatiPerStampa.ContatoriPagine.PartiDocumento = 1 Then
                        cRtn = LBL_desc_attivita.Text
                        cRtn = cRtn.Substring(0, cRtn.LastIndexOf("(") - 1)
                    Else
                        cRtn = "Fine attivit� di stampa parte"
                    End If
                Case Is = eStepAttivita.STA_4_10_CHK_fusi_nee
                    cRtn = "Verifica necessit� fusione moduli"
                Case Is = eStepAttivita.STA_4_11_INI_pfusione
                Case Is = eStepAttivita.STA_4_12_FUS_fondidoc
                    cRtn = "Procedi con l'attivit� di fusione dei documenti"
                Case Is = eStepAttivita.STA_4_14_FIN_pfusione
                Case Is = eStepAttivita.STA_3_40_LOO_cont_mod
                    cRtn = "Fine attivit� di stampa"
                Case Is = eStepAttivita.STA_1_15_FIN_stampa_P
                    cRtn = "Fine procedura di stampa documenti generati in proprio"

                    '
                    ' Attivita per la stampa con documenti pregenerati
                    '
                Case Is = eStepAttivita.STA_5_01_INI_stampa_G
                    cRtn = "Inizio procedura di stampa dei documenti pregenerati"
                Case Is = eStepAttivita.STA_5_02_CLE_folderEl
                    cRtn = "Pulisci cartella Elaborati"
                Case Is = eStepAttivita.STA_3_02_RST_cont_grp, Is = eStepAttivita.STA_5_03_RST_cont_grp
                    cRtn = "Reset contatore gruppi stampa"
                Case Is = eStepAttivita.STA_3_03_NXT_cont_grp, Is = eStepAttivita.STA_5_04_NXT_cont_grp
                    cRtn = "Avanzamento contatore gruppi stampa"
                Case Is = eStepAttivita.STA_3_04_EXT_cont_grp, Is = eStepAttivita.STA_5_05_EXT_cont_grp
                    cRtn = "Verifica esistenza gruppo stampa"
                Case Is = eStepAttivita.STA_5_08_BUI_tokenCod
                    cRtn = "Creazione token stampa"
                Case Is = eStepAttivita.STA_5_09_INS_BarcodeI
                    cRtn = "Inserimento barcode imbustamento su documenti"
                Case Is = eStepAttivita.STA_5_10_BUI_fileStam
                    cRtn = "Unione documenti per gruppo di stampa"
                Case Is = eStepAttivita.STA_5_11_LOO_cont_grp
                    cRtn = "Fine ciclo stampa"
                Case Is = eStepAttivita.STA_5_12_FIN_stampa_G
                    cRtn = "Fine procedura di stampa dei documenti pregenerati"
                    '
                    ' Attivita per l'invio dei documenti tramite mail
                    '
                Case Is = eStepAttivita.STA_6_01_INI_send_mai
                Case Is = eStepAttivita.STA_6_02_OPERAZIONI_INVIO
                Case Is = eStepAttivita.STA_6_03_FIN_send_mai

                    '
                    ' Attivit� di chiusura.
                    '
                Case Is = eStepAttivita.STA_0_02_FFO_Attivita
                    cRtn = "Chiusura forzata dell'applicazione"
                Case Is = eStepAttivita.STA_0_03_FIN_Attivita
                    cRtn = "Termine attivit� prescelta"
            End Select
            If cRtn = "" Then
                cRtn = _AttivitaEsecuzione.ToString()
            End If

            Return String.Concat(cRtn, " (", CInt(_AttivitaEsecuzione), ").")
        End Get

    End Property

    Public Sub New()

        ' Chiamata richiesta dalla finestra di progettazione.
        InitializeComponent()

        ' Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent().

        _AttivitaEsecuzione = eStepAttivita.STA_0_01_INI_Attivita
        _LastAttivitaEsecuzione = eStepAttivita.STA_0_00_NUL_attivita
        _NumModulo = -1

        '
        ' Abilita di default la modalit� di FastPrint, recupero delle pagine di anteprima dal file di preview
        '
        ' _FastPrintEnabled = True

    End Sub

    Private Sub FRM_EsecuzioneAttivita_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        _log_ope = New CLS_logger(ObjGlobali.Folders("LogsFLD"), ObjGlobali.CFG.LogLevel, ObjGlobali.CFG.LogFileSize, "EsecuzioneAttivita")
        SetUpMaschera()

    End Sub

    Private Sub FRM_printing_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        TMR_check.Start()

    End Sub

    Private Sub FRM_printing_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        If Not _Thr2Exec Is Nothing Then
            While _Thr2Exec.IsAlive
                System.Threading.Thread.Sleep(200)
            End While
        End If

    End Sub

    Private Sub BTN_annulla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_annulla.Click

        ForcedExit = True

    End Sub

    Private Sub TMR_check_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMR_check.Tick

        If _Thr2Exec Is Nothing Then
            ForcedExit = False
            _dTimeStart = Now
            _Thr2Exec = New System.Threading.Thread(New System.Threading.ThreadStart(AddressOf EsecuzioneAttivita))
            _Thr2Exec.Start()
        Else
            If _Thr2Exec.IsAlive Then
                SetUpMaschera()
                LBL_tim_from_start.Text = FormattaOrario(Now - _dTimeStart)
                If ForcedExit Then
                    _Thr2Exec.Abort()
                End If
            Else
                _Thr2Exec = Nothing
                Me.Close()
            End If
        End If

    End Sub

    Private Sub SetUpMaschera()
        Dim lVisible As Boolean
        If _AttivitaEsecuzione = eStepAttivita.STA_0_01_INI_Attivita Then
            Me.Text = "Inizializzazione attivit�"
            Visibilit�GRBStampa(False)
            Visibilit�StampaPagine()
            Me.GRB_spedizione.Visible = False
            Visibilit�StampaParti(False)
        ElseIf _AttivitaEsecuzione >= eStepAttivita.STA_1_01_INI_stampa_P And _AttivitaEsecuzione <= eStepAttivita.STA_1_15_FIN_stampa_P Then
            Me.Text = "Stampa documenti in proprio"
            Visibilit�GRBStampa(True)
            Visibilit�StampaPagine()
        ElseIf _AttivitaEsecuzione >= eStepAttivita.STA_5_01_INI_stampa_G And _AttivitaEsecuzione <= eStepAttivita.STA_5_12_FIN_stampa_G Then
            Me.Text = "Stampa documenti pregenerati"
            Visibilit�GRBStampa(True)
        ElseIf _AttivitaEsecuzione >= eStepAttivita.STA_6_01_INI_send_mai And _AttivitaEsecuzione <= eStepAttivita.STA_6_03_FIN_send_mai Then
            Me.Text = "Invio documenti tramite mail"
            Me.GRB_spedizione.Visible = True
        End If

        '
        ' Inserimento descrizione.
        '
        If _LastAttivitaEsecuzione <> _AttivitaEsecuzione Then
            LBL_desc_attivita.Text = _DescrizioneAttivitaEsecuzione
            If GPS.GruppoInStampa > 0 Then
                LBL_tim_to_end.Text = FormattaOrario(TimeSpan.FromSeconds((Now - _dTimeStart).TotalSeconds / GPS.GruppoInStampa * (GPS.GruppiDaStampare - GPS.GruppoInStampa)))
            End If
            InserisciLogAttivita()
            _LastAttivitaEsecuzione = _AttivitaEsecuzione
        End If
        If GRB_stampa.Visible Then
            '
            ' Visibilit� informazioni gruppi di stampa
            '
            lVisible = GPS.GruppoInStampa > -1
            LBL_grp_ela_desc.Visible = lVisible
            LBL_grp_pro_desc.Visible = lVisible
            LBL_grp_ela.Visible = lVisible
            LBL_grp_pro.Visible = lVisible
            PGB_gruppi.Visible = lVisible
            If lVisible Then
                If GPS.GruppoInStampa < GPS.GruppiDaStampare Then
                    LBL_grp_pro.Text = GPS.GruppoInStampa + 1
                End If
                PGB_gruppi.Value = GPS.GruppoInStampa
                If GMS IsNot Nothing Then
                    LBL_pag_pro.Text = GMS.DatiPerStampa.ProgressivoPagine
                    If LBL_pag_ela.Visible And (GMS.DatiPerStampa.TotalePagine > -1) Then
                        PGB_pagine.Maximum = GMS.DatiPerStampa.TotalePagine
                        PGB_pagine.Value = GMS.DatiPerStampa.ProgressivoPagine
                    End If
                End If
            End If

            '
            ' Imposta la descrizione e la visibilit� del campo modulo
            '
            If GMS Is Nothing Then
                LBL_modulo_elab.Text = ""
            Else
                LBL_modulo_elab.Text = GMS.NomeModulo
            End If
            LBL_modulo_elab_desc.Visible = GMS IsNot Nothing
            LBL_modulo_elab.Visible = LBL_modulo_elab_desc.Visible

            '
            ' Visibilit� dei dati di stampa in parti.
            '
            If _AttivitaEsecuzione > eStepAttivita.STA_3_24_EXT_cont_par And _AttivitaEsecuzione <= eStepAttivita.STA_3_39_LOO_cont_par Then
                If GMS.DatiPerStampa.ContatoriPagine.PartiDocumento > 1 Then
                    Visibilit�StampaParti(True)
                    LBL_par_ela.Text = GMS.DatiPerStampa.ContatoriPagine.PartiDocumento
                    LBL_par_pro.Text = GMS.DatiPerStampa.ContatoriPagine.ParteInStampa
                    PGB_doc_parti.Maximum = GMS.DatiPerStampa.ContatoriPagine.PartiDocumento + 1
                    PGB_doc_parti.Value = GMS.DatiPerStampa.ContatoriPagine.ParteInStampa
                End If
            End If
        End If

    End Sub

    Private Sub InserisciLogAttivita(Optional ByVal cDescrizione As String = "")
        Dim cTmp As String

        If cDescrizione = "" Then
            cDescrizione = _DescrizioneAttivitaEsecuzione
        End If
        cTmp = String.Concat(Now.ToString("dd/MM/yyyy HH:mm:ss"), " - ", cDescrizione)
        LogAttivita.Items.Insert(0, cTmp)
        If LogAttivita.Items.Count > 1000 Then
            LogAttivita.Items.RemoveAt(1000)
        End If
        _log_ope.ScriviMessaggioLog(eLogLevel.LL_debugging, 1, "", "Attivita", _DescrizioneAttivitaEsecuzione)

    End Sub

    Private Sub Visibilit�GRBStampa(ByVal lVisible As Boolean)

        Me.GRB_stampa.Visible = lVisible
        If Me.GRB_stampa.Visible Then
            LBL_grp_ela.Text = GPS.GruppiDaStampare
            PGB_gruppi.Minimum = 0
            PGB_gruppi.Maximum = GPS.GruppiDaStampare
        End If

    End Sub

    Private Sub Visibilit�StampaPagine()
        Dim lVisible As Boolean
        Dim nPagine As Integer

        lVisible = _AttivitaEsecuzione = eStepAttivita.STA_3_14_ANT_document Or _AttivitaEsecuzione = eStepAttivita.STA_3_28_StampaModulo
        LBL_pag_pro.Visible = lVisible
        LBL_pag_pro_desc.Visible = lVisible

        lVisible = _AttivitaEsecuzione = eStepAttivita.STA_3_28_StampaModulo
        LBL_pag_ela_desc.Visible = lVisible
        LBL_pag_ela.Visible = lVisible
        PGB_pagine.Visible = lVisible

        If GMS IsNot Nothing Then
            nPagine = GMS.DatiPerStampa.TotalePagine
            If nPagine > -1 Then
                LBL_pag_ela.Text = nPagine
                PGB_pagine.Maximum = nPagine
            End If
        End If

    End Sub

    '
    ' Questa procedura sistema la visibilit� dei controlli per la visibilit� delle parti.
    '
    Private Sub Visibilit�StampaParti(ByVal lVisible As Boolean)

        Label9.Visible = lVisible
        LBL_par_pro.Visible = Label9.Visible
        Label11.Visible = Label9.Visible
        LBL_par_ela.Visible = Label9.Visible
        PGB_doc_parti.Visible = Label9.Visible

    End Sub

    '
    ' Questa procedura esegue materialmente la procedura selezionata di stampa o invio mail.
    '
    Private Sub EsecuzioneAttivita()
        Dim lExecNext As Boolean

        While _AttivitaEsecuzione <> eStepAttivita.STA_0_03_FIN_Attivita
            lExecNext = True
            Select Case _AttivitaEsecuzione
                Case Is = eStepAttivita.STA_0_01_CHK_tipo_ope
                    If TipoAttivita = eTipoAttivita.Stampa Then
                        If GSS.TipologiaStampa = 1 Then
                            _AttivitaEsecuzione = eStepAttivita.STA_1_01_INI_stampa_P
                        ElseIf GSS.TipologiaStampa = 2 Then
                            _AttivitaEsecuzione = eStepAttivita.STA_5_01_INI_stampa_G
                        End If
                    ElseIf TipoAttivita = eTipoAttivita.InvioMail Then
                        _AttivitaEsecuzione = eStepAttivita.STA_6_01_INI_send_mai
                    End If
                    lExecNext = False
                Case Is = eStepAttivita.STA_2_01_INI_oper_pre
                    If (GPS.FondiModuli) Then
                        DatiFusione = New CLS_DatiPerFusione
                        DatiFusione.SOrientamento = GSS.ModuloUnificato.Orientamento
                        DatiFusione.Rotation = GSS.ModuloUnificato.Rotazione
                        DatiFusione.PaginePariDocumentoGlobale = GSS.ModuloUnificato.PaginePariDocumentoGlobale
                        DatiFusione.PaginePariSingoloDocumento = GSS.ModuloUnificato.PaginePariSingoloDocumento
                    End If
                Case Is = eStepAttivita.STA_2_02_RST_cont_mod, Is = eStepAttivita.STA_3_05_RST_cont_mod
                    _NumModulo = -1
                Case Is = eStepAttivita.STA_2_03_NXT_cont_mod, Is = eStepAttivita.STA_3_06_NXT_cont_mod
                    _NumModulo += 1
                Case Is = eStepAttivita.STA_2_04_EXT_cont_mod, Is = eStepAttivita.STA_3_07_EXT_cont_mod
                    If _NumModulo = GPS.NumeroModuliDaStampare Then
                        If _AttivitaEsecuzione = eStepAttivita.STA_2_04_EXT_cont_mod Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_01_INI_prstampa
                        ElseIf _AttivitaEsecuzione = eStepAttivita.STA_3_07_EXT_cont_mod Then
                            _AttivitaEsecuzione = eStepAttivita.STA_4_10_CHK_fusi_nee
                        End If
                        lExecNext = False
                    End If
                Case Is = eStepAttivita.STA_2_12_LOO_cont_mod, Is = eStepAttivita.STA_3_40_LOO_cont_mod
                    If _AttivitaEsecuzione = eStepAttivita.STA_2_12_LOO_cont_mod Then
                        _AttivitaEsecuzione = eStepAttivita.STA_2_03_NXT_cont_mod
                    ElseIf _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod Then
                        _AttivitaEsecuzione = eStepAttivita.STA_3_06_NXT_cont_mod
                    End If
                    lExecNext = False

                    '
                    '
                    '
                Case Is = eStepAttivita.STA_3_02_RST_cont_grp, Is = eStepAttivita.STA_5_03_RST_cont_grp
                    GPS.GruppoInStampa = -1
                Case Is = eStepAttivita.STA_3_03_NXT_cont_grp, Is = eStepAttivita.STA_5_04_NXT_cont_grp
                    GPS.GruppoInStampa += 1
                    GPS.FirstRecord2Manage = GestioneSessioneStampa.CurrentRecordNumber
                Case Is = eStepAttivita.STA_3_04_EXT_cont_grp, Is = eStepAttivita.STA_5_05_EXT_cont_grp
                    If (GPS.FondiModuli) Then
                        DatiFusione.ResetDocumentiFondere()
                    End If
                    If GPS.GruppoInStampa = GPS.GruppiDaStampare Then
                        If _AttivitaEsecuzione = eStepAttivita.STA_3_04_EXT_cont_grp Then
                            _AttivitaEsecuzione = eStepAttivita.STA_1_15_FIN_stampa_P
                        ElseIf _AttivitaEsecuzione = eStepAttivita.STA_5_05_EXT_cont_grp Then
                            _AttivitaEsecuzione = eStepAttivita.STA_5_12_FIN_stampa_G
                        End If
                        lExecNext = False
                    End If
                Case Is = eStepAttivita.STA_4_10_CHK_fusi_nee
                    If Not GPS.FondiModuli Then
                        _AttivitaEsecuzione = eStepAttivita.STA_3_41_LOO_cont_grp
                        lExecNext = False
                    End If
                Case Is = eStepAttivita.STA_4_12_FUS_fondidoc
                    Fusione = New CLS_motore_fusione
                    Fusione.DatiFusione = DatiFusione
                    Fusione.FondiDocumenti()
                Case Is = eStepAttivita.STA_4_13_SaveSendByMail
                    'If Not GMS.StampaForzataDettaglio Then
                    'If Not GMS.DatiPerStampa.Ristampa Then
                    RimuoviDatiInvioByMail(True)
                    SaveSendByMail(True)
                    'End If
                Case Is = eStepAttivita.STA_3_41_LOO_cont_grp, Is = eStepAttivita.STA_5_11_LOO_cont_grp
                    If _AttivitaEsecuzione = eStepAttivita.STA_3_41_LOO_cont_grp Then
                        _AttivitaEsecuzione = eStepAttivita.STA_3_03_NXT_cont_grp
                    ElseIf _AttivitaEsecuzione = eStepAttivita.STA_5_11_LOO_cont_grp Then
                        _AttivitaEsecuzione = eStepAttivita.STA_5_04_NXT_cont_grp
                    End If
                    lExecNext = False
                Case Is = eStepAttivita.STA_1_15_FIN_stampa_P
                    Restore_PDFPrinterParam()
                    _AttivitaEsecuzione = eStepAttivita.STA_0_03_FIN_Attivita
                    lExecNext = False
                Case Is = eStepAttivita.STA_5_12_FIN_stampa_G
                    _AttivitaEsecuzione = eStepAttivita.STA_0_03_FIN_Attivita
                    lExecNext = False
                Case Is = eStepAttivita.STA_6_03_FIN_send_mai
                    _AttivitaEsecuzione = eStepAttivita.STA_0_03_FIN_Attivita
                    lExecNext = False

                Case Is = eStepAttivita.STA_3_09_CHK_consisDB, Is = eStepAttivita.STA_3_11_CHK_consisDB, Is = eStepAttivita.STA_3_16_CHK_consisDB, _
                    Is = eStepAttivita.STA_3_19_CHK_consisDB, Is = eStepAttivita.STA_3_29_CHK_consisDB, Is = eStepAttivita.STA_5_07_CHK_consisDB

                    '
                    ' Operazione per la verifica della presenza di record nel Fulldataset 
                    '
                    If GSS.IsEmptyFullDataset Then
                        If _AttivitaEsecuzione = eStepAttivita.STA_3_09_CHK_consisDB Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                        ElseIf _AttivitaEsecuzione = eStepAttivita.STA_3_11_CHK_consisDB Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                        ElseIf _AttivitaEsecuzione = eStepAttivita.STA_3_16_CHK_consisDB Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                        ElseIf _AttivitaEsecuzione = eStepAttivita.STA_3_19_CHK_consisDB Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                        ElseIf _AttivitaEsecuzione = eStepAttivita.STA_3_29_CHK_consisDB Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                        ElseIf _AttivitaEsecuzione = eStepAttivita.STA_5_07_CHK_consisDB Then
                            _AttivitaEsecuzione = eStepAttivita.STA_5_11_LOO_cont_grp
                        End If
                        lExecNext = False
                    End If

                Case Is = eStepAttivita.STA_3_08_LOA_database, Is = eStepAttivita.STA_5_06_LOA_database
                    '
                    ' Procedura di caricamento dei dati da stampare
                    '
                    CaricaDatabase()

                Case Is = eStepAttivita.STA_3_10_ONL_stampaca
                    '
                    ' Procedura per la rimozione dal Fulldataset delle righe per cui non � necessaria la stampa cartacea
                    '
                    Rimuovi_DocumentiStampaSoloElettronica()

                Case Is = eStepAttivita.STA_3_17_PostAnteprima
                    OperazioniPostAnteprima()

                Case Is = eStepAttivita.STA_5_02_CLE_folderEl
                    '
                    ' Procedura per la pulizia delle cartelle.
                    '
                    If _AttivitaEsecuzione = eStepAttivita.STA_5_02_CLE_folderEl Then
                        If GestioneProcessoStampa.PulisciFolderElaborati Then
                            PulisciFolder(GSS.Folders("ElaboratiFLD"))
                        End If
                    End If

                Case Is = eStepAttivita.STA_3_13_GET_previewD
                    '
                    ' Procedura per il recupero dei dati dell'anteprima gi� stampata.
                    '
                    If GPS.FastPrintEnabled AndAlso Carica_DatiAnteprima() Then
                        _AttivitaEsecuzione = eStepAttivita.STA_3_17_PostAnteprima
                        lExecNext = False
                    End If
                Case Is = eStepAttivita.STA_3_15_SAV_previewD
                    Salva_DatiAnteprima()
                Case Is = eStepAttivita.STA_3_18_REM_data_sbm
                    '
                    ' E' necessario verificare che fa questa procedura. 
                    '
                    If Not GMS.DatiPerStampa.Ristampa Then
                        RimuoviDatiInvioByMail(False)
                    End If
                Case Is = eStepAttivita.STA_3_12_PRE_stampa, Is = eStepAttivita.STA_3_20_PRE_stampa
                    PreparaPerStampa(_AttivitaEsecuzione = eStepAttivita.STA_3_12_PRE_stampa)
                Case Is = eStepAttivita.STA_3_14_ANT_document
                    If StampaDocumento(True) Then

                    End If
                Case Is = eStepAttivita.STA_3_28_StampaModulo
                    If StampaDocumento(False) Then

                    End If

                    '
                    ' Aggiorna le statistiche di stampa in caso di stampa cartacea
                    '
                    If Not GPS.ArchiviazioneOttica Then
                        GSS.AggiornaStatisticheStampa(GMS.DatiPerStampa.FogliStampati)
                    End If
                Case Is = eStepAttivita.STA_2_05_CHK_pag_gene
                    If Not GMS.EsistonoPagineGenerare Then
                        _AttivitaEsecuzione = eStepAttivita.STA_2_12_LOO_cont_mod
                        lExecNext = False
                    End If
                Case Is = eStepAttivita.STA_5_01_INI_stampa_G
                    _NumModulo = 0
                Case Is = eStepAttivita.STA_5_08_BUI_tokenCod
                    GPS.GeneraTokenElaborazione()
                Case Is = eStepAttivita.STA_5_09_INS_BarcodeI
                    InserisciBarcode()
                Case Is = eStepAttivita.STA_5_10_BUI_fileStam
                    RiunisciFile()
                Case Is = eStepAttivita.STA_3_22_RST_cont_par
                    GMS.DatiPerStampa.ContatoriPagine.ParteInStampa = 0
                Case Is = eStepAttivita.STA_3_23_NXT_cont_par
                    GMS.DatiPerStampa.ContatoriPagine.ParteInStampa += 1
                Case Is = eStepAttivita.STA_3_24_EXT_cont_par
                    If Check_FineStampaParte() Then
                        If GMS.DatiPerStampa.ContatoriPagine.PartiDocumento > 1 Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_40_MER_parti
                        Else
                            _AttivitaEsecuzione = eStepAttivita.STA_3_XX_LOO_SettaSintetico_Dettaglio
                            '_AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                        End If
                        lExecNext = False
                    End If
                Case Is = eStepAttivita.STA_3_40_MER_parti
                    RiunisciParti()
                Case Is = eStepAttivita.STA_3_26_SET_pdf_prin
                    PredisponiStampanteOttica(ePDFCreatorFileType.FT_PDF1A)
                Case Is = eStepAttivita.STA_3_25_CHK_pdf_prin, Is = eStepAttivita.STA_3_30_CHK_pdf_prin
                    If Not GPS.ArchiviazioneOttica Then
                        If _AttivitaEsecuzione = eStepAttivita.STA_3_25_CHK_pdf_prin Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_28_StampaModulo
                        ElseIf _AttivitaEsecuzione = eStepAttivita.STA_3_30_CHK_pdf_prin Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_36_PostStampa
                        End If
                        lExecNext = False
                    End If
                Case Is = eStepAttivita.STA_3_32_CHK_paginepa
                    If Not GMS.DatiPerStampa.RendiPaginePari Then
                        _AttivitaEsecuzione = eStepAttivita.STA_3_33_RES_pdf_prin
                        lExecNext = False
                    End If
                Case Is = eStepAttivita.STA_3_32_BUI_paginepa
                    RendiPariPagine()
                Case Is = eStepAttivita.STA_3_31_WAI_pdf_prin
                    AttesaProduzioneOttico(200)
                Case Is = eStepAttivita.STA_3_33_RES_pdf_prin
                    Restore_PDFPrinterParam()
                Case Is = eStepAttivita.STA_3_35_SaveSendByMail
                    If Not GMS.DatiPerStampa.Ristampa Then
                        SaveSendByMail(False)
                    End If
                Case Is = eStepAttivita.STA_3_39_LOO_cont_par
                    _AttivitaEsecuzione = eStepAttivita.STA_3_23_NXT_cont_par
                    lExecNext = False
                Case Is = eStepAttivita.STA_3_22_RST_SettaSintetico_Dettaglio
                    GMS.DatiPerStampa.LoopSinteticoDettaglio = 0
                Case Is = eStepAttivita.STA_3_23_NXT_SettaSintetico_Dettaglio
                    GMS.DatiPerStampa.LoopSinteticoDettaglio += 1
                    'If GMS.StampaConDettaglio Then
                    '    If (GMS.DatiPerStampa.LoopSinteticoDettaglio = 1) Then
                    '        If GSS.FullDataSet_MainTable.Columns.Contains("DEF_fattura_dettaglio") Then
                    '            GSS.FullDataSet_MainTable.Rows(0).Item("DEF_stampa_dettaglio") = GSS.FullDataSet_MainTable.Rows(0).Item("DEF_fattura_dettaglio")
                    '        Else
                    '            GSS.FullDataSet_MainTable.Rows(0).Item("DEF_stampa_dettaglio") = GSS.FullDataSet_MainTable.Rows(0).Item("DEF_richiesta_dettaglio")
                    '        End If
                    '    ElseIf (GMS.DatiPerStampa.LoopSinteticoDettaglio = 2) Then
                    '        GSS.FullDataSet_MainTable.Rows(0).Item("DEF_stampa_dettaglio") = GSS.FullDataSet_MainTable.Rows(0).Item("DEF_fattura_dettaglio")
                    '    ElseIf (GMS.DatiPerStampa.LoopSinteticoDettaglio = 3) Then
                    '        GSS.FullDataSet_MainTable.Rows(0).Item("DEF_stampa_dettaglio") = True
                    '    End If
                    'End If
                Case Is = eStepAttivita.STA_3_24_EXT_SettaSintetico_Dettaglio
                    If (Not GMS.DatiPerStampa.ArchiviazioneOttica) Then
                        If (GMS.DatiPerStampa.LoopSinteticoDettaglio = 2) Then
                            _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                            lExecNext = False
                        End If
                    Else
                        If (GMS.StampaConDettaglio) Then
                            If GSS.FullDataSet_MainTable.Columns.Contains("DEF_fattura_alias") Then
                                If (GMS.DatiPerStampa.LoopSinteticoDettaglio = 3 And Not GSS.FullDataSet_MainTable.Rows(0).Item("DEF_fattura_alias")) Or (GMS.DatiPerStampa.LoopSinteticoDettaglio = 4) Then
                                    _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                                    lExecNext = False
                                End If
                            Else
                                If (GMS.DatiPerStampa.LoopSinteticoDettaglio = 3 And Not GSS.FullDataSet_MainTable.Rows(0).Item("DEF_richiesta_dettaglio")) Or (GMS.DatiPerStampa.LoopSinteticoDettaglio = 4) Then
                                    _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                                    lExecNext = False
                                End If
                            End If
                        Else
                            If (GMS.DatiPerStampa.LoopSinteticoDettaglio = 2) Then
                                _AttivitaEsecuzione = eStepAttivita.STA_3_40_LOO_cont_mod
                                lExecNext = False
                            End If

                        End If
                    End If
                Case Is = eStepAttivita.STA_3_XX_LOO_SettaSintetico_Dettaglio
                    _AttivitaEsecuzione = eStepAttivita.STA_3_23_NXT_SettaSintetico_Dettaglio
                    lExecNext = False
            End Select

            If lExecNext Then
                _AttivitaEsecuzione += 1
            End If
            System.Threading.Thread.Sleep(ObjGlobali.CFG.SleepEsecuzioneAttivita)
        End While

    End Sub

#Region "Procedure comuni a tutte le tipologie di attivit�"

    ' ********************************************************************* '
    ' Caricamento dei dati che andranno stampati.                           '
    ' ********************************************************************* '
    Private Sub CaricaDatabase()

        _log_ope.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaDatabase", String.Concat("INIZIO - Avvio caricamento dati nel database."))
        _log_ope.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "CaricaDatabase", String.Concat("Attivazione del modulo: ", GMS.NomeModulo, " (", GMS.CodiceModulo, ")"))
        GSS.AttivaModulo(GMS.CodiceModulo)
        GSS.GoToRecordNumber(GPS.FirstRecord2Manage)
        _log_ope.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "CaricaDatabase", String.Concat("Primo record da caricare: ", GPS.FirstRecord2Manage))
        GPS.FirstRecordPrinted = GSS.CurrentRecordNumber
        GSS.Carica_FullDataset(GPS.NumeroDocumenti)
        _log_ope.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "CaricaDatabase", String.Concat("Numero Record caricati: ", GSS.RecordFullDatasetMainTable))
        GPS.LastRecordPrinted = GSS.CurrentRecordNumber
        _log_ope.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaDatabase", String.Concat("FINE - Avvio caricamento dati nel database."))

        '
        ' Verifico se il dataset contiene il campo per la stampa delle bollette Sintetiche di dettaglio.
        '
        'If GSS.FullDataSet_MainTable.Columns.Contains("DEF_stampa_dettaglio") Then
        '    For Each dr As DataRow In GSS.FullDataSet_MainTable.Rows
        '        dr.Item("DEF_stampa_dettaglio") = GMS.StampaForzataDettaglio
        '    Next
        'End If

    End Sub

#End Region

    '
    ' Questa procedura si occupa di stampare rimuovere i documenti che � necessario stampare solo in modalit� elettronica.
    '
    Private Sub Rimuovi_DocumentiStampaSoloElettronica()
        Dim dr_mai_qd As DataRow
        Dim dr_fud As DataRow

        '
        ' Se stiamo stampando i docuementi cartacei e non abbiamo impostato la forzatura della stampa cartacea allora procedo a cancellare i dati dei record interessati.
        '
        If Not GPS.ArchiviazioneOttica And Not GPS.ForceStampaCartaceo And Not GMS.ModuloSenzaSelezioneNecessaria Then
            For Each dr_fud In GSS.FullDataSet_MainTable.Rows
                ' ********************************************************************* '
                ' Per alcuni plug-in il valore della colonna MAINTABLEKEY della tabella ' 
                ' QUICKDATASET � di tipo 'string' e pertanto va effettuata una select   '
                ' con tipologia stringa quindi con apici prima e dopo il valore ricerca ' 
                ' to.                                                                   '
                ' ********************************************************************* '
                If Type.GetTypeCode(GSS.QuickDataTable.Columns(GSS.QuickDataset_MainTableKey).datatype) = TypeCode.String Then
                    dr_mai_qd = GSS.QuickDataTable.Select(String.Concat(GSS.QuickDataset_MainTableKey, " = '", dr_fud.Item(GSS.LinkField_QuickDS_FullDS), "'"))(0)
                Else
                    dr_mai_qd = GSS.QuickDataTable.Select(String.Concat(GSS.QuickDataset_MainTableKey, " = ", dr_fud.Item(GSS.LinkField_QuickDS_FullDS)))(0)
                End If
                If (dr_mai_qd.Item("DEF_onlyarchott") And dr_mai_qd.Item("DEF_sendbymail")) Then
                    dr_fud.Delete()
                End If
            Next
            GSS.FullDataSet_MainTable.AcceptChanges()
        End If

    End Sub

    ' ********************************************************************* '
    ' Stampa dei documenti.                                                 '
    ' ********************************************************************* '
    Private Function StampaDocumento(ByVal lAnteprima As Boolean) As Boolean
        Dim prnprn As CLS_motore_stampa
        Dim lRtn As Boolean

        lRtn = True
        Try
            If GPS.ArchiviazioneOttica Then
                prnprn = New CLS_motore_stampa(GMS.NomeStampanteOttica, ObjGlobali.CFG.Folders("LogsFLD"), ObjGlobali.CFG.LogLevel)
            Else
                prnprn = New CLS_motore_stampa(GMS.NomeStampanteCartacea, ObjGlobali.CFG.Folders("LogsFLD"), ObjGlobali.CFG.LogLevel)
            End If

            prnprn.DatiPerStampa = GMS.DatiPerStampa
            prnprn.GestioneModulo = GMS.Modulo
            prnprn.StampaForzataDettaglio = GMS.StampaForzataDettaglio
            prnprn.WorkFLD = ObjGlobali.Folders("PrnWrkFLD")
            GMS.DatiPerStampa.StampaForzata = GMS.StampaForzataDettaglio
            prnprn.PrintController = New System.Drawing.Printing.StandardPrintController
            If GMS.DatiPerStampa.Anteprima Then
                prnprn.PrinterSettings.PrintFileName = String.Concat(Folders("PrnWrkFLD"), "print_temp_", Timestamp)
                prnprn.PrinterSettings.PrintToFile = True
            End If

            If Not prnprn Is Nothing Then
                prnprn.Print()
                If Not GMS.DatiPerStampa.ContatoriPagine.PartiDocumento > 1 Then
                    SetUpError2QuickDataset(prnprn.StatusPrn)
                End If
                If prnprn.Cancelled Then
                    lRtn = False
                End If
                prnprn.Dispose()
            End If
        Catch ex As Exception
            lRtn = False
        End Try
        Return lRtn

    End Function

    Private Sub SetUpError2QuickDataset(ByVal _StatusPrn As Dictionary(Of Integer, String()))
        Dim nRec As Integer
        Dim dr As DataRow
        Dim nRecCode As Integer

        For nRec = GPS.FirstRecordPrinted To GPS.LastRecordPrinted - 1
            dr = GSS.QuickDataTable.Rows(nRec)
            nRecCode = dr(GSS.QuickDataset_MainTableKey)
            If _StatusPrn.ContainsKey(nRecCode) Then
                If (dr("DEF_errcode") = 0) And (_StatusPrn(nRecCode)(0) < 0) Then
                    dr("DEF_errcode") = _StatusPrn(nRecCode)(0)
                    dr("DEF_errdesc") = _StatusPrn(nRecCode)(1)
                End If
            End If
        Next

    End Sub

    Private Sub CancellaDocumentoGenerare(ByVal cFileName As String)
        Dim lDeleted As Boolean
        Dim cTmp As String

        lDeleted = False
        _log_ope.ScriviMessaggioLog(eLogLevel.LL_informational, 1, "", "CancellaDocumentoGenerare", "INIZIO - Cancellazionne file")
        _log_ope.ScriviMessaggioLog(eLogLevel.LL_informational, 2, "", "CancellaDocumentoGenerare", "File da cancellare: " + cFileName)
        cTmp = String.Concat("_", GMS.DatiPerStampa.SuffissoParte, "_")
        While Not lDeleted And Not ForcedExit
            Try
                '
                ' Rimuove i file singoli.
                '
                For Each f As String In System.IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(cFileName), System.IO.Path.GetFileName(cFileName))
                    _log_ope.ScriviMessaggioLog(eLogLevel.LL_informational, 2, "", "CancellaDocumentoGenerare", "Cancellazione del file: " + f)
                    System.IO.File.Delete(f)
                Next
                '
                ' Rimuovo eventuali parti.
                '
                _log_ope.ScriviMessaggioLog(eLogLevel.LL_informational, 2, "", "CancellaDocumentoGenerare", "Cancellazione dei file: " + cTmp)
                If cFileName.Contains(cTmp) Then
                    cFileName = cFileName.Remove(cFileName.IndexOf(cTmp), cTmp.Length + 3)
                End If
                cFileName = cFileName.Replace(".pdf", cTmp + "*.pdf")
                If Not cFileName.Contains("*") Then
                    cFileName = cFileName.Replace(".pdf", "*.pdf")
                End If
                _log_ope.ScriviMessaggioLog(eLogLevel.LL_informational, 2, "", "CancellaDocumentoGenerare", "Cancellazione dei file: " + cFileName)
                For Each f As String In System.IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(cFileName), System.IO.Path.GetFileName(cFileName))
                    _log_ope.ScriviMessaggioLog(eLogLevel.LL_informational, 2, "", "CancellaDocumentoGenerare", "Cancellazione del file: " + f)
                    System.IO.File.Delete(f)
                Next
                lDeleted = True
            Catch ex As Exception
                If MessageBox.Show(String.Concat("Attenzione il file """, cFileName, """ � aperto da un'altra applicazione. Attendo la chiusura del file?"), "File bloccato", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No Then
                    ForcedExit = True
                End If
            End Try
        End While
        _log_ope.ScriviMessaggioLog(eLogLevel.LL_informational, 1, "", "CancellaDocumentoGenerare", "FINE - Cancellazionne file")

    End Sub

    Private Sub OperazioniPostAnteprima()
        Dim lStampaComunque As Boolean
        Dim nRecCode As Integer
        Dim nFogli As Integer
        Dim nRec As Integer
        Dim dr As DataRow

        GSS.PostAnteprima(GMS.DatiPerStampa.ContatoriPagine.PagineSubDocumenti)

        '
        ' Controlla che il numero di pagine siano nei limiti di quanto stampabile
        '
        lStampaComunque = True
        If Not GPS.ArchiviazioneOttica Then
            If GMS.DatiPerStampa.DocumentoTroppiFogli Then
                '
                ' Imposta ad errore il record con troppi fogli
                '
                If MessageBox.Show(String.Concat("Attenzione! Ci sono dei moduli che superano il numero massimo di fogli stampabili.", vbLf, "Li stampo comunque?"), "Moduli troppo grandi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No Then
                    For nRec = GPS.FirstRecordPrinted To GPS.LastRecordPrinted - 1
                        dr = GSS.QuickDataTable.Rows(nRec)
                        nRecCode = dr(GSS.QuickDataset_MainTableKey)
                        If GMS.DatiPerStampa.ElencoPagineDocumenti.ContainsKey(nRecCode) Then
                            If GMS.DatiPerStampa.ElencoPagineDocumenti(nRecCode).TroppiFogli Then
                                If dr("DEF_errcode") >= -10 Then
                                    dr("DEF_errcode") = -11
                                    dr("DEF_errdesc") = String.Concat("Il modulo in stampa si compone di ", nFogli, " fogli superando il limite di fogli stampabili di ", GPS.MaxNumFogli)
                                    dr.AcceptChanges()
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        End If

    End Sub

    Private Function Check_FineStampaParte() As Boolean

        Return GMS.DatiPerStampa.ContatoriPagine.PartiDocumento < GMS.DatiPerStampa.ContatoriPagine.ParteInStampa

    End Function

    ' ********************************************************************* '
    ' Ciclo di attesa per la produzione del documento ottico.               '
    ' ********************************************************************* '
    Private Sub AttesaProduzioneOttico(ByVal SleepInterval As Integer)
        Dim cFileName As String
        Dim cTmp As String

        Try
            cFileName = GMS.DatiPerStampa.ArchiviazioneOtticaFN
            While Not System.IO.File.Exists(cFileName) And Not ForcedExit
                System.Threading.Thread.Sleep(SleepInterval)
            End While
            While _prnPDF.NumeroJobInStampa > 0 And Not ForcedExit
                System.Threading.Thread.Sleep(SleepInterval)
                ' ********************************************************************* '
                ' L'istruzione che segue serve esclusivamente a controllare un abort    ' 
                ' del processo PDFCreator.                                              '
                ' ********************************************************************* '
                cTmp = _prnPDF.AutosaveFilename
            End While
        Catch ex As Exception
            MessageBox.Show("Errore nella generazione del documento PDF. La procedura verr� interrotta", "Errore generazione PDF", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub RendiPariPagine()
        Dim cTmpFileName As String
        Dim document2 As Document
        Dim cFileName As String
        Dim reader As PdfReader
        Dim nPagine As Integer
        Dim copy As PdfCopy
        Dim i As Integer

        Try
            cFileName = GMS.DatiPerStampa.ArchiviazioneOtticaFN
            reader = New PdfReader(System.IO.File.ReadAllBytes(cFileName))
            nPagine = reader.NumberOfPages
            If nPagine Mod 2 = 1 Then
                cTmpFileName = String.Concat(Folders("PrnWrkFLD"), "OddPage_", Timestamp, ".pdf")
                document2 = New Document
                copy = New PdfCopy(document2, New System.IO.FileStream(cTmpFileName, System.IO.FileMode.OpenOrCreate))
                document2.Open()
                For i = 1 To nPagine
                    copy.AddPage(copy.GetImportedPage(reader, i))
                Next

                '
                ' Aggiungi la pagina in pi�
                '
                copy.AddPage(PageSize.A4, 0)
                document2.Close()

                '
                ' Copio il documento nella posizione originale.
                '
                System.IO.File.Copy(cTmpFileName, cFileName, True)
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub RiunisciParti()
        Dim document2 As Document
        Dim reader As PdfReader
        Dim nPagine As Integer
        Dim copy As PdfCopy
        Dim i As Integer

        Dim nParti As Integer
        Dim cFileName As String
        Dim aListaFile As Array

        Try
            nParti = GMS.DatiPerStampa.ContatoriPagine.PartiDocumento
            GMS.DatiPerStampa.ContatoriPagine.PartiDocumento = 1
            cFileName = GMS.DatiPerStampa.ArchiviazioneOtticaFN.Replace(".pdf", "*.pdf")
            aListaFile = System.IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(cFileName), System.IO.Path.GetFileName(cFileName))
            If nParti = aListaFile.Length Then
                document2 = New Document
                copy = New PdfCopy(document2, New System.IO.FileStream(GMS.DatiPerStampa.ArchiviazioneOtticaFN(), System.IO.FileMode.OpenOrCreate))
                document2.Open()
                For Each cFile As String In aListaFile
                    reader = New PdfReader(System.IO.File.ReadAllBytes(cFile))
                    nPagine = reader.NumberOfPages
                    For i = 1 To nPagine
                        copy.AddPage(copy.GetImportedPage(reader, i))
                    Next
                Next
                document2.Close()
                For Each cFile As String In aListaFile
                    System.IO.File.Delete(cFile)
                Next
            Else
                Throw New Exception("Numero di parti differenti")
            End If
        Catch ex As Exception

        End Try

    End Sub

    'Private Sub FondiDocumenti(ByVal cFolder As String, ByVal cfilePrefix As String)
    '    Dim cTmpFileName As String
    '    Dim document2 As Document
    '    Dim reader As PdfReader
    '    Dim nPagine As Integer
    '    Dim copy As PdfCopy
    '    Dim i As Integer

    '    Try
    '        cTmpFileName = String.Concat(ObjGlobali.GestioneSessioneStampa.Folders("DocumentiUnificatiFLD"), _FileNameUnificato, ".pdf")
    '        document2 = New Document
    '        copy = New PdfCopy(document2, New System.IO.FileStream(cTmpFileName, System.IO.FileMode.OpenOrCreate))
    '        document2.Open()
    '        'For Each cFile As String In _documentiDaFondere
    '        '    reader = New PdfReader(System.IO.File.ReadAllBytes(cFile))
    '        '    nPagine = reader.NumberOfPages
    '        '    For i = 1 To nPagine
    '        '        copy.AddPage(copy.GetImportedPage(reader, i))
    '        '    Next
    '        '    If nPagine Mod 2 = 1 Then
    '        '        copy.AddPage(PageSize.A4, 0)
    '        '    End If
    '        'Next
    '        document2.Close()
    '    Catch ex As Exception

    '    End Try

    'End Sub

    '
    ' Prepara la struttura dei dati per la stampa.
    '
    Private Sub PreparaPerStampa(ByVal lAnteprima As Boolean)

        '
        ' Impostazione dell'anteprima
        '
        GPS.Anteprima = lAnteprima
        GMS.DatiPerStampa.Anteprima = lAnteprima
        If GPS.ArchiviazioneOttica Then
            GMS.DatiPerStampa.ArchiviazioneOtticaFNTipo = GMS.Modulo.NomeFileArchivioOtticoSelect.AOF_filename
        End If

        '
        ' Impostazione dei dati
        '
        GMS.DatiPerStampa.DBStampa.SetUpDB(GSS.FullDataset, GSS.FullDataset_MainTableName, GSS.Files("PlugInDatiFissiFIL"), GSS.LinkField_QuickDS_FullDS)

        '
        ' 
        '
        If lAnteprima Then
            For Each dr As DataRow In GSS.FullDataSet_MainTable.Rows
                GMS.DatiPerStampa.AggiungiContatoriPagineVuoto(dr(GSS.LinkField_QuickDS_FullDS))
            Next
        Else
            If GPS.FondiModuli And GMS.Modulo.DatiModulo.Tipologia = eTipologiaModulo.TIM_Master Then
                DatiFusione.NomeDocumentoFuso = String.Concat(ObjGlobali.GestioneSessioneStampa.Folders("DocumentiUnificatiFLD"), GMS.DatiPerStampa.AccessiDati.ExtractValue(GSS.ModuloUnificato.NomeFileArchivioOtticoSelect.AOF_filename, "", 0, "", 0), ".pdf")
            End If
        End If

    End Sub

    Private Sub PredisponiStampanteOttica(ByVal asFormat As ePDFCreatorFileType)
        Dim cFileName As String

        cFileName = GMS.DatiPerStampa.ArchiviazioneOtticaFN

        '
        ' Rimuovi il documento elettronico generato in precedenza
        '
        If GMS.DatiPerStampa.ContatoriPagine.ParteInStampa = 1 Then
            CancellaDocumentoGenerare(cFileName)
        End If

        '
        ' Predisponi la stampante ottica con il nome file corretto.
        '
        If _prnPDF Is Nothing Then
            _prnPDF = New CLS_PDFCreator("PDFCreator")
            _prnPDF.AutosaveFormat = asFormat
        End If
        _prnPDF.AutosaveDirectory = System.IO.Path.GetDirectoryName(cFileName)
        _prnPDF.AutosaveFilename = System.IO.Path.GetFileName(cFileName)
        _prnPDF.AttivaStampante()

        '
        ' Aggiungo il nome del file all'elenco dei file da fondere eventualmente
        '
        If (GPS.FondiModuli) Then
            DatiFusione.DocumentiDaFondere.Add(cFileName)
        End If

    End Sub

#Region "Elenco delle procedure per il bypass dell'anteprima"

    Private Function Carica_DatiAnteprima() As Boolean
        Dim c As ContatoriPagineEntity
        Dim xdFastPrint As XmlDocument
        Dim xnDocumento As XmlNode
        Dim xnGruppo As XmlNode
        Dim cValue As String
        Dim nKey As Integer
        Dim lRtn As Boolean
        Dim lNotFound As Boolean
        Dim lDettaglio As Boolean

        Try
            xdFastPrint = New XmlDocument
            lNotFound = False
            If System.IO.File.Exists(GSS.Files("FastPrintFIL")) Then
                lRtn = True
                xdFastPrint.Load(GSS.Files("FastPrintFIL"))

                lDettaglio = GSS.FullDataSet_MainTable.Rows(0).Item("DEF_stampa_dettaglio")
                '
                ' Ricerca presenza 3 cheksum di riferimento
                '
                'xnGruppo = xdFastPrint.SelectSingleNode(String.Concat("fastprint/anteprima[@plugin='", GSS.CheckSumPlugIn, "' and @modulo='", GMS.Modulo.ModuloCheckSum, "' and @datifissi='", GSS.CheckSumDatiFissi, "']"))
                xnGruppo = xdFastPrint.SelectSingleNode(String.Concat("fastprint/anteprima[@plugin='", GSS.CheckSumPlugIn, "' and @modulo='", GMS.Modulo.ModuloCheckSum, "' and @datifissi='", GSS.CheckSumDatiFissi, "' and @dettaglio='", lDettaglio, "']"))
                If xnGruppo IsNot Nothing Then
                    For Each dr As DataRow In GSS.FullDataSet_MainTable.Rows
                        nKey = dr(GSS.LinkField_QuickDS_FullDS)
                        xnDocumento = xnGruppo.SelectSingleNode(String.Concat("documento[@codice='", nKey, "']"))
                        If xnDocumento IsNot Nothing Then
                            c = GMS.DatiPerStampa.ElencoPagineDocumenti(nKey)
                            If Not c.GiaEseguitaAnteprima Then
                                '                                c.PagineDocumento = xnDocumento.SelectSingleNode("totali").InnerText
                                For Each cValue In xnDocumento.SelectSingleNode("parzia").InnerText.Split(";")
                                    c.PagineDocumento += Int(cValue)
                                    c.FineStampaSubDocumento(cValue)
                                Next
                                c.GiaEseguitaAnteprima = True
                                GMS.DatiPerStampa.ProgressivoPagine += c.PagineDocumento
                                GMS.DatiPerStampa.FineStampaDocumento(nKey)
                            End If
                        Else
                            lNotFound = True
                        End If
                    Next
                Else
                    lNotFound = True
                End If
            Else
                lNotFound = True
            End If
            lRtn = Not lNotFound
        Catch ex As Exception
            '
            ' Se il file non � leggibile allora lo butto.
            '
            System.IO.File.Delete(GSS.Files("FastPrintFIL"))
            MessageBox.Show("Attenzione il file per il FastPrint � illegibile. Si proceder� senza questa modalit�", "Errore su FastPrint", MessageBoxButtons.OK, MessageBoxIcon.Error)
            GPS.FastPrintEnabled = False
            lRtn = False
        End Try
        Return lRtn

    End Function

    '
    ' Procedura peril salvataggio dei dati di anteprima.
    '
    Private Sub Salva_DatiAnteprima()
        Dim xdFastPrint As XmlDocument
        Dim cPagineparziali As String
        Dim xnDocumento As XmlNode
        Dim xnGruppo As XmlNode
        Dim nPagine As Integer
        Dim lDettaglio As Boolean

        Try
            xdFastPrint = New XmlDocument
            If System.IO.File.Exists(GSS.Files("FastPrintFIL")) Then
                xdFastPrint.Load(GSS.Files("FastPrintFIL"))
            Else
                xdFastPrint.LoadXml("<fastprint></fastprint>")
            End If

            lDettaglio = GSS.FullDataSet_MainTable.Rows(0).Item("DEF_stampa_dettaglio")
            '
            ' Ricerca presenza 3 cheksum di riferimento
            '
            xnGruppo = xdFastPrint.SelectSingleNode(String.Concat("fastprint/anteprima[@plugin='", GSS.CheckSumPlugIn, "' and @modulo='", GMS.Modulo.ModuloCheckSum, "' and @datifissi='", GSS.CheckSumDatiFissi, "' and @dettaglio='", lDettaglio, "']"))
            If xnGruppo Is Nothing Then
                xnGruppo = xdFastPrint.CreateNode(XmlNodeType.Element, "anteprima", "")
                xnGruppo.Attributes.Append(xdFastPrint.CreateNode(XmlNodeType.Attribute, "nomemodulo", ""))
                xnGruppo.Attributes("nomemodulo").Value = GMS.NomeModulo
                xnGruppo.Attributes.Append(xdFastPrint.CreateNode(XmlNodeType.Attribute, "plugin", ""))
                xnGruppo.Attributes("plugin").Value = GSS.CheckSumPlugIn
                xnGruppo.Attributes.Append(xdFastPrint.CreateNode(XmlNodeType.Attribute, "modulo", ""))
                xnGruppo.Attributes("modulo").Value = GMS.Modulo.ModuloCheckSum
                xnGruppo.Attributes.Append(xdFastPrint.CreateNode(XmlNodeType.Attribute, "datifissi", ""))
                xnGruppo.Attributes("datifissi").Value = GSS.CheckSumDatiFissi
                xnGruppo.Attributes.Append(xdFastPrint.CreateNode(XmlNodeType.Attribute, "dettaglio", ""))
                xnGruppo.Attributes("dettaglio").Value = lDettaglio
                xdFastPrint.SelectSingleNode("fastprint").AppendChild(xnGruppo)
            End If

            '
            ' All'interno del gruppo ricerca la presenza del documento di cui devo salvare i dati di anteprima.
            '
            For Each k As KeyValuePair(Of Integer, ContatoriPagineEntity) In GMS.DatiPerStampa.ElencoPagineDocumenti
                xnDocumento = xnGruppo.SelectSingleNode(String.Concat("documento[@codice='", k.Key, "']"))
                If xnDocumento Is Nothing Then
                    xnDocumento = xdFastPrint.CreateNode(XmlNodeType.Element, "documento", "")
                    xnDocumento.Attributes.Append(xdFastPrint.CreateNode(XmlNodeType.Attribute, "codice", ""))
                    xnDocumento.Attributes("codice").Value = k.Key
                    xnDocumento.AppendChild(xdFastPrint.CreateNode(XmlNodeType.Element, "totali", ""))
                    xnDocumento.AppendChild(xdFastPrint.CreateNode(XmlNodeType.Element, "parzia", ""))
                    xnGruppo.AppendChild(xnDocumento)
                End If
                '
                ' Aggiorna i dati delle pagine.
                '
                xnDocumento.SelectSingleNode("totali").InnerText = k.Value.PagineDocumento
                cPagineparziali = ""
                For Each nPagine In k.Value.PagineSubDocumenti
                    cPagineparziali = String.Concat(cPagineparziali, nPagine, ";")
                Next
                k.Value.GiaEseguitaAnteprima = True
                xnDocumento.SelectSingleNode("parzia").InnerText = cPagineparziali.Substring(0, cPagineparziali.Length - 1)
            Next
            xdFastPrint.Save(GSS.Files("FastPrintFIL"))
            xdFastPrint = Nothing
        Catch ex As Exception

        End Try

    End Sub

#End Region

#Region "Procedure esclusive per elaborazione documenti pregenerati"

    Private Sub InserisciBarcode()
        Dim document1 As Document
        Dim writer1 As PdfWriter
        Dim page As PdfImportedPage
        Dim rotation As Integer
        Dim nPagina As Integer
        Dim nDocumento As Integer
        Dim imgeansub As iTextSharp.text.Image
        Dim bar2of5 As BarcodeInter25
        Dim nPagine As Integer
        Dim cb1 As PdfContentByte
        Dim nFoglio As Integer
        Dim nFogli As Integer
        Dim cBarCode As String
        Dim nContaFogli As Integer
        Dim reader As PdfReader
        Dim dr As DataRow
        Dim cTmp As String

        Try
            nPagina = 0

            nDocumento = 0
            nContaFogli = ObjGlobali.GestioneModuliStampare(1).WrapRound

            For Each dr In GSS.FullDataSet_MainTable.Rows
                nDocumento += 1
                cTmp = dr("MAI_filename")
                reader = New PdfReader(cTmp)

                '
                ' Recuperiamo il numero totale di pagine
                '
                nPagina = 1
                nPagine = reader.NumberOfPages
                nFogli = (nPagine / 2)

                '
                ' Predispone il nuovo documento
                '
                document1 = New Document(reader.GetPageSizeWithRotation(nPagina))
                writer1 = PdfWriter.GetInstance(document1, New System.IO.FileStream(Folders("PrnWrkFLD") + GPS.TokenElaborazione + "_" + dr("MAI_codice").ToString().PadLeft(10, "0") + System.IO.Path.GetExtension(cTmp), System.IO.FileMode.Create))
                document1.Open()
                '
                ' Cicla su tutte le pagine per aggiungereil codice a barre.
                '
                While (nPagina <= nPagine)
                    document1.NewPage()
                    page = writer1.GetImportedPage(reader, nPagina)
                    cb1 = writer1.DirectContent
                    If (nPagina Mod 2 = 1) Then

                        ' 
                        '  Generazione del BarCode
                        '  
                        nFoglio = ((nPagina - 1) / 2) + 1
                        If ((nFoglio = nFogli) And (nFoglio = 1)) Then
                            cBarCode = "3"
                        Else
                            If (nFoglio = nFogli) Then
                                cBarCode = "2"
                            Else
                                If (nFoglio = 1) Then
                                    cBarCode = "1"
                                Else
                                    cBarCode = "0"
                                End If
                            End If
                        End If
                        '
                        ' Fisso a 2
                        '
                        cBarCode += "2"
                        cBarCode += nContaFogli.ToString()
                        cBarCode += nDocumento.ToString().PadLeft(7, "0")
                        bar2of5 = New BarcodeInter25()
                        bar2of5.Code = cBarCode
                        bar2of5.StartStopText = False
                        bar2of5.BarHeight = GMS.Modulo.IMBUSTAMENTO_AltezzaBarCode
                        imgeansub = bar2of5.CreateImageWithBarcode(cb1, BaseColor.BLACK, BaseColor.WHITE)
                        imgeansub.SetAbsolutePosition(GMS.Modulo.IMBUSTAMENTO_BarcodeX, GMS.Modulo.IMBUSTAMENTO_BarcodeY)
                        imgeansub.Rotation = Math.PI / 2
                        document1.Add(imgeansub)
                        nContaFogli += 1
                        If (nContaFogli = 10) Then
                            nContaFogli = 1
                        End If
                    End If
                    rotation = reader.GetPageRotation(nPagina)
                    If (rotation = 90 Or rotation = 270) Then
                        cb1.AddTemplate(page, 0, -1.0F, 1.0F, 0, 0, reader.GetPageSizeWithRotation(nPagina).Height)
                    Else
                        cb1.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0)
                    End If
                    nPagina += 1
                End While
                document1.Close()
                reader.Close()
            Next
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub RiunisciFile()
        Dim page As PdfImportedPage
        Dim document1 As Document
        Dim writer1 As PdfWriter
        Dim reader As PdfReader
        Dim nPagine As Integer
        Dim nPagina As Integer
        Dim nFile As Integer
        Dim cb1 As PdfContentByte

        Try
            nPagine = -1
            nFile = 0
            writer1 = Nothing
            document1 = Nothing
            For Each cTmp As String In System.IO.Directory.GetFiles(Folders("PrnWrkFLD"), GPS.TokenElaborazione + "*")
                If ((nPagine > 1000) Or (nPagine = -1)) Then
                    If (nPagine <> -1) Then
                        document1.Close()
                    End If
                    nFile += 1
                    nPagine = 0
                    document1 = New Document()
                    writer1 = PdfWriter.GetInstance(document1, New System.IO.FileStream(GSS.Folders("ElaboratiFLD") + "Elaborato_" + Timestamp() + ".pdf", System.IO.FileMode.Create))
                    document1.Open()
                End If
                reader = New PdfReader(cTmp)
                For nPagina = 1 To reader.NumberOfPages
                    cb1 = writer1.DirectContent
                    document1.SetPageSize(reader.GetPageSizeWithRotation(nPagina))
                    document1.NewPage()
                    page = writer1.GetImportedPage(reader, nPagina)
                    cb1.AddTemplate(page, 1.0F, 0, 0, 1.0F, 0, 0)
                Next
                nPagine += (nPagina - 1)
            Next
            If (nPagine > 0) Then
                document1.Close()
            End If
            reader = Nothing
        Catch e As Exception

        End Try

    End Sub

#End Region

    Private Sub Restore_PDFPrinterParam()

        If _prnPDF IsNot Nothing Then
            _prnPDF.RestorePrinterParam()
            _prnPDF = Nothing
        End If

    End Sub

    Private Sub RimuoviDatiInvioByMail(ByVal lDocumentofuso As Boolean)
        Dim DaInviareViaMail As Dictionary(Of Integer, SendByMailItemEntity)
        Dim si As SendByMailItemEntity
        Dim dr_mai_qd As DataRow

        '
        ' Recupero il codice del recod del quick dataset che stiamo stampando
        '
        If lDocumentofuso Then
            dr_mai_qd = ObjGlobali.GestioneSessioneStampa.QuickDataTable.Rows(ObjGlobali.GestioneSessioneStampa.CurrentRecordNumber - 1)
        Else
            dr_mai_qd = ObjGlobali.GestioneSessioneStampa.QuickDataTable.Select(String.Concat(ObjGlobali.GestioneSessioneStampa.QuickDataset_MainTableKey, " = ", ObjGlobali.GestioneSessioneStampa.FullDataSet_MainTable.Rows(0).Item(ObjGlobali.GestioneSessioneStampa.LinkField_QuickDS_FullDS)))(0)
        End If

        If dr_mai_qd IsNot Nothing AndAlso dr_mai_qd("DEF_sendbymail") Then
            si = Nothing

            '
            ' Se esiste il file sendbymail esiste allora lo leggo e cerco il recor dche mi interessa
            '
            If (System.IO.File.Exists(ObjGlobali.GestioneSessioneStampa.Files("DatiSpedizioneEmailFIL"))) Then
                Try
                    DaInviareViaMail = CType(OGC_utilitaXML.DeserializeXML(ObjGlobali.GestioneSessioneStampa.Files("DatiSpedizioneEmailFIL"), GetType(List(Of SendByMailItemEntity))), List(Of SendByMailItemEntity)).ToDictionary(Of Integer, SendByMailItemEntity)(Function(p) p.MAI_codice, Function(p) p)
                Catch ex As Exception
                    DaInviareViaMail = New Dictionary(Of Integer, SendByMailItemEntity)
                End Try
                If DaInviareViaMail.ContainsKey(dr_mai_qd.Item("MAI_codice")) Then
                    si = DaInviareViaMail(dr_mai_qd.Item("MAI_codice"))
                End If
            Else
                DaInviareViaMail = New Dictionary(Of Integer, SendByMailItemEntity)
            End If
            If (si IsNot Nothing) Then
                If lDocumentofuso Then
                    si.ListaDocumenti = si.ListaDocumenti.Where(Function(x) x.DOC_cod_mod <> 900).ToList
                Else
                    si.ListaDocumenti = si.ListaDocumenti.Where(Function(x) x.DOC_cod_mod <> GMS.CodiceModulo).ToList
                End If
            End If
            SerializzaXML(ObjGlobali.GestioneSessioneStampa.Files("DatiSpedizioneEmailFIL"), DaInviareViaMail.Values.ToList, GetType(List(Of SendByMailItemEntity)))
        End If

    End Sub

    '
    ' Salvo i dati per l'invio dei documenti nella file degli invii
    '
    Private Sub SaveSendByMail(ByVal lDocumentoFuso As Boolean)
        Dim DaInviareViaMail As Dictionary(Of Integer, SendByMailItemEntity)
        Dim si As SendByMailItemEntity
        Dim doc As DocumentoEntity
        Dim dr_mai_qd As DataRow
        Dim lLoop As Boolean

        Try
            '
            ' Recupero il codice del recod del quick dataset che stiamo stampando
            '
            If lDocumentoFuso Then
                dr_mai_qd = ObjGlobali.GestioneSessioneStampa.QuickDataTable.Rows(ObjGlobali.GestioneSessioneStampa.CurrentRecordNumber - 1)
            Else
                dr_mai_qd = ObjGlobali.GestioneSessioneStampa.QuickDataTable.Select(String.Concat(ObjGlobali.GestioneSessioneStampa.QuickDataset_MainTableKey, " = ", ObjGlobali.GestioneSessioneStampa.FullDataSet_MainTable.Rows(0).Item(ObjGlobali.GestioneSessioneStampa.LinkField_QuickDS_FullDS)))(0)
            End If

            If (GMS Is Nothing) Then
                lLoop = True
            Else
                lLoop = (GMS.DatiPerStampa.LoopSinteticoDettaglio <> 2)
            End If

            If dr_mai_qd IsNot Nothing And ((dr_mai_qd("DEF_sendbymail") Or GPS.FondiModuli) And lLoop) Then
                si = Nothing

                '
                ' Se esiste il file sendbymail esiste allora lo leggo e cerco il recor dche mi interessa
                '
                If (System.IO.File.Exists(ObjGlobali.GestioneSessioneStampa.Files("DatiSpedizioneEmailFIL"))) Then
                    Try
                        DaInviareViaMail = CType(OGC_utilitaXML.DeserializeXML(ObjGlobali.GestioneSessioneStampa.Files("DatiSpedizioneEmailFIL"), GetType(List(Of SendByMailItemEntity))), List(Of SendByMailItemEntity)).ToDictionary(Of Integer, SendByMailItemEntity)(Function(p) p.MAI_codice, Function(p) p)
                    Catch ex As Exception
                        DaInviareViaMail = New Dictionary(Of Integer, SendByMailItemEntity)
                    End Try

                    If DaInviareViaMail.ContainsKey(dr_mai_qd.Item("MAI_codice")) Then
                        si = DaInviareViaMail(dr_mai_qd.Item("MAI_codice"))
                    End If
                Else
                    DaInviareViaMail = New Dictionary(Of Integer, SendByMailItemEntity)
                End If
                If (si Is Nothing) Then
                    si = New SendByMailItemEntity()
                    si.MAI_codice = dr_mai_qd.Item("MAI_codice")
                    DaInviareViaMail.Add(si.MAI_codice, si)
                End If

                doc = New DocumentoEntity
                If lDocumentoFuso Then
                    doc.DOC_cod_mod = 900
                    doc.DOC_filename = DatiFusione.NomeDocumentoFuso.Replace(ObjGlobali.GestioneSessioneStampa.Folders(MSE_Costanti.FLD_sessioneroot), "")
                Else
                    doc.DOC_cod_mod = GMS.CodiceModulo
                    doc.DOC_filename = GMS.DatiPerStampa.ArchiviazioneOtticaFN.Replace(ObjGlobali.GestioneSessioneStampa.Folders(MSE_Costanti.FLD_sessioneroot), "")
                End If
                si.ListaDocumenti.Add(doc)
                SerializzaXML(ObjGlobali.GestioneSessioneStampa.Files("DatiSpedizioneEmailFIL"), DaInviareViaMail.Values.ToList, GetType(List(Of SendByMailItemEntity)))
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

End Class
