﻿Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports ISC.LibrerieComuni.ManageSessione

Module ObjGlobali

    '
    ' Proprietà principali
    '
    Public Property CFG As CLS_cfg_inv
    Public Property GestioneModuliEsportare As Dictionary(Of Integer, CLS_GestioneModuloStampare)
    Public Property GestioneModuliStampare As Dictionary(Of Integer, CLS_GestioneModuloStampare)
    Public Property GestioneProcessoStampa As CLS_GestioneProcessoStampa
    Public Property GestioneSessioneStampa As GestioneSessione
    Public Property Logger As CLS_logger
    '
    ' Proprietà derivate da quelle precedenti
    '
    Public Property Folders As CLS_FileFolderDic

        Get
            Return ObjGlobali.CFG.Folders
        End Get
        Set(ByVal value As CLS_FileFolderDic)
            ObjGlobali.CFG.Folders = value
        End Set

    End Property

End Module