Imports ISC.LibrerieComuni.ManageSessione
Imports ISC.LibrerieComuni.ManageSessione.MSE_Costanti

Public Class FRM_attach_mail

    Private _GestSess As GestioneSessione

    Public WriteOnly Property GestSess() As GestioneSessione

        Set(ByVal value As GestioneSessione)
            _GestSess = value
        End Set

    End Property

    Private Sub BTN_add_attach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_add_attach.Click
        Dim cFileSrc As String
        Dim cFileDst As String

        Try
            cFileSrc = TXB_attach_fil.Text
            cFileDst = String.Concat(_GestSess.Folders("AllegatiFLD"), System.IO.Path.GetFileName(cFileSrc))
            If System.IO.File.Exists(cFileSrc) Then
                System.IO.File.Copy(cFileSrc, cFileDst)
                _GestSess.AppendAttachFile(cFileDst.Replace(_GestSess.Folders(FLD_sessioneroot), ""))
            End If
            MessageBox.Show("Caricamento completato con successo.", "Caricamento corretto", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(String.Concat("Caricamento completato con errore.", vbLf, ex.Message), "Caricamento corretto", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub BTN_attach_fil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_attach_fil.Click
        Dim cPath As String = ""
        Dim cFile As String = ""

        OFD_new_file.Multiselect = False
        cFile = ""
        If TXB_attach_fil.Text.EndsWith("\") Then
            cPath = TXB_attach_fil.Text
        ElseIf TXB_attach_fil.Text = "" Then
            cPath = _GestSess.WorkFolder
        Else
            If System.IO.File.Exists(TXB_attach_fil.Text) Then
                cFile = System.IO.Path.GetFileName(TXB_attach_fil.Text)
                cPath = TXB_attach_fil.Text.Replace(cFile, "")
            Else
                cPath = System.IO.Path.GetDirectoryName(TXB_attach_fil.Text)
            End If
        End If
        OFD_new_file.InitialDirectory = cPath
        OFD_new_file.FileName = cFile

        If OFD_new_file.ShowDialog = Windows.Forms.DialogResult.OK Then
            TXB_attach_fil.Text = OFD_new_file.FileName
        End If

    End Sub

End Class