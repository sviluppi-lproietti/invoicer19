<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_fake
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue � richiesta da Progettazione Windows Form
    'Pu� essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TAC_modulistampa = New System.Windows.Forms.TabControl()
        Me.TAP_modulo_00 = New System.Windows.Forms.TabPage()
        Me.LBL_stato_modulo_00 = New System.Windows.Forms.Label()
        Me.FLP_moduli_00 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GRB_stampante_car_00 = New System.Windows.Forms.GroupBox()
        Me.CHB_reset_cod_wr_car_00 = New System.Windows.Forms.CheckBox()
        Me.CHB_insert_fincatura_00 = New System.Windows.Forms.CheckBox()
        Me.TXB_cod_wr_car_00 = New System.Windows.Forms.TextBox()
        Me.CHB_fronte_retro_00 = New System.Windows.Forms.CheckBox()
        Me.LBL_stampante_00 = New System.Windows.Forms.Label()
        Me.CMB_stampanti_00 = New System.Windows.Forms.ComboBox()
        Me.DGV_ass_cassetti_00 = New System.Windows.Forms.DataGridView()
        Me.Pagina = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cassetto = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.LBL_fat_cor_x_00 = New System.Windows.Forms.Label()
        Me.RDB_cassetti_00 = New System.Windows.Forms.RadioButton()
        Me.TXB_fat_cor_car_x_00 = New System.Windows.Forms.TextBox()
        Me.CMB_cassetto_def_00 = New System.Windows.Forms.ComboBox()
        Me.LBL_fat_cor_y_00 = New System.Windows.Forms.Label()
        Me.RDB_default_00 = New System.Windows.Forms.RadioButton()
        Me.TXB_fat_cor_car_y_00 = New System.Windows.Forms.TextBox()
        Me.GRB_stampante_ele_00 = New System.Windows.Forms.GroupBox()
        Me.CHB_rendi_pari_00 = New System.Windows.Forms.CheckBox()
        Me.CHB_reset_cod_wr_ele_00 = New System.Windows.Forms.CheckBox()
        Me.TXB_cod_wr_ele_00 = New System.Windows.Forms.TextBox()
        Me.LBL_nome_file_arch_ott = New System.Windows.Forms.Label()
        Me.CMB_file_arch_ott_00 = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TXB_fat_cor_ele_x_00 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TXB_fat_cor_ele_y_00 = New System.Windows.Forms.TextBox()
        Me.GRB_generazione_00 = New System.Windows.Forms.GroupBox()
        Me.TXB_Altezza_Barcode_00 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TXB_Coordinata_Y_00 = New System.Windows.Forms.TextBox()
        Me.TXB_Coordinata_X_00 = New System.Windows.Forms.TextBox()
        Me.CHB_modulo_da_stampare_00 = New System.Windows.Forms.CheckBox()
        Me.CHB_stampa_dettaglio_00 = New System.Windows.Forms.CheckBox()
        Me.TAC_modulistampa.SuspendLayout()
        Me.TAP_modulo_00.SuspendLayout()
        Me.FLP_moduli_00.SuspendLayout()
        Me.GRB_stampante_car_00.SuspendLayout()
        CType(Me.DGV_ass_cassetti_00, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRB_stampante_ele_00.SuspendLayout()
        Me.GRB_generazione_00.SuspendLayout()
        Me.SuspendLayout()
        '
        'TAC_modulistampa
        '
        Me.TAC_modulistampa.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TAC_modulistampa.Controls.Add(Me.TAP_modulo_00)
        Me.TAC_modulistampa.Location = New System.Drawing.Point(70, 35)
        Me.TAC_modulistampa.Name = "TAC_modulistampa"
        Me.TAC_modulistampa.SelectedIndex = 0
        Me.TAC_modulistampa.Size = New System.Drawing.Size(1013, 471)
        Me.TAC_modulistampa.TabIndex = 19
        '
        'TAP_modulo_00
        '
        Me.TAP_modulo_00.Controls.Add(Me.CHB_stampa_dettaglio_00)
        Me.TAP_modulo_00.Controls.Add(Me.LBL_stato_modulo_00)
        Me.TAP_modulo_00.Controls.Add(Me.FLP_moduli_00)
        Me.TAP_modulo_00.Controls.Add(Me.CHB_modulo_da_stampare_00)
        Me.TAP_modulo_00.Location = New System.Drawing.Point(4, 25)
        Me.TAP_modulo_00.Name = "TAP_modulo_00"
        Me.TAP_modulo_00.Padding = New System.Windows.Forms.Padding(3)
        Me.TAP_modulo_00.Size = New System.Drawing.Size(1005, 442)
        Me.TAP_modulo_00.TabIndex = 0
        Me.TAP_modulo_00.Text = "TabPage1"
        Me.TAP_modulo_00.UseVisualStyleBackColor = True
        '
        'LBL_stato_modulo_00
        '
        Me.LBL_stato_modulo_00.AutoSize = True
        Me.LBL_stato_modulo_00.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_stato_modulo_00.Location = New System.Drawing.Point(143, 5)
        Me.LBL_stato_modulo_00.Name = "LBL_stato_modulo_00"
        Me.LBL_stato_modulo_00.Size = New System.Drawing.Size(34, 16)
        Me.LBL_stato_modulo_00.TabIndex = 18
        Me.LBL_stato_modulo_00.Text = "N/A"
        '
        'FLP_moduli_00
        '
        Me.FLP_moduli_00.Controls.Add(Me.GRB_stampante_car_00)
        Me.FLP_moduli_00.Controls.Add(Me.GRB_stampante_ele_00)
        Me.FLP_moduli_00.Controls.Add(Me.GRB_generazione_00)
        Me.FLP_moduli_00.Location = New System.Drawing.Point(6, 29)
        Me.FLP_moduli_00.Name = "FLP_moduli_00"
        Me.FLP_moduli_00.Size = New System.Drawing.Size(993, 410)
        Me.FLP_moduli_00.TabIndex = 17
        '
        'GRB_stampante_car_00
        '
        Me.GRB_stampante_car_00.AutoSize = True
        Me.GRB_stampante_car_00.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.GRB_stampante_car_00.Controls.Add(Me.CHB_reset_cod_wr_car_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.CHB_insert_fincatura_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.TXB_cod_wr_car_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.CHB_fronte_retro_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.LBL_stampante_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.CMB_stampanti_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.DGV_ass_cassetti_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.LBL_fat_cor_x_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.RDB_cassetti_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.TXB_fat_cor_car_x_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.CMB_cassetto_def_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.LBL_fat_cor_y_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.RDB_default_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.TXB_fat_cor_car_y_00)
        Me.GRB_stampante_car_00.Location = New System.Drawing.Point(3, 3)
        Me.GRB_stampante_car_00.Name = "GRB_stampante_car_00"
        Me.GRB_stampante_car_00.Size = New System.Drawing.Size(985, 235)
        Me.GRB_stampante_car_00.TabIndex = 3
        Me.GRB_stampante_car_00.TabStop = False
        Me.GRB_stampante_car_00.Text = "Impostazioni stampante cartacea"
        '
        'CHB_reset_cod_wr_car_00
        '
        Me.CHB_reset_cod_wr_car_00.AutoSize = True
        Me.CHB_reset_cod_wr_car_00.Location = New System.Drawing.Point(748, 77)
        Me.CHB_reset_cod_wr_car_00.Name = "CHB_reset_cod_wr_car_00"
        Me.CHB_reset_cod_wr_car_00.Size = New System.Drawing.Size(164, 17)
        Me.CHB_reset_cod_wr_car_00.TabIndex = 10
        Me.CHB_reset_cod_wr_car_00.Text = "Resetta codice Wrap Around"
        Me.CHB_reset_cod_wr_car_00.UseVisualStyleBackColor = True
        '
        'CHB_insert_fincatura_00
        '
        Me.CHB_insert_fincatura_00.AutoSize = True
        Me.CHB_insert_fincatura_00.Location = New System.Drawing.Point(748, 100)
        Me.CHB_insert_fincatura_00.Name = "CHB_insert_fincatura_00"
        Me.CHB_insert_fincatura_00.Size = New System.Drawing.Size(111, 17)
        Me.CHB_insert_fincatura_00.TabIndex = 12
        Me.CHB_insert_fincatura_00.Text = "Inserisci Fincatura"
        Me.CHB_insert_fincatura_00.UseVisualStyleBackColor = True
        '
        'TXB_cod_wr_car_00
        '
        Me.TXB_cod_wr_car_00.Location = New System.Drawing.Point(927, 75)
        Me.TXB_cod_wr_car_00.Name = "TXB_cod_wr_car_00"
        Me.TXB_cod_wr_car_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_cod_wr_car_00.TabIndex = 11
        Me.TXB_cod_wr_car_00.Text = "0"
        Me.TXB_cod_wr_car_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CHB_fronte_retro_00
        '
        Me.CHB_fronte_retro_00.AutoSize = True
        Me.CHB_fronte_retro_00.Location = New System.Drawing.Point(748, 123)
        Me.CHB_fronte_retro_00.Name = "CHB_fronte_retro_00"
        Me.CHB_fronte_retro_00.Size = New System.Drawing.Size(121, 17)
        Me.CHB_fronte_retro_00.TabIndex = 13
        Me.CHB_fronte_retro_00.Text = "Stampa Fronte/retro"
        Me.CHB_fronte_retro_00.UseVisualStyleBackColor = True
        '
        'LBL_stampante_00
        '
        Me.LBL_stampante_00.AutoSize = True
        Me.LBL_stampante_00.Location = New System.Drawing.Point(3, 24)
        Me.LBL_stampante_00.Name = "LBL_stampante_00"
        Me.LBL_stampante_00.Size = New System.Drawing.Size(58, 13)
        Me.LBL_stampante_00.TabIndex = 0
        Me.LBL_stampante_00.Text = "Stampante"
        '
        'CMB_stampanti_00
        '
        Me.CMB_stampanti_00.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_stampanti_00.FormattingEnabled = True
        Me.CMB_stampanti_00.Location = New System.Drawing.Point(129, 21)
        Me.CMB_stampanti_00.Name = "CMB_stampanti_00"
        Me.CMB_stampanti_00.Size = New System.Drawing.Size(554, 21)
        Me.CMB_stampanti_00.TabIndex = 1
        '
        'DGV_ass_cassetti_00
        '
        Me.DGV_ass_cassetti_00.AllowUserToAddRows = False
        Me.DGV_ass_cassetti_00.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_ass_cassetti_00.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV_ass_cassetti_00.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_ass_cassetti_00.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Pagina, Me.Cassetto})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_ass_cassetti_00.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGV_ass_cassetti_00.Location = New System.Drawing.Point(129, 75)
        Me.DGV_ass_cassetti_00.Name = "DGV_ass_cassetti_00"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_ass_cassetti_00.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DGV_ass_cassetti_00.RowHeadersWidth = 4
        Me.DGV_ass_cassetti_00.Size = New System.Drawing.Size(554, 141)
        Me.DGV_ass_cassetti_00.TabIndex = 5
        '
        'Pagina
        '
        Me.Pagina.DataPropertyName = "CAP_desc_pag"
        Me.Pagina.HeaderText = "Pagine"
        Me.Pagina.Name = "Pagina"
        Me.Pagina.ReadOnly = True
        Me.Pagina.Width = 200
        '
        'Cassetto
        '
        Me.Cassetto.DataPropertyName = "CAP_cassetto"
        Me.Cassetto.HeaderText = "Cassetto"
        Me.Cassetto.Name = "Cassetto"
        Me.Cassetto.Width = 330
        '
        'LBL_fat_cor_x_00
        '
        Me.LBL_fat_cor_x_00.AutoSize = True
        Me.LBL_fat_cor_x_00.Location = New System.Drawing.Point(765, 24)
        Me.LBL_fat_cor_x_00.Name = "LBL_fat_cor_x_00"
        Me.LBL_fat_cor_x_00.Size = New System.Drawing.Size(113, 13)
        Me.LBL_fat_cor_x_00.TabIndex = 6
        Me.LBL_fat_cor_x_00.Text = "Fattore di correzione X"
        '
        'RDB_cassetti_00
        '
        Me.RDB_cassetti_00.AutoSize = True
        Me.RDB_cassetti_00.Location = New System.Drawing.Point(6, 80)
        Me.RDB_cassetti_00.Name = "RDB_cassetti_00"
        Me.RDB_cassetti_00.Size = New System.Drawing.Size(110, 17)
        Me.RDB_cassetti_00.TabIndex = 4
        Me.RDB_cassetti_00.Text = "Seleziona cassetti"
        Me.RDB_cassetti_00.UseVisualStyleBackColor = True
        '
        'TXB_fat_cor_car_x_00
        '
        Me.TXB_fat_cor_car_x_00.Location = New System.Drawing.Point(927, 21)
        Me.TXB_fat_cor_car_x_00.Name = "TXB_fat_cor_car_x_00"
        Me.TXB_fat_cor_car_x_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_fat_cor_car_x_00.TabIndex = 7
        Me.TXB_fat_cor_car_x_00.Text = "0"
        Me.TXB_fat_cor_car_x_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CMB_cassetto_def_00
        '
        Me.CMB_cassetto_def_00.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_cassetto_def_00.FormattingEnabled = True
        Me.CMB_cassetto_def_00.Location = New System.Drawing.Point(129, 48)
        Me.CMB_cassetto_def_00.Name = "CMB_cassetto_def_00"
        Me.CMB_cassetto_def_00.Size = New System.Drawing.Size(554, 21)
        Me.CMB_cassetto_def_00.TabIndex = 3
        '
        'LBL_fat_cor_y_00
        '
        Me.LBL_fat_cor_y_00.AutoSize = True
        Me.LBL_fat_cor_y_00.Location = New System.Drawing.Point(765, 50)
        Me.LBL_fat_cor_y_00.Name = "LBL_fat_cor_y_00"
        Me.LBL_fat_cor_y_00.Size = New System.Drawing.Size(113, 13)
        Me.LBL_fat_cor_y_00.TabIndex = 8
        Me.LBL_fat_cor_y_00.Text = "Fattore di correzione Y"
        '
        'RDB_default_00
        '
        Me.RDB_default_00.AutoSize = True
        Me.RDB_default_00.Checked = True
        Me.RDB_default_00.Location = New System.Drawing.Point(6, 49)
        Me.RDB_default_00.Name = "RDB_default_00"
        Me.RDB_default_00.Size = New System.Drawing.Size(112, 17)
        Me.RDB_default_00.TabIndex = 2
        Me.RDB_default_00.TabStop = True
        Me.RDB_default_00.Text = "Cassetto di default"
        Me.RDB_default_00.UseVisualStyleBackColor = True
        '
        'TXB_fat_cor_car_y_00
        '
        Me.TXB_fat_cor_car_y_00.Location = New System.Drawing.Point(927, 48)
        Me.TXB_fat_cor_car_y_00.Name = "TXB_fat_cor_car_y_00"
        Me.TXB_fat_cor_car_y_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_fat_cor_car_y_00.TabIndex = 9
        Me.TXB_fat_cor_car_y_00.Text = "0"
        Me.TXB_fat_cor_car_y_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GRB_stampante_ele_00
        '
        Me.GRB_stampante_ele_00.AutoSize = True
        Me.GRB_stampante_ele_00.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.GRB_stampante_ele_00.Controls.Add(Me.CHB_rendi_pari_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.CHB_reset_cod_wr_ele_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.TXB_cod_wr_ele_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.LBL_nome_file_arch_ott)
        Me.GRB_stampante_ele_00.Controls.Add(Me.CMB_file_arch_ott_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.Label25)
        Me.GRB_stampante_ele_00.Controls.Add(Me.TXB_fat_cor_ele_x_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.Label29)
        Me.GRB_stampante_ele_00.Controls.Add(Me.TXB_fat_cor_ele_y_00)
        Me.GRB_stampante_ele_00.Location = New System.Drawing.Point(3, 244)
        Me.GRB_stampante_ele_00.Name = "GRB_stampante_ele_00"
        Me.GRB_stampante_ele_00.Size = New System.Drawing.Size(985, 82)
        Me.GRB_stampante_ele_00.TabIndex = 17
        Me.GRB_stampante_ele_00.TabStop = False
        Me.GRB_stampante_ele_00.Text = "Impostazioni stampante elettronica"
        '
        'CHB_rendi_pari_00
        '
        Me.CHB_rendi_pari_00.AutoSize = True
        Me.CHB_rendi_pari_00.Location = New System.Drawing.Point(9, 45)
        Me.CHB_rendi_pari_00.Name = "CHB_rendi_pari_00"
        Me.CHB_rendi_pari_00.Size = New System.Drawing.Size(148, 17)
        Me.CHB_rendi_pari_00.TabIndex = 5
        Me.CHB_rendi_pari_00.Text = "Pareggia il numero pagine"
        Me.CHB_rendi_pari_00.UseVisualStyleBackColor = True
        '
        'CHB_reset_cod_wr_ele_00
        '
        Me.CHB_reset_cod_wr_ele_00.AutoSize = True
        Me.CHB_reset_cod_wr_ele_00.Location = New System.Drawing.Point(163, 45)
        Me.CHB_reset_cod_wr_ele_00.Name = "CHB_reset_cod_wr_ele_00"
        Me.CHB_reset_cod_wr_ele_00.Size = New System.Drawing.Size(164, 17)
        Me.CHB_reset_cod_wr_ele_00.TabIndex = 6
        Me.CHB_reset_cod_wr_ele_00.Text = "Resetta codice Wrap Around"
        Me.CHB_reset_cod_wr_ele_00.UseVisualStyleBackColor = True
        '
        'TXB_cod_wr_ele_00
        '
        Me.TXB_cod_wr_ele_00.Location = New System.Drawing.Point(333, 43)
        Me.TXB_cod_wr_ele_00.Name = "TXB_cod_wr_ele_00"
        Me.TXB_cod_wr_ele_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_cod_wr_ele_00.TabIndex = 7
        Me.TXB_cod_wr_ele_00.Text = "0"
        Me.TXB_cod_wr_ele_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LBL_nome_file_arch_ott
        '
        Me.LBL_nome_file_arch_ott.AutoSize = True
        Me.LBL_nome_file_arch_ott.Location = New System.Drawing.Point(6, 21)
        Me.LBL_nome_file_arch_ott.Name = "LBL_nome_file_arch_ott"
        Me.LBL_nome_file_arch_ott.Size = New System.Drawing.Size(68, 13)
        Me.LBL_nome_file_arch_ott.TabIndex = 0
        Me.LBL_nome_file_arch_ott.Text = "Nome del file"
        '
        'CMB_file_arch_ott_00
        '
        Me.CMB_file_arch_ott_00.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_file_arch_ott_00.FormattingEnabled = True
        Me.CMB_file_arch_ott_00.Location = New System.Drawing.Point(128, 18)
        Me.CMB_file_arch_ott_00.Name = "CMB_file_arch_ott_00"
        Me.CMB_file_arch_ott_00.Size = New System.Drawing.Size(555, 21)
        Me.CMB_file_arch_ott_00.TabIndex = 1
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(765, 21)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(113, 13)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "Fattore di correzione X"
        '
        'TXB_fat_cor_ele_x_00
        '
        Me.TXB_fat_cor_ele_x_00.Location = New System.Drawing.Point(927, 18)
        Me.TXB_fat_cor_ele_x_00.Name = "TXB_fat_cor_ele_x_00"
        Me.TXB_fat_cor_ele_x_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_fat_cor_ele_x_00.TabIndex = 3
        Me.TXB_fat_cor_ele_x_00.Text = "0"
        Me.TXB_fat_cor_ele_x_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(765, 46)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(113, 13)
        Me.Label29.TabIndex = 8
        Me.Label29.Text = "Fattore di correzione Y"
        '
        'TXB_fat_cor_ele_y_00
        '
        Me.TXB_fat_cor_ele_y_00.Location = New System.Drawing.Point(927, 43)
        Me.TXB_fat_cor_ele_y_00.Name = "TXB_fat_cor_ele_y_00"
        Me.TXB_fat_cor_ele_y_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_fat_cor_ele_y_00.TabIndex = 9
        Me.TXB_fat_cor_ele_y_00.Text = "0"
        Me.TXB_fat_cor_ele_y_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GRB_generazione_00
        '
        Me.GRB_generazione_00.Controls.Add(Me.TXB_Altezza_Barcode_00)
        Me.GRB_generazione_00.Controls.Add(Me.Label1)
        Me.GRB_generazione_00.Controls.Add(Me.Label27)
        Me.GRB_generazione_00.Controls.Add(Me.Label28)
        Me.GRB_generazione_00.Controls.Add(Me.TXB_Coordinata_Y_00)
        Me.GRB_generazione_00.Controls.Add(Me.TXB_Coordinata_X_00)
        Me.GRB_generazione_00.Location = New System.Drawing.Point(3, 332)
        Me.GRB_generazione_00.Name = "GRB_generazione_00"
        Me.GRB_generazione_00.Size = New System.Drawing.Size(985, 51)
        Me.GRB_generazione_00.TabIndex = 16
        Me.GRB_generazione_00.TabStop = False
        Me.GRB_generazione_00.Text = "Coordinate del barcode di imbustamento"
        Me.GRB_generazione_00.Visible = False
        '
        'TXB_Altezza_Barcode_00
        '
        Me.TXB_Altezza_Barcode_00.Location = New System.Drawing.Point(712, 19)
        Me.TXB_Altezza_Barcode_00.Name = "TXB_Altezza_Barcode_00"
        Me.TXB_Altezza_Barcode_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_Altezza_Barcode_00.TabIndex = 19
        Me.TXB_Altezza_Barcode_00.Text = "0"
        Me.TXB_Altezza_Barcode_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(604, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Altezza Barcode"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(6, 22)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(68, 13)
        Me.Label27.TabIndex = 16
        Me.Label27.Text = "Coordinata X"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(340, 22)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(68, 13)
        Me.Label28.TabIndex = 17
        Me.Label28.Text = "Coordinata Y"
        '
        'TXB_Coordinata_Y_00
        '
        Me.TXB_Coordinata_Y_00.Location = New System.Drawing.Point(509, 19)
        Me.TXB_Coordinata_Y_00.Name = "TXB_Coordinata_Y_00"
        Me.TXB_Coordinata_Y_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_Coordinata_Y_00.TabIndex = 17
        Me.TXB_Coordinata_Y_00.Text = "0"
        Me.TXB_Coordinata_Y_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_Coordinata_X_00
        '
        Me.TXB_Coordinata_X_00.Location = New System.Drawing.Point(175, 19)
        Me.TXB_Coordinata_X_00.Name = "TXB_Coordinata_X_00"
        Me.TXB_Coordinata_X_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_Coordinata_X_00.TabIndex = 16
        Me.TXB_Coordinata_X_00.Text = "0"
        Me.TXB_Coordinata_X_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CHB_modulo_da_stampare_00
        '
        Me.CHB_modulo_da_stampare_00.AutoSize = True
        Me.CHB_modulo_da_stampare_00.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CHB_modulo_da_stampare_00.Location = New System.Drawing.Point(9, 6)
        Me.CHB_modulo_da_stampare_00.Name = "CHB_modulo_da_stampare_00"
        Me.CHB_modulo_da_stampare_00.Size = New System.Drawing.Size(128, 17)
        Me.CHB_modulo_da_stampare_00.TabIndex = 16
        Me.CHB_modulo_da_stampare_00.Text = "Modulo da stampare?"
        Me.CHB_modulo_da_stampare_00.UseVisualStyleBackColor = True
        '
        'CHB_stampa_dettaglio_00
        '
        Me.CHB_stampa_dettaglio_00.AutoSize = True
        Me.CHB_stampa_dettaglio_00.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CHB_stampa_dettaglio_00.Location = New System.Drawing.Point(832, 6)
        Me.CHB_stampa_dettaglio_00.Name = "CHB_stampa_dettaglio_00"
        Me.CHB_stampa_dettaglio_00.Size = New System.Drawing.Size(167, 17)
        Me.CHB_stampa_dettaglio_00.TabIndex = 19
        Me.CHB_stampa_dettaglio_00.Text = "Stampa documento dettaglio?"
        Me.CHB_stampa_dettaglio_00.UseVisualStyleBackColor = True
        '
        'FRM_fake
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1153, 540)
        Me.Controls.Add(Me.TAC_modulistampa)
        Me.Name = "FRM_fake"
        Me.Text = "FRM_fake"
        Me.TAC_modulistampa.ResumeLayout(False)
        Me.TAP_modulo_00.ResumeLayout(False)
        Me.TAP_modulo_00.PerformLayout()
        Me.FLP_moduli_00.ResumeLayout(False)
        Me.FLP_moduli_00.PerformLayout()
        Me.GRB_stampante_car_00.ResumeLayout(False)
        Me.GRB_stampante_car_00.PerformLayout()
        CType(Me.DGV_ass_cassetti_00, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRB_stampante_ele_00.ResumeLayout(False)
        Me.GRB_stampante_ele_00.PerformLayout()
        Me.GRB_generazione_00.ResumeLayout(False)
        Me.GRB_generazione_00.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TAC_modulistampa As System.Windows.Forms.TabControl
    Friend WithEvents TAP_modulo_00 As System.Windows.Forms.TabPage
    Friend WithEvents LBL_stato_modulo_00 As System.Windows.Forms.Label
    Friend WithEvents FLP_moduli_00 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents GRB_stampante_car_00 As System.Windows.Forms.GroupBox
    Friend WithEvents CHB_reset_cod_wr_car_00 As System.Windows.Forms.CheckBox
    Friend WithEvents CHB_insert_fincatura_00 As System.Windows.Forms.CheckBox
    Friend WithEvents TXB_cod_wr_car_00 As System.Windows.Forms.TextBox
    Friend WithEvents CHB_fronte_retro_00 As System.Windows.Forms.CheckBox
    Friend WithEvents LBL_stampante_00 As System.Windows.Forms.Label
    Friend WithEvents CMB_stampanti_00 As System.Windows.Forms.ComboBox
    Friend WithEvents DGV_ass_cassetti_00 As System.Windows.Forms.DataGridView
    Friend WithEvents Pagina As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cassetto As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents LBL_fat_cor_x_00 As System.Windows.Forms.Label
    Friend WithEvents RDB_cassetti_00 As System.Windows.Forms.RadioButton
    Friend WithEvents TXB_fat_cor_car_x_00 As System.Windows.Forms.TextBox
    Friend WithEvents CMB_cassetto_def_00 As System.Windows.Forms.ComboBox
    Friend WithEvents LBL_fat_cor_y_00 As System.Windows.Forms.Label
    Friend WithEvents RDB_default_00 As System.Windows.Forms.RadioButton
    Friend WithEvents TXB_fat_cor_car_y_00 As System.Windows.Forms.TextBox
    Friend WithEvents GRB_stampante_ele_00 As System.Windows.Forms.GroupBox
    Friend WithEvents CHB_rendi_pari_00 As System.Windows.Forms.CheckBox
    Friend WithEvents CHB_reset_cod_wr_ele_00 As System.Windows.Forms.CheckBox
    Friend WithEvents TXB_cod_wr_ele_00 As System.Windows.Forms.TextBox
    Friend WithEvents LBL_nome_file_arch_ott As System.Windows.Forms.Label
    Friend WithEvents CMB_file_arch_ott_00 As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TXB_fat_cor_ele_x_00 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents TXB_fat_cor_ele_y_00 As System.Windows.Forms.TextBox
    Friend WithEvents GRB_generazione_00 As System.Windows.Forms.GroupBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TXB_Coordinata_Y_00 As System.Windows.Forms.TextBox
    Friend WithEvents TXB_Coordinata_X_00 As System.Windows.Forms.TextBox
    Friend WithEvents CHB_modulo_da_stampare_00 As System.Windows.Forms.CheckBox
    Friend WithEvents TXB_Altezza_Barcode_00 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CHB_stampa_dettaglio_00 As System.Windows.Forms.CheckBox
End Class