<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_opzioni
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FBD_scelta = New System.Windows.Forms.FolderBrowserDialog()
        Me.BTN_ok = New System.Windows.Forms.Button()
        Me.BTN_ignore = New System.Windows.Forms.Button()
        Me.CHB_clean_work = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TXB_mod_cont = New System.Windows.Forms.TextBox()
        Me.CHB_dati_var = New System.Windows.Forms.CheckBox()
        Me.CMB_stampante_arch_ottica = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CHB_reset_wrap_round = New System.Windows.Forms.CheckBox()
        Me.TAC_opzioni = New System.Windows.Forms.TabControl()
        Me.TAP_generale = New System.Windows.Forms.TabPage()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TXB_max_num_fogli = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TXB_ses_tipo_fld = New System.Windows.Forms.TextBox()
        Me.BTN_ses_tipo_fld = New System.Windows.Forms.Button()
        Me.TXB_arc_sess_fld = New System.Windows.Forms.TextBox()
        Me.BTN_arc_sess_fld = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TXB_work_fld = New System.Windows.Forms.TextBox()
        Me.CMB_stampante_default = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BTN_work_fld = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TAP_arch_ott = New System.Windows.Forms.TabPage()
        Me.TXB_suffisso_parte = New System.Windows.Forms.TextBox()
        Me.TXB_pagine_x_parte = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TXB_dbgiriconnectionstring = New System.Windows.Forms.TextBox()
        Me.TAC_opzioni.SuspendLayout()
        Me.TAP_generale.SuspendLayout()
        Me.TAP_arch_ott.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BTN_ok
        '
        Me.BTN_ok.Location = New System.Drawing.Point(195, 270)
        Me.BTN_ok.Name = "BTN_ok"
        Me.BTN_ok.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ok.TabIndex = 3
        Me.BTN_ok.Text = "OK"
        Me.BTN_ok.UseVisualStyleBackColor = True
        '
        'BTN_ignore
        '
        Me.BTN_ignore.Location = New System.Drawing.Point(317, 270)
        Me.BTN_ignore.Name = "BTN_ignore"
        Me.BTN_ignore.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignore.TabIndex = 4
        Me.BTN_ignore.Text = "ANNULLA"
        Me.BTN_ignore.UseVisualStyleBackColor = True
        '
        'CHB_clean_work
        '
        Me.CHB_clean_work.AutoSize = True
        Me.CHB_clean_work.Location = New System.Drawing.Point(10, 116)
        Me.CHB_clean_work.Name = "CHB_clean_work"
        Me.CHB_clean_work.Size = New System.Drawing.Size(244, 17)
        Me.CHB_clean_work.TabIndex = 8
        Me.CHB_clean_work.Text = "Pulisci la cartella work quando apri la sessione"
        Me.CHB_clean_work.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.CHB_clean_work.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 169)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(206, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Documenti stampati contemporaneamente"
        '
        'TXB_mod_cont
        '
        Me.TXB_mod_cont.Location = New System.Drawing.Point(219, 166)
        Me.TXB_mod_cont.Name = "TXB_mod_cont"
        Me.TXB_mod_cont.Size = New System.Drawing.Size(69, 20)
        Me.TXB_mod_cont.TabIndex = 10
        '
        'CHB_dati_var
        '
        Me.CHB_dati_var.AutoSize = True
        Me.CHB_dati_var.Location = New System.Drawing.Point(10, 90)
        Me.CHB_dati_var.Name = "CHB_dati_var"
        Me.CHB_dati_var.Size = New System.Drawing.Size(206, 17)
        Me.CHB_dati_var.TabIndex = 7
        Me.CHB_dati_var.Text = "Ricorda sempre immisione dati variabili"
        Me.CHB_dati_var.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.CHB_dati_var.UseVisualStyleBackColor = True
        '
        'CMB_stampante_arch_ottica
        '
        Me.CMB_stampante_arch_ottica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_stampante_arch_ottica.FormattingEnabled = True
        Me.CMB_stampante_arch_ottica.Location = New System.Drawing.Point(129, 10)
        Me.CMB_stampante_arch_ottica.Name = "CMB_stampante_arch_ottica"
        Me.CMB_stampante_arch_ottica.Size = New System.Drawing.Size(414, 21)
        Me.CMB_stampante_arch_ottica.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(114, 13)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Stampante arch. ottica"
        '
        'CHB_reset_wrap_round
        '
        Me.CHB_reset_wrap_round.AutoSize = True
        Me.CHB_reset_wrap_round.Location = New System.Drawing.Point(301, 169)
        Me.CHB_reset_wrap_round.Name = "CHB_reset_wrap_round"
        Me.CHB_reset_wrap_round.Size = New System.Drawing.Size(271, 17)
        Me.CHB_reset_wrap_round.TabIndex = 11
        Me.CHB_reset_wrap_round.Text = "Resetta sempre codice Wrap Araund dove utilizzato"
        Me.CHB_reset_wrap_round.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.CHB_reset_wrap_round.UseVisualStyleBackColor = True
        '
        'TAC_opzioni
        '
        Me.TAC_opzioni.Controls.Add(Me.TAP_generale)
        Me.TAC_opzioni.Controls.Add(Me.TAP_arch_ott)
        Me.TAC_opzioni.Controls.Add(Me.TabPage1)
        Me.TAC_opzioni.Location = New System.Drawing.Point(12, 12)
        Me.TAC_opzioni.Name = "TAC_opzioni"
        Me.TAC_opzioni.SelectedIndex = 0
        Me.TAC_opzioni.Size = New System.Drawing.Size(586, 252)
        Me.TAC_opzioni.TabIndex = 20
        '
        'TAP_generale
        '
        Me.TAP_generale.Controls.Add(Me.Label7)
        Me.TAP_generale.Controls.Add(Me.TXB_max_num_fogli)
        Me.TAP_generale.Controls.Add(Me.Label1)
        Me.TAP_generale.Controls.Add(Me.CHB_reset_wrap_round)
        Me.TAP_generale.Controls.Add(Me.TXB_ses_tipo_fld)
        Me.TAP_generale.Controls.Add(Me.BTN_ses_tipo_fld)
        Me.TAP_generale.Controls.Add(Me.TXB_arc_sess_fld)
        Me.TAP_generale.Controls.Add(Me.Label5)
        Me.TAP_generale.Controls.Add(Me.TXB_mod_cont)
        Me.TAP_generale.Controls.Add(Me.CHB_dati_var)
        Me.TAP_generale.Controls.Add(Me.BTN_arc_sess_fld)
        Me.TAP_generale.Controls.Add(Me.Label2)
        Me.TAP_generale.Controls.Add(Me.CHB_clean_work)
        Me.TAP_generale.Controls.Add(Me.TXB_work_fld)
        Me.TAP_generale.Controls.Add(Me.CMB_stampante_default)
        Me.TAP_generale.Controls.Add(Me.Label4)
        Me.TAP_generale.Controls.Add(Me.BTN_work_fld)
        Me.TAP_generale.Controls.Add(Me.Label3)
        Me.TAP_generale.Location = New System.Drawing.Point(4, 22)
        Me.TAP_generale.Name = "TAP_generale"
        Me.TAP_generale.Padding = New System.Windows.Forms.Padding(3)
        Me.TAP_generale.Size = New System.Drawing.Size(578, 226)
        Me.TAP_generale.TabIndex = 0
        Me.TAP_generale.Text = "Generale"
        Me.TAP_generale.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 195)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(171, 13)
        Me.Label7.TabIndex = 25
        Me.Label7.Text = "Numero massimo di fogli per moduli"
        '
        'TXB_max_num_fogli
        '
        Me.TXB_max_num_fogli.Location = New System.Drawing.Point(219, 192)
        Me.TXB_max_num_fogli.Name = "TXB_max_num_fogli"
        Me.TXB_max_num_fogli.Size = New System.Drawing.Size(69, 20)
        Me.TXB_max_num_fogli.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Cartella sessioni tipo"
        '
        'TXB_ses_tipo_fld
        '
        Me.TXB_ses_tipo_fld.Location = New System.Drawing.Point(130, 14)
        Me.TXB_ses_tipo_fld.Name = "TXB_ses_tipo_fld"
        Me.TXB_ses_tipo_fld.Size = New System.Drawing.Size(408, 20)
        Me.TXB_ses_tipo_fld.TabIndex = 1
        '
        'BTN_ses_tipo_fld
        '
        Me.BTN_ses_tipo_fld.Image = Global.Invoicer.My.Resources.Resources.PNG_folder
        Me.BTN_ses_tipo_fld.Location = New System.Drawing.Point(544, 12)
        Me.BTN_ses_tipo_fld.Name = "BTN_ses_tipo_fld"
        Me.BTN_ses_tipo_fld.Size = New System.Drawing.Size(27, 23)
        Me.BTN_ses_tipo_fld.TabIndex = 2
        Me.BTN_ses_tipo_fld.UseVisualStyleBackColor = True
        '
        'TXB_arc_sess_fld
        '
        Me.TXB_arc_sess_fld.Location = New System.Drawing.Point(130, 40)
        Me.TXB_arc_sess_fld.Name = "TXB_arc_sess_fld"
        Me.TXB_arc_sess_fld.Size = New System.Drawing.Size(408, 20)
        Me.TXB_arc_sess_fld.TabIndex = 3
        '
        'BTN_arc_sess_fld
        '
        Me.BTN_arc_sess_fld.Image = Global.Invoicer.My.Resources.Resources.PNG_folder
        Me.BTN_arc_sess_fld.Location = New System.Drawing.Point(544, 38)
        Me.BTN_arc_sess_fld.Name = "BTN_arc_sess_fld"
        Me.BTN_arc_sess_fld.Size = New System.Drawing.Size(27, 23)
        Me.BTN_arc_sess_fld.TabIndex = 4
        Me.BTN_arc_sess_fld.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(122, 13)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Cartella archivio sessioni"
        '
        'TXB_work_fld
        '
        Me.TXB_work_fld.Location = New System.Drawing.Point(130, 66)
        Me.TXB_work_fld.Name = "TXB_work_fld"
        Me.TXB_work_fld.Size = New System.Drawing.Size(408, 20)
        Me.TXB_work_fld.TabIndex = 5
        '
        'CMB_stampante_default
        '
        Me.CMB_stampante_default.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_stampante_default.FormattingEnabled = True
        Me.CMB_stampante_default.Location = New System.Drawing.Point(202, 139)
        Me.CMB_stampante_default.Name = "CMB_stampante_default"
        Me.CMB_stampante_default.Size = New System.Drawing.Size(369, 21)
        Me.CMB_stampante_default.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 142)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 13)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Stampante di default"
        '
        'BTN_work_fld
        '
        Me.BTN_work_fld.Image = Global.Invoicer.My.Resources.Resources.PNG_folder
        Me.BTN_work_fld.Location = New System.Drawing.Point(544, 64)
        Me.BTN_work_fld.Name = "BTN_work_fld"
        Me.BTN_work_fld.Size = New System.Drawing.Size(27, 23)
        Me.BTN_work_fld.TabIndex = 6
        Me.BTN_work_fld.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 13)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Cartella per work file"
        '
        'TAP_arch_ott
        '
        Me.TAP_arch_ott.Controls.Add(Me.TXB_suffisso_parte)
        Me.TAP_arch_ott.Controls.Add(Me.TXB_pagine_x_parte)
        Me.TAP_arch_ott.Controls.Add(Me.Label9)
        Me.TAP_arch_ott.Controls.Add(Me.Label8)
        Me.TAP_arch_ott.Controls.Add(Me.Label6)
        Me.TAP_arch_ott.Controls.Add(Me.CMB_stampante_arch_ottica)
        Me.TAP_arch_ott.Location = New System.Drawing.Point(4, 22)
        Me.TAP_arch_ott.Name = "TAP_arch_ott"
        Me.TAP_arch_ott.Padding = New System.Windows.Forms.Padding(3)
        Me.TAP_arch_ott.Size = New System.Drawing.Size(578, 226)
        Me.TAP_arch_ott.TabIndex = 1
        Me.TAP_arch_ott.Text = "Archiviazione ottica"
        Me.TAP_arch_ott.UseVisualStyleBackColor = True
        '
        'TXB_suffisso_parte
        '
        Me.TXB_suffisso_parte.Location = New System.Drawing.Point(195, 61)
        Me.TXB_suffisso_parte.Name = "TXB_suffisso_parte"
        Me.TXB_suffisso_parte.Size = New System.Drawing.Size(98, 20)
        Me.TXB_suffisso_parte.TabIndex = 22
        '
        'TXB_pagine_x_parte
        '
        Me.TXB_pagine_x_parte.Location = New System.Drawing.Point(195, 37)
        Me.TXB_pagine_x_parte.Name = "TXB_pagine_x_parte"
        Me.TXB_pagine_x_parte.Size = New System.Drawing.Size(98, 20)
        Me.TXB_pagine_x_parte.TabIndex = 21
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 64)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(163, 13)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Suffisso per stampa divisa in parti"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 40)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(167, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Numero massimo pagine per parte"
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.TXB_dbgiriconnectionstring)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(578, 226)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "Giri di consegna"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(133, 13)
        Me.Label10.TabIndex = 16
        Me.Label10.Text = "Percorso DB giri consegna"
        '
        'TXB_dbgiriconnectionstring
        '
        Me.TXB_dbgiriconnectionstring.Location = New System.Drawing.Point(145, 6)
        Me.TXB_dbgiriconnectionstring.Name = "TXB_dbgiriconnectionstring"
        Me.TXB_dbgiriconnectionstring.Size = New System.Drawing.Size(427, 20)
        Me.TXB_dbgiriconnectionstring.TabIndex = 15
        '
        'FRM_opzioni
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(610, 305)
        Me.Controls.Add(Me.TAC_opzioni)
        Me.Controls.Add(Me.BTN_ignore)
        Me.Controls.Add(Me.BTN_ok)
        Me.Name = "FRM_opzioni"
        Me.Text = "Opzioni"
        Me.TAC_opzioni.ResumeLayout(False)
        Me.TAP_generale.ResumeLayout(False)
        Me.TAP_generale.PerformLayout()
        Me.TAP_arch_ott.ResumeLayout(False)
        Me.TAP_arch_ott.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FBD_scelta As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents BTN_ok As System.Windows.Forms.Button
    Friend WithEvents BTN_ignore As System.Windows.Forms.Button
    Friend WithEvents CHB_clean_work As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TXB_mod_cont As System.Windows.Forms.TextBox
    Friend WithEvents CHB_dati_var As System.Windows.Forms.CheckBox
    Friend WithEvents CMB_stampante_arch_ottica As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CHB_reset_wrap_round As System.Windows.Forms.CheckBox
    Friend WithEvents TAC_opzioni As System.Windows.Forms.TabControl
    Friend WithEvents TAP_generale As System.Windows.Forms.TabPage
    Friend WithEvents TAP_arch_ott As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXB_ses_tipo_fld As System.Windows.Forms.TextBox
    Friend WithEvents BTN_ses_tipo_fld As System.Windows.Forms.Button
    Friend WithEvents TXB_arc_sess_fld As System.Windows.Forms.TextBox
    Friend WithEvents BTN_arc_sess_fld As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TXB_work_fld As System.Windows.Forms.TextBox
    Friend WithEvents CMB_stampante_default As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents BTN_work_fld As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TXB_max_num_fogli As System.Windows.Forms.TextBox
    Friend WithEvents TXB_suffisso_parte As System.Windows.Forms.TextBox
    Friend WithEvents TXB_pagine_x_parte As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TXB_dbgiriconnectionstring As System.Windows.Forms.TextBox
End Class
