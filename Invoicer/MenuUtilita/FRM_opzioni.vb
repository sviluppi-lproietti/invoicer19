Public Class FRM_opzioni

    'Private _cfg As CLS_configurazione

    'WriteOnly Property CFG() As CLS_configurazione

    '    Set(ByVal value As CLS_configurazione)
    '        _cfg = value

    '    End Set

    'End Property

    Private Sub FRM_opzioni_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        TXB_ses_tipo_fld.Text = ObjGlobali.Folders("SessioniDefaultFLD")
        TXB_arc_sess_fld.Text = ObjGlobali.Folders("SessioniCreateFLD")
        TXB_work_fld.Text = ObjGlobali.Folders("WorkFLD")
        CHB_clean_work.Checked = ObjGlobali.CFG.CleanWork
        CHB_dati_var.Checked = ObjGlobali.CFG.CheckDatiVar

        '
        ' Aggiunge la lista stampanti Cartacee
        '
        CMB_stampante_default.DataSource = Trasformazioni.TrasformaInLista(ObjGlobali.CFG.Stampanti)
        CMB_stampante_default.ValueMember = "STA_codice"
        CMB_stampante_default.DisplayMember = "STA_nome"
        CMB_stampante_default.SelectedIndex = Trasformazioni.TrasformaInLista(ObjGlobali.CFG.Stampanti).IndexOf(ObjGlobali.CFG.StampanteCartaceaDefault)

        TXB_mod_cont.Text = ObjGlobali.CFG.DocumentiContemporanei.ToString()
        CHB_reset_wrap_round.Checked = ObjGlobali.CFG.ResetWrapRound

        '
        ' Aggiunge la lista stampanti Ottiche
        '
        CMB_stampante_arch_ottica.DataSource = Trasformazioni.TrasformaInLista(ObjGlobali.CFG.Stampanti)
        CMB_stampante_arch_ottica.ValueMember = "STA_codice"
        CMB_stampante_arch_ottica.DisplayMember = "STA_nome"
        CMB_stampante_arch_ottica.SelectedIndex = Trasformazioni.TrasformaInLista(ObjGlobali.CFG.Stampanti).IndexOf(ObjGlobali.CFG.StampanteOtticaDefault)

        TXB_max_num_fogli.Text = ObjGlobali.CFG.MaxNumeroFogli
        TXB_pagine_x_parte.Text = ObjGlobali.CFG.PaginePerParte
        TXB_suffisso_parte.Text = ObjGlobali.CFG.SuffissoParte
        TXB_dbgiriconnectionstring.Text = ObjGlobali.CFG.DatabaseGiriConsegna

        Label5.Visible = ObjGlobali.CFG.LicenseSwitch
        TXB_mod_cont.Visible = ObjGlobali.CFG.LicenseSwitch
        Label7.Visible = ObjGlobali.CFG.LicenseSwitch
        TXB_max_num_fogli.Visible = ObjGlobali.CFG.LicenseSwitch
        CHB_reset_wrap_round.Visible = ObjGlobali.CFG.LicenseSwitch

    End Sub

    Private Sub BTN_ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ok.Click

        With ObjGlobali.CFG
            .CheckDatiVar = CHB_dati_var.Checked
            .CleanWork = CHB_clean_work.Checked

            ObjGlobali.Folders("SessioniDefaultFLD") = TXB_ses_tipo_fld.Text
            .RegistraParametroSuFileCFG("moduli_path", ObjGlobali.Folders("SessioniDefaultFLD"))

            ObjGlobali.Folders("SessioniCreateFLD") = TXB_arc_sess_fld.Text
            .RegistraParametroSuFileCFG("dati_path", ObjGlobali.Folders("SessioniCreateFLD"))

            ObjGlobali.Folders("WorkFLD") = TXB_work_fld.Text
            .RegistraParametroSuFileCFG("work_path", ObjGlobali.Folders("WorkFLD"))

            .DocumentiContemporanei = TXB_mod_cont.Text

            '.SessioniDefaultFLD = TXB_ses_tipo_fld.Text
            .StampanteCartaceaDefault = CMB_stampante_default.SelectedItem
            .StampanteOtticaDefault = CMB_stampante_arch_ottica.SelectedItem

            .ResetWrapRound = CHB_reset_wrap_round.Checked
            .PaginePerParte = TXB_pagine_x_parte.Text
            .SuffissoParte = TXB_suffisso_parte.Text
            .DatabaseGiriConsegna = TXB_dbgiriconnectionstring.Text

            .SalvaFileConfigurazione()
        End With
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub BTN_ignore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ignore.Click

        Me.DialogResult = Windows.Forms.DialogResult.Ignore
        Me.Close()

    End Sub

    Private Sub BTN_ses_tipo_fld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ses_tipo_fld.Click

        FBD_scelta.Description = "Indicare la cartella dove sono memorizzate le sessioni di default:"
        FBD_scelta.SelectedPath = TXB_ses_tipo_fld.Text
        If FBD_scelta.ShowDialog = Windows.Forms.DialogResult.OK Then
            TXB_ses_tipo_fld.Text = FBD_scelta.SelectedPath
        End If

    End Sub

    Private Sub BTN_arc_sess_fld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_arc_sess_fld.Click

        FBD_scelta.Description = "Indicare la cartella dove saranno memorizzate le sessioni di stampa:"
        FBD_scelta.SelectedPath = TXB_arc_sess_fld.Text
        If FBD_scelta.ShowDialog = Windows.Forms.DialogResult.OK Then
            TXB_arc_sess_fld.Text = FBD_scelta.SelectedPath
        End If

    End Sub

    Private Sub BTN_work_fld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_work_fld.Click

        FBD_scelta.Description = "Indicare la cartella di lavoro:"
        FBD_scelta.SelectedPath = TXB_work_fld.Text
        If FBD_scelta.ShowDialog = Windows.Forms.DialogResult.OK Then
            TXB_work_fld.Text = FBD_scelta.SelectedPath
        End If

    End Sub

End Class