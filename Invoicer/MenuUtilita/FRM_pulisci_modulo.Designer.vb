<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_pulisci_modulo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TXB_file_modulo = New System.Windows.Forms.TextBox
        Me.BTN_clean_form = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(566, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Questa funzione permette di pulire un modulo da quei TAG che hanno come attributo" & _
            " TOPRINT=0"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Modulo da pulire"
        '
        'TXB_file_modulo
        '
        Me.TXB_file_modulo.Location = New System.Drawing.Point(119, 40)
        Me.TXB_file_modulo.Name = "TXB_file_modulo"
        Me.TXB_file_modulo.Size = New System.Drawing.Size(459, 20)
        Me.TXB_file_modulo.TabIndex = 2
        Me.TXB_file_modulo.Text = "C:\Invoicer\Invoicer\bin\Debug\Moduli\UmbriaEnergy_GAS\moduli\Modulo_Bollettino.x" & _
            "ml"
        '
        'BTN_clean_form
        '
        Me.BTN_clean_form.Location = New System.Drawing.Point(140, 95)
        Me.BTN_clean_form.Name = "BTN_clean_form"
        Me.BTN_clean_form.Size = New System.Drawing.Size(75, 23)
        Me.BTN_clean_form.TabIndex = 3
        Me.BTN_clean_form.Text = "Pulisci"
        Me.BTN_clean_form.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(272, 95)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'FRM_pulisci_modulo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 159)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.BTN_clean_form)
        Me.Controls.Add(Me.TXB_file_modulo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FRM_pulisci_modulo"
        Me.Text = "Pulisce modulo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TXB_file_modulo As System.Windows.Forms.TextBox
    Friend WithEvents BTN_clean_form As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
