<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_mng_citta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGV_citta = New System.Windows.Forms.DataGridView
        Me.COL_citta = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DGV_citta_gest = New System.Windows.Forms.DataGridView
        Me.COL_toponimo_gen = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TXB_citta = New System.Windows.Forms.TextBox
        Me.DGV_citta_na = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LBL_toponimo = New System.Windows.Forms.Label
        Me.GRP_citta = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GRP_citta_na = New System.Windows.Forms.GroupBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.TSB_aggiungi = New System.Windows.Forms.ToolStripButton
        Me.TSB_cancella = New System.Windows.Forms.ToolStripButton
        Me.TSB_link_cit = New System.Windows.Forms.ToolStripButton
        Me.TSB_unlink_cit = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.TSB_salva = New System.Windows.Forms.ToolStripButton
        Me.TSB_ignora = New System.Windows.Forms.ToolStripButton
        CType(Me.DGV_citta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_citta_gest, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_citta_na, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRP_citta.SuspendLayout()
        Me.GRP_citta_na.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DGV_citta
        '
        Me.DGV_citta.AllowUserToAddRows = False
        Me.DGV_citta.AllowUserToDeleteRows = False
        Me.DGV_citta.AllowUserToOrderColumns = True
        Me.DGV_citta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_citta.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_citta})
        Me.DGV_citta.Location = New System.Drawing.Point(12, 50)
        Me.DGV_citta.Name = "DGV_citta"
        Me.DGV_citta.Size = New System.Drawing.Size(410, 440)
        Me.DGV_citta.TabIndex = 0
        '
        'COL_citta
        '
        Me.COL_citta.DataPropertyName = "CIT_nome"
        Me.COL_citta.HeaderText = "Citt�"
        Me.COL_citta.Name = "COL_citta"
        Me.COL_citta.ReadOnly = True
        Me.COL_citta.Width = 350
        '
        'DGV_citta_gest
        '
        Me.DGV_citta_gest.AllowUserToAddRows = False
        Me.DGV_citta_gest.AllowUserToDeleteRows = False
        Me.DGV_citta_gest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_citta_gest.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_toponimo_gen})
        Me.DGV_citta_gest.Location = New System.Drawing.Point(6, 55)
        Me.DGV_citta_gest.Name = "DGV_citta_gest"
        Me.DGV_citta_gest.Size = New System.Drawing.Size(470, 123)
        Me.DGV_citta_gest.TabIndex = 1
        '
        'COL_toponimo_gen
        '
        Me.COL_toponimo_gen.DataPropertyName = "CIG_nome"
        Me.COL_toponimo_gen.HeaderText = "Varianti"
        Me.COL_toponimo_gen.Name = "COL_toponimo_gen"
        Me.COL_toponimo_gen.ReadOnly = True
        Me.COL_toponimo_gen.Width = 410
        '
        'TXB_citta
        '
        Me.TXB_citta.Location = New System.Drawing.Point(51, 16)
        Me.TXB_citta.Multiline = True
        Me.TXB_citta.Name = "TXB_citta"
        Me.TXB_citta.Size = New System.Drawing.Size(425, 20)
        Me.TXB_citta.TabIndex = 2
        '
        'DGV_citta_na
        '
        Me.DGV_citta_na.AllowUserToAddRows = False
        Me.DGV_citta_na.AllowUserToDeleteRows = False
        Me.DGV_citta_na.AllowUserToResizeColumns = False
        Me.DGV_citta_na.AllowUserToResizeRows = False
        Me.DGV_citta_na.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_citta_na.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1})
        Me.DGV_citta_na.Location = New System.Drawing.Point(6, 19)
        Me.DGV_citta_na.Name = "DGV_citta_na"
        Me.DGV_citta_na.Size = New System.Drawing.Size(470, 242)
        Me.DGV_citta_na.TabIndex = 3
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "CIG_nome"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Citt� generiche"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 410
        '
        'LBL_toponimo
        '
        Me.LBL_toponimo.AutoSize = True
        Me.LBL_toponimo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_toponimo.Location = New System.Drawing.Point(12, 34)
        Me.LBL_toponimo.Name = "LBL_toponimo"
        Me.LBL_toponimo.Size = New System.Drawing.Size(135, 13)
        Me.LBL_toponimo.TabIndex = 6
        Me.LBL_toponimo.Text = "Elenco toponimi gestiti"
        '
        'GRP_citta
        '
        Me.GRP_citta.Controls.Add(Me.Label2)
        Me.GRP_citta.Controls.Add(Me.Label1)
        Me.GRP_citta.Controls.Add(Me.DGV_citta_gest)
        Me.GRP_citta.Controls.Add(Me.TXB_citta)
        Me.GRP_citta.Location = New System.Drawing.Point(438, 34)
        Me.GRP_citta.Name = "GRP_citta"
        Me.GRP_citta.Size = New System.Drawing.Size(482, 185)
        Me.GRP_citta.TabIndex = 7
        Me.GRP_citta.TabStop = False
        Me.GRP_citta.Text = "Dati citt�"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Varianti della citt�"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Nome"
        '
        'GRP_citta_na
        '
        Me.GRP_citta_na.Controls.Add(Me.DGV_citta_na)
        Me.GRP_citta_na.Location = New System.Drawing.Point(438, 225)
        Me.GRP_citta_na.Name = "GRP_citta_na"
        Me.GRP_citta_na.Size = New System.Drawing.Size(482, 265)
        Me.GRP_citta_na.TabIndex = 8
        Me.GRP_citta_na.TabStop = False
        Me.GRP_citta_na.Text = "Citt� non assegnate"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSB_aggiungi, Me.TSB_cancella, Me.TSB_link_cit, Me.TSB_unlink_cit, Me.ToolStripSeparator1, Me.TSB_salva, Me.TSB_ignora})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(932, 25)
        Me.ToolStrip1.TabIndex = 9
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'TSB_aggiungi
        '
        Me.TSB_aggiungi.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_aggiungi.Image = Global.Invoicer.My.Resources.Resources.PNG_add
        Me.TSB_aggiungi.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_aggiungi.Name = "TSB_aggiungi"
        Me.TSB_aggiungi.Size = New System.Drawing.Size(23, 22)
        Me.TSB_aggiungi.ToolTipText = "Aggiungi citt�"
        '
        'TSB_cancella
        '
        Me.TSB_cancella.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_cancella.Image = Global.Invoicer.My.Resources.Resources.PNG_delete
        Me.TSB_cancella.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_cancella.Name = "TSB_cancella"
        Me.TSB_cancella.Size = New System.Drawing.Size(23, 22)
        Me.TSB_cancella.ToolTipText = "Cancella citt�"
        '
        'TSB_link_cit
        '
        Me.TSB_link_cit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_link_cit.Image = Global.Invoicer.My.Resources.Resources.PNG_chain
        Me.TSB_link_cit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_link_cit.Name = "TSB_link_cit"
        Me.TSB_link_cit.Size = New System.Drawing.Size(23, 22)
        Me.TSB_link_cit.ToolTipText = "Associa citt� generica"
        '
        'TSB_unlink_cit
        '
        Me.TSB_unlink_cit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_unlink_cit.Image = Global.Invoicer.My.Resources.Resources.PNG_chain_brk
        Me.TSB_unlink_cit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_unlink_cit.Name = "TSB_unlink_cit"
        Me.TSB_unlink_cit.Size = New System.Drawing.Size(23, 22)
        Me.TSB_unlink_cit.ToolTipText = "Rimuovi associazione citt� generica"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'TSB_salva
        '
        Me.TSB_salva.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_salva.Image = Global.Invoicer.My.Resources.Resources.PNG_salva
        Me.TSB_salva.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_salva.Name = "TSB_salva"
        Me.TSB_salva.Size = New System.Drawing.Size(23, 22)
        Me.TSB_salva.ToolTipText = "Salva modifiche"
        '
        'TSB_ignora
        '
        Me.TSB_ignora.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_ignora.Image = Global.Invoicer.My.Resources.Resources.PNG_ignore
        Me.TSB_ignora.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_ignora.Name = "TSB_ignora"
        Me.TSB_ignora.Size = New System.Drawing.Size(23, 22)
        Me.TSB_ignora.ToolTipText = "Annulla modifiche"
        '
        'FRM_mng_citta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(932, 497)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GRP_citta_na)
        Me.Controls.Add(Me.GRP_citta)
        Me.Controls.Add(Me.LBL_toponimo)
        Me.Controls.Add(Me.DGV_citta)
        Me.Name = "FRM_mng_citta"
        Me.Text = "Gestione citt�"
        CType(Me.DGV_citta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_citta_gest, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_citta_na, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRP_citta.ResumeLayout(False)
        Me.GRP_citta.PerformLayout()
        Me.GRP_citta_na.ResumeLayout(False)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV_citta As System.Windows.Forms.DataGridView
    Friend WithEvents DGV_citta_gest As System.Windows.Forms.DataGridView
    Friend WithEvents TXB_citta As System.Windows.Forms.TextBox
    Friend WithEvents DGV_citta_na As System.Windows.Forms.DataGridView
    Friend WithEvents LBL_toponimo As System.Windows.Forms.Label
    Friend WithEvents GRP_citta As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GRP_citta_na As System.Windows.Forms.GroupBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents TSB_aggiungi As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSB_salva As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_ignora As System.Windows.Forms.ToolStripButton
    Friend WithEvents COL_citta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COL_toponimo_gen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TSB_cancella As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_link_cit As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_unlink_cit As System.Windows.Forms.ToolStripButton
End Class
