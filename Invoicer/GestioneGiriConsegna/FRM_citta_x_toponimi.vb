Imports ISC.LibrerieComuni.OggettiComuni

Public Class FRM_citta_x_toponimi

    Private _data_citta_x_toponimi As DS_citta_x_toponimi

    Private Sub FRM_citta_x_toponimi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lda As Layer_DataAccess
        Dim bind As BindingSource

        Try
            _data_citta_x_toponimi = New DS_citta_x_toponimi
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_citta_x_toponimi)

            lda.CaricaDataSet("TBL_citta")
            lda.CaricaDataSet("QRY_citta_x_toponimi")
            lda.CaricaDataSet("TBL_toponimi")

            bind = New BindingSource
            bind.DataSource = _data_citta_x_toponimi
            bind.DataMember = "TBL_citta"
            bind.Sort = "CIT_nome"
            DGV_citta.AutoGenerateColumns = False
            DGV_citta.DataSource = bind

            CMB_citta.DataSource = _data_citta_x_toponimi.TBL_citta
            CMB_citta.ValueMember = "CIT_codice"
            CMB_citta.DisplayMember = "CIT_nome"
            CMB_toponimo.DataSource = _data_citta_x_toponimi.TBL_toponimi
            CMB_toponimo.ValueMember = "TOP_codice"
            CMB_toponimo.DisplayMember = "TOP_nome"
            ShowGroup()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub DGR_toponimi_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_citta.CellEnter
        Dim nCitta As Integer
        Dim bm As BindingManagerBase
        Dim dr_cit As DS_citta_x_toponimi.TBL_cittaRow
        Dim dv As DataView

        bm = DGV_citta.BindingContext(DGV_citta.DataSource, DGV_citta.DataMember)
        dr_cit = CType(CType(bm.Current, DataRowView).Row, DS_citta_x_toponimi.TBL_cittaRow)
        nCitta = dr_cit.CIT_codice

        dv = New DataView(_data_citta_x_toponimi.QRY_citta_x_toponimi)
        dv.RowFilter = String.Concat("CXT_cod_cit = ", nCitta)
        dv.Sort = "TOP_nome"

        DGV_toponimi.AutoGenerateColumns = False
        DGV_toponimi.DataSource = dv

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_link_cxt.Click

        ShowGroup()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim dr_cxt As DS_citta_x_toponimi.TBL_citta_x_toponimiRow
        Dim lda As Layer_DataAccess
        Dim nToponimo As Integer
        Dim nCitta As Integer

        If MessageBox.Show("Salvo l'associazione Citt�-Toponimo indicata?", "Conferma salvataggio", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            nCitta = CMB_citta.SelectedValue
            nToponimo = CMB_toponimo.SelectedValue
            If ExistCittaToponimo(nCitta, nToponimo) Then
                MessageBox.Show("Associazione gi� esistente.", "Salvataggio annullato", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                dr_cxt = _data_citta_x_toponimi.TBL_citta_x_toponimi.NewRow
                dr_cxt.CXT_cod_cit = nCitta
                dr_cxt.CXT_cod_top = nToponimo
                _data_citta_x_toponimi.TBL_citta_x_toponimi.Rows.Add(dr_cxt)

                lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_citta_x_toponimi)
                lda.AggiornaDataSet("TBL_citta_x_toponimi")
                lda.PulisciTabella("QRY_citta_x_toponimi")
                lda.CaricaDataSet("QRY_citta_x_toponimi")
                MessageBox.Show("Associazione salvata.", "Salvataggio terminato", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If

    End Sub

    Private Function ExistCittaToponimo(ByVal nCitta As Integer, ByVal nToponimo As Integer) As Boolean
        Dim oComBuil As OleDb.OleDbCommandBuilder
        Dim oDa As OleDb.OleDbDataAdapter
        Dim oCmd1 As OleDb.OleDbCommand
        Dim nRecord As Integer

        oCmd1 = New OleDb.OleDbCommand(String.Concat("SELECT COUNT(*) as NumRecord FROM TBL_citta_x_toponimi WHERE (CXT_cod_cit = ", nCitta, ") and (CXT_cod_top = ", nToponimo, ")"), New OleDb.OleDbConnection(ObjGlobali.CFG.DBGiriConsegna_CS))
        oDa = New OleDb.OleDbDataAdapter(oCmd1)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oCmd1.Connection.Open()
        nRecord = oCmd1.ExecuteScalar()
        oCmd1.Connection.Close()
        Return nRecord > 0

    End Function

    Private Sub ShowGroup()

        GRP_add_ass.Visible = Not GRP_add_ass.Visible
        If GRP_add_ass.Visible Then
            Me.Height = 498
        Else
            Me.Height = 415
        End If

    End Sub

    Private Sub TSB_unlink_cxt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_unlink_cxt.Click
        Dim dr_cxt_q As DS_citta_x_toponimi.QRY_citta_x_toponimiRow
        Dim dr_cxt_t As DS_citta_x_toponimi.TBL_citta_x_toponimiRow
        Dim bm_cxt As BindingManagerBase
        Dim lda As Layer_DataAccess

        If MessageBox.Show("Cancello l'associazione Citt�-Toponimo indicata?", "Conferma rimozione associazione", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            bm_cxt = DGV_toponimi.BindingContext(DGV_toponimi.DataSource, DGV_toponimi.DataMember)
            dr_cxt_q = CType(CType(bm_cxt.Current, DataRowView).Row, DS_citta_x_toponimi.QRY_citta_x_toponimiRow)

            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_citta_x_toponimi)
            lda.PulisciTabella("TBL_citta_x_toponimi")
            lda.CaricaDataSet("TBL_citta_x_toponimi", New String(,) {{"FEQ", "", "CXT_codice", dr_cxt_q.CXT_codice}})
            dr_cxt_t = _data_citta_x_toponimi.TBL_citta_x_toponimi.Rows(0)
            dr_cxt_t.Delete()
            lda.AggiornaDataSet("TBL_citta_x_toponimi")
            lda.PulisciTabella("QRY_citta_x_toponimi")
            lda.CaricaDataSet("QRY_citta_x_toponimi")
            MessageBox.Show("Associazione Rimossa.", "Salvataggio terminato", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

End Class