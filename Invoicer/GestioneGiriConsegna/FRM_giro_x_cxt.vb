Imports System.Data.OleDb
Imports System.IO
Imports System.IO.Compression
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuniForm.FC_forms

Public Class FRM_giro_x_cxt

    Private _data_giri As DS_giri

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lda As Layer_DataAccess
        Dim bind As BindingSource

        Try
            _data_giri = New DS_giri
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri)
            lda.CaricaDataSet("TBL_giri", New String(,) {{"FEQ", "", "GIR_tipo", "G"}})
            lda.CaricaDataSet("QRY_giri_x_cxt")

            bind = New BindingSource
            bind.DataSource = _data_giri
            bind.DataMember = "TBL_giri"
            bind.Sort = "GIR_nome"

            DGV_giri.AutoGenerateColumns = False
            DGV_cxt.AutoGenerateColumns = False
            DGV_giri.DataSource = bind

            TXB_nome.DataBindings.Clear()
            TXB_nome.DataBindings.Add(New Binding("Text", bind, "GIR_nome", True))
            TXB_sigla.DataBindings.Clear()
            TXB_sigla.DataBindings.Add(New Binding("Text", bind, "GIR_sigla", True))
            TXB_descrizione.DataBindings.Clear()
            TXB_descrizione.DataBindings.Add(New Binding("Text", bind, "GIR_descrizione", True))
            DGV_giri_CellMouseClick(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub DGV_giri_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_giri.CellContentClick


    End Sub

    Private Sub DGV_giri_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_giri.CellEnter
        Dim dr_gir As DS_giri.TBL_giriRow
        Dim bm As BindingManagerBase
        Dim dv As DataView

        bm = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
        dr_gir = CType(CType(bm.Current, DataRowView).Row, DS_giri.TBL_giriRow)
        dv = New DataView(_data_giri.QRY_giri_x_cxt)
        dv.RowFilter = String.Concat("GXC_cod_gir = ", dr_gir.GIR_codice)
        dv.Sort = "TOP_nome"
        DGV_cxt.AutoGenerateColumns = False
        DGV_cxt.DataSource = dv

    End Sub

    Private Sub DGV_giri_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles DGV_giri.CellMouseClick
        'Dim oComBuil As OleDb.OleDbCommandBuilder
        'Dim oDa As OleDb.OleDbDataAdapter
        'Dim oCmd As OleDb.OleDbCommand
        'Dim nGiro As Integer

        'Dim bm As BindingManagerBase
        'Dim dr_gir As DS_giri.TBL_giriRow

        'bm = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
        'dr_gir = CType(CType(bm.Current, DataRowView).Row, DS_giri.TBL_giriRow)
        'nGiro = dr_gir.GIR_codice
        '_data_giri.QRY_cit_x_top.Rows.Clear()
        '' carica i dati delle vie
        'oCmd = New OleDb.OleDbCommand
        'oCmd.Connection = New OleDb.OleDbConnection(_DBGestioneGiriConnStr)
        '' Salvataggio della tabella TBL_main
        'oCmd.CommandText = String.Concat("SELECT * FROM QRY_cit_x_top WHERE GXC_cod_gir = ", nGiro)
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_giri, "QRY_cit_x_top")
        'oCmd.Connection.Close()
        'DataGridView2.DataSource = _data_giri.QRY_cit_x_top

    End Sub

    Private Sub TSB_aggiungi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_aggiungi.Click
        Dim bm_gir As BindingManagerBase

        If MessageBox.Show("Creo un nuovo giro di consegna?", "Creazione giro di consegna", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bm_gir = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
            bm_gir.AddNew()
            bm_gir.EndCurrentEdit()
        End If

    End Sub

    Private Sub TSB_link_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_link_gxc.Click
        Dim frm_slc_cxt As FRM_select_cxt
        Dim bm_gir As BindingManagerBase
        Dim dr_gir As DS_giri.TBL_giriRow
        Dim dr_cxt As DS_giri.TBL_giri_x_cxtRow
        Dim lda As Layer_DataAccess

        frm_slc_cxt = BuildForm(New FRM_select_cxt)
        frm_slc_cxt.DBGestioneGiriConnStr = ObjGlobali.CFG.DBGiriConsegna_CS
        If frm_slc_cxt.ShowDialog = Windows.Forms.DialogResult.OK Then
            If MessageBox.Show("Procedo con l'associazione tra il giro e la coppia citt�-toponimo?", "Conferma associazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri)
                bm_gir = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)

                dr_gir = CType(CType(bm_gir.Current, DataRowView).Row, DS_giri.TBL_giriRow)
                For Each dgvr As DataGridViewRow In frm_slc_cxt.DGV_toponimi.SelectedRows
                    dr_cxt = _data_giri.TBL_giri_x_cxt.NewRow
                    dr_cxt.GXC_cod_gir = dr_gir.GIR_codice
                    dr_cxt.GXC_cod_cxt = dgvr.Cells(1).Value
                    _data_giri.TBL_giri_x_cxt.Rows.Add(dr_cxt)
                Next
                lda.AggiornaDataSet("TBL_giri_x_cxt")
                lda.PulisciTabella("QRY_giri_x_cxt")
                lda.CaricaDataSet("QRY_giri_x_cxt")
            End If
        End If
        DestroyForm(frm_slc_cxt)

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_cancella.Click

    End Sub

    Private Sub TSB_salva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_salva.Click
        Dim dr_gir As DS_giri.TBL_giriRow
        Dim bm_gir As BindingManagerBase
        Dim lda As Layer_DataAccess

        bm_gir = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
        bm_gir.EndCurrentEdit()
        dr_gir = CType(CType(bm_gir.Current, DataRowView).Row, DS_giri.TBL_giriRow)
        dr_gir.GIR_tipo = "G"
        lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri)
        lda.AggiornaDataSet("TBL_giri")

    End Sub

    Private Sub TSB_ignora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_ignora.Click
        Dim bm_gir As BindingManagerBase

        bm_gir = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
        bm_gir.CancelCurrentEdit()

    End Sub

    Private Sub TSB_unlink_gxc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_unlink_gxc.Click
        Dim dr_gxc_q As DS_giri.QRY_giri_x_cxtRow
        Dim dr_gxc_t As DS_giri.TBL_giri_x_cxtRow
        Dim lda As Layer_DataAccess

        If MessageBox.Show("Procedo con la rimozione dell'associazione tra il giro di consegna e la coppia citt�-toponimo?", "Conferma rimozione associazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri)
            For Each dgvr As DataGridViewRow In DGV_cxt.SelectedRows
                dr_gxc_q = DirectCast(DGV_cxt.Rows(dgvr.Index).DataBoundItem, DataRowView).Row
                lda.PulisciTabella("TBL_giri_x_cxt")
                lda.CaricaDataSet("TBL_giri_x_cxt", New String(,) {{"FEQ", "", "GXC_codice", dr_gxc_q.GXC_codice}})
                dr_gxc_t = _data_giri.TBL_giri_x_cxt.Rows(0)
                dr_gxc_t.Delete()
                lda.AggiornaDataSet("TBL_giri_x_cxt")
            Next
            lda.PulisciTabella("QRY_giri_x_cxt")
            lda.CaricaDataSet("QRY_giri_x_cxt")
        End If

    End Sub

End Class
