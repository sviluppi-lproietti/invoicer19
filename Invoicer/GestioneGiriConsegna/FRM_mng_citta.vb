Imports ISC.LibrerieComuni.OggettiComuni

Public Class FRM_mng_citta

    Private _data_citta As DS_citta
    Private _CittaEditata As Boolean
    Private lStartBlank As Boolean

    Private Sub FRM_mng_toponimi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lda As Layer_DataAccess
        Dim bind As BindingSource
        Dim dv As DataView

        Try
            _CittaEditata = False
            _data_citta = New DS_citta

            ' Caricvamento dei dati dei toponimi.
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_citta)
            lda.CaricaDataSet("TBL_citta")
            lda.CaricaDataSet("TBL_citta_gen")

            bind = New BindingSource
            bind.DataSource = _data_citta
            bind.DataMember = "TBL_citta"
            bind.Sort = "CIT_nome"

            dv = New DataView(_data_citta.TBL_citta_gen)
            dv.RowFilter = "CIG_cod_cit = -1"
            dv.Sort = "CIG_nome"

            DGV_citta.AutoGenerateColumns = False
            DGV_citta.DataSource = bind
            DGV_citta_gest.AutoGenerateColumns = False
            DGV_citta_na.AutoGenerateColumns = False
            DGV_citta_na.DataSource = dv

            TXB_citta.DataBindings.Clear()
            TXB_citta.DataBindings.Add(New Binding("Text", bind, "CIT_nome", True))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub DGR_toponimi_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_citta.CellEnter
        Dim dr_cit As DS_citta.TBL_cittaRow
        Dim bm As BindingManagerBase
        Dim dv As DataView

        bm = DGV_citta.BindingContext(DGV_citta.DataSource, DGV_citta.DataMember)
        dr_cit = CType(CType(bm.Current, DataRowView).Row, DS_citta.TBL_cittaRow)
        dv = New DataView(_data_citta.TBL_citta_gen)
        dv.RowFilter = String.Concat("CIG_cod_cit = ", dr_cit.CIT_codice)
        dv.Sort = "CIG_nome"
        DGV_citta_gest.AutoGenerateColumns = False
        DGV_citta_gest.DataSource = dv

    End Sub

    Private Sub TXB_citta_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXB_citta.TextChanged
        Dim bm As BindingManagerBase
        Dim dr_cit As DS_citta.TBL_cittaRow

        bm = DGV_citta.BindingContext(DGV_citta.DataSource, DGV_citta.DataMember)
        dr_cit = CType(CType(bm.Current, DataRowView).Row, DS_citta.TBL_cittaRow)
        '_ToponimoEditato = Not dr_cit.Item("CIT_nome", DataRowVersion.Original) = TXB_citta.Text

    End Sub

    Private Sub DGR_toponimi_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DGV_citta.CellValidating
        Dim bm As BindingManagerBase
        Dim dr_top As DS_citta.TBL_cittaRow

        If _CittaEditata Then
            e.Cancel = MessageBox.Show("Il toponimo � stato modificato. Spostandosi su un altro record si perderanno le modifiche effettuate. Procedo comunque?", "Toponimo modificato", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No
            If Not e.Cancel Then
                bm = DGV_citta.BindingContext(DGV_citta.DataSource, DGV_citta.DataMember)
                dr_top = CType(CType(bm.Current, DataRowView).Row, DS_citta.TBL_cittaRow)
                dr_top.RejectChanges()
            End If
        End If

    End Sub

    ' Aggiungi dati toponimo gestito
    Private Sub TSB_aggiungi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_aggiungi.Click
        Dim dr_cig As DS_citta.TBL_citta_genRow
        Dim dr_cit As DS_citta.TBL_cittaRow
        Dim bm_cit As BindingManagerBase
        Dim bm_cig As BindingManagerBase
        Dim oAnswer As DialogResult

        If DGV_citta_na.Rows.Count > 0 Then
            oAnswer = MessageBox.Show("Creo la citt� a partire da quella generica selezionata?", "Creazione citt�", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
        Else
            oAnswer = Windows.Forms.DialogResult.No
        End If
        If Not (oAnswer = Windows.Forms.DialogResult.Cancel) Then
            lStartBlank = (oAnswer = Windows.Forms.DialogResult.No)
            bm_cit = DGV_citta.BindingContext(DGV_citta.DataSource, DGV_citta.DataMember)
            bm_cit.AddNew()
            dr_cit = CType(CType(bm_cit.Current, DataRowView).Row, DS_citta.TBL_cittaRow)
            If lStartBlank Then
                dr_cit.CIT_nome = ""
            Else
                bm_cig = DGV_citta_na.BindingContext(DGV_citta_na.DataSource, DGV_citta_na.DataMember)
                dr_cig = CType(CType(bm_cig.Current, DataRowView).Row, DS_citta.TBL_citta_genRow)
                dr_cit.CIT_nome = dr_cig.CIG_nome
            End If
            bm_cit.EndCurrentEdit()
            TXB_citta.Focus()
            _CittaEditata = True
        End If

    End Sub

    Private Sub TSB_cancella_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_cancella.Click
        Dim dr_cig As DS_citta.TBL_citta_genRow
        Dim dr_cit As DS_citta.TBL_cittaRow
        Dim drv As DataRowView
        Dim bm_cit As BindingManagerBase
        'Dim bm_cig As BindingManagerBase
        Dim lda As Layer_DataAccess

        If MessageBox.Show("Procedo con la cancellazione della citt� selezionata?", "Cancellazione citt�", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bm_cit = DGV_citta.BindingContext(DGV_citta.DataSource, DGV_citta.DataMember)
            'bm_cig = DGV_citta_gest.BindingContext(DGV_citta_gest.DataSource, DGV_citta_gest.DataMember)

            For Each drv In CType(DGV_citta_gest.DataSource, DataView)
                dr_cig = drv.Row
                dr_cig.CIG_cod_cit = -1
            Next
            dr_cit = CType(CType(bm_cit.Current, DataRowView).Row, DS_citta.TBL_cittaRow)
            dr_cit.Delete()

            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_citta)
            lda.AggiornaDataSet("TBL_citta")
            lda.AggiornaDataSet("TBL_citta_gen")
        End If

    End Sub

    Private Sub TSB_link_cit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_link_cit.Click
        Dim dr_cig As DS_citta.TBL_citta_genRow
        Dim dr_cit As DS_citta.TBL_cittaRow
        Dim bm_cit As BindingManagerBase
        Dim bm_cig As BindingManagerBase
        Dim lda As Layer_DataAccess

        If MessageBox.Show("Procedo con l'associazione tra la citt� e la citt� generica?", "Conferma associazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bm_cit = DGV_citta.BindingContext(DGV_citta.DataSource, DGV_citta.DataMember)
            bm_cig = DGV_citta_na.BindingContext(DGV_citta_na.DataSource, DGV_citta_na.DataMember)
            dr_cit = CType(CType(bm_cit.Current, DataRowView).Row, DS_citta.TBL_cittaRow)
            dr_cig = CType(CType(bm_cig.Current, DataRowView).Row, DS_citta.TBL_citta_genRow)
            dr_cig.CIG_cod_cit = dr_cit.CIT_codice
            bm_cig.EndCurrentEdit()
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_citta)
            lda.AggiornaDataSet("TBL_citta_gen")
        End If

    End Sub

    Private Sub TSB_unlink_cit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_unlink_cit.Click
        Dim dr_cig As DS_citta.TBL_citta_genRow
        Dim bm_cig As BindingManagerBase
        Dim lda As Layer_DataAccess

        If MessageBox.Show("Procedo con la disassociazione tra il toponimo e il toponimo generico?", "Conferma associazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bm_cig = DGV_citta_gest.BindingContext(DGV_citta_gest.DataSource, DGV_citta_gest.DataMember)
            dr_cig = CType(CType(bm_cig.Current, DataRowView).Row, DS_citta.TBL_citta_genRow)
            dr_cig.CIG_cod_cit = -1
            bm_cig.EndCurrentEdit()
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_citta)
            lda.AggiornaDataSet("TBL_citta_gen")
        End If

    End Sub

    ' Salva dati 
    Private Sub TSB_salva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_salva.Click
        Dim dr_cig As DS_citta.TBL_citta_genRow
        Dim dr_cit As DS_citta.TBL_cittaRow
        Dim bm_cit As BindingManagerBase
        Dim bm_cig As BindingManagerBase
        Dim lda As Layer_DataAccess
        Dim lAdd As Boolean

        bm_cit = DGV_citta.BindingContext(DGV_citta.DataSource, DGV_citta.DataMember)
        bm_cit.EndCurrentEdit()
        dr_cit = CType(CType(bm_cit.Current, DataRowView).Row, DS_citta.TBL_cittaRow)
        lAdd = (dr_cit.RowState = DataRowState.Added)
        lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_citta)
        lda.AggiornaDataSet("TBL_citta")
        If lAdd And Not lStartBlank Then
            bm_cig = DGV_citta_na.BindingContext(DGV_citta_na.DataSource, DGV_citta_na.DataMember)
            dr_cig = CType(CType(bm_cig.Current, DataRowView).Row, DS_citta.TBL_citta_genRow)
            dr_cig.CIG_cod_cit = dr_cit.CIT_codice
            lda.AggiornaDataSet("TBL_citta_gen")
        End If
        _CittaEditata = False

    End Sub

    Private Sub TSB_ignora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_ignora.Click
        Dim dr_cit As DS_citta.TBL_cittaRow
        Dim bm_cit As BindingManagerBase

        bm_cit = DGV_citta.BindingContext(DGV_citta.DataSource, DGV_citta.DataMember)
        dr_cit = CType(CType(bm_cit.Current, DataRowView).Row, DS_citta.TBL_cittaRow)
        If dr_cit.RowState = DataRowState.Added Then
            dr_cit.Delete()
        Else
            bm_cit.CancelCurrentEdit()
        End If
        _CittaEditata = False

    End Sub

End Class