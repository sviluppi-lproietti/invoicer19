<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_select_cxt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMB_citta = New System.Windows.Forms.ComboBox
        Me.DGV_toponimi = New System.Windows.Forms.DataGridView
        Me.TOP_nome = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CXT_codice = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        CType(Me.DGV_toponimi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMB_citta
        '
        Me.CMB_citta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_citta.FormattingEnabled = True
        Me.CMB_citta.Location = New System.Drawing.Point(72, 8)
        Me.CMB_citta.Name = "CMB_citta"
        Me.CMB_citta.Size = New System.Drawing.Size(591, 21)
        Me.CMB_citta.TabIndex = 0
        '
        'DGV_toponimi
        '
        Me.DGV_toponimi.AllowUserToAddRows = False
        Me.DGV_toponimi.AllowUserToDeleteRows = False
        Me.DGV_toponimi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_toponimi.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TOP_nome, Me.CXT_codice})
        Me.DGV_toponimi.Location = New System.Drawing.Point(72, 35)
        Me.DGV_toponimi.Name = "DGV_toponimi"
        Me.DGV_toponimi.Size = New System.Drawing.Size(591, 326)
        Me.DGV_toponimi.TabIndex = 1
        '
        'TOP_nome
        '
        Me.TOP_nome.DataPropertyName = "TOP_nome"
        Me.TOP_nome.HeaderText = "Toponimo"
        Me.TOP_nome.Name = "TOP_nome"
        Me.TOP_nome.Width = 530
        '
        'CXT_codice
        '
        Me.CXT_codice.DataPropertyName = "CXT_codice"
        Me.CXT_codice.HeaderText = "CXT_codice"
        Me.CXT_codice.Name = "CXT_codice"
        Me.CXT_codice.ReadOnly = True
        Me.CXT_codice.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Citt�"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Toponimo"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(213, 367)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Associa"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(365, 367)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Annulla"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'FRM_select_cxt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(672, 397)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DGV_toponimi)
        Me.Controls.Add(Me.CMB_citta)
        Me.Name = "FRM_select_cxt"
        Me.Text = "Seleziona Citt�-Toponimo"
        CType(Me.DGV_toponimi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMB_citta As System.Windows.Forms.ComboBox
    Friend WithEvents DGV_toponimi As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TOP_nome As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CXT_codice As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
