<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_giro_x_gir
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TXB_nome = New System.Windows.Forms.TextBox
        Me.LBL_nome = New System.Windows.Forms.Label
        Me.DGV_giri = New System.Windows.Forms.DataGridView
        Me.DGC_nomegiro = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GRP_giro = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.TXB_sigla = New System.Windows.Forms.TextBox
        Me.DGV_giri_gest = New System.Windows.Forms.DataGridView
        Me.COL_nome_gest = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.COL_descrizione_gest = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TXB_descrizione = New System.Windows.Forms.TextBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.TSB_aggiungi = New System.Windows.Forms.ToolStripButton
        Me.TSB_cancella = New System.Windows.Forms.ToolStripButton
        Me.TSB_link_gxc = New System.Windows.Forms.ToolStripButton
        Me.TSB_unlink_gxc = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.TSB_salva = New System.Windows.Forms.ToolStripButton
        Me.TSB_ignora = New System.Windows.Forms.ToolStripButton
        Me.DGV_giri_na = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GRP_giri_na = New System.Windows.Forms.GroupBox
        CType(Me.DGV_giri, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRP_giro.SuspendLayout()
        CType(Me.DGV_giri_gest, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.DGV_giri_na, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRP_giri_na.SuspendLayout()
        Me.SuspendLayout()
        '
        'TXB_nome
        '
        Me.TXB_nome.Location = New System.Drawing.Point(74, 19)
        Me.TXB_nome.Name = "TXB_nome"
        Me.TXB_nome.Size = New System.Drawing.Size(412, 20)
        Me.TXB_nome.TabIndex = 3
        '
        'LBL_nome
        '
        Me.LBL_nome.AutoSize = True
        Me.LBL_nome.Location = New System.Drawing.Point(6, 22)
        Me.LBL_nome.Name = "LBL_nome"
        Me.LBL_nome.Size = New System.Drawing.Size(55, 13)
        Me.LBL_nome.TabIndex = 1
        Me.LBL_nome.Text = "Nome giro"
        '
        'DGV_giri
        '
        Me.DGV_giri.AllowUserToAddRows = False
        Me.DGV_giri.AllowUserToDeleteRows = False
        Me.DGV_giri.AllowUserToResizeColumns = False
        Me.DGV_giri.AllowUserToResizeRows = False
        Me.DGV_giri.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_giri.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DGC_nomegiro})
        Me.DGV_giri.Location = New System.Drawing.Point(12, 35)
        Me.DGV_giri.MultiSelect = False
        Me.DGV_giri.Name = "DGV_giri"
        Me.DGV_giri.ReadOnly = True
        Me.DGV_giri.RowHeadersVisible = False
        Me.DGV_giri.RowHeadersWidth = 20
        Me.DGV_giri.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_giri.Size = New System.Drawing.Size(254, 474)
        Me.DGV_giri.TabIndex = 2
        '
        'DGC_nomegiro
        '
        Me.DGC_nomegiro.DataPropertyName = "GIR_nome"
        Me.DGC_nomegiro.HeaderText = "Nome giro"
        Me.DGC_nomegiro.Name = "DGC_nomegiro"
        Me.DGC_nomegiro.ReadOnly = True
        Me.DGC_nomegiro.Width = 234
        '
        'GRP_giro
        '
        Me.GRP_giro.Controls.Add(Me.Label3)
        Me.GRP_giro.Controls.Add(Me.TXB_sigla)
        Me.GRP_giro.Controls.Add(Me.DGV_giri_gest)
        Me.GRP_giro.Controls.Add(Me.Label2)
        Me.GRP_giro.Controls.Add(Me.Label1)
        Me.GRP_giro.Controls.Add(Me.TXB_descrizione)
        Me.GRP_giro.Controls.Add(Me.TXB_nome)
        Me.GRP_giro.Controls.Add(Me.LBL_nome)
        Me.GRP_giro.Location = New System.Drawing.Point(278, 28)
        Me.GRP_giro.Name = "GRP_giro"
        Me.GRP_giro.Size = New System.Drawing.Size(575, 271)
        Me.GRP_giro.TabIndex = 3
        Me.GRP_giro.TabStop = False
        Me.GRP_giro.Text = "Definizione del raggruppamento di giri"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(492, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Sigla"
        '
        'TXB_sigla
        '
        Me.TXB_sigla.Location = New System.Drawing.Point(528, 19)
        Me.TXB_sigla.Name = "TXB_sigla"
        Me.TXB_sigla.Size = New System.Drawing.Size(41, 20)
        Me.TXB_sigla.TabIndex = 4
        '
        'DGV_giri_gest
        '
        Me.DGV_giri_gest.AllowUserToAddRows = False
        Me.DGV_giri_gest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_giri_gest.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_nome_gest, Me.COL_descrizione_gest})
        Me.DGV_giri_gest.Location = New System.Drawing.Point(9, 89)
        Me.DGV_giri_gest.Name = "DGV_giri_gest"
        Me.DGV_giri_gest.ReadOnly = True
        Me.DGV_giri_gest.RowHeadersWidth = 35
        Me.DGV_giri_gest.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_giri_gest.Size = New System.Drawing.Size(560, 171)
        Me.DGV_giri_gest.TabIndex = 6
        '
        'COL_nome_gest
        '
        Me.COL_nome_gest.DataPropertyName = "GIR_nome"
        Me.COL_nome_gest.HeaderText = "Nome"
        Me.COL_nome_gest.Name = "COL_nome_gest"
        Me.COL_nome_gest.ReadOnly = True
        Me.COL_nome_gest.Width = 200
        '
        'COL_descrizione_gest
        '
        Me.COL_descrizione_gest.DataPropertyName = "GIR_descrizione"
        Me.COL_descrizione_gest.HeaderText = "Descrizione"
        Me.COL_descrizione_gest.Name = "COL_descrizione_gest"
        Me.COL_descrizione_gest.ReadOnly = True
        Me.COL_descrizione_gest.Width = 306
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(161, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Elenco delle vie associate al giro"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Descrizione"
        '
        'TXB_descrizione
        '
        Me.TXB_descrizione.Location = New System.Drawing.Point(74, 45)
        Me.TXB_descrizione.Multiline = True
        Me.TXB_descrizione.Name = "TXB_descrizione"
        Me.TXB_descrizione.Size = New System.Drawing.Size(495, 20)
        Me.TXB_descrizione.TabIndex = 5
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSB_aggiungi, Me.TSB_cancella, Me.TSB_link_gxc, Me.TSB_unlink_gxc, Me.ToolStripSeparator1, Me.TSB_salva, Me.TSB_ignora})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(859, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'TSB_aggiungi
        '
        Me.TSB_aggiungi.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_aggiungi.Image = Global.Invoicer.My.Resources.Resources.PNG_add
        Me.TSB_aggiungi.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_aggiungi.Name = "TSB_aggiungi"
        Me.TSB_aggiungi.Size = New System.Drawing.Size(23, 22)
        Me.TSB_aggiungi.ToolTipText = "Aggiungi giro consegna"
        '
        'TSB_cancella
        '
        Me.TSB_cancella.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_cancella.Image = Global.Invoicer.My.Resources.Resources.PNG_delete
        Me.TSB_cancella.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_cancella.Name = "TSB_cancella"
        Me.TSB_cancella.Size = New System.Drawing.Size(23, 22)
        Me.TSB_cancella.ToolTipText = "Rimuovi giro di consegna"
        '
        'TSB_link_gxc
        '
        Me.TSB_link_gxc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_link_gxc.Image = Global.Invoicer.My.Resources.Resources.PNG_chain
        Me.TSB_link_gxc.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_link_gxc.Name = "TSB_link_gxc"
        Me.TSB_link_gxc.Size = New System.Drawing.Size(23, 22)
        Me.TSB_link_gxc.ToolTipText = "Associa Toponimo a Giro di consegna"
        '
        'TSB_unlink_gxc
        '
        Me.TSB_unlink_gxc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_unlink_gxc.Image = Global.Invoicer.My.Resources.Resources.PNG_chain_brk
        Me.TSB_unlink_gxc.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_unlink_gxc.Name = "TSB_unlink_gxc"
        Me.TSB_unlink_gxc.Size = New System.Drawing.Size(23, 22)
        Me.TSB_unlink_gxc.ToolTipText = "Rimuovi associazione Toponimo a Giro di consegna"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'TSB_salva
        '
        Me.TSB_salva.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_salva.Image = Global.Invoicer.My.Resources.Resources.PNG_salva
        Me.TSB_salva.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_salva.Name = "TSB_salva"
        Me.TSB_salva.Size = New System.Drawing.Size(23, 22)
        Me.TSB_salva.ToolTipText = "Salva modifiche"
        '
        'TSB_ignora
        '
        Me.TSB_ignora.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_ignora.Image = Global.Invoicer.My.Resources.Resources.PNG_ignore
        Me.TSB_ignora.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_ignora.Name = "TSB_ignora"
        Me.TSB_ignora.Size = New System.Drawing.Size(23, 22)
        Me.TSB_ignora.ToolTipText = "Annulla modifiche"
        '
        'DGV_giri_na
        '
        Me.DGV_giri_na.AllowUserToAddRows = False
        Me.DGV_giri_na.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_giri_na.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        Me.DGV_giri_na.Location = New System.Drawing.Point(6, 19)
        Me.DGV_giri_na.Name = "DGV_giri_na"
        Me.DGV_giri_na.ReadOnly = True
        Me.DGV_giri_na.RowHeadersWidth = 35
        Me.DGV_giri_na.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_giri_na.Size = New System.Drawing.Size(560, 180)
        Me.DGV_giri_na.TabIndex = 7
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "GIR_nome"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Nome"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "GIR_descrizione"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Descrizione"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 306
        '
        'GRP_giri_na
        '
        Me.GRP_giri_na.Controls.Add(Me.DGV_giri_na)
        Me.GRP_giri_na.Location = New System.Drawing.Point(278, 305)
        Me.GRP_giri_na.Name = "GRP_giri_na"
        Me.GRP_giri_na.Size = New System.Drawing.Size(575, 205)
        Me.GRP_giri_na.TabIndex = 9
        Me.GRP_giri_na.TabStop = False
        Me.GRP_giri_na.Text = "Giri disponibili"
        '
        'FRM_giro_x_gir
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(859, 522)
        Me.Controls.Add(Me.GRP_giri_na)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GRP_giro)
        Me.Controls.Add(Me.DGV_giri)
        Me.Name = "FRM_giro_x_gir"
        Me.Text = "Definizione raggruppamento di giro"
        CType(Me.DGV_giri, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRP_giro.ResumeLayout(False)
        Me.GRP_giro.PerformLayout()
        CType(Me.DGV_giri_gest, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.DGV_giri_na, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRP_giri_na.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TXB_nome As System.Windows.Forms.TextBox
    Friend WithEvents LBL_nome As System.Windows.Forms.Label
    Friend WithEvents DGV_giri As System.Windows.Forms.DataGridView
    Friend WithEvents GRP_giro As System.Windows.Forms.GroupBox
    Friend WithEvents DGV_giri_gest As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXB_descrizione As System.Windows.Forms.TextBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents TSB_aggiungi As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_cancella As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_link_gxc As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_unlink_gxc As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSB_salva As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_ignora As System.Windows.Forms.ToolStripButton
    Friend WithEvents DGC_nomegiro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TXB_sigla As System.Windows.Forms.TextBox
    Friend WithEvents DGV_giri_na As System.Windows.Forms.DataGridView
    Friend WithEvents GRP_giri_na As System.Windows.Forms.GroupBox
    Friend WithEvents COL_nome_gest As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COL_descrizione_gest As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
