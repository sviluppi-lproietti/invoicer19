Imports ISC.LibrerieComuni.OggettiComuni

Public Class FRM_mng_toponimi

    Private _data_toponimi As DS_toponimi
    Private _ToponimoEditato As Boolean
    Private _StartBlank As Boolean

    Private Sub FRM_mng_toponimi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lda As Layer_DataAccess
        Dim dv As DataView
        Dim bind As BindingSource

        Try
            _ToponimoEditato = False
            _data_toponimi = New DS_toponimi

            ' Caricvamento dei dati dei toponimi.
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_toponimi)
            lda.CaricaDataSet("TBL_toponimi")
            lda.CaricaDataSet("TBL_toponimi_gen")

            bind = New BindingSource
            bind.DataSource = _data_toponimi
            bind.DataMember = "TBL_toponimi"
            bind.Sort = "TOP_nome"

            dv = New DataView(_data_toponimi.TBL_toponimi_gen)
            dv.RowFilter = "TOG_cod_top = -1"
            dv.Sort = "TOG_nome"

            DGV_toponimi.AutoGenerateColumns = False
            DGV_toponimi.DataSource = bind
            DGV_toponimi_gest.AutoGenerateColumns = False
            DGV_toponimi_na.AutoGenerateColumns = False
            DGV_toponimi_na.DataSource = dv

            TXB_toponimo.DataBindings.Clear()
            TXB_toponimo.DataBindings.Add(New Binding("Text", bind, "TOP_nome", True))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub DGR_toponimi_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_toponimi.CellEnter
        Dim dr_top As DS_toponimi.TBL_toponimiRow
        Dim bm As BindingManagerBase
        Dim dv As DataView

        bm = DGV_toponimi.BindingContext(DGV_toponimi.DataSource, DGV_toponimi.DataMember)
        dr_top = CType(CType(bm.Current, DataRowView).Row, DS_toponimi.TBL_toponimiRow)
        dv = New DataView(_data_toponimi.TBL_toponimi_gen)
        dv.RowFilter = String.Concat("TOG_cod_top = ", dr_top.TOP_codice)
        dv.Sort = "TOG_nome"
        DGV_toponimi_gest.AutoGenerateColumns = False
        DGV_toponimi_gest.DataSource = dv

    End Sub

    Private Sub TXB_toponimo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXB_toponimo.TextChanged
        Dim bm As BindingManagerBase
        Dim dr_top As DS_toponimi.TBL_toponimiRow

        If DGV_toponimi.Rows.Count > 0 Then
            bm = DGV_toponimi.BindingContext(DGV_toponimi.DataSource, DGV_toponimi.DataMember)
            dr_top = CType(CType(bm.Current, DataRowView).Row, DS_toponimi.TBL_toponimiRow)
            '_ToponimoEditato = Not dr_top.Item("TOP_nome", DataRowVersion.Original) = TXB_toponimo.Text
            _ToponimoEditato = Not dr_top.TOP_nome = TXB_toponimo.Text
        Else
            _ToponimoEditato = False
        End If

    End Sub

    Private Sub DGR_toponimi_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DGV_toponimi.CellValidating
        Dim bm As BindingManagerBase
        Dim dr_top As DS_toponimi.TBL_toponimiRow

        If _ToponimoEditato Then
            e.Cancel = MessageBox.Show("Il toponimo � stato modificato. Spostandosi su un altro record si perderanno le modifiche effettuate. Procedo comunque?", "Toponimo modificato", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No
            If Not e.Cancel Then
                bm = DGV_toponimi.BindingContext(DGV_toponimi.DataSource, DGV_toponimi.DataMember)
                dr_top = CType(CType(bm.Current, DataRowView).Row, DS_toponimi.TBL_toponimiRow)
                dr_top.RejectChanges()
                _ToponimoEditato = False
            End If
        End If

    End Sub

    ' Aggiungi dati toponimo gestito
    Private Sub TSB_aggiungi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_aggiungi.Click
        Dim dr_tog As DS_toponimi.TBL_toponimi_genRow
        Dim dr_top As DS_toponimi.TBL_toponimiRow
        Dim bm_top As BindingManagerBase
        Dim bm_tog As BindingManagerBase
        Dim oAnswer As DialogResult

        If DGV_toponimi_na.Rows.Count > 0 Then
            oAnswer = MessageBox.Show("Creo il toponimo a partire da quello generico selezionato?", "Creazione toponimo", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
        Else
            oAnswer = Windows.Forms.DialogResult.No
        End If
        If Not (oAnswer = Windows.Forms.DialogResult.Cancel) Then
            _StartBlank = (oAnswer = Windows.Forms.DialogResult.No)
            bm_top = DGV_toponimi.BindingContext(DGV_toponimi.DataSource, DGV_toponimi.DataMember)
            bm_top.AddNew()
            dr_top = CType(CType(bm_top.Current, DataRowView).Row, DS_toponimi.TBL_toponimiRow)
            If _StartBlank Then
                dr_top.TOP_nome = ""
            Else
                bm_tog = DGV_toponimi_na.BindingContext(DGV_toponimi_na.DataSource, DGV_toponimi_na.DataMember)
                dr_tog = CType(CType(bm_tog.Current, DataRowView).Row, DS_toponimi.TBL_toponimi_genRow)
                dr_top.TOP_nome = dr_tog.TOG_nome
            End If
            bm_top.EndCurrentEdit()
            TXB_toponimo.Focus()
            _ToponimoEditato = True
        End If

    End Sub

    Private Sub TSB_cancella_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_cancella.Click
        Dim dr_tog As DS_toponimi.TBL_toponimi_genRow
        Dim dr_top As DS_toponimi.TBL_toponimiRow
        Dim bm_top As BindingManagerBase
        Dim bm_tog As BindingManagerBase
        Dim lda As Layer_DataAccess
        Dim drv As DataRowView

        If MessageBox.Show("Procedo con la cancellazione del toponimo selezionato?", "Cancellazione toponimo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bm_top = DGV_toponimi.BindingContext(DGV_toponimi.DataSource, DGV_toponimi.DataMember)
            bm_tog = DGV_toponimi_gest.BindingContext(DGV_toponimi_gest.DataSource, DGV_toponimi_gest.DataMember)

            For Each drv In CType(DGV_toponimi_gest.DataSource, DataView)
                dr_tog = drv.Row
                dr_tog.TOG_cod_top = -1
            Next
            dr_top = CType(CType(bm_top.Current, DataRowView).Row, DS_toponimi.TBL_toponimiRow)
            dr_top.Delete()

            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_toponimi)
            lda.AggiornaDataSet("TBL_toponimi")
            lda.AggiornaDataSet("TBL_toponimi_gen")
            bm_top.EndCurrentEdit()
            Me.Refresh()
        End If

    End Sub

    Private Sub TSB_link_top_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_link_top.Click
        Dim dr_tog As DS_toponimi.TBL_toponimi_genRow
        Dim dr_top As DS_toponimi.TBL_toponimiRow
        Dim bm_top As BindingManagerBase
        Dim bm_tog As BindingManagerBase
        Dim lda As Layer_DataAccess

        If MessageBox.Show("Procedo con l'associazione tra il toponimo e il toponimo generico?", "Conferma associazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bm_top = DGV_toponimi.BindingContext(DGV_toponimi.DataSource, DGV_toponimi.DataMember)
            bm_tog = DGV_toponimi_na.BindingContext(DGV_toponimi_na.DataSource, DGV_toponimi_na.DataMember)
            dr_top = CType(CType(bm_top.Current, DataRowView).Row, DS_toponimi.TBL_toponimiRow)
            dr_tog = CType(CType(bm_tog.Current, DataRowView).Row, DS_toponimi.TBL_toponimi_genRow)
            dr_tog.TOG_cod_top = dr_top.TOP_codice
            bm_tog.EndCurrentEdit()
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_toponimi)
            lda.AggiornaDataSet("TBL_toponimi_gen")
        End If

    End Sub

    Private Sub TSB_unlink_top_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_unlink_top.Click
        Dim dr_tog As DS_toponimi.TBL_toponimi_genRow
        Dim bm_tog As BindingManagerBase
        Dim lda As Layer_DataAccess

        If MessageBox.Show("Procedo con la disassociazione tra il toponimo e il toponimo generico?", "Conferma associazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bm_tog = DGV_toponimi_gest.BindingContext(DGV_toponimi_gest.DataSource, DGV_toponimi_gest.DataMember)
            dr_tog = CType(CType(bm_tog.Current, DataRowView).Row, DS_toponimi.TBL_toponimi_genRow)
            dr_tog.TOG_cod_top = -1
            bm_tog.EndCurrentEdit()
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_toponimi)
            lda.AggiornaDataSet("TBL_toponimi_gen")
        End If

    End Sub

    ' Salva dati 
    Private Sub TSB_salva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_salva.Click
        Dim dr_tog As DS_toponimi.TBL_toponimi_genRow
        Dim dr_top As DS_toponimi.TBL_toponimiRow
        Dim bm_top As BindingManagerBase
        Dim bm_tog As BindingManagerBase
        Dim lda As Layer_DataAccess
        Dim lAdd As Boolean

        bm_top = DGV_toponimi.BindingContext(DGV_toponimi.DataSource, DGV_toponimi.DataMember)
        bm_top.EndCurrentEdit()
        dr_top = CType(CType(bm_top.Current, DataRowView).Row, DS_toponimi.TBL_toponimiRow)
        lAdd = (dr_top.RowState = DataRowState.Added)
        lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_toponimi)
        lda.AggiornaDataSet("TBL_toponimi")
        If lAdd Then
            If _StartBlank Then
                dr_tog = _data_toponimi.TBL_toponimi_gen.NewRow
                dr_tog.TOG_cod_top = dr_top.TOP_codice
                dr_tog.TOG_nome = dr_top.TOP_nome
                _data_toponimi.TBL_toponimi_gen.Rows.Add(dr_tog)
            Else
                bm_tog = DGV_toponimi_na.BindingContext(DGV_toponimi_na.DataSource, DGV_toponimi_na.DataMember)
                dr_tog = CType(CType(bm_tog.Current, DataRowView).Row, DS_toponimi.TBL_toponimi_genRow)
                dr_tog.TOG_cod_top = dr_top.TOP_codice
            End If
            lda.AggiornaDataSet("TBL_toponimi_gen")
        End If
        _ToponimoEditato = False

    End Sub

    Private Sub TSB_ignora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_ignora.Click
        Dim dr_top As DS_toponimi.TBL_toponimiRow
        Dim bm_top As BindingManagerBase

        bm_top = DGV_toponimi.BindingContext(DGV_toponimi.DataSource, DGV_toponimi.DataMember)
        dr_top = CType(CType(bm_top.Current, DataRowView).Row, DS_toponimi.TBL_toponimiRow)
        If dr_top.RowState = DataRowState.Added Then
            dr_top.Delete()
        Else
            bm_top.CancelCurrentEdit()
        End If
        _ToponimoEditato = False

    End Sub

End Class