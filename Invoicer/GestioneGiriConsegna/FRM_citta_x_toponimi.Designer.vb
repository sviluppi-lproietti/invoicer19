<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_citta_x_toponimi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LBL_citta = New System.Windows.Forms.Label
        Me.DGV_citta = New System.Windows.Forms.DataGridView
        Me.COL_citta = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.DGV_toponimi = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.TSB_link_cxt = New System.Windows.Forms.ToolStripButton
        Me.GRP_add_ass = New System.Windows.Forms.GroupBox
        Me.CMB_citta = New System.Windows.Forms.ComboBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.CMB_toponimo = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.TSB_unlink_cxt = New System.Windows.Forms.ToolStripButton
        CType(Me.DGV_citta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_toponimi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.GRP_add_ass.SuspendLayout()
        Me.SuspendLayout()
        '
        'LBL_citta
        '
        Me.LBL_citta.AutoSize = True
        Me.LBL_citta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_citta.Location = New System.Drawing.Point(9, 34)
        Me.LBL_citta.Name = "LBL_citta"
        Me.LBL_citta.Size = New System.Drawing.Size(75, 13)
        Me.LBL_citta.TabIndex = 8
        Me.LBL_citta.Text = "Elenco citt�"
        '
        'DGV_citta
        '
        Me.DGV_citta.AllowUserToAddRows = False
        Me.DGV_citta.AllowUserToDeleteRows = False
        Me.DGV_citta.AllowUserToOrderColumns = True
        Me.DGV_citta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_citta.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_citta})
        Me.DGV_citta.Location = New System.Drawing.Point(12, 50)
        Me.DGV_citta.Name = "DGV_citta"
        Me.DGV_citta.Size = New System.Drawing.Size(410, 316)
        Me.DGV_citta.TabIndex = 7
        '
        'COL_citta
        '
        Me.COL_citta.DataPropertyName = "CIT_nome"
        Me.COL_citta.HeaderText = "Citt�"
        Me.COL_citta.Name = "COL_citta"
        Me.COL_citta.ReadOnly = True
        Me.COL_citta.Width = 350
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(428, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Elenco toponimi gestiti"
        '
        'DGV_toponimi
        '
        Me.DGV_toponimi.AllowUserToAddRows = False
        Me.DGV_toponimi.AllowUserToDeleteRows = False
        Me.DGV_toponimi.AllowUserToOrderColumns = True
        Me.DGV_toponimi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_toponimi.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1})
        Me.DGV_toponimi.Location = New System.Drawing.Point(428, 50)
        Me.DGV_toponimi.Name = "DGV_toponimi"
        Me.DGV_toponimi.Size = New System.Drawing.Size(410, 316)
        Me.DGV_toponimi.TabIndex = 9
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "TOP_nome"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Toponimo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 350
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSB_link_cxt, Me.TSB_unlink_cxt})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(850, 25)
        Me.ToolStrip1.TabIndex = 12
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'TSB_link_cxt
        '
        Me.TSB_link_cxt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_link_cxt.Image = Global.Invoicer.My.Resources.Resources.PNG_chain
        Me.TSB_link_cxt.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_link_cxt.Name = "TSB_link_cxt"
        Me.TSB_link_cxt.Size = New System.Drawing.Size(23, 22)
        Me.TSB_link_cxt.Text = "ToolStripButton1"
        Me.TSB_link_cxt.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.TSB_link_cxt.ToolTipText = "Associa citt� - toponimo "
        '
        'GRP_add_ass
        '
        Me.GRP_add_ass.Controls.Add(Me.CMB_citta)
        Me.GRP_add_ass.Controls.Add(Me.Button1)
        Me.GRP_add_ass.Controls.Add(Me.Label3)
        Me.GRP_add_ass.Controls.Add(Me.CMB_toponimo)
        Me.GRP_add_ass.Controls.Add(Me.Label2)
        Me.GRP_add_ass.Location = New System.Drawing.Point(12, 372)
        Me.GRP_add_ass.Name = "GRP_add_ass"
        Me.GRP_add_ass.Size = New System.Drawing.Size(826, 83)
        Me.GRP_add_ass.TabIndex = 13
        Me.GRP_add_ass.TabStop = False
        Me.GRP_add_ass.Text = "Aggiungi associazione"
        '
        'CMB_citta
        '
        Me.CMB_citta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_citta.FormattingEnabled = True
        Me.CMB_citta.Location = New System.Drawing.Point(74, 24)
        Me.CMB_citta.Name = "CMB_citta"
        Me.CMB_citta.Size = New System.Drawing.Size(474, 21)
        Me.CMB_citta.TabIndex = 4
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(710, 35)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Salva"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 54)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Toponimo"
        '
        'CMB_toponimo
        '
        Me.CMB_toponimo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_toponimo.FormattingEnabled = True
        Me.CMB_toponimo.Location = New System.Drawing.Point(74, 51)
        Me.CMB_toponimo.Name = "CMB_toponimo"
        Me.CMB_toponimo.Size = New System.Drawing.Size(474, 21)
        Me.CMB_toponimo.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Citt�"
        '
        'TSB_unlink_cxt
        '
        Me.TSB_unlink_cxt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_unlink_cxt.Image = Global.Invoicer.My.Resources.Resources.PNG_chain_brk
        Me.TSB_unlink_cxt.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_unlink_cxt.Name = "TSB_unlink_cxt"
        Me.TSB_unlink_cxt.Size = New System.Drawing.Size(23, 22)
        Me.TSB_unlink_cxt.Text = "ToolStripButton2"
        Me.TSB_unlink_cxt.ToolTipText = "Rimuovi associazione citt� toponimo"
        '
        'FRM_citta_x_toponimi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(850, 464)
        Me.Controls.Add(Me.GRP_add_ass)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DGV_toponimi)
        Me.Controls.Add(Me.LBL_citta)
        Me.Controls.Add(Me.DGV_citta)
        Me.Name = "FRM_citta_x_toponimi"
        Me.Text = "Gestione associazione citt�-toponimi"
        CType(Me.DGV_citta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_toponimi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GRP_add_ass.ResumeLayout(False)
        Me.GRP_add_ass.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LBL_citta As System.Windows.Forms.Label
    Friend WithEvents DGV_citta As System.Windows.Forms.DataGridView
    Friend WithEvents COL_citta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DGV_toponimi As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents TSB_link_cxt As System.Windows.Forms.ToolStripButton
    Friend WithEvents GRP_add_ass As System.Windows.Forms.GroupBox
    Friend WithEvents CMB_citta As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CMB_toponimo As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TSB_unlink_cxt As System.Windows.Forms.ToolStripButton
End Class
