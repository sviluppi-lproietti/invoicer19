Imports System.Data.OleDb
Imports System.IO
Imports System.IO.Compression
Imports ISC.LibrerieComuni.OggettiComuni
Imports OggettiComuniForm.FC_forms

Public Class FRM_giro_x_gir

    Private _data_giri_gir As DS_giri_gir

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lda As Layer_DataAccess
        Dim bind As BindingSource
        Dim dv_gxg As DataView

        Try
            _data_giri_gir = New DS_giri_gir
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri_gir)
            lda.CaricaDataSet("TBL_giri", New String(,) {{"FEQ", "", "GIR_tipo", "R"}})
            lda.CaricaDataSet("QRY_giri_x_gir")
            lda.CaricaDataSet("QRY_giri_giri")

            bind = New BindingSource
            bind.DataSource = _data_giri_gir
            bind.DataMember = "TBL_giri"
            bind.Sort = "GIR_nome"

            dv_gxg = New DataView(_data_giri_gir.QRY_giri_x_gir)

            DGV_giri.AutoGenerateColumns = False
            DGV_giri.DataSource = bind

            DGV_giri_gest.AutoGenerateColumns = False
            DGV_giri_gest.DataSource = dv_gxg

            TXB_nome.DataBindings.Clear()
            TXB_nome.DataBindings.Add(New Binding("Text", bind, "GIR_nome", True))
            TXB_sigla.DataBindings.Clear()
            TXB_sigla.DataBindings.Add(New Binding("Text", bind, "GIR_sigla", True))
            TXB_descrizione.DataBindings.Clear()
            TXB_descrizione.DataBindings.Add(New Binding("Text", bind, "GIR_descrizione", True))
            DGV_giri_CellMouseClick(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub DGV_giri_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_giri.CellEnter
        Dim lda As Layer_DataAccess
        Dim dv As DataView
        Dim dr_gxg As DS_giri_gir.QRY_giri_x_girRow
        Dim dr_gxg1 As DS_giri_gir.QRY_giri_giriRow

        lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri_gir)
        lda.PulisciTabella("QRY_giri_giri")
        lda.CaricaDataSet("QRY_giri_giri")
        For Each dr_gxg In _data_giri_gir.QRY_giri_x_gir
            dr_gxg1 = _data_giri_gir.QRY_giri_giri.FindByGIR_codice(dr_gxg.GXG_cod_gir_s)
            dr_gxg1.Delete()
        Next
        _data_giri_gir.QRY_giri_giri.AcceptChanges()
        dv = New DataView(_data_giri_gir.QRY_giri_giri)
        dv.Sort = "GIR_nome"
        DGV_giri_na.AutoGenerateColumns = False
        DGV_giri_na.DataSource = dv

    End Sub

    Private Sub DGV_giri_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles DGV_giri.CellMouseClick
        'Dim oComBuil As OleDb.OleDbCommandBuilder
        'Dim oDa As OleDb.OleDbDataAdapter
        'Dim oCmd As OleDb.OleDbCommand
        'Dim nGiro As Integer

        'Dim bm As BindingManagerBase
        'Dim dr_gir As DS_giri.TBL_giriRow

        'bm = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
        'dr_gir = CType(CType(bm.Current, DataRowView).Row, DS_giri.TBL_giriRow)
        'nGiro = dr_gir.GIR_codice
        '_data_giri.QRY_cit_x_top.Rows.Clear()
        '' carica i dati delle vie
        'oCmd = New OleDb.OleDbCommand
        'oCmd.Connection = New OleDb.OleDbConnection(_DBGestioneGiriConnStr)
        '' Salvataggio della tabella TBL_main
        'oCmd.CommandText = String.Concat("SELECT * FROM QRY_cit_x_top WHERE GXC_cod_gir = ", nGiro)
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_giri, "QRY_cit_x_top")
        'oCmd.Connection.Close()
        'DataGridView2.DataSource = _data_giri.QRY_cit_x_top

    End Sub

    Private Sub TSB_aggiungi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_aggiungi.Click
        Dim bm_gir As BindingManagerBase

        If MessageBox.Show("Creo un nuovo giro di consegna?", "Creazione giro di consegna", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bm_gir = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
            bm_gir.AddNew()
            bm_gir.EndCurrentEdit()
        End If

    End Sub

    Private Sub TSB_link_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_link_gxc.Click
        Dim bm_gir As BindingManagerBase
        Dim dr_gir As DS_giri_gir.TBL_giriRow
        Dim bm_gir_s As BindingManagerBase
        Dim dr_gir_s As DS_giri_gir.QRY_giri_giriRow
        Dim lda As Layer_DataAccess
        Dim dr_gxg As DS_giri_gir.TBL_giri_x_girRow
        Dim dr_gxg1 As DS_giri_gir.QRY_giri_giriRow
        Dim dr_gxg_2 As DS_giri_gir.QRY_giri_x_girRow

        If MessageBox.Show("Procedo con l'associazione tra i giri?", "Conferma associazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bm_gir = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
            dr_gir = CType(CType(bm_gir.Current, DataRowView).Row, DS_giri_gir.TBL_giriRow)
            bm_gir_s = DGV_giri_na.BindingContext(DGV_giri_na.DataSource, DGV_giri_na.DataMember)
            dr_gir_s = CType(CType(bm_gir_s.Current, DataRowView).Row, DS_giri_gir.QRY_giri_giriRow)

            dr_gxg = _data_giri_gir.TBL_giri_x_gir.NewRow
            dr_gxg.GXG_cod_gir_m = dr_gir.GIR_codice
            dr_gxg.GXG_cod_gir_s = dr_gir_s.GIR_codice
            _data_giri_gir.TBL_giri_x_gir.Rows.Add(dr_gxg)
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri_gir)
            lda.AggiornaDataSet("TBL_giri_x_gir")
            lda.PulisciTabella("QRY_giri_x_gir")
            lda.CaricaDataSet("QRY_giri_x_gir")

            Dim dv_gxg As DataView
            dv_gxg = New DataView(_data_giri_gir.QRY_giri_x_gir)

            DGV_giri_gest.DataSource = dv_gxg

            lda.CaricaDataSet("QRY_giri_giri")
            For Each dr_gxg_2 In _data_giri_gir.QRY_giri_x_gir
                dr_gxg1 = _data_giri_gir.QRY_giri_giri.FindByGIR_codice(dr_gxg_2.GXG_cod_gir_s)
                dr_gxg1.Delete()
            Next
        End If

    End Sub

    Private Sub TSB_salva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_salva.Click
        Dim dr_gir As DS_giri_gir.TBL_giriRow
        Dim bm_gir As BindingManagerBase
        Dim lda As Layer_DataAccess

        bm_gir = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
        bm_gir.EndCurrentEdit()
        dr_gir = CType(CType(bm_gir.Current, DataRowView).Row, DS_giri_gir.TBL_giriRow)
        dr_gir.GIR_tipo = "R"
        lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri_gir)
        lda.AggiornaDataSet("TBL_giri")

    End Sub

    Private Sub TSB_ignora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_ignora.Click
        Dim bm_gir As BindingManagerBase

        bm_gir = DGV_giri.BindingContext(DGV_giri.DataSource, DGV_giri.DataMember)
        bm_gir.CancelCurrentEdit()

    End Sub

    Private Sub TSB_unlink_gxc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSB_unlink_gxc.Click
        Dim dr_gxg_q As DS_giri_gir.QRY_giri_x_girRow
        Dim dr_gxg_t As DS_giri_gir.TBL_giri_x_girRow
        Dim lda As Layer_DataAccess
        Dim dr_gxg_2 As DS_giri_gir.QRY_giri_x_girRow
        Dim dr_gxg1 As DS_giri_gir.QRY_giri_giriRow

        If MessageBox.Show("Procedo con la rimozione dell'associazione tra il giro di consegna e la coppia citt�-toponimo?", "Conferma rimozione associazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            lda = New Layer_DataAccess(ObjGlobali.CFG.DBGiriConsegna_CS, _data_giri_gir)
            For Each dgvr As DataGridViewRow In DGV_giri_gest.SelectedRows
                dr_gxg_q = DirectCast(DGV_giri_gest.Rows(dgvr.Index).DataBoundItem, DataRowView).Row
                lda.PulisciTabella("TBL_giri_x_gir")
                lda.CaricaDataSet("TBL_giri_x_gir", New String(,) {{"FEQ", "", "GXG_codice", dr_gxg_q.GXG_codice}})
                dr_gxg_t = _data_giri_gir.TBL_giri_x_gir.Rows(0)
                dr_gxg_t.Delete()
                lda.AggiornaDataSet("TBL_giri_x_gir")
            Next
            lda.PulisciTabella("QRY_giri_x_gir")
            lda.CaricaDataSet("QRY_giri_x_gir")
            lda.CaricaDataSet("QRY_giri_giri")
            For Each dr_gxg_2 In _data_giri_gir.QRY_giri_x_gir
                dr_gxg1 = _data_giri_gir.QRY_giri_giri.FindByGIR_codice(dr_gxg_2.GXG_cod_gir_s)
                dr_gxg1.Delete()
            Next
        End If

    End Sub

End Class
