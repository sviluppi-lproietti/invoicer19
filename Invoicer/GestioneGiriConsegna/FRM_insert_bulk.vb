Imports ISC.LibrerieComuni.OggettiComuni

Public Class FRM_insert_bulk

    Private _FileNameCSV As String
    Private _ForceExit As Boolean
    Private _RowInsert As Integer
    Private _RowReaded As Integer
    Private _thr As System.Threading.Thread

    Private Sub BTN_inserisci_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_inserisci.Click

        If BTN_inserisci.Text.ToLower = "inserisci" Then
            _FileNameCSV = TXB_file_import.Text
            _ForceExit = False
            BTN_inserisci.Text = "Ferma"
            TMR_insert.Start()
            _thr = New Threading.Thread(AddressOf InserimentoBulk)
            _thr.Start()
        Else
            _ForceExit = True
        End If

    End Sub

    Private Sub InserimentoBulk()
        Dim dr_bli As DS_bulk_insert.TBL_bulk_insertRow
        Dim sr As System.IO.StreamReader
        Dim data_bulk As DS_bulk_insert
        Dim mng_gir As FC_manage_giri
        Dim cLine As String

        data_bulk = New DS_bulk_insert
        sr = New System.IO.StreamReader(_FileNameCSV)
        sr.ReadLine()

        _RowReaded = 0
        While Not sr.EndOfStream And Not _ForceExit
            cLine = sr.ReadLine()
            _RowReaded += 1
            If (cLine > "") And cLine.Split(";").Length = 3 Then
                dr_bli = data_bulk.TBL_bulk_insert.NewRow
                dr_bli.BLI_giro = cLine.Split(";")(0)
                dr_bli.BLI_citta = cLine.Split(";")(1)
                dr_bli.BLI_toponimo = cLine.Split(";")(2)
                data_bulk.TBL_bulk_insert.Rows.Add(dr_bli)
            End If
        End While
        sr.Close()
        data_bulk.TBL_bulk_insert.AcceptChanges()

        _RowInsert = 0
        mng_gir = New FC_manage_giri(ObjGlobali.CFG.DBGiriConsegna_CS)
        For Each dr_bli In data_bulk.TBL_bulk_insert.Rows
            mng_gir.InserisciDatiBulk(dr_bli.BLI_giro, dr_bli.BLI_citta, dr_bli.BLI_toponimo)
            _RowInsert += 1
            If _ForceExit Then
                Exit For
            End If
        Next

    End Sub

    Private Sub TMR_insert_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMR_insert.Tick

        LBL_righe_lette.Text = _RowReaded
        LBL_righe_insDB.Text = _RowInsert
        Me.Refresh()
        If Not _thr.IsAlive Then
            TMR_insert.Stop()
            BTN_inserisci.Text = "Inserisci"
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim cPath As String
        Dim cFile As String

        OFD_new_file.Multiselect = False
        cFile = ""
        cPath = ""
        If TXB_file_import.Text.EndsWith("\") Then
            cPath = TXB_file_import.Text
        Else
            If System.IO.File.Exists(TXB_file_import.Text) Then
                cFile = System.IO.Path.GetFileName(TXB_file_import.Text)
                cPath = TXB_file_import.Text.Replace(cFile, "")
            Else
                cPath = System.IO.Path.GetDirectoryName(TXB_file_import.Text)
            End If
        End If
        OFD_new_file.InitialDirectory = cPath
        OFD_new_file.FileName = cFile

        If OFD_new_file.ShowDialog = Windows.Forms.DialogResult.OK Then
            TXB_file_import.Text = OFD_new_file.FileName
        End If

    End Sub

    Private Sub TXB_file_import_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXB_file_import.TextChanged

        BTN_inserisci.Enabled = System.IO.File.Exists(TXB_file_import.Text)

    End Sub

    Private Sub BTN_chiudi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_chiudi.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel

    End Sub

End Class
