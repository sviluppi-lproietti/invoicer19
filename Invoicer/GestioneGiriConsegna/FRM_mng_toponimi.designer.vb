<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_mng_toponimi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DGV_toponimi = New System.Windows.Forms.DataGridView
        Me.COL_toponimo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DGV_toponimi_gest = New System.Windows.Forms.DataGridView
        Me.COL_toponimo_gen = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TXB_toponimo = New System.Windows.Forms.TextBox
        Me.DGV_toponimi_na = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LBL_toponimo = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.TSB_aggiungi = New System.Windows.Forms.ToolStripButton
        Me.TSB_cancella = New System.Windows.Forms.ToolStripButton
        Me.TSB_link_top = New System.Windows.Forms.ToolStripButton
        Me.TSB_unlink_top = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.TSB_salva = New System.Windows.Forms.ToolStripButton
        Me.TSB_ignora = New System.Windows.Forms.ToolStripButton
        CType(Me.DGV_toponimi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_toponimi_gest, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGV_toponimi_na, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DGV_toponimi
        '
        Me.DGV_toponimi.AllowUserToAddRows = False
        Me.DGV_toponimi.AllowUserToDeleteRows = False
        Me.DGV_toponimi.AllowUserToOrderColumns = True
        Me.DGV_toponimi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_toponimi.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_toponimo})
        Me.DGV_toponimi.Location = New System.Drawing.Point(12, 50)
        Me.DGV_toponimi.MultiSelect = False
        Me.DGV_toponimi.Name = "DGV_toponimi"
        Me.DGV_toponimi.Size = New System.Drawing.Size(410, 440)
        Me.DGV_toponimi.TabIndex = 0
        '
        'COL_toponimo
        '
        Me.COL_toponimo.DataPropertyName = "TOP_nome"
        Me.COL_toponimo.HeaderText = "Toponimo"
        Me.COL_toponimo.Name = "COL_toponimo"
        Me.COL_toponimo.ReadOnly = True
        Me.COL_toponimo.Width = 350
        '
        'DGV_toponimi_gest
        '
        Me.DGV_toponimi_gest.AllowUserToAddRows = False
        Me.DGV_toponimi_gest.AllowUserToDeleteRows = False
        Me.DGV_toponimi_gest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_toponimi_gest.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_toponimo_gen})
        Me.DGV_toponimi_gest.Location = New System.Drawing.Point(6, 55)
        Me.DGV_toponimi_gest.Name = "DGV_toponimi_gest"
        Me.DGV_toponimi_gest.Size = New System.Drawing.Size(470, 123)
        Me.DGV_toponimi_gest.TabIndex = 1
        '
        'COL_toponimo_gen
        '
        Me.COL_toponimo_gen.DataPropertyName = "TOG_nome"
        Me.COL_toponimo_gen.HeaderText = "Varianti"
        Me.COL_toponimo_gen.Name = "COL_toponimo_gen"
        Me.COL_toponimo_gen.ReadOnly = True
        Me.COL_toponimo_gen.Width = 410
        '
        'TXB_toponimo
        '
        Me.TXB_toponimo.Location = New System.Drawing.Point(51, 16)
        Me.TXB_toponimo.Multiline = True
        Me.TXB_toponimo.Name = "TXB_toponimo"
        Me.TXB_toponimo.Size = New System.Drawing.Size(425, 20)
        Me.TXB_toponimo.TabIndex = 2
        '
        'DGV_toponimi_na
        '
        Me.DGV_toponimi_na.AllowUserToAddRows = False
        Me.DGV_toponimi_na.AllowUserToDeleteRows = False
        Me.DGV_toponimi_na.AllowUserToResizeColumns = False
        Me.DGV_toponimi_na.AllowUserToResizeRows = False
        Me.DGV_toponimi_na.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_toponimi_na.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1})
        Me.DGV_toponimi_na.Location = New System.Drawing.Point(6, 19)
        Me.DGV_toponimi_na.Name = "DGV_toponimi_na"
        Me.DGV_toponimi_na.Size = New System.Drawing.Size(470, 242)
        Me.DGV_toponimi_na.TabIndex = 3
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "TOG_nome"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Toponimo generico"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 410
        '
        'LBL_toponimo
        '
        Me.LBL_toponimo.AutoSize = True
        Me.LBL_toponimo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_toponimo.Location = New System.Drawing.Point(12, 34)
        Me.LBL_toponimo.Name = "LBL_toponimo"
        Me.LBL_toponimo.Size = New System.Drawing.Size(135, 13)
        Me.LBL_toponimo.TabIndex = 6
        Me.LBL_toponimo.Text = "Elenco toponimi gestiti"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.DGV_toponimi_gest)
        Me.GroupBox1.Controls.Add(Me.TXB_toponimo)
        Me.GroupBox1.Location = New System.Drawing.Point(438, 34)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(482, 185)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Dati toponimo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Variante del toponimo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Nome"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DGV_toponimi_na)
        Me.GroupBox2.Location = New System.Drawing.Point(438, 225)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(482, 265)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Toponimi non assegnati"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator1, Me.TSB_aggiungi, Me.TSB_cancella, Me.TSB_link_top, Me.TSB_unlink_top, Me.ToolStripSeparator2, Me.TSB_salva, Me.TSB_ignora})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(932, 25)
        Me.ToolStrip1.TabIndex = 9
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'TSB_aggiungi
        '
        Me.TSB_aggiungi.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_aggiungi.Image = Global.Invoicer.My.Resources.Resources.PNG_add
        Me.TSB_aggiungi.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_aggiungi.Name = "TSB_aggiungi"
        Me.TSB_aggiungi.Size = New System.Drawing.Size(23, 22)
        Me.TSB_aggiungi.ToolTipText = "Aggiungi toponimo"
        '
        'TSB_cancella
        '
        Me.TSB_cancella.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_cancella.Image = Global.Invoicer.My.Resources.Resources.PNG_delete
        Me.TSB_cancella.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_cancella.Name = "TSB_cancella"
        Me.TSB_cancella.Size = New System.Drawing.Size(23, 22)
        Me.TSB_cancella.ToolTipText = "Cancella Toponimo"
        '
        'TSB_link_top
        '
        Me.TSB_link_top.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_link_top.Image = Global.Invoicer.My.Resources.Resources.PNG_chain
        Me.TSB_link_top.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_link_top.Name = "TSB_link_top"
        Me.TSB_link_top.Size = New System.Drawing.Size(23, 22)
        Me.TSB_link_top.Text = "ToolStripButton3"
        Me.TSB_link_top.ToolTipText = "Associa toponimo generico"
        '
        'TSB_unlink_top
        '
        Me.TSB_unlink_top.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_unlink_top.Image = Global.Invoicer.My.Resources.Resources.PNG_chain_brk
        Me.TSB_unlink_top.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_unlink_top.Name = "TSB_unlink_top"
        Me.TSB_unlink_top.Size = New System.Drawing.Size(23, 22)
        Me.TSB_unlink_top.ToolTipText = "Rimuovi associazione toponimo"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'TSB_salva
        '
        Me.TSB_salva.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_salva.Image = Global.Invoicer.My.Resources.Resources.PNG_salva
        Me.TSB_salva.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_salva.Name = "TSB_salva"
        Me.TSB_salva.Size = New System.Drawing.Size(23, 22)
        Me.TSB_salva.Text = "ToolStripButton1"
        Me.TSB_salva.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.TSB_salva.ToolTipText = "Salva modifiche"
        '
        'TSB_ignora
        '
        Me.TSB_ignora.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TSB_ignora.Image = Global.Invoicer.My.Resources.Resources.PNG_ignore
        Me.TSB_ignora.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_ignora.Name = "TSB_ignora"
        Me.TSB_ignora.Size = New System.Drawing.Size(23, 22)
        Me.TSB_ignora.ToolTipText = "Annulla modifiche"
        '
        'FRM_mng_toponimi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(932, 497)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.LBL_toponimo)
        Me.Controls.Add(Me.DGV_toponimi)
        Me.Name = "FRM_mng_toponimi"
        Me.Text = "Gestione toponimi"
        CType(Me.DGV_toponimi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_toponimi_gest, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGV_toponimi_na, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DGV_toponimi As System.Windows.Forms.DataGridView
    Friend WithEvents DGV_toponimi_gest As System.Windows.Forms.DataGridView
    Friend WithEvents TXB_toponimo As System.Windows.Forms.TextBox
    Friend WithEvents DGV_toponimi_na As System.Windows.Forms.DataGridView
    Friend WithEvents LBL_toponimo As System.Windows.Forms.Label
    Friend WithEvents COL_toponimo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents COL_toponimo_gen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSB_aggiungi As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_link_top As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_salva As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_ignora As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSB_unlink_top As System.Windows.Forms.ToolStripButton
    Friend WithEvents TSB_cancella As System.Windows.Forms.ToolStripButton
End Class
