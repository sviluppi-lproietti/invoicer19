Imports ISC.LibrerieComuni.OggettiComuni

Public Class FRM_select_cxt

    Private _DBGestioneGiriConnStr As String
    Private _data_citta_x_toponimi As DS_ass_giro_cxt

    WriteOnly Property DBGestioneGiriConnStr() As String

        Set(ByVal value As String)
            _DBGestioneGiriConnStr = value
        End Set

    End Property

    Private Sub FRM_select_cxt_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lda As Layer_DataAccess

        _data_citta_x_toponimi = New DS_ass_giro_cxt
        lda = New Layer_DataAccess(_DBGestioneGiriConnStr, _data_citta_x_toponimi)
        lda.CaricaDataSet("TBL_citta")
        lda.CaricaDataSet("QRY_ass_giri_x_cxt")

        DGV_toponimi.AutoGenerateColumns = False
        CMB_citta.ValueMember = "CIT_codice"
        CMB_citta.DisplayMember = "CIT_nome"
        CMB_citta.DataSource = _data_citta_x_toponimi.TBL_citta

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Me.DialogResult = Windows.Forms.DialogResult.OK

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel

    End Sub

    Private Sub CMB_citta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CMB_citta.SelectedIndexChanged
        Dim dv As DataView

        dv = New DataView(_data_citta_x_toponimi.QRY_ass_giri_x_cxt)
        If CMB_citta.SelectedIndex > -1 Then
            dv.RowFilter = String.Concat("CXT_cod_cit = ", CMB_citta.SelectedValue)
        End If
        dv.Sort = "TOP_nome"
        DGV_toponimi.DataSource = dv


    End Sub

End Class