<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_insert_bulk
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.TXB_file_import = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.BTN_inserisci = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.LBL_righe_lette = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.LBL_righe_insDB = New System.Windows.Forms.Label
        Me.BTN_chiudi = New System.Windows.Forms.Button
        Me.TMR_insert = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button
        Me.OFD_new_file = New System.Windows.Forms.OpenFileDialog
        Me.SuspendLayout()
        '
        'TXB_file_import
        '
        Me.TXB_file_import.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.TXB_file_import.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem
        Me.TXB_file_import.Location = New System.Drawing.Point(147, 12)
        Me.TXB_file_import.Name = "TXB_file_import"
        Me.TXB_file_import.Size = New System.Drawing.Size(457, 20)
        Me.TXB_file_import.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Nome del file da importare"
        '
        'BTN_inserisci
        '
        Me.BTN_inserisci.Location = New System.Drawing.Point(234, 94)
        Me.BTN_inserisci.Name = "BTN_inserisci"
        Me.BTN_inserisci.Size = New System.Drawing.Size(75, 23)
        Me.BTN_inserisci.TabIndex = 2
        Me.BTN_inserisci.Text = "Inserisci"
        Me.BTN_inserisci.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Righe lette dal file"
        '
        'LBL_righe_lette
        '
        Me.LBL_righe_lette.Location = New System.Drawing.Point(144, 44)
        Me.LBL_righe_lette.Name = "LBL_righe_lette"
        Me.LBL_righe_lette.Size = New System.Drawing.Size(91, 13)
        Me.LBL_righe_lette.TabIndex = 4
        Me.LBL_righe_lette.Text = "N/D"
        Me.LBL_righe_lette.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(106, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Righe inserite nel DB"
        '
        'LBL_righe_insDB
        '
        Me.LBL_righe_insDB.Location = New System.Drawing.Point(144, 69)
        Me.LBL_righe_insDB.Name = "LBL_righe_insDB"
        Me.LBL_righe_insDB.Size = New System.Drawing.Size(91, 13)
        Me.LBL_righe_insDB.TabIndex = 6
        Me.LBL_righe_insDB.Text = "N/D"
        Me.LBL_righe_insDB.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'BTN_chiudi
        '
        Me.BTN_chiudi.Location = New System.Drawing.Point(335, 94)
        Me.BTN_chiudi.Name = "BTN_chiudi"
        Me.BTN_chiudi.Size = New System.Drawing.Size(75, 23)
        Me.BTN_chiudi.TabIndex = 7
        Me.BTN_chiudi.Text = "Chiudi"
        Me.BTN_chiudi.UseVisualStyleBackColor = True
        '
        'TMR_insert
        '
        Me.TMR_insert.Interval = 1000
        '
        'Button1
        '
        Me.Button1.Image = Global.Invoicer.My.Resources.Resources.PNG_file
        Me.Button1.Location = New System.Drawing.Point(610, 10)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(27, 23)
        Me.Button1.TabIndex = 8
        Me.Button1.UseVisualStyleBackColor = True
        '
        'OFD_new_file
        '
        Me.OFD_new_file.FileName = "OpenFileDialog1"
        Me.OFD_new_file.Filter = "File CSV|*.csv"
        '
        'FRM_insert_bulk
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(641, 133)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.BTN_chiudi)
        Me.Controls.Add(Me.LBL_righe_insDB)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.LBL_righe_lette)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.BTN_inserisci)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXB_file_import)
        Me.Name = "FRM_insert_bulk"
        Me.Text = "Inserimento giri-citt�-toponimi bulk"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TXB_file_import As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BTN_inserisci As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LBL_righe_lette As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents LBL_righe_insDB As System.Windows.Forms.Label
    Friend WithEvents BTN_chiudi As System.Windows.Forms.Button
    Friend WithEvents TMR_insert As System.Windows.Forms.Timer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents OFD_new_file As System.Windows.Forms.OpenFileDialog

End Class
