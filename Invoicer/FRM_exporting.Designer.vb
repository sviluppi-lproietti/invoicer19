<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_exporting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.BTN_annulla = New System.Windows.Forms.Button
        Me.GRP_act_in_prg = New System.Windows.Forms.GroupBox
        Me.PGB_dett_grp = New System.Windows.Forms.ProgressBar
        Me.LBL_act_runn = New System.Windows.Forms.Label
        Me.TMR_check = New System.Windows.Forms.Timer(Me.components)
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.LBL_tim_from_start = New System.Windows.Forms.Label
        Me.LBL_tim_to_end = New System.Windows.Forms.Label
        Me.GRP_act_in_prg.SuspendLayout()
        Me.SuspendLayout()
        '
        'BTN_annulla
        '
        Me.BTN_annulla.Location = New System.Drawing.Point(129, 122)
        Me.BTN_annulla.Name = "BTN_annulla"
        Me.BTN_annulla.Size = New System.Drawing.Size(75, 23)
        Me.BTN_annulla.TabIndex = 6
        Me.BTN_annulla.Text = "ANNULLA"
        Me.BTN_annulla.UseVisualStyleBackColor = True
        '
        'GRP_act_in_prg
        '
        Me.GRP_act_in_prg.Controls.Add(Me.PGB_dett_grp)
        Me.GRP_act_in_prg.Controls.Add(Me.LBL_act_runn)
        Me.GRP_act_in_prg.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GRP_act_in_prg.Location = New System.Drawing.Point(9, 41)
        Me.GRP_act_in_prg.Name = "GRP_act_in_prg"
        Me.GRP_act_in_prg.Size = New System.Drawing.Size(329, 75)
        Me.GRP_act_in_prg.TabIndex = 6
        Me.GRP_act_in_prg.TabStop = False
        Me.GRP_act_in_prg.Text = "Attivit� in corso"
        '
        'PGB_dett_grp
        '
        Me.PGB_dett_grp.Location = New System.Drawing.Point(6, 57)
        Me.PGB_dett_grp.Maximum = 4
        Me.PGB_dett_grp.Name = "PGB_dett_grp"
        Me.PGB_dett_grp.Size = New System.Drawing.Size(317, 10)
        Me.PGB_dett_grp.Step = 1
        Me.PGB_dett_grp.TabIndex = 5
        '
        'LBL_act_runn
        '
        Me.LBL_act_runn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_act_runn.Location = New System.Drawing.Point(5, 19)
        Me.LBL_act_runn.Name = "LBL_act_runn"
        Me.LBL_act_runn.Size = New System.Drawing.Size(318, 35)
        Me.LBL_act_runn.TabIndex = 0
        Me.LBL_act_runn.Text = "N/A"
        '
        'TMR_check
        '
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Tempo trascorso:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(108, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Tempo rimanente:"
        '
        'LBL_tim_from_start
        '
        Me.LBL_tim_from_start.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_tim_from_start.Location = New System.Drawing.Point(230, 9)
        Me.LBL_tim_from_start.Name = "LBL_tim_from_start"
        Me.LBL_tim_from_start.Size = New System.Drawing.Size(105, 13)
        Me.LBL_tim_from_start.TabIndex = 9
        Me.LBL_tim_from_start.Text = "N/A"
        Me.LBL_tim_from_start.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_tim_to_end
        '
        Me.LBL_tim_to_end.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_tim_to_end.Location = New System.Drawing.Point(230, 25)
        Me.LBL_tim_to_end.Name = "LBL_tim_to_end"
        Me.LBL_tim_to_end.Size = New System.Drawing.Size(105, 13)
        Me.LBL_tim_to_end.TabIndex = 10
        Me.LBL_tim_to_end.Text = "N/A"
        Me.LBL_tim_to_end.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'FRM_exporting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(343, 153)
        Me.Controls.Add(Me.LBL_tim_to_end)
        Me.Controls.Add(Me.LBL_tim_from_start)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GRP_act_in_prg)
        Me.Controls.Add(Me.BTN_annulla)
        Me.Name = "FRM_exporting"
        Me.Text = "Esportazione in corso"
        Me.GRP_act_in_prg.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BTN_annulla As System.Windows.Forms.Button
    Friend WithEvents GRP_act_in_prg As System.Windows.Forms.GroupBox
    Friend WithEvents LBL_act_runn As System.Windows.Forms.Label
    Friend WithEvents PGB_dett_grp As System.Windows.Forms.ProgressBar
    Friend WithEvents TMR_check As System.Windows.Forms.Timer
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents LBL_tim_from_start As System.Windows.Forms.Label
    Friend WithEvents LBL_tim_to_end As System.Windows.Forms.Label
End Class
