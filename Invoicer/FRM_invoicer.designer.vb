<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_Invoicer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FRM_Invoicer))
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TAC_principale = New System.Windows.Forms.TabControl()
        Me.TAP_dati = New System.Windows.Forms.TabPage()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.DGV_dati = New System.Windows.Forms.DataGridView()
        Me.CMS_dati = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CSM_seleziona = New System.Windows.Forms.ToolStripMenuItem()
        Me.CSM_deseleziona = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ResetStatoErroreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.LBL_err_desc = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LBL_stato_rec = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.LBL_err_fase = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.LBL_rec_selz = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.TAP_stampa = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LSB_moduli_stampare = New System.Windows.Forms.ListBox()
        Me.TAC_modulistampa = New System.Windows.Forms.TabControl()
        Me.TAP_modulo_00 = New System.Windows.Forms.TabPage()
        Me.LBL_stato_modulo_00 = New System.Windows.Forms.Label()
        Me.FLP_moduli_00 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GRB_stampante_car_00 = New System.Windows.Forms.GroupBox()
        Me.CHB_reset_cod_wr_car_00 = New System.Windows.Forms.CheckBox()
        Me.CHB_insert_fincatura_00 = New System.Windows.Forms.CheckBox()
        Me.TXB_cod_wr_car_00 = New System.Windows.Forms.TextBox()
        Me.CHB_fronte_retro_00 = New System.Windows.Forms.CheckBox()
        Me.LBL_stampante_00 = New System.Windows.Forms.Label()
        Me.CMB_stampanti_00 = New System.Windows.Forms.ComboBox()
        Me.DGV_ass_cassetti_00 = New System.Windows.Forms.DataGridView()
        Me.Pagina = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cassetto = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.LBL_fat_cor_x_00 = New System.Windows.Forms.Label()
        Me.RDB_cassetti_00 = New System.Windows.Forms.RadioButton()
        Me.TXB_fat_cor_car_x_00 = New System.Windows.Forms.TextBox()
        Me.CMB_cassetto_def_00 = New System.Windows.Forms.ComboBox()
        Me.LBL_fat_cor_y_00 = New System.Windows.Forms.Label()
        Me.RDB_default_00 = New System.Windows.Forms.RadioButton()
        Me.TXB_fat_cor_car_y_00 = New System.Windows.Forms.TextBox()
        Me.GRB_stampante_ele_00 = New System.Windows.Forms.GroupBox()
        Me.CHB_rendi_pari_00 = New System.Windows.Forms.CheckBox()
        Me.CHB_reset_cod_wr_ele_00 = New System.Windows.Forms.CheckBox()
        Me.TXB_cod_wr_ele_00 = New System.Windows.Forms.TextBox()
        Me.LBL_nome_file_arch_ott = New System.Windows.Forms.Label()
        Me.CMB_file_arch_ott_00 = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TXB_fat_cor_ele_x_00 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.TXB_fat_cor_ele_y_00 = New System.Windows.Forms.TextBox()
        Me.GRB_generazione_00 = New System.Windows.Forms.GroupBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TXB_Coordinata_Y_00 = New System.Windows.Forms.TextBox()
        Me.TXB_Coordinata_X_00 = New System.Windows.Forms.TextBox()
        Me.CHB_modulo_da_stampare_00 = New System.Windows.Forms.CheckBox()
        Me.FLP_param_stampa = New System.Windows.Forms.FlowLayoutPanel()
        Me.CHB_prn_arch_ott = New System.Windows.Forms.CheckBox()
        Me.PNL_documenti_cont = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.NUD_documenti_gruppo = New System.Windows.Forms.NumericUpDown()
        Me.CHB_clean_FLD_elab = New System.Windows.Forms.CheckBox()
        Me.CHB_eseg_poststampa = New System.Windows.Forms.CheckBox()
        Me.CHB_force_paper = New System.Windows.Forms.CheckBox()
        Me.CHB_fast_print = New System.Windows.Forms.CheckBox()
        Me.BTN_stampa = New System.Windows.Forms.Button()
        Me.CHB_fusione_moduli = New System.Windows.Forms.CheckBox()
        Me.TAP_export = New System.Windows.Forms.TabPage()
        Me.CHB_onlyselected = New System.Windows.Forms.CheckBox()
        Me.TXB_nomefile_export = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.BTN_esporta = New System.Windows.Forms.Button()
        Me.MES_principale = New System.Windows.Forms.MenuStrip()
        Me.TSM_sessione_stampa = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_nuovo = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_apri = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_sessioni_recenti = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSM_chiudi = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSM_esci = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_RecapitoViaMail = New System.Windows.Forms.ToolStripMenuItem()
        Me.AggiungiAllegatiDaInviareToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_gestione_modulo = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImpostaFattoreCorrezioneToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSM_pulisci_modulo = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_gestione_giri = New System.Windows.Forms.ToolStripMenuItem()
        Me.RaggruppamentoGiriConsegnaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_gest_giro = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSM_ass_cit_top = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_gest_citta = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_gest_topo = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSM_insert_bulk = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_Selezione = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_sel_tutti = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_sel_nessuno = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_con_err_gr = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_send_by_mail = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSM_sel_range = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_sel_norange = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSM_sel_inverti = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_ricerca = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_ric_progressivo = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_visualizza = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSM_vis_tutti = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_vis_noerror = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_vis_error = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_vis_err_load_lievi = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_vis_err_load_gravi = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_vis_err_prn = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.Utilit�ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_manage_azioni_ps = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_configurazioni = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSM_reset_prn_stat = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_EsportaFileDiLog = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSM_puliscicartellawork = New System.Windows.Forms.ToolStripMenuItem()
        Me.STS_bottom = New System.Windows.Forms.StatusStrip()
        Me.TSSL_versione_software = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSSL_stato_prn_pap = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSSL_stato_prn_ott = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSSL_versione_pdfcreator = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSSL_prnwrk = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TSSL_memorysize = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TOS_menu = New System.Windows.Forms.ToolStrip()
        Me.TSB_refresh = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSB_open_arcott_fld = New System.Windows.Forms.ToolStripButton()
        Me.TSB_open_documenti_unificati_fld = New System.Windows.Forms.ToolStripButton()
        Me.TSB_open_elaborati_fld = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.TSB_export_fld = New System.Windows.Forms.ToolStripButton()
        Me.IML_standard = New System.Windows.Forms.ImageList(Me.components)
        Me.TMR_sched_op = New System.Windows.Forms.Timer(Me.components)
        Me.GRB_anagraficasessione = New System.Windows.Forms.GroupBox()
        Me.LBL_sessione_nome = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LBL_plugin_versione = New System.Windows.Forms.Label()
        Me.LBL_sessione_cartella = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.LBL_plugin_filename = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.LBL_plugin_name = New System.Windows.Forms.Label()
        Me.LBL_sessione_Descrizione = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GRP_statistiche = New System.Windows.Forms.GroupBox()
        Me.LBL_dati_var = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.LBL_err_car_liev = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.LBL_rec_load = New System.Windows.Forms.Label()
        Me.LBL_err_prn = New System.Windows.Forms.Label()
        Me.LBL_rec_sel = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.LBL_err_car_grav = New System.Windows.Forms.Label()
        Me.TAC_statistiche = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.DGV_stat_glob = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.DGV_stat_parz = New System.Windows.Forms.DataGridView()
        Me.STA_descrizione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STS_stampate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TAC_principale.SuspendLayout()
        Me.TAP_dati.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.DGV_dati, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMS_dati.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TAP_stampa.SuspendLayout()
        Me.TAC_modulistampa.SuspendLayout()
        Me.TAP_modulo_00.SuspendLayout()
        Me.FLP_moduli_00.SuspendLayout()
        Me.GRB_stampante_car_00.SuspendLayout()
        CType(Me.DGV_ass_cassetti_00, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRB_stampante_ele_00.SuspendLayout()
        Me.GRB_generazione_00.SuspendLayout()
        Me.FLP_param_stampa.SuspendLayout()
        Me.PNL_documenti_cont.SuspendLayout()
        CType(Me.NUD_documenti_gruppo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TAP_export.SuspendLayout()
        Me.MES_principale.SuspendLayout()
        Me.STS_bottom.SuspendLayout()
        Me.TOS_menu.SuspendLayout()
        Me.GRB_anagraficasessione.SuspendLayout()
        Me.GRP_statistiche.SuspendLayout()
        Me.TAC_statistiche.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DGV_stat_glob, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.DGV_stat_parz, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TAC_principale
        '
        Me.TAC_principale.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TAC_principale.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TAC_principale.Controls.Add(Me.TAP_dati)
        Me.TAC_principale.Controls.Add(Me.TAP_stampa)
        Me.TAC_principale.Controls.Add(Me.TAP_export)
        Me.TAC_principale.Location = New System.Drawing.Point(10, 211)
        Me.TAC_principale.Name = "TAC_principale"
        Me.TAC_principale.SelectedIndex = 0
        Me.TAC_principale.Size = New System.Drawing.Size(1466, 623)
        Me.TAC_principale.TabIndex = 0
        '
        'TAP_dati
        '
        Me.TAP_dati.Controls.Add(Me.FlowLayoutPanel2)
        Me.TAP_dati.Location = New System.Drawing.Point(4, 25)
        Me.TAP_dati.Name = "TAP_dati"
        Me.TAP_dati.Padding = New System.Windows.Forms.Padding(3)
        Me.TAP_dati.Size = New System.Drawing.Size(1458, 594)
        Me.TAP_dati.TabIndex = 0
        Me.TAP_dati.Text = "Dati da stampare"
        Me.TAP_dati.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.DGV_dati)
        Me.FlowLayoutPanel2.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(1452, 588)
        Me.FlowLayoutPanel2.TabIndex = 24
        '
        'DGV_dati
        '
        Me.DGV_dati.AllowUserToAddRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_dati.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV_dati.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_dati.ContextMenuStrip = Me.CMS_dati
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_dati.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGV_dati.Location = New System.Drawing.Point(3, 3)
        Me.DGV_dati.Name = "DGV_dati"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_dati.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DGV_dati.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_dati.ShowEditingIcon = False
        Me.DGV_dati.Size = New System.Drawing.Size(1115, 582)
        Me.DGV_dati.TabIndex = 2
        '
        'CMS_dati
        '
        Me.CMS_dati.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CSM_seleziona, Me.CSM_deseleziona, Me.ToolStripMenuItem9, Me.ResetStatoErroreToolStripMenuItem})
        Me.CMS_dati.Name = "CMS_dati"
        Me.CMS_dati.Size = New System.Drawing.Size(166, 76)
        '
        'CSM_seleziona
        '
        Me.CSM_seleziona.Name = "CSM_seleziona"
        Me.CSM_seleziona.Size = New System.Drawing.Size(165, 22)
        Me.CSM_seleziona.Text = "Seleziona"
        '
        'CSM_deseleziona
        '
        Me.CSM_deseleziona.Name = "CSM_deseleziona"
        Me.CSM_deseleziona.Size = New System.Drawing.Size(165, 22)
        Me.CSM_deseleziona.Text = "Deseleziona"
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(162, 6)
        '
        'ResetStatoErroreToolStripMenuItem
        '
        Me.ResetStatoErroreToolStripMenuItem.Name = "ResetStatoErroreToolStripMenuItem"
        Me.ResetStatoErroreToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ResetStatoErroreToolStripMenuItem.Text = "Reset stato errore"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.Label30)
        Me.Panel1.Controls.Add(Me.LBL_err_desc)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.LBL_stato_rec)
        Me.Panel1.Controls.Add(Me.Label31)
        Me.Panel1.Controls.Add(Me.LBL_err_fase)
        Me.Panel1.Controls.Add(Me.Label33)
        Me.Panel1.Controls.Add(Me.LBL_rec_selz)
        Me.Panel1.Controls.Add(Me.Label35)
        Me.Panel1.Location = New System.Drawing.Point(1124, 3)
        Me.Panel1.MaximumSize = New System.Drawing.Size(317, 600)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(317, 582)
        Me.Panel1.TabIndex = 23
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(290, 49)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(20, 18)
        Me.PictureBox2.TabIndex = 17
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(290, 25)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 18)
        Me.PictureBox1.TabIndex = 16
        Me.PictureBox1.TabStop = False
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(55, 7)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(208, 13)
        Me.Label30.TabIndex = 15
        Me.Label30.Text = "STATO DEL RECORD  CORRENTE"
        '
        'LBL_err_desc
        '
        Me.LBL_err_desc.Location = New System.Drawing.Point(7, 133)
        Me.LBL_err_desc.Name = "LBL_err_desc"
        Me.LBL_err_desc.Size = New System.Drawing.Size(303, 426)
        Me.LBL_err_desc.TabIndex = 9
        Me.LBL_err_desc.Text = "N/D"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, 112)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Descrizione errore....:"
        '
        'LBL_stato_rec
        '
        Me.LBL_stato_rec.AutoSize = True
        Me.LBL_stato_rec.Location = New System.Drawing.Point(142, 51)
        Me.LBL_stato_rec.Name = "LBL_stato_rec"
        Me.LBL_stato_rec.Size = New System.Drawing.Size(28, 13)
        Me.LBL_stato_rec.TabIndex = 14
        Me.LBL_stato_rec.Text = "N/D"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(6, 28)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(130, 13)
        Me.Label31.TabIndex = 11
        Me.Label31.Text = "Selezionabile...........:"
        '
        'LBL_err_fase
        '
        Me.LBL_err_fase.AutoSize = True
        Me.LBL_err_fase.Location = New System.Drawing.Point(7, 90)
        Me.LBL_err_fase.Name = "LBL_err_fase"
        Me.LBL_err_fase.Size = New System.Drawing.Size(28, 13)
        Me.LBL_err_fase.TabIndex = 8
        Me.LBL_err_fase.Text = "N/D"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(7, 51)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(129, 13)
        Me.Label33.TabIndex = 13
        Me.Label33.Text = "Selezionato.............:"
        '
        'LBL_rec_selz
        '
        Me.LBL_rec_selz.AutoSize = True
        Me.LBL_rec_selz.Location = New System.Drawing.Point(142, 28)
        Me.LBL_rec_selz.Name = "LBL_rec_selz"
        Me.LBL_rec_selz.Size = New System.Drawing.Size(28, 13)
        Me.LBL_rec_selz.TabIndex = 12
        Me.LBL_rec_selz.Text = "N/D"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(7, 74)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(118, 13)
        Me.Label35.TabIndex = 7
        Me.Label35.Text = "Errore nella fase di:"
        '
        'TAP_stampa
        '
        Me.TAP_stampa.Controls.Add(Me.Label1)
        Me.TAP_stampa.Controls.Add(Me.LSB_moduli_stampare)
        Me.TAP_stampa.Controls.Add(Me.TAC_modulistampa)
        Me.TAP_stampa.Controls.Add(Me.FLP_param_stampa)
        Me.TAP_stampa.Controls.Add(Me.CHB_fusione_moduli)
        Me.TAP_stampa.Location = New System.Drawing.Point(4, 25)
        Me.TAP_stampa.Name = "TAP_stampa"
        Me.TAP_stampa.Padding = New System.Windows.Forms.Padding(3)
        Me.TAP_stampa.Size = New System.Drawing.Size(1458, 594)
        Me.TAP_stampa.TabIndex = 1
        Me.TAP_stampa.Text = "Stampa"
        Me.TAP_stampa.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 16)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Moduli da stampare"
        '
        'LSB_moduli_stampare
        '
        Me.LSB_moduli_stampare.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSB_moduli_stampare.FormattingEnabled = True
        Me.LSB_moduli_stampare.ItemHeight = 16
        Me.LSB_moduli_stampare.Location = New System.Drawing.Point(6, 29)
        Me.LSB_moduli_stampare.Name = "LSB_moduli_stampare"
        Me.LSB_moduli_stampare.Size = New System.Drawing.Size(317, 116)
        Me.LSB_moduli_stampare.Sorted = True
        Me.LSB_moduli_stampare.TabIndex = 19
        '
        'TAC_modulistampa
        '
        Me.TAC_modulistampa.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TAC_modulistampa.Controls.Add(Me.TAP_modulo_00)
        Me.TAC_modulistampa.Location = New System.Drawing.Point(329, 7)
        Me.TAC_modulistampa.Name = "TAC_modulistampa"
        Me.TAC_modulistampa.SelectedIndex = 0
        Me.TAC_modulistampa.Size = New System.Drawing.Size(1013, 471)
        Me.TAC_modulistampa.TabIndex = 18
        '
        'TAP_modulo_00
        '
        Me.TAP_modulo_00.Controls.Add(Me.LBL_stato_modulo_00)
        Me.TAP_modulo_00.Controls.Add(Me.FLP_moduli_00)
        Me.TAP_modulo_00.Controls.Add(Me.CHB_modulo_da_stampare_00)
        Me.TAP_modulo_00.Location = New System.Drawing.Point(4, 25)
        Me.TAP_modulo_00.Name = "TAP_modulo_00"
        Me.TAP_modulo_00.Padding = New System.Windows.Forms.Padding(3)
        Me.TAP_modulo_00.Size = New System.Drawing.Size(1005, 442)
        Me.TAP_modulo_00.TabIndex = 0
        Me.TAP_modulo_00.Text = "TabPage1"
        Me.TAP_modulo_00.UseVisualStyleBackColor = True
        '
        'LBL_stato_modulo_00
        '
        Me.LBL_stato_modulo_00.AutoSize = True
        Me.LBL_stato_modulo_00.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_stato_modulo_00.Location = New System.Drawing.Point(143, 5)
        Me.LBL_stato_modulo_00.Name = "LBL_stato_modulo_00"
        Me.LBL_stato_modulo_00.Size = New System.Drawing.Size(34, 16)
        Me.LBL_stato_modulo_00.TabIndex = 18
        Me.LBL_stato_modulo_00.Text = "N/A"
        '
        'FLP_moduli_00
        '
        Me.FLP_moduli_00.Controls.Add(Me.GRB_stampante_car_00)
        Me.FLP_moduli_00.Controls.Add(Me.GRB_stampante_ele_00)
        Me.FLP_moduli_00.Controls.Add(Me.GRB_generazione_00)
        Me.FLP_moduli_00.Location = New System.Drawing.Point(6, 29)
        Me.FLP_moduli_00.Name = "FLP_moduli_00"
        Me.FLP_moduli_00.Size = New System.Drawing.Size(993, 410)
        Me.FLP_moduli_00.TabIndex = 17
        '
        'GRB_stampante_car_00
        '
        Me.GRB_stampante_car_00.AutoSize = True
        Me.GRB_stampante_car_00.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.GRB_stampante_car_00.Controls.Add(Me.CHB_reset_cod_wr_car_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.CHB_insert_fincatura_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.TXB_cod_wr_car_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.CHB_fronte_retro_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.LBL_stampante_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.CMB_stampanti_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.DGV_ass_cassetti_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.LBL_fat_cor_x_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.RDB_cassetti_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.TXB_fat_cor_car_x_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.CMB_cassetto_def_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.LBL_fat_cor_y_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.RDB_default_00)
        Me.GRB_stampante_car_00.Controls.Add(Me.TXB_fat_cor_car_y_00)
        Me.GRB_stampante_car_00.Location = New System.Drawing.Point(3, 3)
        Me.GRB_stampante_car_00.Name = "GRB_stampante_car_00"
        Me.GRB_stampante_car_00.Size = New System.Drawing.Size(985, 235)
        Me.GRB_stampante_car_00.TabIndex = 3
        Me.GRB_stampante_car_00.TabStop = False
        Me.GRB_stampante_car_00.Text = "Impostazioni stampante cartacea"
        '
        'CHB_reset_cod_wr_car_00
        '
        Me.CHB_reset_cod_wr_car_00.AutoSize = True
        Me.CHB_reset_cod_wr_car_00.Location = New System.Drawing.Point(748, 77)
        Me.CHB_reset_cod_wr_car_00.Name = "CHB_reset_cod_wr_car_00"
        Me.CHB_reset_cod_wr_car_00.Size = New System.Drawing.Size(164, 17)
        Me.CHB_reset_cod_wr_car_00.TabIndex = 10
        Me.CHB_reset_cod_wr_car_00.Text = "Resetta codice Wrap Around"
        Me.CHB_reset_cod_wr_car_00.UseVisualStyleBackColor = True
        '
        'CHB_insert_fincatura_00
        '
        Me.CHB_insert_fincatura_00.AutoSize = True
        Me.CHB_insert_fincatura_00.Location = New System.Drawing.Point(748, 100)
        Me.CHB_insert_fincatura_00.Name = "CHB_insert_fincatura_00"
        Me.CHB_insert_fincatura_00.Size = New System.Drawing.Size(111, 17)
        Me.CHB_insert_fincatura_00.TabIndex = 12
        Me.CHB_insert_fincatura_00.Text = "Inserisci Fincatura"
        Me.CHB_insert_fincatura_00.UseVisualStyleBackColor = True
        '
        'TXB_cod_wr_car_00
        '
        Me.TXB_cod_wr_car_00.Location = New System.Drawing.Point(927, 75)
        Me.TXB_cod_wr_car_00.Name = "TXB_cod_wr_car_00"
        Me.TXB_cod_wr_car_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_cod_wr_car_00.TabIndex = 11
        Me.TXB_cod_wr_car_00.Text = "0"
        Me.TXB_cod_wr_car_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CHB_fronte_retro_00
        '
        Me.CHB_fronte_retro_00.AutoSize = True
        Me.CHB_fronte_retro_00.Location = New System.Drawing.Point(748, 123)
        Me.CHB_fronte_retro_00.Name = "CHB_fronte_retro_00"
        Me.CHB_fronte_retro_00.Size = New System.Drawing.Size(121, 17)
        Me.CHB_fronte_retro_00.TabIndex = 13
        Me.CHB_fronte_retro_00.Text = "Stampa Fronte/retro"
        Me.CHB_fronte_retro_00.UseVisualStyleBackColor = True
        '
        'LBL_stampante_00
        '
        Me.LBL_stampante_00.AutoSize = True
        Me.LBL_stampante_00.Location = New System.Drawing.Point(3, 24)
        Me.LBL_stampante_00.Name = "LBL_stampante_00"
        Me.LBL_stampante_00.Size = New System.Drawing.Size(58, 13)
        Me.LBL_stampante_00.TabIndex = 0
        Me.LBL_stampante_00.Text = "Stampante"
        '
        'CMB_stampanti_00
        '
        Me.CMB_stampanti_00.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_stampanti_00.FormattingEnabled = True
        Me.CMB_stampanti_00.Location = New System.Drawing.Point(129, 21)
        Me.CMB_stampanti_00.Name = "CMB_stampanti_00"
        Me.CMB_stampanti_00.Size = New System.Drawing.Size(554, 21)
        Me.CMB_stampanti_00.TabIndex = 1
        '
        'DGV_ass_cassetti_00
        '
        Me.DGV_ass_cassetti_00.AllowUserToAddRows = False
        Me.DGV_ass_cassetti_00.AllowUserToDeleteRows = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_ass_cassetti_00.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DGV_ass_cassetti_00.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_ass_cassetti_00.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Pagina, Me.Cassetto})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_ass_cassetti_00.DefaultCellStyle = DataGridViewCellStyle5
        Me.DGV_ass_cassetti_00.Location = New System.Drawing.Point(129, 75)
        Me.DGV_ass_cassetti_00.Name = "DGV_ass_cassetti_00"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_ass_cassetti_00.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DGV_ass_cassetti_00.RowHeadersWidth = 4
        Me.DGV_ass_cassetti_00.Size = New System.Drawing.Size(554, 141)
        Me.DGV_ass_cassetti_00.TabIndex = 5
        '
        'Pagina
        '
        Me.Pagina.DataPropertyName = "CAP_desc_pag"
        Me.Pagina.HeaderText = "Pagine"
        Me.Pagina.Name = "Pagina"
        Me.Pagina.ReadOnly = True
        Me.Pagina.Width = 200
        '
        'Cassetto
        '
        Me.Cassetto.DataPropertyName = "CAP_cassetto"
        Me.Cassetto.HeaderText = "Cassetto"
        Me.Cassetto.Name = "Cassetto"
        Me.Cassetto.Width = 330
        '
        'LBL_fat_cor_x_00
        '
        Me.LBL_fat_cor_x_00.AutoSize = True
        Me.LBL_fat_cor_x_00.Location = New System.Drawing.Point(765, 24)
        Me.LBL_fat_cor_x_00.Name = "LBL_fat_cor_x_00"
        Me.LBL_fat_cor_x_00.Size = New System.Drawing.Size(113, 13)
        Me.LBL_fat_cor_x_00.TabIndex = 6
        Me.LBL_fat_cor_x_00.Text = "Fattore di correzione X"
        '
        'RDB_cassetti_00
        '
        Me.RDB_cassetti_00.AutoSize = True
        Me.RDB_cassetti_00.Location = New System.Drawing.Point(6, 80)
        Me.RDB_cassetti_00.Name = "RDB_cassetti_00"
        Me.RDB_cassetti_00.Size = New System.Drawing.Size(110, 17)
        Me.RDB_cassetti_00.TabIndex = 4
        Me.RDB_cassetti_00.Text = "Seleziona cassetti"
        Me.RDB_cassetti_00.UseVisualStyleBackColor = True
        '
        'TXB_fat_cor_car_x_00
        '
        Me.TXB_fat_cor_car_x_00.Location = New System.Drawing.Point(927, 21)
        Me.TXB_fat_cor_car_x_00.Name = "TXB_fat_cor_car_x_00"
        Me.TXB_fat_cor_car_x_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_fat_cor_car_x_00.TabIndex = 7
        Me.TXB_fat_cor_car_x_00.Text = "0"
        Me.TXB_fat_cor_car_x_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CMB_cassetto_def_00
        '
        Me.CMB_cassetto_def_00.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_cassetto_def_00.FormattingEnabled = True
        Me.CMB_cassetto_def_00.Location = New System.Drawing.Point(129, 48)
        Me.CMB_cassetto_def_00.Name = "CMB_cassetto_def_00"
        Me.CMB_cassetto_def_00.Size = New System.Drawing.Size(554, 21)
        Me.CMB_cassetto_def_00.TabIndex = 3
        '
        'LBL_fat_cor_y_00
        '
        Me.LBL_fat_cor_y_00.AutoSize = True
        Me.LBL_fat_cor_y_00.Location = New System.Drawing.Point(765, 50)
        Me.LBL_fat_cor_y_00.Name = "LBL_fat_cor_y_00"
        Me.LBL_fat_cor_y_00.Size = New System.Drawing.Size(113, 13)
        Me.LBL_fat_cor_y_00.TabIndex = 8
        Me.LBL_fat_cor_y_00.Text = "Fattore di correzione Y"
        '
        'RDB_default_00
        '
        Me.RDB_default_00.AutoSize = True
        Me.RDB_default_00.Checked = True
        Me.RDB_default_00.Location = New System.Drawing.Point(6, 49)
        Me.RDB_default_00.Name = "RDB_default_00"
        Me.RDB_default_00.Size = New System.Drawing.Size(112, 17)
        Me.RDB_default_00.TabIndex = 2
        Me.RDB_default_00.TabStop = True
        Me.RDB_default_00.Text = "Cassetto di default"
        Me.RDB_default_00.UseVisualStyleBackColor = True
        '
        'TXB_fat_cor_car_y_00
        '
        Me.TXB_fat_cor_car_y_00.Location = New System.Drawing.Point(927, 48)
        Me.TXB_fat_cor_car_y_00.Name = "TXB_fat_cor_car_y_00"
        Me.TXB_fat_cor_car_y_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_fat_cor_car_y_00.TabIndex = 9
        Me.TXB_fat_cor_car_y_00.Text = "0"
        Me.TXB_fat_cor_car_y_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GRB_stampante_ele_00
        '
        Me.GRB_stampante_ele_00.AutoSize = True
        Me.GRB_stampante_ele_00.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.GRB_stampante_ele_00.Controls.Add(Me.CHB_rendi_pari_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.CHB_reset_cod_wr_ele_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.TXB_cod_wr_ele_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.LBL_nome_file_arch_ott)
        Me.GRB_stampante_ele_00.Controls.Add(Me.CMB_file_arch_ott_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.Label25)
        Me.GRB_stampante_ele_00.Controls.Add(Me.TXB_fat_cor_ele_x_00)
        Me.GRB_stampante_ele_00.Controls.Add(Me.Label29)
        Me.GRB_stampante_ele_00.Controls.Add(Me.TXB_fat_cor_ele_y_00)
        Me.GRB_stampante_ele_00.Location = New System.Drawing.Point(3, 244)
        Me.GRB_stampante_ele_00.Name = "GRB_stampante_ele_00"
        Me.GRB_stampante_ele_00.Size = New System.Drawing.Size(985, 82)
        Me.GRB_stampante_ele_00.TabIndex = 17
        Me.GRB_stampante_ele_00.TabStop = False
        Me.GRB_stampante_ele_00.Text = "Impostazioni stampante elettronica"
        '
        'CHB_rendi_pari_00
        '
        Me.CHB_rendi_pari_00.AutoSize = True
        Me.CHB_rendi_pari_00.Location = New System.Drawing.Point(9, 45)
        Me.CHB_rendi_pari_00.Name = "CHB_rendi_pari_00"
        Me.CHB_rendi_pari_00.Size = New System.Drawing.Size(148, 17)
        Me.CHB_rendi_pari_00.TabIndex = 5
        Me.CHB_rendi_pari_00.Text = "Pareggia il numero pagine"
        Me.CHB_rendi_pari_00.UseVisualStyleBackColor = True
        '
        'CHB_reset_cod_wr_ele_00
        '
        Me.CHB_reset_cod_wr_ele_00.AutoSize = True
        Me.CHB_reset_cod_wr_ele_00.Location = New System.Drawing.Point(163, 45)
        Me.CHB_reset_cod_wr_ele_00.Name = "CHB_reset_cod_wr_ele_00"
        Me.CHB_reset_cod_wr_ele_00.Size = New System.Drawing.Size(164, 17)
        Me.CHB_reset_cod_wr_ele_00.TabIndex = 6
        Me.CHB_reset_cod_wr_ele_00.Text = "Resetta codice Wrap Around"
        Me.CHB_reset_cod_wr_ele_00.UseVisualStyleBackColor = True
        '
        'TXB_cod_wr_ele_00
        '
        Me.TXB_cod_wr_ele_00.Location = New System.Drawing.Point(333, 43)
        Me.TXB_cod_wr_ele_00.Name = "TXB_cod_wr_ele_00"
        Me.TXB_cod_wr_ele_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_cod_wr_ele_00.TabIndex = 7
        Me.TXB_cod_wr_ele_00.Text = "0"
        Me.TXB_cod_wr_ele_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LBL_nome_file_arch_ott
        '
        Me.LBL_nome_file_arch_ott.AutoSize = True
        Me.LBL_nome_file_arch_ott.Location = New System.Drawing.Point(6, 21)
        Me.LBL_nome_file_arch_ott.Name = "LBL_nome_file_arch_ott"
        Me.LBL_nome_file_arch_ott.Size = New System.Drawing.Size(68, 13)
        Me.LBL_nome_file_arch_ott.TabIndex = 0
        Me.LBL_nome_file_arch_ott.Text = "Nome del file"
        '
        'CMB_file_arch_ott_00
        '
        Me.CMB_file_arch_ott_00.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_file_arch_ott_00.FormattingEnabled = True
        Me.CMB_file_arch_ott_00.Location = New System.Drawing.Point(128, 18)
        Me.CMB_file_arch_ott_00.Name = "CMB_file_arch_ott_00"
        Me.CMB_file_arch_ott_00.Size = New System.Drawing.Size(555, 21)
        Me.CMB_file_arch_ott_00.TabIndex = 1
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(765, 21)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(113, 13)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "Fattore di correzione X"
        '
        'TXB_fat_cor_ele_x_00
        '
        Me.TXB_fat_cor_ele_x_00.Location = New System.Drawing.Point(927, 18)
        Me.TXB_fat_cor_ele_x_00.Name = "TXB_fat_cor_ele_x_00"
        Me.TXB_fat_cor_ele_x_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_fat_cor_ele_x_00.TabIndex = 3
        Me.TXB_fat_cor_ele_x_00.Text = "0"
        Me.TXB_fat_cor_ele_x_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(765, 46)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(113, 13)
        Me.Label29.TabIndex = 8
        Me.Label29.Text = "Fattore di correzione Y"
        '
        'TXB_fat_cor_ele_y_00
        '
        Me.TXB_fat_cor_ele_y_00.Location = New System.Drawing.Point(927, 43)
        Me.TXB_fat_cor_ele_y_00.Name = "TXB_fat_cor_ele_y_00"
        Me.TXB_fat_cor_ele_y_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_fat_cor_ele_y_00.TabIndex = 9
        Me.TXB_fat_cor_ele_y_00.Text = "0"
        Me.TXB_fat_cor_ele_y_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GRB_generazione_00
        '
        Me.GRB_generazione_00.Controls.Add(Me.Label27)
        Me.GRB_generazione_00.Controls.Add(Me.Label28)
        Me.GRB_generazione_00.Controls.Add(Me.TXB_Coordinata_Y_00)
        Me.GRB_generazione_00.Controls.Add(Me.TXB_Coordinata_X_00)
        Me.GRB_generazione_00.Location = New System.Drawing.Point(3, 332)
        Me.GRB_generazione_00.Name = "GRB_generazione_00"
        Me.GRB_generazione_00.Size = New System.Drawing.Size(985, 51)
        Me.GRB_generazione_00.TabIndex = 16
        Me.GRB_generazione_00.TabStop = False
        Me.GRB_generazione_00.Text = "Coordinate del barcode di imbustamento"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(6, 22)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(68, 13)
        Me.Label27.TabIndex = 16
        Me.Label27.Text = "Coordinata X"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(340, 22)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(68, 13)
        Me.Label28.TabIndex = 17
        Me.Label28.Text = "Coordinata Y"
        '
        'TXB_Coordinata_Y_00
        '
        Me.TXB_Coordinata_Y_00.Location = New System.Drawing.Point(509, 19)
        Me.TXB_Coordinata_Y_00.Name = "TXB_Coordinata_Y_00"
        Me.TXB_Coordinata_Y_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_Coordinata_Y_00.TabIndex = 17
        Me.TXB_Coordinata_Y_00.Text = "0"
        Me.TXB_Coordinata_Y_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TXB_Coordinata_X_00
        '
        Me.TXB_Coordinata_X_00.Location = New System.Drawing.Point(175, 19)
        Me.TXB_Coordinata_X_00.Name = "TXB_Coordinata_X_00"
        Me.TXB_Coordinata_X_00.Size = New System.Drawing.Size(52, 20)
        Me.TXB_Coordinata_X_00.TabIndex = 16
        Me.TXB_Coordinata_X_00.Text = "0"
        Me.TXB_Coordinata_X_00.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CHB_modulo_da_stampare_00
        '
        Me.CHB_modulo_da_stampare_00.AutoSize = True
        Me.CHB_modulo_da_stampare_00.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CHB_modulo_da_stampare_00.Location = New System.Drawing.Point(9, 6)
        Me.CHB_modulo_da_stampare_00.Name = "CHB_modulo_da_stampare_00"
        Me.CHB_modulo_da_stampare_00.Size = New System.Drawing.Size(128, 17)
        Me.CHB_modulo_da_stampare_00.TabIndex = 16
        Me.CHB_modulo_da_stampare_00.Text = "Modulo da stampare?"
        Me.CHB_modulo_da_stampare_00.UseVisualStyleBackColor = True
        '
        'FLP_param_stampa
        '
        Me.FLP_param_stampa.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FLP_param_stampa.Controls.Add(Me.CHB_prn_arch_ott)
        Me.FLP_param_stampa.Controls.Add(Me.PNL_documenti_cont)
        Me.FLP_param_stampa.Controls.Add(Me.CHB_clean_FLD_elab)
        Me.FLP_param_stampa.Controls.Add(Me.CHB_eseg_poststampa)
        Me.FLP_param_stampa.Controls.Add(Me.CHB_force_paper)
        Me.FLP_param_stampa.Controls.Add(Me.CHB_fast_print)
        Me.FLP_param_stampa.Controls.Add(Me.BTN_stampa)
        Me.FLP_param_stampa.Location = New System.Drawing.Point(6, 175)
        Me.FLP_param_stampa.Name = "FLP_param_stampa"
        Me.FLP_param_stampa.Size = New System.Drawing.Size(321, 212)
        Me.FLP_param_stampa.TabIndex = 17
        '
        'CHB_prn_arch_ott
        '
        Me.CHB_prn_arch_ott.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CHB_prn_arch_ott.Location = New System.Drawing.Point(3, 3)
        Me.CHB_prn_arch_ott.Name = "CHB_prn_arch_ott"
        Me.CHB_prn_arch_ott.Size = New System.Drawing.Size(306, 21)
        Me.CHB_prn_arch_ott.TabIndex = 13
        Me.CHB_prn_arch_ott.Text = "Stampa modulo per archiviazione ottica"
        Me.CHB_prn_arch_ott.UseVisualStyleBackColor = True
        '
        'PNL_documenti_cont
        '
        Me.PNL_documenti_cont.Controls.Add(Me.Label3)
        Me.PNL_documenti_cont.Controls.Add(Me.NUD_documenti_gruppo)
        Me.PNL_documenti_cont.Location = New System.Drawing.Point(3, 30)
        Me.PNL_documenti_cont.Name = "PNL_documenti_cont"
        Me.PNL_documenti_cont.Size = New System.Drawing.Size(314, 30)
        Me.PNL_documenti_cont.TabIndex = 17
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(225, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Documenti da stampare contemporaneamente"
        '
        'NUD_documenti_gruppo
        '
        Me.NUD_documenti_gruppo.Location = New System.Drawing.Point(244, 5)
        Me.NUD_documenti_gruppo.Maximum = New Decimal(New Integer() {100000000, 0, 0, 0})
        Me.NUD_documenti_gruppo.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.NUD_documenti_gruppo.Name = "NUD_documenti_gruppo"
        Me.NUD_documenti_gruppo.Size = New System.Drawing.Size(67, 20)
        Me.NUD_documenti_gruppo.TabIndex = 7
        Me.NUD_documenti_gruppo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NUD_documenti_gruppo.Value = New Decimal(New Integer() {100000000, 0, 0, 0})
        '
        'CHB_clean_FLD_elab
        '
        Me.CHB_clean_FLD_elab.Checked = True
        Me.CHB_clean_FLD_elab.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CHB_clean_FLD_elab.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.CHB_clean_FLD_elab.Location = New System.Drawing.Point(3, 66)
        Me.CHB_clean_FLD_elab.Name = "CHB_clean_FLD_elab"
        Me.CHB_clean_FLD_elab.Size = New System.Drawing.Size(303, 18)
        Me.CHB_clean_FLD_elab.TabIndex = 16
        Me.CHB_clean_FLD_elab.Text = "Pulisci folder Elaborati"
        Me.CHB_clean_FLD_elab.UseVisualStyleBackColor = True
        '
        'CHB_eseg_poststampa
        '
        Me.CHB_eseg_poststampa.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.CHB_eseg_poststampa.Location = New System.Drawing.Point(3, 90)
        Me.CHB_eseg_poststampa.Name = "CHB_eseg_poststampa"
        Me.CHB_eseg_poststampa.Size = New System.Drawing.Size(303, 18)
        Me.CHB_eseg_poststampa.TabIndex = 14
        Me.CHB_eseg_poststampa.Text = "Esegui operazioni post stampa"
        Me.CHB_eseg_poststampa.UseVisualStyleBackColor = True
        '
        'CHB_force_paper
        '
        Me.CHB_force_paper.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.CHB_force_paper.Location = New System.Drawing.Point(3, 114)
        Me.CHB_force_paper.Name = "CHB_force_paper"
        Me.CHB_force_paper.Size = New System.Drawing.Size(303, 18)
        Me.CHB_force_paper.TabIndex = 15
        Me.CHB_force_paper.Text = "Forza stampa cartacea"
        Me.CHB_force_paper.UseVisualStyleBackColor = True
        '
        'CHB_fast_print
        '
        Me.CHB_fast_print.Checked = True
        Me.CHB_fast_print.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CHB_fast_print.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.CHB_fast_print.Location = New System.Drawing.Point(3, 138)
        Me.CHB_fast_print.Name = "CHB_fast_print"
        Me.CHB_fast_print.Size = New System.Drawing.Size(303, 18)
        Me.CHB_fast_print.TabIndex = 18
        Me.CHB_fast_print.Text = "Abilita/disabilita fast print"
        Me.CHB_fast_print.UseVisualStyleBackColor = True
        '
        'BTN_stampa
        '
        Me.BTN_stampa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_stampa.Location = New System.Drawing.Point(3, 162)
        Me.BTN_stampa.Name = "BTN_stampa"
        Me.BTN_stampa.Size = New System.Drawing.Size(314, 39)
        Me.BTN_stampa.TabIndex = 19
        Me.BTN_stampa.Text = "STAMPA"
        Me.BTN_stampa.UseVisualStyleBackColor = True
        '
        'CHB_fusione_moduli
        '
        Me.CHB_fusione_moduli.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CHB_fusione_moduli.Location = New System.Drawing.Point(6, 151)
        Me.CHB_fusione_moduli.Name = "CHB_fusione_moduli"
        Me.CHB_fusione_moduli.Size = New System.Drawing.Size(306, 18)
        Me.CHB_fusione_moduli.TabIndex = 21
        Me.CHB_fusione_moduli.Text = "Unisci documenti in uno solo"
        Me.CHB_fusione_moduli.UseVisualStyleBackColor = True
        '
        'TAP_export
        '
        Me.TAP_export.Controls.Add(Me.CHB_onlyselected)
        Me.TAP_export.Controls.Add(Me.TXB_nomefile_export)
        Me.TAP_export.Controls.Add(Me.Label26)
        Me.TAP_export.Controls.Add(Me.BTN_esporta)
        Me.TAP_export.Location = New System.Drawing.Point(4, 25)
        Me.TAP_export.Name = "TAP_export"
        Me.TAP_export.Padding = New System.Windows.Forms.Padding(3)
        Me.TAP_export.Size = New System.Drawing.Size(1458, 594)
        Me.TAP_export.TabIndex = 2
        Me.TAP_export.Text = "Esporta dati"
        Me.TAP_export.UseVisualStyleBackColor = True
        '
        'CHB_onlyselected
        '
        Me.CHB_onlyselected.AutoSize = True
        Me.CHB_onlyselected.Location = New System.Drawing.Point(9, 42)
        Me.CHB_onlyselected.Name = "CHB_onlyselected"
        Me.CHB_onlyselected.Size = New System.Drawing.Size(174, 17)
        Me.CHB_onlyselected.TabIndex = 12
        Me.CHB_onlyselected.Text = "Esporta solo i record selezionati"
        Me.CHB_onlyselected.UseVisualStyleBackColor = True
        '
        'TXB_nomefile_export
        '
        Me.TXB_nomefile_export.Location = New System.Drawing.Point(183, 11)
        Me.TXB_nomefile_export.Name = "TXB_nomefile_export"
        Me.TXB_nomefile_export.Size = New System.Drawing.Size(794, 20)
        Me.TXB_nomefile_export.TabIndex = 11
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(6, 14)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(171, 13)
        Me.Label26.TabIndex = 10
        Me.Label26.Text = "Nome del file su cui esportare i dati"
        '
        'BTN_esporta
        '
        Me.BTN_esporta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_esporta.Location = New System.Drawing.Point(289, 128)
        Me.BTN_esporta.Name = "BTN_esporta"
        Me.BTN_esporta.Size = New System.Drawing.Size(263, 39)
        Me.BTN_esporta.TabIndex = 9
        Me.BTN_esporta.Text = "ESPORTA"
        Me.BTN_esporta.UseVisualStyleBackColor = True
        '
        'MES_principale
        '
        Me.MES_principale.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSM_sessione_stampa, Me.TSM_RecapitoViaMail, Me.TSM_gestione_modulo, Me.TSM_gestione_giri, Me.TSM_Selezione, Me.TSM_ricerca, Me.TSM_visualizza, Me.Utilit�ToolStripMenuItem})
        Me.MES_principale.Location = New System.Drawing.Point(0, 0)
        Me.MES_principale.Name = "MES_principale"
        Me.MES_principale.Size = New System.Drawing.Size(1484, 24)
        Me.MES_principale.TabIndex = 1
        Me.MES_principale.Text = "MenuStrip1"
        '
        'TSM_sessione_stampa
        '
        Me.TSM_sessione_stampa.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSM_nuovo, Me.TSM_apri, Me.TSM_sessioni_recenti, Me.ToolStripMenuItem1, Me.TSM_chiudi, Me.ToolStripMenuItem3, Me.TSM_esci})
        Me.TSM_sessione_stampa.Name = "TSM_sessione_stampa"
        Me.TSM_sessione_stampa.Size = New System.Drawing.Size(107, 20)
        Me.TSM_sessione_stampa.Tag = "*"
        Me.TSM_sessione_stampa.Text = "Sessione Stampa"
        '
        'TSM_nuovo
        '
        Me.TSM_nuovo.Name = "TSM_nuovo"
        Me.TSM_nuovo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.TSM_nuovo.Size = New System.Drawing.Size(161, 22)
        Me.TSM_nuovo.Tag = ""
        Me.TSM_nuovo.Text = "Nuova"
        '
        'TSM_apri
        '
        Me.TSM_apri.Name = "TSM_apri"
        Me.TSM_apri.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.TSM_apri.Size = New System.Drawing.Size(161, 22)
        Me.TSM_apri.Tag = "*"
        Me.TSM_apri.Text = "Apri"
        '
        'TSM_sessioni_recenti
        '
        Me.TSM_sessioni_recenti.Name = "TSM_sessioni_recenti"
        Me.TSM_sessioni_recenti.Size = New System.Drawing.Size(161, 22)
        Me.TSM_sessioni_recenti.Text = "Sessioni recenti"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(158, 6)
        Me.ToolStripMenuItem1.Tag = "*"
        '
        'TSM_chiudi
        '
        Me.TSM_chiudi.Name = "TSM_chiudi"
        Me.TSM_chiudi.Size = New System.Drawing.Size(161, 22)
        Me.TSM_chiudi.Tag = "*"
        Me.TSM_chiudi.Text = "Chiudi sessione"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(158, 6)
        Me.ToolStripMenuItem3.Tag = "*"
        '
        'TSM_esci
        '
        Me.TSM_esci.Name = "TSM_esci"
        Me.TSM_esci.Size = New System.Drawing.Size(161, 22)
        Me.TSM_esci.Tag = "*"
        Me.TSM_esci.Text = "Esci"
        '
        'TSM_RecapitoViaMail
        '
        Me.TSM_RecapitoViaMail.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AggiungiAllegatiDaInviareToolStripMenuItem})
        Me.TSM_RecapitoViaMail.Name = "TSM_RecapitoViaMail"
        Me.TSM_RecapitoViaMail.Size = New System.Drawing.Size(109, 20)
        Me.TSM_RecapitoViaMail.Text = "Recapito via mail"
        '
        'AggiungiAllegatiDaInviareToolStripMenuItem
        '
        Me.AggiungiAllegatiDaInviareToolStripMenuItem.Name = "AggiungiAllegatiDaInviareToolStripMenuItem"
        Me.AggiungiAllegatiDaInviareToolStripMenuItem.Size = New System.Drawing.Size(218, 22)
        Me.AggiungiAllegatiDaInviareToolStripMenuItem.Text = "Aggiungi allegati da inviare"
        '
        'TSM_gestione_modulo
        '
        Me.TSM_gestione_modulo.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImpostaFattoreCorrezioneToolStripMenuItem, Me.ToolStripMenuItem8, Me.TSM_pulisci_modulo})
        Me.TSM_gestione_modulo.Enabled = False
        Me.TSM_gestione_modulo.Name = "TSM_gestione_modulo"
        Me.TSM_gestione_modulo.Size = New System.Drawing.Size(110, 20)
        Me.TSM_gestione_modulo.Text = "Gestione Modulo"
        '
        'ImpostaFattoreCorrezioneToolStripMenuItem
        '
        Me.ImpostaFattoreCorrezioneToolStripMenuItem.Name = "ImpostaFattoreCorrezioneToolStripMenuItem"
        Me.ImpostaFattoreCorrezioneToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.ImpostaFattoreCorrezioneToolStripMenuItem.Text = "Imposta fattore correzione"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(210, 6)
        Me.ToolStripMenuItem8.Visible = False
        '
        'TSM_pulisci_modulo
        '
        Me.TSM_pulisci_modulo.Name = "TSM_pulisci_modulo"
        Me.TSM_pulisci_modulo.Size = New System.Drawing.Size(213, 22)
        Me.TSM_pulisci_modulo.Tag = "*"
        Me.TSM_pulisci_modulo.Text = "Pulisci modulo"
        Me.TSM_pulisci_modulo.Visible = False
        '
        'TSM_gestione_giri
        '
        Me.TSM_gestione_giri.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RaggruppamentoGiriConsegnaToolStripMenuItem, Me.TSM_gest_giro, Me.ToolStripMenuItem7, Me.TSM_ass_cit_top, Me.TSM_gest_citta, Me.TSM_gest_topo, Me.ToolStripSeparator3, Me.TSM_insert_bulk})
        Me.TSM_gestione_giri.Name = "TSM_gestione_giri"
        Me.TSM_gestione_giri.Size = New System.Drawing.Size(142, 20)
        Me.TSM_gestione_giri.Text = "Gestione Giri Consegna"
        '
        'RaggruppamentoGiriConsegnaToolStripMenuItem
        '
        Me.RaggruppamentoGiriConsegnaToolStripMenuItem.Name = "RaggruppamentoGiriConsegnaToolStripMenuItem"
        Me.RaggruppamentoGiriConsegnaToolStripMenuItem.Size = New System.Drawing.Size(241, 22)
        Me.RaggruppamentoGiriConsegnaToolStripMenuItem.Text = "Raggruppamento giri consegna"
        '
        'TSM_gest_giro
        '
        Me.TSM_gest_giro.Name = "TSM_gest_giro"
        Me.TSM_gest_giro.Size = New System.Drawing.Size(241, 22)
        Me.TSM_gest_giro.Text = "Definisci giro consegna"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(238, 6)
        '
        'TSM_ass_cit_top
        '
        Me.TSM_ass_cit_top.Name = "TSM_ass_cit_top"
        Me.TSM_ass_cit_top.Size = New System.Drawing.Size(241, 22)
        Me.TSM_ass_cit_top.Text = "Associa Citt�-Toponimo"
        '
        'TSM_gest_citta
        '
        Me.TSM_gest_citta.Name = "TSM_gest_citta"
        Me.TSM_gest_citta.Size = New System.Drawing.Size(241, 22)
        Me.TSM_gest_citta.Text = "Gestione citt�"
        '
        'TSM_gest_topo
        '
        Me.TSM_gest_topo.Name = "TSM_gest_topo"
        Me.TSM_gest_topo.Size = New System.Drawing.Size(241, 22)
        Me.TSM_gest_topo.Text = "Gestione toponimi"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(238, 6)
        '
        'TSM_insert_bulk
        '
        Me.TSM_insert_bulk.Name = "TSM_insert_bulk"
        Me.TSM_insert_bulk.Size = New System.Drawing.Size(241, 22)
        Me.TSM_insert_bulk.Text = "Inserimento Bulk"
        '
        'TSM_Selezione
        '
        Me.TSM_Selezione.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSM_sel_tutti, Me.TSM_sel_nessuno, Me.TSM_con_err_gr, Me.TSM_send_by_mail, Me.ToolStripMenuItem2, Me.TSM_sel_range, Me.TSM_sel_norange, Me.ToolStripSeparator1, Me.TSM_sel_inverti})
        Me.TSM_Selezione.Name = "TSM_Selezione"
        Me.TSM_Selezione.Size = New System.Drawing.Size(68, 20)
        Me.TSM_Selezione.Tag = "*"
        Me.TSM_Selezione.Text = "Selezione"
        '
        'TSM_sel_tutti
        '
        Me.TSM_sel_tutti.Name = "TSM_sel_tutti"
        Me.TSM_sel_tutti.Size = New System.Drawing.Size(188, 22)
        Me.TSM_sel_tutti.Tag = "*"
        Me.TSM_sel_tutti.Text = "Tutti"
        '
        'TSM_sel_nessuno
        '
        Me.TSM_sel_nessuno.Name = "TSM_sel_nessuno"
        Me.TSM_sel_nessuno.Size = New System.Drawing.Size(188, 22)
        Me.TSM_sel_nessuno.Tag = "*"
        Me.TSM_sel_nessuno.Text = "Nessuno"
        '
        'TSM_con_err_gr
        '
        Me.TSM_con_err_gr.Name = "TSM_con_err_gr"
        Me.TSM_con_err_gr.Size = New System.Drawing.Size(188, 22)
        Me.TSM_con_err_gr.Text = "Con errore grave"
        '
        'TSM_send_by_mail
        '
        Me.TSM_send_by_mail.Name = "TSM_send_by_mail"
        Me.TSM_send_by_mail.Size = New System.Drawing.Size(188, 22)
        Me.TSM_send_by_mail.Text = "Da inviare per email"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(185, 6)
        Me.ToolStripMenuItem2.Tag = "*"
        '
        'TSM_sel_range
        '
        Me.TSM_sel_range.Name = "TSM_sel_range"
        Me.TSM_sel_range.Size = New System.Drawing.Size(188, 22)
        Me.TSM_sel_range.Tag = "*"
        Me.TSM_sel_range.Text = "Seleziona intervallo"
        '
        'TSM_sel_norange
        '
        Me.TSM_sel_norange.Name = "TSM_sel_norange"
        Me.TSM_sel_norange.Size = New System.Drawing.Size(188, 22)
        Me.TSM_sel_norange.Tag = "*"
        Me.TSM_sel_norange.Text = "Deseleziona intervallo"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(185, 6)
        Me.ToolStripSeparator1.Tag = "*"
        '
        'TSM_sel_inverti
        '
        Me.TSM_sel_inverti.Name = "TSM_sel_inverti"
        Me.TSM_sel_inverti.Size = New System.Drawing.Size(188, 22)
        Me.TSM_sel_inverti.Tag = "*"
        Me.TSM_sel_inverti.Text = "Inverti selezione"
        '
        'TSM_ricerca
        '
        Me.TSM_ricerca.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSM_ric_progressivo})
        Me.TSM_ricerca.Name = "TSM_ricerca"
        Me.TSM_ricerca.Size = New System.Drawing.Size(57, 20)
        Me.TSM_ricerca.Tag = "*"
        Me.TSM_ricerca.Text = "Ricerca"
        '
        'TSM_ric_progressivo
        '
        Me.TSM_ric_progressivo.Name = "TSM_ric_progressivo"
        Me.TSM_ric_progressivo.Size = New System.Drawing.Size(155, 22)
        Me.TSM_ric_progressivo.Tag = ""
        Me.TSM_ric_progressivo.Text = "Per progressivo"
        '
        'TSM_visualizza
        '
        Me.TSM_visualizza.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem5, Me.TSM_vis_tutti, Me.TSM_vis_noerror, Me.TSM_vis_error, Me.ToolStripMenuItem4})
        Me.TSM_visualizza.Name = "TSM_visualizza"
        Me.TSM_visualizza.Size = New System.Drawing.Size(69, 20)
        Me.TSM_visualizza.Tag = "*"
        Me.TSM_visualizza.Text = "Visualizza"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(171, 6)
        Me.ToolStripMenuItem5.Tag = "*"
        Me.ToolStripMenuItem5.Visible = False
        '
        'TSM_vis_tutti
        '
        Me.TSM_vis_tutti.Name = "TSM_vis_tutti"
        Me.TSM_vis_tutti.Size = New System.Drawing.Size(174, 22)
        Me.TSM_vis_tutti.Tag = "*"
        Me.TSM_vis_tutti.Text = "Tutti i record"
        '
        'TSM_vis_noerror
        '
        Me.TSM_vis_noerror.Name = "TSM_vis_noerror"
        Me.TSM_vis_noerror.Size = New System.Drawing.Size(174, 22)
        Me.TSM_vis_noerror.Tag = "*"
        Me.TSM_vis_noerror.Text = "Record senza errori"
        '
        'TSM_vis_error
        '
        Me.TSM_vis_error.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSM_vis_err_load_lievi, Me.TSM_vis_err_load_gravi, Me.TSM_vis_err_prn})
        Me.TSM_vis_error.Name = "TSM_vis_error"
        Me.TSM_vis_error.Size = New System.Drawing.Size(174, 22)
        Me.TSM_vis_error.Tag = "*"
        Me.TSM_vis_error.Text = "Record con errori"
        '
        'TSM_vis_err_load_lievi
        '
        Me.TSM_vis_err_load_lievi.Name = "TSM_vis_err_load_lievi"
        Me.TSM_vis_err_load_lievi.Size = New System.Drawing.Size(198, 22)
        Me.TSM_vis_err_load_lievi.Text = "Caricamento dati: Lievi "
        '
        'TSM_vis_err_load_gravi
        '
        Me.TSM_vis_err_load_gravi.Name = "TSM_vis_err_load_gravi"
        Me.TSM_vis_err_load_gravi.Size = New System.Drawing.Size(198, 22)
        Me.TSM_vis_err_load_gravi.Text = "Caricamento dati: Gravi"
        '
        'TSM_vis_err_prn
        '
        Me.TSM_vis_err_prn.Name = "TSM_vis_err_prn"
        Me.TSM_vis_err_prn.Size = New System.Drawing.Size(198, 22)
        Me.TSM_vis_err_prn.Text = "Fase di stampa"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(171, 6)
        '
        'Utilit�ToolStripMenuItem
        '
        Me.Utilit�ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSM_manage_azioni_ps, Me.TSM_configurazioni, Me.ToolStripMenuItem6, Me.TSM_reset_prn_stat, Me.TSM_EsportaFileDiLog, Me.TSM_puliscicartellawork})
        Me.Utilit�ToolStripMenuItem.Name = "Utilit�ToolStripMenuItem"
        Me.Utilit�ToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
        Me.Utilit�ToolStripMenuItem.Tag = "*"
        Me.Utilit�ToolStripMenuItem.Text = "Utilit�"
        '
        'TSM_manage_azioni_ps
        '
        Me.TSM_manage_azioni_ps.Enabled = False
        Me.TSM_manage_azioni_ps.Name = "TSM_manage_azioni_ps"
        Me.TSM_manage_azioni_ps.Size = New System.Drawing.Size(225, 22)
        Me.TSM_manage_azioni_ps.Text = "Gestione Azioni Post Stampa"
        '
        'TSM_configurazioni
        '
        Me.TSM_configurazioni.Name = "TSM_configurazioni"
        Me.TSM_configurazioni.Size = New System.Drawing.Size(225, 22)
        Me.TSM_configurazioni.Tag = "*"
        Me.TSM_configurazioni.Text = "Configurazioni"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(222, 6)
        '
        'TSM_reset_prn_stat
        '
        Me.TSM_reset_prn_stat.Name = "TSM_reset_prn_stat"
        Me.TSM_reset_prn_stat.Size = New System.Drawing.Size(225, 22)
        Me.TSM_reset_prn_stat.Text = "Azzera statistiche di stampa"
        '
        'TSM_EsportaFileDiLog
        '
        Me.TSM_EsportaFileDiLog.Name = "TSM_EsportaFileDiLog"
        Me.TSM_EsportaFileDiLog.Size = New System.Drawing.Size(225, 22)
        Me.TSM_EsportaFileDiLog.Text = "Esporta File di Log"
        '
        'TSM_puliscicartellawork
        '
        Me.TSM_puliscicartellawork.Name = "TSM_puliscicartellawork"
        Me.TSM_puliscicartellawork.Size = New System.Drawing.Size(225, 22)
        Me.TSM_puliscicartellawork.Tag = "*"
        Me.TSM_puliscicartellawork.Text = "Pulisci cartella work"
        Me.TSM_puliscicartellawork.Visible = False
        '
        'STS_bottom
        '
        Me.STS_bottom.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSSL_versione_software, Me.TSSL_stato_prn_pap, Me.TSSL_stato_prn_ott, Me.TSSL_versione_pdfcreator, Me.TSSL_prnwrk, Me.TSSL_memorysize})
        Me.STS_bottom.Location = New System.Drawing.Point(0, 837)
        Me.STS_bottom.Name = "STS_bottom"
        Me.STS_bottom.Size = New System.Drawing.Size(1484, 24)
        Me.STS_bottom.TabIndex = 19
        '
        'TSSL_versione_software
        '
        Me.TSSL_versione_software.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.TSSL_versione_software.BorderStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.TSSL_versione_software.Name = "TSSL_versione_software"
        Me.TSSL_versione_software.Size = New System.Drawing.Size(56, 19)
        Me.TSSL_versione_software.Text = "Versione"
        '
        'TSSL_stato_prn_pap
        '
        Me.TSSL_stato_prn_pap.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.TSSL_stato_prn_pap.BorderStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.TSSL_stato_prn_pap.Name = "TSSL_stato_prn_pap"
        Me.TSSL_stato_prn_pap.Size = New System.Drawing.Size(98, 19)
        Me.TSSL_stato_prn_pap.Text = "Stampa cartacea"
        '
        'TSSL_stato_prn_ott
        '
        Me.TSSL_stato_prn_ott.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.TSSL_stato_prn_ott.BorderStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.TSSL_stato_prn_ott.Name = "TSSL_stato_prn_ott"
        Me.TSSL_stato_prn_ott.Size = New System.Drawing.Size(84, 19)
        Me.TSSL_stato_prn_ott.Text = "Stampa ottica"
        '
        'TSSL_versione_pdfcreator
        '
        Me.TSSL_versione_pdfcreator.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.TSSL_versione_pdfcreator.BorderStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.TSSL_versione_pdfcreator.Name = "TSSL_versione_pdfcreator"
        Me.TSSL_versione_pdfcreator.Size = New System.Drawing.Size(119, 19)
        Me.TSSL_versione_pdfcreator.Text = "Versione PDFCreator"
        '
        'TSSL_prnwrk
        '
        Me.TSSL_prnwrk.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.TSSL_prnwrk.BorderStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.TSSL_prnwrk.Name = "TSSL_prnwrk"
        Me.TSSL_prnwrk.Size = New System.Drawing.Size(163, 19)
        Me.TSSL_prnwrk.Text = "Cartella temporanea stampe:"
        '
        'TSSL_memorysize
        '
        Me.TSSL_memorysize.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.TSSL_memorysize.BorderStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.TSSL_memorysize.Name = "TSSL_memorysize"
        Me.TSSL_memorysize.Size = New System.Drawing.Size(208, 19)
        Me.TSSL_memorysize.Text = "Dimensione memoria (Attuale/Picco)"
        '
        'TOS_menu
        '
        Me.TOS_menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSB_refresh, Me.ToolStripSeparator4, Me.TSB_open_arcott_fld, Me.TSB_open_documenti_unificati_fld, Me.TSB_open_elaborati_fld, Me.ToolStripSeparator2, Me.TSB_export_fld})
        Me.TOS_menu.Location = New System.Drawing.Point(0, 24)
        Me.TOS_menu.Name = "TOS_menu"
        Me.TOS_menu.Size = New System.Drawing.Size(1484, 25)
        Me.TOS_menu.TabIndex = 20
        Me.TOS_menu.Text = "ToolStrip1"
        '
        'TSB_refresh
        '
        Me.TSB_refresh.Image = Global.Invoicer.My.Resources.Resources.PNG_refresh
        Me.TSB_refresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_refresh.Name = "TSB_refresh"
        Me.TSB_refresh.Size = New System.Drawing.Size(66, 22)
        Me.TSB_refresh.Text = "Refresh"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'TSB_open_arcott_fld
        '
        Me.TSB_open_arcott_fld.Image = Global.Invoicer.My.Resources.Resources.PNG_folder
        Me.TSB_open_arcott_fld.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_open_arcott_fld.Name = "TSB_open_arcott_fld"
        Me.TSB_open_arcott_fld.Size = New System.Drawing.Size(172, 22)
        Me.TSB_open_arcott_fld.Text = "Cartella archiviazione ottica"
        Me.TSB_open_arcott_fld.ToolTipText = "Apri cartella di archiviazione ottica"
        '
        'TSB_open_documenti_unificati_fld
        '
        Me.TSB_open_documenti_unificati_fld.Image = Global.Invoicer.My.Resources.Resources.PNG_folder
        Me.TSB_open_documenti_unificati_fld.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_open_documenti_unificati_fld.Name = "TSB_open_documenti_unificati_fld"
        Me.TSB_open_documenti_unificati_fld.Size = New System.Drawing.Size(174, 22)
        Me.TSB_open_documenti_unificati_fld.Text = "Cartella documenti unificati"
        Me.TSB_open_documenti_unificati_fld.ToolTipText = "Apri cartella di archiviazione ottica"
        '
        'TSB_open_elaborati_fld
        '
        Me.TSB_open_elaborati_fld.Image = Global.Invoicer.My.Resources.Resources.PNG_folder
        Me.TSB_open_elaborati_fld.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_open_elaborati_fld.Name = "TSB_open_elaborati_fld"
        Me.TSB_open_elaborati_fld.Size = New System.Drawing.Size(182, 22)
        Me.TSB_open_elaborati_fld.Text = "Cartella prestampati elaborati"
        Me.TSB_open_elaborati_fld.ToolTipText = "Apri cartella di archiviazione ottica"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'TSB_export_fld
        '
        Me.TSB_export_fld.Image = Global.Invoicer.My.Resources.Resources.PNG_folder
        Me.TSB_export_fld.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TSB_export_fld.Name = "TSB_export_fld"
        Me.TSB_export_fld.Size = New System.Drawing.Size(134, 22)
        Me.TSB_export_fld.Text = "Cartella esportazioni"
        '
        'IML_standard
        '
        Me.IML_standard.ImageStream = CType(resources.GetObject("IML_standard.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.IML_standard.TransparentColor = System.Drawing.Color.Transparent
        Me.IML_standard.Images.SetKeyName(0, "PNG_add.jpg")
        Me.IML_standard.Images.SetKeyName(1, "PNG_delete.png")
        Me.IML_standard.Images.SetKeyName(2, "refresh_icon.png")
        '
        'TMR_sched_op
        '
        Me.TMR_sched_op.Interval = 1000
        '
        'GRB_anagraficasessione
        '
        Me.GRB_anagraficasessione.Controls.Add(Me.LBL_sessione_nome)
        Me.GRB_anagraficasessione.Controls.Add(Me.Label4)
        Me.GRB_anagraficasessione.Controls.Add(Me.LBL_plugin_versione)
        Me.GRB_anagraficasessione.Controls.Add(Me.LBL_sessione_cartella)
        Me.GRB_anagraficasessione.Controls.Add(Me.Label6)
        Me.GRB_anagraficasessione.Controls.Add(Me.Label9)
        Me.GRB_anagraficasessione.Controls.Add(Me.Label11)
        Me.GRB_anagraficasessione.Controls.Add(Me.LBL_plugin_filename)
        Me.GRB_anagraficasessione.Controls.Add(Me.Label10)
        Me.GRB_anagraficasessione.Controls.Add(Me.LBL_plugin_name)
        Me.GRB_anagraficasessione.Controls.Add(Me.LBL_sessione_Descrizione)
        Me.GRB_anagraficasessione.Controls.Add(Me.Label8)
        Me.GRB_anagraficasessione.Controls.Add(Me.Label7)
        Me.GRB_anagraficasessione.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GRB_anagraficasessione.Location = New System.Drawing.Point(10, 53)
        Me.GRB_anagraficasessione.Name = "GRB_anagraficasessione"
        Me.GRB_anagraficasessione.Size = New System.Drawing.Size(796, 152)
        Me.GRB_anagraficasessione.TabIndex = 26
        Me.GRB_anagraficasessione.TabStop = False
        Me.GRB_anagraficasessione.Text = "Dati anagrafica sessione"
        '
        'LBL_sessione_nome
        '
        Me.LBL_sessione_nome.Location = New System.Drawing.Point(89, 16)
        Me.LBL_sessione_nome.Name = "LBL_sessione_nome"
        Me.LBL_sessione_nome.Size = New System.Drawing.Size(346, 13)
        Me.LBL_sessione_nome.TabIndex = 14
        Me.LBL_sessione_nome.Text = "N/A"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(1, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(79, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Nome.........:"
        '
        'LBL_plugin_versione
        '
        Me.LBL_plugin_versione.AutoSize = True
        Me.LBL_plugin_versione.Location = New System.Drawing.Point(531, 64)
        Me.LBL_plugin_versione.Name = "LBL_plugin_versione"
        Me.LBL_plugin_versione.Size = New System.Drawing.Size(27, 13)
        Me.LBL_plugin_versione.TabIndex = 2
        Me.LBL_plugin_versione.Text = "N/A"
        '
        'LBL_sessione_cartella
        '
        Me.LBL_sessione_cartella.Location = New System.Drawing.Point(89, 32)
        Me.LBL_sessione_cartella.Name = "LBL_sessione_cartella"
        Me.LBL_sessione_cartella.Size = New System.Drawing.Size(346, 31)
        Me.LBL_sessione_cartella.TabIndex = 12
        Me.LBL_sessione_cartella.Text = "N/A"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(454, 32)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(71, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Nome.......:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(454, 64)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(72, 13)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "Versione...:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(1, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(82, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Cartella.......:"
        '
        'LBL_plugin_filename
        '
        Me.LBL_plugin_filename.Location = New System.Drawing.Point(531, 80)
        Me.LBL_plugin_filename.Name = "LBL_plugin_filename"
        Me.LBL_plugin_filename.Size = New System.Drawing.Size(259, 32)
        Me.LBL_plugin_filename.TabIndex = 9
        Me.LBL_plugin_filename.Text = "N/A"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(1, 80)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(81, 13)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Descrizione.:"
        '
        'LBL_plugin_name
        '
        Me.LBL_plugin_name.Location = New System.Drawing.Point(531, 32)
        Me.LBL_plugin_name.Name = "LBL_plugin_name"
        Me.LBL_plugin_name.Size = New System.Drawing.Size(259, 31)
        Me.LBL_plugin_name.TabIndex = 3
        Me.LBL_plugin_name.Text = "N/A"
        '
        'LBL_sessione_Descrizione
        '
        Me.LBL_sessione_Descrizione.Location = New System.Drawing.Point(89, 80)
        Me.LBL_sessione_Descrizione.Name = "LBL_sessione_Descrizione"
        Me.LBL_sessione_Descrizione.Size = New System.Drawing.Size(346, 53)
        Me.LBL_sessione_Descrizione.TabIndex = 7
        Me.LBL_sessione_Descrizione.Text = "N/A"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(454, 80)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Nome File.:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(454, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Plugin"
        '
        'GRP_statistiche
        '
        Me.GRP_statistiche.Controls.Add(Me.LBL_dati_var)
        Me.GRP_statistiche.Controls.Add(Me.Label14)
        Me.GRP_statistiche.Controls.Add(Me.Label15)
        Me.GRP_statistiche.Controls.Add(Me.Label18)
        Me.GRP_statistiche.Controls.Add(Me.Label20)
        Me.GRP_statistiche.Controls.Add(Me.LBL_err_car_liev)
        Me.GRP_statistiche.Controls.Add(Me.Label21)
        Me.GRP_statistiche.Controls.Add(Me.LBL_rec_load)
        Me.GRP_statistiche.Controls.Add(Me.LBL_err_prn)
        Me.GRP_statistiche.Controls.Add(Me.LBL_rec_sel)
        Me.GRP_statistiche.Controls.Add(Me.Label19)
        Me.GRP_statistiche.Controls.Add(Me.LBL_err_car_grav)
        Me.GRP_statistiche.Location = New System.Drawing.Point(812, 53)
        Me.GRP_statistiche.Name = "GRP_statistiche"
        Me.GRP_statistiche.Size = New System.Drawing.Size(407, 152)
        Me.GRP_statistiche.TabIndex = 25
        Me.GRP_statistiche.TabStop = False
        Me.GRP_statistiche.Text = "Statistiche sessione"
        '
        'LBL_dati_var
        '
        Me.LBL_dati_var.Location = New System.Drawing.Point(218, 96)
        Me.LBL_dati_var.Name = "LBL_dati_var"
        Me.LBL_dati_var.Size = New System.Drawing.Size(183, 13)
        Me.LBL_dati_var.TabIndex = 14
        Me.LBL_dati_var.Text = "N/A"
        Me.LBL_dati_var.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(6, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(99, 13)
        Me.Label14.TabIndex = 11
        Me.Label14.Text = "Record Caricati:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(6, 80)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(133, 13)
        Me.Label15.TabIndex = 10
        Me.Label15.Text = "Record errore stampa:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(6, 96)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(142, 13)
        Me.Label18.TabIndex = 18
        Me.Label18.Text = "Dati variabili controllati:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(6, 48)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(208, 13)
        Me.Label20.TabIndex = 4
        Me.Label20.Text = "Record errore gravi di caricamento:"
        '
        'LBL_err_car_liev
        '
        Me.LBL_err_car_liev.Location = New System.Drawing.Point(351, 64)
        Me.LBL_err_car_liev.Name = "LBL_err_car_liev"
        Me.LBL_err_car_liev.Size = New System.Drawing.Size(50, 13)
        Me.LBL_err_car_liev.TabIndex = 18
        Me.LBL_err_car_liev.Text = "N/A"
        Me.LBL_err_car_liev.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(6, 32)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(114, 13)
        Me.Label21.TabIndex = 6
        Me.Label21.Text = "Record Selezionati"
        '
        'LBL_rec_load
        '
        Me.LBL_rec_load.Location = New System.Drawing.Point(351, 16)
        Me.LBL_rec_load.Name = "LBL_rec_load"
        Me.LBL_rec_load.Size = New System.Drawing.Size(50, 13)
        Me.LBL_rec_load.TabIndex = 13
        Me.LBL_rec_load.Text = "N/A"
        Me.LBL_rec_load.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_err_prn
        '
        Me.LBL_err_prn.Location = New System.Drawing.Point(351, 80)
        Me.LBL_err_prn.Name = "LBL_err_prn"
        Me.LBL_err_prn.Size = New System.Drawing.Size(50, 13)
        Me.LBL_err_prn.TabIndex = 16
        Me.LBL_err_prn.Text = "N/A"
        Me.LBL_err_prn.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_rec_sel
        '
        Me.LBL_rec_sel.Location = New System.Drawing.Point(351, 32)
        Me.LBL_rec_sel.Name = "LBL_rec_sel"
        Me.LBL_rec_sel.Size = New System.Drawing.Size(50, 13)
        Me.LBL_rec_sel.TabIndex = 14
        Me.LBL_rec_sel.Text = "N/A"
        Me.LBL_rec_sel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(6, 64)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(203, 13)
        Me.Label19.TabIndex = 17
        Me.Label19.Text = "Record errore lievi di caricamento:"
        '
        'LBL_err_car_grav
        '
        Me.LBL_err_car_grav.Location = New System.Drawing.Point(351, 48)
        Me.LBL_err_car_grav.Name = "LBL_err_car_grav"
        Me.LBL_err_car_grav.Size = New System.Drawing.Size(50, 13)
        Me.LBL_err_car_grav.TabIndex = 15
        Me.LBL_err_car_grav.Text = "N/A"
        Me.LBL_err_car_grav.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TAC_statistiche
        '
        Me.TAC_statistiche.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TAC_statistiche.Controls.Add(Me.TabPage1)
        Me.TAC_statistiche.Controls.Add(Me.TabPage2)
        Me.TAC_statistiche.Location = New System.Drawing.Point(1225, 57)
        Me.TAC_statistiche.Name = "TAC_statistiche"
        Me.TAC_statistiche.SelectedIndex = 0
        Me.TAC_statistiche.Size = New System.Drawing.Size(253, 148)
        Me.TAC_statistiche.TabIndex = 24
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.DGV_stat_glob)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(245, 119)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Stat. Globali"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'DGV_stat_glob
        '
        Me.DGV_stat_glob.AllowUserToAddRows = False
        Me.DGV_stat_glob.AllowUserToDeleteRows = False
        Me.DGV_stat_glob.AllowUserToResizeColumns = False
        Me.DGV_stat_glob.AllowUserToResizeRows = False
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_stat_glob.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.DGV_stat_glob.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_stat_glob.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2})
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_stat_glob.DefaultCellStyle = DataGridViewCellStyle10
        Me.DGV_stat_glob.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_stat_glob.Location = New System.Drawing.Point(3, 3)
        Me.DGV_stat_glob.MultiSelect = False
        Me.DGV_stat_glob.Name = "DGV_stat_glob"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_stat_glob.RowHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.DGV_stat_glob.RowHeadersWidth = 20
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DGV_stat_glob.RowsDefaultCellStyle = DataGridViewCellStyle12
        Me.DGV_stat_glob.RowTemplate.Height = 16
        Me.DGV_stat_glob.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DGV_stat_glob.Size = New System.Drawing.Size(239, 113)
        Me.DGV_stat_glob.TabIndex = 23
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "STS_desc_num_pagine"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn1.HeaderText = "Moduli"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "STS_stampate"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridViewTextBoxColumn2.HeaderText = "Quantit�"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 50
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.DGV_stat_parz)
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(245, 119)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Stat. ultima stampa"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'DGV_stat_parz
        '
        Me.DGV_stat_parz.AllowUserToAddRows = False
        Me.DGV_stat_parz.AllowUserToDeleteRows = False
        Me.DGV_stat_parz.AllowUserToResizeColumns = False
        Me.DGV_stat_parz.AllowUserToResizeRows = False
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_stat_parz.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.DGV_stat_parz.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGV_stat_parz.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.STA_descrizione, Me.STS_stampate})
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_stat_parz.DefaultCellStyle = DataGridViewCellStyle16
        Me.DGV_stat_parz.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGV_stat_parz.Location = New System.Drawing.Point(3, 3)
        Me.DGV_stat_parz.MultiSelect = False
        Me.DGV_stat_parz.Name = "DGV_stat_parz"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_stat_parz.RowHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.DGV_stat_parz.RowHeadersWidth = 20
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DGV_stat_parz.RowsDefaultCellStyle = DataGridViewCellStyle18
        Me.DGV_stat_parz.RowTemplate.Height = 16
        Me.DGV_stat_parz.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DGV_stat_parz.Size = New System.Drawing.Size(239, 113)
        Me.DGV_stat_parz.TabIndex = 20
        '
        'STA_descrizione
        '
        Me.STA_descrizione.DataPropertyName = "STS_desc_num_pagine"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.STA_descrizione.DefaultCellStyle = DataGridViewCellStyle14
        Me.STA_descrizione.HeaderText = "Moduli"
        Me.STA_descrizione.Name = "STA_descrizione"
        Me.STA_descrizione.ReadOnly = True
        Me.STA_descrizione.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.STA_descrizione.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.STA_descrizione.Width = 150
        '
        'STS_stampate
        '
        Me.STS_stampate.DataPropertyName = "STS_stampate"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.STS_stampate.DefaultCellStyle = DataGridViewCellStyle15
        Me.STS_stampate.HeaderText = "Quantit�"
        Me.STS_stampate.Name = "STS_stampate"
        Me.STS_stampate.ReadOnly = True
        Me.STS_stampate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.STS_stampate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.STS_stampate.Width = 50
        '
        'FRM_Invoicer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1484, 861)
        Me.Controls.Add(Me.GRB_anagraficasessione)
        Me.Controls.Add(Me.GRP_statistiche)
        Me.Controls.Add(Me.TAC_statistiche)
        Me.Controls.Add(Me.TOS_menu)
        Me.Controls.Add(Me.STS_bottom)
        Me.Controls.Add(Me.MES_principale)
        Me.Controls.Add(Me.TAC_principale)
        Me.MainMenuStrip = Me.MES_principale
        Me.Name = "FRM_Invoicer"
        Me.Text = "Gestione stampe"
        Me.TAC_principale.ResumeLayout(False)
        Me.TAP_dati.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.DGV_dati, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMS_dati.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TAP_stampa.ResumeLayout(False)
        Me.TAP_stampa.PerformLayout()
        Me.TAC_modulistampa.ResumeLayout(False)
        Me.TAP_modulo_00.ResumeLayout(False)
        Me.TAP_modulo_00.PerformLayout()
        Me.FLP_moduli_00.ResumeLayout(False)
        Me.FLP_moduli_00.PerformLayout()
        Me.GRB_stampante_car_00.ResumeLayout(False)
        Me.GRB_stampante_car_00.PerformLayout()
        CType(Me.DGV_ass_cassetti_00, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRB_stampante_ele_00.ResumeLayout(False)
        Me.GRB_stampante_ele_00.PerformLayout()
        Me.GRB_generazione_00.ResumeLayout(False)
        Me.GRB_generazione_00.PerformLayout()
        Me.FLP_param_stampa.ResumeLayout(False)
        Me.PNL_documenti_cont.ResumeLayout(False)
        Me.PNL_documenti_cont.PerformLayout()
        CType(Me.NUD_documenti_gruppo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TAP_export.ResumeLayout(False)
        Me.TAP_export.PerformLayout()
        Me.MES_principale.ResumeLayout(False)
        Me.MES_principale.PerformLayout()
        Me.STS_bottom.ResumeLayout(False)
        Me.STS_bottom.PerformLayout()
        Me.TOS_menu.ResumeLayout(False)
        Me.TOS_menu.PerformLayout()
        Me.GRB_anagraficasessione.ResumeLayout(False)
        Me.GRB_anagraficasessione.PerformLayout()
        Me.GRP_statistiche.ResumeLayout(False)
        Me.GRP_statistiche.PerformLayout()
        Me.TAC_statistiche.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.DGV_stat_glob, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.DGV_stat_parz, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TAC_principale As System.Windows.Forms.TabControl
    Friend WithEvents TAP_dati As System.Windows.Forms.TabPage
    Friend WithEvents TAP_stampa As System.Windows.Forms.TabPage
    Friend WithEvents CHB_fronte_retro_00 As System.Windows.Forms.CheckBox
    Friend WithEvents NUD_documenti_gruppo As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CHB_insert_fincatura_00 As System.Windows.Forms.CheckBox
    Friend WithEvents MES_principale As System.Windows.Forms.MenuStrip
    Friend WithEvents TSM_sessione_stampa As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_nuovo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_apri As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSM_esci As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DGV_dati As System.Windows.Forms.DataGridView
    Friend WithEvents TSM_Selezione As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_sel_tutti As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSM_sel_nessuno As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_chiudi As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSM_sel_norange As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_visualizza As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSM_vis_tutti As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_vis_noerror As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_vis_error As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_vis_err_load_lievi As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_vis_err_prn As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Utilit�ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_sel_inverti As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_puliscicartellawork As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_sel_range As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CHB_prn_arch_ott As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSM_reset_prn_stat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_vis_err_load_gravi As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_ricerca As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_ric_progressivo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_configurazioni As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CMS_dati As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents CSM_seleziona As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CSM_deseleziona As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_EsportaFileDiLog As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_gestione_modulo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_pulisci_modulo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImpostaFattoreCorrezioneToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents STS_bottom As System.Windows.Forms.StatusStrip
    Friend WithEvents TSSL_versione_software As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TSM_sessioni_recenti As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TXB_cod_wr_car_00 As System.Windows.Forms.TextBox
    Friend WithEvents CHB_reset_cod_wr_car_00 As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripMenuItem9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ResetStatoErroreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TOS_menu As System.Windows.Forms.ToolStrip
    Friend WithEvents TSB_open_arcott_fld As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSM_RecapitoViaMail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_con_err_gr As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AggiungiAllegatiDaInviareToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CHB_eseg_poststampa As System.Windows.Forms.CheckBox
    Friend WithEvents CHB_force_paper As System.Windows.Forms.CheckBox
    Friend WithEvents TSSL_stato_prn_pap As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TSSL_stato_prn_ott As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TSSL_versione_pdfcreator As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TSM_gestione_giri As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_gest_topo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_gest_citta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_gest_giro As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSM_ass_cit_top As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IML_standard As System.Windows.Forms.ImageList
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSM_insert_bulk As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RaggruppamentoGiriConsegnaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSM_send_by_mail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TAP_export As System.Windows.Forms.TabPage
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents BTN_esporta As System.Windows.Forms.Button
    Friend WithEvents TXB_nomefile_export As System.Windows.Forms.TextBox
    Friend WithEvents TSB_export_fld As System.Windows.Forms.ToolStripButton
    Friend WithEvents CHB_onlyselected As System.Windows.Forms.CheckBox
    Friend WithEvents TSM_manage_azioni_ps As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSB_refresh As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents TSSL_prnwrk As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TMR_sched_op As System.Windows.Forms.Timer
    Friend WithEvents TSSL_memorysize As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents CHB_clean_FLD_elab As System.Windows.Forms.CheckBox
    Friend WithEvents GRB_generazione_00 As System.Windows.Forms.GroupBox
    Friend WithEvents TXB_Coordinata_X_00 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TXB_Coordinata_Y_00 As System.Windows.Forms.TextBox
    Friend WithEvents TSB_open_elaborati_fld As System.Windows.Forms.ToolStripButton
    Friend WithEvents FLP_param_stampa As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TAC_modulistampa As System.Windows.Forms.TabControl
    Friend WithEvents TAP_modulo_00 As System.Windows.Forms.TabPage
    Friend WithEvents GRB_stampante_car_00 As System.Windows.Forms.GroupBox
    Friend WithEvents LBL_stampante_00 As System.Windows.Forms.Label
    Friend WithEvents CMB_stampanti_00 As System.Windows.Forms.ComboBox
    Friend WithEvents DGV_ass_cassetti_00 As System.Windows.Forms.DataGridView
    Friend WithEvents LBL_fat_cor_x_00 As System.Windows.Forms.Label
    Friend WithEvents RDB_cassetti_00 As System.Windows.Forms.RadioButton
    Friend WithEvents TXB_fat_cor_car_x_00 As System.Windows.Forms.TextBox
    Friend WithEvents CMB_cassetto_def_00 As System.Windows.Forms.ComboBox
    Friend WithEvents LBL_fat_cor_y_00 As System.Windows.Forms.Label
    Friend WithEvents RDB_default_00 As System.Windows.Forms.RadioButton
    Friend WithEvents TXB_fat_cor_car_y_00 As System.Windows.Forms.TextBox
    Friend WithEvents CHB_modulo_da_stampare_00 As System.Windows.Forms.CheckBox
    Friend WithEvents LSB_moduli_stampare As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FLP_moduli_00 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents GRB_stampante_ele_00 As System.Windows.Forms.GroupBox
    Friend WithEvents CHB_rendi_pari_00 As System.Windows.Forms.CheckBox
    Friend WithEvents CHB_reset_cod_wr_ele_00 As System.Windows.Forms.CheckBox
    Friend WithEvents TXB_cod_wr_ele_00 As System.Windows.Forms.TextBox
    Friend WithEvents LBL_nome_file_arch_ott As System.Windows.Forms.Label
    Friend WithEvents CMB_file_arch_ott_00 As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TXB_fat_cor_ele_x_00 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents TXB_fat_cor_ele_y_00 As System.Windows.Forms.TextBox
    Friend WithEvents Pagina As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cassetto As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents LBL_stato_modulo_00 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents LBL_err_desc As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LBL_stato_rec As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents LBL_err_fase As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents LBL_rec_selz As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents GRB_anagraficasessione As System.Windows.Forms.GroupBox
    Friend WithEvents LBL_sessione_nome As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents LBL_plugin_versione As System.Windows.Forms.Label
    Friend WithEvents LBL_sessione_cartella As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents LBL_plugin_filename As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents LBL_plugin_name As System.Windows.Forms.Label
    Friend WithEvents LBL_sessione_Descrizione As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GRP_statistiche As System.Windows.Forms.GroupBox
    Friend WithEvents LBL_dati_var As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents LBL_err_car_liev As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents LBL_rec_load As System.Windows.Forms.Label
    Friend WithEvents LBL_err_prn As System.Windows.Forms.Label
    Friend WithEvents LBL_rec_sel As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents LBL_err_car_grav As System.Windows.Forms.Label
    Friend WithEvents TAC_statistiche As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents DGV_stat_glob As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents DGV_stat_parz As System.Windows.Forms.DataGridView
    Friend WithEvents CHB_fusione_moduli As System.Windows.Forms.CheckBox
    Friend WithEvents PNL_documenti_cont As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STA_descrizione As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STS_stampate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TSB_open_documenti_unificati_fld As System.Windows.Forms.ToolStripButton
    Friend WithEvents CHB_fast_print As System.Windows.Forms.CheckBox
    Friend WithEvents BTN_stampa As System.Windows.Forms.Button
End Class
