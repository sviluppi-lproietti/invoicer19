<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_start
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LBL_app_name = New System.Windows.Forms.Label()
        Me.LBL_version = New System.Windows.Forms.Label()
        Me.LBL_descr_contr = New System.Windows.Forms.Label()
        Me.LBL_controllo_runn = New System.Windows.Forms.Label()
        Me.TMR_controlli = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = Global.Invoicer.My.Resources.Resources.PNG_printer
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(223, 158)
        Me.Panel1.TabIndex = 0
        '
        'LBL_app_name
        '
        Me.LBL_app_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_app_name.Location = New System.Drawing.Point(8, 182)
        Me.LBL_app_name.Name = "LBL_app_name"
        Me.LBL_app_name.Size = New System.Drawing.Size(95, 20)
        Me.LBL_app_name.TabIndex = 1
        Me.LBL_app_name.Text = "......"
        '
        'LBL_version
        '
        Me.LBL_version.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_version.Location = New System.Drawing.Point(109, 182)
        Me.LBL_version.Name = "LBL_version"
        Me.LBL_version.Size = New System.Drawing.Size(126, 20)
        Me.LBL_version.TabIndex = 2
        Me.LBL_version.Text = "Vers. "
        Me.LBL_version.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'LBL_descr_contr
        '
        Me.LBL_descr_contr.AutoSize = True
        Me.LBL_descr_contr.Location = New System.Drawing.Point(9, 213)
        Me.LBL_descr_contr.Name = "LBL_descr_contr"
        Me.LBL_descr_contr.Size = New System.Drawing.Size(88, 13)
        Me.LBL_descr_contr.TabIndex = 3
        Me.LBL_descr_contr.Text = "Controllo in corso"
        '
        'LBL_controllo_runn
        '
        Me.LBL_controllo_runn.AutoSize = True
        Me.LBL_controllo_runn.Location = New System.Drawing.Point(9, 235)
        Me.LBL_controllo_runn.Name = "LBL_controllo_runn"
        Me.LBL_controllo_runn.Size = New System.Drawing.Size(40, 13)
        Me.LBL_controllo_runn.TabIndex = 4
        Me.LBL_controllo_runn.Text = "-----------"
        '
        'TMR_controlli
        '
        Me.TMR_controlli.Interval = 1000
        '
        'FRM_start
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(243, 266)
        Me.ControlBox = False
        Me.Controls.Add(Me.LBL_controllo_runn)
        Me.Controls.Add(Me.LBL_descr_contr)
        Me.Controls.Add(Me.LBL_version)
        Me.Controls.Add(Me.LBL_app_name)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "FRM_start"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LBL_app_name As System.Windows.Forms.Label
    Friend WithEvents LBL_version As System.Windows.Forms.Label
    Friend WithEvents LBL_descr_contr As System.Windows.Forms.Label
    Friend WithEvents LBL_controllo_runn As System.Windows.Forms.Label
    Friend WithEvents TMR_controlli As System.Windows.Forms.Timer
End Class
