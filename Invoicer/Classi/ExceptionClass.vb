Public Class ExceptionClass
    Inherits System.Exception

    Public Sub New()
        Me.New("An error occurred in the component.", Nothing)
    End Sub

    Public Sub New(ByVal message As String)
        Me.New(message, Nothing)
    End Sub

    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
        Source = "My_Custom_Component"
    End Sub

End Class
