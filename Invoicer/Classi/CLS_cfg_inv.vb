Imports Microsoft.Win32
Imports ISC.Invoicer.ManageStampa
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Public Class CLS_cfg_inv
    Inherits CLS_BaseConfig

    Public Property LicenseSwitch As Boolean
    Public Property Stampanti As Dictionary(Of Integer, StampantiEntity)

    Public Property SleepEsecuzioneAttivita As Integer

        Get
            Return GetConfigVariabile("SleepEsecuzioneAttivita", "100")
        End Get
        Set(ByVal value As Integer)
            SetConfigVariabile("SleepEsecuzioneAttivita", value)
        End Set

    End Property

    Public ReadOnly Property StampaCartaceaAblitata As Boolean

        Get
            Return Me.StampanteCartaceaDefault IsNot Nothing
        End Get

    End Property

    Public Property StampanteCartaceaDefault As StampantiEntity

        Get
            Return _StampanteCartaceaDefault
        End Get
        Set(ByVal value As StampantiEntity)
            _StampanteCartaceaDefault = value
            If value IsNot Nothing Then
                SetConfigVariabile("prndefault", value.STA_nome)
            End If
        End Set

    End Property

    Public ReadOnly Property StampaOtticaAblitata As Boolean

        Get
            Return Me.StampanteCartaceaDefault IsNot Nothing
        End Get

    End Property

    Public Property StampanteOtticaDefault As StampantiEntity

        Get
            Return _StampanteOtticaDefault
        End Get
        Set(ByVal value As StampantiEntity)
            _StampanteOtticaDefault = value
            If value IsNot Nothing Then
                SetConfigVariabile("prnarchott", value.STA_nome)
            End If
        End Set

    End Property

    Public Property VersionePDFCreator As String

    '
    ' Contiene la lista delle stampanti presenti nel sistema
    '
    Private Property _StampanteCartaceaDefault As StampantiEntity
    Private Property _StampanteOtticaDefault As StampantiEntity

    ' ************************************************************************* '
    ' Carica il file di configurazione se presente altrimenti predispone un do- '
    ' cumento vuoto per il salvataggio dei dati di configurazione. Inoltre leg- '
    ' ge lo switch per determinare se l'applicazione � libera o vincolata.      '
    ' ************************************************************************* '
    Public Sub New(ByVal cBaseDirectory As String)
        MyBase.New(eTipoApplicazione.TAP_Invoicer, cBaseDirectory, "invoicerCFG.xml")

        '
        ' Legge lo switch Free/Licensed
        '
        LoadSwitchKey()

        '
        ' Aggiungi le cartelle al dizionario dei folder
        '
        AggiungiFolders()

        '
        ' Aggiungi i file al dizionario dei file
        '
        AggiungiFiles()

        '
        ' Imposta l'elenco dei file necessari al funzionamento dell'applicazione.
        '
        ImpostaListaFileNecessari()

        '
        ' Recupero la lista delle stampanti
        '
        RecuperaListaStampanti()

    End Sub

    Private Sub AggiungiFolders()

        Folders.Add("PrnWrkFLD", ConcatenaFolderValue(Folders("RootApplicationFLD"), "PrnWork"))
        Folders.Add("DatiAppFLD", ConcatenaFolderValue(Folders("RootApplicationFLD"), "Dati"))

        '
        ' Sostituisce il valore presente nella oggetto di configurazione con quello del file di configurazione.
        '
        Folders("SessioniDefaultFLD") = GetConfigVariabile("moduli_path", "")
        Folders("SessioniCreateFLD") = GetConfigVariabile("dati_path", "")
        Folders("WorkFLD") = GetConfigVariabile("work_path", "")

    End Sub

    Private Sub AggiungiFiles()

        Files.Add("SessioniRecentiFIL", ConcatenaFolderFileValue(Folders("DatiAppFLD"), "SessioniRecenti.xml"))

    End Sub

    Public Sub RegistraParametroSuFileCFG(ByVal xPath As String, ByVal cValue As String)

        SetConfigVariabile(xPath, cValue)

    End Sub

    '
    ' Elenco dei file con i quali l'applicazione funziona.
    '
    Private Sub ImpostaListaFileNecessari()

        'Me.ListaFileNecessari.Add("Interop.PDFCreator.dll")
        'Me.ListaFileNecessari.Add("itextsharp.dll")
        'Me.ListaFileNecessari.Add("ManageDataset.dll")
        'Me.ListaFileNecessari.Add("ManageSessione.dll")
        'Me.ListaFileNecessari.Add("ManageModulo.dll")
        'Me.ListaFileNecessari.Add("ManageStampa.dll")
        'Me.ListaFileNecessari.Add("OggettiComuni.dll")
        'Me.ListaFileNecessari.Add("OggettiComuniForm.dll")

        '
        ' Librerie dismesse ma mantenute per compatibilit� con vecchi plugin.
        '
        Me.ListaFileNecessari.Add("TipiComuni.dll")
        Me.ListaFileNecessari.Add("TipiComuni2.dll")

    End Sub

    Public ReadOnly Property ErrorRecipient() As String

        Get
            Return "sortecert.ccn@gmail.com"      'GetConfigVariab("BackOfficeRecipient", "")
        End Get

    End Property

    Public ReadOnly Property CCNRecipient() As String

        Get
            Return "sortecert.ccn@gmail.com"      'GetConfigVariab("BackOfficeRecipient", "")
        End Get

    End Property

    ReadOnly Property DimensioneMaxLogFLD() As Integer

        Get
            Return GetConfigVariabile("DimensioneMaxLogFLD", "500")
        End Get

    End Property

    Public Function FolderLocked(ByVal cFolder As String) As Boolean
        Dim aFolderLocked As ArrayList

        aFolderLocked = New ArrayList
        'aFolderLocked.Add(Me.SanitizeNomeLungoFileFLD)
        'aFolderLocked.Add(Me.ConsegnaForzataBaseFLD)
        'aFolderLocked.Add(Me.ElaboraFileEsiti)
        'aFolderLocked.Add(Me.ScartiErrorixDBFLD(0))
        'aFolderLocked.Add(Me.ScartiErrorixDBFLD(5))
        'aFolderLocked.Add(Me.ScartiGiaProcessatiFLD(""))
        'aFolderLocked.Add(Me.ScartiErratiFLD(0))
        'aFolderLocked.Add(Me.ScartiErratiFLD(5))
        'aFolderLocked.Add(Me.RiposizionaDOCBaseFLD)
        'aFolderLocked.Add(Me.SanitizeVefiricaFileFLD)
        'aFolderLocked.Add(Me.RielaboraEsitiFLD)
        Return aFolderLocked.Contains(StandardFolderValue(cFolder))

    End Function

    Public Property PrnWrkFLDSize() As Long

        Get
            Return 100000000
        End Get
        Set(ByVal value As Long)

        End Set

    End Property

#Region "Gestione delle stampanti di sistema e di quelle predefinite"

    Public Sub VerificaStampantiConfigurate()
        Dim s As StampantiEntity

        Me.StampanteCartaceaDefault = Nothing
        Me.StampanteOtticaDefault = Nothing
        For Each p As KeyValuePair(Of Integer, StampantiEntity) In _Stampanti
            s = p.Value
            If s.STA_nome = GetConfigVariabile("prndefault", "") Then
                Me.StampanteCartaceaDefault = s
            End If
            If s.STA_nome = GetConfigVariabile("prnarchott", "") Then
                Me.StampanteOtticaDefault = s
            End If
        Next

    End Sub

    ' ********************************************************************* '
    ' Recupera la lista completa delle stampanti e popola la lista delle    ' 
    ' stampanti.                                                            '
    ' ********************************************************************* '
    Private Sub RecuperaListaStampanti()

        _Stampanti = New Dictionary(Of Integer, StampantiEntity)
        _Stampanti.Add(_Stampanti.Count, New StampantiEntity(0, "----- Seleziona una stampante -----"))
        For Each cPrinter As String In Printing.PrinterSettings.InstalledPrinters
            _Stampanti.Add(_Stampanti.Count, New StampantiEntity(_Stampanti.Count, cPrinter))
        Next

    End Sub

#End Region

    Public ReadOnly Property SuddivisioneModuli() As Boolean

        Get
            Return System.IO.File.Exists("itextsharp.dll")
        End Get

    End Property

    Property CheckDatiVar() As Boolean

        Get
            Return GetConfigVariabile("checkdativar", 0) = 1
        End Get
        Set(ByVal value As Boolean)
            SetConfigVariabile("checkdativar", value)
        End Set

    End Property

    Property CleanWork() As Boolean

        Get
            Return GetConfigVariabile("cleanwork", 0) = 1
        End Get
        Set(ByVal value As Boolean)
            SetConfigVariabile("cleanwork", value)
        End Set

    End Property

    Property MaxNumeroFogli() As Integer

        Get
            Return GetConfigVariabile("maxnumfogli", "999999")
        End Get
        Set(ByVal value As Integer)
            SetConfigVariabile("maxnumfogli", value.ToString)
        End Set

    End Property

    Property DocumentiContemporanei() As Integer

        Get
            Return GetConfigVariabile("ModuliContemporanei", 100)
        End Get
        Set(ByVal value As Integer)
            SetConfigVariabile("ModuliContemporanei", value.ToString)
        End Set

    End Property

    Property PaginePerParte() As Integer

        Get
            Return GetConfigVariabile("pagineperparte", 100)
        End Get
        Set(ByVal value As Integer)
            SetConfigVariabile("pagineperparte", value.ToString)
        End Set

    End Property

    Property ResetWrapRound() As Boolean

        Get
            Return GetConfigVariabile("resetwrapround", 0) = 1
        End Get
        Set(ByVal value As Boolean)
            SetConfigVariabile("resetwrapround", value)
        End Set

    End Property

    Property SuffissoParte() As String

        Get
            Return GetConfigVariabile("suffissoparte", "parte")
        End Get
        Set(ByVal value As String)
            SetConfigVariabile("suffissoparte", value)
        End Set

    End Property

    Property DatabaseGiriConsegna() As String

        Get
            Return GetConfigVariabile("pathdbgiri", "DefinizioneGiri.mdb")
        End Get
        Set(ByVal value As String)
            SetConfigVariabile("pathdbgiri", value)
        End Set

    End Property

    Private Sub LoadSwitchKey()
        Dim regKey As RegistryKey

        Try
            If Me.BitOs = etipoBitOS.bit_32 Then
                regKey = Registry.LocalMachine.OpenSubKey("Software\Invoicer", False)
            ElseIf Me.BitOs = etipoBitOS.bit_64 Then
                regKey = Registry.LocalMachine.OpenSubKey("Software\Wow6432Node\Invoicer", False)
            Else
                regKey = Nothing
            End If

            If regKey IsNot Nothing Then
                Me.LicenseSwitch = (regKey.GetValue("LicenceSwitch", "Free") = "Licensed")
                regKey.Close()
            Else
                Me.LicenseSwitch = False
            End If
        Catch ex As Exception
            Me.LicenseSwitch = False
        End Try

    End Sub

    Sub ControllaVersioneLibrerie()
        Dim VersionInfo As FileVersionInfo
        Dim cFilename As String

        VersionePDFCreator = ""
        If BitOs = etipoBitOS.bit_32 Then
            cFilename = String.Concat(Environment.GetEnvironmentVariable("ProgramFiles"), "\pdfcreator\pdfcreator.exe")
            VersionInfo = FileVersionInfo.GetVersionInfo(cFilename)
            VersionePDFCreator = String.Concat(VersionInfo.FileMajorPart, ".", VersionInfo.FileMinorPart, ".", VersionInfo.FilePrivatePart)
        ElseIf BitOs = etipoBitOS.bit_64 Then
            cFilename = String.Concat(Environment.GetEnvironmentVariable("ProgramFiles(x86)"), "\pdfcreator\pdfcreator.exe")
            VersionInfo = FileVersionInfo.GetVersionInfo(cFilename)
            VersionePDFCreator = String.Concat(VersionInfo.FileMajorPart, ".", VersionInfo.FileMinorPart, ".", VersionInfo.FilePrivatePart)
        End If

    End Sub

    ReadOnly Property IsPDFCreatorCorretto() As Boolean

        Get
            Return VersionePDFCreator = MST_costanti.Versione_PDFCreator_Corretta
        End Get

    End Property

    ReadOnly Property VersioneSistemaOperativo() As String

        Get
            Return String.Concat("Windows ", Me.OSVersion)
        End Get

    End Property

    ReadOnly Property DBGiriConsegna_CS() As String

        Get
            Dim cRtnConnString As String

            Select Case Me.OSVersion
                Case Is = eTipoSistemaOperativo.Win_XP
                    cRtnConnString = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", Me.DatabaseGiriConsegna, ";")
                Case Is = eTipoSistemaOperativo.Win_2012
                    cRtnConnString = String.Concat("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=", Me.DatabaseGiriConsegna, ";")
                Case Else
                    cRtnConnString = ""
            End Select
            Return cRtnConnString
        End Get

    End Property

End Class
