Public Interface IPLG_dati_lib

    ' ************************************************************************** '
    ' Elenco delle propriet� dell'interfaccia di accesso al Plug IN.             '
    ' ************************************************************************** '
#Region "Elenco delle propriet� dell'interfaccia"

    Property MessaggiAvanzamento() As String()

    ReadOnly Property CapacitaInvioMail() As Boolean
    ReadOnly Property CurrentRecordNumber() As Integer
    ReadOnly Property DataSendByMail() As Xml.XmlNode
    ReadOnly Property ElementiMenuDedicati() As ArrayList
    ReadOnly Property ErrorePlugIn() As String

    ' ************************************************************************** '
    ' Dati del dataset che serve per la stampa e l'invio delle email nelle appli '
    ' cazioni INVOICER ed INVOICER_SENDER.                                       '
    ' ************************************************************************** '
    ReadOnly Property FullDataset() As DataSet
    ReadOnly Property FullDataset_Serialized() As String
    ReadOnly Property FullDataSet_MainTableName() As String
    ReadOnly Property FullDataset_MainTableKey() As String
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ '

    ' ************************************************************************** '
    ' Campo che linka il QUICKDATASET con il FULLDATASET in base alla tipologia  '
    ' di valori caricati.                                                        '
    ' ************************************************************************** '
    ReadOnly Property LinkField_QuickDS_FullDS() As String
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ '

    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList
    ReadOnly Property LogDSStampa() As DataSet
    ReadOnly Property MessaggioAvviso() As String
    ReadOnly Property NomePlugIn() As String
    ReadOnly Property DescrizionePlugIn() As String
    ReadOnly Property NuoviToponimi() As Boolean

    ' ************************************************************************** '
    ' Dati del dataset che serve per la visualizzazione guida nelle applicazioni '
    ' INVOICER ed INVOICER_SENDER.                                               '
    ' ************************************************************************** '
    ReadOnly Property QuickDataset() As DataSet
    ReadOnly Property QuickDataset_Serialized() As String
    ReadOnly Property QuickDataset_MainTableName() As String
    ReadOnly Property QuickDataset_MainTableKey() As String
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ '

    'ReadOnly Property ResultAL() As ArrayList
    ReadOnly Property StampaInProprio() As Boolean
    ReadOnly Property DatiAzioniPostStampa() As ArrayList
    ReadOnly Property DatiAzioniPreInvio() As ArrayList

    ' ************************************************************************** '
    ' Questo codice indica la tipologia di caricamento dei dati che bisogna effe ' 
    ' ttuare. I moduli per ciascun plug in sono liberi tra i codice 1 e 900, men '
    ' tre lo 0 e quelli tra 901 e 999 sono fissi.                                '
    '   0 - Dati del Quickdataset                                                '
    ' 999 - Dati per l'invio tramite e-mail del documento                        '
    ' ************************************************************************** '
    Property CodiceModulo() As Integer
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ '

    Property FileDati() As ArrayList
    WriteOnly Property DBGiriConnectionString() As String
    WriteOnly Property FeedString() As String
    WriteOnly Property FiltroSelezione() As String                               ' Indica con quale campo del record del Quickdataset indica se � selezionabile.
    WriteOnly Property ListaGiri() As ArrayList
    WriteOnly Property SessioneFld() As String
    WriteOnly Property SessioneNuova() As Boolean
    WriteOnly Property Loggingobj() As Object
    WriteOnly Property TipoAmb() As Object
    WriteOnly Property TempFLD() As String
    WriteOnly Property OLEDB_prefixStrs() As Dictionary(Of String, String)

#End Region

    ' ************************************************************************** '
    ' Elenco dei metodi dell'interfaccia di accesso al Plug IN.                  '
    ' ************************************************************************** '
#Region "Elenco dei metodi dell'interfaccia"

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean
    Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean
    Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean
    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String()
    Function DeliveryDataTranslate(ByVal cField As String) As String
    Function ValidaFileSessione(ByVal aDatiCreazioneSessione As ArrayList) As Boolean

    Sub CreaDataBaseSessione(ByVal _DatiCreazioneSessione As ArrayList, ByVal cSrcFLD As String, ByVal cDstDatiFLD As String)
    Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)
    Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)
    Sub GoToRecordNumber(ByVal nRecord As Integer)
    Sub ImpostaDGVDati(ByVal DGV As System.Windows.Forms.DataGridView)
    Sub LoadQuickDataset(ByVal cQuickDataSetIndex_FileName As String)
    Sub LoadFullDataset(ByVal nRecordToLoad As Integer)
    Sub PostAnteprima(ByVal aLista As ArrayList)
    Sub RemovePRNDSErrorRecord()
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)
    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String, ByVal cParametroSelez As String)
    Sub UpdateQuickDatasetFile()

#End Region

End Interface

