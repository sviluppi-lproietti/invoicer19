Imports System.Windows.Forms

Public Interface IPLG_dati_lib

    ' Propriet� 
    Property MessaggiAvanzamento() As String()
    Property CurrentRecordNumber() As Integer
    Property LogDSStampa() As DataSet

    ReadOnly Property AlertPlugIn() As String
    'ReadOnly Property BOD() As Boolean
    'ReadOnly Property EOD() As Boolean
    'ReadOnly Property FeedArray() As ArrayList
    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList

    ReadOnly Property NomePlugIn() As String
    ReadOnly Property VersionePlugIn() As String

    ReadOnly Property LinkField_QuickDS_PrintDS() As String

    ReadOnly Property PrintDataset() As DataSet
    ReadOnly Property PrintDataSet_MainTableName() As String

    ReadOnly Property QuickDataset() As DataSet
    ReadOnly Property QuickDataset_MainTableName() As String
    ReadOnly Property QuickDataset_MainTableKey() As String
    ReadOnly Property ResultAL() As ArrayList

    WriteOnly Property CodiceModulo() As Integer
    WriteOnly Property FeedString() As String
    WriteOnly Property FiltroSelezione() As String                      ' Indica con quale campo del record del Quickdataset indica se � selezionabile.
    'WriteOnly Property SessioneNuova() As Boolean
    WriteOnly Property SessioneFld() As String
    WriteOnly Property SezioneDati() As Xml.XmlNode
    WriteOnly Property SessioneDatiFissiFileName() As String
    WriteOnly Property SessioneTrasformazioniFileName() As String
    WriteOnly Property SessioneDatiPath() As String

    'Function LoadDataNewSession(ByVal aDatiNewSession As ArrayList) As Boolean
    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean
    Function ElementiMenuDedicati() As ArrayList
    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String()

    Sub ClearPrintDataset()
    Sub GoToRecordNumber(ByVal nRecord As Integer)
    Sub ImpostaDGVDati(ByVal DGV As DataGridView)
    Sub LoadQuickDataset()
    Sub LoadPrintDataset(ByVal nRecordToLoad As Integer)
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)
    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String)
    Sub CreaDataBaseSessione(ByVal DatiSessione As ArrayList)
    Sub RemovePRNDSErrorRecord()
    Sub PostPreStampa(ByVal aLista As ArrayList)

End Interface

