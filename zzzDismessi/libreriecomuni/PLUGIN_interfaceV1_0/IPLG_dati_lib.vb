Imports System.Windows.Forms

Public Interface IPLG_dati_lib

    ' ProprietÓ 
    Property MessaggiAvanzamento() As String()
    Property CurrentRecordNumber() As Integer
    Property LogDS() As DataSet

    ReadOnly Property AlertPlugIn() As String
    ReadOnly Property BOD() As Boolean
    ReadOnly Property EOD() As Boolean
    ReadOnly Property FeedArray() As ArrayList
    ReadOnly Property ListaControlliForm() As ArrayList
    ReadOnly Property LoadSpecific() As Boolean
    ReadOnly Property QuickDataset() As DataSet
    ReadOnly Property PlugInName() As String
    ReadOnly Property PlugInVersion() As String
    ReadOnly Property PlugInMainTable(ByVal nModulo As Integer) As String
    ReadOnly Property PlugInMainTableKey(ByVal nModulo As Integer) As String
    ReadOnly Property PlugInMainLinkToQD(ByVal nModulo As Integer) As String
    ReadOnly Property PrintDataset() As DataSet
    ReadOnly Property ResultAL() As ArrayList

    WriteOnly Property FeedString() As String
    WriteOnly Property CodiceModulo() As Integer
    WriteOnly Property SessioneNuova() As Boolean
    WriteOnly Property SessionePath() As String
    WriteOnly Property SessioneDati() As Xml.XmlNode
    WriteOnly Property SessioneDatiFissiFileName() As String
    WriteOnly Property SessioneTrasformazioniFileName() As String
    WriteOnly Property SessioneDatiPath() As String

    Function GetSessioneMenuItem() As ArrayList
    Function LoadDataNewSession(ByVal aDatiNewSession As ArrayList) As Boolean
    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean
    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String()

    Sub ClearPrintDataset()
    Sub GoToRecordNumber(ByVal nRecord As Integer)
    Sub ImpostaDGVDati(ByVal DGV As DataGridView)
    Sub LoadQuickDataset()
    Sub LoadPrintDataset(ByVal nRecordToLoad As Integer)
    Sub SetSelectable(ByVal dr As DataRow)
    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String)
    Sub CreaDBSessioneS1(ByVal DatiSessione As ArrayList)
    Sub CreaDBSessioneS2()
    Sub RemovePRNDSErrorRecord()

End Interface

