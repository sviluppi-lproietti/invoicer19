﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Le informazioni generali relative a un assembly sono controllate dal seguente 
' insieme di attributi. Per modificare le informazioni associate a un assembly
' occorre quindi modificare i valori di questi attributi.

' Rivedere i valori degli attributi dell'assembly

<Assembly: AssemblyTitle("PLUGIN_interfaceV1_1")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Microsoft")> 
<Assembly: AssemblyProduct("PLUGIN_interfaceV1_1")> 
<Assembly: AssemblyCopyright("Copyright © Microsoft 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'Se il progetto viene esposto a COM, il GUID che segue verrà utilizzato per creare l'ID della libreria dei tipi
<Assembly: Guid("a885f837-c27c-4ae4-b524-2b239becc056")> 

' Le informazioni sulla versione di un assembly sono costituite dai seguenti quattro valori:
'
'      Numero di versione principale
'      Numero di versione secondario 
'      Numero build
'      Revisione
'
' È possibile specificare tutti i valori oppure impostare valori predefiniti per i numeri relativi alla revisione e alla build 
' utilizzando l'asterisco (*) come descritto di seguito:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
