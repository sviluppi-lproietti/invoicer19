Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem

Public Interface PlugIN_2_0_0

    ' ************************************************************************** '
    ' Elenco delle propriet� dell'interfaccia di accesso al Plug IN.             '
    ' ************************************************************************** '
#Region "Elenco delle propriet� dell'interfaccia"

    Property MessaggiAvanzamento() As String()

    Property MessaggiProgress As MessaggiEntity
    Property OleDb_ConnString() As Dictionary(Of String, CLS_oledbconnstring)

    ReadOnly Property CapacitaInvioMail() As Boolean
    ReadOnly Property DataSendByMail() As Xml.XmlNode
    ReadOnly Property ElementiMenuDedicati() As ArrayList
    ReadOnly Property ErrorePlugIn() As String
    ReadOnly Property CurrentRecordNumber() As Integer

    ' ************************************************************************** '
    ' Dati del dataset che serve per la stampa e l'invio delle email nelle appli '
    ' cazioni INVOICER ed INVOICER_SENDER.                                       '
    ' ************************************************************************** '
    ReadOnly Property FullDataset() As DataSet
    ReadOnly Property FullDataSet_MainTableName() As String
    ReadOnly Property FullDataset_MainTableKey() As String
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ '

    ' ************************************************************************** '
    ' Campo che linka il QUICKDATASET con il FULLDATASET in base alla tipologia  '
    ' di valori caricati.                                                        '
    ' ************************************************************************** '
    ReadOnly Property LinkField_QuickDS_FullDS() As String
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ '

    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList
    ReadOnly Property LogDSStampa() As DataSet
    ReadOnly Property MessaggioAvviso() As String
    ReadOnly Property NomePlugIn() As String
    ReadOnly Property DescrizionePlugIn() As String
    ReadOnly Property NuoviToponimi() As Boolean

    ' ************************************************************************** '
    ' Dati del dataset che serve per la visualizzazione guida nelle applicazioni '
    ' INVOICER ed INVOICER_SENDER.                                               '
    ' ************************************************************************** '
    ReadOnly Property QuickDataset() As DataSet
    ReadOnly Property QuickDataset_MainTableName() As String
    ReadOnly Property QuickDataset_MainTableKey() As String
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ '

    Property ResultAL() As ArrayList
    ReadOnly Property StampaInProprio() As Boolean
    ReadOnly Property DatiAzioniPostStampa() As ArrayList
    ReadOnly Property DatiAzioniPreInvio() As ArrayList

    ' ************************************************************************** '
    ' Questo codice indica la tipologia di caricamento dei dati che bisogna effe ' 
    ' ttuare. I moduli per ciascun plug in sono liberi tra i codice 1 e 900, men '
    ' tre lo 0 e quelli tra 901 e 999 sono fissi.                                '
    '   0 - Dati del Quickdataset                                                '
    ' 999 - Dati per l'invio tramite e-mail del documento                        '
    ' ************************************************************************** '
    Property CodiceModulo() As Integer
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ '

    Property FileDati() As ArrayList
    Property SezioneDati As Xml.XmlNode
    WriteOnly Property DBGiriConnectionString() As String
    WriteOnly Property FeedString() As String
    WriteOnly Property FiltroSelezione() As String                               ' Indica con quale campo del record del Quickdataset indica se � selezionabile.
    WriteOnly Property ListaGiri() As ArrayList
    WriteOnly Property SessioneFld() As String
    WriteOnly Property SessioneNuova() As Boolean
    WriteOnly Property Loggingobj() As CLS_logger
    WriteOnly Property TipoAmbiente() As eTipoApplicazione
    WriteOnly Property TipoAmb() As Object
    WriteOnly Property OLEDB_prefixStrs() As Dictionary(Of String, String)

#End Region

    ' ************************************************************************** '
    ' Elenco dei metodi dell'interfaccia di accesso al Plug IN.                  '
    ' ************************************************************************** '
#Region "Elenco dei metodi dell'interfaccia"

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean
    Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean
    Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean
    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String()
    Function DeliveryDataTranslate(ByVal cField As String) As String
    Function ValidaFileSessione(ByVal aDatiCreazioneSessione As ArrayList) As Boolean

    Sub CreaDataBaseSessione(ByVal _DatiCreazioneSessione As ArrayList)
    Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)
    Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)
    Sub GoToRecordNumber(ByVal nRecord As Integer)
    Sub ImpostaDGVDati(ByVal DGV As System.Windows.Forms.DataGridView)
    Sub Carica_QuickDataset()
    Sub LoadQuickDataset(ByVal cQuickDataSetIndex_FileName As String)
    Sub LoadFullDataset(ByVal nRecordToLoad As Integer)
    Sub PostAnteprima(ByVal aLista As ArrayList)
    Sub RemovePRNDSErrorRecord()
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)
    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String, ByVal cParametroSelez As String)
    Sub UpdateQuickDatasetFile()

    Property Files As CLS_FileFolderDic
    Property Folders As CLS_FileFolderDic
    Property FoldersGlobali As CLS_FileFolderDic

#End Region

End Interface