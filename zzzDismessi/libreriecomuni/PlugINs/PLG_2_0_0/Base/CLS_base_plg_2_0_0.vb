﻿Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori

Public MustInherit Class CLS_base_plg_2_0_0
    Implements PlugINs.PlugIN_2_0_0

    Public Property CodiceModulo As Integer Implements PlugIN_2_0_0.CodiceModulo
    Public Property ErroriLievi As String
    Public Property Files As CLS_FileFolderDic Implements PlugIN_2_0_0.Files
    Public Property Folders As CLS_FileFolderDic Implements PlugIN_2_0_0.Folders
    Public Property FoldersGlobali As CLS_FileFolderDic Implements PlugIN_2_0_0.FoldersGlobali
    Public Property MessaggiProgress As MessaggiEntity Implements PlugIN_2_0_0.MessaggiProgress
    Public Property OleDb_ConnString As System.Collections.Generic.Dictionary(Of String, CLS_oledbconnstring) Implements PlugIN_2_0_0.OleDb_ConnString
    Public Property SezioneDati As Xml.XmlNode Implements PlugIN_2_0_0.SezioneDati

    Public MustOverride ReadOnly Property CapacitaInvioMail As Boolean Implements PlugIN_2_0_0.CapacitaInvioMail
    Public MustOverride ReadOnly Property FullDataset_MainTableKey As String Implements PlugIN_2_0_0.FullDataset_MainTableKey
    Public MustOverride ReadOnly Property FullDataSet_MainTableName As String Implements PlugIN_2_0_0.FullDataSet_MainTableName
    Public MustOverride ReadOnly Property NomePlugIn As String Implements PlugIN_2_0_0.NomePlugIn

    '
    ' Proprietà relativa al quickdataset
    '
    Public MustOverride ReadOnly Property QuickDataset() As DataSet Implements PlugIN_2_0_0.QuickDataset
    Public MustOverride ReadOnly Property QuickDataset_MainTableKey As String Implements PlugIN_2_0_0.QuickDataset_MainTableKey
    Public MustOverride ReadOnly Property QuickDataset_MainTableName As String Implements PlugIN_2_0_0.QuickDataset_MainTableName
    Public MustOverride ReadOnly Property StampaInProprio As Boolean Implements PlugIN_2_0_0.StampaInProprio

    Shadows Property RecordNumber As Integer

    WriteOnly Property TipoAmb() As Object Implements PlugIN_2_0_0.TipoAmb
        Set(ByVal value As Object)

        End Set
    End Property

    Public MustOverride Sub CreaDataBaseSessione(ByVal _DatiCreazioneSessione As System.Collections.ArrayList) Implements PlugIN_2_0_0.CreaDataBaseSessione
    Public MustOverride Sub Carica_FullDataset(ByVal nRecordToLoad As Integer) Implements PlugIN_2_0_0.LoadFullDataset

    Public Sub GoToRecordNumber(ByVal nRecord As Integer) Implements PlugIN_2_0_0.GoToRecordNumber

        Me.RecordNumber = nRecord

    End Sub

    Public Property MessaggiAvanzamento() As String() Implements PlugIN_2_0_0.MessaggiAvanzamento

    Private _CurrentRecordNumber As Integer

    Public ReadOnly Property CurrentRecordNumber() As Integer Implements PlugIN_2_0_0.CurrentRecordNumber
        Get
            Return _CurrentRecordNumber
        End Get
    End Property

    Public MustOverride Sub Carica_QuickDataset() Implements PlugIN_2_0_0.Carica_QuickDataset

    Public Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PlugIN_2_0_0.CheckCondAPI

    End Function

    Public Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PlugIN_2_0_0.CheckCondPSA

    End Function

    WriteOnly Property OLEDB_prefixStrs() As Dictionary(Of String, String) Implements PlugIN_2_0_0.OLEDB_prefixStrs
        Set(ByVal value As Dictionary(Of String, String))

        End Set
    End Property

    Public Function CheckDatiSessione(ByVal aDati As System.Collections.ArrayList) As Boolean Implements PlugIN_2_0_0.CheckDatiSessione

    End Function

    ReadOnly Property FullDataset() As DataSet Implements PlugIN_2_0_0.FullDataset
        Get

        End Get
    End Property
    Sub LoadQuickDataset(ByVal cQuickDataSetIndex_FileName As String) Implements PlugIN_2_0_0.LoadQuickDataset

    End Sub

    Public ReadOnly Property DataSendByMail As System.Xml.XmlNode Implements PlugIN_2_0_0.DataSendByMail
        Get

        End Get
    End Property

    Public ReadOnly Property DatiAzioniPostStampa As System.Collections.ArrayList Implements PlugIN_2_0_0.DatiAzioniPostStampa
        Get

        End Get
    End Property

    Public ReadOnly Property DatiAzioniPreInvio As System.Collections.ArrayList Implements PlugIN_2_0_0.DatiAzioniPreInvio
        Get

        End Get
    End Property

    Public WriteOnly Property DBGiriConnectionString As String Implements PlugIN_2_0_0.DBGiriConnectionString
        Set(ByVal value As String)

        End Set
    End Property

    Public Function DeliveryDataTranslate(ByVal cField As String) As String Implements PlugIN_2_0_0.DeliveryDataTranslate

    End Function

    Public ReadOnly Property DescrizionePlugIn As String Implements PlugIN_2_0_0.DescrizionePlugIn
        Get

        End Get
    End Property

    Public ReadOnly Property ElementiMenuDedicati As System.Collections.ArrayList Implements PlugIN_2_0_0.ElementiMenuDedicati
        Get

        End Get
    End Property

    Public ReadOnly Property ErrorePlugIn As String Implements PlugIN_2_0_0.ErrorePlugIn
        Get

        End Get
    End Property

    Public Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PlugIN_2_0_0.ExecActionAPI

    End Sub

    Public Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PlugIN_2_0_0.ExecActionPSA

    End Sub

    Public WriteOnly Property FeedString As String Implements PlugIN_2_0_0.FeedString
        Set(ByVal value As String)

        End Set
    End Property

    Public Property FileDati As System.Collections.ArrayList Implements PlugIN_2_0_0.FileDati
        Get

        End Get
        Set(ByVal value As System.Collections.ArrayList)

        End Set
    End Property

    Public WriteOnly Property FiltroSelezione As String Implements PlugIN_2_0_0.FiltroSelezione
        Set(ByVal value As String)

        End Set
    End Property

    Public Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String() Implements PlugIN_2_0_0.GetSelectedRow

    End Function

    Public MustOverride Sub ImpostaDGVDati(ByVal DGV As System.Windows.Forms.DataGridView) Implements PlugIN_2_0_0.ImpostaDGVDati

    Public MustOverride ReadOnly Property LinkField_QuickDS_FullDS As String Implements PlugIN_2_0_0.LinkField_QuickDS_FullDS

    Public MustOverride ReadOnly Property ListaControlliFormNuovaSessione As System.Collections.ArrayList Implements PlugIN_2_0_0.ListaControlliFormNuovaSessione

    Public WriteOnly Property ListaGiri As System.Collections.ArrayList Implements PlugIN_2_0_0.ListaGiri
        Set(ByVal value As System.Collections.ArrayList)

        End Set
    End Property

    Public ReadOnly Property LogDSStampa As System.Data.DataSet Implements PlugIN_2_0_0.LogDSStampa
        Get

        End Get
    End Property

    Public WriteOnly Property Loggingobj As CLS_logger Implements PlugIN_2_0_0.Loggingobj
        Set(ByVal value As CLS_logger)

        End Set
    End Property



    Public ReadOnly Property MessaggioAvviso As String Implements PlugIN_2_0_0.MessaggioAvviso
        Get

        End Get
    End Property

    Public ReadOnly Property NuoviToponimi As Boolean Implements PlugIN_2_0_0.NuoviToponimi
        Get

        End Get
    End Property

    Public Sub PostAnteprima(ByVal aLista As System.Collections.ArrayList) Implements PlugIN_2_0_0.PostAnteprima

    End Sub

    Public Sub RemovePRNDSErrorRecord() Implements PlugIN_2_0_0.RemovePRNDSErrorRecord

    End Sub

    Public Property ResultAL As System.Collections.ArrayList Implements PlugIN_2_0_0.ResultAL

    Public WriteOnly Property SessioneFld As String Implements PlugIN_2_0_0.SessioneFld
        Set(ByVal value As String)

        End Set
    End Property

    Public WriteOnly Property SessioneNuova As Boolean Implements PlugIN_2_0_0.SessioneNuova
        Set(ByVal value As Boolean)

        End Set
    End Property

    Public Sub SetPrintable(ByVal dr As System.Data.DataRow, ByVal _SelectFromPlugInType As String, ByVal cParametroSelez As String) Implements PlugIN_2_0_0.SetPrintable

    End Sub

    Public Sub SetSelectable(ByVal dr As System.Data.DataRow, ByVal cFilter As String) Implements PlugIN_2_0_0.SetSelectable

    End Sub

    Public WriteOnly Property TipoAmbiente As eTipoApplicazione Implements PlugIN_2_0_0.TipoAmbiente

        Set(ByVal value As eTipoApplicazione)

        End Set

    End Property

    Public Sub UpdateQuickDatasetFile() Implements PlugIN_2_0_0.UpdateQuickDatasetFile

    End Sub

    Public Function ValidaFileSessione(ByVal aDatiCreazioneSessione As System.Collections.ArrayList) As Boolean Implements PlugIN_2_0_0.ValidaFileSessione

    End Function

    '
    ' Questo file processa l'ordinamento dei record del QuickDataset
    '
    Shadows Sub FastSortFile(ByVal cSourceFile As String, ByVal cDestFile As String, ByVal cSortFile As String)
        Dim xslt As System.Xml.Xsl.XslCompiledTransform

        Try
            xslt = New System.Xml.Xsl.XslCompiledTransform()
            xslt.Load(cSortFile)

            xslt.Transform(cSourceFile, cDestFile)
        Catch ex As Exception

        End Try

    End Sub

    Shadows Function Carica_Excel2Dataset(ByVal cFileName As String, ByVal cFoglioName As String) As DataSet
        Dim oConn As OleDb.OleDbConnection
        Dim oDA As OleDb.OleDbDataAdapter
        Dim oCmd As OleDb.OleDbCommand
        Dim cEstensione As String
        Dim dsExcel As DataSet

        If cFileName > "" Then
            cEstensione = System.IO.Path.GetExtension(cFileName).Replace(".", "").ToLower

            oConn = New OleDb.OleDbConnection(Me.OleDb_ConnString(cEstensione).BuildConnString(cFileName))
            oConn.Open()
            oCmd = New OleDb.OleDbCommand(String.Concat("SELECT * FROM [", cFoglioName, "$]"), oConn)
            oCmd.CommandType = CommandType.Text
            oDA = New OleDb.OleDbDataAdapter
            oDA.SelectCommand = oCmd

            dsExcel = New DataSet()
            oDA.Fill(dsExcel)
            oConn.Close()
        Else
            dsExcel = Nothing
        End If
        Return dsExcel

    End Function

    Shadows Function GetValueFromXML(ByVal xmlItem As Xml.XmlNode, ByVal xmlPath As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cDefault
            If Not xmlItem.SelectSingleNode(xmlPath) Is Nothing Then
                cValue = xmlItem.SelectSingleNode(xmlPath).InnerText
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori del file XML.", vbLf, "percorso da cui si tenta il recupero:", vbLf, xmlPath), ex)
        End Try
        Return cValue

    End Function

    Shadows Function GetValueFromText(ByVal cText As String, ByVal nStart As Integer, ByVal nLength As Integer, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cText.Substring(nStart, nLength)
            If cValue.Trim = "" And cDefault > "" Then
                cValue = cDefault
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori da un testo.", vbLf, "Dati per il recupero: ", vbLf, "- inizio ", nStart, vbLf, "- fine ", nLength), ex)
        End Try
        Return cValue

    End Function

    Shadows Sub AddErroreLieve(ByVal cMsg As String)

        If ErroriLievi = "" Then
            ErroriLievi = cMsg
        Else
            ErroriLievi = String.Concat(ErroriLievi, "; ", cMsg)
        End If

    End Sub

    '
    ' Copia dei file indicati nella loro posizione''
    '
    Public Sub ImportaFileInSessione(ByVal _DatiSessione As ArrayList)
        Dim cFileName As String
        Dim nTipoFile As Integer

        For Each aValue In _DatiSessione
            nTipoFile = aValue(0)
            cFileName = aValue(1)
            Dim cDstFile As String

            If nTipoFile = 1 Then
                MessaggiProgress.TipoOperazioneSecon = "Copia del file nella cartella di archiviazione..."
                cDstFile = String.Concat(Me.Folders("DatiFLD"), cFileName.Substring(cFileName.LastIndexOf("\") + 1))

                System.IO.File.Copy(cFileName, cDstFile)
                MessaggiProgress.ProgressivoSecon += 1
                Me.ResultAL.Add(New String() {"filename", cDstFile.Replace(Me.Folders("SessioneRootFLD"), ""), MessaggiProgress.ProgressivoSecon, nTipoFile, 0})
            End If
        Next

    End Sub

    Public Function PrintDataSet_DataFile(ByVal _FileIdx As Integer) As String


        Return String.Concat(Me.Folders("SessioneRootFLD"), SezioneDati.SelectSingleNode(String.Concat("filename[@codice=", "", _FileIdx, "", "]")).InnerText)


    End Function



End Class
