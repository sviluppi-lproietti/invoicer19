Imports System.Windows.Forms

Public Interface IPLG_dati_lib

    ' Propriet� 
    Property MessaggiAvanzamento() As String()
    Property CurrentRecordNumber() As Integer
    Property LogDSStampa() As DataSet

    ReadOnly Property AlertPlugIn_PRN() As String
    ReadOnly Property AlertPlugIn_SND() As String
    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList

    ReadOnly Property NomePlugIn() As String
    ReadOnly Property VersionePlugIn() As String

    ReadOnly Property LinkField_QuickDS_PrintDS() As String
    ReadOnly Property LinkField_QuickDS_SendDS() As String

    ReadOnly Property PrintDataset() As DataSet
    ReadOnly Property PrintDataSet_MainTableName() As String

    ReadOnly Property QuickDataset() As DataSet
    ReadOnly Property QuickDataset_MainTableName() As String
    ReadOnly Property QuickDataset_MainTableKey() As String

    ReadOnly Property SendEmailDataset() As DataSet
    ReadOnly Property SendEmailDataset_MainTableName() As String
    ReadOnly Property SendEmailDataset_MainTableKey() As String

    ReadOnly Property ResultAL() As ArrayList
    ReadOnly Property DataSendByMail() As Xml.XmlNode

    WriteOnly Property CodiceModulo() As Integer
    WriteOnly Property FeedString() As String
    WriteOnly Property FiltroSelezione() As String                      ' Indica con quale campo del record del Quickdataset indica se � selezionabile.
    WriteOnly Property SessioneFld() As String
    WriteOnly Property SezioneDati() As Xml.XmlNode
    WriteOnly Property SessioneDatiFissiFileName() As String
    WriteOnly Property SessioneTrasformazioniFileName() As String
    WriteOnly Property SessioneDatiPath() As String

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean
    Function ElementiMenuDedicati(ByVal nTipoCaricamento As Integer) As ArrayList
    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String()

    Sub ClearPrintDataset()
    Sub GoToRecordNumber(ByVal nRecord As Integer)
    Sub ImpostaDGVDati(ByVal nTipoCaricamento As Integer, ByVal DGV As DataGridView)
    Sub LoadQuickDataset(ByVal nTipoCaricamento As Integer)
    Sub LoadPrintDataset(ByVal nRecordToLoad As Integer)
    Sub LoadSendDataset()
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)
    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String)
    Sub CreaDataBaseSessione(ByVal DatiSessione As ArrayList)
    Sub RemovePRNDSErrorRecord()
    Sub PostPreStampa(ByVal aLista As ArrayList)
    Sub UpdateQuickDatasetFile()

End Interface

