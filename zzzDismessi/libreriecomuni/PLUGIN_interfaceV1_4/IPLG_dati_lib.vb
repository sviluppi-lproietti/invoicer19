Imports System.Windows.Forms

Public Interface IPLG_dati_lib

    ' Propriet� 
    Property MessaggiAvanzamento() As String()
    Property CurrentRecordNumber() As Integer
    Property LogDSStampa() As DataSet

    ReadOnly Property AlertPlugIn_PRN() As String
    ReadOnly Property AlertPlugIn_SND() As String
    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList

    ReadOnly Property NomePlugIn() As String
    ReadOnly Property VersionePlugIn() As String

    ' ************************************************************************** '
    ' Dati del dataset che serve per la visualizzazione guida nelle applicazioni '
    ' INVOICER ed INVOICER_SENDER.                                               '
    ' ************************************************************************** '
    ReadOnly Property QuickDataset() As DataSet
    ReadOnly Property QuickDataset_MainTableName() As String
    ReadOnly Property QuickDataset_MainTableKey() As String

    ' ************************************************************************** '
    ' Campo che linka il QUICKDATASET con il FULLDATASET in base alla tipologia  '
    ' di valori caricati.                                                        '
    ' ************************************************************************** '
    ReadOnly Property LinkField_QuickDS_FullDS() As String

    ' ************************************************************************** '
    ' Dati del dataset che serve per la stampa e l'invio delle email nelle appli '
    ' cazioni INVOICER ed INVOICER_SENDER.                                       '
    ' ************************************************************************** '
    ReadOnly Property FullDataset() As DataSet
    ReadOnly Property FullDataSet_MainTableName() As String
    ReadOnly Property FullDataset_MainTableKey() As String

    ' ************************************************************************** '
    ' Questo codice indica la tipologia di caricamento dei dati che bisogna effe ' 
    ' ttuare. I moduli per ciascun plug in sono liberi tra i codice 1 e 900, men '
    ' tre lo 0 e quelli tra 901 e 999 sono fissi.                                '
    '   0 - Dati del Quickdataset                                                '
    ' 999 - Dati per l'invio tramite e-mail del documento                        '
    ' ************************************************************************** '
    WriteOnly Property CodiceModulo() As Integer

    ReadOnly Property ResultAL() As ArrayList
    ReadOnly Property DataSendByMail() As Xml.XmlNode


    WriteOnly Property FeedString() As String
    WriteOnly Property FiltroSelezione() As String                      ' Indica con quale campo del record del Quickdataset indica se � selezionabile.
    WriteOnly Property SessioneFld() As String
    WriteOnly Property SezioneDati() As Xml.XmlNode
    WriteOnly Property SessioneDatiFissiFileName() As String
    WriteOnly Property SessioneTrasformazioniFileName() As String
    WriteOnly Property SessioneDatiPath() As String
    WriteOnly Property SessioneNuova() As Boolean

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean
    Function ElementiMenuDedicati(ByVal nTipoCaricamento As Integer) As ArrayList
    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String()

    Sub ClearPrintDataset()
    Sub GoToRecordNumber(ByVal nRecord As Integer)
    Sub ImpostaDGVDati(ByVal nTipoCaricamento As Integer, ByVal DGV As DataGridView)
    Sub LoadQuickDataset(ByVal nTipoCaricamento As Integer)
    Sub LoadFullDataset(ByVal nTipoCaricamento As Integer, ByVal nRecordToLoad As Integer)
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)
    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String)
    Sub CreaDataBaseSessione(ByVal DatiSessione As ArrayList)
    Sub RemovePRNDSErrorRecord()
    Sub PostPreStampa(ByVal aLista As ArrayList)
    Sub UpdateQuickDatasetFile()

End Interface

