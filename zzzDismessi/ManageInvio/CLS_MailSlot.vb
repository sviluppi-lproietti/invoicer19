Public Class CLS_MailSlot
    Inherits ArrayList

    ReadOnly Property InAttesa() As Boolean

        Get
            Return ContaStatus(1) > 0
        End Get

    End Property

    ReadOnly Property DaInviare() As Boolean

        Get
            Return ContaStatus(2) > 0
        End Get

    End Property

    ReadOnly Property Inviate() As Boolean

        Get
            Return ContaStatus(5) > 0 Or ContaStatus(6) > 0 Or ContaStatus(7) > 0
        End Get

    End Property

    Public Function ContaStatus(ByVal nStatus As Integer) As Integer
        Dim nCount As Integer

        nCount = 1
        For Each ms As GestioneEmail In Me
            If ms.Status = nStatus Then
                nCount += 1
            End If
        Next

        Return nCount

    End Function

    Public Sub PulisciCompletati()
        Dim ms As GestioneEmail
        Dim i As Integer

        While i < Me.Count
            ms = Me(i)
            If ms.Status > 4 Then
                Me.RemoveAt(i)
                i -= 1
            End If
            i += 1
        End While

    End Sub

End Class
