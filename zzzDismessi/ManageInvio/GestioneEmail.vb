Imports System.Net.Mail
Imports TipiComuni.FC_utilita

Public Class GestioneEmail

    Private _xnDatiMail As Xml.XmlNode
    Private _GestData As ManageDataset.GestioneDataset
    Private _GruppoInvio As Integer
    Private _MailDiTest As Boolean
    Private _xdMailData As Xml.XmlDocument
    Private _MailAddressTo As String
    Private _MailAddressFrom As String
    Private _xnDatiServer As Xml.XmlNode
    Private _OrarioServizio As DateTime
    Private _HtmlBody As Boolean

    Private Const sSchema = "http://schemas.microsoft.com/cdo/configuration/"
    Public Const itm_conditions = "Conditions"

    Sub New(ByVal cMailDatiFileName As String)

        _GruppoInvio = 1
        _xdMailData = New Xml.XmlDocument
        _xdMailData.Load(cMailDatiFileName)

    End Sub

    Public Property DatatToSend() As ManageDataset.GestioneDataset

        Get
            Return _GestData
        End Get
        Set(ByVal value As ManageDataset.GestioneDataset)
            _GestData = value
        End Set

    End Property

    Private ReadOnly Property MailSubject() As String

        Get
            Dim cObject As String

            cObject = _xdMailData.SelectSingleNode("MailData/MailSubject").InnerText
            If _MailDiTest Then
                cObject = String.Concat("MAIL DI TEST NON CONSIDERARE: ", cObject)
            End If
            If _xnDatiMail.SelectSingleNode("NumeroParti").InnerText > 1 Then
                cObject = String.Concat(cObject, " (Parte ", _xnDatiMail.SelectSingleNode("Parte").InnerText, "/", _xnDatiMail.SelectSingleNode("NumeroParti").InnerText, ")")
            End If
            Return _GestData.ExtractValue(cObject, "", 0, "")
        End Get

    End Property

    Private ReadOnly Property MailBody() As String

        Get
            Dim cBodyHtml As String
            Dim cBody As String
            Dim nStart As Integer
            Dim nEnd As Integer

            If _xdMailData.SelectSingleNode("MailData/MailBody") Is Nothing Then
                cBody = ""
            Else
                cBody = BuildText(_xdMailData.SelectSingleNode("MailData/MailBody"))
            End If
            If _MailDiTest And MailBodyType = "plaintext" Then
                cBody = String.Concat("***********************************************************", vbCr, vbLf, _
                                      "******** MAIL DI TEST NON TENERE IN CONSIDERAZIONE ********", vbCr, vbLf, _
                                      "***********************************************************", vbCr, vbLf, _
                                      cBody, vbCr, vbLf, _
                                      "***********************************************************", vbCr, vbLf, _
                                      "******** MAIL DI TEST NON TENERE IN CONSIDERAZIONE ********", vbCr, vbLf, _
                                      "***********************************************************")
            End If
            If _MailDiTest And MailBodyType = "html" Then
                If cBody.Contains("__lt__body__gt__") And cBody.Contains("__lt__/body__gt__") Then
                    nStart = cBody.IndexOf("__lt__body__gt__") + 16
                    nEnd = cBody.IndexOf("__lt__/body__gt__")
                    cBodyHtml = cBody.Substring(nStart, nEnd - nStart)
                    cBodyHtml = String.Concat("***********************************************************", vbCr, vbLf, _
                                      "******** MAIL DI TEST NON TENERE IN CONSIDERAZIONE ********", vbCr, vbLf, _
                                      "***********************************************************", vbCr, vbLf, _
                                      cBodyHtml, vbCr, vbLf, _
                                      "***********************************************************", vbCr, vbLf, _
                                      "******** MAIL DI TEST NON TENERE IN CONSIDERAZIONE ********", vbCr, vbLf, _
                                      "***********************************************************")
                    cBody = cBody.Remove(nStart, nEnd - nStart)
                    cBody = cBody.Insert(nStart, cBodyHtml)
                End If
            End If

            If MailBodyType = "html" Then
                nStart = cBody.IndexOf("__lt__body__gt__") + 16
                nEnd = cBody.IndexOf("__lt__/body__gt__")
                If nStart > -1 And nEnd > -1 And nEnd > nStart Then
                    cBodyHtml = cBody.Substring(nStart, nEnd - nStart)
                    cBodyHtml = cBodyHtml.Replace(String.Concat(vbCr, vbLf), "<BR>")
                    cBodyHtml = cBodyHtml.Replace("{B}", "<B>")
                    cBodyHtml = cBodyHtml.Replace("{/B}", "</B>")
                    cBodyHtml = cBodyHtml.Replace("{U}", "<U>")
                    cBodyHtml = cBodyHtml.Replace("{/U}", "</U>")
                    cBody = cBody.Remove(nStart, nEnd - nStart)
                    cBody = cBody.Insert(nStart, cBodyHtml)
                Else
                    cBodyHtml = cBody
                    cBodyHtml = cBodyHtml.Replace(String.Concat(vbCr, vbLf), "<BR>")
                    cBodyHtml = cBodyHtml.Replace("{B}", "<B>")
                    cBodyHtml = cBodyHtml.Replace("{/B}", "</B>")
                    cBodyHtml = cBodyHtml.Replace("{U}", "<U>")
                    cBodyHtml = cBodyHtml.Replace("{/U}", "</U>")
                    cBody = cBodyHtml
                End If

                cBody = cBody.Replace("__lt__", "<")
                cBody = cBody.Replace("__gt__", ">")

            End If
                Return cBody
        End Get

    End Property

    Private ReadOnly Property MailBodyType() As String

        Get
            Return _xdMailData.SelectSingleNode("MailData/MailBodyType").InnerText
        End Get

    End Property

    Public WriteOnly Property MailDiTest() As Boolean

        Set(ByVal value As Boolean)
            _MailDiTest = value
        End Set

    End Property

    Public Property Status() As Integer

        Get
            Return _xnDatiMail.SelectSingleNode("StatoInvio").InnerText
        End Get
        Set(ByVal value As Integer)
            _xnDatiMail.SelectSingleNode("StatoInvio").InnerText = value
        End Set

    End Property

    Public WriteOnly Property DatiServer() As Xml.XmlNode

        Set(ByVal value As Xml.XmlNode)
            _xnDatiServer = value
        End Set

    End Property

    Public WriteOnly Property MailAddressFrom() As String

        Set(ByVal value As String)
            _MailAddressFrom = value
        End Set

    End Property

    Public ReadOnly Property MailAddressTo() As String

        Get
            Return _MailAddressTo
        End Get

    End Property

    Public ReadOnly Property OrarioServizio() As DateTime

        Get
            Return _OrarioServizio
        End Get

    End Property

    Public Sub SendMail()
        Dim cSMTPServer As String
        Dim nSMTPPorta As Integer
        Dim lSMTPAuth As Boolean
        Dim cSMTPUsername As String
        Dim cSMTPPassword As String
        Dim lSMTPProtect As Boolean
        Dim objMail As Object
        Dim cBody As String
        Dim lOtherToSend As Boolean

        Try
            _OrarioServizio = Now
            Status = 3
            With _xnDatiServer
                cSMTPServer = .SelectSingleNode("ServerName").InnerText
                nSMTPPorta = .SelectSingleNode("ServerPort").InnerText
                lSMTPAuth = .SelectSingleNode("ServerAuth").InnerText
                cSMTPUsername = .SelectSingleNode("UserId").InnerText
                cSMTPPassword = .SelectSingleNode("Password").InnerText
                lSMTPProtect = .SelectSingleNode("Protected").InnerText
            End With
            objMail = CreateObject("CDO.Message")

            With objMail.Configuration.Fields
                .Item(sSchema & "smtpserver") = cSMTPServer
                .Item(sSchema & "smtpserverport") = nSMTPPorta
                .Item(sSchema & "sendusing") = 2
                If lSMTPAuth Then
                    .Item(sSchema & "smtpauthenticate") = 1
                    .Item(sSchema & "sendusername") = cSMTPUsername
                    .Item(sSchema & "sendpassword") = cSMTPPassword
                Else
                    .Item(sSchema & "smtpauthenticate") = 0
                End If

                .Item(sSchema & "smtpusessl") = lSMTPProtect
                .Update()
            End With

            _HtmlBody = True
            With objMail
                .From = _MailAddressFrom
                .To = _MailAddressTo
                .Subject = MailSubject
                If MailBodyType = "html" Then
                    .htmlBody = MailBody
                ElseIf MailBodyType = "plaintext" Then
                    .textBody = MailBody
                End If
                _GruppoInvio = 1
                lOtherToSend = False
                For Each xnTmp As Xml.XmlNode In _xnDatiMail.SelectSingleNode("Documenti").ChildNodes
                    .AddAttachment(xnTmp.InnerText)
                Next
                .send()
            End With
            If lOtherToSend Then
                Status = 4
            Else
                Status = 5
            End If
            _OrarioServizio = Now
        Catch ex As Exception
            Status = 6
        End Try

    End Sub

    Private Function BuildText(ByVal xnItem As Xml.XmlNode) As String
        Dim cText As String
        Dim xnTmp1 As Xml.XmlNode
        Dim xnTmp2 As Xml.XmlNode
        Dim xnWork As Xml.XmlNode
        Dim cValueTmp As String

        cText = xnItem.InnerText
        For Each xnTmp1 In xnItem.SelectNodes("item")
            If Not xnTmp1.SelectSingleNode("value") Is Nothing Then
                xnTmp2 = xnTmp1.SelectSingleNode("value").Clone
                xnTmp1.RemoveChild(xnTmp1.SelectSingleNode("value"))
                xnWork = _xdMailData.CreateElement("values")
                xnWork.InnerXml = String.Concat("<valuesItem>", xnTmp2.OuterXml, "</valuesItem>")
                xnTmp1.AppendChild(xnWork)
            End If

            cValueTmp = ""
            If _GestData.EvaluateCondition(xnTmp1, "") Then
                cValueTmp = _GestData.ExtractValues(xnTmp1, "", 0, "")
            End If
            cText = cText.Replace(xnTmp1.InnerText, cValueTmp)
        Next
        cText = _GestData.ExtractValue(cText, "", 0, "")
        cText = cText.Replace("{LF}", String.Concat(vbCr, vbLf))
        Return cText

    End Function

    Public WriteOnly Property DatiMail() As Xml.XmlNode

        Set(ByVal value As Xml.XmlNode)
            _xnDatiMail = value
            _MailAddressTo = _xnDatiMail.SelectSingleNode("ToAddress").InnerText
            Status = 1
        End Set

    End Property

    Public ReadOnly Property CodiceQD() As Integer

        Get
            Return _xnDatiMail.SelectSingleNode("CodiceQD").InnerText
        End Get

    End Property

End Class
