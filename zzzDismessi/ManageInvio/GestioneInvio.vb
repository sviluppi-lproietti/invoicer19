Public Class GestioneInvio

    Private _MailAddressTest As String
    Private _InviiSimultanei As Integer
    Private _IntervalloTraInvii As Integer
    Private _MaximumTime As Integer
    Private _FirstRecordSended As Integer
    Private _LastRecordSended As Integer
    Private _InvioDiTest As Boolean
    Private _FattureDaAccodare As Integer
    Private _FattureAccodate As Integer
    Private _ListaMail As Xml.XmlDocument
    Private _InvioEseguito As Boolean

    Public Property FattureDaAccodare() As Integer

        Get
            Return _FattureDaAccodare
        End Get
        Set(ByVal value As Integer)
            _FattureDaAccodare = value
        End Set

    End Property

    Public Property FattureAccodate() As Integer

        Get
            Return _FattureAccodate
        End Get
        Set(ByVal value As Integer)
            _FattureAccodate = value
        End Set

    End Property

    Public ReadOnly Property MailDaInviareTot() As Integer

        Get
            Return _ListaMail.SelectNodes("MailList/MailItem").Count
        End Get

    End Property

    Public ReadOnly Property MailDaInviare() As Integer

        Get
            Return _ListaMail.SelectNodes("MailList/MailItem/StatoInvio[.='0']").Count
        End Get

    End Property

    Public ReadOnly Property MailInviate() As Integer

        Get
            Return _ListaMail.SelectNodes("MailList/MailItem/StatoInvio[.>'3']").Count
        End Get

    End Property

    Public Property NumeroSlot() As Integer

        Get
            Return 3
        End Get
        Set(ByVal value As Integer)

        End Set

    End Property

    Public Property MaximumTime() As Integer

        Get
            Return _MaximumTime
        End Get
        Set(ByVal value As Integer)
            _MaximumTime = value
        End Set

    End Property

    Public Property InviiSimultanei() As Integer

        Get
            Return _InviiSimultanei
        End Get
        Set(ByVal value As Integer)
            _InviiSimultanei = value
        End Set

    End Property

    Public Property IntervalloTraInvii() As Integer

        Get
            Return _IntervalloTraInvii
        End Get
        Set(ByVal value As Integer)
            _IntervalloTraInvii = value
        End Set

    End Property

    Property FirstRecordSended() As Integer

        Get
            Return _FirstRecordSended
        End Get
        Set(ByVal value As Integer)
            _FirstRecordSended = value
        End Set

    End Property

    Property LastRecordSended() As Integer

        Get
            Return _LastRecordSended
        End Get
        Set(ByVal value As Integer)
            _LastRecordSended = value
        End Set

    End Property

    Public ReadOnly Property MaxSizeAttach() As Long

        Get
            Return 1500000
        End Get

    End Property

    Public ReadOnly Property DelayStessoDominio() As Integer

        Get
            Return 30
        End Get

    End Property

    Public Property MailAddressTest() As String

        Get
            Return _MailAddressTest
        End Get
        Set(ByVal value As String)
            _MailAddressTest = value
        End Set

    End Property

    Public Property InvioDiTest() As Boolean

        Get
            Return _InvioDiTest
        End Get
        Set(ByVal value As Boolean)
            _InvioDiTest = value
        End Set

    End Property

    Public Property ListaMail() As Xml.XmlDocument

        Get
            Return _ListaMail
        End Get
        Set(ByVal value As Xml.XmlDocument)
            _ListaMail = value
        End Set

    End Property

    Public Sub New()

        _FattureAccodate = 0

    End Sub

    Property InvioEseguito() As Boolean

        Get
            Return _InvioEseguito
        End Get
        Set(ByVal value As Boolean)
            _InvioEseguito = value
        End Set

    End Property

End Class
