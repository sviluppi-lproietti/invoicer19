﻿Imports ISC.LibrerieComuni.InvoicerDomain.Impl


Public Class CLS_repositorydati

    '
    ' Contiene il nome della tabella principale della base dati contenuta nel dataset.
    '
    Public Property MainTableName As String

    Public Property DBDatiPlugIn As DataSet

        Get
            Return _DBDatiPlugIn
        End Get
        Set(ByVal value As DataSet)
            _DBDatiPlugIn = value
            If Not Me.IsEmpty Then
                SetUpRecordNumber(MainTableName)
            End If
        End Set

    End Property

    '
    ' Contiene il nome del file dei dati fissi.
    '
    Public Property DatiFissiFileName As String

    Private _DBDatiPlugIn As DataSet
    Private _RecordMaster As ArrayList
    Private _RecordMasterIdx As Integer
    Private _RecordMasterDepth As Integer

    Public Property ElencoPagineDocumenti As Dictionary(Of Integer, ContatoriPagineEntity)
    Public Property MainLinkToQD As String

    Public ReadOnly Property MainTable() As DataTable

        Get
            Return DBDatiPlugIn.Tables(MainTableName)
        End Get

    End Property

    Public ReadOnly Property RecordMasterRow(Optional ByVal nValue As Integer = -1) As DataRow

        Get
            If nValue = -1 Then
                Return _RecordMaster(_RecordMasterIdx)(1)
            Else
                Return _RecordMaster(nValue)(1)
            End If
        End Get

    End Property

    Public ReadOnly Property RecordMasterTableName() As String

        Get
            Return _RecordMaster(_RecordMasterIdx)(0)
        End Get

    End Property

    Public Property RecordNumber As Dictionary(Of String, Integer)

    ReadOnly Property StatoRaccolta() As Integer

        Get
            Return Me.MainTable.Rows(RecordNumber(MainTableName))("DEF_raccolta")
        End Get

    End Property

    Public ReadOnly Property ContatoriPagine(Optional ByVal nDocId As Integer = -1) As ContatoriPagineEntity

        Get
            If nDocId = -1 Then
                Return Me.ElencoPagineDocumenti(Me.RecordMasterRow(0).Item(Me.MainLinkToQD))
            Else
                Return Me.ElencoPagineDocumenti(nDocId)
            End If
        End Get

    End Property

    Public Sub New()

        ElencoPagineDocumenti = New Dictionary(Of Integer, ContatoriPagineEntity)

    End Sub

    Public Sub New(ByVal cm As Integer, ByVal cMainTable As String, ByVal cDatiFissiFileName As String, ByVal cLinkField_QuickDS_FullDS As String)


        MainTableName = cMainTable
        DatiFissiFileName = cDatiFissiFileName
        MainLinkToQD = cLinkField_QuickDS_FullDS
        RecordNumber = New Dictionary(Of String, Integer)

    End Sub

    Public Sub SetUpDB(ByVal ds As DataSet, ByVal cMainTable As String, ByVal cDatiFissiFileName As String, ByVal cLinkField_QuickDS_FullDS As String)

        MainTableName = cMainTable
        DatiFissiFileName = cDatiFissiFileName
        MainLinkToQD = cLinkField_QuickDS_FullDS
        RecordNumber = New Dictionary(Of String, Integer)
        'SetUpRecordNumber(MainTableName)
        DBDatiPlugIn = ds

    End Sub

    Public ReadOnly Property RecordFullDatasetMainTable As Integer

        Get
            Return MainTable.Rows.Count
        End Get

    End Property

    Public ReadOnly Property MainTableCurrentRecord As DataRow

        Get
            Return MainTable.Rows(RecordNumber(MainTableName))
        End Get

    End Property

    Public Function IsEmpty() As Boolean

        Return MainTable.Rows.Count = 0

    End Function

    Public Sub SetUpRecordNumber(ByVal cTable As String, Optional ByVal lExist As Boolean = True)

        Try
            If Not RecordNumber.ContainsKey(cTable) Then
                RecordNumber.Add(cTable, 0)
            End If
            If lExist Then
                RecordNumber(cTable) = 0
                SetupMasterRecord(cTable = MainTableName)
            End If
            'If lExist Then
            '    If RecordNumber.ContainsKey(cTable) Then
            '        RecordNumber(cTable) = 0
            '    Else
            '        RecordNumber.Add(cTable, 0)
            '    End If
            '    SetupMasterRecord(cTable = MainTableName)
            'Else
            '    If Not RecordNumber.ContainsKey(cTable) Then
            '        RecordNumber.Add(cTable, 0)
            '    End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub SetupMasterRecord(ByVal lNewMasterRecord As Boolean)

        If lNewMasterRecord And Me.MainTable IsNot Nothing Then
            _RecordMaster = New ArrayList
            AddRecordMaster(New Object() {Me.MainTableName, Me.MainTable.Rows(RecordNumber(MainTableName))})
            _RecordMasterIdx = 0
            _RecordMasterDepth = 0
        End If

    End Sub

    Public Sub AddRecordMaster(ByVal aOggetto As Object())

        _RecordMaster.Add(aOggetto)

    End Sub

    Public Sub RemoveRecordMaster()

        _RecordMaster.RemoveAt(_RecordMaster.Count - 1)

    End Sub

    Public Sub AvanzaRecordMaster(Optional ByVal lFF As Boolean = True)

        If lFF Then
            _RecordMasterDepth += 1
            If _RecordMasterIdx < _RecordMasterDepth - 1 Then
                _RecordMasterIdx += 1
            End If
        Else
            _RecordMasterDepth -= 1
            If (_RecordMasterIdx > 0) And _RecordMasterIdx = _RecordMasterDepth Then
                _RecordMasterIdx -= 1
            End If

        End If

    End Sub

    Public Sub MoveRecordNumber(ByVal cTable As String)

        If Not RecordNumber.ContainsKey(cTable) Then
            RecordNumber.Add(cTable, 0)
        End If
        RecordNumber(cTable) += 1
        SetupMasterRecord(cTable = MainTableName)

    End Sub

    Public Function GetRelationName(ByVal cTable As String) As String
        Dim cRtn As String

        cRtn = ""
        If cTable > "" Then
            For Each drel_wrk As DataRelation In DBDatiPlugIn.Relations
                If drel_wrk.ParentTable.TableName = RecordMasterTableName And drel_wrk.ChildTable.TableName = cTable Then
                    cRtn = drel_wrk.RelationName
                End If
            Next
            SetUpRecordNumber(MainTableName, False)
            SetUpRecordNumber(cTable, False)
        End If
        Return cRtn

    End Function

End Class
