﻿Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Public Class CLS_accessodati

    Public Property RepositoryDati As CLS_repositorydati

    Const cnt_null_value = "&^^&NULL&^^&"
    Const cnt_not_null_value = "&^^&NOTNULL&^^&"

    Public Sub New()

        RepositoryDati = New CLS_repositorydati()

    End Sub

    Public Function DMFillValues(ByVal ValoriDaRecuperare As ArrayList) As ArrayList
        Dim aValue As ArrayList

        aValue = New ArrayList
        For Each cValue As String In ValoriDaRecuperare
            aValue.Add(ExtractValue(cValue, "", 0, ""))
        Next
        Return aValue

    End Function

#Region "Procedure per la stampa del codice a Barre GS1-128"

    Public Function AIFillValues(ByVal xmlItems As Xml.XmlNode) As ArrayList
        Dim xnWork As Xml.XmlNode
        Dim xnTmp As Xml.XmlNode
        Dim nNumValue As Integer
        Dim aAIValue As ArrayList

        Try
            aAIValue = New ArrayList
            If Not xmlItems.SelectSingleNode("value") Is Nothing Then
                xnTmp = xmlItems.SelectSingleNode("value").Clone
                xmlItems.RemoveChild(xmlItems.SelectSingleNode("value"))
                xnWork = xmlItems.OwnerDocument.CreateElement("values")
                xnWork.InnerXml = String.Concat("<valuesItem>", xnTmp.OuterXml, "</valuesItem>")
                xmlItems.AppendChild(xnWork)
            End If
            If NodeExist(xmlItems, "values") Then
                nNumValue = 0
                For Each xnTmp In xmlItems.SelectNodes("values/valuesItem")
                    If EvaluateCondition(xnTmp, "") Then
                        aAIValue = AIFillValue(xnTmp.SelectNodes("value/ai"))
                        nNumValue += 1
                    End If
                Next
            End If
        Catch ex As Exception
            aAIValue = New ArrayList
            Throw New Exception("Errore nell'estrazione del valore.", Nothing)
        End Try
        If nNumValue < 1 Then
            aAIValue = New ArrayList
            Throw New Exception("Errore Non è stato recuperato alcun valore.", Nothing)
        End If
        If nNumValue > 1 Then
            aAIValue = New ArrayList
            Throw New Exception("Errore è stato recuperato più di un valore.", Nothing)
        End If
        Return aAIValue

    End Function

    Private Function AIFillValue(ByVal xmlItems As Xml.XmlNodeList) As ArrayList
        Dim aValue As New ArrayList
        Dim nValue As Decimal
        Dim cTmp1 As String
        Dim cTmp2 As String
        Dim cTmp3 As String
        Dim ai As String()
        Dim i As Integer

        For Each xmlItem_1 As Xml.XmlNode In xmlItems
            ' Da inserire la decodifica dei valori ricavabili dal db.
            cTmp1 = xmlItem_1.InnerText
            cTmp2 = cTmp1
            cTmp2 = cTmp2.Substring(cTmp2.IndexOf(";") + 1)
            cTmp2 = cTmp2.Substring(0, cTmp2.LastIndexOf(";"))
            cTmp3 = ExtractValue(cTmp2, "", 0, "")
            If cTmp2 <> cTmp3 Then
                cTmp1 = cTmp1.Replace(cTmp2, cTmp3)
            End If

            ai = cTmp1.Split(";")

            ' Per l'AI 415 non c'è necessità di modifiche al valore
            If ai(0) = 415 Then
            End If
            If ai(0) = 8020 Then
                If ai(2) <> "--" Then
                    If ai(1).Length > ai(2) Then
                        ai(1) = ai(1).Substring(0, ai(2))
                    Else
                        ai(1) = ai(1).PadLeft(ai(2), "0")
                    End If
                End If
            End If
            If ai(0).Substring(0, 3) = 390 Then
                nValue = CDec(ai(1))
                For i = 1 To ai(0).Substring(3, 1)
                    nValue = nValue * 10
                Next
                ai(1) = Int(nValue).ToString
                If ai(2) <> "--" Then
                    If ai(1).Length > ai(2) Then
                        ai(1) = ai(1).Substring(0, ai(2))
                    Else
                        ai(1) = ai(1).PadLeft(ai(2), "0")
                    End If
                End If
            End If
            aValue.Add(ai)
        Next
        Return aValue

    End Function

#End Region

#Region "Raccolta delle procedure per l'estrazione del valore da stampare"

    Public Function ExtractValues(ByVal xnValues As Xml.XmlNode, ByVal cFilterItem As String, ByVal nFormatValue As Integer, ByVal _RelationName As String, Optional ByVal nRecord As Integer = -1) As String
        Dim cDefaultRtnValue As String
        Dim xnTmp As Xml.XmlNode
        Dim nNumValue As Integer
        Dim cRtnValue As String

        cRtnValue = ""
        cDefaultRtnValue = ""
        Try
            nNumValue = 0
            For Each xnTmp In xnValues.SelectNodes("values/valuesItem")
                If IsConditionDefualtValue(xnTmp) Then
                    cDefaultRtnValue = ExtractValue(xnTmp.SelectSingleNode("value").InnerText, cFilterItem, nFormatValue, _RelationName, nRecord)
                ElseIf EvaluateCondition(xnTmp, _RelationName) Then
                    cRtnValue = ExtractValue(xnTmp.SelectSingleNode("value").InnerText, cFilterItem, nFormatValue, _RelationName, nRecord)
                    nNumValue += 1
                End If
            Next
        Catch ex As Exception
            Throw New Exception("ExtractValues - Errore nell'estrazione del valore.", Nothing)
        Finally
            If nNumValue < 1 Then
                cRtnValue = cDefaultRtnValue
            End If
            If nNumValue > 1 Then
                cRtnValue = ""
                Throw New Exception("ExtractValues - Errore è stato recuperato più di un valore.", Nothing)
            End If
            While cRtnValue.IndexOf("{LF}") > -1
                cRtnValue = cRtnValue.Replace("{LF}", vbCrLf)
            End While
        End Try
        Return cRtnValue

    End Function

    Public Function ExtractValue(ByVal cValueItem As String, ByVal cFilterItem As String, ByVal nFormatValue As Integer, ByVal _RelationName As String, Optional ByVal nRecord As Integer = -1) As String
        Dim cValueTmp As String
        Dim cFirstValue As String
        Dim cField As String
        Dim cTable As String
        Dim cFormat As String
        Dim nStart As Integer
        Dim nEnd As Integer
        Dim nDecimal As Integer
        Dim aData As String()
        Dim i As Integer
        Dim cErrorTmp As String
        Dim cTmp1 As String
        Dim nPos As Integer
        Dim lFormatted As Boolean
        Dim nTmp As Decimal
        Dim dTmp As DateTime
        Dim lExit As Boolean
        Dim cFiller As String
        Dim nLengt As Integer
        Dim nStart1 As String

        Try
            nStart = cValueItem.IndexOf("{\")
            While nStart > -1
                nEnd = cValueItem.IndexOf("/}")

                ' Recupero il valore che dovrà essere restituito
                cValueTmp = cValueItem.Substring(nStart, nEnd - nStart + 2).Replace("{\", "").Replace("/}", "")
                aData = cValueTmp.Split(";")
                cFirstValue = ""
                cField = ""
                cFormat = ""
                nDecimal = -1
                _RelationName = ""
                For i = 0 To aData.Length - 1
                    Select Case i
                        Case Is = 0
                            cFirstValue = aData(i)
                        Case Is = 1
                            cField = aData(i)
                        Case Is = 2
                            If IsNumeric(aData(i)) Then
                                nDecimal = aData(i)
                            End If
                        Case Is = 3
                            cFormat = aData(i)
                            If cFormat = "CLBOLLETTINO" Or cFormat = "ZEROIZEIMP" Then
                                nDecimal = 2
                            End If
                    End Select
                Next

                Select Case cFirstValue
                    Case Is = "PAGENUMBER"
                        '
                        ' Il progressivo delle pagine del documento è sempre inferiore di 1 
                        ' in quanto conteggia le pagine stampate e quinndi viene aumentato alla 
                        ' fine della stampa della pagina
                        '
                        cValueTmp = RepositoryDati.ContatoriPagine.PagineDocumento + 1
                    Case Is = "PAGETOTAL"
                        cValueTmp = RepositoryDati.ContatoriPagine.PagineTotaliDocumento
                    Case Else
                        cTable = cFirstValue
                        cValueTmp = RecuperaValore(cTable, cField, _RelationName, "", nRecord, "", cValueItem)
                End Select

                If nDecimal > -1 Then
                    nTmp = cValueTmp
                    For p As Integer = 1 To nDecimal
                        nTmp = nTmp * 10
                    Next
                    nTmp = Int(nTmp)
                    For p As Integer = 1 To nDecimal
                        nTmp = nTmp / 10
                    Next
                    cValueTmp = nTmp
                End If

                If cFormat > "" Then
                    If cFormat = "CLBOLLETTINO" Then
                        cValueTmp = Format(nTmp, "#,##0.00")
                        cValueTmp = String.Concat("00000000", cValueTmp.ToString.Replace(",", "+").Replace(".", ""))
                        cValueTmp = cValueTmp.Substring(cValueTmp.Length - 11)
                        lFormatted = True
                    ElseIf cFormat = "ZEROIZEIMP" And Not lFormatted Then
                        cValueTmp = Format(nTmp, "#,##0.00")
                        cValueTmp = String.Concat("0000000000", cValueTmp.ToString.Replace(",", "").Replace(".", ""))
                        cValueTmp = cValueTmp.Substring(cValueTmp.Length - 10)
                        lFormatted = True
                    ElseIf cFormat.StartsWith("SUB") And Not lFormatted Then
                        cFormat = cFormat.Replace("SUB", "")
                        cTmp1 = cFormat
                        nLengt = cTmp1.Substring(cTmp1.LastIndexOf("^") + 1)
                        If cFormat.StartsWith("L^") Then
                            cFormat = cFormat.Replace("L^", "")
                            cFiller = cFormat.Substring(0, cFormat.IndexOf("^"))
                            cFormat = cFormat.Substring(cFormat.IndexOf("^") + 1)
                            nStart1 = cFormat.Substring(0, cFormat.IndexOf("^"))
                            cFormat = cFormat.Substring(cFormat.IndexOf("^") + 1)
                            If cValueTmp.Length > nLengt Then
                                cValueTmp = cValueTmp.Substring(nStart1, nLengt)
                            ElseIf cValueTmp.Length < nLengt Then
                                cValueTmp = cValueTmp.PadLeft(nLengt, cFiller)
                            End If
                        End If
                        If cFormat.StartsWith("R^") Then
                            cFormat = cFormat.Replace("R^", "")
                            cFiller = cFormat.Substring(0, cFormat.IndexOf("^"))
                            cFormat = cFormat.Substring(cFormat.IndexOf("^") + 1)
                            nStart1 = cFormat.Substring(0, cFormat.IndexOf("^"))
                            cFormat = cFormat.Substring(cFormat.IndexOf("^") + 1)
                            If cValueTmp.Length > nLengt Then
                                cValueTmp = cValueTmp.Substring(nStart1, nLengt)
                            ElseIf cValueTmp.Length < nLengt Then
                                cValueTmp = cValueTmp.PadRight(nLengt, cFiller)
                            End If
                        End If
                        lFormatted = True
                    ElseIf cFormat.StartsWith("PAD") And Not lFormatted Then
                        cFormat = cFormat.Replace("PAD", "")
                        cTmp1 = cFormat
                        cTmp1 = cTmp1.Substring(cTmp1.LastIndexOf("^") + 1)
                        If cFormat.StartsWith("L^") Then
                            cFormat = cFormat.Replace("L^", "")
                            cFormat = cFormat.Substring(0, cFormat.LastIndexOf("^"))
                            cValueTmp = cValueTmp.PadLeft(cTmp1, cFormat)
                        End If
                        If cFormat.StartsWith("R^") Then
                            cFormat = cFormat.Replace("R^", "")
                            cFormat = cFormat.Substring(0, cFormat.LastIndexOf("^"))
                            cValueTmp = cValueTmp.PadRight(cTmp1, cFormat)
                        End If
                        lFormatted = True
                    ElseIf cFormat.StartsWith("TRIM") And Not lFormatted Then
                        cFormat = cFormat.Replace("TRIM", "")
                        cValueTmp = cValueTmp.Trim()
                        lFormatted = True
                    ElseIf (cFormat = "DDMMYY" Or cFormat = "YYYYMMDD" Or cFormat = "DDMMYYYY" Or cFormat = "YY") And Not lFormatted Then
                        If IsDate(cValueTmp) Then
                            dTmp = CDate(cValueTmp)
                            If cFormat = "DDMMYY" Then
                                cValueTmp = dTmp.Day.ToString.PadLeft(2, "0")
                                cValueTmp = String.Concat(cValueTmp, dTmp.Month.ToString.PadLeft(2, "0"))
                                cValueTmp = String.Concat(cValueTmp, dTmp.Year.ToString.Substring(2, 2))
                            ElseIf cFormat = "DDMMYYYY" Then
                                cValueTmp = dTmp.Day.ToString.PadLeft(2, "0")
                                cValueTmp = String.Concat(cValueTmp, dTmp.Month.ToString.PadLeft(2, "0"))
                                cValueTmp = String.Concat(cValueTmp, dTmp.Year.ToString)
                            ElseIf cFormat = "YYYYMMDD" Then
                                cValueTmp = dTmp.Year.ToString
                                cValueTmp = String.Concat(cValueTmp, dTmp.Month.ToString.PadLeft(2, "0"))
                                cValueTmp = String.Concat(cValueTmp, dTmp.Day.ToString.PadLeft(2, "0"))
                            ElseIf cFormat = "YY" Then
                                cValueTmp = dTmp.Year.ToString.Substring(2, 2)
                            End If
                        Else
                            cValueTmp = " ".PadLeft(cFormat.Length, " ")
                        End If
                        lFormatted = True
                    ElseIf cFormat = "NOPOINT" Then
                        For i = 1 To nDecimal
                            nTmp *= 10
                        Next
                        cValueTmp = nTmp.ToString()
                    ElseIf cFormat = "0000000dd" Then
                        nTmp = nTmp * 10 * 10
                        cFormat = cFormat.Replace("dd", "00")
                        cValueTmp = Format(nTmp, cFormat)
                    Else
                        cValueTmp = Format(nTmp, cFormat)
                    End If
                End If
                cValueItem = String.Concat(cValueItem.Substring(0, nStart), cValueTmp, cValueItem.Substring(nEnd + 2))
                nStart = cValueItem.IndexOf("{\")
            End While
            Select Case nFormatValue
                Case Is = 1
                    If cValueItem.Length > 1 Then
                        cValueItem = String.Concat(cValueItem(0).ToString.ToUpper, cValueItem.ToLower.Substring(1, cValueItem.Length - 1))
                    Else
                        cValueItem = cValueItem.ToUpper
                    End If
                Case Is = 2
                    cValueItem = cValueItem.ToUpper
                Case Is = 3
                    cValueItem = cValueItem.ToLower
                Case Is = 4
                    While cValueItem.IndexOf("  ") > 0
                        cValueItem = cValueItem.Replace("  ", " ")
                    End While
                Case Is = 5
                    If cValueItem.Trim.Length > 0 Then
                        cValueItem = String.Concat(" ", cValueItem.ToLower)
                        nPos = 0
                        lExit = False
                        While Not lExit And (cValueItem.Length > 1)
                            cValueItem = String.Concat(cValueItem.Substring(0, nPos + 1), cValueItem.Substring(nPos + 1, 1).ToUpper, cValueItem.Substring(nPos + 2))
                            nPos = cValueItem.IndexOf(" ", nPos + 1)
                            lExit = nPos = -1
                        End While
                        cValueItem = cValueItem.Substring(1)
                    End If
            End Select
        Catch ex As Exception
            cErrorTmp = String.Concat("Errore su ExtractValue: valore: ", cValueItem, " Filtro: ", cFilterItem, " Formattazione: ", nFormatValue, " Nome relazione: ", _RelationName)
            Throw New Exception(cErrorTmp, ex)
        End Try
        Return cValueItem

    End Function

    Private Function RecuperaValore(ByVal cTable As String, ByVal cField As String, Optional ByVal cRelazione As String = "", Optional ByVal cDefault As String = "", Optional ByVal nRecord As Integer = -1, Optional ByVal cFilteredItem As String = "", Optional ByVal cValueCalc As String = "", Optional ByVal lCheckIsDbNull As Boolean = False)
        Dim cValue As String
        Dim nPos As Integer

        cValue = ""
        If cTable.StartsWith("DF:") Then
            cValue = ExtractValueFromDatiFissi(cTable.Substring(3), cField, cDefault)
        ElseIf cTable = "CalVal" Then
            cValue = CalcolaValore(cField, cValueCalc, cDefault)
        Else
            If (cTable > "") And (cField > "") Then
                cValue = ExtractValueFromTable(cTable, cField, cRelazione, nRecord, cFilteredItem, lCheckIsDbNull)
            End If
        End If
        nPos = cValue.IndexOf("{LF}")
        While nPos > -1
            cValue = cValue.Replace("{LF}", vbCrLf)
            nPos = cValue.IndexOf("{LF}")
        End While
        Return cValue

    End Function

    Private Function CalcolaValore(ByVal cField As String, ByVal cValueCalc As String, ByVal cDefault As String)
        Dim cRtnValue As String
        Dim nTmp As Integer

        cRtnValue = cDefault
        If cField = "CheckDigit1" Then
            cValueCalc = cValueCalc.Substring(0, cValueCalc.IndexOf("{\"))
            If IsNumeric(cValueCalc) Then
                nTmp = cValueCalc Mod 93
                cRtnValue = nTmp.ToString.PadLeft(2, "0")
            End If
        End If

        Return cRtnValue

    End Function

    Private Function ExtractValueFromDatiFissi(ByVal cTable As String, ByVal cField As String, Optional ByVal cDefault As String = "") As String
        Dim xdDatiFissi As Xml.XmlDocument
        Dim xnTemp As Xml.XmlNode
        Dim cValue As String

        cValue = cDefault
        If RepositoryDati.DatiFissiFileName > "" Then
            Try
                xdDatiFissi = New Xml.XmlDocument
                xdDatiFissi.Load(RepositoryDati.DatiFissiFileName)
                xnTemp = xdDatiFissi.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))
                cValue = GetValueFromXML(xnTemp, "valore")
                xdDatiFissi = Nothing
            Catch ex As Exception
                cValue = ""
                Throw New Exception("Errore durante il caricamento dei dati fissi (ExtractValueFromDatiFissi)", ex)
            End Try
        Else
            cValue = ""
            Throw New Exception("Nome del file dei dati fissi non settato (ExtractValueFromDatiFissi)", Nothing)
        End If
        Return cValue

    End Function

    Private Function ExtractValueFromTable(ByVal cTable As String, ByVal cField As String, ByVal cNomeRelazione As String, ByVal nRecord As Integer, ByVal cFilteredItem As String, ByVal lCheckIsDbNull As Boolean) As String
        Dim aData As String()
        Dim cValue As String
        Dim nRow As Integer
        Dim dr As DataRow

        cValue = ""
        dr = Nothing
        Try
            If (cNomeRelazione = "") And cTable <> RepositoryDati.MainTableName Then
                cNomeRelazione = RepositoryDati.GetRelationName(cTable)
            End If
            If cFilteredItem = "" Then
                If (cNomeRelazione = "") Or cTable = RepositoryDati.MainTableName Then
                    If nRecord = -1 Then
                        nRecord = RepositoryDati.RecordNumber(cTable)
                    Else
                        nRecord = 0
                    End If
                    dr = RepositoryDati.DBDatiPlugIn.Tables(cTable).Rows(nRecord)
                Else
                    nRow = RepositoryDati.RecordMasterRow.GetChildRows(cNomeRelazione).Length
                    If nRow > 0 Then
                        dr = RepositoryDati.RecordMasterRow.GetChildRows(cNomeRelazione)(RepositoryDati.RecordNumber(cTable))
                    End If
                End If
                If Not dr Is Nothing Then
                    If lCheckIsDbNull Then
                        If IsDBNull(dr(cField)) Then
                            cValue = cnt_null_value
                        Else
                            cValue = cnt_not_null_value
                        End If
                    Else
                        If Not IsDBNull(dr(cField)) Then
                            cValue = dr.Item(cField)
                        End If
                    End If

                End If
            Else
                aData = cFilteredItem.Split(";")
                If cNomeRelazione = "" Then
                    For Each dr_tbl As DataRow In RepositoryDati.DBDatiPlugIn.Tables(cTable).Rows
                        If (dr_tbl.Item(aData(0)) = aData(2)) And Not IsDBNull(dr_tbl.Item(cField)) Then
                            cValue = dr_tbl.Item(cField)
                        End If
                    Next
                Else
                    cValue = Nothing
                    For Each dr_tbl As DataRow In RepositoryDati.RecordMasterRow.GetChildRows(cNomeRelazione)         '_ds_print.Tables(_PrintDataset_MainTableName).Rows(_cRecordNumber(_PrintDataset_MainTableName)).GetChildRows(cNomeRelazione)
                        If (dr_tbl.Item(aData(0)) = aData(2)) And Not IsDBNull(dr_tbl.Item(cField)) Then
                            cValue = dr_tbl.Item(cField)
                        End If
                    Next
                    If cValue Is Nothing Then
                        cValue = 0
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore durante il caricamento dei dati dalla tabella (", cTable, ") e dal campo (", cField, ") (ExtractValueFromTable)", ex))
        End Try
        Return cValue

    End Function

#End Region

#Region "Procedure per la valutazione delle condizioni"

    Public Function IsConditionDefualtValue(ByVal xmlItem As Xml.XmlNode)
        Dim xnWork As Xml.XmlNode
        Dim lRtn As Boolean

        lRtn = False
        xnWork = GetFirstChildOf(xmlItem, MAD_costanti.itm_conditions)
        While xnWork IsNot Nothing
            lRtn = lRtn Or xnWork.InnerText.ToLower = ";;default;;;;;;"
            xnWork = xnWork.NextSibling
        End While
        Return lRtn

    End Function

    Public Function EvaluateCondition(ByVal xmlItem As Xml.XmlNode, Optional ByVal cRelNameMaster As String = "", Optional ByVal cRelNameSlave As String = "") As Boolean
        Dim aOperator As New ArrayList
        Dim aElement As New ArrayList
        Dim aLista As New ArrayList
        Dim lCondition As Boolean
        Dim cValue As String
        Dim aData As String()
        Dim xnWork As Xml.XmlNode
        Dim nRecNo As Integer
        Dim dr_tbl As DataRow
        Dim dr_array As DataRow()

        Dim lOp1 As Boolean
        Dim lOp2 As Boolean
        Dim lResult As Boolean
        Dim cOperator As String
        Dim lExit As Boolean

        Const ec_leve = 0
        Const ec_oplo = 1
        Const ec_tipo = 2
        Const ec_oper = 3
        Const ec_tabl = 4
        Const ec_fiel = 5
        Const ec_daty = 6
        Const ec_valu = 7

        Try
            lCondition = True
            xnWork = GetFirstChildOf(xmlItem, MAD_costanti.itm_conditions)
            lExit = (xnWork Is Nothing)
            If lExit Then
                aElement.Add(lCondition)
            End If
            While Not lExit
                If xnWork Is Nothing Then
                    aData = ";);;;;;;".Split(";")
                Else
                    aData = xnWork.InnerText.Split(";")
                End If

                Select Case aData(ec_oplo)
                    Case Is = "("
                        aOperator.Insert(0, aData(ec_oplo))
                    Case Is = ")"
                        cOperator = aOperator(0)
                        aOperator.RemoveAt(0)
                        If cOperator = "or" Or cOperator = "and" Then
                            lOp1 = aElement(aElement.Count - 1)
                            lOp2 = aElement(aElement.Count - 2)
                            aElement.RemoveAt(aElement.Count - 1)
                            aElement.RemoveAt(aElement.Count - 1)

                            If cOperator = "and" Then
                                lResult = lOp1 And lOp2
                            End If
                            If cOperator = "or" Then
                                lResult = lOp1 Or lOp2
                            End If
                            aElement.Add(lResult)
                        End If
                    Case Else
                        If aData(ec_oplo) > "" Then
                            aOperator.Insert(0, aData(ec_oplo))
                        End If
                        Select Case aData(ec_tipo).ToLower
                            Case Is = "default"
                                cValue = ""
                            Case Is = "field"
                                cValue = RecuperaValore(aData(ec_tabl), aData(ec_fiel), cRelNameMaster, , , , , aData(ec_oper).Contains("DBNull"))
                            Case Is = "notoprint"
                                cValue = "NoToPrint"
                                aData(ec_oper) = "EQ"
                                aData(ec_daty) = "c"
                            Case Is = "page"
                                '
                                ' Il numero delle pagine del sub documento è sempre inferiore di 1 
                                ' in quanto conteggia le pagine stampate e quinndi viene aumentato alla 
                                ' fine della stampa della pagina
                                '
                                cValue = RepositoryDati.ContatoriPagine.PagineSubDocumento + 1
                            Case Is = "recordnumber", Is = "rowexist"
                                If (cRelNameSlave = "") And (aData(ec_tabl) = "") Then
                                    nRecNo = RepositoryDati.MainTable.Rows.Count
                                ElseIf (cRelNameSlave = "") And (aData(ec_tabl) <> "") Then
                                    cRelNameSlave = String.Concat(RepositoryDati.MainTableName, "_", aData(ec_tabl))
                                    dr_array = RepositoryDati.RecordMasterRow.GetChildRows(cRelNameSlave) '_ds_print.Tables(_PrintDataset_MainTableName).Rows(_cRecordNumber(_PrintDataset_MainTableName)).GetChildRows(cRelNameSlave)
                                    nRecNo = dr_array.Length
                                Else
                                    nRecNo = 0
                                    dr_array = RepositoryDati.RecordMasterRow.GetChildRows(cRelNameSlave) '_ds_print.Tables(_PrintDataset_MainTableName).Rows(_cRecordNumber(_PrintDataset_MainTableName)).GetChildRows(cRelNameSlave)
                                    For Each dr_tbl In dr_array
                                        If FilterTableRow(dr_array, dr_tbl, xmlItem) Then
                                            nRecNo += 1
                                        End If
                                    Next
                                End If
                                cValue = nRecNo
                            Case Is = "rowfilterexist"
                                nRecNo = 0
                                cRelNameSlave = String.Concat(RepositoryDati.MainTableName, "_", aData(ec_tabl))
                                dr_array = RepositoryDati.RecordMasterRow.GetChildRows(cRelNameSlave) '_ds_print.Tables(_PrintDataset_MainTableName).Rows(_cRecordNumber(_PrintDataset_MainTableName)).GetChildRows(cRelNameSlave)
                                Dim xnItem As Xml.XmlNode
                                xnItem = xmlItem.OwnerDocument.CreateElement("appo")
                                xnItem.AppendChild(xmlItem.OwnerDocument.CreateElement("BLKFilter"))
                                xnItem.ChildNodes(0).InnerText = aData(5).Replace(",", ";")
                                For Each dr_tbl In dr_array
                                    If FilterTableRow(dr_array, dr_tbl, xnItem) Then
                                        nRecNo += 1
                                    End If
                                Next
                                cValue = nRecNo
                            Case Is = "rownum"
                                cValue = RepositoryDati.RecordNumber(aData(ec_tabl)) + 1
                            Case Else
                                Throw New Exception("Tipo Campo condizione non indicato.", Nothing)
                        End Select
                        If (aData(ec_tipo).ToLower = "rowexist") Or (aData(ec_tipo).ToLower = "rowfilterexist") Then
                            lCondition = CDec(cValue) > 0
                        ElseIf (aData(ec_tipo).ToLower = "default") Then
                            lCondition = True
                        Else
                            Select Case aData(ec_daty)
                                Case Is = "c"
                                    Select Case aData(ec_oper)
                                        Case Is = "DBNull"
                                            lCondition = cValue = cnt_null_value
                                        Case Is = "NDBNull"
                                            lCondition = cValue = cnt_not_null_value
                                        Case Is = "EQ"
                                            lCondition = cValue = aData(ec_valu)
                                        Case Is = "NE"
                                            lCondition = cValue <> aData(ec_valu)
                                        Case Is = "LT"
                                            lCondition = cValue < aData(ec_valu)
                                        Case Is = "GT"
                                            lCondition = cValue > aData(ec_valu)
                                        Case Is = "NL"
                                            lCondition = cValue >= aData(ec_valu)
                                        Case Is = "NG"
                                            lCondition = cValue <= aData(ec_valu)
                                    End Select
                                Case Is = "n"
                                    Select Case aData(ec_oper)
                                        Case Is = "EQ"
                                            lCondition = CDec(cValue) = CDec(aData(ec_valu))
                                        Case Is = "NE"
                                            lCondition = CDec(cValue) <> CDec(aData(ec_valu))
                                        Case Is = "LT"
                                            lCondition = CDec(cValue) < CDec(aData(ec_valu))
                                        Case Is = "GT"
                                            lCondition = CDec(cValue) > CDec(aData(ec_valu))
                                        Case Is = "NL"
                                            lCondition = CDec(cValue) >= CDec(aData(ec_valu))
                                        Case Is = "NG"
                                            lCondition = CDec(cValue) <= CDec(aData(ec_valu))
                                        Case Is = "PARI"
                                            lCondition = CDec(cValue) Mod 2 = 0
                                    End Select
                                Case Is = "b"
                                    Select Case aData(ec_oper)
                                        Case Is = "EQ"
                                            lCondition = CBool(cValue) = CBool(aData(ec_valu))
                                        Case Else
                                            Throw New Exception("Tipo dato non indicato.", Nothing)
                                    End Select
                            End Select
                        End If
                        aElement.Add(lCondition)
                End Select
                If Not xnWork Is Nothing Then
                    xnWork = xnWork.NextSibling
                End If
                lExit = (xnWork Is Nothing And aElement.Count = 1 And aOperator.Count = 0)
            End While
            lCondition = aElement(0)
        Catch ex As Exception
            Throw ex
        End Try
        Return lCondition

    End Function

#End Region

    Private Function FilterTableRow(ByVal dr_array As DataRow(), ByVal dr_tbl As DataRow, ByVal xmlItem As Xml.XmlNode, Optional ByVal nRowSelected As Integer = 0) As Boolean
        Dim aData As String()
        Dim lRtn As Boolean
        Dim lOkCond As Boolean
        Dim nRowBlk As Integer
        Dim nRowTst As Integer
        Dim nRow As Integer
        Dim lExit As Boolean
        Dim dr_wrk As DataRow
        Dim lOkCond_1 As Boolean

        If xmlItem.SelectSingleNode("BLKFilter") Is Nothing Then
            lRtn = True
        Else
            aData = xmlItem.SelectSingleNode("BLKFilter").InnerText.ToString.Split(";")
            If aData(2).Contains("^") Then
                For Each cTmp As String In aData(2).Split("^")
                    lOkCond = lOkCond Or (dr_tbl.Item(aData(0)) = cTmp)
                Next
            Else
                lOkCond = dr_tbl.Item(aData(0)) = aData(2)
            End If

            If aData(1).ToLower = "ne" Then
                lOkCond = Not lOkCond
            End If
            If lOkCond And (nRowSelected <> 0) Then
                nRowSelected = Math.Abs(nRowSelected)
                '
                ' Abilita per la stampa degli ultimi n record.
                '
                If IsNumeric(nRowSelected) Then
                    nRow = 0
                    lExit = False
                    nRowBlk = 0
                    nRowTst = -1
                    While nRow < dr_array.Length And Not lExit
                        dr_wrk = dr_array(nRow)
                        If aData(2).Contains("^") Then
                            lOkCond_1 = False
                            For Each cTmp As String In aData(2).Split("^")
                                lOkCond_1 = lOkCond_1 Or (dr_tbl.Item(aData(0)) = cTmp)
                            Next
                        Else
                            lOkCond_1 = dr_tbl.Item(aData(0)) = aData(2)
                        End If
                        If lOkCond_1 Then
                            If ((dr_wrk.Item(0) = dr_tbl.Item(0)) And nRowTst = -1) Then
                                nRowTst = nRow
                            End If
                            If (nRowTst > -1) Then
                                nRowBlk += 1
                            End If
                        End If
                        lExit = nRowBlk > nRowSelected
                        nRow += 1
                    End While
                    lOkCond = (nRowBlk <= nRowSelected)

                    'nRowBlk = 0
                    'nRowTst = -1
                    'For Each dr_wrk As DataRow In dr_array
                    '    If dr_wrk.Item(aData(0)) = aData(2) Then
                    '        nRowBlk += 1
                    '        If (dr_wrk.Item(0) = dr_tbl.Item(0)) Then
                    '            nRowTst = nRowBlk
                    '        End If
                    '    End If
                    'Next
                    'lOkCond = (nRowTst > -1) And (nRowTst = nRowBlk)
                End If
                'Select Case aData(3)
                '    Case Is = 1
                '        nRowBlk = 0
                '        nRowTst = -1
                '        For Each dr_wrk As DataRow In dr_array
                '            If dr_wrk.Item(aData(0)) = aData(2) Then
                '                nRowBlk += 1
                '                If (dr_wrk.Item(0) = dr_tbl.Item(0)) Then
                '                    nRowTst = nRowBlk
                '                End If
                '            End If
                '        Next
                '        lOkCond = (nRowTst > -1) And (nRowTst = nRowBlk)
                'End Select
            End If
            lRtn = lOkCond
        End If
        Return lRtn

    End Function

End Class
