﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.InvoicerDomain.Impl
{
    public class CassettiPagineEntity
    {

        public CassettiPagineEntity(int id, int foglio, string nomeFoglio, int cassetto)
        {
            CAP_codice = id;
            CAP_pagina = foglio;
            CAP_desc_pag = nomeFoglio;
            CAP_cassetto = cassetto;
        }

        public int CAP_codice { get; set; }
        public int CAP_pagina { get; set; }
        public string CAP_desc_pag { get; set; }
        public int CAP_cassetto { get; set; }
    }
}
