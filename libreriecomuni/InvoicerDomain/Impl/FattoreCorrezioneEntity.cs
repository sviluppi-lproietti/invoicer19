﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.InvoicerDomain.Impl
{
    public class FattoreCorrezioneEntity
    {
        public String FCS_stampante { get; set; }
        public decimal FCS_coord_x { get; set; }
        public decimal FCS_coord_y { get; set; }

        public FattoreCorrezioneEntity()
        {
        }

        public FattoreCorrezioneEntity(string cValue)
        {
            FCS_stampante = cValue;
            FCS_coord_x = 0;
            FCS_coord_y = 0;
        }
    }
}
