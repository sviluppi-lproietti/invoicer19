﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.InvoicerDomain.Impl
{
    /// <summary>
    /// Gestisce le proprietà di un cassetto
    /// </summary>
    public class CassettoEntity
    {
        public CassettoEntity(int id, string kind, int rawkind, string name)
        {
            PRT_codice = id;
            PRT_kind = kind;
            PRT_rawkind = rawkind;
            PRT_sourcename = name;
        }

        public int PRT_codice { get; set; }
        public string PRT_kind { get; set; }
        public int PRT_rawkind { get; set; }
        public string PRT_sourcename { get; set; }

    }
}
