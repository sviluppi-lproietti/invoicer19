﻿Public Class ContatoriPagineEntity

    Public Property GiaEseguitaAnteprima As Boolean

    '
    ' Indica il numero dei fogli utilizzati per la stampa del documento
    '
    Public ReadOnly Property NumeroFogliDocumento As Integer

        Get
            Dim nFogli As Integer

            nFogli = PagineDocumento
            If _FronteRetro Then
                nFogli = Int((nFogli + 0.5) / 2)
            End If
            Return nFogli
        End Get

    End Property

    '
    ' Indica il numero di pagine totali del documento che si sta stampando o dei sotto documenti che compongono il documento.
    '
    Public Property PagineSubDocumento As Integer
    Public Property NuovaParte As Boolean

    '
    ' Indica il numero di pagine totali di TUTTO il documento
    '
    Public Property PagineDocumento As Integer

    '
    ' Contiene il numero di pagine dei subdocumenti
    '
    Public Property PagineSubDocumenti As ArrayList
    Public Property PagineTotaliDocumento As Integer
    Public Property PaginaUltimaParte As Integer
    Public Property ParteInStampa As Integer
    Public Property PartiDocumento As Integer

    Public ReadOnly Property TroppiFogli As Boolean

        Get
            Return Me.PagineDocumento > _MaxFogliDocumento
        End Get

    End Property

    Private _ArchiviazineOttica As Boolean
    Private _FronteRetro As Boolean
    Private _MaxFogliDocumento As Integer
    Private _MaxPagineParte As Integer


    Public Sub New(ByVal lArchiviazineOttica As Boolean, ByVal lFronteRetro As Boolean, ByVal nMaxPagineParte As Integer, ByVal nMaxFogli As Integer)

        _ArchiviazineOttica = lArchiviazineOttica
        _FronteRetro = lFronteRetro
        _MaxPagineParte = nMaxPagineParte
        _MaxFogliDocumento = nMaxFogli
        Me.PaginaUltimaParte = 0
        Me.GiaEseguitaAnteprima = False
        Me.PagineSubDocumento = 0
        Me.PagineDocumento = 0
        Me.PagineSubDocumenti = New ArrayList
        Me.ParteInStampa = 1
        Me.PartiDocumento = 1
        Me.PagineTotaliDocumento = 0

    End Sub

    Public Sub FineStampaSubDocumento(Optional ByVal nPagineAnteprima As Integer = -1)
        Dim nTmp As Integer

        If nPagineAnteprima = -1 Then
            nTmp = Me.PagineSubDocumento
        Else
            nTmp = nPagineAnteprima
        End If
        Me.PagineSubDocumenti.Add(nTmp)
        If (Me.PagineDocumento - Me.PaginaUltimaParte > Me._MaxPagineParte) And _ArchiviazineOttica Then
            Me.PaginaUltimaParte = Me.PagineDocumento
            If Not GiaEseguitaAnteprima Then
                Me.PartiDocumento += 1
            End If
            Me.NuovaParte = True
        Else
            Me.NuovaParte = False
        End If
        Me.PagineSubDocumento = 0

    End Sub

End Class
