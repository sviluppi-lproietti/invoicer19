﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.InvoicerDomain.Impl
{
    /// <summary>
    /// Gestisce le proprietà di una sessione recente
    /// </summary>
    public class StampantiEntity
    {
        public StampantiEntity(int id, string nome)
        {
            STA_codice = id;
            STA_nome = nome;
            
        }

        public int STA_codice { get; set; }
        public string STA_nome { get; set; }
    }
}
