﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.InvoicerDomain.Impl
{
    public class DocumentoEntity
    {
        public int DOC_codice { get; set; }
        public int DOC_cod_mod { get; set; }
        public string DOC_filename { get; set; }
    }
}
