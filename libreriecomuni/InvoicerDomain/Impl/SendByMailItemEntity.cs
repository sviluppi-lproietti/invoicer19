﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.InvoicerDomain.Impl
{
    public class SendByMailItemEntity
    {
        public SendByMailItemEntity()
        {
            ListaDocumenti = new List<DocumentoEntity>();
        }

        public int MAI_codice { get; set; }
        public List<DocumentoEntity> ListaDocumenti { get; set; }
    }
}
