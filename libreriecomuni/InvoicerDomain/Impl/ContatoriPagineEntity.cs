﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace ISC.LibrerieComuni.InvoicerDomain.Impl
{
    public class ContatoriPagineEntity
    {
        public Boolean GiaEseguitaAnteprima { get; set; }

        //
        // Indica il numero dei fogli utilizzati per la stampa del documento
        //
        public int NumeroFogliDocumento
        {
            get
            {
                int nFogli;

                nFogli = PagineDocumento;
                if (_FronteRetro)
                    nFogli = int.Parse(Math.Truncate((nFogli + 0.5) / 2).ToString());
                return nFogli;
            }
        }


        //
        // Indica il numero di pagine totali del documento che si sta stampando o dei sotto documenti che compongono il documento.
        //
        public int PagineSubDocumento { get; set; }
        public Boolean NuovaParte { get; set; }

        //
        // Indica il numero di pagine totali di TUTTO il documento
        //
        public int PagineDocumento { get; set; }

        //
        // Contiene il numero di pagine dei subdocumenti
        //
        public ArrayList PagineSubDocumenti { get; set; }
        public int PagineTotaliDocumento { get; set; }
        public int PaginaUltimaParte { get; set; }
        public int ParteInStampa { get; set; }
        public int PartiDocumento { get; set; }

        public Boolean TroppiFogli
        {
            get
            {
                return this.PagineDocumento > _MaxFogliDocumento;
            }
        }

        private Boolean _ArchiviazineOttica { get; set; }
        private Boolean _FronteRetro { get; set; }
        private int _MaxFogliDocumento { get; set; }
        private int _MaxPagineParte { get; set; }


        public ContatoriPagineEntity(Boolean lArchiviazineOttica, Boolean lFronteRetro, int nMaxPagineParte, int nMaxFogli)
        {
            _ArchiviazineOttica = lArchiviazineOttica;
            _FronteRetro = lFronteRetro;
            _MaxPagineParte = nMaxPagineParte;
            _MaxFogliDocumento = nMaxFogli;
            this.PaginaUltimaParte = 0;
            this.GiaEseguitaAnteprima = false;
            this.PagineSubDocumento = 0;
            this.PagineDocumento = 0;
            this.PagineSubDocumenti = new ArrayList();
            this.ParteInStampa = 1;
            this.PartiDocumento = 1;
            this.PagineTotaliDocumento = 0;
        }

        public void FineStampaSubDocumento(int nPagineAnteprima = -1)
        {
            int nTmp;

            if (nPagineAnteprima == -1)
                nTmp = this.PagineSubDocumento;
            else
                nTmp = nPagineAnteprima;

            this.PagineSubDocumenti.Add(nTmp);
            if ((this.PagineDocumento - this.PaginaUltimaParte > this._MaxPagineParte) && _ArchiviazineOttica)
            {
                this.PaginaUltimaParte = this.PagineDocumento;
                if (!GiaEseguitaAnteprima)
                    this.PartiDocumento += 1;
                this.NuovaParte = true;
            }
            else
                this.NuovaParte = false;

            this.PagineSubDocumento = 0;
        }

    }
}
