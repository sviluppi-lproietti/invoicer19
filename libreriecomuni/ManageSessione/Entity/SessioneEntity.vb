﻿Namespace Entity

    Public Class SessioneEntity

        Public Property SES_codice As Integer
        Public Property SES_nome As String
        Public Property SES_folder As String

        Public Sub New()

            SES_codice = 0

        End Sub

        Public Sub New(ByVal cNomeSessione As String)

            SES_nome = cNomeSessione
            SES_folder = ""

            SessioneValida = True

        End Sub

        Public Sub New(ByVal cNomeSessione As String, ByVal cFolder As String)

            SES_nome = cNomeSessione
            SES_folder = cFolder

            SessioneValida = System.IO.File.Exists(String.Concat(Me.SES_folder, "\", Me.SES_nome, ".xml"))

        End Sub

        Public Property SessioneValida() As Boolean

    End Class

End Namespace