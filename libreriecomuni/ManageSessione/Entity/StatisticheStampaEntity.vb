﻿Namespace Entity

    Public Class StatisticheStampaEntity

        Public Property STS_codice As Integer
        Public Property STS_num_pagine As Integer
        Public Property STS_stampate As Integer

        Public Sub New()

            STS_stampate = 0

        End Sub

        Public ReadOnly Property STS_desc_num_pagine As String

            Get
                Dim cTmp As String

                If STS_num_pagine = 1 Then
                    cTmp = String.Concat("Mod. con ", STS_num_pagine, " foglio")
                Else
                    cTmp = String.Concat("Mod. con ", STS_num_pagine, " fogli")
                End If
                Return cTmp
            End Get

        End Property

    End Class

End Namespace