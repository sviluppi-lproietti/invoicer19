﻿Public Class CLS_Ordina_DatiCreazioneSessione
    Implements IComparer

    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements IComparer.Compare
        Dim v1 As Integer
        Dim v2 As Integer
        Dim s As Integer

        v1 = CType(x, String())(0)
        v2 = CType(y, String())(0)
        If v1 < v2 Then
            s = -1
        ElseIf v1 > v2 Then
            s = 1
        Else
            s = 0
        End If
        Return s

    End Function

End Class