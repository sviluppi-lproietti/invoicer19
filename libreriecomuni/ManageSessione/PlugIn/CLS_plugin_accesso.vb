Imports System.Reflection
Imports System.Windows.Forms
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem

Namespace PlugIn

    Public Class CLS_plugin_accesso

        Public Property Files As CLS_FileFolderDic

            Get
                Return _Files
            End Get
            Set(ByVal value As CLS_FileFolderDic)
                _Files = value
                Me.SessioneDatiFissiFileName = Me.Files("PlugInDatiFissiFIL")
                Me.SessioneTrasformazioniFileName = Me.Files("PlugInTrasformazioniFIL")
            End Set

        End Property

        Public Property Folders As CLS_FileFolderDic

            Get
                Return _Folders
            End Get
            Set(ByVal value As CLS_FileFolderDic)
                _Folders = value
                Me.SessioneFld = Me.Folders("SessioneRootFLD")
                Me.SessioneDatiPath = Me.Folders("DatiFLD")
            End Set

        End Property

        Private _CodiceModulo As Integer
        Private _Files As CLS_FileFolderDic
        Private _Folders As CLS_FileFolderDic

        '
        ' Variabili di appoggio del plug in.
        '
        Private Property _PlugInV1_0_0 As PLUGIN_interfaceV1_0.IPLG_dati_lib
        Private Property _PlugInV1_1_0 As PLUGIN_interfaceV1_1.IPLG_dati_lib
        Private Property _PlugInV1_2_0 As PLUGIN_interfaceV1_2.IPLG_dati_lib
        Private Property _PlugInV1_3_0 As PLUGIN_interfaceV1_3.IPLG_dati_lib
        Private Property _PlugInV1_3_1 As PLUGIN_interfaceV1_3_1.IPLG_dati_lib
        Private Property _PlugInV1_3_2 As PLUGIN_interfaceV1_3_2.IPLG_dati_lib
        Private Property _PlugInV1_4_0 As PLUGIN_interfaceV1_4.IPLG_dati_lib
        Private Property _PlugInV1_4_1 As PLUGIN_interfaceV1_4_1.IPLG_dati_lib
        Private Property _PlugInV1_4_2 As PLUGIN_interfaceV1_4_2.IPLG_dati_lib
        Private Property _PlugInV1_4_3 As PLUGIN_interfaceV1_4_3.IPLG_dati_lib
        Private Property _PlugInV1_5_0 As PLUGIN_interfaceV1_5_0.IPLG_dati_lib
        Private Property _PlugInV1_5_1 As PLUGIN_interfaceV1_5_1.IPLG_dati_lib
        Private Property _PlugInV1_6_0 As PLUGIN_interfaceV1_6_0.IPLG_dati_lib
        Private Property _PlugInV1_6_1 As PLUGIN_interfaceV1_6_1.IPLG_dati_lib
        Private Property _PlugInV2_0_0 As PlugINs.PlugIN_2_0_0
        Private _TipoCaricamento As Integer
        Private _VersionePlugIn As eVersionePlugIn
        Private _xdSessione As Xml.XmlDocument
        Private _VersioneSistemaOperativo As String

        Public Sub New(ByVal nTipoCaricamento As Integer, ByVal cVersioneSistemaOperativo As String)

            _VersionePlugIn = eVersionePlugIn.No_versione
            _TipoCaricamento = nTipoCaricamento
            _VersioneSistemaOperativo = cVersioneSistemaOperativo

        End Sub

        ' ********************************************************************* '
        ' La capacit� di inviare documenti per email � stata introdotta con la  ' 
        ' versione 3 del plugin.                                                '
        ' ********************************************************************* '
        ReadOnly Property CapacitaInvioEmail() As Boolean

            Get
                Dim lRtn As Boolean

                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        lRtn = _PlugInV1_4_2.CapacitaInvioMail
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        lRtn = _PlugInV1_4_3.CapacitaInvioMail
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        lRtn = _PlugInV1_5_0.CapacitaInvioMail
                    Case Else
                        lRtn = _VersionePlugIn >= eVersionePlugIn.Versione_1_3_2
                End Select
                Return lRtn
            End Get

        End Property

        ReadOnly Property IsPlugInLoaded() As Boolean

            Get
                Return _VersionePlugIn <> eVersionePlugIn.No_versione
            End Get

        End Property

        ReadOnly Property VersionePlugIn() As String

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.LogDS = New DataSet
                        Return _PlugInV1_0_0.PlugInVersion
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        Return _PlugInV1_3_2.VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return "1.4.1"
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return "1.4.2"
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return "1.4.3"
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return "1.5.0"
                    Case Else
                        Return Nothing
                End Select
            End Get

        End Property

        Private Function SetExceptionItem(ByVal cErrMsg As String, ByVal ex As Exception) As Exception
            Dim ex_rtn As Exception

            ex_rtn = ex
            cErrMsg = String.Concat(cErrMsg, vbLf, ex_rtn.Message)
            Return New Exception(cErrMsg, ex_rtn)

        End Function

        Private Sub CompatibilizzaPlugIn()
            Dim dr_2 As DataRow
            Dim i As Integer
            Dim j As Integer

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    ' In questo caso dobbiamo aggiungere il campo DEF_alimimb al dataset nella tabella principale

                    With _PlugInV1_0_0.PrintDataset.Tables(_PlugInV1_0_0.PlugInMainTable(_CodiceModulo)).Columns
                        .Add(New DataColumn("DEF_alimimb"))
                        .Add(New DataColumn("DEF_raccolta", System.Type.GetType("System.Int32")))
                    End With

                    If _PlugInV1_0_0.FeedArray.Count = 0 Then
                        For Each dr As DataRow In _PlugInV1_0_0.PrintDataset.Tables(_PlugInV1_0_0.PlugInMainTable(_CodiceModulo)).Rows
                            dr("DEF_raccolta") = 9
                        Next
                    Else
                        i = 0
                        j = 0
                        For Each dr_1 As DataRow In _PlugInV1_0_0.PrintDataset.Tables(_PlugInV1_0_0.PlugInMainTable(_CodiceModulo)).Rows
                            dr_2 = _PlugInV1_0_0.QuickDataset.Tables(0).Select(String.Concat(_PlugInV1_0_0.PlugInMainTableKey(0), " = '", dr_1(_PlugInV1_0_0.PlugInMainTableKey(_CodiceModulo)), "'"))(0)
                            If dr_2("DEF_errcode") = 0 Or dr_2("DEF_errcode") = -2 Then
                                _PlugInV1_0_0.PrintDataset.Tables(_PlugInV1_0_0.PlugInMainTable(_CodiceModulo)).Rows(j).Item("DEF_alimimb") = _PlugInV1_0_0.FeedArray(i)
                                _PlugInV1_0_0.PrintDataset.Tables(_PlugInV1_0_0.PlugInMainTable(_CodiceModulo)).Rows(j).Item("DEF_raccolta") = 9
                                i += 1
                            End If
                            j += 1
                        Next
                    End If
                    _PlugInV1_0_0.PrintDataset.AcceptChanges()
                Case Is = eVersionePlugIn.Versione_1_1_0
                    ' Al momento non occorre nessuna compatibilizzazione
                Case Is = eVersionePlugIn.Versione_1_2_0
                    ' Al momento non occorre nessuna compatibilizzazione
                Case Is = eVersionePlugIn.Versione_1_3_0
                    ' Al momento non occorre nessuna compatibilizzazione
                Case Is = eVersionePlugIn.Versione_1_3_1
                    ' Al momento non occorre nessuna compatibilizzazione
                Case Is = eVersionePlugIn.Versione_1_3_2
                    ' Al momento non occorre nessuna compatibilizzazione
                Case Is = eVersionePlugIn.Versione_1_4_0
                    ' Al momento non occorre nessuna compatibilizzazione
                Case Is = eVersionePlugIn.Versione_1_4_1
                    ' Al momento non occorre nessuna compatibilizzazione
                Case Is = eVersionePlugIn.Versione_1_4_2
                    ' Al momento non occorre nessuna compatibilizzazione
                Case Is = eVersionePlugIn.Versione_1_4_3
                    ' Al momento non occorre nessuna compatibilizzazione
                Case Is = eVersionePlugIn.Versione_1_5_0
                    ' Al momento non occorre nessuna compatibilizzazione
            End Select

        End Sub

        Public Sub Carica_Plugin(ByVal _PlugInSessione As CLS_PluginBase)

            Try
                '
                ' Resetta i dati relativi ai plugin
                '
                ResetDatiPlugIn()

                _VersionePlugIn = _PlugInSessione.Versione
                Select Case _PlugInSessione.Versione
                    Case Is = eVersionePlugIn.No_versione
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0 = CType(_PlugInSessione, CLS_plugin_v1_0_0).PluginBase
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0 = CType(_PlugInSessione, CLS_plugin_v1_1_0).PluginBase
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0 = CType(_PlugInSessione, CLS_plugin_v1_2_0).PluginBase
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0 = CType(_PlugInSessione, CLS_plugin_v1_3_0).PluginBase
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1 = CType(_PlugInSessione, CLS_plugin_v1_3_1).PluginBase
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2 = CType(_PlugInSessione, CLS_plugin_v1_3_2).PluginBase
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0 = CType(_PlugInSessione, CLS_plugin_v1_4_0).PluginBase
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1 = CType(_PlugInSessione, CLS_plugin_v1_4_1).PluginBase
                        _PlugInV1_4_1.TipoCaricamento = _TipoCaricamento
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2 = CType(_PlugInSessione, CLS_plugin_v1_4_2).PluginBase
                        _PlugInV1_4_2.TipoCaricamento = _TipoCaricamento
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3 = CType(_PlugInSessione, CLS_plugin_v1_4_3).PluginBase
                        _PlugInV1_4_3.TipoCaricamento = _TipoCaricamento
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0 = CType(_PlugInSessione, CLS_plugin_v1_5_0).PluginBase
                        _PlugInV1_5_0.TipoCaricamento = _TipoCaricamento
                    Case Is = eVersionePlugIn.Versione_1_5_1
                        _PlugInV1_5_1 = CType(_PlugInSessione, CLS_plugin_v1_5_1).PluginBase
                        _PlugInV1_5_1.TipoCaricamento = _TipoCaricamento
                    Case Is = eVersionePlugIn.Versione_1_6_0
                        _PlugInV1_6_0 = CType(_PlugInSessione, CLS_plugin_v1_6_0).PluginBase
                        '_PlugInV1_6_0.TipoCaricamento = _TipoCaricamento
                    Case Is = eVersionePlugIn.Versione_1_6_1
                        _PlugInV1_6_1 = CType(_PlugInSessione, CLS_plugin_v1_6_1).PluginBase
                        '_PlugInV1_6_1.TipoCaricamento = _TipoCaricamento
                    Case Is = eVersionePlugIn.Versione_2_0_0
                        _PlugInV2_0_0 = CType(_PlugInSessione, CLS_plugin_v2_0_0).PluginBase
                        ' _PlugInV2_0_0.TipoCaricamento = _TipoCaricamento
                End Select
            Catch ex As Exception
                ResetDatiPlugIn()
                LogHelper.Logger.Error("Errore caricando il plug in", ex)
                Throw SetExceptionItem("Errore nel caricamento del plugin.", ex)
            End Try

        End Sub

        Private Sub ResetDatiPlugIn()

            _PlugInV1_0_0 = Nothing
            _PlugInV1_1_0 = Nothing
            _PlugInV1_2_0 = Nothing
            _PlugInV1_3_0 = Nothing
            _PlugInV1_3_1 = Nothing
            _PlugInV1_3_2 = Nothing
            _PlugInV1_4_0 = Nothing
            _PlugInV1_4_1 = Nothing
            _PlugInV1_4_2 = Nothing
            _PlugInV1_4_3 = Nothing
            _PlugInV1_5_0 = Nothing
            _VersionePlugIn = eVersionePlugIn.No_versione

        End Sub

        Private Sub WriteSessionData()
            Dim xiWork As Xml.XmlNode

            For Each aValue As String() In ResultAL
                xiWork = _xdSessione.CreateElement(aValue(0))
                xiWork.InnerText = aValue(1)
                xiWork.Attributes.Append(_xdSessione.CreateAttribute("codice"))
                xiWork.Attributes("codice").Value = aValue(2)
                xiWork.Attributes.Append(_xdSessione.CreateAttribute("tipo"))
                xiWork.Attributes("tipo").Value = aValue(3)
                xiWork.Attributes.Append(_xdSessione.CreateAttribute("stato"))
                xiWork.Attributes("stato").Value = aValue(4)
                _xdSessione.SelectSingleNode("sessione/dati").AppendChild(xiWork)
            Next

        End Sub

        ' ************************************************************************** '
        ' Propriet� e metodi che implementano l'accesso al plug in, al fine di rende '
        ' re trasparente l'accesso alle diverse versioni via via create.             '
        ' ************************************************************************** '
#Region " Lista delle procedure per l'accesso ad un plug in"

        Property MessaggiAvanzamento1() As String()

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        Return _PlugInV1_0_0.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        Return _PlugInV1_3_2.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return _PlugInV1_4_1.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.MessaggiAvanzamento
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.MessaggiAvanzamento
                    Case Else
                        Return Nothing
                End Select
            End Get
            Set(ByVal value As String())
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.MessaggiAvanzamento = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.MessaggiAvanzamento = value
                End Select
            End Set

        End Property

        ReadOnly Property CurrentRecordNumber() As Integer

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        Return _PlugInV1_0_0.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        Return _PlugInV1_3_2.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return _PlugInV1_4_1.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.CurrentRecordNumber
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.CurrentRecordNumber
                    Case Else
                        Return Nothing
                End Select
            End Get

        End Property

        ReadOnly Property DataSendByMail() As Xml.XmlNode

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        Return _PlugInV1_3_2.DataSendByMail
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.DataSendByMail
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return _PlugInV1_4_1.DataSendByMail
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.DataSendByMail
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.DataSendByMail
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.DataSendByMail
                    Case Else
                        Return Nothing
                End Select
            End Get

        End Property

        ReadOnly Property ElementiMenuDedicati() As ArrayList

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        Return _PlugInV1_0_0.GetSessioneMenuItem
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.ElementiMenuDedicati
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.ElementiMenuDedicati
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.ElementiMenuDedicati
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.ElementiMenuDedicati
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        Return _PlugInV1_3_2.ElementiMenuDedicati(_TipoCaricamento)
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.ElementiMenuDedicati(_TipoCaricamento)
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return _PlugInV1_4_1.ElementiMenuDedicati()
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.ElementiMenuDedicati()
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.ElementiMenuDedicati()
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.ElementiMenuDedicati()
                    Case Else
                        Return Nothing
                End Select
            End Get

        End Property

        ReadOnly Property FullDataset() As DataSet

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        Return _PlugInV1_0_0.PrintDataset
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.PrintDataset
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.PrintDataset
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.PrintDataset
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.PrintDataset
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        If _CodiceModulo = eCodiceModuloFix.MOD_SendEmail Then
                            Return _PlugInV1_3_2.SendEmailDataset
                        Else
                            Return _PlugInV1_3_2.PrintDataset
                        End If
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.FullDataset
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return _PlugInV1_4_1.FullDataset
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.FullDataset
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.FullDataset
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.FullDataset
                    Case Else
                        Return Nothing
                End Select
            End Get

        End Property

        ReadOnly Property FullDataSet_MainTable() As DataTable

            Get
                Dim dtRtn As DataTable

                dtRtn = Nothing
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        dtRtn = _PlugInV1_0_0.PrintDataset.Tables(_PlugInV1_0_0.PlugInMainTable(_CodiceModulo))
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        dtRtn = _PlugInV1_1_0.PrintDataset.Tables(_PlugInV1_1_0.PrintDataSet_MainTableName)
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        dtRtn = _PlugInV1_2_0.PrintDataset.Tables(_PlugInV1_2_0.PrintDataSet_MainTableName)
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        dtRtn = _PlugInV1_3_0.PrintDataset.Tables(_PlugInV1_3_0.PrintDataSet_MainTableName)
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        dtRtn = _PlugInV1_3_1.PrintDataset.Tables(_PlugInV1_3_1.PrintDataSet_MainTableName)
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        If _CodiceModulo = eCodiceModuloFix.MOD_SendEmail Then
                            dtRtn = _PlugInV1_3_2.SendEmailDataset.Tables(_PlugInV1_3_2.SendEmailDataset_MainTableName)
                        Else
                            dtRtn = _PlugInV1_3_2.PrintDataset.Tables(_PlugInV1_3_2.PrintDataSet_MainTableName)
                        End If
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        dtRtn = _PlugInV1_4_0.FullDataset.Tables(_PlugInV1_4_0.FullDataSet_MainTableName)
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        dtRtn = _PlugInV1_4_1.FullDataset.Tables(_PlugInV1_4_1.FullDataSet_MainTableName)
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        dtRtn = _PlugInV1_4_2.FullDataset.Tables(_PlugInV1_4_2.FullDataSet_MainTableName)
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        dtRtn = _PlugInV1_4_3.FullDataset.Tables(_PlugInV1_4_3.FullDataSet_MainTableName)
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        dtRtn = _PlugInV1_5_0.FullDataset.Tables(_PlugInV1_5_0.FullDataSet_MainTableName)
                End Select
                Return dtRtn
            End Get

        End Property

        ReadOnly Property FullDataSet_MainTableName() As String

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        Return _PlugInV1_0_0.PlugInMainTable(_CodiceModulo)
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.PrintDataSet_MainTableName
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.PrintDataSet_MainTableName
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.PrintDataSet_MainTableName
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.PrintDataSet_MainTableName
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        If _CodiceModulo = eCodiceModuloFix.MOD_SendEmail Then
                            Return _PlugInV1_3_2.SendEmailDataset_MainTableName
                        Else
                            Return _PlugInV1_3_2.PrintDataSet_MainTableName
                        End If
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.FullDataSet_MainTableName
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return _PlugInV1_4_1.FullDataSet_MainTableName
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.FullDataSet_MainTableName
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.FullDataSet_MainTableName
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.FullDataSet_MainTableName
                    Case Else
                        Return Nothing
                End Select
            End Get

        End Property

        ReadOnly Property LinkField_QuickDS_FullDS() As String

            Get
                Dim cLinkField As String

                cLinkField = ""
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        cLinkField = _PlugInV1_0_0.PlugInMainLinkToQD(_CodiceModulo)
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        cLinkField = _PlugInV1_1_0.LinkField_QuickDS_PrintDS()
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        cLinkField = _PlugInV1_2_0.LinkField_QuickDS_PrintDS()
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        cLinkField = _PlugInV1_3_0.LinkField_QuickDS_PrintDS()
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        cLinkField = _PlugInV1_3_1.LinkField_QuickDS_PrintDS()
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        If _CodiceModulo = eCodiceModuloFix.MOD_SendEmail Then
                            cLinkField = _PlugInV1_3_2.LinkField_QuickDS_SendDS()
                        Else
                            cLinkField = _PlugInV1_3_2.LinkField_QuickDS_PrintDS()
                        End If
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        cLinkField = _PlugInV1_4_0.LinkField_QuickDS_FullDS()
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        cLinkField = _PlugInV1_4_1.LinkField_QuickDS_FullDS()
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        cLinkField = _PlugInV1_4_2.LinkField_QuickDS_FullDS()
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        cLinkField = _PlugInV1_4_3.LinkField_QuickDS_FullDS()
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        cLinkField = _PlugInV1_5_0.LinkField_QuickDS_FullDS()
                End Select
                Return cLinkField
            End Get

        End Property

        ReadOnly Property ListaControlliFormNuovaSessione()

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        Return _PlugInV1_0_0.ListaControlliForm
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.ListaControlliFormNuovaSessione
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.ListaControlliFormNuovaSessione
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.ListaControlliFormNuovaSessione
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.ListaControlliFormNuovaSessione
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        Return _PlugInV1_3_2.ListaControlliFormNuovaSessione
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.ListaControlliFormNuovaSessione
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return _PlugInV1_4_1.ListaControlliFormNuovaSessione
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.ListaControlliFormNuovaSessione
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.ListaControlliFormNuovaSessione
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.ListaControlliFormNuovaSessione
                    Case Else
                        Return Nothing
                End Select
            End Get

        End Property

        ReadOnly Property LogDSStampa() As DataSet

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.LogDS = New DataSet
                        Return _PlugInV1_0_0.LogDS
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.LogDSStampa
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.LogDSStampa
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.LogDSStampa
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.LogDSStampa
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        Return _PlugInV1_3_2.LogDSStampa
                    Case Else
                        Return Nothing
                End Select
            End Get

        End Property

        ReadOnly Property MessaggioAvviso() As String

            Get
                Dim cValue As String

                cValue = ""
                If _VersionePlugIn = eVersionePlugIn.Versione_1_4_1 Then
                    cValue = _PlugInV1_4_1.MessaggioAvviso
                ElseIf _VersionePlugIn = eVersionePlugIn.Versione_1_4_2 Then
                    cValue = _PlugInV1_4_2.MessaggioAvviso
                ElseIf _VersionePlugIn = eVersionePlugIn.Versione_1_4_3 Then
                    cValue = _PlugInV1_4_3.MessaggioAvviso
                ElseIf _VersionePlugIn = eVersionePlugIn.Versione_1_5_0 Then
                    cValue = _PlugInV1_5_0.MessaggioAvviso
                ElseIf _VersionePlugIn < eVersionePlugIn.Versione_1_4_1 And _CodiceModulo = eCodiceModuloFix.MOD_SendEmail Then
                    Select Case _VersionePlugIn
                        Case Is = eVersionePlugIn.Versione_1_3_0
                            cValue = _PlugInV1_3_0.AlertPlugIn_SND
                        Case Is = eVersionePlugIn.Versione_1_3_1
                            cValue = _PlugInV1_3_1.AlertPlugIn_SND
                        Case Is = eVersionePlugIn.Versione_1_3_2
                            cValue = _PlugInV1_3_2.AlertPlugIn_SND
                        Case Is = eVersionePlugIn.Versione_1_4_0
                            cValue = _PlugInV1_4_0.AlertPlugIn_SND
                    End Select
                ElseIf _VersionePlugIn < eVersionePlugIn.Versione_1_4_1 And _CodiceModulo <> eCodiceModuloFix.MOD_SendEmail Then
                    Select Case _VersionePlugIn
                        Case Is = eVersionePlugIn.Versione_1_0_0
                            cValue = _PlugInV1_0_0.AlertPlugIn
                        Case Is = eVersionePlugIn.Versione_1_1_0
                            cValue = _PlugInV1_1_0.AlertPlugIn
                        Case Is = eVersionePlugIn.Versione_1_2_0
                            cValue = _PlugInV1_2_0.AlertPlugIn
                        Case Is = eVersionePlugIn.Versione_1_3_0
                            cValue = _PlugInV1_3_0.AlertPlugIn_PRN
                        Case Is = eVersionePlugIn.Versione_1_3_1
                            cValue = _PlugInV1_3_1.AlertPlugIn_PRN
                        Case Is = eVersionePlugIn.Versione_1_3_2
                            cValue = _PlugInV1_3_2.AlertPlugIn_PRN
                        Case Is = eVersionePlugIn.Versione_1_4_0
                            cValue = _PlugInV1_4_0.AlertPlugIn_PRN
                    End Select
                End If
                Return cValue
            End Get

        End Property

        ReadOnly Property NomePlugIn() As String

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        Return _PlugInV1_0_0.PlugInName
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.NomePlugIn
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.NomePlugIn
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.NomePlugIn
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.NomePlugIn
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        Return _PlugInV1_3_2.NomePlugIn
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.NomePlugIn
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return _PlugInV1_4_1.NomePlugIn
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.NomePlugIn
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.NomePlugIn
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.NomePlugIn
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        ReadOnly Property ResultAL() As ArrayList

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        Return _PlugInV1_0_0.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        Return _PlugInV1_1_0.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        Return _PlugInV1_2_0.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        Return _PlugInV1_3_0.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        Return _PlugInV1_3_1.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        Return _PlugInV1_3_2.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        Return _PlugInV1_4_0.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        Return _PlugInV1_4_1.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.ResultAL
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.ResultAL
                    Case Else
                        Return Nothing
                End Select
            End Get

        End Property

        WriteOnly Property AlimentazioneImbustatrice() As String

            Set(ByVal value As String)
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.FeedString = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.FeedString = value
                End Select
            End Set

        End Property

        WriteOnly Property CodiceModulo() As Integer

            Set(ByVal value As Integer)
                _CodiceModulo = value
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.CodiceModulo = _CodiceModulo
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.CodiceModulo = _CodiceModulo
                End Select
            End Set

        End Property

        WriteOnly Property FiltroSelezione() As String

            Set(ByVal value As String)
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        ' ********************************************************************* '
                        ' In questa versione non era prevista la possibilit� di filtrare i re-  ' 
                        ' cord della tabella pricipale del quickdataset.                        '
                        ' ********************************************************************* '
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0.FiltroSelezione = value
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0.FiltroSelezione = value
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0.FiltroSelezione = value
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1.FiltroSelezione = value
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2.FiltroSelezione = value
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.FiltroSelezione = value
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.FiltroSelezione = value
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.FiltroSelezione = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.FiltroSelezione = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.FiltroSelezione = value
                End Select
            End Set

        End Property

        WriteOnly Property SessioneFld() As String

            Set(ByVal value As String)
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.SessionePath = value
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0.SessioneFld = value
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0.SessioneFld = value
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0.SessioneFld = value
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1.SessioneFld = value
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2.SessioneFld = value
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.SessioneFld = value
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.SessioneFld = value
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.SessioneFld = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.SessioneFld = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.SessioneFld = value
                End Select
            End Set

        End Property

        WriteOnly Property SezioneDati() As Xml.XmlNode

            Set(ByVal value As Xml.XmlNode)
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.SessioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0.SezioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0.SezioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0.SezioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1.SezioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2.SezioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.SezioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.SezioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.SezioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.SezioneDati = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.SezioneDati = value
                End Select
            End Set

        End Property

        WriteOnly Property SessioneDatiFissiFileName() As String

            Set(ByVal value As String)
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.SessioneDatiFissiFileName = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.SessioneDatiFissiFileName = value
                End Select
            End Set

        End Property

        WriteOnly Property SessioneTrasformazioniFileName() As String

            Set(ByVal value As String)
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.SessioneTrasformazioniFileName = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.SessioneTrasformazioniFileName = value
                End Select
            End Set

        End Property

        WriteOnly Property SessioneDatiPath() As String

            Set(ByVal value As String)
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _PlugInV1_1_0.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _PlugInV1_2_0.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _PlugInV1_3_0.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _PlugInV1_3_1.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _PlugInV1_3_2.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.SessioneDatiPath = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.SessioneDatiPath = value
                End Select

            End Set

        End Property

        WriteOnly Property SessioneNuova() As Boolean

            Set(ByVal value As Boolean)
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _PlugInV1_0_0.SessioneNuova = value
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _PlugInV1_4_0.SessioneNuova = value
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _PlugInV1_4_1.SessioneNuova = value
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.SessioneNuova = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.SessioneNuova = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.SessioneNuova = value
                    Case Else
                        ' ********************************************************************* '
                        ' Nelle versioni successive la dichiarazione esplicita di nuova sessio- ' 
                        ' ne non � pi� necessaria in quanto non viene pi� utilizzzata. Il fatto ' 
                        ' di creare una nuova sessione � indicato esplicitamente da programma e ' 
                        ' non dal plugin.                                                       ' 
                        '                 NESSUNA DICHIARAZIONE � NECESSARIA                    '
                        ' ********************************************************************* '
                End Select
            End Set

        End Property

        Public Function CheckDatiSessione(ByVal _DatiSessione As ArrayList) As Boolean

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    Return _PlugInV1_0_0.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_1_0
                    Return _PlugInV1_1_0.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_2_0
                    Return _PlugInV1_2_0.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_3_0
                    Return _PlugInV1_3_0.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_3_1
                    Return _PlugInV1_3_1.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_3_2
                    Return _PlugInV1_3_2.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_4_0
                    Return _PlugInV1_4_0.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_4_1
                    Return _PlugInV1_4_1.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_4_2
                    Return _PlugInV1_4_2.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_4_3
                    Return _PlugInV1_4_3.CheckDatiSessione(_DatiSessione)
                Case Is = eVersionePlugIn.Versione_1_5_0
                    Return _PlugInV1_5_0.CheckDatiSessione(_DatiSessione)
                Case Else
                    Return Nothing
            End Select

        End Function

        Public Function GetSelectedRow(ByVal _TipoRic As String, ByVal _ValueRic As String) As String()

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    Return _PlugInV1_0_0.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_1_0
                    Return _PlugInV1_1_0.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_2_0
                    Return _PlugInV1_2_0.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_3_0
                    Return _PlugInV1_3_0.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_3_1
                    Return _PlugInV1_3_1.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_3_2
                    Return _PlugInV1_3_2.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_4_0
                    Return _PlugInV1_4_0.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_4_1
                    Return _PlugInV1_4_1.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_4_2
                    Return _PlugInV1_4_2.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_4_3
                    Return _PlugInV1_4_3.GetSelectedRow(_TipoRic, _ValueRic)
                Case Is = eVersionePlugIn.Versione_1_5_0
                    Return _PlugInV1_5_0.GetSelectedRow(_TipoRic, _ValueRic)
                Case Else
                    Return Nothing
            End Select

        End Function

        Public Function PostStampa() As ArrayList

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_4_1
                    Return _PlugInV1_4_1.PostStampa
                Case Is = eVersionePlugIn.Versione_1_4_2
                    Return _PlugInV1_4_2.PostStampa
                Case Is = eVersionePlugIn.Versione_1_4_3
                    Return _PlugInV1_4_3.PostStampa
                Case Else
                    Return New ArrayList
            End Select

        End Function

        Public Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList, ByVal xdSessione As Xml.XmlDocument)

            Try
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_0_0
                        _xdSessione = xdSessione
                        _PlugInV1_0_0.CreaDBSessioneS1(_DatiSessione)
                        WriteSessionData()
                        _PlugInV1_0_0.CreaDBSessioneS2()
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_1_0
                        _xdSessione = xdSessione
                        _PlugInV1_1_0.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_2_0
                        _xdSessione = xdSessione
                        _PlugInV1_2_0.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_3_0
                        _xdSessione = xdSessione
                        _PlugInV1_3_0.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_3_1
                        _xdSessione = xdSessione
                        _PlugInV1_3_1.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_3_2
                        _xdSessione = xdSessione
                        _PlugInV1_3_2.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_4_0
                        _xdSessione = xdSessione
                        _PlugInV1_4_0.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_4_1
                        _xdSessione = xdSessione
                        _PlugInV1_4_1.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _xdSessione = xdSessione
                        _PlugInV1_4_2.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _xdSessione = xdSessione
                        _PlugInV1_4_3.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _xdSessione = xdSessione
                        _PlugInV1_5_0.CreaDataBaseSessione(_DatiSessione)
                        WriteSessionData()
                End Select
            Catch ex As Exception
                Throw New Exception("ACCPL-000001: Errore nella creazione della sessione", ex)
            End Try

        End Sub

        Public Sub GoToRecordNumber(ByVal nRow As Integer)

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    _PlugInV1_0_0.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_1_0
                    _PlugInV1_1_0.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_2_0
                    _PlugInV1_2_0.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_3_0
                    _PlugInV1_3_0.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_3_1
                    _PlugInV1_3_1.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_3_2
                    _PlugInV1_3_2.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_4_0
                    _PlugInV1_4_0.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_4_1
                    _PlugInV1_4_1.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_4_2
                    _PlugInV1_4_2.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_4_3
                    _PlugInV1_4_3.GoToRecordNumber(nRow)
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInV1_5_0.GoToRecordNumber(nRow)
            End Select

        End Sub

        Public Sub ImpostaDGVDati(ByVal DGV As DataGridView)

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    _PlugInV1_0_0.ImpostaDGVDati(DGV)
                Case Is = eVersionePlugIn.Versione_1_1_0
                    _PlugInV1_1_0.ImpostaDGVDati(DGV)
                Case Is = eVersionePlugIn.Versione_1_2_0
                    _PlugInV1_2_0.ImpostaDGVDati(DGV)
                Case Is = eVersionePlugIn.Versione_1_3_0
                    _PlugInV1_3_0.ImpostaDGVDati(DGV)
                Case Is = eVersionePlugIn.Versione_1_3_1
                    _PlugInV1_3_1.ImpostaDGVDati(DGV)
                Case Is = eVersionePlugIn.Versione_1_3_2
                    _PlugInV1_3_2.ImpostaDGVDati(_TipoCaricamento, DGV)
                Case Is = eVersionePlugIn.Versione_1_4_0
                    _PlugInV1_4_0.ImpostaDGVDati(_TipoCaricamento, DGV)
                Case Is = eVersionePlugIn.Versione_1_4_1
                    _PlugInV1_4_1.ImpostaDGVDati(DGV)
                Case Is = eVersionePlugIn.Versione_1_4_2
                    _PlugInV1_4_2.ImpostaDGVDati(DGV)
                Case Is = eVersionePlugIn.Versione_1_4_3
                    _PlugInV1_4_3.ImpostaDGVDati(DGV)
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInV1_5_0.ImpostaDGVDati(DGV)
            End Select

        End Sub

        Public Sub LoadFullDataset(ByVal nRecordDaCaricare As Integer)

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    _PlugInV1_0_0.LoadPrintDataset(nRecordDaCaricare)
                Case Is = eVersionePlugIn.Versione_1_1_0
                    _PlugInV1_1_0.LoadPrintDataset(nRecordDaCaricare)
                Case Is = eVersionePlugIn.Versione_1_2_0
                    _PlugInV1_2_0.LoadPrintDataset(nRecordDaCaricare)
                Case Is = eVersionePlugIn.Versione_1_3_0
                    _PlugInV1_3_0.LoadPrintDataset(nRecordDaCaricare)
                Case Is = eVersionePlugIn.Versione_1_3_1
                    _PlugInV1_3_1.LoadPrintDataset(nRecordDaCaricare)
                Case Is = eVersionePlugIn.Versione_1_3_2
                    If _CodiceModulo = eCodiceModuloFix.MOD_SendEmail Then
                        _PlugInV1_3_2.LoadSendDataset()
                        With _PlugInV1_3_2.SendEmailDataset.Tables("TBL_documenti").Columns
                            .Add(New DataColumn("DOC_filesize", System.Type.GetType("System.Int64")))
                            .Add(New DataColumn("DOC_status", System.Type.GetType("System.Int64")))
                            .Add(New DataColumn("DOC_gruppoinvio", System.Type.GetType("System.Int64")))
                        End With
                    Else
                        _PlugInV1_3_2.LoadPrintDataset(nRecordDaCaricare)
                    End If
                Case Is = eVersionePlugIn.Versione_1_4_0
                    _PlugInV1_4_0.LoadFullDataset(_CodiceModulo, nRecordDaCaricare)
                Case Is = eVersionePlugIn.Versione_1_4_1
                    _PlugInV1_4_1.LoadFullDataset(nRecordDaCaricare)
                Case Is = eVersionePlugIn.Versione_1_4_2
                    _PlugInV1_4_2.LoadFullDataset(nRecordDaCaricare)
                Case Is = eVersionePlugIn.Versione_1_4_3
                    _PlugInV1_4_3.LoadFullDataset(nRecordDaCaricare)
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInV1_5_0.LoadFullDataset(nRecordDaCaricare)
            End Select
            CompatibilizzaPlugIn()
            If _CodiceModulo = eCodiceModuloFix.MOD_SendEmail And (_VersionePlugIn = eVersionePlugIn.Versione_1_3_2 Or _VersionePlugIn = eVersionePlugIn.Versione_1_4_0) Then


            End If

        End Sub

        Public Sub PostAnteprima(ByVal aLista As ArrayList)

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    ' ********************************************************************* '
                    ' In questa versione non erano previste operazioni tra la fase di ante- ' 
                    ' prima e quella di stampa.                                             '
                    ' ********************************************************************* '
                Case Is = eVersionePlugIn.Versione_1_1_0
                    ' ********************************************************************* '
                    ' In questa versione non erano previste operazioni tra la fase di ante- ' 
                    ' prima e quella di stampa.                                             '
                    ' ********************************************************************* '
                Case Is = eVersionePlugIn.Versione_1_2_0
                    _PlugInV1_2_0.PostPreStampa(aLista)
                Case Is = eVersionePlugIn.Versione_1_3_0
                    _PlugInV1_3_0.PostPreStampa(aLista)
                Case Is = eVersionePlugIn.Versione_1_3_1
                    _PlugInV1_3_1.PostPreStampa(aLista)
                Case Is = eVersionePlugIn.Versione_1_3_2
                    _PlugInV1_3_2.PostPreStampa(aLista)
                Case Is = eVersionePlugIn.Versione_1_4_0
                    _PlugInV1_4_0.PostPreStampa(aLista)
                Case Is = eVersionePlugIn.Versione_1_4_1
                    _PlugInV1_4_1.PostAnteprima(aLista)
                Case Is = eVersionePlugIn.Versione_1_4_2
                    _PlugInV1_4_2.PostAnteprima(aLista)
                Case Is = eVersionePlugIn.Versione_1_4_3
                    _PlugInV1_4_3.PostAnteprima(aLista)
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInV1_5_0.PostAnteprima(aLista)
            End Select

        End Sub

        Public Sub RemovePRNDSErrorRecord()

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    _PlugInV1_0_0.RemovePRNDSErrorRecord()
                Case Is = eVersionePlugIn.Versione_1_1_0
                    _PlugInV1_1_0.RemovePRNDSErrorRecord()
                Case Is = eVersionePlugIn.Versione_1_2_0
                    _PlugInV1_2_0.RemovePRNDSErrorRecord()
                Case Is = eVersionePlugIn.Versione_1_3_0
                    _PlugInV1_3_0.RemovePRNDSErrorRecord()
                Case Is = eVersionePlugIn.Versione_1_3_1
                    _PlugInV1_3_1.RemovePRNDSErrorRecord()
            End Select

        End Sub

        Public Sub SetPrintable(ByVal dr As DataRow, ByVal cSelectFromPlugInType As String, ByVal cParamSelez As String)

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    _PlugInV1_0_0.SetPrintable(dr, cSelectFromPlugInType)
                Case Is = eVersionePlugIn.Versione_1_1_0
                    _PlugInV1_1_0.SetPrintable(dr, cSelectFromPlugInType)
                Case Is = eVersionePlugIn.Versione_1_2_0
                    _PlugInV1_2_0.SetPrintable(dr, cSelectFromPlugInType)
                Case Is = eVersionePlugIn.Versione_1_3_0
                    _PlugInV1_3_0.SetPrintable(dr, cSelectFromPlugInType)
                Case Is = eVersionePlugIn.Versione_1_3_1
                    _PlugInV1_3_1.SetPrintable(dr, cSelectFromPlugInType)
                Case Is = eVersionePlugIn.Versione_1_3_2
                    _PlugInV1_3_2.SetPrintable(dr, cSelectFromPlugInType)
                Case Is = eVersionePlugIn.Versione_1_4_0
                    _PlugInV1_4_0.SetPrintable(dr, cSelectFromPlugInType)
                Case Is = eVersionePlugIn.Versione_1_4_1
                    _PlugInV1_4_1.SetPrintable(dr, cSelectFromPlugInType)
                Case Is = eVersionePlugIn.Versione_1_4_2
                    _PlugInV1_4_2.SetPrintable(dr, cSelectFromPlugInType)
                Case Is = eVersionePlugIn.Versione_1_4_3
                    _PlugInV1_4_3.SetPrintable(dr, cSelectFromPlugInType, cParamSelez)
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInV1_5_0.SetPrintable(dr, cSelectFromPlugInType, cParamSelez)
            End Select

        End Sub

        Public Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_0_0
                    _PlugInV1_0_0.SetSelectable(dr)
                Case Is = eVersionePlugIn.Versione_1_1_0
                    _PlugInV1_1_0.SetSelectable(dr, cFilter)
                Case Is = eVersionePlugIn.Versione_1_2_0
                    _PlugInV1_2_0.SetSelectable(dr, cFilter)
                Case Is = eVersionePlugIn.Versione_1_3_0
                    _PlugInV1_3_0.SetSelectable(dr, cFilter)
                Case Is = eVersionePlugIn.Versione_1_3_1
                    _PlugInV1_3_1.SetSelectable(dr, cFilter)
                Case Is = eVersionePlugIn.Versione_1_3_2
                    _PlugInV1_3_2.SetSelectable(dr, cFilter)
                Case Is = eVersionePlugIn.Versione_1_4_0
                    _PlugInV1_4_0.SetSelectable(dr, cFilter)
                Case Is = eVersionePlugIn.Versione_1_4_1
                    _PlugInV1_4_1.SetSelectable(dr, cFilter)
                Case Is = eVersionePlugIn.Versione_1_4_2
                    _PlugInV1_4_2.SetSelectable(dr, cFilter)
                Case Is = eVersionePlugIn.Versione_1_4_3
                    _PlugInV1_4_3.SetSelectable(dr, cFilter)
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInV1_5_0.SetSelectable(dr, cFilter)
            End Select

        End Sub

        Public Sub UpdateQuickDatasetFile()

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_3_1
                    _PlugInV1_3_1.UpdateQuickDatasetFile()
                Case Is = eVersionePlugIn.Versione_1_3_2
                    _PlugInV1_3_2.UpdateQuickDatasetFile()
                Case Is = eVersionePlugIn.Versione_1_4_0
                    _PlugInV1_4_0.UpdateQuickDatasetFile()
                Case Is = eVersionePlugIn.Versione_1_4_1
                    _PlugInV1_4_1.UpdateQuickDatasetFile()
                Case Is = eVersionePlugIn.Versione_1_4_2
                    _PlugInV1_4_2.UpdateQuickDatasetFile()
                Case Is = eVersionePlugIn.Versione_1_4_3
                    _PlugInV1_4_3.UpdateQuickDatasetFile()
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInV1_5_0.UpdateQuickDatasetFile()
            End Select

        End Sub

#End Region

        WriteOnly Property ListaGiri() As ArrayList

            Set(ByVal value As ArrayList)
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        _PlugInV1_4_2.ListaGiri = value
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        _PlugInV1_4_3.ListaGiri = value
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        _PlugInV1_5_0.ListaGiri = value
                End Select
            End Set

        End Property

        WriteOnly Property DBGiriConnectionString() As String

            Set(ByVal value As String)
                LogHelper.Logger.Debug("Valore della _VersionePlugIn " + _VersionePlugIn.ToString())
                Try
                    Select Case _VersionePlugIn
                        Case Is = eVersionePlugIn.Versione_1_4_2
                            _PlugInV1_4_2.DBGiriConnectionString = value
                        Case Is = eVersionePlugIn.Versione_1_4_3
                            _PlugInV1_4_3.DBGiriConnectionString = value
                        Case Is = eVersionePlugIn.Versione_1_5_0
                            _PlugInV1_5_0.DBGiriConnectionString = value
                    End Select
                    LogHelper.Logger.Debug("Fine Valore della _VersionePlugIn " + _VersionePlugIn.ToString())
                Catch ex As Exception
                    LogHelper.Logger.Error("DBGiriConnectionString", ex)
                End Try
            End Set

        End Property

        ReadOnly Property NuoviToponimi() As Boolean

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_4_2
                        Return _PlugInV1_4_2.NuoviToponimi
                    Case Is = eVersionePlugIn.Versione_1_4_3
                        Return _PlugInV1_4_3.NuoviToponimi
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.NuoviToponimi
                    Case Else
                        Return False
                End Select
            End Get

        End Property

        ReadOnly Property DatiAzioniPostStampa() As ArrayList

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.DatiAzioniPostStampa
                    Case Else
                        Return New ArrayList
                End Select
            End Get

        End Property

        ReadOnly Property DatiAzioniPreInvio() As ArrayList

            Get
                Select Case _VersionePlugIn
                    Case Is = eVersionePlugIn.Versione_1_5_0
                        Return _PlugInV1_5_0.DatiAzioniPreInvio
                    Case Else
                        Return New ArrayList
                End Select
            End Get

        End Property

        Public Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_5_0
                    Return _PlugInV1_5_0.CheckCondPSA(nCond2Check, cValue2Check)
                Case Else
                    Return False
            End Select

        End Function

        Public Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInV1_5_0.ExecActionPSA(nAct2Exec, cValue2Exec)
            End Select

        End Sub

        Public Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_5_0
                    Return _PlugInV1_5_0.CheckCondAPI(nCond2Check, cValue2Check)
                Case Else
                    Return False
            End Select

        End Function

        Public Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)

            Select Case _VersionePlugIn
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInV1_5_0.ExecActionAPI(cTipoExecAPI, nAct2Exec, cValue2Exec)
            End Select

        End Sub

    End Class

End Namespace