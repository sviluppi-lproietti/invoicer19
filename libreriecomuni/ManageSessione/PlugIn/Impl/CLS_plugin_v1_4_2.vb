﻿Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori

Namespace PlugIn

    Public Class CLS_plugin_v1_4_2
        Inherits CLS_PluginBase
        Implements IPlugIn

        Private _data_arch_ott As DS_appo_ao

        Private ReadOnly Property _PlugIn As PLUGIN_interfaceV1_4_2.IPLG_dati_lib

            Get
                Return CType(PluginBase, PLUGIN_interfaceV1_4_2.IPLG_dati_lib)
            End Get

        End Property

        Public Sub New(ByVal p As Object, ByVal fo As CLS_FileFolderDic, ByVal fog As CLS_FileFolderDic, ByVal fi As CLS_FileFolderDic, ByVal ol As Dictionary(Of String, CLS_oledbconnstring))
            MyBase.New(p, fo, fog, fi, ol)

            Versione = eVersionePlugIn.Versione_1_4_2

        End Sub

        Public Overrides Property CodiceModulo As Integer

            Get
                Return MyBase.CodiceModulo
            End Get
            Set(ByVal value As Integer)
                MyBase.CodiceModulo = value
                If value = eCodiceModuloFix.MOD_ArchOtt Then
                    _PlugIn.CodiceModulo = 1
                Else
                    _PlugIn.CodiceModulo = value
                End If
            End Set

        End Property

        Public Overrides ReadOnly Property Descrizione As String

            Get
                Return _PlugIn.NomePlugIn
            End Get

        End Property

        Public Overrides ReadOnly Property LinkField_QuickDS_FullDS() As String

            Get
                Return _PlugIn.LinkField_QuickDS_FullDS()
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataset() As DataSet

            Get
                Dim dsTMP As DataSet

                If CodiceModulo = eCodiceModuloFix.MOD_ArchOtt Then
                    dsTMP = _data_arch_ott
                Else
                    dsTMP = _PlugIn.FullDataset
                End If
                Return dsTMP
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataSet_MainTable() As DataTable

            Get
                Return Me.FullDataset.Tables(Me.FullDataSet_MainTableName)
            End Get

        End Property

        Public Overrides ReadOnly Property FullDataSet_MainTableName() As String

            Get
                Dim cTmp As String

                If CodiceModulo = eCodiceModuloFix.MOD_ArchOtt Then
                    cTmp = "TBL_arch_ott_fatt"
                Else
                    cTmp = _PlugIn.FullDataSet_MainTableName
                End If
                Return cTmp
            End Get

        End Property

        Public Overrides Sub Carica_FullDataset(ByVal nRecordDaCaricare As Integer, ByVal cCodiceServizio As String)
            Dim dr_aof As DS_appo_ao.TBL_arch_ott_fattRow
            Dim nTmp As Integer

            _PlugIn.LoadFullDataset(nRecordDaCaricare)
            If CodiceModulo = eCodiceModuloFix.MOD_ArchOtt Then
                If Me.TipoPlugIn = "PLG_UE_elettrico_lib.PLG_UE_elettrico_lib" Or Me.TipoPlugIn = "PLG_UE_gas.PLG_UE_gas" Then
                    _data_arch_ott = New DS_appo_ao
                    dr_aof = _data_arch_ott.TBL_arch_ott_fatt.NewRow
                    dr_aof.AOF_cod_cli = _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_cod_cli")
                    dr_aof.AOF_nome = _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_nome")
                    dr_aof.AOF_rec_nome = _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_nome")

                    dr_aof.AOF_par_iva = _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_par_iva")
                    dr_aof.AOF_cod_fis = _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_cod_fis")

                    dr_aof.AOF_indirizzo_rec = String.Concat(_PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_sito_1"), " ", _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_sito_2"))
                    dr_aof.AOF_numciv_rec = String.Concat(_PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_sito_3"), " ", _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_sito_4"))
                    dr_aof.AOF_citta_rec = String.Concat(_PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_sito_5"), " ", _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_sito_6"))
                    dr_aof.AOF_cap_rec = _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_sito_7")
                    dr_aof.AOF_prov_rec = _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_sito_8")
                    If _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_nazione").ToString.Trim = "" Then
                        dr_aof.AOF_nazione_rec = "Italia"
                    Else
                        dr_aof.AOF_nazione_rec = _PlugIn.FullDataset.Tables("TBL_main").Rows(0)("MAI_rec_nazione")
                    End If
                    dr_aof.AOF_data_emiss = New DateTime(2012, 1, 30) '_PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_emissione")
                    dr_aof.AOF_barcode = String.Concat(cCodiceServizio, _PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_anno"), _PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_numero").ToString().PadLeft(9, "0"))
                    If IsNumeric(dr_aof.AOF_barcode) Then
                        nTmp = dr_aof.AOF_barcode Mod 93
                        dr_aof.AOF_barcode = String.Concat(dr_aof.AOF_barcode, nTmp.ToString.PadLeft(2, "0"))
                    End If

                    dr_aof.AOF_numero_fattura = String.Concat(_PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_anno"), "/", _
                                                          _PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_registro_iva"), "/", _
                                                          _PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_numero"))
                    dr_aof.AOF_progressivo = _PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_numero")
                    dr_aof.AOF_prefix_doc = String.Concat("01_", _PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_anno"), "_", _
                                                                 _PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_registro_iva"), "_", _
                                                                 _PlugIn.FullDataset.Tables("TBL_dati_fatt").Rows(0)("DFA_numero"))
                    _data_arch_ott.TBL_arch_ott_fatt.Rows.Add(dr_aof)
                End If
            End If

        End Sub

        Public Overrides ReadOnly Property MessaggioAvviso() As String

            Get
                Return _PlugIn.MessaggioAvviso
            End Get

        End Property

        Public Overrides WriteOnly Property AlimentazioneImbustatrice() As String

            Set(ByVal value As String)
                _PlugIn.FeedString = value
            End Set

        End Property

        Public Overrides Sub GoToRecordNumber(ByVal nRow As Integer)

            _PlugIn.GoToRecordNumber(nRow)

        End Sub

        Public Overrides Sub AggiornaQuickDataset()

            _PlugIn.UpdateQuickDatasetFile()

        End Sub

        Public Overrides Function CheckDatiSessione(ByVal _DatiSessione As ArrayList) As Boolean

            Return _PlugIn.CheckDatiSessione(_DatiSessione)

        End Function

        Public Overrides Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String)

            _PlugIn.SetSelectable(dr, cFilter)

        End Sub

        Public Overrides Sub SetPrintable(ByVal dr As DataRow, ByVal cSelectFromPlugInType As String, ByVal cParamSelez As String)

            _PlugIn.SetPrintable(dr, cSelectFromPlugInType)

        End Sub

        Public Overrides ReadOnly Property ListaControlliFormNuovaSessione()

            Get
                Return _PlugIn.ListaControlliFormNuovaSessione
            End Get

        End Property

        Public Overrides ReadOnly Property ElementiMenuDedicati() As ArrayList

            Get
                Return _PlugIn.ElementiMenuDedicati()
            End Get

        End Property

        Public Overrides Sub ImpostaDGVDati(ByVal DGV As System.Windows.Forms.DataGridView)

            _PlugIn.ImpostaDGVDati(DGV)

        End Sub

        Public Overrides Function GetSelectedRow(ByVal _TipoRic As String, ByVal _ValueRic As String) As String()

            Return _PlugIn.GetSelectedRow(_TipoRic, _ValueRic)

        End Function

        Public Overrides WriteOnly Property FiltroSelezione() As String

            Set(ByVal value As String)
                _PlugIn.FiltroSelezione = value
            End Set

        End Property

        Public Overrides Function PostStampa() As ArrayList

            Return _PlugIn.PostStampa

        End Function

        Public Overrides WriteOnly Property SezioneDati() As Xml.XmlNode

            Set(ByVal value As Xml.XmlNode)
                _PlugIn.SezioneDati = value
            End Set

        End Property

        Public Overrides WriteOnly Property DBGiriConnectionString() As String

            Set(ByVal value As String)
                _PlugIn.DBGiriConnectionString = value
            End Set

        End Property

        Public Overrides Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList, ByVal xdSessione As Xml.XmlDocument)

            Try
                _PlugIn.CreaDataBaseSessione(_DatiSessione)
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Overrides WriteOnly Property ListaGiri() As ArrayList

            Set(ByVal value As ArrayList)
                _PlugIn.ListaGiri = value
            End Set

        End Property

        Public Overrides ReadOnly Property NuoviToponimi() As Boolean

            Get
                Return _PlugIn.NuoviToponimi
            End Get

        End Property

        Public Overrides ReadOnly Property TipoPlugIn As String

            Get
                Return _PlugIn.GetType.ToString()
            End Get

        End Property

        Public Overrides ReadOnly Property Modello As String

            Get
                Return _PlugIn.NomePlugIn
            End Get

        End Property

    End Class

End Namespace