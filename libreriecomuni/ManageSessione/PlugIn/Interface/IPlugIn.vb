﻿Namespace PlugIn

    Public Interface IPlugIn

        Sub Carica_QuickDataset()

        ReadOnly Property QuickDataset() As DataSet
        ReadOnly Property QuickDataset_MainTable() As DataTable
        ReadOnly Property QuickDataset_MainTableKey() As String

    End Interface

End Namespace