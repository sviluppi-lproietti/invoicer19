Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Esportazione

    Public Class GestioneEsportazione

        Public Property DatiEsportazione As EsportazioneEntity
        Public Property IsLoaded As Boolean

        '
        ' Contiene l'elenco dei fattori di correzione del modulo.
        '

        Public Property ModuloOriginale As Xml.XmlDocument
        Public Property ModuloPerStampa As Xml.XmlDocument
        Public Property PosizioniFisse() As Dictionary(Of String, Decimal())

        ReadOnly Property BarcodeX As Integer

            Get
                Dim xnNode As Xml.XmlNode
                Dim nCoord As Integer

                nCoord = 7
                If Not _xdModulo.SelectSingleNode("Modulo/Format/ControlloImbustatrice/Posizione/X") Is Nothing Then
                    xnNode = _xdModulo.SelectSingleNode("Modulo/Format/ControlloImbustatrice/Posizione/X")
                    nCoord = xnNode.InnerText
                End If
                Return nCoord
            End Get

        End Property

        ReadOnly Property BarcodeY As Integer

            Get
                Dim xnNode As Xml.XmlNode
                Dim nCoord As Integer

                nCoord = 300
                If Not _xdModulo.SelectSingleNode("Modulo/Format/ControlloImbustatrice/Posizione/Y") Is Nothing Then
                    xnNode = _xdModulo.SelectSingleNode("Modulo/Format/ControlloImbustatrice/Posizione/Y")
                    nCoord = xnNode.InnerText
                End If
                Return nCoord
            End Get

        End Property

        ReadOnly Property MessaggioErrore() As String

            Get
                Dim cErroreRtn As String

                If _Errore Is Nothing Then
                    cErroreRtn = "Descrizione dell'errore non disponibile"
                Else
                    cErroreRtn = String.Concat(_Errore.Message, vbLf, _Errore.StackTrace)
                End If
                Return cErroreRtn
            End Get

        End Property

        ReadOnly Property DefaultOrientamento()

            Get
                Return _xdModulo.SelectSingleNode(MES_Costanti.itm_Orientamento).InnerText.ToLower
            End Get

        End Property

        ReadOnly Property FronteRetr1o() As String

            Get
                Return _xdModulo.SelectSingleNode(MES_Costanti.itm_FronteRetro).InnerText
            End Get

        End Property

        ReadOnly Property ModuloDaStampare() As Xml.XmlDocument

            Get
                Return _xdModuloPerStampa
            End Get

        End Property

        ReadOnly Property NumeroPagineModulo()

            Get
                Return _xdModulo.SelectNodes(MES_Costanti.itm_Page_complete).Count()
            End Get

        End Property

        Private _Folders As CLS_FileFolderDic

        Private _IsOkEsporta As Boolean
        Private _IsLoadedModulo As Boolean
        Private _IsLoadedEsportazione As Boolean
        Private _NomeStampanteCartacea As String
        Private _NomeStampanteOttica As String
        Private _xdModulo As Xml.XmlDocument
        Private _xdEsporta As Xml.XmlDocument
        Private _xdModuloPerStampa As Xml.XmlDocument
        Private _xdEsportaWrk As Xml.XmlDocument
        Private _ProgressItem As Integer
        Private _Errore As Exception
        Private _SplitedAllegato As ArrayList

        Sub New(ByVal xiModulo As Xml.XmlNode, ByVal cFolder As String)
            Dim cTmp As String

            _xdModulo = Nothing
            _Folders = New CLS_FileFolderDic
            _Folders.Add("ModuliFLD", cFolder)

            '
            ' Carico i dati descrittivi del modulo.
            '
            Carica_DatiModulo(xiModulo)

            If DatiEsportazione.ModuloUnificato Then

            Else
                '
                ' Se il modulo � descritto in un file allora lo carico e ne ricavo le informazioni che mi occorrono.
                'p
                Carica_Modulo()

                If DatiEsportazione.DaEsportazione Then
                    CaricaEsportazione()
                End If
            End If

        End Sub

        Public Sub Carica_Modulo()

            Try
                '
                ' Il nome del file del modulo deve essere caricato.
                '
                IsLoaded = True
                If DatiEsportazione.FileName > "" Then
                    _xdModulo = Carica_FileModulo()
                    If IsLoaded Then
                        ' 
                        ' Recupera le posizioni fisse dal modulo.                               
                        ' 
                        Carica_PosizioniFisse()

                        If NodeNotNull(_xdModulo.SelectSingleNode(MES_Costanti.itm_FronteRetro)) Then
                            DatiEsportazione.StampaFronteRetro = _xdModulo.SelectSingleNode(MES_Costanti.itm_FronteRetro).InnerText = "1"
                        Else
                            DatiEsportazione.StampaFronteRetro = False
                        End If

                        '
                        ' Recupero lo switch per la disabilitazione fronte retro
                        '
                        If NodeNotNull(_xdModulo.SelectSingleNode(MES_Costanti.itm_DisabilitaFronteRetro)) Then
                            DatiEsportazione.DisabilitaFronteRetro = _xdModulo.SelectSingleNode(MES_Costanti.itm_DisabilitaFronteRetro).InnerText = "1"
                            DatiEsportazione.DisabilitaFronteRetro = False
                        End If

                    End If
                End If
            Catch ex As Exception
                _xdModulo = Nothing
                IsLoaded = False
                _Errore = ex
            End Try

        End Sub

        Private Sub Carica_PosizioniFisse()
            Dim nPosX As Decimal
            Dim nPosY As Decimal
            Dim cPosS As String

            PosizioniFisse = New Dictionary(Of String, Decimal())
            If Not _xdModulo.SelectSingleNode(MES_Costanti.itm_pos_fisse) Is Nothing Then
                For Each xmlItem As Xml.XmlNode In _xdModulo.SelectSingleNode(MES_Costanti.itm_pos_fisse).SelectNodes("Posizione")
                    cPosS = xmlItem.SelectSingleNode("Name").InnerText
                    nPosX = -1
                    nPosY = -1
                    If Not xmlItem.SelectSingleNode("ValoreX") Is Nothing Then
                        nPosX = xmlItem.SelectSingleNode("ValoreX").InnerText
                    End If
                    If Not xmlItem.SelectSingleNode("ValoreY") Is Nothing Then
                        nPosY = xmlItem.SelectSingleNode("ValoreY").InnerText
                    End If
                    PosizioniFisse.Add(cPosS, New Decimal() {nPosX, nPosY})
                Next
            End If

        End Sub

        Public Function CheckModuloModified() As Boolean
            Dim xdWorkRunn As Xml.XmlDocument
            Dim xdWork As New Xml.XmlDocument
            Dim lModified As Boolean

            If DatiEsportazione.FileName > "" Then
                xdWorkRunn = _xdModulo.Clone
                If Not xdWorkRunn.SelectSingleNode("Modulo/FattoreCorrezione") Is Nothing Then
                    xdWorkRunn.SelectSingleNode("Modulo").RemoveChild(xdWorkRunn.SelectSingleNode("Modulo/FattoreCorrezione"))
                End If

                xdWork = Carica_FileModulo()
                If Not xdWork.SelectSingleNode("Modulo/FattoreCorrezione") Is Nothing Then
                    xdWork.SelectSingleNode("Modulo").RemoveChild(xdWork.SelectSingleNode("Modulo/FattoreCorrezione"))
                End If
                xdWork = xdWork.Clone
                lModified = xdWork.OuterXml <> xdWorkRunn.OuterXml
            Else
                lModified = False
            End If
            Return lModified

        End Function

        Public Function RecuperaModuliNecessari() As ArrayList
            Dim xnPage As Xml.XmlNode
            Dim aRtn As ArrayList

            aRtn = New ArrayList
            For Each xnPage In _xdModulo.SelectNodes(MES_Costanti.itm_Page_complete)
                If xnPage.SelectSingleNode("traydes") Is Nothing Then
                    aRtn.Add(New String() {xnPage.SelectSingleNode("tray").InnerText, String.Concat("Modulo ", xnPage.SelectSingleNode("tray").InnerText)})
                Else
                    aRtn.Add(New String() {xnPage.SelectSingleNode("tray").InnerText, xnPage.SelectSingleNode("traydes").InnerText})
                End If
            Next
            For Each xnPage In _xdModulo.SelectNodes(MES_Costanti.itm_GenPage_complete)
                If xnPage.SelectSingleNode("traydes") Is Nothing Then
                    aRtn.Add(New String() {xnPage.SelectSingleNode("tray").InnerText, String.Concat("Modulo ", xnPage.SelectSingleNode("tray").InnerText)})
                Else
                    aRtn.Add(New String() {xnPage.SelectSingleNode("tray").InnerText, xnPage.SelectSingleNode("traydes").InnerText})
                End If
            Next
            Return aRtn

        End Function

        Public ReadOnly Property PageToGenerate() As Boolean

            Get
                Return _xdModuloPerStampa.SelectNodes("Modulo/Pages/GenPage").Count > 0
            End Get

        End Property

        Public ReadOnly Property AllegatoDaDividere() As String

            Get
                Dim cFileName As String
                Dim xnGenPage As Xml.XmlNode
                Dim xnItem As Xml.XmlNode

                If _xdModuloPerStampa.SelectNodes("Modulo/Pages/GenPage").Count > 0 Then
                    xnGenPage = _xdModuloPerStampa.SelectNodes("Modulo/Pages/GenPage")(0)
                    xnItem = xnGenPage.SelectSingleNode("GenItem")
                    ' cFileName = String.Concat(DatiModulo.ModuloFld, xnItem.SelectSingleNode("value").InnerText)
                Else
                    cFileName = ""
                End If
                Return cFileName
            End Get

        End Property

        Public Property SplitedAllegato() As ArrayList

            Get
                Return _SplitedAllegato
            End Get
            Set(ByVal value As ArrayList)
                _SplitedAllegato = value
            End Set

        End Property

        Public Sub GeneraPagine()
            Dim xnItemGenerato As Xml.XmlNode
            Dim xnGenPage As Xml.XmlNode
            Dim xnTmp1 As Xml.XmlNode
            Dim xnTmp2 As Xml.XmlNode
            Dim xnTmp3 As Xml.XmlNode
            Dim nPageSplit As Integer
            Dim cFileName As String

            xnGenPage = _xdModuloPerStampa.SelectNodes("Modulo/Pages/GenPage")(0)
            If _SplitedAllegato.Count > 0 Then
                nPageSplit = 1
                For Each cFileName In _SplitedAllegato
                    xnItemGenerato = _xdModuloPerStampa.CreateElement("Page")
                    For Each xnTmp1 In xnGenPage.ChildNodes
                        If xnTmp1.Name = "tray" And nPageSplit > 1 Then
                            xnTmp1.InnerText = xnTmp1.InnerText.Replace("*", "")
                        End If
                        If xnTmp1.Name = "GenItem" Then
                            xnTmp2 = _xdModuloPerStampa.CreateElement("item")
                            For Each xnTmp3 In xnTmp1.ChildNodes
                                If xnTmp3.Name = "value" Then
                                    xnTmp3.InnerText = cFileName.Replace(DatiEsportazione.Folder, "")
                                End If
                                xnTmp2.AppendChild(xnTmp3.Clone)
                            Next
                            xnItemGenerato.AppendChild(xnTmp2.Clone)
                        Else
                            xnItemGenerato.AppendChild(xnTmp1.Clone)
                        End If
                    Next
                    _xdModuloPerStampa.SelectSingleNode("Modulo/Pages").InsertBefore(xnItemGenerato, xnGenPage)
                    nPageSplit += 1
                Next
            End If
            _xdModuloPerStampa.SelectSingleNode("Modulo/Pages").RemoveChild(xnGenPage)

        End Sub

        Public Sub PreparaperStampa()

            _xdModuloPerStampa = _xdModulo.Clone

        End Sub

        ReadOnly Property ModuloSenzaSelezioneNecessaria() As Boolean

            Get
                '  Return DatiModulo.ModuloSenzaSelezioneNecessaria
            End Get

        End Property

        ReadOnly Property ModuloCheckSum() As String

            Get
                Return HashString(_xdModulo.OuterXml)
            End Get

        End Property

        ReadOnly Property ModuloEsportabile() As Boolean

            Get
                '    Return DatiModulo.ModuloEsportabile
            End Get

        End Property

        Public Sub CaricaEsportazione()

            _xdEsporta = Nothing
            Try
                LoadFileEsportazione()
                If _IsLoadedEsportazione Then
                    _xdEsporta = _xdEsportaWrk.Clone
                End If
                _IsOkEsporta = True
            Catch ex As Exception
                _xdEsporta = Nothing
                _IsOkEsporta = False
                _Errore = ex
            End Try

        End Sub

        Private Function LoadFileEsportazione() As Xml.XmlDocument

            Try
                _xdEsportaWrk = New Xml.XmlDocument
                _xdEsportaWrk.Load(DatiEsportazione.FileName)
                _IsLoadedEsportazione = True
            Catch ex As Exception
                _IsLoadedEsportazione = True
                _xdEsportaWrk = Nothing
                _Errore = ex
            End Try
            Return _xdEsportaWrk

        End Function

        ReadOnly Property EsportaModulo() As Xml.XmlDocument

            Get
                Return _xdEsporta
            End Get

        End Property

        Private Sub Carica_DatiModulo(ByVal xiModulo As Xml.XmlNode)
            Dim cTmp As String

            DatiEsportazione = New EsportazioneEntity(Me._Folders("EsportazioniFLD"))   'CaricaDatiModulo(xiWork)

            '
            ' Recupera il codice del modulo
            '
            If NodeNotNull(xiModulo.SelectSingleNode("codice")) Then
                DatiEsportazione.Codice = GetValueFromXML(xiModulo, "codice")
            End If
            DatiEsportazione.ModuloUnificato = DatiEsportazione.Codice = 900
            DatiEsportazione.DaStampare = DatiEsportazione.Codice <> 900

            '
            ' Recupera il nome del modulo
            '
            If NodeNotNull(xiModulo.SelectSingleNode("nome")) Then
                DatiEsportazione.Nome = xiModulo.SelectSingleNode("nome").InnerText
            End If

            '
            ' Recupera i filtri da applicare al modulo.
            '
            If NodeNotNull(xiModulo.SelectSingleNode("filtro")) Then
                DatiEsportazione.Filtro = xiModulo.SelectSingleNode("filtro").InnerText
            End If

            '
            ' Recupero il cassetto dell'imbustatrice
            '
            If NodeNotNull(xiModulo.SelectSingleNode("feed")) Then
                DatiEsportazione.AlimentazioneImbustatrice = xiModulo.SelectSingleNode("feed").InnerText
            End If

            '
            ' Recupera il nome del file del modulo.
            '
            If NodeNotNull(xiModulo.SelectSingleNode("file")) Then
                DatiEsportazione.FileName = xiModulo.SelectSingleNode("file").InnerText
            End If
            DatiEsportazione.DaStampare = DatiEsportazione.DaStampare And DatiEsportazione.FileName > ""

            '
            ' Recupera il nome del modulo.
            '
            If NodeNotNull(xiModulo.SelectSingleNode("tipomodulo")) Then
                cTmp = GetValueFromXML(xiModulo, "tipomodulo", "^")
                If cTmp = "m" Then
                    DatiEsportazione.Tipologia = eTipologiaModulo.TIM_Master
                Else
                    If cTmp = "x" Then

                    End If
                    DatiEsportazione.Tipologia = eTipologiaModulo.TIM_Slave
                End If
            End If

            '
            ' Recupera il nome del modulo.
            '
            If NodeNotNull(xiModulo.SelectSingleNode("feed")) Then
                DatiEsportazione.CassettoDefault = xiModulo.SelectSingleNode("feed").InnerText
            End If

        End Sub

#Region "Procedura per la gestione del caricamento del modulo"

        '
        ' Procedura per il caricamento del modulo e sua compatibilizzazione.
        '
        Private Function Carica_FileModulo() As Xml.XmlDocument
            Dim xdWork As Xml.XmlDocument

            xdWork = New Xml.XmlDocument
            xdWork.Load(DatiEsportazione.FileName)

            '
            ' Rimuovo le pagine che non debbono essere stampate.
            '
            For Each xnItem As Xml.XmlNode In xdWork.SelectNodes(MES_Costanti.itm_Page_complete)
                If GetAttrFromXML(xnItem, "toprint", "1") = 0 Then
                    xdWork.SelectSingleNode(MES_Costanti.itm_Pages).RemoveChild(xnItem)
                End If
            Next

            If GetValueFromXML(xdWork, "Modulo/Versione", "1.0") < "1.2" Then
                CompatibilizzaModulo(xdWork.SelectSingleNode("Modulo"))
            End If
            xdWork = Compatibilizza_ArchiviazioneOttica(xdWork)
            Return xdWork.Clone

        End Function

        Private Sub CompatibilizzaModulo(ByVal xnWork1 As Xml.XmlNode)
            Dim xnWork2 As Xml.XmlNode

            ' ********************************************************************* '
            ' Riscrive nella nuova modalit� le condizioni del modulo.               '
            ' ********************************************************************* '
            xnWork2 = xnWork1.FirstChild
            While Not (xnWork2 Is Nothing)
                If xnWork2.Name.ToLower = "default" Then
                    CompatibilizzaDefault(xnWork2)
                End If
                If xnWork2.Name.ToLower = "allpageitem" Then
                    ControllaCondizione(xnWork2)
                    CompatibilizzaModulo(xnWork2)
                    ControllaCondizione2(xnWork2)
                End If
                If xnWork2.Name.ToLower = "format" Then
                    CompatibilizzaModulo(xnWork2)
                End If
                If xnWork2.Name.ToLower = "controlloimbustatrice" Then
                    ControllaCondizione(xnWork2)
                    CompatibilizzaModulo(xnWork2)
                    ControllaCondizione2(xnWork2)
                End If
                If xnWork2.Name.ToLower = "pages" Then
                    ControllaCondizione(xnWork2)
                    CompatibilizzaModulo(xnWork2)
                    ControllaCondizione2(xnWork2)
                End If
                If xnWork2.Name.ToLower = "page" Then
                    ControllaCondizione(xnWork2)
                    CompatibilizzaModulo(xnWork2)
                    ControllaCondizione2(xnWork2)
                End If
                If xnWork2.Name.ToLower = "blocco" Then
                    InserisciProgressivo(xnWork2)
                    ControllaCondizione(xnWork2)
                    ModificaFiltroBlocco(xnWork2)
                    EliminaSeNonVuoto(xnWork2)
                    CompatibilizzaModulo(xnWork2)
                    ControllaCondizione2(xnWork2)
                End If
                If xnWork2.Name.ToLower = "intestazione" Then
                    InserisciProgressivo(xnWork2)
                    ControllaCondizione(xnWork2)
                    CompatibilizzaModulo(xnWork2)
                    ControllaCondizione2(xnWork2)
                End If
                If xnWork2.Name.ToLower = "corpo" Then
                    InserisciProgressivo(xnWork2)
                    ControllaCondizione(xnWork2)
                    CompatibilizzaModulo(xnWork2)
                    ControllaCondizione2(xnWork2)
                End If
                If xnWork2.Name.ToLower = "piediblocco" Then
                    InserisciProgressivo(xnWork2)
                    ControllaCondizione(xnWork2)
                    CompatibilizzaModulo(xnWork2)
                    ControllaCondizione2(xnWork2)
                End If
                If xnWork2.Name.ToLower = "sezione" Then
                    InserisciProgressivo(xnWork2)
                    ControllaCondizione(xnWork2)
                    ExcludedPage(xnWork2)
                    CompatibilizzaModulo(xnWork2)
                    ControllaCondizione2(xnWork2)
                End If
                If xnWork2.Name.ToLower = "item" Then
                    InserisciProgressivo(xnWork2)
                    ControllaCondizione(xnWork2)
                    ExcludedPage(xnWork2)
                    ControllaCondizione2(xnWork2)
                    CompatibilizzaValueItem(xnWork2)
                End If

                xnWork2 = xnWork2.NextSibling
            End While

        End Sub

        Private Sub CompatibilizzaValueItem(ByVal xnValues As Xml.XmlNode)
            Dim xnTmp As Xml.XmlNode
            Dim xnWork As Xml.XmlNode

            If Not xnValues.SelectSingleNode("value") Is Nothing Then
                xnTmp = xnValues.SelectSingleNode("value").Clone
                xnValues.RemoveChild(xnValues.SelectSingleNode("value"))
                xnWork = xnValues.OwnerDocument.CreateElement("values")
                xnWork.InnerXml = String.Concat("<valuesItem>", xnTmp.OuterXml, "</valuesItem>")
                xnValues.AppendChild(xnWork)
            End If

        End Sub

        Private Sub ControllaCondizione(ByVal xnWork As Xml.XmlNode)
            Dim xnWork2 As Xml.XmlNode
            Dim xnWork3 As Xml.XmlNode
            Dim cTmp As String
            Dim aData As String()

            If Not xnWork.SelectSingleNode("Condition") Is Nothing Then
                xnWork2 = xnWork.OwnerDocument.CreateElement("Conditions")
                xnWork3 = xnWork.OwnerDocument.CreateElement("Condition_row")
                cTmp = xnWork.SelectSingleNode("Condition").InnerText
                If cTmp.StartsWith("recordnumber") Then
                    aData = cTmp.Split(";")
                    cTmp = String.Concat(aData(0), ";", aData(1), ";;;", aData(2))
                End If
                xnWork3.InnerText = String.Concat(";", cTmp)
                xnWork2.AppendChild(xnWork3.Clone)
                xnWork.InsertBefore(xnWork2, xnWork.SelectSingleNode("Condition"))
                xnWork.RemoveChild(xnWork.SelectSingleNode("Condition"))
            End If

        End Sub

        Private Sub InserisciProgressivo(ByVal xnWork As Xml.XmlNode)
            Dim xnWork2 As Xml.XmlNode

            _ProgressItem += 1
            xnWork2 = xnWork.OwnerDocument.CreateElement("CodiceItem")
            xnWork2.InnerText = _ProgressItem
            xnWork.InsertBefore(xnWork2, xnWork.FirstChild)

        End Sub

        Private Sub EliminaSeNonVuoto(ByVal xnWork As Xml.XmlNode)
            Dim xnWork2 As Xml.XmlNode
            Dim xnWork3 As Xml.XmlNode

            If Not xnWork.Attributes("SeNonVuoto") Is Nothing Then
                xnWork.Attributes.RemoveNamedItem("SeNonVuoto")
                If xnWork.SelectSingleNode("Conditions") Is Nothing Then
                    xnWork2 = xnWork.OwnerDocument.CreateElement("Conditions")
                    xnWork3 = xnWork.OwnerDocument.CreateElement("Condition_row")
                    xnWork3.InnerText = ""
                    xnWork.InsertAfter(xnWork2, xnWork.SelectSingleNode("CodiceItem"))
                Else
                    xnWork2 = xnWork.SelectSingleNode("Conditions")
                    xnWork3 = xnWork.OwnerDocument.CreateElement("Condition_row")
                    xnWork3.InnerText = "and"
                End If
                xnWork3.InnerText = String.Concat(xnWork3.InnerText, ";recordnumber;GT;;;0;")
                xnWork2.AppendChild(xnWork3.Clone)
            End If

        End Sub

        Private Sub ExcludedPage(ByVal xnWork As Xml.XmlNode)
            Dim xnWork2 As Xml.XmlNode
            Dim xnWork3 As Xml.XmlNode
            Dim xnWork4 As Xml.XmlNode

            If Not xnWork.SelectSingleNode("excludedpage") Is Nothing Then
                xnWork4 = xnWork.SelectSingleNode("excludedpage")
                If xnWork.SelectSingleNode("Conditions") Is Nothing Then
                    xnWork2 = xnWork.OwnerDocument.CreateElement("Conditions")
                    xnWork3 = xnWork.OwnerDocument.CreateElement("Condition_row")
                    xnWork3.InnerText = ""
                    xnWork.InsertAfter(xnWork2, xnWork.SelectSingleNode("CodiceItem"))
                Else
                    xnWork2 = xnWork.SelectSingleNode("Conditions")
                    xnWork3 = xnWork.OwnerDocument.CreateElement("Condition_row")
                    xnWork3.InnerText = "and"
                End If
                xnWork3.InnerText = String.Concat(xnWork3.InnerText, ";page;NE;;;", xnWork4.InnerText, ";")
                xnWork2.AppendChild(xnWork3.Clone)
            End If

        End Sub

        Private Sub ControllaCondizione2(ByVal xnWork As Xml.XmlNode)
            Dim xnWork2 As Xml.XmlNode
            Dim cTmp As String
            Dim aData As String()
            Dim cTmp2 As String

            If Not xnWork.SelectSingleNode("Conditions") Is Nothing Then
                xnWork2 = xnWork.SelectSingleNode("Conditions").FirstChild
                While Not xnWork2 Is Nothing
                    cTmp = String.Concat(xnWork2.InnerText, ";")
                    aData = cTmp.Split(";")

                    If aData(2) = "" Then
                        aData(2) = "EQ"
                    End If
                    aData(6) = aData(5)
                    If IsNumeric(aData(5)) Then
                        aData(5) = "n"
                    Else
                        aData(5) = "c"
                    End If
                    cTmp2 = ";"
                    For Each cTmp3 As String In aData
                        cTmp2 = String.Concat(cTmp2, cTmp3.Replace("{\", "").Replace("/}", ""), ";")
                    Next
                    cTmp2 = cTmp2.Substring(0, cTmp2.Length - 1)

                    xnWork2.InnerText = cTmp2
                    xnWork2 = xnWork2.NextSibling
                End While
            End If

        End Sub

        Private Sub CompatibilizzaDefault(ByVal xnWork As Xml.XmlNode)
            Dim xnWork3 As Xml.XmlNode
            Dim xnWork4 As Xml.XmlNode

            xnWork3 = xnWork.OwnerDocument.CreateElement("PageSettings")
            If Not xnWork.SelectSingleNode("MaxPageLenght") Is Nothing Then
                xnWork4 = xnWork.SelectSingleNode("MaxPageLenght").Clone
                xnWork3.PrependChild(xnWork4)
                xnWork.RemoveChild(xnWork.SelectSingleNode("MaxPageLenght"))
            End If

        End Sub

        Private Sub ModificaFiltroBlocco(ByVal xnWork As Xml.XmlNode)
            Dim xnWork3 As Xml.XmlNode
            Dim cTmp As String
            Dim nVirg As Integer

            If Not xnWork.SelectSingleNode("BLKFilter") Is Nothing Then
                cTmp = xnWork.SelectSingleNode("BLKFilter").InnerText
                nVirg = 0
                For Each cCar As Char In cTmp.ToCharArray
                    If cCar = ";" Then
                        nVirg += 1
                    End If
                Next
                If nVirg = 3 Then
                    xnWork3 = xnWork.OwnerDocument.CreateElement("RigheVisualizzate")
                    xnWork3.InnerText = cTmp.Split(";")(3) * -1
                    xnWork.AppendChild(xnWork3)
                    xnWork.SelectSingleNode("BLKFilter").InnerText = cTmp.Substring(0, cTmp.LastIndexOf(";"))
                End If
            End If

        End Sub

        Private Function Compatibilizza_ArchiviazioneOttica(ByVal xdWork As Xml.XmlDocument) As Xml.XmlDocument
            Dim xnTmp1 As Xml.XmlNode
            Dim xnNode As Xml.XmlNode
            Dim xnTmp As Xml.XmlNode

            xnNode = Nothing
            If Not xdWork.SelectSingleNode("Modulo/ArchiviazioneOttica") Is Nothing Then
                xnNode = xdWork.SelectSingleNode("Modulo/ArchiviazioneOttica")

                If xnNode.HasChildNodes And xnNode.ChildNodes(0).NodeType = Xml.XmlNodeType.Text Then
                    xnTmp = xdWork.CreateElement("ArchOttItem")
                    xnTmp1 = xdWork.CreateElement("Descrizione")
                    xnTmp1.InnerText = "Primo Formato"
                    xnTmp.AppendChild(xnTmp1.Clone)
                    xnTmp1 = xdWork.CreateElement("FileName")
                    xnTmp1.InnerText = xnNode.InnerText
                    xnTmp.AppendChild(xnTmp1.Clone)
                    xnNode.InnerText = ""
                    xnNode.AppendChild(xnTmp)
                End If
            End If
            Return xdWork

        End Function

#End Region

    End Class

End Namespace