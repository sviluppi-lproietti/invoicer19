﻿'
' In questa classe vengono dichiarati tutti gli enumeratori utilizzati in questo progetto
'

Namespace Esportazione

    Public Enum eCodiceModuloFix
        MOD_SendEmail = 999
        MOD_ArchOtt = 998
    End Enum

    Public Enum eTipologiaModulo
        TIM_Master = 1
        TIM_Slave = 2
    End Enum

    Public Enum eTipoElemento
        TEL_nonspecificato
        TEL_sconosciuto
        TEL_pagina
        TEL_sezione
        TEL_blocco
        TEL_item
        TEL_fincatura
        TEL_corpo
        TEL_modulo
    End Enum

    Public Enum eSottoTipoItem
        NonImpostato = -1
        TipoNonGestito = 0
        BarCode = 1
        Fincatura = 2
        Immagine = 3
        Testo = 4
        TestoInvertito = 5
        Grafico1 = 6
        Grafico2 = 7
        Rettangolo = 8
        Linea = 9
    End Enum

    Public Enum eTipoSpecialeItem
        NonImpostato = -1
        TipoNonGestito = 0
        Titolo = 1
    End Enum

    Public Enum eTipoBarcode
        BC_2of5 = 1
        BC_128c = 2
        BC_DataMa = 3
        BC_GS1_128 = 4
        BC_QRCode = 5
    End Enum

    Public Enum eTipoAllineamento
        NonGestito = -1
        Sinistra = 1
        Centro = 2
        Destra = 3
        Giustificato = 4
        F24 = 5
        Centro2 = 6
    End Enum

    Public Enum eDirezione
        Orizzontale = 1
        Verticale = 2
        Verticale_BassoAlto = 3
    End Enum

End Namespace