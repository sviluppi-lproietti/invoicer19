Namespace Esportazione

    Public Class MES_Costanti

        'Public Const FIL_plugin = "PlugInFIL"
        'Public Const FIL_QuickDataSetIndex = "QuickDataSetIndexFIL"

        'Public Const FLD_sessioneroot = "SessioneRootFLD"

        Private Const itm_modulo = "Modulo"
        Private Const itm_formato = itm_modulo + "/Format"
        'Public Const itm_CodiceImbustatrice = itm_formato + "/ControlloImbustatrice/TipoCodice"
        'Public Const itm_ItemImb = itm_formato + "/ControlloImbustatrice/Item"
        Public Const itm_pos_fisse = itm_modulo + "/PosizioniFisse"
        'Public Const itm_PaginePari = itm_formato + "/PaginePari"
        Public Const itm_FronteRetro = itm_formato + "/StampaFronteRetro"
        Public Const itm_DisabilitaFronteRetro = itm_formato + "/disabilitaFronteRetro"
        'Public Const itm_AllBlack = itm_formato + "/AllBlack"
        Public Const itm_Orientamento = itm_formato + "/Orientamento"
        'Public Const itm_default = itm_modulo + "/Default"
        'Public Const itm_DefaultMaxBottom = itm_default + "/MaxPageLenght"
        Public Const itm_Pages = itm_modulo + "/Pages"
        Public Const itm_Page = "Page"
        Public Const itm_GenPage = "GenPage"
        Public Const itm_Page_complete = itm_Pages + "/" + itm_Page
        Public Const itm_GenPage_complete = itm_Pages + "/" + itm_GenPage
        Public Const itm_AllPagesItem = itm_modulo + "/AllPageItem"
        'Public Const itm_condition = "Condition"

    End Class

End Namespace