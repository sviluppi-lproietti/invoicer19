Public Class MST_costanti

    Private Const itm_modulo = "Modulo"
    Private Const itm_formato = itm_modulo + "/Format"
    Public Const itm_CodiceImbustatrice = itm_formato + "/ControlloImbustatrice/TipoCodice"
    Public Const itm_ItemImb = itm_formato + "/ControlloImbustatrice/Item"
    Public Const itm_PaginePari = itm_formato + "/PaginePari"
    Public Const itm_AllBlack = itm_formato + "/AllBlack"
    Public Const itm_default = itm_modulo + "/Default"
    Public Const itm_DefaultMaxBottom = itm_default + "/PageSettings/MaxPageLenght"
    Public Const itm_Pages = itm_modulo + "/Pages"
    Public Const itm_Page = "Page"
    Public Const itm_AllPagesItem = itm_modulo + "/AllPageItem"
    Public Const itm_conditions = "Conditions"
    Public Const itm_condition_row = "Condition_row"

    Public Const Versione_PDFCreator_Corretta = "1.2.0"

End Class
