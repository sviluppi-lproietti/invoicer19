Imports System.Reflection
Imports System.Linq
Imports System.Xml
Imports ISC.LibrerieComuni.ManageSessione.Entity
Imports ISC.LibrerieComuni.ManageSessione.Esportazione
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione.PlugIn
Imports ISC.LibrerieComuni.ManageSessione.MSE_Costanti
Imports ISC.LibrerieComuni.OggettiComuni
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori

Public Class GestioneSessione

    Public Property Errore As Exception
    Public Property FoldersGlobali As CLS_FileFolderDic
    Public Property Folders As CLS_FileFolderDic
    Public Property Files As CLS_FileFolderDic
    Public Property OleDb_ConnString As Dictionary(Of String, CLS_oledbconnstring)
    Public Property RecordErrCaricamentoGravi As Integer
    Public Property RecordErrCaricamentoLievi As Integer

    Private _PlugInAppDomain As AppDomain

    Public ReadOnly Property ListaStatisticheStampaGlobali() As List(Of StatisticheStampaEntity)

        Get
            Dim l As New List(Of StatisticheStampaEntity)

            For Each e As StatisticheStampaEntity In _StatisticheStampaGlobali.Values
                l.Add(e)
            Next
            Return l
        End Get

    End Property

    Public ReadOnly Property ListaStatisticheStampaParziali() As List(Of StatisticheStampaEntity)

        Get
            Dim l As New List(Of StatisticheStampaEntity)

            For Each e As StatisticheStampaEntity In _StatisticheStampaParziali.Values
                l.Add(e)
            Next
            Return l
        End Get

    End Property

    Public Property MessaggiProgress As MessaggiEntity

        Get
            Dim m As MessaggiEntity
            If _MessaggiProgress.MessaggiDalPlugin Then
                m = _PlugInSessione.MessaggiProgress
            Else
                m = _MessaggiProgress
            End If
            Return m
        End Get
        Set(ByVal value As MessaggiEntity)
            _MessaggiProgress = value
        End Set

    End Property

    Public Property ModuloUnificato As Modulo.GestioneModulo

    Public ReadOnly Property NuoviToponimi As Boolean

        Get
            Return _Access2PlugIn.NuoviToponimi
        End Get

    End Property

    Public ReadOnly Property CheckSumDatiFissi As String

        Get
            Return HashFile(Me.Files("PlugInDatiFissiFIL"))
        End Get

    End Property

    Public ReadOnly Property CheckSumPlugIn As String

        Get
            Return HashFile(Me.Files(FIL_plugin))
        End Get

    End Property

    Private Property _Access2PlugIn As CLS_plugin_accesso
    Private Property _PlugInSessione As CLS_PluginBase
    Private Property _CheckDatiVar As Boolean
    Private Property _data_sped_email As DS_dati_sped_email
    Private Property _FeedString As String
    Private Property _MessaggiProgress As MessaggiEntity
    Private Property _DatiModuloAttivo As ModuloEntity
    Private Property _RecordErroreStampa As Integer
    Private Property _RecordSelezionati As Integer
    Private Property _SessionePlugInEmailDataFileName As String
    Private Property _StatisticheStampaGlobali As Dictionary(Of Integer, StatisticheStampaEntity)
    Private Property _StatisticheStampaParziali As Dictionary(Of Integer, StatisticheStampaEntity)
    Private Property _xdSessione As Xml.XmlDocument
    Private Property _TipoAmbiente As eTipoApplicazione
    Private Property _VersioneSistemaOperativo As String

    '
    ' Questa procedura si occupa di verificare che il db con i dati di stampa/invio contenga almeno un record.
    '
    Public Function IsEmptyFullDataset() As Boolean

        Return Me.FullDataSet_MainTable.Rows.Count = 0

    End Function

    Public ReadOnly Property RecordFullDatasetMainTable As Integer

        Get
            Return FullDataSet_MainTable.Rows.Count
        End Get

    End Property

    WriteOnly Property DBGiriConnectionString() As String

        Set(ByVal value As String)
            LogHelper.Logger.Debug("La variabile _Access2PlugIn � valorizzata? " + (_Access2PlugIn IsNot Nothing).ToString())
            _Access2PlugIn.DBGiriConnectionString = value
            _PlugInSessione.DBGiriConnectionString = value
        End Set

    End Property

    ReadOnly Property CapacitaInvioEmail() As Boolean

        Get
            Return _Access2PlugIn.CapacitaInvioEmail
        End Get

    End Property

    '
    ' Questa variabile individua la tipologia di stampa da utilizzare.
    ' La sessione pu� richiedere la generazione del file di stampa o l'eleborazione di un file di stampa pregenerato.
    ' 1 => Stampa da generare
    ' 2 => Stampa pregenerata
    '
    ReadOnly Property TipologiaStampa As Integer

        Get
            Dim nRtn As Integer

            nRtn = 1
            If _xdSessione.SelectSingleNode("sessione/TipologiaStampa") IsNot Nothing Then
                nRtn = _xdSessione.SelectSingleNode("sessione/TipologiaStampa").InnerText
            End If
            Return nRtn
        End Get

    End Property

    ReadOnly Property MessaggioErrore() As String

        Get
            Dim cErroreRtn As String

            If Errore Is Nothing Then
                cErroreRtn = "Descrizione dell'errore non disponibile"
            Else
                cErroreRtn = Errore.Message
            End If
            Return cErroreRtn
        End Get

    End Property

    '
    ' Contiene il nome della sessione.
    '
    Public Property Nome() As String

    ReadOnly Property SessionePlugInEmailDataFileName() As String

        Get
            Return _SessionePlugInEmailDataFileName
        End Get

    End Property

    Public ReadOnly Property WorkFolder() As String

        Get
            Return String.Concat(Me.Folders(FLD_sessioneroot), _xdSessione.SelectSingleNode("sessione/workfolder/foldername").InnerText)
        End Get

    End Property

    Public ReadOnly Property DataSendByMail() As Xml.XmlNode

        Get
            Return _Access2PlugIn.DataSendByMail
        End Get

    End Property

    Public Property ElencoModuli As Dictionary(Of Integer, GestioneModulo)
    Public Property ElencoEsportazioni As Dictionary(Of Integer, GestioneEsportazione)

    ' ************************************************************************** '
    ' Variabile utilizzata per la gesione di una sessioe di stampa.              '
    ' Contiene il puntamento alla cartella che contiene la sessione in uso.      '
    ' ************************************************************************** '
    Property Descrizione() As String

        Get
            If _xdSessione Is Nothing Then
                Return "Cartella non esistente o non contenente una sessione valida."
            Else
                If _xdSessione.SelectSingleNode("sessione/descrizione") Is Nothing Then
                    Return "Descrizione della sessione non disponibile."
                Else
                    Return _xdSessione.SelectSingleNode("sessione/descrizione").InnerText
                End If
            End If
        End Get
        Set(ByVal value As String)
            _xdSessione.SelectSingleNode("sessione/descrizione").InnerText = value
        End Set

    End Property

    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList

        Get
            Return _PlugInSessione.ListaControlliFormNuovaSessione
        End Get

    End Property

    Private ReadOnly Property _Modello As String

        Get
            Return _xdSessione.SelectSingleNode("sessione/modellosessione").InnerText
        End Get

    End Property

    Sub SetSelectable(ByVal dr As DataRow)

        _Access2PlugIn.SetSelectable(dr, _DatiModuloAttivo.Filtro)

    End Sub

    Sub RemovePRNDSErrorRecord()

        _Access2PlugIn.RemovePRNDSErrorRecord()

    End Sub

    Sub GoToRecordNumber(ByVal nRow As Integer)

        _PlugInSessione.GoToRecordNumber(nRow)

    End Sub

    Sub ImpostaDGVDati(ByVal DGV As System.Windows.Forms.DataGridView)

        _PlugInSessione.ImpostaDGVDati(DGV)

    End Sub

    Sub SetPrintable(ByVal dr As DataRow, ByVal cSelectFromPlugInType As String, ByVal cParamSelez As String)

        _Access2PlugIn.SetPrintable(dr, cSelectFromPlugInType, cParamSelez)

    End Sub

    ReadOnly Property PlugInName() As String

        Get
            Return _Access2PlugIn.NomePlugIn
        End Get

    End Property

    ' *******************************************
    ' Propriet� che riguardano l'accesso al PrintDataset
    ' *******************************************
    ReadOnly Property FullDataset() As DataSet

        Get
            Return _PlugInSessione.FullDataSet
        End Get

    End Property

    ReadOnly Property FullDataSet_MainTable() As DataTable

        Get
            Return _PlugInSessione.FullDataSet_MainTable
        End Get

    End Property

    ReadOnly Property FullDataset_MainTableName() As String

        Get
            Return _PlugInSessione.FullDataSet_MainTableName
        End Get

    End Property

    Sub Carica_FullDataset(ByVal nRecordDaCaricare As Integer)

        If _PlugInSessione.Versione < eVersionePlugIn.Versione_2_0_0 Then
            _Access2PlugIn.LoadFullDataset(nRecordDaCaricare)
        ElseIf _PlugInSessione.Versione = eVersionePlugIn.Versione_2_0_0 Then
            _PlugInSessione.Carica_FullDataset(nRecordDaCaricare, "")
        End If
        If (_TipoAmbiente = eTipoApplicazione.TAP_Invoicer_sender) Then
            CaricaDocumentiDaSpedire()
        End If

    End Sub

    Sub PostAnteprima(ByVal aLista As ArrayList)

        _Access2PlugIn.PostAnteprima(aLista)

    End Sub

    Function GetSelectedRow(ByVal _TipoRic As String, ByVal _ValueRic As String) As String()

        Return _Access2PlugIn.GetSelectedRow(_TipoRic, _ValueRic)

    End Function

    ReadOnly Property ElementiMenuDedicati() As ArrayList

        Get
            Return _Access2PlugIn.ElementiMenuDedicati()
        End Get

    End Property

    ReadOnly Property PlugInVersion() As String

        Get
            Return _PlugInSessione.Versione.ToString().Replace("Versione_", "").Replace("_", "-")
        End Get

    End Property

    ReadOnly Property LinkField_QuickDS_FullDS() As String

        Get
            Return _PlugInSessione.LinkField_QuickDS_FullDS
        End Get

    End Property

    ' Alert del plug in in caso di utilizzo da parte dell'applicazione di stampa. '
    ReadOnly Property MessaggioAvviso() As String

        Get
            Return _Access2PlugIn.MessaggioAvviso
        End Get

    End Property

    ReadOnly Property QuickDataset_MainTableKey()

        Get
            Return _PlugInSessione.QuickDataset_MainTableKey
        End Get

    End Property

    '
    ' Indica che la sessione � stata caricata correttamente.
    '
    Public Property IsLoaded() As Boolean

    Private ReadOnly Property SezioneModuli() As Xml.XmlNode

        Get
            Return _xdSessione.SelectSingleNode("sessione/moduli")
        End Get

    End Property

    ReadOnly Property RecordCaricati() As Integer

        Get
            Return _PlugInSessione.QuickDataset_MainTable.Rows.Count
        End Get

    End Property

    ReadOnly Property RecordErroreStampa() As Integer

        Get
            Return _RecordErroreStampa
        End Get

    End Property

    Property DatiSessione() As ArrayList

    WriteOnly Property ListaSortGiri() As ArrayList

        Set(ByVal value As ArrayList)
            _Access2PlugIn.ListaGiri = value
        End Set

    End Property

    Sub ContaRecordConErrori()

        Me.RecordErrCaricamentoGravi = 0
        Me.RecordErrCaricamentoLievi = 0
        _RecordErroreStampa = 0
        For Each dr As DataRow In Me.QuickDataTable.Rows
            Select Case dr.Item("DEF_errcode")
                Case Is = -1
                    Me.RecordErrCaricamentoGravi += 1
                Case Is = -2
                    Me.RecordErrCaricamentoLievi += 1
                Case Is = -10
                    _RecordErroreStampa += 1
            End Select
        Next

    End Sub

    Public Sub New(ByVal nTipoAmbiente As eTipoApplicazione, ByVal F As CLS_FileFolderDic, ByVal cVersioneSistemaOperativo As String, ByVal ol As Dictionary(Of String, CLS_oledbconnstring))

        '
        ' Folders provenienti dall'applicazione.
        '
        FoldersGlobali = F

        _xdSessione = Nothing
        _TipoAmbiente = nTipoAmbiente
        _VersioneSistemaOperativo = cVersioneSistemaOperativo
        Me.Folders = New CLS_FileFolderDic
        Me.Files = New CLS_FileFolderDic
        Me.OleDb_ConnString = ol
        MessaggiProgress = New MessaggiEntity
        ResetSessione()

    End Sub

    '
    ' Carica l'anteprima della sessione con solo i dati riguardanti i 
    '
    Public Sub Anteprima(ByVal cFolder As String)

        Try
            LogHelper.Logger.Debug(String.Concat("Anteprima. Caricamento della sessione: ", cFolder))
            Me.Caricamento(cFolder, True, False)
        Catch ex As Exception
            LogHelper.Logger.Error("Errore", ex)
        End Try

    End Sub

    Public Sub Apertura(ByVal cFolder As String, ByVal lNuovaSessione As Boolean)

        Try
            LogHelper.Logger.Debug(String.Concat("Apertura. Caricamento della sessione: ", cFolder))
            Me.Caricamento(cFolder, False, lNuovaSessione)
        Catch ex As Exception
            LogHelper.Logger.Error("Errore", ex)
        End Try

    End Sub

    Private Sub Caricamento(ByVal cFolder As String, ByVal lAnteprimaSessione As Boolean, ByVal lNuovaSessione As Boolean)

        Try
            Me.IsLoaded = True
            LogHelper.Logger.Debug("Caricamento " + String.Concat("Caricamento della sessione: ", cFolder))
            If System.IO.Directory.Exists(cFolder) Then
                Me.Nome = EstraiNomeSessione(cFolder)
                LogHelper.Logger.Debug("Caricamento " + "Nome sessione ricavato: " + Me.Nome)
                Me.Folders.Add(FLD_sessioneroot, StandardFolderValue(cFolder))
                Me.Files.Add("SessioneFIL", ConcatenaFolderFileValue(Me.Folders(FLD_sessioneroot), String.Concat(Me.Nome, ".xml")))

                LogHelper.Logger.Debug("Caricamento" + "File sessione: " + Me.Files("SessioneFIL"))
                If System.IO.File.Exists(Me.Files("SessioneFIL")) Then
                    '
                    ' Popola la lista delle cartelle della sessione.
                    '
                    Me.Folders.Add("ArchivioOtticoFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "Archivio_ottico"))
                    Me.Folders.Add("RistampeFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "Ristampe"))
                    Me.Folders.Add("AllegatiFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "Allegati"))

                    Me.Folders.Add("DatiFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "Dati"))
                    Me.Folders.Add("EsportaFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "Esportazione"))
                    Me.Folders.Add("ModuliFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "Moduli"))

                    If _TipoAmbiente = eTipoApplicazione.TAP_Invoicer Then
                        Me.Folders.Add("DocumentiUnificatiFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "DocumentiUnificati"))
                        Me.Folders.Add("ElaboratiFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "Elaborati"))
                    ElseIf _TipoAmbiente = eTipoApplicazione.TAP_SorteCert_SRV Then
                        Me.Folders.Add("DatiConsegnaCGFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "DatiConsegnaCG"))
                        Me.Folders.Add("DatiConsegnaDBFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "DatiConsegnaDB"))
                        Me.Folders.Add("DatiConsegnaDBDupFLD", ConcatenaFolderValue(Me.Folders(FLD_sessioneroot), "DatiConsegnaDBDup"))
                    End If

                    '
                    ' Popola la lista dei file delle sessioni
                    '
                    _xdSessione = New Xml.XmlDocument
                    _xdSessione.Load(Me.Files("SessioneFIL"))
                    _SessionePlugInEmailDataFileName = String.Concat(Me.Folders(FLD_sessioneroot), "Moduli\MailData.xml")
                    Me.Files.Add("QuickDatasetSerializadFIL", "")
                    Me.Files.Add("FullDatasetSerializadFIL", "")
                    Me.Files.Add(FIL_plugin, String.Concat(Me.Folders(FLD_sessioneroot), _xdSessione.SelectSingleNode("sessione/plugin/filename").InnerText))
                    Me.Files.Add("PlugInTrasformazioniFIL", ConcatenaFolderFileValue(Me.Folders(FLD_sessioneroot), _xdSessione.SelectSingleNode("sessione/plugin/trasformazioni").InnerText))
                    Me.Files.Add("FastPrintFIL", ConcatenaFolderFileValue(Me.Folders("DatiFLD"), "FastPrint.xml"))
                    Me.Files.Add("PlugInDatiFissiFIL", String.Concat(Me.Folders(FLD_sessioneroot), _xdSessione.SelectSingleNode("sessione/dati/DatiFissi").InnerText))
                    If _xdSessione.SelectSingleNode("sessione/dati/SendByMail") IsNot Nothing Then
                        Me.Files.Add("DatiSpedizioneEmailFIL", String.Concat(Me.Folders(FLD_sessioneroot), _xdSessione.SelectSingleNode("sessione/dati/SendByMail").InnerText))
                    End If
                    Me.Files.Add("StatisticheStampaFIL", ConcatenaFolderFileValue(Me.Folders("DatiFLD"), "StatisticheStampa.csv"))

                    If _xdSessione.SelectSingleNode("sessione/dati/filename[@tipo=""0""]") IsNot Nothing Then
                        Me.Files.Add("QuickDataSetIndexNSFIL", String.Concat(Me.FoldersGlobali("TempFLD"), Timestamp, ".xml"))
                        Me.Files.Add("QuickDataSetIndexFIL", String.Concat(Me.Folders("SessioneRootFLD"), _xdSessione.SelectSingleNode("sessione/dati/filename[@tipo=""0""]").InnerText))
                    End If

                    If Not lAnteprimaSessione Then

                        '
                        ' Verifica esistenza file delle statistiche di stampa ed eventuale compatibilizzazioe con il nuovo formato
                        '
                        If System.IO.File.Exists(ConcatenaFolderFileValue(Me.Folders("DatiFLD"), "DatiStampaSessione.xml")) And Not System.IO.File.Exists(Me.Files("StatisticheStampaFIL")) Then
                            CompatibilizzaStatisticheStampa(ConcatenaFolderFileValue(Me.Folders("DatiFLD"), "DatiStampaSessione.xml"))
                        Else
                            System.IO.File.Delete(ConcatenaFolderFileValue(Me.Folders("DatiFLD"), "DatiStampaSessione.xml"))
                        End If

                        '
                        ' Carica le Statistiche di stampa, oramai sicuramente compatilizzate.
                        '
                        CaricaStatisticheStampa()

                        '
                        ' Caricamento della lista dei moduli
                        ' 
                        Carica_ListaModuli()

                        '
                        ' caricamento della lista delle esportazioni
                        '
                        Carica_ListaEsportazioni()

                        Carica_Plugin_NuovaVersione()

                        '
                        ' Caricamento Modulo Versione Vecchia. Sar� progressivamente sostituita da quella nuova Sistemata appena sopra.
                        '
                        LogHelper.Logger.Debug("GESE-000003: Il plugin non pu� essere caricato correttamente. Non si pu� procedere con l'apertura della sessione di stampa.")
                        _Access2PlugIn = New CLS_plugin_accesso(_TipoAmbiente, _VersioneSistemaOperativo)
                        _Access2PlugIn.SessioneNuova = lNuovaSessione
                        _Access2PlugIn.Carica_Plugin(_PlugInSessione)
                        _Access2PlugIn.Folders = Me.Folders
                        _Access2PlugIn.Files = Me.Files
                        _Access2PlugIn.SezioneDati = _xdSessione.SelectSingleNode("sessione/dati")
                        LogHelper.Logger.Debug("GESE-000003: Il plugin non pu� essere caricato correttamente. Non si pu� procedere con l'apertura della sessione di stampa.")

                        If Not _Access2PlugIn.IsPlugInLoaded Then
                            LogHelper.Logger.Debug("GESE-000003: Il plugin non pu� essere caricato correttamente. Non si pu� procedere con l'apertura della sessione di stampa.")
                            Throw New Exception("GESE-000003: Il plugin non pu� essere caricato correttamente. Non si pu� procedere con l'apertura della sessione di stampa.")
                        End If
                    End If
                Else
                    LogHelper.Logger.Error("GESE-000002: Non � stato possibile caricare il file della sessione. Non si pu� procedere con l'apertura della sessione di stampa.")
                    Throw New Exception("GESE-000002: Non � stato possibile caricare il file della sessione. Non si pu� procedere con l'apertura della sessione di stampa.")
                End If
            Else
                LogHelper.Logger.Error(String.Concat("GESE-000001: Il percorso della sessione (", cFolder, ") non esiste."))
                Throw New Exception(String.Concat("GESE-000001: Il percorso della sessione (", cFolder, ") non esiste."))
            End If
        Catch ex As Exception
            LogHelper.Logger.Error("Caricamento della sessione", ex)
            ResetSessione()
            Errore = ex
            Throw ex
        End Try

    End Sub

    Public Sub ChiudiSessione()

        ResetSessione()

    End Sub

    Private Sub ResetSessione()

        Me.IsLoaded = False
        _xdSessione = Nothing
        _RecordSelezionati = 0

        '
        ' Reset contenitori statistche Globali e Parziali
        '
        _StatisticheStampaGlobali = New Dictionary(Of Integer, StatisticheStampaEntity)
        _StatisticheStampaParziali = New Dictionary(Of Integer, StatisticheStampaEntity)

    End Sub

    '
    ' Crea la cartella dove verr� memorizzata la sessione.
    '
    Public Sub CreaFolderNuovaSessione(ByVal cDstPath As String)
        Dim aLista As ArrayList
        Dim cSrcPath As String
        Dim aValue As String()
        Dim cTmp As String
        Dim i As Integer

        cSrcPath = Me.Folders(FLD_sessioneroot)
        Me.Folders.Add("NEWSessioneRootFLD", cDstPath)
        Me.Nome = EstraiNomeSessione(cDstPath)

        '
        ' Creo l'elenco delle cartelle e dei file da copiare.
        '
        MessaggiProgress.TipoOperazionePrinc = "Copia dati standard della sessione..."

        i = 0
        aLista = New ArrayList
        aLista.Add(New String() {"D", Me.Folders(FLD_sessioneroot)})
        While i < aLista.Count
            aValue = aLista(i)
            If aValue(0) = "D" Then
                For Each cFld As String In System.IO.Directory.GetDirectories(aValue(1))
                    aLista.Add(New String() {"D", cFld})
                Next
                For Each cFile As String In System.IO.Directory.GetFiles(aValue(1))
                    aLista.Add(New String() {"F", cFile})
                Next
            End If
            i += 1
        End While
        MessaggiProgress.MassimoPrinc = aLista.Count

        '
        ' Copia i valori nel folder della sessione.
        '
        MessaggiProgress.ProgressivoPrinc = 0
        MessaggiProgress.Abilitati = True
        For Each aValue In aLista
            MessaggiProgress.ProgressivoPrinc += 1
            cTmp = aValue(1).Replace(cSrcPath, cDstPath)
            Select Case aValue(0)
                Case Is = "D"
                    System.IO.Directory.CreateDirectory(cTmp)
                Case Is = "F"
                    If cTmp.EndsWith(String.Concat(Me._Modello, ".xml")) Then
                        cTmp = cTmp.Replace(String.Concat(Me._Modello, ".xml"), String.Concat(Me.Nome, ".xml"))
                    End If
                    System.IO.File.Copy(aValue(1), cTmp)
            End Select
            System.Threading.Thread.Sleep(500)
        Next

    End Sub

    '
    ' Popola la sessione con i nuovi dati e procede con la sua finalizzazione.
    '
    Public Sub CreaNuovaSessione()
        Dim aValue As String()
        Dim i As Integer

        Try
            MessaggiProgress.ProgressivoPrinc = 1
            MessaggiProgress.MassimoPrinc = 3
            MessaggiProgress.TipoOperazionePrinc = "Setup della sessione..."
            MessaggiProgress.ProgressivoSecon = 1
            MessaggiProgress.MassimoSecon = 3
            MessaggiProgress.TipoOperazioneSecon = "N/D"
            i = 0
            While i < Me.DatiSessione.Count
                aValue = Me.DatiSessione(i)
                If aValue(0) = 202 Then
                    Me.Descrizione = aValue(1)
                End If
                If aValue(0) > 200 Then
                    Me.DatiSessione.Remove(aValue)
                    i -= 1
                End If
                i += 1
            End While
            System.Threading.Thread.Sleep(500)

            ' 
            ' Creazione del database della sessione.                                                                   
            ' 
            MessaggiProgress.ProgressivoPrinc = 2
            MessaggiProgress.TipoOperazioneSecon = "Caricamento dati database della sessione..."
            _PlugInSessione.MessaggiProgress = Me.MessaggiProgress
            MessaggiProgress.MessaggiDalPlugin = True

            If _PlugInSessione.Versione = eVersionePlugIn.Versione_2_0_0 Then
                _PlugInSessione.CreaDataBaseSessione(Me.DatiSessione, _xdSessione)
                WriteSessionData()
            Else
                _Access2PlugIn.CreaDataBaseSessione(Me.DatiSessione, _xdSessione)
            End If
            SalvaSessione()
            MessaggiProgress.ProgressivoPrinc = 3
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub RimuoviSessioneCreazione()

        Try
            Errore = Nothing
            If Me.Folders.ContainsKey("NEWSessioneRootFLD") Then
                System.IO.Directory.Delete(Me.Folders("NEWSessioneRootFLD"), True)
            Else
                System.IO.Directory.Delete(Me.Folders(FLD_sessioneroot), True)
            End If
        Catch ex As Exception
            Errore = ex
        End Try

    End Sub

    Sub WriteSessionData1(ByVal aResultAl As ArrayList)
        Dim xiWork As Xml.XmlNode

        For Each aValue As String() In aResultAl
            If aValue(3) < 199 Then
                xiWork = _xdSessione.CreateElement(aValue(0))
                xiWork.InnerText = aValue(1)
                xiWork.Attributes.Append(_xdSessione.CreateAttribute("codice"))
                xiWork.Attributes("codice").Value = aValue(2)
                xiWork.Attributes.Append(_xdSessione.CreateAttribute("tipo"))
                xiWork.Attributes("tipo").Value = aValue(3)
                xiWork.Attributes.Append(_xdSessione.CreateAttribute("stato"))
                xiWork.Attributes("stato").Value = aValue(4)
                _xdSessione.SelectSingleNode("sessione/dati").AppendChild(xiWork)
            End If
        Next

    End Sub

    Function CheckDatiSessione() As Boolean

        Return _Access2PlugIn.CheckDatiSessione(Me.DatiSessione)

    End Function

    Sub Carica_QuickDataset()

        Try
            MessaggiProgress.MessaggiDalPlugin = False
            MessaggiProgress.ProgressivoPrinc = 0
            MessaggiProgress.MassimoPrinc = 1
            MessaggiProgress.TipoOperazionePrinc = "Caricamento dei dati del Quick Database...."
            MessaggiProgress.ProgressivoSecon = 0
            MessaggiProgress.MassimoSecon = -1
            MessaggiProgress.TipoOperazioneSecon = "N/D"

            _PlugInSessione.MessaggiProgress = MessaggiProgress
            MessaggiProgress.MessaggiDalPlugin = True
            '
            ' Step 1 Caricamento dei dati del QuickDataset
            '
            _PlugInSessione.Carica_QuickDataset()

            ' ********************************************************************* '
            ' Adeguo il QuickDataset a quello che � l'ultima versione del plugin.   '
            ' In particolare:                                                       '
            '                                                                       '
            ' Vers. PLUGIN      Campo                       Valore default          '
            ' 1.3.0             DEF_sendbymail              FALSE                   '
            ' 1.4.1             DEF_onlyarchott             FALSE                   '
            ' ********************************************************************* '
            If _PlugInSessione.Versione < eVersionePlugIn.Versione_1_4_1 Then
                Me.QuickDataTable.Columns.Add(New DataColumn("DEF_onlyarchott", System.Type.GetType("System.Boolean")))
                If _PlugInSessione.Versione < eVersionePlugIn.Versione_1_3_0 Then
                    Me.QuickDataTable.Columns.Add(New DataColumn("DEF_sendbymail", System.Type.GetType("System.Boolean")))
                End If

                For Each dr As DataRow In Me.QuickDataTable.Rows
                    dr("DEF_onlyarchott") = False
                    If _PlugInSessione.Versione < eVersionePlugIn.Versione_1_3_0 Then
                        dr("DEF_sendbymail") = False
                    End If
                Next
                Me.QuickDataTable.AcceptChanges()
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ReadOnly Property QuickDataTable() As DataTable

        Get
            Return _PlugInSessione.QuickDataset_MainTable
        End Get

    End Property

    ReadOnly Property PlugInFLD()

        Get
            Return System.IO.Path.GetDirectoryName(String.Concat(Me.Folders(FLD_sessioneroot), _xdSessione.SelectSingleNode("sessione/plugin/filename").InnerText))
        End Get

    End Property

    Property CheckDatiVar() As Boolean

        Get
            Return _CheckDatiVar
        End Get
        Set(ByVal value As Boolean)
            _CheckDatiVar = value
        End Set

    End Property

    Public Sub SalvaSessione()

        _xdSessione.Save(Me.Files("SessioneFIL"))

    End Sub

    Public Sub ContaSelezionati(ByVal dr As DataRow, Optional ByVal lForcedCount As Boolean = False)

        If dr("DEF_toprint") Then
            _RecordSelezionati += 1
        End If

    End Sub

    Property RecordSelezionati() As Integer

        Get
            Return _RecordSelezionati
        End Get
        Set(ByVal value As Integer)
            _RecordSelezionati = value
        End Set

    End Property

    Private Function EstraiNomeSessione(ByVal cValue As String) As String

        If cValue.EndsWith("\") Then
            cValue = cValue.Substring(0, cValue.Length - 1)
        End If
        Return cValue.Substring(cValue.LastIndexOf("\") + 1)

    End Function

    Public ReadOnly Property MailServer() As String

        Get
            Return _xdSessione.SelectSingleNode("sessione/SendByEMail/MailServer/ServerName").InnerText
        End Get

    End Property

    Public ReadOnly Property MailSrvAuth() As String

        Get
            Return _xdSessione.SelectSingleNode("sessione/SendByEMail/MailServer/ServerAuth").InnerText
        End Get

    End Property

    Public ReadOnly Property MailSrvProt() As String

        Get
            Return _xdSessione.SelectSingleNode("sessione/SendByEMail/MailServer/Protected").InnerText
        End Get

    End Property

    Public ReadOnly Property Email2Use() As String

        Get
            Return _xdSessione.SelectSingleNode("sessione/SendByEMail/Mittente/MailAddress").InnerText
        End Get

    End Property

    Public Sub LoadDatiSpedSessione()

        If _Access2PlugIn.CapacitaInvioEmail Then
            If System.IO.File.Exists(Me.Files("DatiSpedizioneEmailFIL")) Then
                _data_sped_email = New DS_dati_sped_email
                _data_sped_email.ReadXml(Me.Files("DatiSpedizioneEmailFIL"))
            Else
                Throw New Exception("Attenzione! Il file dati delle spedizioni via email non � stato generato o non esiste. Non si pu� procedere oltre.")
                'MessageBox.Show("Attenzione! Il file dati delle spedizioni via email non � stato generato o non esiste. Non si pu� procedere oltre.")
            End If
        End If

    End Sub

    Public Sub SaveDatiSpedSessione()

        If _Access2PlugIn.CapacitaInvioEmail Then
            _data_sped_email.WriteXml(Me.Files("DatiSpedizioneEmailFIL"))
        End If
        ' Else
        'MessageBox.Show("Attenzione! Il file dati delle spedizioni via email non � stato generato o non esiste. Non si pu� procedere oltre.")
        'End If

    End Sub

    Public ReadOnly Property DaInviareDataset() As DS_dati_sped_email

        Get
            Return _data_sped_email
        End Get

    End Property

    Public ReadOnly Property MailSrvXML() As Xml.XmlNode

        Get
            Return _xdSessione.SelectSingleNode("sessione/SendByEMail/MailServer")
        End Get

    End Property

    Public ReadOnly Property MailData() As Xml.XmlNode

        Get
            Return _xdSessione.SelectSingleNode("sessione/SendByEMail/MailData")
        End Get

    End Property

    Public ReadOnly Property MailAttach() As Xml.XmlNode

        Get
            Return _xdSessione.SelectSingleNode("sessione/SendByEMail/MailAttach")
        End Get

    End Property

    Public ReadOnly Property SendByMailNode() As Xml.XmlNode

        Get
            Return _xdSessione.SelectSingleNode("sessione/SendByEMail")
        End Get

    End Property

    Public Sub AggiornaQuickDataset()

        _Access2PlugIn.UpdateQuickDatasetFile()

    End Sub

    Public Sub AppendAttachFile(ByVal cFileName As String)
        Dim xnItem_1 As Xml.XmlNode
        Dim xnItem_2 As Xml.XmlNode

        If (Me.MailAttach Is Nothing) Then
            xnItem_2 = _xdSessione.CreateElement("MailAttach")
            Me.SendByMailNode.AppendChild(xnItem_2.Clone)
        End If
        xnItem_1 = _xdSessione.CreateElement("MailAttachItem")
        xnItem_1.InnerText = cFileName
        Me.MailAttach.AppendChild(xnItem_1.Clone)
        Me.SalvaSessione()

    End Sub

    Public Sub GotoRecordKey(ByVal nKey As String)
        Dim i As Integer

        i = 0
        _Access2PlugIn.GoToRecordNumber(i)
        While Me.QuickDataTable.Rows(_Access2PlugIn.CurrentRecordNumber).Item(_PlugInSessione.QuickDataset_MainTableKey) <> nKey
            i += 1
            _Access2PlugIn.GoToRecordNumber(i)
        End While

    End Sub

    Public Function PostStampa() As ArrayList

        Return _Access2PlugIn.PostStampa()

    End Function

    Public Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean

        Return _Access2PlugIn.CheckCondPSA(nCond2Check, cValue2Check)

    End Function

    Public Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)

        _Access2PlugIn.ExecActionPSA(nAct2Exec, cValue2Exec)

    End Sub

    Public Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean

        Return _Access2PlugIn.CheckCondAPI(nCond2Check, cValue2Check)

    End Function

    Public Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String)

        _Access2PlugIn.ExecActionAPI(cTipoExecAPI, nAct2Exec, cValue2Exec)

    End Sub

    ReadOnly Property CurrentRecordNumber() As Integer

        Get
            Return _Access2PlugIn.CurrentRecordNumber
        End Get

    End Property

    ReadOnly Property LogDSStampa() As DataSet

        Get
            Return _Access2PlugIn.LogDSStampa
        End Get

    End Property

    ReadOnly Property FastPrintFile() As String

        Get
            Return String.Concat(Me.Folders("DatiFLD"), "FastPrint.xml")
        End Get

    End Property

    ReadOnly Property AzioniPostStampaApp() As Boolean

        Get
            Return _Access2PlugIn.VersionePlugIn > "1.4.3"

        End Get

    End Property

    ReadOnly Property DatiAzioniPostStampa() As ArrayList

        Get
            Return _Access2PlugIn.DatiAzioniPostStampa
        End Get

    End Property

    ReadOnly Property DatiAzioniPreInvio() As ArrayList

        Get
            Return _Access2PlugIn.DatiAzioniPreInvio
        End Get

    End Property

    ReadOnly Property AzioniPreInvioApp() As Boolean

        Get
            Return _Access2PlugIn.VersionePlugIn > "1.4.3"

        End Get

    End Property

#Region "Procedure per la gestione delle statistiche di stampa"

    '
    ' Carica le statistiche di stampa
    '
    Private Sub CaricaStatisticheStampa()
        Dim sr As System.IO.StreamReader
        Dim s As StatisticheStampaEntity
        Dim cLine As String

        _StatisticheStampaGlobali = New Dictionary(Of Integer, StatisticheStampaEntity)
        _StatisticheStampaParziali = New Dictionary(Of Integer, StatisticheStampaEntity)
        If System.IO.File.Exists(Me.Files("StatisticheStampaFIL")) Then
            sr = New System.IO.StreamReader(Me.Files("StatisticheStampaFIL"))
            While Not sr.EndOfStream
                cLine = sr.ReadLine
                s = New StatisticheStampaEntity
                s.STS_codice = cLine.Split(";")(0)
                s.STS_num_pagine = cLine.Split(";")(1)
                s.STS_stampate = cLine.Split(";")(2)
                _StatisticheStampaGlobali.Add(s.STS_codice, s)
            End While
            sr.Close()
        End If
        _StatisticheStampaGlobali = _StatisticheStampaGlobali.OrderBy(Function(x) x.Key).ToDictionary(Function(x) x.Key, Function(y) y.Value)

    End Sub

    '
    ' Reset Delle statistiche di stampa
    '
    Public Sub ResetStatisticheStampe()

        _StatisticheStampaGlobali = New Dictionary(Of Integer, StatisticheStampaEntity)
        SalvaStatisticheStampa()
        _StatisticheStampaParziali = New Dictionary(Of Integer, StatisticheStampaEntity)

    End Sub

    '
    ' Aggiornamento delle statistiche di stampa.
    '
    Public Sub AggiornaStatisticheStampa(ByVal _StatStamp As Dictionary(Of Integer, Integer))
        Dim s As StatisticheStampaEntity

        For Each nPagine As Integer In _StatStamp.Keys
            '
            ' Aggiungi i dati di stampa a quelle globali.
            '
            If _StatisticheStampaGlobali.ContainsKey(nPagine) Then
                s = _StatisticheStampaGlobali(nPagine)
            Else
                s = New StatisticheStampaEntity
                s.STS_codice = nPagine
                s.STS_num_pagine = nPagine
                _StatisticheStampaGlobali.Add(nPagine, s)
            End If
            s.STS_stampate += _StatStamp(nPagine)

            '
            ' Aggiungi i dati di stampa a quelle globali.
            '
            If _StatisticheStampaParziali.ContainsKey(nPagine) Then
                s = _StatisticheStampaParziali(nPagine)
            Else
                s = New StatisticheStampaEntity
                s.STS_codice = nPagine
                s.STS_num_pagine = nPagine
                _StatisticheStampaParziali.Add(nPagine, s)
            End If
            s.STS_stampate += _StatStamp(nPagine)
        Next
        SalvaStatisticheStampa()

    End Sub

    '
    ' Modifica il formato di memorizzazione delle statistiche di stampa
    '
    Private Sub CompatibilizzaStatisticheStampa(ByVal cFolder As String)
        Dim s As StatisticheStampaEntity
        Dim ds As DataSet

        _StatisticheStampaGlobali = New Dictionary(Of Integer, StatisticheStampaEntity)
        If System.IO.File.Exists(cFolder) Then
            ds = New DataSet
            ds.ReadXml(cFolder)
            If ds.Tables.Contains("TBL_statistiche") Then
                For Each dr As DataRow In ds.Tables("TBL_statistiche").Rows
                    s = New StatisticheStampaEntity
                    s.STS_codice = dr("STA_codice")
                    s.STS_num_pagine = dr("STA_codice")
                    s.STS_stampate = dr("STA_totale")
                    _StatisticheStampaGlobali.Add(s.STS_codice, s)
                Next
            End If
            System.IO.File.Delete(cFolder)
        End If
        SalvaStatisticheStampa()

    End Sub

    '
    ' Salva dati statistiche di stampa
    '
    Private Sub SalvaStatisticheStampa()
        Dim sw As System.IO.StreamWriter

        sw = New System.IO.StreamWriter(Me.Files("StatisticheStampaFIL"))
        For Each p As KeyValuePair(Of Integer, StatisticheStampaEntity) In _StatisticheStampaGlobali
            sw.WriteLine(String.Concat(p.Value.STS_codice, ";", p.Value.STS_num_pagine, ";", p.Value.STS_stampate))
        Next
        sw.Close()

    End Sub

#End Region

    Private Sub Carica_ListaModuli()
        Dim xiWork As Xml.XmlNode
        Dim m As GestioneModulo

        '
        ' Recupero il cassetto di default
        '
        _FeedString = ""
        If NodeNotNull(SezioneModuli.SelectSingleNode("allfeed")) Then
            _FeedString = SezioneModuli.SelectSingleNode("allfeed").InnerText
        End If

        '
        ' Recupero l'elenco dei moduli
        '
        ElencoModuli = New Dictionary(Of Integer, GestioneModulo)
        For Each xiWork In SezioneModuli.SelectNodes("modulo")
            '
            ' Carica il modulo
            '
            m = New GestioneModulo(xiWork, Me.Folders("ModuliFLD"))

            If m.DatiModulo.Codice <> 900 And m.IsLoaded Then
                ElencoModuli.Add(m.DatiModulo.Codice, m)
                '
                ' Se il modulo ha un filtro allora procedo con il su caricamento.
                '
                If m.DatiModulo.Filtro > "" Then
                    _FeedString = String.Concat(_FeedString, ";", m.DatiModulo.Filtro, ";", m.DatiModulo.AlimentazioneImbustatrice)
                End If
            ElseIf m.DatiModulo.ModuloUnificato Then
                ModuloUnificato = m
            End If
        Next

    End Sub

    Private Sub Carica_ListaEsportazioni()
        Dim xiWork As Xml.XmlNode
        Dim e As GestioneEsportazione

        '
        ' Recupero l'elenco dei moduli
        '
        ElencoEsportazioni = New Dictionary(Of Integer, GestioneEsportazione)
        For Each xiWork In SezioneModuli.SelectNodes("modulo")
            '
            ' Carica il modulo
            '
            e = New GestioneEsportazione(xiWork, Me.Folders("ModuliFLD"))

            If e.IsLoaded Then
                ElencoEsportazioni.Add(e.DatiEsportazione.Codice, e)
                '
                ' Se il modulo ha un filtro allora procedo con il su caricamento.
                '
                If e.DatiEsportazione.Filtro > "" Then

                End If
            End If
        Next

    End Sub

    Private Sub Carica_Plugin_NuovaVersione()
        Dim PlugInLoader As AssemblyLoader.Loader
        Dim cAssLoadFileName As String
        Dim PluginAppoggio As Object
        Dim PluginAppoggio2 As Object

        cAssLoadFileName = ConcatenaFolderFileValue(Me.FoldersGlobali("RootApplicationFLD"), "AssemblyLoader.dll")

        LogHelper.Logger.Debug("Carica_Plugin_NuovaVersione " + String.Concat("File del plugin: ", Me.Files(FIL_plugin)))
        LogHelper.Logger.Debug("Carica_Plugin_NuovaVersione " + cAssLoadFileName)
        LogHelper.Logger.Debug("Carica_Plugin_NuovaVersione " + GetType(AssemblyLoader.Loader).FullName)
        Try
            ' ************************************************************************** '
            ' Passo al caricameno del plugin della sessione.                             '
            ' Se il plug in permette (versione plugin 1.6.0 e superiori allora procedo   '
            ' con il caricamento in un Application Domain separato altrimenti in quello  '
            ' attuale.                                                                   '
            ' Tale attivit� � gestita caricandolo su di un nuovo Application domain e ve '
            ' rificando che non si generi nessun tipo di eccezzione.                     '
            ' ************************************************************************** '
            _PlugInAppDomain = AppDomain.CreateDomain("PlugInAppDomain")
            PlugInLoader = _PlugInAppDomain.CreateInstanceFromAndUnwrap(ConcatenaFolderFileValue(Me.FoldersGlobali("RootApplicationFLD"), "AssemblyLoader.dll"), GetType(AssemblyLoader.Loader).FullName)
            PluginAppoggio = PlugInLoader.CreateInstance(Me.Files(FIL_plugin))

            '
            ' Questa assegnazione � fittizia serve solo per poter verificare che sia correttamente caricato in modalit� serializad
            '
            'PluginAppoggio = PlugInLoader.PlugIn
            LogHelper.Logger.Debug("Carica_Plugin_NuovaVersione " + "Plug in caricato in un AppDomain diverso.")
        Catch ex As System.Runtime.Serialization.SerializationException
            ' 
            ' Caricamento del plugin in maniera tradizionale. Stesso Appdomain dell'ap-  
            ' plicazione.                                                                
            '
            PlugInLoader = New AssemblyLoader.Loader
            PluginAppoggio = PlugInLoader.CreateInstance(Me.Files(FIL_plugin))


            LogHelper.Logger.Debug("Carica_Plugin_NuovaVersione " + "Plug in caricato nello stesso AppDomain.")
        Catch ex As Exception
            PlugInLoader = Nothing
            Throw New Exception(String.Concat("GESE-000004: Errore nel caricamento del plugin (", Me.Files(FIL_plugin), ")."), Nothing)
        End Try

        LogHelper.Logger.Debug("Carica_Plugin_NuovaVersione" + String.Concat("Versione plugin caricato:", PlugInLoader.VersionePlugIn))
        '
        ' Caricamento della classe di gestione del plugin.
        '
        Try
            Select Case PlugInLoader.VersionePlugIn
                Case Is = eVersionePlugIn.No_versione
                    Throw New Exception(String.Concat("GESE-000005: Versione del plugin non identificata."), Nothing)
                Case Is = eVersionePlugIn.Versione_1_0_0
                    _PlugInSessione = New CLS_plugin_v1_0_0(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_1_0
                    _PlugInSessione = New CLS_plugin_v1_1_0(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_2_0
                    _PlugInSessione = New CLS_plugin_v1_2_0(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_3_0
                    _PlugInSessione = New CLS_plugin_v1_3_0(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_3_1
                    _PlugInSessione = New CLS_plugin_v1_3_1(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_3_2
                    _PlugInSessione = New CLS_plugin_v1_3_2(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_4_0
                    _PlugInSessione = New CLS_plugin_v1_4_0(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_4_1
                    _PlugInSessione = New CLS_plugin_v1_4_1(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_4_2
                    _PlugInSessione = New CLS_plugin_v1_4_2(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_4_3
                    _PlugInSessione = New CLS_plugin_v1_4_3(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_5_0
                    _PlugInSessione = New CLS_plugin_v1_5_0(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_5_1
                    _PlugInSessione = New CLS_plugin_v1_5_1(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_6_0
                    _PlugInSessione = New CLS_plugin_v1_6_0(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_1_6_1
                    _PlugInSessione = New CLS_plugin_v1_6_1(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
                Case Is = eVersionePlugIn.Versione_2_0_0
                    _PlugInSessione = New CLS_plugin_v2_0_0(PluginAppoggio, Me.Folders, Me.FoldersGlobali, Me.Files, Me.OleDb_ConnString)
            End Select
            _PlugInSessione.SezioneDati = _xdSessione.SelectSingleNode("sessione/dati")
            LogHelper.Logger.Debug("Carica_Plugin_NuovaVersione" + String.Concat("Plugin sessione caricato? ", (_PlugInSessione IsNot Nothing).ToString()))
        Catch ex As Exception
            LogHelper.Logger.Debug(String.Concat("GESE-000006: Errore nella creazione della classe del plugin."), ex)
        End Try

    End Sub

    Private Sub WriteSessionData()
        Dim xiWork As Xml.XmlNode

        For Each aValue As String() In _PlugInSessione.ResultAL
            xiWork = _xdSessione.CreateElement(aValue(0))
            xiWork.InnerText = aValue(1)
            xiWork.Attributes.Append(_xdSessione.CreateAttribute("codice"))
            xiWork.Attributes("codice").Value = aValue(2)
            xiWork.Attributes.Append(_xdSessione.CreateAttribute("tipo"))
            xiWork.Attributes("tipo").Value = aValue(3)
            xiWork.Attributes.Append(_xdSessione.CreateAttribute("stato"))
            xiWork.Attributes("stato").Value = aValue(4)
            _xdSessione.SelectSingleNode("sessione/dati").AppendChild(xiWork)
        Next

    End Sub

    '
    ' Attiva il modulo di con cui si recupereranno i dati del FullDataset.
    '
    Public Sub AttivaModulo(ByVal c As Integer)

        If c = 999 Then
            _PlugInSessione.CodiceModulo = 999
        Else
            _DatiModuloAttivo = Me.ElencoModuli(c).DatiModulo
            _PlugInSessione.FiltroSelezione = _DatiModuloAttivo.Filtro
            _PlugInSessione.CodiceModulo = _DatiModuloAttivo.Codice

            If _DatiModuloAttivo.Tipologia = Modulo.eTipologiaModulo.TIM_Master Then
                _PlugInSessione.AlimentazioneImbustatrice = _FeedString
            Else
                _PlugInSessione.AlimentazioneImbustatrice = ""
            End If
        End If

    End Sub

    Private Sub CaricaDocumentiDaSpedire()
        Dim DaInviareViaMail As Dictionary(Of Integer, SendByMailItemEntity)
        Dim si As SendByMailItemEntity
        Dim dr_doc As DataRow
        Dim nKey As Integer

        '
        ' Apro il documento 
        '
        Try
            DaInviareViaMail = CType(OGC_utilitaXML.DeserializeXML(Me.Files("DatiSpedizioneEmailFIL"), GetType(List(Of SendByMailItemEntity))), List(Of SendByMailItemEntity)).ToDictionary(Of Integer, SendByMailItemEntity)(Function(p) p.MAI_codice, Function(p) p)
        Catch ex As Exception
            DaInviareViaMail = New Dictionary(Of Integer, SendByMailItemEntity)
        End Try

        nKey = FullDataSet_MainTable.Rows(0).Item(Me.LinkField_QuickDS_FullDS)
        If DaInviareViaMail.ContainsKey(nKey) Then
            si = DaInviareViaMail(nKey)
            '
            ' Ricerco i documenti da inviare per l'utente in 
            '
            For Each doc As DocumentoEntity In si.ListaDocumenti
                dr_doc = Me.FullDataset.Tables("TBL_documenti").NewRow
                dr_doc("DOC_codice") = Me.FullDataset.Tables("TBL_documenti").Rows.Count + 1
                dr_doc("DOC_cod_mai") = nKey
                dr_doc("DOC_cod_mod") = doc.DOC_cod_mod
                dr_doc("DOC_filename") = doc.DOC_filename
                Me.FullDataset.Tables("TBL_documenti").Rows.Add(dr_doc)
            Next
            Me.FullDataset.AcceptChanges()
        End If

    End Sub

End Class
