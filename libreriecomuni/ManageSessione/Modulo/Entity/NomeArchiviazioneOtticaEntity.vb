﻿Namespace Modulo

    Public Class NomeArchiviazioneOtticaEntity

        Private mAOF_codice As Integer
        Private mAOF_Descrizione As String
        Private mAOF_filename As String

        Public Sub New(ByVal id As Integer, ByVal descrizione As String, ByVal name As String)

            mAOF_codice = id
            mAOF_Descrizione = descrizione
            mAOF_filename = name

        End Sub

        Public Property AOF_codice() As Integer

            Get
                Return mAOF_codice
            End Get
            Set(ByVal value As Integer)
                mAOF_codice = value
            End Set

        End Property

        Public Property AOF_Descrizione() As String

            Get
                Return mAOF_Descrizione
            End Get
            Set(ByVal value As String)
                mAOF_Descrizione = value
            End Set

        End Property

        Public Property AOF_filename() As String

            Get
                Return mAOF_filename
            End Get
            Set(ByVal value As String)
                mAOF_filename = value
            End Set

        End Property

    End Class

End Namespace