﻿Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_elemento_base

        Private _MaxPosizioneY As Decimal
        Private _NeedValue As Boolean
        Private _RawPrintItem As String
        Private _TipoPrintItem As eSottoTipoItem

        Public ReadOnly Property Codice As Integer

            Get
                Return GetValueFromXML(XMLItem, "CodiceItem", -999)
            End Get

        End Property

        Public Property Colore As Color

        Public ReadOnly Property ColoreBrush As Brush

            Get
                Return New SolidBrush(Me.Colore)
            End Get

        End Property

        Public ReadOnly Property GetValueTAG() As Xml.XmlNode

            Get
                Dim xnWork As Xml.XmlNode
                Dim xnTmp As Xml.XmlNode

                If Not Me.XMLItem.SelectSingleNode("value") Is Nothing Then
                    xnTmp = Me.XMLItem.SelectSingleNode("value").Clone
                    Me.XMLItem.RemoveChild(Me.XMLItem.SelectSingleNode("value"))
                    xnWork = Me.XMLItem.OwnerDocument.CreateElement("values")
                    xnWork.InnerXml = String.Concat("<valuesItem>", xnTmp.OuterXml, "</valuesItem>")
                    Me.XMLItem.AppendChild(xnWork)
                End If
                Return Me.XMLItem
            End Get

        End Property

        ''' <summary>
        ''' Indica se l'elemento in questione è un elemento di stampa: pagina, blocco, sezione 
        ''' </summary>
        ''' <param name="FixedType"></param>
        ''' <value></value>
        ''' <returns>True: elemento di stampa, False: Altro</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property IsElementoStampa(Optional ByVal FixedType As eTipoElemento = eTipoElemento.TEL_nonspecificato) As Boolean

            Get
                Dim lRtn As Boolean

                lRtn = TipoElemento = eTipoElemento.TEL_blocco Or TipoElemento = eTipoElemento.TEL_item Or
                       TipoElemento = eTipoElemento.TEL_sezione Or TipoElemento = eTipoElemento.TEL_corpo
                If (FixedType <> eTipoElemento.TEL_nonspecificato) Then
                    lRtn = (TipoElemento = FixedType)
                End If
                Return lRtn
            End Get

        End Property

        Public Property MaxPosizioneY As Decimal

            Get
                Return _MaxPosizioneY
            End Get
            Set(ByVal value As Decimal)
                If (value = Decimal.MinValue) And (Me.Posizione.Count > 0) Then
                    _MaxPosizioneY = Math.Max(_MaxPosizioneY, Posizione("Y"))
                Else
                    _MaxPosizioneY = Math.Max(_MaxPosizioneY, value)
                End If
            End Set

        End Property

        '
        ' Indica che l'elemento necessita diun valore per la stampa.
        '
        Public ReadOnly Property NeedValue As Boolean

            Get
                Return _NeedValue
            End Get

        End Property

        '
        ' Nome dell'elemento.
        '
        Public ReadOnly Property Nome As String

            Get
                Return GetValueFromXML(XMLItem, "name")
            End Get

        End Property

        '
        ' Indica la posizione nella quale occorre stampare l'elemento.
        '
        Public Property Posizione As Dictionary(Of String, Decimal)

        Public ReadOnly Property RawPrintItem As String

            Get
                If _RawPrintItem = "" Then
                    _RawPrintItem = GetValueFromXML(XMLItem, "PrintItemType", "text").ToLower
                End If
                Return _RawPrintItem
            End Get

        End Property

        Public ReadOnly Property SottoTipoElemento As eSottoTipoItem

            Get
                '   Dim cTmp As String

                If TipoElemento = eTipoElemento.TEL_Item Then
                    ' _RawPrintItem = GetValueFromXML(XMLItem, "PrintItemType", "text").ToLower
                    If RawPrintItem.StartsWith("bc_") Then
                        Return eSottoTipoItem.BarCode
                    ElseIf RawPrintItem = "form" Then
                        Return eSottoTipoItem.Fincatura
                    ElseIf RawPrintItem = "image" Then
                        Return eSottoTipoItem.Immagine
                    ElseIf RawPrintItem = "text" Then
                        Return eSottoTipoItem.Testo
                    ElseIf RawPrintItem = "testoinvertito" Then
                        Return eSottoTipoItem.TestoInvertito
                    ElseIf RawPrintItem = "grafico1" Then
                        Return eSottoTipoItem.Grafico1
                    ElseIf RawPrintItem = "grafico2" Then
                        Return eSottoTipoItem.Grafico2
                    ElseIf RawPrintItem = "line" Then
                        Return eSottoTipoItem.Linea
                    ElseIf RawPrintItem = "prestampato" Then
                        Return eSottoTipoItem.PreStampato
                    ElseIf (RawPrintItem.StartsWith("rectangle")) Then
                        Return eSottoTipoItem.Rettangolo
                    Else
                        Return eSottoTipoItem.TipoNonGestito
                    End If
                End If
            End Get

        End Property

        Public ReadOnly Property TipoElemento As eTipoElemento

            Get
                If (XMLItem.Name.ToLower() = "modulo") Or (XMLItem.Name.ToLower() = "#document") Then
                    Return eTipoElemento.TEL_modulo
                ElseIf XMLItem.Name.ToLower() = "blocco" Then
                    Return eTipoElemento.TEL_Blocco
                ElseIf XMLItem.Name.ToLower() = "corpo" Then
                    Return eTipoElemento.TEL_corpo
                ElseIf XMLItem.Name.ToLower() = "item" Then
                    Return eTipoElemento.TEL_Item
                ElseIf XMLItem.Name.ToLower() = "intestazione" Then
                    Return eTipoElemento.TEL_Sezione
                ElseIf XMLItem.Name.ToLower() = "page" Then
                    Return eTipoElemento.TEL_Pagina
                ElseIf XMLItem.Name.ToLower() = "piediblocco" Then
                    Return eTipoElemento.TEL_Sezione
                ElseIf XMLItem.Name.ToLower() = "sezione" Then
                    Return eTipoElemento.TEL_Sezione
                Else
                    Return eTipoElemento.TEL_Sconosciuto
                End If
            End Get

        End Property

        Public ReadOnly Property TipoSpeciale As eTipoSpecialeItem

            Get
                Dim cTipo As String

                cTipo = ""
                If XMLItem.SelectSingleNode("SpecialType") IsNot Nothing Then
                    cTipo = XMLItem.SelectSingleNode("SpecialType").InnerText
                End If
                If (cTipo.ToLower() = "titolo") Then
                    Return eTipoSpecialeItem.Titolo
                Else
                    Return eTipoSpecialeItem.NonImpostato
                End If
            End Get

        End Property

        Public Property TipoCarattere As Font

        Public Property XMLItem As Xml.XmlNode

        Public ReadOnly Property DaStampare As Boolean

            Get
                Return (GetAttrFromXML(Me.XMLItem, "toprint", "1") = 1) And (GetAttrFromXML(Me.XMLItem, "printonce", "0") <= 1)
            End Get

        End Property

        Public Property GiaStampato As Boolean

        Shadows ReadOnly Property _PosizioneItmXML As Xml.XmlNode

            Get
                Return XMLItem.SelectSingleNode("Posizione")
            End Get

        End Property

        Shadows Property _PosizioniFisse As CLS_posizioni

        Shadows ReadOnly Property _PosItemExist As Boolean

            Get
                Return _PosizioneItmXML IsNot Nothing
            End Get

        End Property

        Public Sub New()

        End Sub

        Public Sub New(ByVal xi As Xml.XmlNode)
            MyBase.New()

            XMLItem = xi
            Posizione = New Dictionary(Of String, Decimal)
            _NeedValue = False
            '_IndiceRiutilizzo = Integer.MinValue

        End Sub

        Public Sub New(ByVal xi As Xml.XmlNode, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal uppos As Dictionary(Of String, Decimal))

            Me.New(xi)
            _PosizioniFisse = posi
            _MaxPosizioneY = Decimal.MinValue

            '
            ' Alcuni valori hanno bisogno di recuperare il proprio valore dal DB o dall'elemento.
            '
            _NeedValue = (Me.SottoTipoElemento = eSottoTipoItem.BarCode) Or (Me.SottoTipoElemento = eSottoTipoItem.Testo) Or (Me.SottoTipoElemento = eSottoTipoItem.TestoInvertito) Or (Me.SottoTipoElemento = eSottoTipoItem.Fincatura) Or (Me.SottoTipoElemento = eSottoTipoItem.PreStampato)

            '
            ' Recupera la posizione dell'elemento
            '
            If Not (Me.SottoTipoElemento = eSottoTipoItem.Fincatura) Then
                If Not (Me.TipoElemento = eTipoElemento.TEL_pagina) Then
                    GetPosizione(uppos)
                End If
                GetColor(Me.XMLItem, upColor)
                GetFont(Me.XMLItem, upFont)
            End If

        End Sub

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal uppos As Dictionary(Of String, Decimal))

            Me.New(eb.XMLItem)
            CheckCondizioni = eb.CheckCondizioni
            _PosizioniFisse = posi
            _MaxPosizioneY = Decimal.MinValue

            '
            ' Alcuni valori hanno bisogno di recuperare il proprio valore dal DB o dall'elemento.
            '
            _NeedValue = (Me.SottoTipoElemento = eSottoTipoItem.BarCode) Or (Me.SottoTipoElemento = eSottoTipoItem.Testo) Or (Me.SottoTipoElemento = eSottoTipoItem.TestoInvertito) Or (Me.SottoTipoElemento = eSottoTipoItem.Fincatura) Or (Me.SottoTipoElemento = eSottoTipoItem.PreStampato)

            '
            ' Recupera la posizione dell'elemento
            '
            If Not (Me.SottoTipoElemento = eSottoTipoItem.Fincatura) Then
                If Not (Me.TipoElemento = eTipoElemento.TEL_pagina) Then
                    GetPosizione(uppos)
                End If
                GetColor(Me.XMLItem, upColor)
                GetFont(Me.XMLItem, upFont)
            End If

        End Sub

        Public Overridable Sub GetValue()

        End Sub

        Public Sub SettaMassimaPosizione(ByVal nValueX As Decimal, ByVal nValueY As Decimal)

            If Me.Nome > "" Then
                If Me._PosizioniFisse.NamedItem.ContainsKey(Me.Nome) Then
                    _PosizioniFisse.NamedItem(Me.Nome) = New Decimal() {nValueX, nValueY}
                Else
                    _PosizioniFisse.NamedItem.Add(Me.Nome, New Decimal() {nValueX, nValueY})
                End If
            End If

        End Sub

        '
        ' Gestione della posizione dell'elemento
        '
        Public Sub GetPosizione(ByVal upPos As Dictionary(Of String, Decimal))

            Try
                '
                ' Istanzia l'oggetto Posizione
                '
                Posizione.Add("X", Nothing)
                Posizione.Add("Y", Nothing)
                If upPos IsNot Nothing AndAlso upPos.Count > 0 Then
                    Posizione("X") = upPos("X")
                    Posizione("Y") = upPos("Y")
                End If
                If _PosItemExist Then
                    GetPosizioneAsse("X")
                    GetPosizioneAsse("Y")
                End If
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Private Sub GetPosizioneAsse(ByVal cAsse As String)
            Dim nRelativePos As Decimal()
            Dim nForceOffset As Integer

            Try
                If Not _PosizioneItmXML.SelectSingleNode(cAsse) Is Nothing Then
                    If IsNumeric(_PosizioneItmXML.SelectSingleNode(cAsse).InnerText) Then
                        Posizione(cAsse) = _PosizioneItmXML.SelectSingleNode(cAsse).InnerText
                    Else
                        If cAsse = "X" Then
                            Posizione(cAsse) = _PosizioniFisse.Fisse(_PosizioneItmXML.SelectSingleNode(cAsse).InnerText)(0)
                        Else
                            Posizione(cAsse) = _PosizioniFisse.Fisse(_PosizioneItmXML.SelectSingleNode(cAsse).InnerText)(1)
                        End If
                    End If
                Else
                    If Not _PosizioneItmXML.SelectSingleNode(String.Concat("Relative", cAsse)) Is Nothing Then
                        If Not _PosizioneItmXML.SelectSingleNode(String.Concat("Relative", cAsse, "/LinkedItem")) Is Nothing Then
                            'nRelativePos = _PosizioniFisse.NamedItem(String.Concat("LI_", _PosizioneItmXML.SelectSingleNode(String.Concat("Relative", cAsse, "/LinkedItem")).InnerText))
                            If _PosizioniFisse.NamedItem.ContainsKey(_PosizioneItmXML.SelectSingleNode(String.Concat("Relative", cAsse, "/LinkedItem")).InnerText) Then
                                nRelativePos = _PosizioniFisse.NamedItem(_PosizioneItmXML.SelectSingleNode(String.Concat("Relative", cAsse, "/LinkedItem")).InnerText)
                                nForceOffset = GetValueFromXML(_PosizioneItmXML.SelectSingleNode(String.Concat("Relative", cAsse)), "ForceOffSet", "0")
                                Posizione(cAsse) = nRelativePos(1)
                                If (nRelativePos(2) = 1) Or nForceOffset = 1 Then
                                    Posizione(cAsse) += _PosizioneItmXML.SelectSingleNode(String.Concat("Relative", cAsse, "/OffSet")).InnerText
                                End If
                            End If
                        Else
                            Posizione(cAsse) += _PosizioneItmXML.SelectSingleNode(String.Concat("Relative", cAsse, "/OffSet")).InnerText
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw New Exception("Errore nella funzione GetSubPos", ex)
            End Try

        End Sub

        Public Sub ChangePrintItemTo(ByVal nuovoTipo As eSottoTipoItem)

            _TipoPrintItem = nuovoTipo
            If eSottoTipoItem.Testo Then
                _RawPrintItem = "text"
            End If

        End Sub

        ''' <summary>
        ''' Indica il valore dell'indice di riutilizzo se è da riutilizzare
        ''' </summary>
        ''' <value>Intero</value>
        ''' <returns></returns>
        ''' <remarks>-1 significa che l'elemento non è da riutilizzare. 0 è il primo elemento della lista.</remarks>
        Public Property IndiceRiutilizzo As Integer

            Get
                Return GetAttrFromXML(Me.XMLItem, "IndiceRiutilizzo", "-1")
            End Get
            Set(ByVal value As Integer)
                SetAttrToXML(Me.XMLItem, "IndiceRiutilizzo", value)
            End Set

        End Property

        Shadows Function GetXmlNodeFromXML(ByVal xnTmp As Xml.XmlNode, ByVal cPath As String) As Xml.XmlNode

            Return xnTmp.SelectSingleNode(cPath)

        End Function

        Public Function IsDaStampare() As Boolean
            Dim lStampabile As Boolean

            lStampabile = False
            If TypeOf XMLItem Is System.Xml.XmlElement Then
                lStampabile = (GetAttrFromXML(XMLItem, "toprint", "1") = 1) And (GetAttrFromXML(XMLItem, "printonce", "0") <= 1)
            End If
            Return lStampabile

        End Function

        Private Sub GetColor(ByVal xmlItem As Xml.XmlNode, ByVal _UpColor As Brush)
            Dim cColor As String
            Dim nRed As Integer
            Dim nGre As Integer
            Dim nBlu As Integer

            If xmlItem.SelectSingleNode("Color") IsNot Nothing Then
                cColor = GetValueFromXML(xmlItem, "Color", "black")
            ElseIf xmlItem.SelectSingleNode("ForeColor") IsNot Nothing Then
                cColor = GetValueFromXML(xmlItem, "ForeColor", "black")
            Else
                cColor = "black"
            End If

            If (cColor.Contains(",")) Then
                nRed = Integer.Parse(cColor.Split(",")(0))
                nGre = Integer.Parse(cColor.Split(",")(1))
                nBlu = Integer.Parse(cColor.Split(",")(2))
                Me.Colore = System.Drawing.Color.FromArgb(nRed, nGre, nBlu)
            Else
                Me.Colore = System.Drawing.Color.FromName(cColor)
            End If

        End Sub

        Private Sub GetFont(ByVal xmlItem As Xml.XmlNode, ByVal _UpFont As Font)
            Dim xmlItemTMP As Xml.XmlNode
            Dim xmlFont As Xml.XmlNode
            Dim cFont As String
            Dim nSize As Integer
            Dim FntStyle As New FontStyle
            Dim lBold As Boolean = False
            Dim lItalic As Boolean = False
            Dim lStrikeout As Boolean = False
            Dim lUnderline As Boolean = False

            If (xmlItem.SelectSingleNode("Font") Is Nothing) Then
                Me.TipoCarattere = _UpFont
            Else
                xmlFont = xmlItem.SelectSingleNode("Font")
                If Not _UpFont Is Nothing Then
                    cFont = _UpFont.FontFamily.Name
                    nSize = _UpFont.Size
                    lBold = _UpFont.Bold
                    lItalic = _UpFont.Italic
                    lStrikeout = _UpFont.Strikeout
                    lUnderline = _UpFont.Underline
                End If

                For Each xmlItemTMP In xmlFont.ChildNodes
                    Select Case xmlItemTMP.Name.ToLower
                        Case Is = "nome"
                            cFont = xmlItemTMP.InnerText
                        Case Is = "size"
                            nSize = xmlItemTMP.InnerText
                        Case Is = "bold"
                            lBold = xmlItemTMP.InnerText = 1
                        Case Is = "italic"
                            lItalic = xmlItemTMP.InnerText = 1
                        Case Is = "strikeout"
                            lStrikeout = xmlItemTMP.InnerText = 1
                        Case Is = "underline"
                            lUnderline = xmlItemTMP.InnerText = 1
                    End Select
                Next

                ' Imposta lo stile del fnt a normale.
                FntStyle = FontStyle.Regular
                If lBold Then
                    FntStyle = FntStyle Or FontStyle.Bold
                End If
                If lItalic Then
                    FntStyle = FntStyle Or FontStyle.Italic
                End If
                If lStrikeout Then
                    FntStyle = FntStyle Or FontStyle.Strikeout
                End If
                If lUnderline Then
                    FntStyle = FntStyle Or FontStyle.Underline
                End If
                Me.TipoCarattere = New Font(cFont, nSize, FntStyle)
            End If

        End Sub

        Public Overridable Property ValoreS As String

        Public Overridable WriteOnly Property ValoreA() As ArrayList

            Set(ByVal value As ArrayList)

            End Set

        End Property

        ''' <summary>
        ''' Indica se un elemento da stampare deve proseguire a stampare i dati dal punto dove era stato lasciato 
        ''' </summary>
        ''' <value></value>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ContinuaDati As Boolean

            Get
                Return GetAttrFromXML(XMLItem, "continuaDati", "0") = 1
            End Get

        End Property

        Public Sub RimuoviAttributoElemento(ByVal cAttrName As String)

            Me.XMLItem.Attributes.RemoveNamedItem(cAttrName)

        End Sub

        Public Sub ImpostaAttributoElemento(ByVal cAttrName As String, ByVal cValue As String)

            XMLItem.Attributes.Append(XMLItem.OwnerDocument.CreateAttribute(cAttrName))
            XMLItem.Attributes(cAttrName).Value = cValue

        End Sub

        Public Sub SegnaDaRiusare(ByVal nIndiceRiutilizzo As Integer)

            Me.ImpostaAttributoElemento("IndiceRiutilizzo", nIndiceRiutilizzo + 1)

        End Sub

        Delegate Function DelControllaCondizioni(ByVal xnItem As Xml.XmlNode, ByVal cRelNameMaster As String, ByVal cRelNameSlave As String) As Boolean

        Public Property CheckCondizioni As DelControllaCondizioni

        ''' <summary>
        ''' Indica se un elemento è stato inserito dinamicamente ad esempio: elementi per tutte le pagine o codice di imbustamento.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property IsTemporaneo As Boolean

            Get
                Return GetAttrFromXML(XMLItem, "temporaneo", "0") = 1
            End Get

        End Property

        Public ReadOnly Property JumpPrint As Boolean

            Get
                Return GetAttrFromXML(XMLItem, "JumpPrint", "0") = 1
            End Get

        End Property

        Public Property PaginaPreStampato As Integer

            Get
                Return GetValueFromXML(Me.XMLItem, "PaginaPreStampato", "-1")
            End Get
            Set(ByVal value As Integer)
                Dim xnWork As Xml.XmlNode

                xnWork = Me.XMLItem.SelectSingleNode("PaginaPreStampato")
                If xnWork Is Nothing Then
                    xnWork = Me.XMLItem.OwnerDocument.CreateElement("PaginaPreStampato")
                    xnWork.InnerText = value
                    Me.XMLItem.AppendChild(xnWork)
                Else
                    xnWork.InnerText = value
                End If
            End Set

        End Property

    End Class

End Namespace