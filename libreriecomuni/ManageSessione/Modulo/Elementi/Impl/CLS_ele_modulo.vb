﻿Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_modulo
        Inherits CLS_elemento_base

        Public Property Defaults As CLS_ele_defaults


        '
        ' Contiene le posizioni fisse di ciascun Modulo.
        '
        Public Property PosizioniFisseBase As Dictionary(Of String, Decimal())

        '
        ' Lista di tutte le pagine del modulo in stampa.
        '
        Private Property _ListaPagineModulo As List(Of CLS_ele_pagina)
        Private Property _ListaPaginePrestampato As List(Of CLS_ele_pagina_prestampato)


        '
        ' Lista di tutte le pagine da stampare del modulo.
        '
        Public Property ListaPagineDaStampare As List(Of CLS_ele_pagina)

        ''' <summary>
        ''' Indica il numero di pagine contenute nell'array delle pagine da stampare.
        ''' </summary>
        ''' <value></value>
        ''' <returns>Integer</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property NumPagineDaStampare As Integer

            Get
                Return ListaPagineDaStampare.Count
            End Get

        End Property

        Public Sub New(ByVal eb As CLS_elemento_base)
            MyBase.New(eb.XMLItem, Nothing, Nothing, Nothing, Nothing)

            '
            ' Caricamento delle posizioni fisse.
            '
            Carica_PosizioniFisse()

            '
            ' Carica i default del modulo
            '
            Defaults = New CLS_ele_defaults(New CLS_elemento_base(Me.XMLItem.SelectSingleNode(MST_costanti.itm_default)), Nothing, Nothing, Nothing, Nothing)

            '
            ' Caricamento delle pagine di cui è composto il modulo.
            '
            _ListaPagineModulo = New List(Of CLS_ele_pagina)
            For Each xnItem As Xml.XmlNode In eb.XMLItem.SelectNodes("Modulo/Pages/Page")
                _ListaPagineModulo.Add(New CLS_ele_pagina(xnItem.Clone, True))
            Next
            _ListaPaginePrestampato = New List(Of CLS_ele_pagina_prestampato)
            For Each xnItem As Xml.XmlNode In eb.XMLItem.SelectNodes("Modulo/Pages/Prestampato")
                _ListaPaginePrestampato.Add(New CLS_ele_pagina_prestampato(xnItem.Clone, True))
            Next

        End Sub

        Public Sub New(ByVal xi As Xml.XmlNode)
            MyBase.New(xi)

        End Sub

        ''' <summary>
        ''' Prepara la lista delle pagine da stampare.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub PreparaListaPagineDaStampare(ByVal lDettaglioRichiesto As Boolean, ByVal lSoloDettaglio As Boolean)
            Dim nPagina As Integer
            Dim l2Print As Boolean

            Try
                Me.ListaPagineDaStampare = New List(Of CLS_ele_pagina)
                '
                ' Aggiunge eventuali elementi di tipo pagina
                '
                If Not lSoloDettaglio Then
                    For Each page As CLS_ele_pagina In Me._ListaPagineModulo
                        l2Print = page.IsDaStampare()

                        '
                        ' Se l'elemento è da stampare allora verifico eventuali condizioni presenti.
                        '
                        If l2Print Then
                            l2Print = CheckCondizioni(page.XMLItem, "", "")
                        End If

                        '
                        ' Se l'elemento è da stampare lo aggiungo all'elenco delle pagine da stampare.
                        '
                        If l2Print Then
                            Me.ListaPagineDaStampare.Add(New CLS_ele_pagina(page.XMLItem.Clone, False))
                            If Me.ListaPagineDaStampare.Count = 1 Then
                                CType(Me.ListaPagineDaStampare(Me.ListaPagineDaStampare.Count - 1), CLS_ele_pagina).AddSwitchToDettaglio(0)
                            End If
                        End If
                    Next
                End If

                '
                ' Se è previsto l'inserimento di un prestampato allora lo utilizza.
                '
                If (_ListaPaginePrestampato.Count > 0) Then
                    For Each pres As CLS_ele_pagina_prestampato In _ListaPaginePrestampato
                        l2Print = pres.IsDaStampare()

                        '
                        ' Se l'elemento è da stampare allora verifico eventuali condizioni presenti.
                        '
                        If l2Print Then
                            l2Print = CheckCondizioni(pres.XMLItem, "", "")
                        End If

                        '
                        ' Se l'elemento è da stampare lo aggiungo all'elenco delle pagine da stampare.
                        '
                        If l2Print Then
                            Dim xi As Xml.XmlNode
                            Dim xnTmp As Xml.XmlNode

                            xi = pres.XMLItem.Clone.OwnerDocument.CreateElement("Page")
                            For Each xnTmp In pres.XMLItem.Clone.ChildNodes
                                xi.AppendChild(xnTmp.Clone)
                            Next
                            For i As Integer = 1 To pres.PaginePreStampato
                                Dim page As CLS_ele_pagina
                                page = New CLS_ele_pagina(xi.Clone, False)
                                page.IsPrestampato = True
                                page.PaginaPreStampato = i
                                Me.ListaPagineDaStampare.Add(page)
                            Next

                        End If
                    Next

                    ' *************************************************************************** '
                    ' Mantiene il numero di pagine pari qulora sia richiesto dal modulo.          '
                    ' *************************************************************************** '
                    MantieniNumeroPaginePari(True)
                End If

                If (Me.ConDettaglio And lDettaglioRichiesto) Or lSoloDettaglio Then
                    nPagina = 1
                    For Each page As CLS_ele_pagina In Me._ListaPagineModulo
                        l2Print = page.IsDaStampare()

                        '
                        ' Se l'elemento è da stampare allora verifico eventuali condizioni presenti.
                        '
                        If l2Print Then
                            l2Print = CheckCondizioni(page.XMLItem, "", "")
                        End If

                        '
                        ' Se l'elemento è da stampare lo aggiungo all'elenco delle pagine da stampare.
                        '
                        If l2Print Then
                            Me.ListaPagineDaStampare.Add(New CLS_ele_pagina(page.XMLItem.Clone, False))
                            If nPagina = 1 Then
                                CType(Me.ListaPagineDaStampare(Me.ListaPagineDaStampare.Count - 1), CLS_ele_pagina).AddSwitchToDettaglio(1)
                            End If
                        End If
                        nPagina += 1
                    Next
                    MantieniNumeroPaginePari(False)
                End If
            Catch ex As Exception
                Throw New Exception("ELEMOD-000001: Preparazione delle pagine da stampare", ex)
            End Try

        End Sub

        ''' <summary>
        ''' Il metodo formatta la pagina in stampa
        ''' </summary>
        ''' <param name="nPagina"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function FormattaPaginaInStampa(ByVal nPagina As Integer) As CLS_ele_pagina
            Dim xnWrkPageA As Xml.XmlNode
            Dim xnFinc As Xml.XmlNode
            Dim xnWork As Xml.XmlNode

            Try
                xnWrkPageA = Me.ListaPagineDaStampare(nPagina).XMLItem
                If Me.ExistElementiSuTutteLePagine And Me.ListaPagineDaStampare(nPagina).RiportaElementiTuttePagine Then

                    '
                    ' Ricerca il nodo della fincatura
                    '
                    xnFinc = GetNodoFincatura(xnWrkPageA)
                    If NodeNull(xnFinc) Then
                        xnFinc = xnWrkPageA.FirstChild
                    End If

                    If (Not Me.ConDettaglio Or (Me.ConDettaglio And Not Me.ListaPagineDaStampare(nPagina).SwitchStampaDettaglioExist)) Then
                        '
                        ' Inserisce nodi da stampare su tutte le pagine.
                        '
                        For Each xnWork In Me.ElementiSuTutteLePagine.ChildNodes
                            If CheckCondizioni(xnWork, "", "") Then
                                xnWork.Attributes.Append(xnWork.OwnerDocument.CreateAttribute("temporaneo"))
                                xnWork.Attributes("temporaneo").Value = 1
                                xnWrkPageA.InsertAfter(xnWork.Clone, xnFinc)
                            End If
                        Next
                    End If
                End If

            Catch ex As Exception
                Throw New Exception("Errore nel recupero della pagina da stampare", ex)
            End Try
            Return New CLS_ele_pagina(xnWrkPageA)

        End Function

        Private Function GetNodoFincatura(ByVal xnWork1 As Xml.XmlNode) As Xml.XmlNode
            Dim xnFinc As Xml.XmlNode
            Dim xnWork As Xml.XmlNode

            xnFinc = Nothing
            xnWork = xnWork1.FirstChild
            While Not (xnWork Is Nothing) And (xnFinc Is Nothing)
                If GetValueFromXML(xnWork, "PrintItemType") = "form" Then
                    xnFinc = xnWork
                End If
                xnWork = xnWork.NextSibling
            End While
            Return xnFinc

        End Function

        Public ReadOnly Property ConDettaglio As Boolean

            Get
                Dim lRtn As Boolean

                If Not Me.XMLItem.SelectSingleNode("Modulo/modulocondettaglio") Is Nothing Then
                    lRtn = Me.XMLItem.SelectSingleNode("Modulo/modulocondettaglio").InnerText = "1"
                Else
                    lRtn = False
                End If
                Return lRtn
            End Get

        End Property

        '
        ' La procedura procede al caricamento delle posizioni fisse di stampa.
        '
        Private Sub Carica_PosizioniFisse()
            Dim nPosX As Decimal
            Dim nPosY As Decimal
            Dim cNomePosizione As String

            PosizioniFisseBase = New Dictionary(Of String, Decimal())
            If Not Me.XMLItem.SelectSingleNode(MMO_Costanti.itm_pos_fisse) Is Nothing Then
                For Each xmlItem As Xml.XmlNode In Me.XMLItem.SelectSingleNode(MMO_Costanti.itm_pos_fisse).SelectNodes("Posizione")
                    cNomePosizione = GetValueFromXML(xmlItem, "Name", "")
                    If cNomePosizione = "" Then
                        Throw New Exception("Nome posizione non specificata.")
                    Else
                        nPosX = GetValueFromXML(xmlItem, "ValoreX", -1)
                        nPosY = GetValueFromXML(xmlItem, "ValoreY", -1)
                        PosizioniFisseBase.Add(cNomePosizione, New Decimal() {nPosX, nPosY})
                    End If
                Next
            End If

        End Sub

        Private ReadOnly Property XmlDocumentoModulo As Xml.XmlDocument

            Get
                Return Me.XMLItem
            End Get

        End Property

        Private Function SetEmptyPage(Optional ByVal nNoAllPageItems As Integer = 0) As Xml.XmlNode
            Dim xnWork As Xml.XmlNode
            Dim xnWork1 As Xml.XmlNode
            Dim xaWork As Xml.XmlAttribute
            Dim xdTemp As Xml.XmlDocument

            xnWork = XmlDocumentoModulo.CreateElement("Page")
            xaWork = XmlDocumentoModulo.CreateAttribute("PaginaVuota")
            xaWork.Value = 1
            xnWork.Attributes.Append(xaWork)

            If NodeExist(XmlDocumentoModulo, "Modulo/Default/PageSettings") Then
                xnWork1 = XmlDocumentoModulo.SelectSingleNode("Modulo/Default/PageSettings").FirstChild
            Else
                xdTemp = New Xml.XmlDocument
                xdTemp.LoadXml("<PageSettings><MaxPageLenght>270</MaxPageLenght><tray>2</tray><item><PrintItemType>form</PrintItemType><Posizione><X>0</X><Y>0</Y></Posizione><value>Fincature\page_n.jpg</value></item></PageSettings>")
                xnWork1 = XmlDocumentoModulo.ImportNode(xdTemp.ChildNodes(0), True).FirstChild
            End If
            While NodeNotNull(xnWork1)
                xnWork.AppendChild(xnWork1.Clone)
                xnWork1 = xnWork1.NextSibling
            End While
            xnWork1 = XmlDocumentoModulo.CreateElement("NoAllPageItems")
            xnWork1.InnerText = nNoAllPageItems
            xnWork.AppendChild(xnWork1.Clone)
            Return xnWork

        End Function

        ''' <summary>
        ''' Duplica una pagina che deve essere ristampata.
        ''' </summary>
        ''' <param name="nPagina"></param>
        ''' <remarks></remarks>
        Public Sub DuplicaPagina(ByVal nPagina As Integer)
            Dim xnItem As Xml.XmlNode

            Try
                '
                ' Rimuovo gli elementi da stampare su tutte le pagine e l'eventuale codice di imbustamento.
                '
                xnItem = RimuoviElementiTemporanei(ListaPagineDaStampare(nPagina).XMLItem.Clone)

                '
                ' Rimuovo gli elementi che già abbiamo stampato e che non debbono essere riutilizzati.
                '
                ListaPagineDaStampare.Insert(nPagina + 1, New CLS_ele_pagina(RimuoviElementiRiutilizzare(xnItem)))

                ' *************************************************************************** '
                ' Qualora ci sia una pagina che deve essere obbligatoriamente stampata su una ' 
                ' pagina dispari provvedo a posizionarla su un valore dispari.                '
                ' *************************************************************************** '
                nPagina = nPagina + 1
                If (Me.ListaPagineDaStampare(nPagina).ForzaSuPaginaDispari) And ((nPagina + 1) Mod 2 = 0) Then
                    Me.ListaPagineDaStampare.Insert(nPagina, New CLS_ele_pagina(SetEmptyPage(1)))
                End If

                ' *************************************************************************** '
                ' Mantiene il numero di pagine pari qulora sia richiesto dal modulo.          '
                ' *************************************************************************** '
                MantieniNumeroPaginePari(True)
            Catch ex As Exception
                Throw New Exception("Aggiunta pagina supplementare", ex)
            End Try

        End Sub

        Private Sub MantieniNumeroPaginePari(Optional ByVal lAddPageSoloSePari As Boolean = False)

            If IsModuloConPaginePari Then
                If Me.ConDettaglio And lAddPageSoloSePari Then
                    '
                    ' Se il modulo prevede la stampa di un dettaglio allora procedo con l'inserimento forzato di una pagina vuota da stampare solo in caso sia con un numero di pagina pari.
                    '
                    If Me.NumPagineDaStampare Mod 2 = 1 Then
                        If Me.ListaPagineDaStampare(Me.ListaPagineDaStampare.Count - 1).PaginaVuota Then
                            Me.ListaPagineDaStampare.RemoveAt(Me.ListaPagineDaStampare.Count - 1)
                        Else
                            Me.ListaPagineDaStampare.Add(New CLS_ele_pagina(SetEmptyPage(0)))
                            Me.ListaPagineDaStampare(Me.ListaPagineDaStampare.Count - 1).AddStampaSoloSePari()
                        End If
                    End If
                Else
                    If Me.NumPagineDaStampare Mod 2 = 1 Then
                        If Me.ListaPagineDaStampare(Me.ListaPagineDaStampare.Count - 1).PaginaVuota Then
                            Me.ListaPagineDaStampare.RemoveAt(Me.ListaPagineDaStampare.Count - 1)
                        Else
                            Me.ListaPagineDaStampare.Add(New CLS_ele_pagina(SetEmptyPage(0)))
                        End If
                    End If
                End If
            End If

        End Sub

        Private Function RimuoviElementiTemporanei(ByVal xnPagina As Xml.XmlNode) As Xml.XmlNode
            Dim eb As CLS_elemento_base
            Dim i As Integer

            i = 0
            While (i < xnPagina.ChildNodes.Count)
                eb = New CLS_elemento_base(xnPagina.ChildNodes(i))
                If eb.IsTemporaneo Then
                    xnPagina.RemoveChild(eb.XMLItem)
                End If
                i += 1
            End While
            Return xnPagina

        End Function

        '
        ' Rimuove gli elementi che sono 
        '
        Private Function RimuoviElementiRiutilizzare(ByVal xnPagina As Xml.XmlNode, Optional ByVal lPadreBlocco As Boolean = False) As Xml.XmlNode
            Dim lPrimaIndiceRiutilizzo As Boolean
            Dim eb As CLS_elemento_base
            Dim i As Integer

            i = 0
            lPrimaIndiceRiutilizzo = True
            While (i < xnPagina.ChildNodes.Count)
                eb = New CLS_elemento_base(xnPagina.ChildNodes(i))
                If (eb.IsElementoStampa) Then
                    '
                    ' Se c'è un codice di riutilizzo ed è maggiore di 0 allora procedo con l'analisi del suo contenuto.
                    '
                    If (eb.IndiceRiutilizzo > -1) Then
                        '
                        ' Qualora il codice di riutilizzo sia maggiore di 0 allora procedo con la pulizia negli elementi figlio
                        '
                        lPadreBlocco = lPadreBlocco Or eb.TipoElemento = eTipoElemento.TEL_blocco
                        If (eb.IndiceRiutilizzo > 0) Then
                            RimuoviElementiRiutilizzare(eb.XMLItem, lPadreBlocco)
                            eb.IndiceRiutilizzo -= 1
                        End If
                        If (eb.IndiceRiutilizzo = 0) Then
                            i = xnPagina.ChildNodes.Count
                        End If

                        '
                        ' Sostituisco la posizione con la nuova posizione a pagina nuova.
                        '
                        If (eb.TipoElemento <> eTipoElemento.TEL_corpo) Then
                            If (eb.XMLItem.SelectSingleNode("NewPagePosition") Is Nothing) Then
                                Throw New Exception("Posizione non specificata sulla nuova pagina per un elemento")
                            Else
                                If (NodeNotNull(eb.XMLItem.SelectSingleNode("Posizione"))) Then
                                    eb.XMLItem.RemoveChild(eb.XMLItem.SelectSingleNode("Posizione"))
                                End If
                                eb.XMLItem.AppendChild(eb.XMLItem.SelectSingleNode("NewPagePosition").SelectSingleNode("Posizione").Clone)
                            End If
                        End If
                        '
                        ' Rimuovo l'attributo per il riutilizzo del componente.
                        '
                        eb.RimuoviAttributoElemento("type")
                        eb.RimuoviAttributoElemento("IndiceRiutilizzo")

                        '
                        ' Nel caso l'elemento riutilizzato fosse un blocco allora imposto la variabileper continuare ad elencare i dati da dove li abbiamo lasciati.
                        '
                        eb.ImpostaAttributoElemento("continuaDati", 1)
                        lPrimaIndiceRiutilizzo = False
                    ElseIf eb.IndiceRiutilizzo = -1 And eb.TipoSpeciale <> eTipoSpecialeItem.Titolo And lPrimaIndiceRiutilizzo And Not lPadreBlocco Then
                        xnPagina.RemoveChild(eb.XMLItem)
                        i -= 1
                    ElseIf eb.IndiceRiutilizzo = -1 And eb.TipoSpeciale <> eTipoSpecialeItem.Titolo And lPrimaIndiceRiutilizzo And lPadreBlocco Then
                        eb.ImpostaAttributoElemento("JumpPrint", 1)
                    End If
                ElseIf (eb.XMLItem.NodeType = Xml.XmlNodeType.Comment) Then
                End If
                i += 1
            End While
            Return xnPagina

        End Function

        ''' <summary>
        ''' Indica se il numero di pagine del modulo deve essere mantenuto pari.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private ReadOnly Property IsModuloConPaginePari As Boolean

            Get
                Return GetValueFromXML(Me.XmlDocumentoModulo, MST_costanti.itm_PaginePari, "0") = 1
            End Get

        End Property

        Private ReadOnly Property ElementiSuTutteLePagine As Xml.XmlNode

            Get
                Return XMLItem.SelectSingleNode(MMO_Costanti.itm_AllPagesItem)
            End Get

        End Property

        Private ReadOnly Property ExistElementiSuTutteLePagine As Boolean

            Get
                Return NodeNotNull(ElementiSuTutteLePagine)
            End Get

        End Property

        ReadOnly Property ContainsPreStampato As Boolean

            Get
                Return _ListaPaginePrestampato.Count > 0

            End Get

        End Property

        Public Function GetElementoPrestampato() As CLS_ele_prestampato
            Dim ele As CLS_ele_prestampato

            ele = New CLS_ele_prestampato(_ListaPaginePrestampato(0).XMLItem.SelectSingleNode("item/PrintItemType[.='Prestampato']").ParentNode)

            Return ele

        End Function

        Public Sub SetNumeroPaginePrestampato(ByVal nNumeroPagine As Integer)

            _ListaPaginePrestampato(0).PaginePreStampato = nNumeroPagine

        End Sub

    End Class

End Namespace