Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_itm_grafico
        Inherits CLS_elemento_base

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

        End Sub

        Public ReadOnly Property DatiDaTabella As Boolean

            Get
                Return XMLItem.SelectSingleNode("Series/DataTable") IsNot Nothing
            End Get

        End Property

        Public ReadOnly Property GetValueTAGXvalue(ByVal cValueXML As String) As Xml.XmlNode

            Get
                Dim xnWork As Xml.XmlNode

                xnWork = Me.XMLItem.OwnerDocument.CreateElement("values")
                xnWork.InnerXml = String.Concat("<values><valuesItem><value>", cValueXML, "</value></valuesItem></values>")
            End Get

        End Property

        Public ReadOnly Property GetValueTAGYvalue(ByVal cValueXML As String) As Xml.XmlNode

            Get
                Dim xnWork As Xml.XmlNode

                xnWork = Me.XMLItem.OwnerDocument.CreateElement("values")
                xnWork.InnerXml = String.Concat("<values><valuesItem><value>", cValueXML, "</value></valuesItem></values>")
            End Get

        End Property

        Public ReadOnly Property NomeTabellaDati As String

            Get
                Return XMLItem.SelectSingleNode("Series/DataTable").InnerText
            End Get

        End Property

        Public ReadOnly Property TipoLegendaGrafico As Integer
            Get
                If Me.SottoTipoElemento = eSottoTipoItem.Grafico1 Then
                    Return 1
                ElseIf Me.SottoTipoElemento = eSottoTipoItem.Grafico2 Then
                    Return 2
                Else
                    Return -1
                End If
            End Get

        End Property

        Public ReadOnly Property TipoGrafico As String

            Get
                Return GetValueFromXML(XMLItem, "TipoGrafico", 1)
            End Get

        End Property

        Public ReadOnly Property DimensioniX As Integer

            Get
                Return GetValueFromXML(XMLItem, "dimensioni/larghezza", 1)
            End Get

        End Property

        Public ReadOnly Property DimensioniY As Integer

            Get
                Return GetValueFromXML(XMLItem, "dimensioni/altezza", 1)
            End Get

        End Property

        Public ReadOnly Property Series As Xml.XmlNode

            Get
                Return XMLItem.SelectSingleNode("Series")
            End Get

        End Property

        Public ReadOnly Property UseCustomPalette As Boolean

            Get
                Return XMLItem.SelectSingleNode("colore/CustomPalette") IsNot Nothing
            End Get

        End Property

        Public ReadOnly Property Palette As String

            Get
                Return GetValueFromXML(XMLItem, "colore/Palette", "")
            End Get

        End Property

        Public ReadOnly Property customPalette As String

            Get
                Return GetValueFromXML(XMLItem, "colore/CustomPalette", "")
            End Get

        End Property

        Public Property DataTable As DataTable

    End Class

End Namespace