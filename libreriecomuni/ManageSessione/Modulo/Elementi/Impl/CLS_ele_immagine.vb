Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_immagine
        Inherits CLS_elemento_base

        Public Property ImageFLD As String

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

        End Sub

        Public ReadOnly Property ImmagineFile As String

            Get
                Return ConcatenaFolderFileValue(ImageFLD, GetValueFromXML(XMLItem, "value"))
            End Get

        End Property

        Public ReadOnly Property IsResized As Boolean

            Get
                Return NodeNotNull(ResizedElement)
            End Get

        End Property

        Public ReadOnly Property ResizedX As Single

            Get
                Return GetValueFromXML(ResizedElement, "larghezza")
            End Get

        End Property

        Public ReadOnly Property ResizedY As Single

            Get
                Return GetValueFromXML(ResizedElement, "altezza")
            End Get

        End Property

        Private ReadOnly Property ResizedElement As Xml.XmlNode

            Get
                Return Me.XMLItem.SelectSingleNode("dimensioni")
            End Get

        End Property

    End Class

End Namespace