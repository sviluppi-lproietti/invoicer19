Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_blocco
        Inherits CLS_elemento_base

        Public Sub New(ByVal xi As Xml.XmlNode)
            MyBase.New(xi)

        End Sub

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

            '
            ' Recupera l'item per il box se esiste e lo rimuove dall'item padre
            '
            BoxItem = GetXmlNodeFromXML(XMLItem, "item[@type='blockboxed']")
            If Me.IsBoxed Then
                BoxItem.SelectSingleNode("Posizione/X").InnerText = Posizione("X") + BoxItem.SelectSingleNode("Posizione/X").Attributes("OffSet").Value
                BoxItem.SelectSingleNode("Posizione/Y").InnerText = Posizione("Y") + BoxItem.SelectSingleNode("Posizione/Y").Attributes("OffSet").Value
                XMLItem.RemoveChild(BoxItem)
            End If

        End Sub

        Public Sub SetUpBoxedItem(ByVal nMaxPrintedY As Decimal)

            BoxItem.SelectSingleNode("Dimensioni/X").InnerText = Posizione("X") + BoxItem.SelectSingleNode("Dimensioni/X").Attributes("OffSet").Value - BoxItem.SelectSingleNode("Posizione/X").InnerText
            BoxItem.SelectSingleNode("Dimensioni/Y").InnerText = nMaxPrintedY + BoxItem.SelectSingleNode("Dimensioni/Y").Attributes("OffSet").Value - BoxItem.SelectSingleNode("Posizione/Y").InnerText

        End Sub

        Public Property BoxItem As Xml.XmlNode

        Public ReadOnly Property IsBoxed As Boolean

            Get
                Return BoxItem IsNot Nothing
            End Get

        End Property

        Public ReadOnly Property PosizioneBaseXML As Xml.XmlNode

            Get
                Return Me.XMLItem.SelectSingleNode("NewPagePosition").SelectSingleNode("Posizione").Clone
            End Get

        End Property

        ''' <summary>
        ''' Contiene il nome della tabella con i dati del blocco.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property TabellaBlocco As String

            Get
                Return GetValueFromXML(Me.XMLItem, "Table")
            End Get

        End Property

        Public ReadOnly Property ExistIntestazione As Boolean

            Get
                Return NodeNotNull(Me.Intestazione)
            End Get

        End Property

        Public ReadOnly Property ExistCorpo As Boolean

            Get
                Return NodeNotNull(Me.Corpo)
            End Get

        End Property

        Public ReadOnly Property ExistPiediBlocco As Boolean

            Get
                Return NodeNotNull(Me.PieDiBlocco)
            End Get

        End Property

        ''' <summary>
        ''' Contiene gli elementi della parte di intestazione
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Intestazione As Xml.XmlNode

            Get
                Return Me.XMLItem.SelectSingleNode("intestazione")
            End Get

        End Property

        ''' <summary>
        ''' Contiene gli elementi della parte corpo
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Corpo As Xml.XmlNode

            Get
                Return Me.XMLItem.SelectSingleNode("corpo")
            End Get

        End Property

        Public Property IndiceRiutilizzoCorpo As Integer

            Get
                Return GetAttrFromXML(Me.Corpo, "IndiceRiutilizzo", "-1")
            End Get
            Set(ByVal value As Integer)
                SetAttrToXML(Me.Corpo, "IndiceRiutilizzo", value)
            End Set

        End Property

        ''' <summary>
        ''' Contiene gli elementi della parte piediblocco
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property PieDiBlocco As Xml.XmlNode

            Get
                Return Me.XMLItem.SelectSingleNode("piediblocco")
            End Get

        End Property

        Public Sub RipristinaBox()

            XMLItem.AppendChild(_BoxItem)

        End Sub

        Public ReadOnly Property AltezzaMinimaRichiesta As Decimal

            Get
                Return GetValueFromXML(Me.XMLItem, "MinimumHeightRequired", Decimal.MaxValue)
            End Get

        End Property

    End Class

End Namespace