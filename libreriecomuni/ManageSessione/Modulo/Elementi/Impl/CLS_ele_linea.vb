Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML

Namespace Modulo

    Public Class CLS_ele_linea
        Inherits CLS_elemento_base

        Private _BoxItem As Xml.XmlNode
        Private _Lunghezza As Integer
        Private _Altezza As Integer

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

            _Lunghezza = 0
            _Altezza = 0
            If XMLItem.SelectSingleNode("Dimensioni") Is Nothing Then
                If Not XMLItem.SelectSingleNode("lunghezza") Is Nothing Then
                    _Lunghezza = XMLItem.SelectSingleNode("lunghezza").InnerText
                End If
                If Not XMLItem.SelectSingleNode("altezza") Is Nothing Then
                    _Altezza = XMLItem.SelectSingleNode("altezza").InnerText
                End If
            Else
                If Not XMLItem.SelectSingleNode("Dimensioni/X") Is Nothing Then
                    _Lunghezza = XMLItem.SelectSingleNode("Dimensioni/X").InnerText
                End If
                If Not XMLItem.SelectSingleNode("Dimensioni/Y") Is Nothing Then
                    _Altezza = XMLItem.SelectSingleNode("Dimensioni/Y").InnerText
                End If
            End If

        End Sub

        Public ReadOnly Property Lunghezza As Integer

            Get
                Return _Lunghezza
            End Get

        End Property

        Public ReadOnly Property Altezza As Integer

            Get
                Return _Altezza
            End Get

        End Property

        Public ReadOnly Property DimensionePenna As Decimal

            Get
                Return GetValueFromXML(XMLItem, "penna/dimensione")
            End Get

        End Property

        Public ReadOnly Property ColorePenna As System.Drawing.Color

            Get
                Dim cColor As String
                Dim nRed As Integer
                Dim nGre As Integer
                Dim nBlu As Integer

                If XMLItem.SelectSingleNode("penna/colore") Is Nothing Then
                    cColor = GetValueFromXML(XMLItem, "Color", "black")
                Else
                    cColor = GetValueFromXML(XMLItem, "penna/colore", "black")
                End If

                If (cColor.Contains(",")) Then
                    nRed = Integer.Parse(cColor.Split(",")(0))
                    nGre = Integer.Parse(cColor.Split(",")(1))
                    nBlu = Integer.Parse(cColor.Split(",")(2))
                    Return System.Drawing.Color.FromArgb(nRed, nGre, nBlu)
                Else
                    Return System.Drawing.Color.FromName(cColor)
                End If
            End Get

        End Property

    End Class

End Namespace