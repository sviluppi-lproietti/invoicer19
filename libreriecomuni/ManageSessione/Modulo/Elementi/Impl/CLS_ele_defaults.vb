Imports System.Drawing

Namespace Modulo

    Public Class CLS_ele_defaults
        Inherits CLS_elemento_base

        Public Sub New(ByVal eb As CLS_elemento_base, ByVal posi As CLS_posizioni, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal))
            MyBase.New(eb.XMLItem, posi, upColor, upFont, upPos)

        End Sub

        Public Sub New(ByVal xi As Xml.XmlNode)
            MyBase.New(xi)

        End Sub

    End Class

End Namespace