﻿Imports ISC.LibrerieComuni.ManageSessione.Entity

Public Class Trasformazioni

    Public Shared Function MESessione_TO_MEPlugin1_X_X(ByVal me_in As MessaggiEntity) As String()
        Dim me_out(5) As String

        me_out = New String() {"", "", "", "", "", ""}
        me_out(0) = me_in.ProgressivoPrinc
        me_out(1) = me_in.MassimoPrinc
        me_out(2) = me_in.TipoOperazionePrinc
        me_out(3) = me_in.ProgressivoSecon
        me_out(4) = me_in.MassimoSecon
        me_out(5) = me_in.TipoOperazioneSecon
        Return me_out

    End Function

    Public Shared Function MEPlugin1_X_X_TO_MESessione(ByVal me_in As String()) As MessaggiEntity
        Dim me_out As MessaggiEntity

        me_out = New MessaggiEntity
        me_out.MessaggiDalPlugin = True
        me_out.ProgressivoPrinc = me_in(0)
        me_out.MassimoPrinc = me_in(1)
        me_out.TipoOperazionePrinc = me_in(2)
        me_out.ProgressivoSecon = me_in(3)
        me_out.MassimoSecon = me_in(4)
        me_out.TipoOperazioneSecon = me_in(5)
        Return me_out

    End Function

    Public Shared Function MESessione_TO_MEPlugin2_X_X(ByVal me_in As MessaggiEntity) As PlugINs.MessaggiEntity
        Dim me_out As PlugINs.MessaggiEntity

        me_out = New PlugINs.MessaggiEntity
        me_out.MessaggiDalPlugin = True
        me_out.ProgressivoPrinc = me_in.ProgressivoPrinc
        me_out.MassimoPrinc = me_in.MassimoPrinc
        me_out.TipoOperazionePrinc = me_in.TipoOperazionePrinc
        me_out.ProgressivoSecon = me_in.ProgressivoSecon
        me_out.MassimoSecon = me_in.MassimoSecon
        me_out.TipoOperazioneSecon = me_in.TipoOperazioneSecon
        Return me_out

    End Function

    Public Shared Function MEPlugin2_X_X_TO_MESessione(ByVal me_in As PlugINs.MessaggiEntity) As MessaggiEntity
        Dim me_out As MessaggiEntity

        me_out = New MessaggiEntity
        me_out.MessaggiDalPlugin = True
        me_out.ProgressivoPrinc = me_in.ProgressivoPrinc
        me_out.MassimoPrinc = me_in.MassimoPrinc
        me_out.TipoOperazionePrinc = me_in.TipoOperazionePrinc
        me_out.ProgressivoSecon = me_in.ProgressivoSecon
        me_out.MassimoSecon = me_in.MassimoSecon
        me_out.TipoOperazioneSecon = me_in.TipoOperazioneSecon
        Return me_out

    End Function

End Class
