Imports System.Threading

Public Class FRM_attesa

    Private _Thr2Exec As Thread
    Private _ThrStarted As Boolean
    Private _TmrElapsedStart As DateTime

    WriteOnly Property ThreadToExtcute() As Thread

        Set(ByVal value As Thread)
            _Thr2Exec = value
            _ThrStarted = False
            TMR_attesa.Enabled = True
        End Set

    End Property

    Private Sub TMR_attesa_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TMR_attesa.Tick
        Dim tmr_elapsed As TimeSpan

        If _ThrStarted Then
            If _Thr2Exec.IsAlive Then
                tmr_elapsed = (Now - _TmrElapsedStart)
                LBL_time_elapsed.Text = String.Concat(tmr_elapsed.Minutes.ToString.PadLeft(2, "0"), ":", tmr_elapsed.Seconds.ToString.PadLeft(2, "0"))
                Me.Refresh()
            Else
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        Else
            _Thr2Exec.Start()
            _ThrStarted = True
            _TmrElapsedStart = Now
        End If

    End Sub

End Class