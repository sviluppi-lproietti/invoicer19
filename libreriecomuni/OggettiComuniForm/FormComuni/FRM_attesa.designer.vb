<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_attesa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TMR_attesa = New System.Windows.Forms.Timer(Me.components)
        Me.LBL_descr_dett = New System.Windows.Forms.Label()
        Me.LBL_time_elapsed = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TMR_attesa
        '
        '
        'LBL_descr_dett
        '
        Me.LBL_descr_dett.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBL_descr_dett.Location = New System.Drawing.Point(11, 46)
        Me.LBL_descr_dett.Name = "LBL_descr_dett"
        Me.LBL_descr_dett.Size = New System.Drawing.Size(363, 78)
        Me.LBL_descr_dett.TabIndex = 13
        Me.LBL_descr_dett.Text = "N/D"
        Me.LBL_descr_dett.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_time_elapsed
        '
        Me.LBL_time_elapsed.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBL_time_elapsed.Location = New System.Drawing.Point(273, 9)
        Me.LBL_time_elapsed.Name = "LBL_time_elapsed"
        Me.LBL_time_elapsed.Size = New System.Drawing.Size(101, 13)
        Me.LBL_time_elapsed.TabIndex = 12
        Me.LBL_time_elapsed.Text = "N/D"
        Me.LBL_time_elapsed.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Tempo trascorso"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Attivit�"
        '
        'FRM_attesa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(383, 135)
        Me.ControlBox = False
        Me.Controls.Add(Me.LBL_descr_dett)
        Me.Controls.Add(Me.LBL_time_elapsed)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FRM_attesa"
        Me.Text = "Attesa"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TMR_attesa As System.Windows.Forms.Timer
    Friend WithEvents LBL_descr_dett As System.Windows.Forms.Label
    Friend WithEvents LBL_time_elapsed As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
