Imports System.Windows.Forms

Public Class FRM_azione_poststampa

    Private _data_Action As DS_poststampa_azioni
    Private _bind As BindingSource
    Private _InsertRow As Boolean
    Private _DatiFLD As String

    Public WriteOnly Property DatiFLD() As String

        Set(ByVal value As String)
            _DatiFLD = value
        End Set

    End Property

    Private ReadOnly Property AzioniPostStampa_filename() As String

        Get
            Return String.Concat(_DatiFLD, "azioni_poststampa.xml")
        End Get

    End Property

    Private Sub FRM_azione_poststampa_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        _data_Action = New DS_poststampa_azioni
        If System.IO.File.Exists(AzioniPostStampa_filename) Then
            _data_Action.ReadXml(AzioniPostStampa_filename)
            _data_Action.AcceptChanges()
        End If

        CMB_cod_cond.Items.Add(New ValueDescriptionPair(0, "--- Seleziona condizione ---"))
        CMB_cod_cond.Items.Add(New ValueDescriptionPair(1, "Consorzio di acquisto"))

        CMB_cod_action.Items.Add(New ValueDescriptionPair(0, "--- Seleziona azione ---"))
        CMB_cod_action.Items.Add(New ValueDescriptionPair(1, "Copia file"))
        '        CMB_cod_action.Items.Add(New ValueDescriptionPair(2, "Invia email"))

        _bind = New BindingSource
        _bind.DataSource = _data_Action
        _bind.DataMember = "TBL_PostStampa_Azioni"

        CMB_cod_cond.DataBindings.Clear()
        CMB_cod_cond.DataBindings.Add(New Binding("SelectedIndex", _bind, "PSA_cod_cond", True))

        TXB_val_cond.DataBindings.Clear()
        TXB_val_cond.DataBindings.Add(New Binding("Text", _bind, "PSA_val_cond", True))

        CMB_cod_action.DataBindings.Clear()
        CMB_cod_action.DataBindings.Add(New Binding("SelectedIndex", _bind, "PSA_cod_act", True))

        TXB_val_act.DataBindings.Clear()
        TXB_val_act.DataBindings.Add(New Binding("Text", _bind, "PSA_val_act", True))

        DataGridView1.AutoGenerateColumns = False
        DataGridView1.Columns.Add(COL_cod_condizione())
        DataGridView1.Columns.Add(COL_val_condizione())
        DataGridView1.Columns.Add(COL_cod_azione())
        DataGridView1.Columns.Add(COL_val_azione())
        DataGridView1.DataSource = _bind

    End Sub

    Private Sub DGV_dati_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DataGridView1.CellFormatting
        Dim dr As DataGridViewRow
        Dim nValue As Integer

        dr = DataGridView1.Rows(e.RowIndex)
        If DataGridView1.Columns(e.ColumnIndex).DataPropertyName = "PSA_cod_cond" Then
            nValue = dr.DataBoundItem(DataGridView1.Columns(e.ColumnIndex).DataPropertyName)
            If nValue > 0 Then
                e.Value = CMB_cod_cond.Items(nValue)
            Else
                e.Value = ""
            End If
        End If
        If DataGridView1.Columns(e.ColumnIndex).DataPropertyName = "PSA_cod_act" Then
            nValue = dr.DataBoundItem(DataGridView1.Columns(e.ColumnIndex).DataPropertyName)
            If nValue > 0 Then
                e.Value = CMB_cod_action.Items(nValue)
            Else
                e.Value = ""
            End If
        End If

    End Sub

    Private Function COL_cod_condizione() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Condizione"
            .DataPropertyName = "PSA_cod_cond"
            .Width = 150
        End With
        Return col

    End Function

    Private Function COL_val_condizione() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Valore condizione"
            .DataPropertyName = "PSA_val_cond"
            .Width = 200
        End With
        Return col

    End Function

    Private Function COL_cod_azione() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Azione"
            .DataPropertyName = "PSA_cod_act"
            .Width = 200
        End With
        Return col

    End Function

    Private Function COL_val_azione() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Valore azione"
            .DataPropertyName = "PSA_val_act"
            .Width = 150
        End With
        Return col

    End Function

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        ' bottone per aggiungere
        Dim dr_act As DS_poststampa_azioni.TBL_PostStampa_AzioniRow

        Me.ActiveControl = CMB_cod_cond
        dr_act = CType(_bind.AddNew.Row, DS_poststampa_azioni.TBL_PostStampa_AzioniRow)
        dr_act.PSA_cod_cond = 0
        dr_act.PSA_val_cond = ""
        dr_act.PSA_cod_act = 0
        dr_act.PSA_val_act = ""
        _InsertRow = True

    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating

        If _InsertRow Then
            e.Cancel = MessageBox.Show("L'azione inserita non � stata salvata. Spostandosi su un altro record si perderanno le modifiche effettuate. Procedo comunque?", "Nuova azione", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No
            If Not e.Cancel Then
                DataGridView1.BindingContext(DataGridView1.DataSource, DataGridView1.DataMember).CancelCurrentEdit()
                _InsertRow = False
            End If
        End If

    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Dim bm As BindingManagerBase

        bm = DataGridView1.BindingContext(DataGridView1.DataSource, DataGridView1.DataMember)
        bm.EndCurrentEdit()
        _InsertRow = False
        _data_Action.WriteXml(AzioniPostStampa_filename)

    End Sub

    Private Sub CMB_cod_cond_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CMB_cod_cond.SelectedIndexChanged

        TXB_val_cond.Text = ""

    End Sub

    Private Sub CMB_cod_action_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CMB_cod_action.SelectedIndexChanged

        Button1.Visible = CMB_cod_action.SelectedIndex = 1
        TXB_val_act.Text = ""

    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Dim dr_act As DS_poststampa_azioni.TBL_PostStampa_AzioniRow

        dr_act = CType(CType(DataGridView1.BindingContext(DataGridView1.DataSource, DataGridView1.DataMember).Current, DataRowView).Row, DS_poststampa_azioni.TBL_PostStampa_AzioniRow)
        dr_act.Delete()
        _data_Action.AcceptChanges()
        _data_Action.WriteXml(AzioniPostStampa_filename)

    End Sub
End Class

Public Class ValueDescriptionPair

    Public Value As Object
    Public Description As String

    Public Sub New(ByVal NewValue As Object, ByVal NewDescription As String)
        Value = NewValue
        Description = NewDescription
    End Sub

    Public Overrides Function ToString() As String
        Return Description
    End Function

End Class
