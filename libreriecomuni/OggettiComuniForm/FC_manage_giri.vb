Imports ISC.LibrerieComuni.OggettiComuni

Public Class FC_manage_giri

    Private _dbConn As OleDb.OleDbConnection
    Private _DbGiriConnString As String
    Private _NuovoToponimo As Boolean
    Private _ListaGiri As ArrayList
    Private _data_smt_src As DS_smart_search

    Const no_giro = 999999

    Public Sub New(ByVal cDBGiriConnectionstring As String)

        _dbConn = Nothing
        _DbGiriConnString = cDBGiriConnectionstring

    End Sub

    Public Sub New(ByVal dbConn As OleDb.OleDbConnection)

        _dbConn = dbConn
        _DbGiriConnString = ""

    End Sub

    WriteOnly Property ListaGiri() As ArrayList

        Set(ByVal value As ArrayList)
            Dim adr_gxg As DS_smart_search.TBL_giri_x_girRow()
            Dim dr_qss As DS_smart_search.QRY_smart_searchRow
            Dim dr_gxg As DS_smart_search.TBL_giri_x_girRow
            Dim lda As Layer_DataAccess_old
            Dim nGiro As Integer

            Try
                _ListaGiri = New ArrayList
                _data_smt_src = New DS_smart_search
                lda = creaAccessoDati(_data_smt_src)
                lda.CaricaDataSet("TBL_giri_x_gir")
                For Each nGiro In value
                    adr_gxg = _data_smt_src.TBL_giri_x_gir.Select(String.Concat("GXG_cod_gir_m = ", nGiro))
                    If adr_gxg.Length > 0 Then
                        For Each dr_gxg In adr_gxg
                            If _ListaGiri.IndexOf(dr_gxg.GXG_cod_gir_s) = -1 Then
                                _ListaGiri.Add(dr_gxg.GXG_cod_gir_s)
                            End If
                        Next
                    Else
                        If _ListaGiri.IndexOf(nGiro) = -1 Then
                            _ListaGiri.Add(nGiro)
                        End If
                    End If
                Next
                lda.PulisciTabella("TBL_giri_x_gir")
                lda.CaricaDataSet("QRY_smart_search")
                For Each dr_qss In _data_smt_src.QRY_smart_search.Rows
                    If _ListaGiri.IndexOf(dr_qss.GXC_cod_gir) = -1 Then
                        dr_qss.Delete()
                    End If
                Next
                _data_smt_src.QRY_smart_search.AcceptChanges()
            Catch ex As Exception

            End Try
        End Set

    End Property

    ReadOnly Property NuovoToponimo() As Boolean

        Get
            Return _NuovoToponimo
        End Get

    End Property

    Public Function RicercaGiroPLUGIN(ByVal cCitta As String, ByVal cToponimo As String) As Integer
        Dim dr_sms As DS_smart_search.QRY_smart_searchRow
        Dim dr_tmp As DS_smart_search.TBL_tmpRow
        Dim cFilterExp As String
        Dim nGiroCode As Integer
        Dim nRow As Integer

        nGiroCode = no_giro
        cFilterExp = String.Concat("(CIG_nome = '", NormalizzaForteValore(cCitta), "') AND (TOG_nome = '", NormalizzaForteValore(cToponimo), "')")
        For Each dr_sms In _data_smt_src.QRY_smart_search.Select(cFilterExp)
            nRow = _ListaGiri.IndexOf(dr_sms.GXC_cod_gir)
            If (nRow > -1) And (nRow < nGiroCode) Then
                nGiroCode = nRow
            End If
        Next
        If nGiroCode = no_giro Then
            cFilterExp = String.Concat("(TMP_citta = '", NormalizzaForteValore(cCitta), "') AND (TMP_toponimo = '", NormalizzaForteValore(cToponimo), "')")
            If _data_smt_src.TBL_tmp.Select(cFilterExp).Length = 0 Then
                dr_tmp = _data_smt_src.TBL_tmp.NewRow
                dr_tmp.TMP_citta = NormalizzaValore(cCitta)
                dr_tmp.TMP_toponimo = NormalizzaValore(cToponimo)
                _data_smt_src.TBL_tmp.Rows.Add(dr_tmp)
            End If
        End If
        _data_smt_src.TBL_tmp.AcceptChanges()
        Return nGiroCode

    End Function

    Public Sub InserisciCittaToponimiPLUGIN()
        Dim dr_tog As DS_toponimi.TBL_toponimi_genRow
        Dim dr_top As DS_toponimi.TBL_toponimiRow
        Dim dr_tmp As DS_smart_search.TBL_tmpRow
        Dim dr_cig As DS_citta.TBL_citta_genRow
        Dim dr_cit As DS_citta.TBL_cittaRow
        Dim lda As Layer_DataAccess_old
        Dim cCitta As String
        Dim dv As DataView
        Dim cTopon As String
        Dim nCittaCode As Integer
        Dim nToponCode As Integer
        Dim data_citta As DS_citta
        Dim data_topon As DS_toponimi
        Dim data_cxt As DS_citta_x_toponimi
        Dim dr_cxt As DS_citta_x_toponimi.TBL_citta_x_toponimiRow
        Dim i As Integer

        data_citta = New DS_citta
        data_topon = New DS_toponimi
        lda = creaAccessoDati(data_citta)
        lda.CaricaDataSet("TBL_citta")
        lda.CaricaDataSet("TBL_citta_gen")
        lda = creaAccessoDati(data_topon)
        lda.CaricaDataSet("TBL_toponimi")
        lda.CaricaDataSet("TBL_toponimi_gen")
        dv = New DataView(_data_smt_src.TBL_tmp)
        dv.Sort = "TMP_citta, TMP_toponimo"
        cCitta = ""
        cTopon = ""
        i = 0
        _NuovoToponimo = False
        While i < dv.Count
            dr_tmp = CType(dv(i).Row, DS_smart_search.TBL_tmpRow)

            If cCitta <> dr_tmp.TMP_citta Then
                cCitta = dr_tmp.TMP_citta
                If data_citta.TBL_citta_gen.Select(String.Concat("CIG_nome = '", NormalizzaForteValore(cCitta), "'")).Length = 0 Then
                    lda = creaAccessoDati(data_citta)
                    dr_cit = data_citta.TBL_citta.NewRow
                    dr_cit.CIT_nome = StrConv(cCitta, VbStrConv.ProperCase)
                    data_citta.TBL_citta.Rows.Add(dr_cit)
                    lda.AggiornaDataSet("TBL_citta")
                    nCittaCode = dr_cit.CIT_codice
                    dr_cig = data_citta.TBL_citta_gen.NewRow
                    dr_cig.CIG_nome = cCitta.ToLower
                    dr_cig.CIG_cod_cit = nCittaCode
                    data_citta.TBL_citta_gen.Rows.Add(dr_cig)
                    lda.AggiornaDataSet("TBL_citta_gen")
                Else
                    nCittaCode = data_citta.TBL_citta_gen.Select(String.Concat("CIG_nome = '", NormalizzaForteValore(cCitta), "'"))(0).Item("CIG_cod_cit") * -1
                End If
            End If
            If cTopon <> dr_tmp.TMP_toponimo Then
                cTopon = dr_tmp.TMP_toponimo
                If data_topon.TBL_toponimi_gen.Select(String.Concat("TOG_nome = '", NormalizzaForteValore(cTopon), "'")).Length = 0 Then
                    lda = creaAccessoDati(data_topon)
                    dr_top = data_topon.TBL_toponimi.NewRow
                    dr_top.TOP_nome = StrConv(cTopon, VbStrConv.ProperCase)
                    data_topon.TBL_toponimi.Rows.Add(dr_top)
                    lda.AggiornaDataSet("TBL_toponimi")
                    nToponCode = dr_top.TOP_codice
                    dr_tog = data_topon.TBL_toponimi_gen.NewRow
                    dr_tog.TOG_nome = cTopon.ToLower
                    dr_tog.TOG_cod_top = nToponCode
                    data_topon.TBL_toponimi_gen.Rows.Add(dr_tog)
                    lda.AggiornaDataSet("TBL_toponimi_gen")
                Else
                    nToponCode = data_topon.TBL_toponimi_gen.Select(String.Concat("TOG_nome = '", NormalizzaForteValore(cTopon), "'"))(0).Item("TOG_cod_top") * -1
                End If
            End If
            ' ************************************************************************** '
            ' Le citt� e itoponimi che sono gi� memorizzate nel database hanno un valore ' 
            ' negativo per cui se entrambi sono negative gi� esistono nel DB e gi� esi-  ' 
            ' ste il legame tra i due altrimenti lo inserisco.                           '
            ' ************************************************************************** '
            If nCittaCode < 0 And nToponCode < 0 Then
                dr_tmp.Delete()
                i -= 1
            Else
                dr_tmp.TMP_citta = nCittaCode
                dr_tmp.TMP_toponimo = nToponCode
            End If
            i += 1
        End While
        _data_smt_src.TBL_tmp.AcceptChanges()
        data_citta = Nothing
        data_topon = Nothing

        data_cxt = New DS_citta_x_toponimi
        For Each dr_tmp In _data_smt_src.TBL_tmp
            dr_cxt = data_cxt.TBL_citta_x_toponimi.NewRow
            dr_cxt.CXT_cod_cit = Math.Abs(CInt(dr_tmp.TMP_citta))
            dr_cxt.CXT_cod_top = Math.Abs(CInt(dr_tmp.TMP_toponimo))
            data_cxt.TBL_citta_x_toponimi.Rows.Add(dr_cxt)
            _NuovoToponimo = True
        Next
        lda = creaAccessoDati(data_cxt)
        lda.AggiornaDataSet("TBL_citta_x_toponimi")

    End Sub

    Public Sub InserisciDatiBulk(ByVal cGiro As String, ByVal cCitta As String, ByVal cToponimo As String)
        Dim nToponimoCode As Integer
        Dim nCittaCode As Integer
        Dim nGiroCode As Integer
        Dim nAss_CXT As Integer

        nCittaCode = RicercaCitta(cCitta, True)
        nToponimoCode = RicercaToponimo(cToponimo, True)
        nAss_CXT = RicercaAssCittaxTopo(nCittaCode, nToponimoCode, True)
        If (cGiro > "") And (cCitta > "") And (cToponimo > "") Then
            nGiroCode = RicercaGiroByName(cGiro, True)
            RicercaAssGiroCXT(nGiroCode, nAss_CXT, True)
        End If

    End Sub

    Private Function RicercaGiroByName(ByVal cGiro As String, ByVal lInsertGlob As Boolean) As Integer
        Dim dr_gir As DS_giri.TBL_giriRow
        Dim data_giri As DS_giri
        Dim nRecordCode As Integer
        Dim lda As Layer_DataAccess_old

        cGiro = StrConv(NormalizzaValore(cGiro), VbStrConv.ProperCase)
        data_giri = New DS_giri
        lda = creaAccessoDati(data_giri)
        lda.CaricaDataSet("TBL_giri", New String(,) {{"FEQ,", "GIR_nome", cGiro}})
        If data_giri.TBL_giri.Rows.Count = 0 Then
            If lInsertGlob Then
                dr_gir = data_giri.TBL_giri.NewRow
                dr_gir.GIR_nome = cGiro
                dr_gir.GIR_tipo = "G"
                dr_gir.GIR_sigla = " "
                dr_gir.GIR_descrizione = " "
                data_giri.TBL_giri.Rows.Add(dr_gir)
                lda.AggiornaDataSet("TBL_giri")
                nRecordCode = dr_gir.GIR_codice
            Else
                nRecordCode = -1
            End If
            _NuovoToponimo = True
        Else
            nRecordCode = CType(data_giri.TBL_giri.Rows(0), DS_giri.TBL_giriRow).GIR_codice
        End If
        Return nRecordCode

    End Function

    Private Function RicercaCitta(ByVal cCitta As String, ByVal lInsertGlob As Boolean) As Integer
        Dim dr_cig As DS_citta.TBL_citta_genRow
        Dim dr_cit As DS_citta.TBL_cittaRow
        Dim data_citta As DS_citta
        Dim nRecordCode As Integer
        Dim lda As Layer_DataAccess_old

        cCitta = NormalizzaValore(cCitta)
        data_citta = New DS_citta
        lda = creaAccessoDati(data_citta)
        lda.CaricaDataSet("TBL_citta_gen", New String(,) {{"FEQ,", "CIG_nome", cCitta.ToLower}})
        If data_citta.TBL_citta_gen.Rows.Count = 0 Then
            If lInsertGlob Then
                dr_cit = data_citta.TBL_citta.NewRow
                dr_cit.CIT_nome = StrConv(cCitta, VbStrConv.ProperCase)
                data_citta.TBL_citta.Rows.Add(dr_cit)
                lda.AggiornaDataSet("TBL_citta")
                nRecordCode = dr_cit.CIT_codice
            Else
                nRecordCode = -1
            End If
            dr_cig = data_citta.TBL_citta_gen.NewRow
            dr_cig.CIG_nome = cCitta.ToLower
            dr_cig.CIG_cod_cit = nRecordCode
            data_citta.TBL_citta_gen.Rows.Add(dr_cig)
            lda.AggiornaDataSet("TBL_citta_gen")
            _NuovoToponimo = True
        End If
        nRecordCode = CType(data_citta.TBL_citta_gen.Rows(0), DS_citta.TBL_citta_genRow).CIG_cod_cit
        Return nRecordCode

    End Function

    Private Function RicercaToponimo(ByVal cToponimo As String, ByVal lInsertGlob As Boolean) As Integer
        Dim dr_tog As DS_toponimi.TBL_toponimi_genRow
        Dim dr_top As DS_toponimi.TBL_toponimiRow
        Dim data_toponimi As DS_toponimi
        Dim nRecordCode As Integer
        Dim lda As Layer_DataAccess_old

        cToponimo = NormalizzaValore(cToponimo)
        data_toponimi = New DS_toponimi
        lda = creaAccessoDati(data_toponimi)
        'lda = New Layer_DataAccess(_DbGiriConnString, data_toponimi)
        lda.CaricaDataSet("TBL_toponimi_gen", New String(,) {{"FEQ,", "TOG_nome", cToponimo.ToLower}})
        If data_toponimi.TBL_toponimi_gen.Rows.Count = 0 Then
            If lInsertGlob Then
                dr_top = data_toponimi.TBL_toponimi.NewRow
                dr_top.TOP_nome = StrConv(cToponimo, VbStrConv.ProperCase)
                data_toponimi.TBL_toponimi.Rows.Add(dr_top)
                lda.AggiornaDataSet("TBL_toponimi")
                nRecordCode = dr_top.TOP_codice
            Else
                nRecordCode = -1
            End If
            dr_tog = data_toponimi.TBL_toponimi_gen.NewRow
            dr_tog.TOG_nome = cToponimo.ToLower
            dr_tog.TOG_cod_top = nRecordCode
            data_toponimi.TBL_toponimi_gen.Rows.Add(dr_tog)
            lda.AggiornaDataSet("TBL_toponimi_gen")
            _NuovoToponimo = True
        End If
        nRecordCode = CType(data_toponimi.TBL_toponimi_gen.Rows(0), DS_toponimi.TBL_toponimi_genRow).TOG_cod_top
        Return nRecordCode

    End Function

    Private Function RicercaAssCittaxTopo(ByVal nCittaCode As Integer, ByVal nToponimoCode As Integer, ByVal lInsertGlob As Boolean) As Integer
        Dim dr_cxt As DS_citta_x_toponimi.TBL_citta_x_toponimiRow
        Dim data_cxt As DS_citta_x_toponimi
        Dim lda As Layer_DataAccess_old
        Dim nRecordCode As Integer

        data_cxt = New DS_citta_x_toponimi
        lda = creaAccessoDati(data_cxt)
        'lda = New Layer_DataAccess(_DbGiriConnString, data_cxt)
        lda.CaricaDataSet("TBL_citta_x_toponimi", New String(,) {{"FEQ,A", "CXT_cod_cit", nCittaCode}, {"FEQ,", "CXT_cod_top", nToponimoCode}})
        If data_cxt.TBL_citta_x_toponimi.Rows.Count = 0 Then
            If lInsertGlob Then
                dr_cxt = data_cxt.TBL_citta_x_toponimi.NewRow
                dr_cxt.CXT_cod_cit = nCittaCode
                dr_cxt.CXT_cod_top = nToponimoCode
                data_cxt.TBL_citta_x_toponimi.Rows.Add(dr_cxt)
                lda.AggiornaDataSet("TBL_citta_x_toponimi")
                nRecordCode = dr_cxt.CXT_codice
            Else
                nRecordCode = -1
            End If
        Else
            nRecordCode = CType(data_cxt.TBL_citta_x_toponimi.Rows(0), DS_citta_x_toponimi.TBL_citta_x_toponimiRow).CXT_codice
        End If
        Return nRecordCode

    End Function

    Private Function RicercaAssGiroCXT(ByVal nGiroCode As Integer, ByVal nCXTCode As Integer, ByVal lInsertGlob As Boolean) As Integer
        Dim dr_gxc As DS_giri.TBL_giri_x_cxtRow
        Dim lda As Layer_DataAccess_old
        Dim nRecordCode As Integer
        Dim data_gxc As DS_giri

        data_gxc = New DS_giri
        lda = creaAccessoDati(data_gxc)
        'lda = New Layer_DataAccess(_DbGiriConnString, data_gxc)
        lda.CaricaDataSet("TBL_giri_x_cxt", New String(,) {{"FEQ", "A", "GXC_cod_gir", nGiroCode}, {"FEQ", "", "GXC_cod_cxt", nCXTCode}})
        If data_gxc.TBL_giri_x_cxt.Rows.Count = 0 Then
            If lInsertGlob Then
                dr_gxc = data_gxc.TBL_giri_x_cxt.NewRow
                dr_gxc.GXC_cod_gir = nGiroCode
                dr_gxc.GXC_cod_cxt = nCXTCode
                data_gxc.TBL_giri_x_cxt.Rows.Add(dr_gxc)
                lda.AggiornaDataSet("TBL_giri_x_cxt")
                nRecordCode = dr_gxc.GXC_codice
            Else
                nRecordCode = -1
            End If
        Else
            nRecordCode = CType(data_gxc.TBL_giri_x_cxt.Rows(0), DS_giri.TBL_giri_x_cxtRow).GXC_codice
        End If
        Return nRecordCode

    End Function

    Private Function NormalizzaValore(ByVal cValue As String) As String

        While cValue.IndexOf("  ") > -1
            cValue = cValue.Replace("  ", " ")
        End While
        Return cValue

    End Function

    Private Function NormalizzaForteValore(ByVal cValue As String) As String

        Return NormalizzaValore(cValue).Replace("'", "''").ToLower()

    End Function

    Private Function creaAccessoDati(ByVal DatasetUsed As DataSet) As Layer_DataAccess_old
        Dim lda As Layer_DataAccess_old

        If _DbGiriConnString = "" Then
            lda = New Layer_DataAccess_old(_dbConn, DatasetUsed)
        Else
            lda = New Layer_DataAccess_old(_DbGiriConnString, DatasetUsed)
        End If
        Return lda

    End Function

End Class


