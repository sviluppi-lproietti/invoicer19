Imports system.windows.forms

Public Class FC_menu

    ' ********************************************************************* '
    ' Pulisce i menu dalle voci di un plug in.                              '
    ' ********************************************************************* '
    Public Shared Sub PulisciMenu(ByVal MES_principale As MenuStrip, ByVal TOS_menu As ToolStrip)
        Dim mnuItem As ToolStripMenuItem
        Dim i As Integer
        Dim j As Integer

        If Not MES_principale Is Nothing Then
            For i = MES_principale.Items.Count - 1 To 0 Step -1
                If MES_principale.Items(i).Tag = "FROM_plugin" Then
                    MES_principale.Items.Remove(MES_principale.Items(i))
                Else
                    mnuItem = MES_principale.Items(i)
                    For j = mnuItem.DropDownItems.Count - 1 To 0 Step -1
                        If mnuItem.DropDownItems(j).Tag = "FROM_plugin" Then
                            mnuItem.DropDownItems.Remove(mnuItem.DropDownItems(j))
                        End If
                    Next
                End If
            Next
        End If
        If Not TOS_menu Is Nothing Then
            For i = TOS_menu.Items.Count - 1 To 0 Step -1
                If TOS_menu.Items(i).Tag = "FROM_plugin" Then
                    TOS_menu.Items.RemoveAt(i)
                End If
            Next
        End If

    End Sub

    Public Shared Sub BuildMenu_FromPlugIN(ByVal MES_principale As MenuStrip, ByVal TOS_menu As ToolStrip, ByVal ElencoMenuItem As ArrayList, ByVal evHandler As EventHandler)
        Dim mnuitem As ToolStripMenuItem
        Dim mnusep As ToolStripSeparator
        Dim tsb As ToolStripButton

        ' Installazione dei menu specifici per la sessione.
        For Each aMnuItem As Object() In ElencoMenuItem
            Select Case aMnuItem(0)
                Case Is = "n"
                    mnuitem = aMnuItem(2)
                    mnuitem.Tag = "FROM_plugin"
                    MES_principale.Items.Insert(MES_principale.Items.IndexOfKey(aMnuItem(1)) + 1, mnuitem)
                Case Is = "a"
                    If TypeOf aMnuItem(2) Is ToolStripMenuItem Then
                        mnuitem = CType(aMnuItem(2), ToolStripMenuItem)
                        mnuitem.Tag = "FROM_plugin"
                        If mnuitem.Name.ToLower.StartsWith("tsm_sel_") Then
                            AddHandler mnuitem.Click, evHandler
                        End If
                        CType(MES_principale.Items(aMnuItem(1)), ToolStripMenuItem).DropDownItems.Add(mnuitem)
                    End If
                    If TypeOf aMnuItem(2) Is ToolStripSeparator Then
                        mnusep = CType(aMnuItem(2), ToolStripSeparator)
                        mnusep.Tag = "FROM_plugin"
                        CType(MES_principale.Items(aMnuItem(1)), ToolStripMenuItem).DropDownItems.Add(mnusep)
                    End If

                    If (TypeOf aMnuItem(2) Is ToolStripButton) And Not (TOS_menu Is Nothing) Then
                        tsb = aMnuItem(2)
                        tsb.Tag = "FROM_plugin"
                        TOS_menu.Items.Add(tsb)
                    End If
            End Select
        Next

    End Sub

End Class
