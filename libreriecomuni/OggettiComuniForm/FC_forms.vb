Imports System.Windows.Forms

Public Class FC_forms

    ' ********************************************************************* '
    ' Procedura per l'esecuzione di una procedura incapsulata in un thread. '
    ' ********************************************************************* '
    Public Shared Function EseguiOperazioneInThread(ByVal cMessaggio As String, ByVal thrThread2Start As System.Threading.Thread) As Boolean
        Dim frm_wait As FRM_attesa
        Dim lRtn As Boolean

        frm_wait = FC_forms.BuildForm(New FRM_attesa)
        frm_wait.LBL_descr_dett.Text = cMessaggio
        frm_wait.ThreadToExtcute = thrThread2Start
        lRtn = frm_wait.ShowDialog() = Windows.Forms.DialogResult.OK
        FC_forms.DestroyForm(frm_wait)
        Return lRtn

    End Function

    ' ********************************************************************* '
    ' Procedura per la Creazione di una Form.                               '
    ' ********************************************************************* '
    Public Shared Function BuildForm(ByVal frm As Form) As Form

        frm.StartPosition = FormStartPosition.CenterParent
        Return frm

    End Function

    ' ********************************************************************* '
    ' Procedura per la distruzione di una Form.                             '
    ' ********************************************************************* '
    Public Shared Sub DestroyForm(ByVal frm As Form)

        frm.Dispose()
        frm = Nothing

    End Sub

End Class
