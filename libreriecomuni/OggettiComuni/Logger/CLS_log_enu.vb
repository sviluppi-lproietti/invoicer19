﻿Namespace LogSystem

    Public Enum eLogLevel
        LL_emergencies = 0
        LL_alert = 1
        LL_critical = 2
        LL_error = 3
        LL_warning = 4
        LL_notification = 5
        LL_informational = 6
        LL_debugging = 7
    End Enum

End Namespace