Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.LOGSYSTEM

Namespace LogSystem

    <Serializable()> _
    Public Class CLS_logger
        Inherits MarshalByRefObject

        Private _FileSize As Integer
        Private _LogFLD As String
        Private _LogLevel As eLogLevel
        Private _Prefisso As String
        Private Shared fileSmaphore As Object

        Public Sub New(ByVal cLogFolder As String, ByVal nLogLevel As Integer, ByVal nSize As Integer, ByVal cPrefisso As String)

            _LogFLD = cLogFolder
            _LogLevel = nLogLevel
            _FileSize = nSize
            _Prefisso = cPrefisso
            fileSmaphore = New Object

        End Sub

        Public Sub ScriviMessaggioLog1(ByVal nLogLevel As eLogLevel, ByVal nLivello As Integer, ByVal cFuncSub As String, ByVal cMessage As String, Optional ByVal err As Exception = Nothing)

            If err IsNot Nothing Then
                nLogLevel = eLogLevel.LL_error
            End If
            ScriviMessaggioLog(nLogLevel, nLivello, "", cFuncSub, cMessage, err)

        End Sub

        Public Sub ScriviMessaggioLog(ByVal nLogLevel As eLogLevel, ByVal nLivello As Integer, ByVal cPlugIn As String, ByVal cFuncSub As String, ByVal cMessage As String, Optional ByVal err As Exception = Nothing)

            If nLogLevel <= _LogLevel Then
                Try
                    ScriviRigaLog(BuildLogFileName(False), CostruisciRigaLog(nLivello, cFuncSub, cMessage, cPlugIn, err))
                Catch ex1 As Exception
                    SyncLock fileSmaphore
                        ScriviRigaLog(BuildLogFileName(True), CostruisciRigaLog(0, "ERRORE", "INIZIO - Errore durante la scrittura di un messaggio di Log", Nothing))
                        ScriviRigaLog(BuildLogFileName(True), CostruisciRigaLog(nLivello, cFuncSub, cMessage, cPlugIn, err))
                        ScriviRigaLog(BuildLogFileName(True), CostruisciRigaLog(0, "ERRORE", "FINE - Errore durante la scrittura di un messaggio di Log", Nothing))
                    End SyncLock
                End Try
            End If

        End Sub

        Private Function BuildLogFileName(ByVal lError As Boolean) As String
            Dim fiFilesInfo As System.IO.FileInfo
            Dim aFiles() As String
            Dim cFileName As String
            Dim cPrefix As String
            Dim nFile As Integer
            Dim cLogFLD As String

            cLogFLD = ConcatenaFolderValue(_LogFLD, Now.ToString("yyyyMMdd"))
            If lError Then
                cPrefix = String.Concat(Now.ToString("yyyyMMdd"), "_ErrorWritingLog_")
            Else
                cPrefix = String.Concat(Now.ToString("yyyyMMdd"), "_", _Prefisso, "_")
            End If
            If System.IO.Directory.Exists(cLogFLD) Then
                aFiles = System.IO.Directory.GetFiles(cLogFLD, String.Concat(cPrefix, "*"))
                If aFiles.Length = 0 Then
                    nFile = 1
                Else
                    cFileName = aFiles(aFiles.Length - 1)
                    fiFilesInfo = New System.IO.FileInfo(cFileName)
                    nFile = cFileName.Replace(cLogFLD, "").Replace(cPrefix, "").Replace(".txt", "")
                    If fiFilesInfo.Length > _FileSize Then
                        nFile += 1
                    End If
                End If
            Else
                System.IO.Directory.CreateDirectory(cLogFLD)
                nFile = 1
            End If
            Return String.Concat(cLogFLD, cPrefix, nFile.ToString.PadLeft(4, "0"), ".txt")

        End Function

        Private Function CostruisciRigaLog(ByVal nLivello As eLogLevel, ByVal cFuncSub As String, ByVal cMessage As String, Optional ByVal cPlugIn As String = "", Optional ByVal err As Exception = Nothing) As String
            Dim cLine As String

            If cPlugIn > "" Then
                cLine = String.Concat(Now.ToString("dd/MM/yyyy HH:mm:ss"), " - ", cPlugIn, " - ", cFuncSub.PadRight(25).Substring(0, 25), " - ".PadRight(4 * nLivello + 1))
            Else
                cLine = String.Concat(Now.ToString("dd/MM/yyyy HH:mm:ss"), " - ", cFuncSub.PadRight(25).Substring(0, 25), " - ".PadRight(4 * nLivello + 1))
            End If
            If err Is Nothing Then
                cLine = String.Concat(cLine, cMessage)
            Else
                cLine = String.Concat(cLine, cMessage, " ", err.Message, vbLf, err.StackTrace)
            End If
            Return cLine

        End Function

        Private Sub ScriviRigaLog(ByVal cFileName As String, ByVal cLine As String)
            Dim sw As System.IO.StreamWriter

            sw = New System.IO.StreamWriter(cFileName, True)
            sw.WriteLine(cLine)
            sw.Close()

        End Sub

    End Class

End Namespace