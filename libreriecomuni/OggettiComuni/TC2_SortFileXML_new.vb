Public Class FC_XMLSortFile

    Private _RecordTag As String
    Private _xdDoc As Xml.XmlDocument
    Private _FstRecord As Integer
    Private _LstRecord As Integer

    Public Sub New(ByVal xdDoc As Xml.XmlDocument, ByVal cRecordTag As String)

        _RecordTag = cRecordTag
        _xdDoc = xdDoc
        _FstRecord = 0
        _LstRecord = _xdDoc.SelectSingleNode(cRecordTag).ChildNodes.Count - 1

    End Sub

    Public Sub OrdinaAscendente()

        SortFileIndiceXML(_FstRecord, _LstRecord)

    End Sub

    Private Sub SortFileIndiceXML(ByVal nFst As Integer, ByVal nEnd As Integer)
        Dim nPivot As Integer
        Dim k As Integer

        If nFst < nEnd Then
            nPivot = (nFst + nEnd) / 2
            k = Dividi(nFst, nEnd, nPivot)
            SortFileIndiceXML(nFst, k - 1)
            SortFileIndiceXML(k + 1, nEnd)
        End If

    End Sub

    Private Function Dividi(ByVal nFst As Integer, ByVal nEnd As Integer, ByVal nPivot As Integer) As Integer
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim nPivotPlace As Integer
        Dim i As Integer = nFst
        Dim j As Integer = nEnd

        If nPivot < nEnd Then
            ' sposto il valore del pivot alla fine dell file xml
            SwapItem(nPivot, nEnd)
        End If

        xiWork_1 = _xdDoc.SelectSingleNode(_RecordTag).ChildNodes(nEnd).SelectSingleNode("SORTFIELD")
        nPivotPlace = nFst
        For i = nFst To nEnd - 1
            xiWork_2 = _xdDoc.SelectSingleNode(_RecordTag).ChildNodes(i).SelectSingleNode("SORTFIELD")
            If (xiWork_2.InnerText <= xiWork_1.InnerText) Then
                If i <> nPivotPlace Then
                    SwapItem(nPivotPlace, i)
                End If
                nPivotPlace += 1
            End If
        Next
        If nPivotPlace < nEnd Then
            SwapItem(nPivotPlace, nEnd)
        End If
        Return nPivotPlace

    End Function

    Private Sub SwapItem(ByVal nIdx1 As Integer, ByVal nIdx2 As Integer)
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim xiWork_3 As Xml.XmlNode

        xiWork_1 = _xdDoc.SelectSingleNode(_RecordTag).ChildNodes(nIdx1)
        xiWork_2 = _xdDoc.SelectSingleNode(_RecordTag).ChildNodes(nIdx2)
        _xdDoc.SelectSingleNode(_RecordTag).RemoveChild(xiWork_1)
        _xdDoc.SelectSingleNode(_RecordTag).RemoveChild(xiWork_2)
        xiWork_3 = _xdDoc.SelectSingleNode(_RecordTag).ChildNodes(nIdx1 - 1)
        _xdDoc.SelectSingleNode(_RecordTag).InsertAfter(xiWork_2, xiWork_3)
        xiWork_3 = _xdDoc.SelectSingleNode(_RecordTag).ChildNodes(nIdx2 - 1)
        _xdDoc.SelectSingleNode(_RecordTag).InsertAfter(xiWork_1, xiWork_3)

    End Sub

    Public Sub RimuoviSortField()
        Dim xnItem As Xml.XmlNode

        For Each xnItem In _xdDoc.SelectSingleNode(_RecordTag).ChildNodes
            xnItem.RemoveChild(xnItem.SelectSingleNode("SORTFIELD"))
        Next

    End Sub

End Class
