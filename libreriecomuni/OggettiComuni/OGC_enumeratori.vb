Public Class OGC_enumeratori

    Public Const STC_consegnato = 140

    Enum eTipoApplicazione
        TAP_Invoicer = 1
        TAP_Invoicer_sender = 2
        TAP_Invoicer_core = 3
        TAP_Invoicer_service = 4
        TAP_SorteCert_SRV = 5
        TAP_SorteCert_SMT = 6
    End Enum

    Public Const NotificatoreDefault = 2

    Public Const LowLimitUserValue = 100

    Enum TipoCaricamentoSessione
        TCS_anteprima = 1
        TCS_master = 2
        TCS_dettaglio = 3
        TCS_terminale = 4
    End Enum

    Enum CodiceSao
        SAO_aperta = 10
    End Enum

    Enum ListaDataset
        DAS_data_delivery = 998
        DAS_send_email = 999
    End Enum

    Enum CodiceModuloFix
        MOD_SendEmail = 999
    End Enum

    Enum eTipoSistemaOperativo
        Sconosciuto = -1
        Win_3_1 = 1
        Win_95 = 2
        Win_98 = 3
        Win_ME = 4
        Win_NT_3_51 = 5
        Win_NT_4_0 = 6
        Win_2000 = 7
        Win_XP = 8
        Win_2003 = 9
        Win_2008 = 10    ' Al momento classificato come vista manca 7
        Win_CE = 11
        Win_2008_R2 = 12
        Win_2012 = 13
    End Enum

    Enum TipologiaModulo
        Master = 1
        Slave = 2
    End Enum

    'Enum TipoSessione
    '    Master = 1
    '    Dettaglio = 2
    'End Enum

    Enum etipoBitOS
        bit_sc = 0
        bit_32 = 1
        bit_64 = 2
    End Enum

    Enum eSwitchPrdSvi
        SPS_sviluppo = 1
        SPS_produzio = 2
        SPS_test = 3
    End Enum

    Enum TipoNewDelivery
        NDE_new = 1
        NDE_byfile = 2
    End Enum

    Enum DelTipoFile
        DTF_compatto = 10
        DTF_esteso = 20
    End Enum

    Enum DelTipoCod
        DTC_no_cod = 0
        DTC_16_2 = 1
        DTC_22 = 2
    End Enum

    Enum eDelTipoCod
        DTC_no_cod = 0
        DTC_16_2 = 1
        DTC_22 = 2
    End Enum

    Enum StepCheckFile
        SCF_1 = 1
    End Enum

    Enum TipoExport
        TEX_terminali = 1
        TEX_sorce_smt = 2
    End Enum

    Enum TipoUtenti
        Amministrazione = 1
        Titolare = 11
        ResponsabileSede = 21
        ResponsabileEnte = 22
        UtenteEnte = 31
        Incaricato = 32
    End Enum

    Enum eTipoScarto
        SCA_noscarto = -1
        SCA_ErroreGene = 0
        SCA_NonXmlFile = 1
        SCA_FileErrato = 2
        SCA_Giaprocess = 3
        SCA_BCTroLungo = 4
        SCA_BCVuoto = 5
        SCA_FLDVuota = 6
        SCA_NOAffidame = 7
        SCA_TroppeConsegne = 8
        SCA_NOConsegna = 9
        SCA_StatoConsGiaReg = 10
        SCA_NonDelEvent = 11
        SCA_MailErrata = 12
        SCA_SessioneParametriMancanti = 13
        SCA_CGRLocked = 14
        SCA_TermCompBadFile = 15
        SCA_ErrorDwnlFile = 16
    End Enum

    ' Per compatibilitÓ

   

End Class
