Public Class FC_SortFile

    Public Shared Sub SortFileIndiceXML(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer)
        Dim nPivot As Integer
        Dim k As Integer

        If nFst < nEnd Then
            nPivot = (nFst + nEnd) / 2
            k = Dividi(xiWork, nFst, nEnd, nPivot)
            SortFileIndiceXML(xiWork, nFst, k - 1)
            SortFileIndiceXML(xiWork, k + 1, nEnd)
        End If

    End Sub

    Public Shared Function Dividi(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer, ByVal nPivot As Integer) As Integer
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim nPivotPlace As Integer
        Dim i As Integer = nFst
        Dim j As Integer = nEnd

        If nPivot < nEnd Then
            ' sposto il valore del pivot alla fine dell file xml
            SwapItem(xiWork, nPivot, nEnd)
        End If

        xiWork_1 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nEnd).SelectSingleNode("SORTFIELD")
        nPivotPlace = nFst
        For i = nFst To nEnd - 1
            xiWork_2 = xiWork.SelectSingleNode("FATTURA").ChildNodes(i).SelectSingleNode("SORTFIELD")
            If (xiWork_2.InnerText <= xiWork_1.InnerText) Then
                If i <> nPivotPlace Then
                    SwapItem(xiWork, nPivotPlace, i)
                End If
                nPivotPlace += 1
            End If
        Next
        If nPivotPlace < nEnd Then
            SwapItem(xiWork, nPivotPlace, nEnd)
        End If
        Return nPivotPlace

    End Function

    Public Shared Sub SwapItem(ByVal xiWork As Xml.XmlDocument, ByVal nIdx1 As Integer, ByVal nIdx2 As Integer)
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim xiWork_3 As Xml.XmlNode

        xiWork_1 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx1)
        xiWork_2 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx2)
        xiWork.SelectSingleNode("FATTURA").RemoveChild(xiWork_1)
        xiWork.SelectSingleNode("FATTURA").RemoveChild(xiWork_2)
        xiWork_3 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx1 - 1)
        xiWork.SelectSingleNode("FATTURA").InsertAfter(xiWork_2, xiWork_3)
        xiWork_3 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx2 - 1)
        xiWork.SelectSingleNode("FATTURA").InsertAfter(xiWork_1, xiWork_3)

    End Sub

End Class
