Imports System.Xml
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita

Public Class OGC_utilitaXML

    Public Shared Function NodeNull(ByVal xnTmp As Xml.XmlNode) As Boolean

        Return (xnTmp Is Nothing)

    End Function

    Public Shared Function NodeNotNull(ByVal xnTmp As Xml.XmlNode) As Boolean

        Return Not (xnTmp Is Nothing)

    End Function

    Public Shared Function NodeExist(ByVal xnTmp As Xml.XmlNode, ByVal xnPath As String) As Boolean

        Return Not xnTmp.SelectSingleNode(xnPath) Is Nothing

    End Function

    Public Shared Function AttrExist(ByVal xnTmp As Xml.XmlNode, ByVal xaPath As String) As Boolean

        Return Not xnTmp.Attributes(xaPath) Is Nothing

    End Function

    Public Shared Function IsNodeElement(ByVal xnTmp As Xml.XmlNode) As Boolean

        Return (TypeOf xnTmp Is System.Xml.XmlElement)

    End Function

    Public Shared Function GetAttrFromXML(ByVal xiWork As Xml.XmlNode, ByVal xaPath As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = cDefault
        Try
            If NodeNotNull(xiWork) Then
                If AttrExist(xiWork, xaPath) Then
                    cValue = xiWork.Attributes(xaPath).Value
                End If
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore in GetAttrFromXML:", vbLf, xaPath), ex)
        End Try
        Return cValue

    End Function

    Public Shared Sub SetAttrToXML(ByVal xiWork As Xml.XmlNode, ByVal xaPath As String, ByVal cValue As String)

        Try
            If NodeNotNull(xiWork) Then
                If AttrExist(xiWork, xaPath) Then
                    xiWork.Attributes(xaPath).Value = cValue
                End If
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore in SetAttrFromXML:", vbLf, xaPath), ex)
        End Try

    End Sub

    ' *************************************************************************** '
    ' Recupera il valore presente all'interno di un TAG xml.                      '
    ' *************************************************************************** '
    Public Shared Function GetValueFromXML(ByVal xmlItem As Xml.XmlNode, ByVal xmlPath As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = cDefault
        Try
            If Not xmlItem.SelectSingleNode(xmlPath) Is Nothing Then
                cValue = xmlItem.SelectSingleNode(xmlPath).InnerText
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore in GetValueFromXML.", vbLf, "Percorso da cui si tenta il recupero: ", vbLf, xmlPath), ex)
        End Try
        Return cValue

    End Function

    Public Shared Function GetFirstChildOf(ByVal xnWork As Xml.XmlNode, ByVal xpPath As String) As Xml.XmlNode
        Dim xnRtn As Xml.XmlNode

        xnRtn = Nothing
        If Not (xnWork.SelectSingleNode(xpPath) Is Nothing) Then
            xnRtn = xnWork.SelectSingleNode(xpPath).FirstChild
        End If
        Return xnRtn

    End Function

    Public Shared Sub SetRecordXML(ByVal xdDoc As Xml.XmlDocument, ByVal xnItem As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xnTmp As Xml.XmlNode

        xnTmp = xdDoc.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xnTmp.InnerText = cValue
        xnItem.AppendChild(xnTmp)

    End Sub

    Public Shared Sub AggiungiNodoXml(ByVal xnNodoMaster As Object, ByVal xnNodoSlave As Xml.XmlNode)

        If TypeOf xnNodoMaster Is Xml.XmlDocument Then
            CType(xnNodoMaster, Xml.XmlDocument).AppendChild(xnNodoSlave)
        End If
        If TypeOf xnNodoMaster Is Xml.XmlNode Then
            CType(xnNodoMaster, Xml.XmlNode).AppendChild(xnNodoSlave)
        End If

    End Sub

    Public Shared Sub CreateNodeXML(ByVal xnItem As Xml.XmlNode, ByVal cField As String, ByVal cValue As Object)
        Dim xnTmp As Xml.XmlNode

        xnTmp = xnItem.OwnerDocument.CreateNode(Xml.XmlNodeType.Element, cField, "")
        If cValue IsNot Nothing Then
            xnTmp.InnerText = CType(cValue, String)
        End If
        xnItem.AppendChild(xnTmp)

    End Sub

    Public Shared Sub CreateNodeXML(ByVal xdDoc As Xml.XmlDocument, ByVal cField As String, ByVal cValue As Object)
        Dim xnTmp As Xml.XmlNode

        xnTmp = xdDoc.CreateNode(Xml.XmlNodeType.Element, cField, "")
        If cValue IsNot Nothing Then
            xnTmp.InnerText = CType(cValue, String)
        End If
        xdDoc.AppendChild(xnTmp)

    End Sub

    Public Shared Function CreaNodoXML(ByVal xnItem As Xml.XmlNode, ByVal cField As String, Optional ByVal cValue As Object = Nothing) As Xml.XmlNode
        Dim xnTmp As Xml.XmlNode

        xnTmp = xnItem.OwnerDocument.CreateNode(Xml.XmlNodeType.Element, cField, "")
        If cValue IsNot Nothing Then
            xnTmp.InnerText = CType(cValue, String)
        End If
        Return xnTmp

    End Function

    Public Shared Function CreaNodoXML(ByVal xdDoc As Xml.XmlDocument, ByVal cField As String, Optional ByVal cValue As Object = Nothing) As Xml.XmlNode
        Dim xnTmp As Xml.XmlNode

        xnTmp = xdDoc.CreateNode(Xml.XmlNodeType.Element, cField, "")
        If cValue IsNot Nothing Then
            xnTmp.InnerText = CType(cValue, String)
        End If
        'xdDoc.AppendChild(xnTmp)
        Return xnTmp

    End Function

    Public Shared Sub SafeXMLSaveFile(ByVal xdFile As Xml.XmlDocument, ByVal cFileNameDST As String, ByVal cFolderAppo As String)
        Dim cTmpFileName As String

        Try
            cTmpFileName = ConcatenaFolderFileValue(cFolderAppo, String.Concat("~", Timestamp, ".tmp"))
            xdFile.Save(cTmpFileName)
            System.IO.File.Copy(cTmpFileName, cFileNameDST, True)
            System.IO.File.Delete(cTmpFileName)
        Catch ex As Exception
            Throw New Exception("(SafeXMLSaveFileName) Errore salvando il file XML.", ex)
        End Try

    End Sub

    Public Shared Function GetXmlNodeFromXML(ByVal xnTmp As Xml.XmlNode, ByVal cPath As String) As Xml.XmlNode

        Return xnTmp.SelectSingleNode(cPath)

    End Function

    Public Shared Sub SerializzaXML(ByVal cFileName As String, ByVal oggetto As Object, ByVal tipoOggetto As Type)
        Dim serializer As Serialization.XmlSerializer
        Dim writer As System.IO.Stream

        serializer = New Serialization.XmlSerializer(tipoOggetto)
        writer = Nothing
        Try
            ' Create a FileStream to write with.
            writer = New System.IO.FileStream(cFileName, System.IO.FileMode.Create)
            '// Serialize the object, and close the TextWriter
            serializer.Serialize(writer, oggetto)
        Catch ex As Exception

        Finally
            If (writer IsNot Nothing) Then
                writer.Close()
            End If
        End Try


    End Sub

    Public Shared Function DeserializeXML(ByVal cFileName As String, ByVal tipoOggetto As Type) As Object
        Dim serializer As Serialization.XmlSerializer = New Serialization.XmlSerializer(tipoOggetto)
        Dim oRtn As Object


        '// Create a FileStream to write with.
        Dim reader As System.IO.Stream = New System.IO.FileStream(cFileName, System.IO.FileMode.Open)
        '// Serialize the object, and close the TextWriter
        oRtn = serializer.Deserialize(reader)
        reader.Close()
        Return oRtn

    End Function

End Class
