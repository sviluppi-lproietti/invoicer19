Public Class CLS_reversecomparer
    Implements IComparer

    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements IComparer.Compare
        Dim nRtn As Integer

        nRtn = 0
        If CType(x, String) < CType(y, String) Then
            nRtn = 1
        End If
        If CType(x, String) > CType(y, String) Then
            nRtn = -1
        End If
        Return nRtn

    End Function

End Class