Imports System.Globalization
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori
Imports ISC.LibrerieComuni.OggettiComuni.OGC_enumeratori.eTipoScarto
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita

Public Class CLS_delivery

    Private _CodeOk As Boolean
    Private _FileNameDelEve As String
    Private _IsErrore As Boolean
    Private _IsLoaded As Boolean
    Private _xdDelivery As Xml.XmlDocument
    Private _TipoFile As DelTipoFile
    Private _TipoCodifica As DelTipoCod
    Private _RootFLD As String
    Private _IsDeliveryEvent As Boolean
    Private _CheckSum As String
    Private _CodiceCTR As Integer
    Private _CodiceCGR As Integer
    Private _CodiceScarto As eTipoScarto

#Region "Costruttori"

    Public Sub New(ByVal TipoFileBuild As DelTipoFile, ByVal TipoCodifica As DelTipoCod)

        _TipoFile = TipoFileBuild
        _TipoCodifica = TipoCodifica
        _xdDelivery = New Xml.XmlDocument
        _xdDelivery.AppendChild(_xdDelivery.CreateNode(Xml.XmlNodeType.Element, "delivery", ""))
        SetDeliveryValue("tipofile", _TipoFile)
        SetDeliveryValue("tipocodifica", _TipoCodifica)
        _IsErrore = False
        _IsLoaded = False
        _FileNameDelEve = ""
        _IsDeliveryEvent = True
        _CodiceCTR = -1
        _CodiceCGR = -1
        _CodiceScarto = SCA_noscarto

    End Sub

    Public Sub New(ByVal TipoFileBuild As DelTipoFile, ByVal TipoCodifica As eDelTipoCod)

        _TipoFile = TipoFileBuild
        _TipoCodifica = TipoCodifica
        _xdDelivery = New Xml.XmlDocument
        _xdDelivery.AppendChild(_xdDelivery.CreateNode(Xml.XmlNodeType.Element, "delivery", ""))
        SetDeliveryValue("tipofile", _TipoFile)
        SetDeliveryValue("tipocodifica", _TipoCodifica)
        _IsErrore = False
        _IsLoaded = False
        _FileNameDelEve = ""
        _IsDeliveryEvent = True
        _CodiceCTR = -1
        _CodiceCGR = -1
        _CodiceScarto = SCA_noscarto

    End Sub

    Public Sub New(ByVal cFileName As String, Optional ByVal cRootFLD As String = "")

        Try
            _FileNameDelEve = cFileName
            _RootFLD = cRootFLD
            If cRootFLD = "" Then
                _RootFLD = System.IO.Path.GetDirectoryName(cFileName)
            End If
            _xdDelivery = New Xml.XmlDocument
            _xdDelivery.Load(_FileNameDelEve)

            _IsDeliveryEvent = _xdDelivery.ChildNodes(0).Name = "delivery"
            If CheckFileCorr(StepCheckFile.SCF_1) Then
                _TipoFile = GetDeliveryValue("tipofile", "")
                _TipoCodifica = GetDeliveryValue("tipocodifica", "")
                _IsErrore = Not CheckDeliveryCode(CodiceDelivery)
                'CheckCompatibilita()
                RimuoviCheckSum()
            Else
                _IsErrore = True
            End If
            _IsLoaded = True
            _CodiceCTR = -1
            _CodiceCGR = -1
            _CodiceScarto = SCA_noscarto
        Catch ex As Exception
            _IsLoaded = False
        End Try

    End Sub

    Public Sub New(ByVal TipoCodifica As DelTipoCod, ByVal cCodiceDelivery As String)

        _TipoCodifica = TipoCodifica
        CheckDeliveryCode(cCodiceDelivery)
        _IsDeliveryEvent = True

    End Sub

#End Region

#Region "ProprietÓ Pubbliche"

    Public ReadOnly Property IsErrore() As Boolean

        Get
            Return _IsErrore
        End Get

    End Property

    Public ReadOnly Property IsLoaded() As Boolean

        Get
            Return _IsLoaded
        End Get

    End Property

    Public Property CodiceCTR1() As Integer

        Get
            Return _CodiceCTR
        End Get
        Set(ByVal value As Integer)
            _CodiceCTR = value
        End Set

    End Property

    Public Property CodiceCTRRett() As Integer

        Get
            Return _CodiceCTR
        End Get
        Set(ByVal value As Integer)
            _CodiceCTR = value
            If _CodiceCTR = 140 Then
                _CodiceCTR = 161
            ElseIf _CodiceCTR = 160 Then
                _CodiceCTR = 141
            End If
        End Set

    End Property

    Public Property CodiceCGR() As Integer
        Get
            Return _CodiceCGR
        End Get
        Set(ByVal value As Integer)
            _CodiceCGR = value
        End Set
    End Property

    Public Property CodiceScarto() As eTipoScarto
        Get
            Return _CodiceScarto
        End Get
        Set(ByVal value As eTipoScarto)
            _CodiceScarto = value
        End Set
    End Property
#End Region

#Region "ProprietÓ Private"

#End Region

#Region "Metodi Pubbliche"

#End Region

#Region "Metodi Privati"

    ' ************************************************************************* '
    ' Controlla che il delivery code sia corretto.                              '
    ' ************************************************************************* '
    Private Function CheckDeliveryCode(ByVal cDeliveryCode As String) As Boolean
        Dim nTmp As Integer
        Dim lRtn As Boolean

        If _TipoCodifica = DelTipoCod.DTC_no_cod Then
            lRtn = True
        ElseIf _TipoCodifica = DelTipoCod.DTC_16_2 Then
            lRtn = (cDeliveryCode.Length = 18) And IsNumeric(cDeliveryCode)
            If lRtn Then
                nTmp = cDeliveryCode.Substring(0, 16) Mod 93
                lRtn = (nTmp.ToString.PadLeft(2, "0") = cDeliveryCode.Substring(16, 2))
            End If
        ElseIf _TipoCodifica = DelTipoCod.DTC_22 Then
            lRtn = (cDeliveryCode.Length = 22)
        Else
            lRtn = False
        End If
        _CodeOk = lRtn
        Return lRtn

    End Function

    Private Function CheckFileCorr(ByVal StepCheck As StepCheckFile) As Boolean
        Dim lOk As Boolean

        lOk = False
        If StepCheck = StepCheckFile.SCF_1 Then
            lOk = ExistNode("tipofile") And ExistNode("tipocodifica")
        End If
        Return lOk

    End Function

    '' ************************************************************************ '
    '' Recupera il node dell'item della variabile di configurazione.            '
    '' ************************************************************************ '
    'Private Function GetItemConfigVariable(ByVal cItemToken As String) As Xml.XmlNode
    '    Dim xnItem As Xml.XmlNode

    '    xnItem = _xdDelivery.SelectSingleNode(String.Concat("delivery/", cItemToken))
    '    If xnItem Is Nothing Then
    '        xnItem = _xdDelivery.CreateNode(Xml.XmlNodeType.Element, cItemToken, "")
    '        _xdDelivery.SelectSingleNode("delivery").AppendChild(xnItem)
    '    End If
    '    Return xnItem

    'End Function

    Public Property CheckSum() As String

        Get
            Return _CheckSum
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("checksum", value)
            _CheckSum = value
        End Set

    End Property

    Public Property CodiceDelivery() As String

        Get
            Dim cDeliveryCode As String

            cDeliveryCode = GetDeliveryValue("codice", "")
            If Not CheckDeliveryCode(cDeliveryCode) Then
                cDeliveryCode = ""
            End If
            Return cDeliveryCode
        End Get
        Set(ByVal value As String)
            If CheckDeliveryCode(value) Then
                SetDeliveryValue("codice", value)
                OrarioDelivery = Now
            End If
        End Set

    End Property

    Public ReadOnly Property CodiceDeliveryHashed() As String

        Get
            Return HashString(Me.CodiceDelivery, "md5", "X2")
        End Get

    End Property

    Public ReadOnly Property CodiceDeliveryNoCheckSum() As String

        Get
            Dim cRtn As String

            If _TipoCodifica = DelTipoCod.DTC_no_cod Then
                cRtn = CodiceDelivery
            ElseIf _TipoCodifica = DelTipoCod.DTC_16_2 Then
                cRtn = CodiceDelivery.Substring(0, 16)
            ElseIf _TipoCodifica = DelTipoCod.DTC_22 Then
                cRtn = CodiceDelivery
            Else
                cRtn = ""
            End If
            Return cRtn
        End Get

    End Property

    Public Property Coordinate() As String

        Get
            Return GetDeliveryValue("coordinate", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("coordinate", value)
        End Set

    End Property

    Public Property DettaglioStato() As Integer

        Get
            Dim nDettStato As Integer

            nDettStato = GetDeliveryValue("dettagliostato", "")
            If nDettStato < 10000 Then
                nDettStato = Stato * 1000 + 1
            ElseIf nDettStato = 13001 Then
                nDettStato = 130001
            ElseIf nDettStato = 14001 Then
                nDettStato = 140001
            End If
            Return nDettStato
        End Get
        Set(ByVal value As Integer)
            SetDeliveryValue("dettagliostato", value)
        End Set

    End Property

    Public Property NoteConsegna() As String

        Get
            Return GetDeliveryValue("note", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("note", value)
        End Set

    End Property

    Public Property Notificatore() As Integer

        Get
            Return GetDeliveryValue("notificatore", "")
        End Get
        Set(ByVal value As Integer)
            SetDeliveryValue("notificatore", value)
        End Set

    End Property

    Public Property NotifyTo() As String

        Get
            Return GetDeliveryValue("notificaa", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("notificaa", value.ToString)
        End Set

    End Property

    Public Property OrarioDelivery() As DateTime

        Get
            Dim dDate As DateTime

            Try
                Dim cTmp As String

                cTmp = GetDeliveryValue("orariodelivery", "")
                If cTmp.Contains(".") Then
                    cTmp = cTmp.Replace(".", ":")
                End If
                dDate = DateTime.ParseExact(cTmp, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
            Catch ex As Exception
                Throw ex
            End Try
            Return dDate
        End Get
        Set(ByVal value As DateTime)
            'If cTmp.Contains(".") Then
            '    cTmp = cTmp.Replace(".", ":")
            'End If
            SetDeliveryValue("orariodelivery", value.ToString("dd/MM/yyyy HH:mm:ss"))
        End Set

    End Property

    Public Property Stato() As Integer

        Get
            Return GetDeliveryValue("stato", "")
        End Get
        Set(ByVal value As Integer)
            SetDeliveryValue("stato", value)
        End Set

    End Property

    Public ReadOnly Property CodeOK() As Boolean

        Get
            Return _CodeOk
        End Get

    End Property

    Public ReadOnly Property FileEsitoExist1(ByVal cFolder As String) As Boolean

        Get
            'BuildFileName(cFolder, False)
            Return System.IO.File.Exists(_FileNameDelEve)
        End Get

    End Property

    Public ReadOnly Property aaaaa(ByVal cRootFLD As String) As String

        Get
            Return String.Concat(cRootFLD, CodiceDelivery)
        End Get

    End Property

    'Public ReadOnly Property Filename() As String

    '    Get
    '        Return System.IO.Path.GetFileName(_FileNameDelEve)   ' String.Concat(CodiceDelivery, "\", Stato, "_", CodiceDelivery, ".xml")
    '    End Get

    'End Property

    Public ReadOnly Property FileValido() As Boolean

        Get
            Return CheckFileValido()
        End Get

    End Property

    'Public Property ProvinciaRecapito() As String

    ''    _xdDelivery.Save(String.Concat(cFolder, System.IO.Path.GetFileName(_FileNameDelEve)))
    ''    '  System.IO.File.Delete(_FileDelEveName)

    'End Property

    Public ReadOnly Property DirectoryEsito(ByVal cRootFLD As String) As String

        Get
            Return String.Concat(cRootFLD, CodiceDelivery)
        End Get

    End Property

    'Public Sub SaveDelivery(ByVal cFolder As String)

    '    Get
    '        Return CheckFileValido()
    '    End Get

    'End Property

#End Region

#Region "ProprietÓ Private"

#End Region

#Region "Metodi Pubblici"

    Public Sub MoveToFolder(ByVal cFolder As String)
        Dim cOldFileName As String

        Try
            cOldFileName = _FileNameDelEve
            Me.SaveDelivery(cFolder)
            System.IO.File.Delete(cOldFileName)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub SaveDelivery(ByVal cFolder As String, Optional ByVal lCheckComp As Boolean = False)

        If lCheckComp Then
            SetDeliveryValue("writedate", Now.ToString)
        End If
        'BuildFileName(cFolder)
        Me.CheckSum = HashString(_xdDelivery.OuterXml, "md5", "X2")
        If _CodiceCTR = -1 Then
            If cFolder <> "*" Then
                _FileNameDelEve = FileNameUnivoco(String.Concat(ConcatenaFolderValue(cFolder, Me.CodiceDeliveryHashed), Me.Stato.ToString.PadLeft(3, "0"), "_", Me.CodiceDeliveryHashed, ".xml"))
            End If
        Else
            _FileNameDelEve = String.Concat(ConcatenaFolderValue(cFolder, Me.CodiceDeliveryHashed), HashString(_CodiceCTR, "md5", "X2"), ".xml")
        End If
        If Not System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(_FileNameDelEve)) Then
            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(_FileNameDelEve))
        End If
        _xdDelivery.Save(_FileNameDelEve)
        RimuoviCheckSum()

    End Sub

#End Region

#Region "Metodi Privati"

    ' ************************************************************************* '
    ' Controllo che il file sia valido.                                         '
    ' ************************************************************************* '
    Private Function CheckFileValido() As Boolean
        Dim lOkFile As Boolean

        lOkFile = ExistNode("coordinate") And ExistNode("note") And ExistNodeValorized("codice") And _
                  ExistNodeValorized("stato") And ExistNodeValorized("dettagliostato") And ExistNodeValorized("notificatore") And _
                  ExistNodeValorized("orario") And ExistNodeValorized("writedate") And ExistNodeValorized("checksum")
        Return lOkFile

    End Function

#End Region

#Region "Procedure di utilitÓ"

    Private Sub RimuoviCheckSum()
        Dim xnItem As Xml.XmlNode

        _CheckSum = GetDeliveryValue("checksum", "")
        xnItem = _xdDelivery.SelectSingleNode("delivery/checksum")
        _xdDelivery.SelectSingleNode("delivery").RemoveChild(xnItem)

    End Sub

    ' ************************************************************************ '
    ' Recupera il node dell'item della variabile di configurazione.            '
    ' ************************************************************************ '
    Private Function GetItemConfigVariable(ByVal cItemToken As String) As Xml.XmlNode
        Dim xnItem As Xml.XmlNode

        xnItem = _xdDelivery.SelectSingleNode(String.Concat("delivery/", cItemToken))
        If xnItem Is Nothing Then
            xnItem = _xdDelivery.CreateNode(Xml.XmlNodeType.Element, cItemToken, "")
            _xdDelivery.SelectSingleNode("delivery").AppendChild(xnItem)
        End If
        Return xnItem

    End Function

    ' ************************************************************************ '
    ' Imposta il valore della variabli di configurazione STRINGA nel file di   ' 
    ' gestione.                                                                '
    ' ************************************************************************ '
    Private Sub SetDeliveryValue(ByVal cItemToken As String, ByVal cItemValue As String)
        Dim xnItem As Xml.XmlNode

        xnItem = _xdDelivery.SelectSingleNode(String.Concat("delivery/", cItemToken))
        If xnItem Is Nothing Then
            xnItem = _xdDelivery.CreateNode(Xml.XmlNodeType.Element, cItemToken, "")
            _xdDelivery.SelectSingleNode("delivery").AppendChild(xnItem)
        Else
            xnItem = GetItemConfigVariable(cItemToken)
        End If
        xnItem.InnerText = cItemValue

    End Sub

    Private Function ExistNode(ByVal cItemToken As String) As Boolean

        Return _xdDelivery.SelectSingleNode(String.Concat("delivery/", cItemToken)) IsNot Nothing

    End Function

    Private Function ExistNodeValorized(ByVal cItemToken As String) As Boolean
        Dim lOk As Boolean

        lOk = ExistNode(cItemToken)
        If lOk Then
            lOk = GetDeliveryValue(cItemToken, "") > ""
        End If
        Return lOk

    End Function

    ' ************************************************************************* '
    ' Recupera il valore della variabli di configurazione dal file di gestione. '
    ' ************************************************************************* '
    Private Function GetDeliveryValue(ByVal cItemToken As String, ByVal cDefaultValue As String) As String
        Dim cRtn As String

        cRtn = cDefaultValue
        cItemToken = String.Concat("delivery/", cItemToken)
        If _xdDelivery.SelectSingleNode(cItemToken) IsNot Nothing Then
            cRtn = _xdDelivery.SelectSingleNode(cItemToken).InnerText
        End If
        Return cRtn

    End Function

#End Region

    Public ReadOnly Property FilenameFP() As String

        Get
            Return _FileNameDelEve
        End Get

    End Property

    Public Property Nominativo() As String

        Get
            Return GetDeliveryValue("nominativo", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("nominativo", value.ToString)
        End Set

    End Property

    Public Property DestinatarioElettronico() As String

        Get
            Return GetDeliveryValue("destinatarioelettronico", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("destinatarioelettronico", value.ToString)
        End Set

    End Property

    Public Property ParteDocumento() As Integer

        Get
            Return GetDeliveryValue("partedocumento", "1")
        End Get
        Set(ByVal value As Integer)
            SetDeliveryValue("partedocumento", value.ToString)
        End Set

    End Property

    Public Property NumDocumento() As String

        Get
            Return GetDeliveryValue("numdocumento", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("numdocumento", value.ToString)
        End Set

    End Property

    Public Property IndirizzoRecapito() As String

        Get
            Return GetDeliveryValue("indirizzorecapito", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("indirizzorecapito", value.ToString)
        End Set

    End Property

    Public Property CittaRecapito() As String

        Get
            Return GetDeliveryValue("cittarecapito", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("cittarecapito", value.ToString)
        End Set

    End Property

    Public Property CAPRecapito() As String

        Get
            Return GetDeliveryValue("caprecapito", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("caprecapito", value.ToString)
        End Set


    End Property

    Public Property ProvinciaRecapito() As String

        Get
            Return GetDeliveryValue("provinciarecapito", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("provinciarecapito", value.ToString)
        End Set

    End Property

    Public Property codfis_piva() As String

        Get
            Return GetDeliveryValue("codfis_piva", "")
        End Get
        Set(ByVal value As String)
            SetDeliveryValue("codfis_piva", value.ToString)
        End Set

    End Property

    Public ReadOnly Property IsDeliveryEvent() As Boolean

        Get
            Return _IsDeliveryEvent
        End Get

    End Property

    Public Sub DeleteEvento()

        System.IO.File.Delete(_FileNameDelEve)

    End Sub

    Public ReadOnly Property EventoConsegna As Boolean

        Get
            Return Array.IndexOf(New Integer() {30, 40, 60}, Me.Stato Mod 100) > -1
        End Get

    End Property

    Public ReadOnly Property EventoAffidemento As Boolean

        Get
            Return Array.IndexOf(New Integer() {10}, Me.Stato Mod 100) > -1
        End Get

    End Property


End Class
