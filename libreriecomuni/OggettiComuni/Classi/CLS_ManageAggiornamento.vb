Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem

Public Class CLS_ManageAggiornamento

    Private _LockedFolder As ArrayList
    Private _FolderExist As ArrayList
    Private _FilesExist As ArrayList
    Private _FolderDwUpload As ArrayList
    Private _FilesDwUpload As ArrayList
    Private _FolderDelete As ArrayList
    Private _FilesDelete As ArrayList
    Private _FolderDstPrefix As String
    Private _FolderSrcPrefix As String
    Private _FolderAppPrefix As String
    Private _FolderOLDPrefix As String
    Private _FolderToScan As String()
    Private _FolderApplicaz As String()
    Private _FTPPassword As String
    Private _FTPUsername As String
    Private _log As CLS_logger
    Private _MainTag As String()
    Private _oftp As CLS_ftp
    Private _xdFileGuida As Xml.XmlDocument
    Private _GeneralBuilding As Boolean
    Private _UpdateSoftware As Boolean
    Private _ApplicazioniAggiornate As String
    Private _AbortDownLoad As Boolean

    Enum eTipoDownload
        DatiApplicazioni
        FileGuida
        Tutti
    End Enum

    Public ReadOnly Property AbortDownLoad() As Boolean

        Get
            Return _AbortDownLoad
        End Get

    End Property

    Public ReadOnly Property ApplicazioniAggiornate() As String

        Get
            Return _ApplicazioniAggiornate
        End Get

    End Property

    Public ReadOnly Property UpdateSoftware() As Boolean

        Get
            Return _UpdateSoftware
        End Get

    End Property

    Public WriteOnly Property FolderApplicaz() As String()

        Set(ByVal value As String())
            _FolderApplicaz = value
        End Set

    End Property

    Public Property FolderToScan() As String()

        Get
            Return _FolderToScan
        End Get
        Set(ByVal value As String())
            _FolderToScan = value
        End Set

    End Property

    Public WriteOnly Property MainTag() As String()

        Set(ByVal value As String())
            _MainTag = value
        End Set

    End Property

    Public WriteOnly Property log() As CLS_logger

        Set(ByVal value As CLS_logger)
            _log = value
        End Set

    End Property

    Public WriteOnly Property FolderSrcPrefix() As String

        Set(ByVal value As String)
            _FolderSrcPrefix = value
        End Set

    End Property

    Public WriteOnly Property FolderAppPrefix() As String

        Set(ByVal value As String)
            _FolderAppPrefix = value
        End Set

    End Property

    Public WriteOnly Property FolderDstPrefix() As String

        Set(ByVal value As String)
            _FolderDstPrefix = value
        End Set

    End Property

    Public WriteOnly Property FolderOLDPrefix As String

        Set(ByVal value As String)
            _FolderOLDPrefix = value
        End Set

    End Property

    Public WriteOnly Property FTPUsername() As String
        Set(ByVal value As String)
            _FTPUsername = value
        End Set
    End Property

    Public WriteOnly Property FTPPassword() As String

        Set(ByVal value As String)
            _FTPPassword = value
        End Set

    End Property

    Public ReadOnly Property NumeroElementiAggiornare() As Integer

        Get
            Return (_FolderDwUpload.Count + _FilesDwUpload.Count + _FolderDelete.Count + _FilesDelete.Count)
        End Get

    End Property

    Public Sub New()

        _UpdateSoftware = False

    End Sub

    Public Sub ScaricaDaFTP(Optional ByVal TipoDownload As eTipoDownload = eTipoDownload.DatiApplicazioni)
        Dim cFolder As String

        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword)
        _AbortDownLoad = False
        For Each cFolder In _FolderToScan
            If TipoDownload = eTipoDownload.DatiApplicazioni Then
                ScaricaDaFTPLoop(ConcatenaFolderValueWeb(_FolderSrcPrefix, cFolder), ConcatenaFolderValue(_FolderDstPrefix, cFolder))
            ElseIf TipoDownload = eTipoDownload.FileGuida Then
                ScaricaFileDaFTP(ConcatenaFolderFileValueWeb(_FolderSrcPrefix, String.Concat(cFolder, ".xml")), ConcatenaFolderFileValue(_FolderDstPrefix, String.Concat(cFolder, ".xml")))
            End If
        Next
        _oftp = Nothing

    End Sub

    Public Sub CaricaSuFTP(Optional ByVal TipoDownload As eTipoDownload = eTipoDownload.DatiApplicazioni)
        Dim cFolder As String

        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword)
        _AbortDownLoad = False
        For Each cFolder In _FolderToScan
            If (TipoDownload = eTipoDownload.DatiApplicazioni) Or (TipoDownload = eTipoDownload.Tutti) Then
                CaricaSuFTPLoop(ConcatenaFolderValue(_FolderSrcPrefix, cFolder), ConcatenaFolderValueWeb(_FolderDstPrefix, cFolder))
            End If
            If (TipoDownload = eTipoDownload.FileGuida) Or (TipoDownload = eTipoDownload.Tutti) Then
                CaricaFileSuFTP(ConcatenaFolderFileValue(_FolderSrcPrefix, String.Concat(cFolder, ".xml")), ConcatenaFolderFileValueWeb(_FolderDstPrefix, String.Concat(cFolder, ".xml")))
            End If
        Next
        _oftp = Nothing

    End Sub

    Public Sub CopiaTraCartelle()
        Dim cFolder As String

        For Each cFolder In _FolderToScan
            CopiaTraCartelleLoop(ConcatenaFolderValue(_FolderSrcPrefix, cFolder), ConcatenaFolderValue(_FolderDstPrefix, cFolder))
        Next

    End Sub

    Public Sub BuildFileGuida(ByVal lGeneralBuilding As Boolean)
        Dim i As Integer

        _GeneralBuilding = lGeneralBuilding
        _LockedFolder = New ArrayList
        If Not _GeneralBuilding Then
            If _MainTag(0) = "SorteCert_SMT" Then
                _LockedFolder.Add(ConcatenaFolderValue(_FolderSrcPrefix, "Archivi"))
                _LockedFolder.Add(ConcatenaFolderValue(_FolderSrcPrefix, "Sessioni"))
                ' _LockedFolder.Add(ConcatenaFolderValue(_FolderSrcPrefix, "ArchiviSMT"))
                _LockedFolder.Add(ConcatenaFolderValue(_FolderSrcPrefix, "Exchange"))
                _LockedFolder.Add(ConcatenaFolderValue(_FolderSrcPrefix, "Logs"))
                _LockedFolder.Add(ConcatenaFolderValue(_FolderSrcPrefix, "Work"))
                _LockedFolder.Add(ConcatenaFolderValue(_FolderSrcPrefix, "Terminali"))
            End If
        End If
        For i = 0 To _FolderToScan.Length - 1
            _xdFileGuida = New Xml.XmlDocument
            _xdFileGuida.AppendChild(_xdFileGuida.CreateNode(Xml.XmlNodeType.Element, _MainTag(i), ""))
            If _GeneralBuilding Then
                BuildFileGuidaLoop(ConcatenaFolderValue(_FolderSrcPrefix, _FolderToScan(i)), ConcatenaFolderValue(_FolderSrcPrefix, _FolderToScan(i)), _MainTag(i))
            Else
                BuildFileGuidaLoop(StandardFolderValue(_FolderSrcPrefix), StandardFolderValue(_FolderSrcPrefix), _MainTag(i))
            End If
            _xdFileGuida.Save(String.Concat(_FolderSrcPrefix, _FolderApplicaz(i), ".xml"))
        Next

    End Sub

    Public Sub CaricaSuFTP_old()
        Dim cFolder As String
        Dim cFile As String

        _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaSuFTP", "Aggiornamento su sito ftp.")
        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword, True)
        _oftp.CreaFolder("FTP://95.110.155.97/SVI/NEWOUT/")


        For Each cFile In _FilesDelete
            cFile = String.Concat(_FolderDstPrefix, cFile)
            _oftp.DeleteFileFTP(cFile)
            If Not _oftp.IsErrore Then
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaSuFTP", String.Concat("Rimozione del file (terminata correttamente): ", cFile))
            Else
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaSuFTP", String.Concat("Rimozione del file (terminata NON correttamente): ", cFile, vbLf, "ERRORE: ", _oftp.Errore))
            End If
        Next

        For Each cFolder In _FolderDelete
            cFolder = String.Concat(_FolderDstPrefix, cFolder)
            _oftp.DeleteFolderFTP(cFolder)
            If Not _oftp.IsErrore Then
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaSuFTP", String.Concat("Rimozione della directory (terminata correttamente): ", cFolder))
            Else
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaSuFTP", String.Concat("Rimozione della directory (terminata NON correttamente): ", cFolder, vbLf, "ERRORE: ", _oftp.Errore))
            End If
        Next

        For Each cFolder In _FolderDwUpload
            cFolder = String.Concat(_FolderDstPrefix, cFolder, "/").Replace("\", "/")
            _oftp.CreaFolder(cFolder)
            If _oftp.IsErrore Then

            End If
        Next
        For Each cFile In _FilesDwUpload
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaSuFTP", String.Concat("File locale.: ", String.Concat(_FolderSrcPrefix, cFile)))
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaSuFTP", String.Concat("File remoto.: ", String.Concat(_FolderDstPrefix, cFile)))
            _oftp.UploadFile(String.Concat(_FolderSrcPrefix, cFile), String.Concat(_FolderDstPrefix, cFile))
            If Not _oftp.IsErrore Then
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaSuFTP", String.Concat("Aggiornamento del file (terminato correttamente): ", cFile))
            Else
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "CaricaSuFTP", String.Concat("Aggiornamento del file (terminata NON correttamente): ", cFile, " ERRORE: ", _oftp.Errore))
            End If
        Next
        _oftp = Nothing

    End Sub

    Public Sub RecuperaAggiornamenti()
        Dim cSrcFile As String
        Dim cFolder As String
        Dim cFile As String

        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword, True)
        For Each cFile In _FilesDwUpload
            cSrcFile = String.Concat(_FolderSrcPrefix, _MainTag(0), "/", cFile).Replace("\", "/")
            cFile = String.Concat(_FolderDstPrefix, _MainTag(0), "\", cFile)
            cFolder = System.IO.Path.GetDirectoryName(cFile)
            If Not System.IO.Directory.Exists(cFolder) Then
                System.IO.Directory.CreateDirectory(cFolder)
            End If
            _oftp.DownloadFile(cSrcFile, cFile)
            If Not _oftp.IsErrore Then
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "EseguiOperazione", String.Concat("Aggiornamento del file (terminato correttamente): ", cFile))
            Else
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "EseguiOperazione", String.Concat("Aggiornamento del file (terminata NON correttamente): ", cFile, " ERRORE : ", _oftp.Errore))
            End If
        Next
        _oftp = Nothing

    End Sub

    Public Sub AggiornaApplicazione()
        Dim cSoftwareUpdater As String
        Dim cFolder As String
        Dim cFile As String

        _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "EseguiOperazione", "Aggiornamento su sito ftp.")
        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword, True)
        For Each cFile In _FilesDelete
            Try
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "EseguiOperazione", String.Concat("file da rimuovere:", cFile))
                cFile = String.Concat(_FolderAppPrefix, cFile)
                System.IO.File.Delete(cFile)
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "EseguiOperazione", "File rimosso correttamente.")
            Catch ex As Exception
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "EseguiOperazione", "File NON rimosso.")
            End Try
        Next

        For Each cFolder In _FolderDelete
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "EseguiOperazione", String.Concat("Folder da rimuovere:", cFolder))
            cFolder = String.Concat(_FolderAppPrefix, cFolder)
            System.IO.Directory.Delete(cFolder, True)
        Next

        For Each cFolder In _FolderDwUpload
            cFolder = String.Concat(_FolderAppPrefix, cFolder, "/").Replace("\", "/")
            System.IO.Directory.CreateDirectory(cFolder)
        Next
        If System.IO.Directory.Exists(String.Concat(_FolderSrcPrefix, _MainTag(0))) Then
            For Each cFolder In System.IO.Directory.GetDirectories(String.Concat(_FolderSrcPrefix, _MainTag(0)))
                AggiornaApplicazioneLoop(cFolder, cFolder.Replace(ConcatenaFolderValue(_FolderSrcPrefix, _MainTag(0)), _FolderAppPrefix))
            Next
        End If
        cSoftwareUpdater = String.Concat(ConcatenaFolderValue(_FolderSrcPrefix, _MainTag(0)), "SorteCert_SMT_upd.exe")
        If System.IO.File.Exists(cSoftwareUpdater) Then
            System.IO.File.Copy(cSoftwareUpdater, cSoftwareUpdater.Replace(ConcatenaFolderValue(_FolderSrcPrefix, _MainTag(0)), _FolderAppPrefix), True)
            System.IO.File.Delete(cSoftwareUpdater)
        End If
        If System.IO.Directory.Exists(ConcatenaFolderValue(_FolderSrcPrefix, _MainTag(0))) Then
            _UpdateSoftware = System.IO.Directory.GetFiles(ConcatenaFolderValue(_FolderSrcPrefix, _MainTag(0))).Length > 0
        Else
            _UpdateSoftware = False
        End If

    End Sub

    Private Sub AggiornaApplicazioneLoop(ByVal cSrcFolder As String, ByVal cDstFolder As String)
        Dim cFolder As String
        Dim cFile As String

        If Not System.IO.Directory.Exists(cDstFolder) Then
            System.IO.Directory.CreateDirectory(cDstFolder)
        End If
        For Each cFolder In System.IO.Directory.GetDirectories(cSrcFolder)
            AggiornaApplicazioneLoop(cFolder, cFolder.Replace(cSrcFolder, cDstFolder))
        Next
        For Each cFile In System.IO.Directory.GetFiles(cSrcFolder)
            System.IO.File.Copy(cFile, cFile.Replace(cSrcFolder, cDstFolder), True)
        Next

    End Sub

    Public Sub ScaricaFileGuida()
        Dim cSrcFile As String
        Dim cDstFile As String

        _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "EseguiOperazione", "Aggiornamento su sito ftp.")
        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword, True)
        cSrcFile = String.Concat(_FolderSrcPrefix, _FolderToScan(0), ".xml")
        cDstFile = String.Concat(_FolderDstPrefix, _FolderToScan(0), ".xml")
        _oftp.DownloadFile(cSrcFile, cDstFile)
        _oftp = Nothing

    End Sub

    Public Sub ConfrontaFileGuida()
        Dim xdFileMaster As Xml.XmlDocument
        Dim xdFileSlave As Xml.XmlDocument
        Dim cLocalFile As String
        Dim cRemoteFile As String
        Dim nFile As Integer
        Dim i As Integer

        _FolderExist = New ArrayList
        _FilesExist = New ArrayList
        _FolderDwUpload = New ArrayList
        _FilesDwUpload = New ArrayList
        _FolderDelete = New ArrayList
        _FilesDelete = New ArrayList
        nFile = 0
        _ApplicazioniAggiornate = ""
        For i = 0 To _FolderToScan.Length - 1
            cLocalFile = String.Concat(_FolderSrcPrefix, _FolderToScan(i), ".xml")
            cRemoteFile = String.Concat(_FolderDstPrefix, _FolderToScan(i), ".xml")
            ' **************************************************************** '
            ' Caricamento del file guida principale (quello al quale ci dovrem '
            ' mo adeguare                                                      ' 
            ' **************************************************************** '
            If System.IO.File.Exists(cLocalFile) Then
                xdFileMaster = New Xml.XmlDocument
                xdFileMaster.Load(cLocalFile)
            Else
                xdFileMaster = Nothing
            End If
            If System.IO.File.Exists(cRemoteFile) Then
                xdFileSlave = New Xml.XmlDocument
                xdFileSlave.Load(cRemoteFile)
            Else
                xdFileSlave = Nothing
            End If
            ConfrontaFileGuidaLoop(_FolderToScan(i), _MainTag(i), xdFileMaster, xdFileSlave, True)
            ConfrontaFileGuidaLoop(_FolderToScan(i), _MainTag(i), xdFileSlave, xdFileMaster, False)
            If nFile < Me.NumeroElementiAggiornare And _GeneralBuilding Then
                _ApplicazioniAggiornate = String.Concat(_ApplicazioniAggiornate, ";", _FolderToScan(i))
                _FilesDwUpload.Add(System.IO.Path.GetFileName(cLocalFile))
                nFile = Me.NumeroElementiAggiornare
            End If
        Next
        _FolderDelete.Sort(New CLS_reversecomparer)
        _FolderDwUpload.Sort()

    End Sub

    Public Sub PulisciFolderToScanFTP()
        Dim cFolder As String

        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword)
        For Each cFolder In _FolderToScan
            PulisciFolderFTP(ConcatenaFolderValueWeb(_FolderSrcPrefix, cFolder), False)
        Next
        _oftp = Nothing

    End Sub

    Private Sub ScaricaFileDaFTP(ByVal cSrcFil As String, ByVal cDstFil As String)

        Try
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaFileDaFTP", String.Concat("INIZIO - Download del file.: ", cSrcFil))

            ' **************************************************************** '
            ' Verifica presenza di aggiornamenti nel sito FTP                  '
            ' **************************************************************** '
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaFileDaFTP", String.Concat("File rintracciato. Copia da : ", cSrcFil))
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaFileDaFTP", String.Concat("                   Copia a .: ", cDstFil))

            _oftp.DownloadFile(cSrcFil, cDstFil)
            If _oftp.Errore IsNot Nothing Then
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaFileDaFTP", String.Concat("                   Errore durante il trasferimento.", cDstFil))
                _AbortDownLoad = True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaFileDaFTP", String.Concat("INIZIO - Download del file.: ", cSrcFil))
        End Try

    End Sub

    Private Sub ScaricaDaFTPLoop(ByVal cSrcFld As String, ByVal cDstFld As String)
        Dim aListaFld As ArrayList
        Dim aListaFil As ArrayList
        Dim cSrcFile As String
        Dim cDstFile As String
        Dim cFolder As String
        Dim cFile As String

        Try
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaDaFTP", String.Concat("INIZIO - Scansione della cartella .: ", cSrcFld))
            If Not System.IO.Directory.Exists(cDstFld) Then
                System.IO.Directory.CreateDirectory(cDstFld)
            End If
            _oftp.RecuperaElementiFolder(cSrcFld)
            aListaFld = _oftp.ListaFLD
            aListaFil = _oftp.ListaFIL

            ' **************************************************************** '
            ' Verifica presenza di aggiornamenti nel sito FTP                  '
            ' **************************************************************** '
            For Each cFolder In aListaFld
                ScaricaDaFTPLoop(ConcatenaFolderValueWeb(cSrcFld, cFolder), ConcatenaFolderValue(cDstFld, cFolder))
            Next
            For Each cFile In aListaFil
                cSrcFile = String.Concat(cSrcFld, cFile)
                cDstFile = String.Concat(cDstFld, cFile)

                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaDaFTP", String.Concat("File rintracciato. Copia da : ", cSrcFile))
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaDaFTP", String.Concat("                   Copia a .: ", cDstFile))

                _oftp.DownloadFile(cSrcFile, cDstFile)
                If _oftp.Errore IsNot Nothing Then
                    _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaDaFTP", String.Concat("                   Errore durante il trasferimento.", cDstFile))
                    _AbortDownLoad = True
                End If
            Next
        Catch ex As Exception
            Throw ex
        Finally
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaDaFTP", String.Concat("FINE - Scansione della cartella .: ", cSrcFld))
        End Try

    End Sub

    Private Sub CopiaTraCartelleLoop(ByVal cSrcFld As String, ByVal cDstFld As String)
        Dim cSrcFile As String
        Dim cDstFile As String
        Dim cFolder As String
        Dim cFile As String

        Try
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "SpostaAggiornamenti", String.Concat("INIZIO - Scansione della cartella .: ", cSrcFld))
            If Not System.IO.Directory.Exists(cDstFld) Then
                System.IO.Directory.CreateDirectory(cDstFld)
            End If
            If System.IO.Directory.Exists(cSrcFld) Then
                For Each cFolder In System.IO.Directory.GetDirectories(cSrcFld)
                    CopiaTraCartelleLoop(cFolder, cFolder.Replace(cSrcFld, cDstFld))
                Next
                For Each cFile In System.IO.Directory.GetFiles(cSrcFld)
                    cSrcFile = cFile
                    cDstFile = cFile.Replace(cSrcFld, cDstFld)
                    _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "SpostaAggiornamenti", String.Concat("Aggiornamento trovato. Copia da : ", cSrcFile))
                    _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "SpostaAggiornamenti", String.Concat("Aggiornamento trovato. Copia a .: ", cDstFile))

                    System.IO.File.Copy(cSrcFile, cDstFile, True)
                    System.IO.File.Delete(cSrcFile)
                Next
            End If
        Catch ex As Exception
            Throw ex
        Finally
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "SpostaAggiornamenti", String.Concat("FINE - Scansione della cartella .: ", cSrcFld))
        End Try

    End Sub

    Private Sub BuildFileGuidaLoop(ByVal cSrcFLD As String, ByVal cStartFLD As String, ByVal cMainTag As String)
        Dim cFolder As String
        Dim cFile As String

        With _xdFileGuida.SelectSingleNode(cMainTag)
            For Each cFolder In System.IO.Directory.GetDirectories(cSrcFLD)
                .AppendChild(_xdFileGuida.CreateNode(Xml.XmlNodeType.Element, "item", ""))
                With .LastChild
                    .AppendChild(_xdFileGuida.CreateNode(Xml.XmlNodeType.Element, "folder", ""))
                    .SelectSingleNode("folder").InnerText = cFolder.Replace(cStartFLD, "")
                End With
                If _GeneralBuilding Then
                    BuildFileGuidaLoop(cFolder, cStartFLD, cMainTag)
                Else
                    If Not _LockedFolder.Contains(StandardFolderValue(cFolder)) Then
                        BuildFileGuidaLoop(cFolder, cStartFLD, cMainTag)
                    End If
                End If
            Next
            For Each cFIle In System.IO.Directory.GetFiles(cSrcFLD)
                If Not cFile.EndsWith("CFG.xml") And Not (System.IO.Path.GetFileName(cFile).Contains(String.Concat(_MainTag(0), ".xml"))) Then
                    .AppendChild(_xdFileGuida.CreateNode(Xml.XmlNodeType.Element, "item", ""))
                    With .LastChild
                        .AppendChild(_xdFileGuida.CreateNode(Xml.XmlNodeType.Element, "filename", ""))
                        .SelectSingleNode("filename").InnerText = cFile.Replace(cStartFLD, "")
                        .AppendChild(_xdFileGuida.CreateNode(Xml.XmlNodeType.Element, "hash", ""))
                        .SelectSingleNode("hash").InnerText = HashFile(cFile, "MD5", "X2")
                    End With
                End If
            Next
        End With

    End Sub

    Private Sub ConfrontaFileGuidaLoop(ByVal cMainFLD As String, ByVal cMainTag As String, ByVal xdMaster As Xml.XmlDocument, ByVal xdSlave As Xml.XmlDocument, ByVal lAdd As Boolean)
        Dim xnGuida As Xml.XmlNode
        Dim xnConfr As Xml.XmlNode
        Dim cFolder As String
        Dim cFile As String

        If xdMaster IsNot Nothing Then
            For Each xnGuida In xdMaster.SelectNodes(String.Concat(cMainTag, "/item"))
                cFolder = ""
                cFile = ""
                If xnGuida.SelectSingleNode("folder") IsNot Nothing Then
                    cFolder = xnGuida.SelectSingleNode("folder").InnerText
                End If
                If xnGuida.SelectSingleNode("filename") IsNot Nothing Then
                    cFile = xnGuida.SelectSingleNode("filename").InnerText
                End If

                If xdSlave IsNot Nothing Then
                    If cFolder > "" Then
                        xnConfr = xdSlave.SelectSingleNode(String.Concat(cMainTag, "/item[folder='", cFolder, "']"))
                        If xnConfr IsNot Nothing Then
                            If Not _FolderExist.Contains(cFolder) Then
                                If _GeneralBuilding Then
                                    _FolderExist.Add(String.Concat(cMainFLD, "\", cFolder))
                                Else
                                    _FolderExist.Add(cFolder)
                                End If
                            End If
                        Else
                            If lAdd Then
                                If Not _FolderDwUpload.Contains(cFolder) Then
                                    If _GeneralBuilding Then
                                        _FolderDwUpload.Add(String.Concat(cMainFLD, "\", cFolder))
                                    Else
                                        _FolderDwUpload.Add(cFolder)
                                    End If
                                End If
                            Else
                                If Not _FolderDelete.Contains(cFolder) Then
                                    If _GeneralBuilding Then
                                        _FolderDelete.Add(String.Concat(cMainFLD, "\", cFolder))
                                    Else
                                        _FolderDelete.Add(cFolder)
                                    End If
                                End If
                            End If
                        End If
                    ElseIf cFile > "" Then
                        xnConfr = xdSlave.SelectSingleNode(String.Concat(cMainTag, "/item[filename='", cFile, "']"))
                        If xnConfr IsNot Nothing Then
                            If (xnGuida.SelectSingleNode("hash").InnerText = xnConfr.SelectSingleNode("hash").InnerText) Then
                                If _GeneralBuilding Then
                                    _FilesExist.Add(String.Concat(cMainFLD, "\", cFile))
                                Else
                                    _FilesExist.Add(cFile)
                                End If
                            Else
                                If lAdd And Not _FilesDwUpload.Contains(cFile) And Not _FilesExist.Contains(cFile) Then
                                    If _GeneralBuilding Then
                                        _FilesDwUpload.Add(String.Concat(cMainFLD, "\", cFile))
                                    Else
                                        _FilesDwUpload.Add(cFile)
                                    End If
                                End If
                            End If
                        Else
                            If lAdd Then
                                If Not _FilesDwUpload.Contains(cFile) Then
                                    If _GeneralBuilding Then
                                        _FilesDwUpload.Add(String.Concat(cMainFLD, "\", cFile))
                                    Else
                                        _FilesDwUpload.Add(cFile)
                                    End If
                                End If
                            Else
                                If Not _FilesDelete.Contains(cFile) Then
                                    If _GeneralBuilding Then
                                        _FilesDelete.Add(String.Concat(cMainFLD, "\", cFile))
                                    Else
                                        _FilesDelete.Add(cFile)
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    If lAdd Then
                        If cFolder > "" And Not _FolderDwUpload.Contains(cFolder) Then
                            _FolderDwUpload.Add(String.Concat(cMainFLD, "\", cFolder))
                        End If
                        If cFile > "" And Not _FilesDwUpload.Contains(cFile) Then
                            _FilesDwUpload.Add(String.Concat(cMainFLD, "\", cFile))
                        End If
                    End If
                End If
            Next
        End If

    End Sub

    Public Sub PulisciFolderFTP(ByVal cSrcFld As String, ByVal lDeleteFLD As Boolean)
        Dim aListaFld As ArrayList
        Dim aListaFil As ArrayList
        Dim cSrcFile As String
        Dim cFolder As String
        Dim cFile As String
        Dim oftp As CLS_ftp

        Try
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "PulisciFolder", String.Concat("INIZIO - Scansione della cartella .: ", cSrcFld))
            oftp = New CLS_ftp(_FTPUsername, _FTPPassword)
            oftp.RecuperaElementiFolder(cSrcFld)
            aListaFld = oftp.ListaFLD
            aListaFil = oftp.ListaFIL

            ' **************************************************************** '
            ' Verifica presenza di aggiornamenti nel sito FTP                  '
            ' **************************************************************** '
            For Each cFolder In aListaFld
                PulisciFolderFTP(ConcatenaFolderValueWeb(cSrcFld, cFolder), True)
            Next
            For Each cFile In aListaFil
                cSrcFile = String.Concat(cSrcFld, cFile)
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "PulisciFolder", String.Concat("Aggiornamento trovato. Copia da : ", cSrcFile))
                oftp.DeleteFileFTP(cSrcFile)
            Next
            If lDeleteFLD Then
                oftp.DeleteFolderFTP(cSrcFld)
            End If
        Catch ex As Exception
            Throw ex
        Finally
            oftp = Nothing
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "PulisciFolder", String.Concat("FINE - Scansione della cartella .: ", cSrcFld))
        End Try

    End Sub

    'Public Sub RicaricaSuFTP()
    '    Dim cFolder As String

    '    _oftp = New CLS_ftp(_FTPUsername, _FTPPassword)
    '    _AbortDownLoad = False
    '    _oftp.DeleteFolderFTP(_FolderDstPrefix.Substring(0, _FolderDstPrefix.Length - 1))
    '    _oftp.CreaFolder(_FolderDstPrefix)
    '    For Each cFolder In _FolderToScan
    '        RicaricaSuFTPLoop(ConcatenaFolderValue(_FolderSrcPrefix, cFolder), ConcatenaFolderFileValueWeb(_FolderDstPrefix, cFolder))
    '    Next
    '    _oftp.DeleteFolderFTP(_FolderOLDPrefix)
    '    cFolder = _FolderOLDPrefix.Substring(0, _FolderOLDPrefix.Length - 1)
    '    cFolder = System.IO.Path.GetFileName(cFolder)
    '    _oftp.RinominaFolder(_FolderDstPrefix.Substring(0, _FolderDstPrefix.Length - 1), cFolder)
    '    _oftp = Nothing

    'End Sub

    Private Sub CaricaFileSuFTP(ByVal cSrcFil As String, ByVal cDstFil As String)

        Try
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "CaricaFileSuFTP", String.Concat("INIZIO - Upload del file.: ", cSrcFil))

            ' **************************************************************** '
            ' Verifica presenza di aggiornamenti nel sito FTP                  '
            ' **************************************************************** '
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "CaricaFileSuFTP", String.Concat("File rintracciato. Copia da : ", cSrcFil))
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "CaricaFileSuFTP", String.Concat("                   Copia a .: ", cDstFil))

            _oftp.UploadFile(cSrcFil, cDstFil)
            If _oftp.Errore IsNot Nothing Then
                _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "CaricaFileSuFTP", String.Concat("                   Errore durante il trasferimento.", cDstFil))
                _AbortDownLoad = True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "CaricaFileSuFTP", String.Concat("FINE - Upload del file.: ", cSrcFil))
        End Try

    End Sub

    Private Sub CaricaSuFTPLoop(ByVal cSrcFld As String, ByVal cDstFld As String)
        Dim cFolder As String
        Dim cFile As String

        Try
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaDaFTP", String.Concat("INIZIO - Scansione della cartella .: ", cSrcFld))
            _oftp.CreaFolder(cDstFld)
            For Each cFolder In System.IO.Directory.GetDirectories(cSrcFld)
                CaricaSuFTPLoop(cFolder, ConcatenaFolderValueWeb(cDstFld, System.IO.Path.GetFileName(cFolder)))
            Next
            For Each cFile In System.IO.Directory.GetFiles(cSrcFld)
                _oftp.UploadFile(cFile, ConcatenaFolderFileValueWeb(cDstFld, System.IO.Path.GetFileName(cFile)))
            Next

            'For Each cFolder In System.IO.Directory.GetDirectories
            '    If Not System.IO.Directory.Exists(cDstFld) Then
            '        System.IO.Directory.CreateDirectory(cDstFld)
            '    End If
            '    _oftp.RecuperaElementiFolder(cSrcFld)
            '    aListaFld = _oftp.ListaFLD
            '    aListaFil = _oftp.ListaFIL

            '    ' **************************************************************** '
            '    ' Verifica presenza di aggiornamenti nel sito FTP                  '
            '    ' **************************************************************** '
            '    For Each cFolder In aListaFld
            '        ScaricaDaFTPLoop(ConcatenaFolderValueWeb(cSrcFld, cFolder), ConcatenaFolderValue(cDstFld, cFolder))
            '    Next
            '    For Each cFile In aListaFil
            '        cSrcFile = String.Concat(cSrcFld, cFile)
            '        cDstFile = String.Concat(cDstFld, cFile)

            '        _log.ScriviMessaggioLog(LL_debugging, 3, "ScaricaDaFTP", String.Concat("File rintracciato. Copia da : ", cSrcFile))
            '        _log.ScriviMessaggioLog(LL_debugging, 3, "ScaricaDaFTP", String.Concat("                   Copia a .: ", cDstFile))

            '        _oftp.DownloadFile(cSrcFile, cDstFile)
            '        If _oftp.Errore IsNot Nothing Then
            '            _log.ScriviMessaggioLog(LL_debugging, 3, "ScaricaDaFTP", String.Concat("                   Errore durante il trasferimento.", cDstFile))
            '            _AbortDownLoad = True
            '        End If
            '    Next
        Catch ex As Exception
            Throw ex
        Finally
            _log.ScriviMessaggioLog(eLogLevel.LL_debugging, 3, "", "ScaricaDaFTP", String.Concat("FINE - Scansione della cartella .: ", cSrcFld))
        End Try

    End Sub

    Public Sub SpostaSuFTP()

        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword)
        _AbortDownLoad = False
        _oftp.RinominaFolder(_FolderSrcPrefix.Substring(0, _FolderSrcPrefix.Length - 1), System.IO.Path.GetFileName(_FolderDstPrefix.Substring(0, _FolderDstPrefix.Length - 1)))
        _oftp = Nothing

    End Sub

    Public Sub RimuoviSuFtp(Optional ByVal cFolder As String = "SRC*")
        Dim cFld As String

        If cFolder = "SRC*" Then
            cFld = _FolderSrcPrefix
        Else
            cFld = cFolder
        End If
        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword)
        _AbortDownLoad = False
        _oftp.DeleteFolderFTP(cFld.Substring(0, cFld.Length - 1))
        _oftp = Nothing

    End Sub

    Public Sub CreaFolder(ByVal cFolder As String)

        _oftp = New CLS_ftp(_FTPUsername, _FTPPassword, True)
        _oftp.CreaFolder(cFolder)
        _oftp = Nothing

    End Sub
    
End Class

