Imports System.Net
Imports System.io

Public Class CLS_ftp

    Private _FTPusername As String
    Private _FTPpassword As String
    Private _Errore As Exception
    Private _RequestFile As FtpWebRequest
    Private _ListaFLD As ArrayList
    Private _ListaFIL As ArrayList
    Private _ThrowError As Boolean

    Public Sub New(ByVal cUsername As String, ByVal cPassword As String, Optional ByVal lThrowErrore As Boolean = False)

        _FTPusername = cUsername
        _FTPpassword = cPassword

    End Sub

    Public Property Errore() As Exception

        Get
            Return _Errore
        End Get
        Set(ByVal value As Exception)
            _Errore = value
            If _ThrowError Then
                Throw _Errore
            End If
        End Set

    End Property

    Public ReadOnly Property IsErrore() As Boolean

        Get
            Return _Errore IsNot Nothing
        End Get

    End Property

    Public ReadOnly Property ListaFIL() As ArrayList

        Get
            Return _ListaFIL
        End Get

    End Property

    Public ReadOnly Property ListaFLD() As ArrayList

        Get
            Return _ListaFLD
        End Get

    End Property

    Public WriteOnly Property FTPPassword() As String

        Set(ByVal value As String)
            _FTPpassword = value
        End Set

    End Property

    Public WriteOnly Property FTPUserName() As String

        Set(ByVal value As String)
            _FTPusername = value
        End Set

    End Property

    Public Sub UploadFile(ByVal cLocation As String, ByVal destFileName As String)
        Dim responseFTP As System.Net.FtpWebResponse
        Dim responseStream As IO.Stream
        Dim outFile As IO.FileStream
        Dim readBuffer(4095) As Byte
        Dim count As Integer

        Try
            _Errore = Nothing

            ' ----- Connect to the file on the FTP site.
            CreaConnessioneFTP(destFileName, System.Net.WebRequestMethods.Ftp.UploadFile)

            ' ----- Open a transmission channel for the file content.
            '****************************************************************************
            ' responseFTP = CType(_RequestFile.GetResponse, System.Net.FtpWebResponse)

            'responseStream = responseFTP.GetResponseStream
            responseStream = _RequestFile.GetRequestStream
            outFile = New IO.FileStream(cLocation, IO.FileMode.Open)

            Do
                count = outFile.Read(readBuffer, 0, readBuffer.Length)
                responseStream.Write(readBuffer, 0, count)
            Loop Until count = 0

            'responseStream.Write(System.IO.File.ReadAllBytes(cLocation), 0, System.IO.File.ReadAllBytes(cLocation).Length)
        Catch ex As Exception
            _Errore = ex
        Finally
            ' ----- Cleanup.
            If responseStream IsNot Nothing Then
                responseStream.Close()
            End If
            If outFile IsNot Nothing Then
                outFile.Flush()
                outFile.Close()
            End If

            If responseFTP IsNot Nothing Then
                responseFTP.Close()
            End If
        End Try

    End Sub

    Public Sub DownloadFile(ByVal cLocation As String, ByVal destFileName As String)
        Dim responseFTP As System.Net.FtpWebResponse
        Dim responseStream As IO.Stream
        Dim outFile As IO.FileStream
        Dim readBuffer(4095) As Byte
        Dim count As Integer

        Try
            _Errore = Nothing

            ' ----- Connect to the file on the FTP site.
            CreaConnessioneFTP(cLocation, System.Net.WebRequestMethods.Ftp.DownloadFile)

            ' ----- Open a transmission channel for the file content.
            '****************************************************************************
            responseFTP = CType(_RequestFile.GetResponse, System.Net.FtpWebResponse)

            responseStream = responseFTP.GetResponseStream
            outFile = New IO.FileStream(destFileName, IO.FileMode.Create)

            Do
                count = responseStream.Read(readBuffer, 0, readBuffer.Length)
                outFile.Write(readBuffer, 0, count)
            Loop Until count = 0
        Catch ex As Exception
            _Errore = ex
        Finally
            ' ----- Cleanup.
            If responseStream IsNot Nothing Then
                responseStream.Close()
            End If
            If outFile IsNot Nothing Then
                outFile.Flush()
                outFile.Close()
            End If
            If responseFTP IsNot Nothing Then
                responseFTP.Close()
            End If
        End Try

    End Sub

    Public Sub RecuperaElementiFolder(ByVal cLocation As String)
        Dim responseFTP As FtpWebResponse
        Dim ResponseStream As Stream
        Dim reader As StreamReader
        Dim cTmp As String

        Try
            _Errore = Nothing
            _ListaFLD = New ArrayList
            _ListaFIL = New ArrayList
            CreaConnessioneFTP(cLocation, WebRequestMethods.Ftp.ListDirectoryDetails)

            responseFTP = CType(_RequestFile.GetResponse(), FtpWebResponse)
            ResponseStream = responseFTP.GetResponseStream()
            reader = New StreamReader(ResponseStream)

            ' Recupera Elenco Cartelle
            While Not reader.EndOfStream
                cTmp = reader.ReadLine
                If cTmp.Contains("<DIR>") Then
                    cTmp = cTmp.Substring(cTmp.LastIndexOf(" ") + 1)
                    _ListaFLD.Add(cTmp)
                Else
                    cTmp = cTmp.Substring(cTmp.LastIndexOf(" ") + 1)
                    _ListaFIL.Add(cTmp)
                End If
            End While
        Catch ex As Exception
            _Errore = ex
        Finally
            ' ----- Cleanup.
            If ResponseStream IsNot Nothing Then
                ResponseStream.Close()
            End If
            If reader IsNot Nothing Then
                reader.Close()
            End If
            If responseFTP IsNot Nothing Then
                responseFTP.Close()
            End If
        End Try

    End Sub

    Public Sub DeleteFileFTP(ByVal cLocation As String)
        Dim responseFTP As System.Net.FtpWebResponse

        Try
            _Errore = Nothing
            CreaConnessioneFTP(cLocation, WebRequestMethods.Ftp.DeleteFile)

            ' ----- Open a transmission channel for the file content.
            '****************************************************************************
            responseFTP = CType(_RequestFile.GetResponse, System.Net.FtpWebResponse)

            responseFTP.Close()

        Catch ex As Exception
            _Errore = ex
        Finally
            If responseFTP IsNot Nothing Then
                responseFTP.Close()
            End If
        End Try

    End Sub

    Public Sub CreaFolder(ByVal cFLDSource As String)
        Dim responseFTP As System.Net.FtpWebResponse
        Dim ftpStream As Stream = Nothing

        Try
            _Errore = Nothing
            CreaConnessioneFTP(cFLDSource, WebRequestMethods.Ftp.MakeDirectory)
            ' ----- Open a transmission channel for the file content.
            '****************************************************************************
            responseFTP = CType(_RequestFile.GetResponse, System.Net.FtpWebResponse)
            ftpStream = responseFTP.GetResponseStream()
            ftpStream.Close()
            responseFTP.Close()

        Catch ex As Exception
            _Errore = ex
            'Throw ex
        Finally
            If responseFTP IsNot Nothing Then
                responseFTP.Close()
            End If
        End Try

    End Sub

    Public Sub RinominaFolder(ByVal cFLDSource As String, ByVal cFLDDestin As String)
        Dim responseFTP As System.Net.FtpWebResponse
        Dim ftpStream As Stream = Nothing

        Try
            _Errore = Nothing
            CreaConnessioneFTP(cFLDSource, WebRequestMethods.Ftp.Rename)
            _RequestFile.RenameTo = cFLDDestin
            ' ----- Open a transmission channel for the file content.
            '****************************************************************************
            responseFTP = CType(_RequestFile.GetResponse, System.Net.FtpWebResponse)
            ftpStream = responseFTP.GetResponseStream()
            ftpStream.Close()
            responseFTP.Close()

        Catch ex As Exception
            _Errore = ex
            Throw ex
        Finally
            If responseFTP IsNot Nothing Then
                responseFTP.Close()
            End If
        End Try

    End Sub

    Public Sub DeleteFolderFTP(ByVal cLocation As String)
        Dim responseFTP As System.Net.FtpWebResponse

        Try
            _Errore = Nothing
            CreaConnessioneFTP(cLocation, WebRequestMethods.Ftp.RemoveDirectory)

            ' ----- Open a transmission channel for the file content.
            '****************************************************************************
            responseFTP = CType(_RequestFile.GetResponse, System.Net.FtpWebResponse)

            responseFTP.Close()

        Catch ex As Exception
            _Errore = ex
        Finally
            If responseFTP IsNot Nothing Then
                responseFTP.Close()
            End If
        End Try

    End Sub

    Private Sub CreaConnessioneFTP(ByVal cLocation As String, ByVal cMethod As String)

        _RequestFile = CType(System.Net.FtpWebRequest.Create(cLocation), System.Net.FtpWebRequest)
        _RequestFile.Credentials = New System.Net.NetworkCredential(_FTPusername, _FTPpassword)
        _RequestFile.KeepAlive = False
        _RequestFile.UseBinary = True
        _RequestFile.UsePassive = False
        _RequestFile.Method = cMethod

    End Sub

End Class
