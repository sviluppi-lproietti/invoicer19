<Serializable()> _
Public Class CLS_FileFolderDic


    Private _FFDic As Dictionary(Of String, String)

    ReadOnly Property Values As Dictionary(Of String, String)

        Get
            Return _FFDic
        End Get

    End Property

    Public Sub New(Optional ByVal lWebItem As Boolean = False)

        _FFDic = New Dictionary(Of String, String)
        WebItem = lWebItem

    End Sub

    Public Sub New(ByVal base As CLS_FileFolderDic)
        Me.new(base.WebItem)
        Dim cKey As String

        For Each cKey In base.Keys
            _FFDic.Add(cKey, base(cKey, False))
        Next

    End Sub

    Public Property WebItem As Boolean

    Public Sub Add(ByVal cKey As String, ByVal cValue As String)

        _FFDic.Add(cKey, cValue)

    End Sub

    Default Public Overloads Property Item(ByVal cKey As String, Optional ByVal lCalculate As Boolean = True, Optional ByVal cTimeStamp As String = "", Optional ByVal lCreate As Boolean = True) As String

        Get
            '        lCalculate = True
            '        cTimeStamp = ""
            '        lCreate = True
            Dim cRtn As String

            If Not _FFDic.ContainsKey(cKey) Then
                _FFDic.Add(cKey, "")
            End If
            cRtn = _FFDic(cKey)
            If lCalculate Then
                While cRtn.Contains("*")
                    If cRtn.Contains("*DATE*") Then
                        cRtn = cRtn.Replace("*DATE*", Now.ToString("yyyyMMdd"))
                    ElseIf cRtn.Contains("*TS*") Then
                        cRtn = cRtn.Replace("*TS*", cTimeStamp)
                    End If
                End While

                If cRtn.EndsWith("\") And lCreate And Not WebItem Then
                    If Not System.IO.Directory.Exists(cRtn) Then
                        System.IO.Directory.CreateDirectory(cRtn)
                    End If
                End If
            End If
            Return cRtn
        End Get
        Set(ByVal value As String)
            _FFDic(cKey) = value
        End Set

    End Property

    Public Function ContainsKey(ByVal cKey As String) As Boolean

        Return _FFDic.ContainsKey(cKey)

    End Function

    Public Shared Sub copy(ByVal src As CLS_FileFolderDic, ByVal dst As CLS_FileFolderDic)
        Dim cKey As String

        For Each cKey In src._FFDic.Keys
            dst.Add(cKey, src(cKey))
        Next

    End Sub

    Public ReadOnly Property Keys() As System.Collections.Generic.Dictionary(Of String, String).KeyCollection

        Get
            Return _FFDic.Keys
        End Get

    End Property

    Public Sub replacevalue(ByVal oTmp As CLS_FileFolderDic)
        Dim cKey As String

        For Each cKey In oTmp.Keys
            If _FFDic.ContainsKey(cKey) Then
                _FFDic(cKey) = oTmp(cKey)
            Else
                _FFDic.Add(cKey, oTmp(cKey))
            End If
        Next

    End Sub

End Class
