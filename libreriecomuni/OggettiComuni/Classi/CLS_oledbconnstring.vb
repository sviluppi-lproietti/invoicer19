﻿Public Class CLS_oledbconnstring

    Private _Provider As String
    Private _ExtendedProperty As String

    Public Sub New(ByVal p As String, ByVal e As String)

        _Provider = p
        _ExtendedProperty = e

    End Sub

    Public Function BuildConnString(ByVal cFileName As String) As String
        Dim cRtn As String

        cRtn = String.Concat("Provider=", _Provider, ";")
        cRtn = String.Concat(cRtn, "Data Source=", cFileName, ";")
        cRtn = String.Concat(cRtn, "Extended Properties=", _ExtendedProperty, ";")
        Return cRtn

    End Function

End Class
