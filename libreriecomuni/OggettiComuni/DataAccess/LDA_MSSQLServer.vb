Imports System.Data.SqlClient
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita

Public Class LDA_MSSQLServer
    Inherits LDA_BaseClass

    Private _daDataAccess As SqlDataAdapter
    Private _dbConn As SqlConnection
    Private _LeaveConnOpen As Boolean
    Private _SQLCmdSP As SqlClient.SqlCommand
    Private _Transazione As SqlClient.SqlTransaction

    Public Sub New(Optional ByVal cDBConnectioinString As String = "")

        MyBase.New(cDBConnectioinString)
        _dbConn = New SqlConnection(cDBConnectioinString)
        _LeaveConnOpen = True

    End Sub

    Public Sub New(ByVal cDBConnectioinString As String, ByRef ds As DataSet)

        MyBase.New(cDBConnectioinString, ds)
        _dbConn = New SqlConnection(cDBConnectioinString)
        _LeaveConnOpen = True

    End Sub

    Public Sub New(ByVal dbConn As SqlConnection, ByRef ds As DataSet)

        _dbConn = dbConn
        DatasetUsed = ds
        _LeaveConnOpen = True

    End Sub

    Public Overrides Sub PreparaComando(ByVal cTabella As String, Optional ByVal aListaParam As ArrayList = Nothing)

        _daDataAccess = New SqlDataAdapter
        For Each tCmd As TipoOperDS In ListaComandi
            If tCmd = TipoOperDS.TODS_delete Then
                _daDataAccess.DeleteCommand = BuildCommand(tCmd)
                _daDataAccess.DeleteCommand.CommandTimeout = CmdTimeout
                _daDataAccess.DeleteCommand.CommandText = CreateSQLCommand(TipoOperDS.TODS_delete, DatasetUsed, cTabella, aListaParam, "")
            ElseIf tCmd = TipoOperDS.TODS_insert Then
                _daDataAccess.InsertCommand = BuildCommand(tCmd)
                _daDataAccess.InsertCommand.CommandText = CreateSQLCommand(TipoOperDS.TODS_insert, DatasetUsed, cTabella, aListaParam, "")
                _daDataAccess.InsertCommand.CommandTimeout = CmdTimeout
            ElseIf tCmd = TipoOperDS.TODS_select Then
                _daDataAccess.SelectCommand = BuildCommand(tCmd)
                _daDataAccess.SelectCommand.CommandText = CreateSQLCommand(TipoOperDS.TODS_select, DatasetUsed, cTabella, aListaParam, "")
                _daDataAccess.SelectCommand.CommandTimeout = CmdTimeout
            ElseIf tCmd = TipoOperDS.TODS_update Then
                _daDataAccess.UpdateCommand = BuildCommand(tCmd)
                _daDataAccess.UpdateCommand.CommandText = CreateSQLCommand(TipoOperDS.TODS_update, DatasetUsed, cTabella, New ArrayList, "")
                _daDataAccess.UpdateCommand.CommandTimeout = CmdTimeout
            ElseIf tCmd = TipoOperDS.TODS_storpr Then
                _SQLCmdSP = BuildCommand(tCmd)
                _SQLCmdSP.CommandText = cTabella
                _SQLCmdSP.CommandTimeout = CmdTimeout
                For Each aParam As String() In aListaParam
                    ListaParametri.Add(New String() {tCmd, aParam(2), aParam(1), aParam(2).Replace("@", ""), "O", aParam(3)})
                Next
            End If
        Next

    End Sub

    Public Overrides Sub CreaParametri()
        Dim aValue As String()

        For Each aValue In ListaParametri
            Select Case aValue(0)
                Case Is = TipoOperDS.TODS_delete
                    _daDataAccess.DeleteCommand.Parameters.Add(CreaParametro(aValue(1), aValue(2), aValue(3), aValue(4), aValue(5)))
                Case Is = TipoOperDS.TODS_insert
                    _daDataAccess.InsertCommand.Parameters.Add(CreaParametro(aValue(1), aValue(2), aValue(3), aValue(4), aValue(5)))
                Case Is = TipoOperDS.TODS_select
                    _daDataAccess.SelectCommand.Parameters.Add(CreaParametro(aValue(1), aValue(2), aValue(3), aValue(4), aValue(5)))
                Case Is = TipoOperDS.TODS_update
                    _daDataAccess.UpdateCommand.Parameters.Add(CreaParametro(aValue(1), aValue(2), aValue(3), aValue(4), aValue(5)))
                Case Is = TipoOperDS.TODS_storpr
                    _SQLCmdSP.Parameters.Add(CreaParametroSP(aValue(1), aValue(2), aValue(3), aValue(4), aValue(5)))
            End Select
        Next

    End Sub

    Public Overrides Sub EseguiCaricamento(ByVal cTabella As String, ByVal nMaxRecordToLoad As Integer)
        Dim sqlReader As SqlClient.SqlDataReader
        Dim allCols(0) As Object
        Dim cMapTable As String
        Dim lExit As Boolean
        Dim nRow As Integer

        With _daDataAccess
            Try
                _TroppiRecord = False
                cMapTable = "table" & .TableMappings.Count
                .TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping(cMapTable, cTabella)})
                .TableMappings(cMapTable).DataSetTable = cTabella
                ApriConnessioneDB()
                If nMaxRecordToLoad = 0 Then
                    .Fill(DatasetUsed, cMapTable)
                Else
                    sqlReader = _daDataAccess.SelectCommand.ExecuteReader
                    ReDim allCols(sqlReader.FieldCount - 1)

                    nRow = 0
                    lExit = False
                    While (nRow <= nMaxRecordToLoad) And Not lExit
                        lExit = Not sqlReader.Read()
                        If Not lExit Then
                            sqlReader.GetValues(allCols)
                            DatasetUsed.Tables(cTabella).Rows.Add(allCols)
                            nRow += 1
                        End If
                    End While
                    _TroppiRecord = nRow > nMaxRecordToLoad
                    If _TroppiRecord Then
                        DatasetUsed.Tables(cTabella).Rows.Clear()
                    End If
                    sqlReader.Close()
                End If
                DatasetUsed.Tables(cTabella).AcceptChanges()
            Catch ex As Exception
                Throw ex
            Finally
                ChiudiConnessioneDB()
            End Try
        End With

    End Sub

    Public Overrides Sub EseguiAggiornamento(ByVal cTabella As String)
        Dim cTableDS As String

        Try
            AddHandler _daDataAccess.RowUpdated, AddressOf daDataAccess_RowUpdated
            ApriConnessioneDB()
            cTableDS = "table" & _daDataAccess.TableMappings.Count
            If Not _daDataAccess.TableMappings.Contains(cTabella) Then
                _daDataAccess.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping(cTableDS, cTabella)})
            End If
            _daDataAccess.TableMappings(cTableDS).DataSetTable = cTabella
            _daDataAccess.Update(DatasetUsed, cTabella)
            DatasetUsed.Tables(cTabella).AcceptChanges()
        Catch Ex As Exception
            Throw Ex
        Finally
            ChiudiConnessioneDB()
        End Try

    End Sub

    Public Overrides Function NomeParametriCMD(ByVal cNomeColonna As String) As String

        Return String.Concat("@", cNomeColonna)

    End Function

    Public Overrides Function ExecuteScalarS(ByVal cSql As String) As Object
        Dim oRtn As Object

        Try
            _daDataAccess = New SqlDataAdapter
            _daDataAccess.SelectCommand = SetUpCommand_ES(cSql)
            ApriConnessioneDB()
            oRtn = _daDataAccess.SelectCommand.ExecuteScalar()
        Catch ex As Exception
            oRtn = Nothing
            Throw ex
        Finally
            ChiudiConnessioneDB()
        End Try
        Return oRtn

    End Function

    Public Overrides Sub ExecuteScalarU(ByVal cSql As String)
        Dim nRtn As Integer

        Try
            _daDataAccess = New SqlDataAdapter
            _daDataAccess.UpdateCommand = SetUpCommand_US(cSql)
            ApriConnessioneDB()
            _daDataAccess.UpdateCommand.ExecuteScalar()
        Catch ex As Exception
            nRtn = -1
            Throw ex
        Finally
            ChiudiConnessioneDB()
        End Try

    End Sub

    Public Overrides Sub EseguiSP()

        Try
            ApriConnessioneDB()
            _SQLCmdSP.ExecuteScalar()
        Catch ex As Exception
            Throw ex
        Finally
            ChiudiConnessioneDB()
        End Try

    End Sub

    Private Function CreaParametro(ByVal cNomeParam As String, ByVal cTipoParam As String, ByVal cSourceColumn As String, ByVal cTipoValoreParam As String, ByVal cValoreParam As Object) As SqlParameter
        Dim pParam As SqlParameter

        pParam = New SqlParameter
        With pParam
            .ParameterName = cNomeParam
            If cTipoParam.ToLower = "string" Then
                .SqlDbType = SqlDbType.VarChar
                If Not (cValoreParam Is Nothing) Then
                    .Value = cValoreParam
                End If
            ElseIf cTipoParam.ToLower = "int32" Then
                .SqlDbType = SqlDbType.Int
                If Not (cValoreParam Is Nothing) Then
                    .Value = cValoreParam
                End If
            ElseIf cTipoParam.ToLower = "boolean" Then
                .SqlDbType = SqlDbType.Bit
                If Not (cValoreParam Is Nothing) Then
                    .Value = CBool(cValoreParam)
                End If
            End If
            If (cValoreParam Is Nothing) Then
                .SourceColumn = cSourceColumn
                Select Case cTipoValoreParam
                    Case Is = "O"
                        .SourceVersion = DataRowVersion.Original
                End Select
            End If
        End With
        Return pParam

    End Function

    Private Function CreaParametroSP(ByVal cNomeParam As String, ByVal cTipoParam As String, ByVal cSourceColumn As String, ByVal cTipoValoreParam As String, ByVal cValoreParam As Object) As SqlParameter
        Dim pParam As SqlParameter

        pParam = New SqlParameter
        With pParam
            .ParameterName = cNomeParam
            If cTipoParam.ToLower = "string" Then
                .SqlDbType = SqlDbType.VarChar
                If Not (cValoreParam Is Nothing) Then
                    .Value = cValoreParam
                End If
            ElseIf cTipoParam.ToLower = "int" Then
                .SqlDbType = SqlDbType.Int
                If Not (cValoreParam Is Nothing) Then
                    .Value = cValoreParam
                End If
            ElseIf cTipoParam.ToLower = "boolean" Then
                .SqlDbType = SqlDbType.Bit
                If Not (cValoreParam Is Nothing) Then
                    .Value = CBool(cValoreParam)
                End If
            End If
            If (cValoreParam Is Nothing) Then
                .SourceColumn = cSourceColumn
                Select Case cTipoValoreParam
                    Case Is = "O"
                        .SourceVersion = DataRowVersion.Original
                End Select
            End If
        End With
        Return pParam

    End Function

    Private Sub daDataAccess_RowUpdated(ByVal sender As Object, ByVal e As SqlRowUpdatedEventArgs)
        Dim _Command As New SqlCommand
        Dim nValue As Integer
        Dim dc As DataColumn

        If e.StatementType = StatementType.Insert And e.Status = UpdateStatus.Continue Then
            dc = CType(DatasetUsed.Tables(_TabellaUpdating).PrimaryKey(0), DataColumn)
            If dc.AutoIncrement Then
                _Command.CommandText = String.Concat("SELECT MAX(", dc.ColumnName, ") FROM ", _TabellaUpdating)
                _Command.Connection = _daDataAccess.InsertCommand.Connection
                If _Transazione IsNot Nothing Then
                    _Command.Transaction = _Transazione
                End If
                nValue = _Command.ExecuteScalar()
                AggiornaRecord(e.Row, nValue)
            End If
        ElseIf e.StatementType = StatementType.Delete And e.Status = UpdateStatus.ErrorsOccurred Then
            e.Row.RejectChanges()
            e.Row("DELETED") = 1
            DeleteOperationKO = True
        End If

    End Sub

    Private Function SetUpCommand_ES(ByVal cSQL As String) As SqlCommand
        Dim sqlCMD As SqlCommand

        sqlCMD = BuildCommand()
        sqlCMD.CommandText = cSQL
        sqlCMD.CommandTimeout = CmdTimeout
        Return sqlCMD

    End Function

    Private Function SetUpCommand_US(ByVal cSQL As String) As SqlCommand
        Dim sqlCMD As SqlCommand

        sqlCMD = BuildCommand()
        sqlCMD.CommandText = cSQL
        sqlCMD.CommandTimeout = CmdTimeout
        Return sqlCMD

    End Function

    Private Function BuildCommand(Optional ByVal tCmd As TipoOperDS = TipoOperDS.TODS_NotDef) As SqlCommand
        Dim sqlCMD As SqlCommand

        sqlCMD = New SqlCommand()
        If _dbConn Is Nothing Then
            _dbConn = New SqlConnection(DBConnectioinString)
        End If
        sqlCMD.Connection = _dbConn
        If tCmd = TipoOperDS.TODS_storpr Then
            sqlCMD.CommandType = CommandType.StoredProcedure
        Else
            sqlCMD.CommandType = CommandType.Text
        End If
        Return sqlCMD

    End Function

    Private Sub ApriConnessioneDB()

        If _dbConn.State = ConnectionState.Closed Then
            _dbConn.Open()
        End If
        If _Transazione IsNot Nothing Then
            If _daDataAccess.SelectCommand IsNot Nothing Then
                _daDataAccess.SelectCommand.Transaction = _Transazione
            End If
            If _daDataAccess.DeleteCommand IsNot Nothing Then
                _daDataAccess.DeleteCommand.Transaction = _Transazione
            End If
            If _daDataAccess.InsertCommand IsNot Nothing Then
                _daDataAccess.InsertCommand.Transaction = _Transazione
            End If
            If _daDataAccess.UpdateCommand IsNot Nothing Then
                _daDataAccess.UpdateCommand.Transaction = _Transazione
            End If
        End If
 
    End Sub

    Private Sub ChiudiConnessioneDB()

        If Not _LeaveConnOpen Then
            _dbConn.Close()
        End If

    End Sub

    Public Overrides Sub StartTransazione()

        If _dbConn.State = ConnectionState.Closed Then
            _dbConn.Open()
        End If
        _Transazione = _dbConn.BeginTransaction(String.Concat("TRANSAZIONE_", Timestamp()))

    End Sub

    Public Overrides Sub CommitTransazione()

        _Transazione.Commit()

    End Sub

    Public Overrides Sub RollBackTransazione()

        _Transazione.Rollback()

    End Sub

End Class