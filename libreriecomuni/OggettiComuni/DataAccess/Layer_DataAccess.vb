Public Class Layer_DataAccess

    Private _DBConnectioinString As String
    Private _ConnectionDB As String
    Private _LDA As LDA_BaseClass
    Private _Provider As String
    Private _TabellaUpdating As String
    Private _Errore As Exception
    Private _LeaveConnOpen As Boolean
    Private _ThrowError As Boolean

    Public Sub New(Optional ByVal cDBConnectioinString As String = "")

        DBConnectioinString = cDBConnectioinString
        _LDA.LeaveConnOpen = False
        _LDA.CmdTimeout = 1000
        _ThrowError = False

    End Sub

    Public WriteOnly Property LeaveConnOpen() As Boolean

        Set(ByVal value As Boolean)
            _LDA.LeaveConnOpen = value
        End Set

    End Property

    Public WriteOnly Property ThrowError() As Boolean

        Set(ByVal value As Boolean)
            _ThrowError = True
        End Set

    End Property


    Public Sub New(ByVal cDBConnectioinString As String, ByRef ds As DataSet)

        DBConnectioinString = cDBConnectioinString
        _LDA.DatasetUsed = ds

    End Sub

    Public WriteOnly Property DBConnectioinString() As String

        Set(ByVal value As String)
            If value > "" Then
                _Provider = value.Substring(0, value.IndexOf(";")).ToLower.Replace("provider=", "")
                Select Case _Provider.ToLower
                    Case Is = "microsoft.jet.oledb.4.0", Is = "microsoft.ace.oledb.12.0"
                        _ConnectionDB = value
                        _LDA = New LDA_OleDB(_ConnectionDB)
                    Case Is = "sqldb"
                        _DBConnectioinString = value.Substring(0, value.IndexOf(";")).ToLower.Replace("provider=", "")
                        _ConnectionDB = value.Substring(value.IndexOf(";") + 1)
                        _LDA = New LDA_MSSQLServer(_ConnectionDB)
                End Select
                _LDA.CmdTimeout = 30
            End If
        End Set

    End Property

    Public WriteOnly Property CmdTimeout() As Integer

        Set(ByVal value As Integer)
            _LDA.CmdTimeout = value
        End Set

    End Property


    Public ReadOnly Property Errore() As Exception

        Get
            Return _Errore
        End Get

    End Property

    Public Property DatasetUsed() As DataSet

        Get
            Return _LDA.DatasetUsed
        End Get
        Set(ByVal value As DataSet)
            _LDA.DatasetUsed = value
        End Set

    End Property

    Public ReadOnly Property TroppiRecord() As Boolean

        Get
            Return _LDA.TroppiRecord
        End Get

    End Property

    Public Sub CaricaDataSet(ByVal cTabella As String, ByVal aListaParametri As ArrayList, Optional ByVal nMaxRecordToLoad As Integer = 0)

        Try
            _LDA.ListaComandi = New ArrayList
            _LDA.ListaParametri = New ArrayList
            _LDA.ListaComandi.Add(LDA_BaseClass.TipoOperDS.TODS_select)
            _LDA.PreparaComando(cTabella, aListaParametri)
            _LDA.CreaParametri()
            _LDA.EseguiCaricamento(cTabella, nMaxRecordToLoad)
        Catch ex As Exception
            _Errore = ex
            If _ThrowError Then
                Throw ex
            End If
        End Try

    End Sub

    Public Sub CaricaDataSet(ByVal cTabella As String, Optional ByVal aParams(,) As String = Nothing, Optional ByVal nMaxRecordToLoad As Integer = 0)
        Dim aListaParametri As ArrayList
        Dim i As Integer

        Try
            aListaParametri = New ArrayList
            If Not aParams Is Nothing Then
                For i = 0 To aParams.GetUpperBound(0)
                    aListaParametri.Add(New String() {aParams(i, 0), aParams(i, 1), aParams(i, 2), aParams(i, 3)})
                Next
            End If
            CaricaDataSet(cTabella, aListaParametri, nMaxRecordToLoad)
        Catch ex As Exception
            _Errore = ex
            If _ThrowError Then
                Throw ex
            End If
        End Try

    End Sub

    '
    ' lNoDelete inibisce la cancellazione delle righe in caso di tabelle con relazioni preimpostate.
    '
    Public Function AggiornaDataSet(ByVal cTabella As String, Optional ByVal cCommandText As String = "", Optional ByVal aParams(,) As String = Nothing, Optional ByVal lDelete As Boolean = True) As Boolean
        Dim aListaParametri As ArrayList
        Dim lRtn As Boolean
        Dim i As Integer

        Try
            aListaParametri = New ArrayList
            If aParams IsNot Nothing Then
                For i = 0 To aParams.GetUpperBound(0)
                    aListaParametri.Add(New String() {aParams(i, 0), aParams(i, 1), aParams(i, 2), aParams(i, 3)})
                Next
            End If
            _LDA.ListaComandi = New ArrayList
            _LDA.ListaParametri = New ArrayList
            _LDA.DeleteOperationKO = False
            If Not (DatasetUsed.Tables(cTabella).GetChanges(DataRowState.Added) Is Nothing) Then
                _LDA.ListaComandi.Add(LDA_BaseClass.TipoOperDS.TODS_insert)
                _TabellaUpdating = cTabella
            End If
            If Not (DatasetUsed.Tables(cTabella).GetChanges(DataRowState.Deleted) Is Nothing) And lDelete Then
                _LDA.ListaComandi.Add(LDA_BaseClass.TipoOperDS.TODS_delete)
            End If
            If Not (DatasetUsed.Tables(cTabella).GetChanges(DataRowState.Modified) Is Nothing) Then
                _LDA.ListaComandi.Add(LDA_BaseClass.TipoOperDS.TODS_update)
                _TabellaUpdating = cTabella
            End If
            If _LDA.ListaComandi.Count > 0 Then
                _LDA.PreparaComando(cTabella, aListaParametri)
                _LDA.CreaParametri()
                _LDA.EseguiAggiornamento(cTabella)
            End If
            lRtn = True
            _Errore = Nothing
        Catch ex As Exception
            _Errore = ex
            If _LDA.DeleteOperationKO Then
                ' Partendo dal presupposto che si cancella un record alla volta imposto ad uno il campo DELETED della tabella in questione.
                aListaParametri = New ArrayList
                _LDA.ListaComandi = New ArrayList
                _LDA.ListaParametri = New ArrayList
                _LDA.DeleteOperationKO = False
                If Not (DatasetUsed.Tables(cTabella).GetChanges(DataRowState.Added) Is Nothing) Then
                    _LDA.ListaComandi.Add(LDA_BaseClass.TipoOperDS.TODS_insert)
                End If
                If Not (DatasetUsed.Tables(cTabella).GetChanges(DataRowState.Deleted) Is Nothing) Then
                    _LDA.ListaComandi.Add(LDA_BaseClass.TipoOperDS.TODS_delete)
                End If
                If Not (DatasetUsed.Tables(cTabella).GetChanges(DataRowState.Modified) Is Nothing) Then
                    _LDA.ListaComandi.Add(LDA_BaseClass.TipoOperDS.TODS_update)
                End If
                _LDA.PreparaComando(cTabella, aListaParametri)
                _LDA.CreaParametri()
                _LDA.EseguiAggiornamento(cTabella)
                For Each dr As DataRow In _LDA.DatasetUsed.Tables(cTabella).Rows
                    If dr("DELETED") Then
                        dr.Delete()
                    End If
                Next
                _LDA.DatasetUsed.Tables(cTabella).AcceptChanges()
                _Errore = Nothing
                lRtn = True
            Else
                lRtn = False
            End If
            If _ThrowError Then
                Throw ex
            End If
        End Try
        Return lRtn

    End Function

    Public Sub PulisciTabella(ByVal cTabella As String)

        DatasetUsed.Tables(cTabella).Rows.Clear()

    End Sub

    Public Function ExecuteScalarS(ByVal cSql As String, Optional ByVal cTipoValore As String = "int") As Object
        Dim oRtn As Object

        Try
            oRtn = _LDA.ExecuteScalarS(cSql)
            If oRtn Is Nothing Then
                If cTipoValore.ToLower = "int" Then
                    oRtn = -1
                End If
            End If
            _Errore = Nothing
        Catch ex As Exception
            _Errore = ex
            If _ThrowError Then
                Throw ex
            End If
        End Try
        Return oRtn

    End Function

    Public Sub ExecuteScalarU(ByVal cSql As String)

        _LDA.ExecuteScalarU(cSql)

    End Sub

    Public Sub ExecuteSP(ByVal cSPName As String, ByVal aListaParametri As ArrayList)

        Try
            _LDA.ListaComandi = New ArrayList
            _LDA.ListaParametri = New ArrayList
            _LDA.ListaComandi.Add(LDA_BaseClass.TipoOperDS.TODS_storpr)
            _LDA.PreparaComando(cSPName, aListaParametri)
            _LDA.CreaParametri()
            _LDA.EseguiSP()
        Catch ex As Exception
            _Errore = ex
            If _ThrowError Then
                Throw ex
            End If
        End Try

    End Sub

    Public Sub ExecuteSP(ByVal cSPName As String, Optional ByVal aParams(,) As String = Nothing)
        Dim aListaParametri As ArrayList
        Dim i As Integer

        Try
            aListaParametri = New ArrayList
            If Not aParams Is Nothing Then
                For i = 0 To aParams.GetUpperBound(0)
                    aListaParametri.Add(New String() {aParams(i, 0), aParams(i, 1), aParams(i, 2), aParams(i, 3)})
                Next
            End If
            ExecuteSP(cSPName, aListaParametri)
        Catch ex As Exception
            _Errore = ex
        End Try

    End Sub

    Public Sub StartTransazione()

        _LDA.StartTransazione()

    End Sub

    Public Sub CommitTransazione()

        _LDA.CommitTransazione()
    End Sub

    Public Sub RollBackTransazione()

        _LDA.RollBackTransazione()

    End Sub

End Class