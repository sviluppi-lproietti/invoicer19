imports System.Data.OleDb

Public Class Layer_DataAccess_old

    Const noOperation = "n"
    Const delete_oper = "d"
    Const insert_oper = "i"
    Const select_oper = "s"
    Const update_oper = "u"

    Private _daDataAccess As OleDb.OleDbDataAdapter
    Private _dbConn As OleDb.OleDbConnection
    Private _DBConnectioinString As String
    Private _DatasetUsed As DataSet
    Private _TabellaUpdating As String

    Public Sub New(ByVal cDBConnectioinString As String, ByRef ds As DataSet)

        _DBConnectioinString = cDBConnectioinString
        _DatasetUsed = ds

    End Sub

    Public Sub New(ByVal dbConn As OleDb.OleDbConnection, ByRef ds As DataSet)

        _DBConn = dbConn
        _DatasetUsed = ds

    End Sub

    Public Sub CaricaDataSet(ByVal cTabella As String, Optional ByVal aParam(,) As String = Nothing)
        Dim cMapTable As String

        _daDataAccess = New OleDb.OleDbDataAdapter
        With _daDataAccess
            Try
                .SelectCommand = CreateSQLCommand(select_oper, _DatasetUsed, cTabella, aParam, "")
                cMapTable = "table" & .TableMappings.Count
                .TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping(cMapTable, cTabella)})
                .TableMappings(cMapTable).DataSetTable = cTabella
                .Fill(_DatasetUsed, cMapTable)
                _DatasetUsed.Tables(cTabella).AcceptChanges()
            Catch ex As Exception
                Throw ex
            Finally
                If _dbConn Is Nothing Then
                    .SelectCommand.Connection.Close()
                End If
            End Try
        End With

    End Sub

    Public Sub CaricadatasetC(ByVal cCommandText As String)
        Dim cMapTable As String

        _daDataAccess = New OleDb.OleDbDataAdapter
        With _daDataAccess
            Try
                .SelectCommand = CreateSQLCommand(select_oper, _DatasetUsed, "", Nothing, cCommandText)
                .SelectCommand.Connection = GetDbConnection()
                If _dbConn Is Nothing Then
                    .SelectCommand.Connection = New OleDb.OleDbConnection(_DBConnectioinString)
                Else
                    .SelectCommand.Connection = _dbConn
                End If
                cMapTable = "table" & .TableMappings.Count
                '.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping(cMapTable, cTabella)})
                '.TableMappings(cMapTable).DataSetTable = cTabella
                .Fill(_DatasetUsed, cMapTable)
                '_DatasetUsed.Tables(cTabella).AcceptChanges()
            Catch ex As Exception
                Throw ex
            Finally
                If _dbConn Is Nothing Then
                    .SelectCommand.Connection.Close()
                End If
            End Try
        End With

    End Sub

    Public Sub AggiornaDataSet(ByVal cTabella As String, Optional ByVal cCommandText As String = "", Optional ByVal aParam(,) As String = Nothing)
        Dim cTableDS As String
        Dim dbConn As OleDb.OleDbConnection

        _daDataAccess = New OleDb.OleDbDataAdapter
        If _dbConn Is Nothing Then
            dbConn = New OleDb.OleDbConnection(_DBConnectioinString)
        Else
            dbConn = _dbConn
        End If
        With _daDataAccess
            AddHandler .RowUpdated, AddressOf daDataAccess_RowUpdated
            If Not (_DatasetUsed.Tables(cTabella).GetChanges Is Nothing) Then
                For Each dr As DataRow In _DatasetUsed.Tables(cTabella).GetChanges.Rows
                    Select Case dr.RowState
                        Case DataRowState.Added
                            If .InsertCommand Is Nothing Then
                                .InsertCommand = SetUpCommand_DIU(insert_oper, _DatasetUsed.Tables(cTabella), aParam)
                                .InsertCommand.Connection = dbConn
                                _TabellaUpdating = cTabella
                            End If
                        Case DataRowState.Deleted
                            If .DeleteCommand Is Nothing Then
                                .DeleteCommand = SetUpCommand_DIU(delete_oper, _DatasetUsed.Tables(cTabella), aParam)
                                .DeleteCommand.Connection = dbConn
                            End If
                        Case DataRowState.Modified
                            If .UpdateCommand Is Nothing Then
                                .UpdateCommand = SetUpCommand_DIU(update_oper, _DatasetUsed.Tables(cTabella), aParam)
                                .UpdateCommand.Connection = dbConn
                            End If
                        Case Else
                    End Select
                Next
                Try
                    dbConn.Open()
                    cTableDS = "table" & _daDataAccess.TableMappings.Count
                    If Not _daDataAccess.TableMappings.Contains(cTabella) Then
                        _daDataAccess.TableMappings.AddRange(New System.Data.Common.DataTableMapping() _
                                                 {New System.Data.Common.DataTableMapping(cTableDS, cTabella)})
                    End If
                    _daDataAccess.TableMappings(cTableDS).DataSetTable = cTabella
                    _daDataAccess.Update(_DatasetUsed, cTabella)
                    _DatasetUsed.Tables(cTabella).AcceptChanges()
                Catch Err As Exception
                    Throw Err
                Finally
                    dbConn.Close()
                End Try
            End If
        End With

    End Sub

    Public Sub PulisciTabella(ByVal cTabella As String)

        _DatasetUsed.Tables(cTabella).Rows.Clear()

    End Sub

    Private Function CreateSQLCommand(ByVal cOper As String, ByVal ds As DataSet, ByVal cTabella As String, ByVal aParams(,) As String, ByRef cCommandString As String) As OleDbCommand
        Dim oleCMD As OleDbCommand
        Dim cFieldString As String
        Dim cValueString As String
        Dim cSubWhere As String
        Dim cSubSort As String
        Dim dc As DataColumn
        Dim cWhere As String
        Dim cSort As String
        Dim i As Integer

        oleCMD = New OleDbCommand
        oleCMD.Connection = GetDbConnection()
        If (cCommandString = "") Then
            cFieldString = ""
            cValueString = ""
            cWhere = ""
            cSort = ""
            For Each dc In ds.Tables.Item(cTabella).Columns
                If Not dc.ReadOnly And (cOper = insert_oper) Then
                    cFieldString = String.Concat(cFieldString, dc.ColumnName, ", ")
                    cValueString = String.Concat(cValueString, "?, ")
                ElseIf Not dc.ReadOnly And (cOper = update_oper) Then
                    cFieldString = String.Concat(cFieldString, dc.ColumnName, "= ?, ")
                ElseIf (cOper = select_oper) Then
                    cFieldString = String.Concat(cFieldString, dc.ColumnName, ", ")
                End If
            Next
            If cFieldString.EndsWith(", ") Then
                cFieldString = cFieldString.Substring(0, cFieldString.Length - 2)
            End If
            If cValueString.EndsWith(", ") Then
                cValueString = cValueString.Substring(0, cValueString.Length - 2)
            End If
            Select Case cOper
                Case Is = delete_oper
                    cCommandString = String.Concat("DELETE FROM ", cTabella, " ")
                Case Is = insert_oper
                    cCommandString = String.Concat("INSERT INTO ", cTabella, "(", cFieldString, ") VALUES (", cValueString, ")")
                Case Is = select_oper
                    cCommandString = String.Concat("SELECT ", cFieldString, " FROM ", cTabella)
                Case Is = update_oper
                    cCommandString = String.Concat("UPDATE ", cTabella, " SET ", cFieldString)
                Case Else
                    cCommandString = ""
            End Select
            If Not aParams Is Nothing Then
                For i = 0 To aParams.GetUpperBound(0)
                    cSubWhere = ""
                    cSubSort = ""
                    If aParams(i, 0) = "FEQ" Then
                        cSubWhere = String.Concat(aParams(i, 2), " = ?")
                    ElseIf aParams(i, 0) = "FGT" Then
                        cSubWhere = String.Concat(aParams(i, 2), " > ?")
                    ElseIf aParams(i, 0) = "FNE" Then
                        cSubWhere = String.Concat(aParams(i, 2), " <> ?")
                    ElseIf aParams(i, 0) = "FQR" Then
                        cSubWhere = aParams(i, 2)
                    ElseIf aParams(i, 0) = "FLK" Then
                        cSubWhere = String.Concat(aParams(i, 2), " LIKE ", aParams(i, 3))
                    ElseIf aParams(i, 0) = "FFE" Then
                        cSubWhere = String.Concat(aParams(i, 2), " = ", aParams(i, 3))
                    ElseIf aParams(i, 0) = "FEX" Then
                        cSubWhere = aParams(i, 2)
                    ElseIf aParams(i, 0) = "SRT" Then
                        cSubSort = aParams(i, 2)
                    End If
                    If cSubWhere > "" Then
                        cWhere = String.Concat(cWhere, "(", cSubWhere, ")")
                        Select Case aParams(i, 1)
                            Case Is = "A"
                                cWhere = String.Concat(cWhere, " AND ")
                            Case Is = "O"
                                cWhere = String.Concat(cWhere, " Or ")
                        End Select
                        If ds.Tables.Item(cTabella).Columns.IndexOf(aParams(i, 2)) > -1 Then
                            dc = ds.Tables.Item(cTabella).Columns(aParams(i, 2))
                            If Type.GetTypeCode(dc.DataType) = TypeCode.String Then
                                oleCMD.Parameters.Add(CreaParametro(String.Concat("Original_", oleCMD.Parameters.Count), OleDbType.VarChar, aParams(i, 3)))
                            ElseIf Type.GetTypeCode(dc.DataType) = TypeCode.Int32 Then
                                oleCMD.Parameters.Add(CreaParametro(String.Concat("Original_", oleCMD.Parameters.Count), OleDbType.Numeric, aParams(i, 3)))
                            End If
                        Else
                            Throw New Exception("Parametro non creabile")
                        End If
                    End If
                    If cSubSort > "" Then
                        If cSort = "" Then
                            cSort = cSubSort
                        Else
                            cSort = String.Concat(cSort, ", ", cSubSort)
                        End If
                    End If
                Next
            End If
            If cWhere > "" Then
                cCommandString = String.Concat(cCommandString, " WHERE ", cWhere)
            End If
            If cSort > "" Then
                cCommandString = String.Concat(cCommandString, " ORDER BY ", cSort)
            End If
        End If
        oleCMD.CommandText = cCommandString
        Return oleCMD

    End Function

    Private Function CreaParametro(ByVal cName As String, ByVal pType As System.Data.OleDb.OleDbType, ByVal cValue As Object) As OleDbParameter
        Dim pParam As OleDbParameter

        pParam = New OleDbParameter
        With pParam
            .ParameterName = cName
            .OleDbType = pType
            If Not (cValue Is Nothing) Then
                If cValue = "**" Then
                    .Value = " "
                Else
                    .Value = cValue
                End If
            End If
        End With
        Return pParam

    End Function

    Private Sub daDataAccess_RowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)    '(ByVal sender As Object, ByVal e As System.Data.OleDb.OleDbRowUpdatedEventArgs)
        'Dim cTabella As String = e.TableMapping.DataSetTable
        'Dim cProvider As String = ""
        Dim nValue As Integer
        Dim _Command As New OleDbCommand
        Dim dc As DataColumn

        If e.StatementType = StatementType.Insert And e.Status = UpdateStatus.Continue Then
            _Command.CommandText = "SELECT @@IDENTITY"
            _Command.Connection = _daDataAccess.InsertCommand.Connection
            nValue = _Command.ExecuteScalar()
            If _DatasetUsed.Tables(_TabellaUpdating).PrimaryKey.Length = 1 Then
                dc = CType(_DatasetUsed.Tables(_TabellaUpdating).PrimaryKey(0), DataColumn)
                dc.ReadOnly = False
                e.Row(dc.ColumnName) = nValue
                dc.ReadOnly = True
            End If
        End If

    End Sub

    Private Function SetUpCommand_DIU(ByVal cOper As String, ByVal dt As DataTable, ByVal aParams(,) As String) As OleDbCommand
        Dim dbCMD As New OleDbCommand
        Dim cCmdText As String = ""
        Dim cTemp_1 As String = ""
        Dim cParamName As String
        Dim pk() As DataColumn
        Dim dc As DataColumn
        Dim _Tabella As String

        _Tabella = dt.TableName
        'dbCMD.Connection = _DBConn
        Select Case cOper
            Case Is = delete_oper
                cCmdText = String.Concat("DELETE FROM ", _Tabella, " ")
            Case Is = insert_oper
                cCmdText = String.Concat("INSERT INTO ", _Tabella, "(")
            Case Is = update_oper
                cCmdText = String.Concat("UPDATE ", _Tabella, " SET ")
        End Select

        With dt
            If (cOper = insert_oper) Or (cOper = update_oper) Then
                For Each dc In .Columns
                    If Not dc.ReadOnly Then
                        cParamName = String.Concat("@", dc.ColumnName)
                        If cOper = update_oper Then
                            cCmdText = String.Concat(cCmdText, dc.ColumnName, " = ?, ")
                        Else
                            cCmdText = String.Concat(cCmdText, dc.ColumnName, ", ")
                            cTemp_1 = String.Concat(cTemp_1, cParamName, ", ")
                        End If
                        dbCMD.Parameters.Add(create_parameter(cParamName, dc.DataType, Nothing, Nothing))
                        dbCMD.Parameters(cParamName).SourceVersion = DataRowVersion.Current
                        dbCMD.Parameters(cParamName).SourceColumn = dc.ColumnName
                    End If
                Next
                cCmdText = cCmdText.Substring(0, cCmdText.Length - 2)
                If cOper = insert_oper Then
                    cTemp_1 = cTemp_1.Substring(0, cTemp_1.Length - 2)
                    cCmdText = String.Concat(cCmdText, ") VALUES (", cTemp_1, ")")
                End If
            End If

            dbCMD.CommandText = cCmdText
            If (cOper <> insert_oper) Then
                pk = dt.PrimaryKey
                If (pk.Length > 0) Then
                    'cParamName = String.Concat("@", pk(0).ColumnName)
                    cParamName = String.Concat(pk(0).ColumnName)
                    cCmdText = String.Concat(cCmdText, " WHERE ", pk(0).ColumnName, " = ?")
                    dbCMD.Parameters.Add(create_parameter(cParamName, pk(0).DataType, Nothing, Nothing))
                    If cOper = delete_oper Then
                        dbCMD.Parameters(cParamName).SourceVersion = DataRowVersion.Original
                    End If
                    If (cOper = insert_oper) Or (cOper = update_oper) Then
                        dbCMD.Parameters(cParamName).SourceVersion = DataRowVersion.Current
                    End If
                    dbCMD.Parameters(cParamName).SourceColumn = pk(0).ColumnName
                    dbCMD.CommandText = cCmdText
                Else
                    create_SQL_where(cOper, aParams, dbCMD)
                    create_SQL_parameter(dbCMD, dt, _Tabella, aParams)
                End If
            End If

        End With
        Return dbCMD

    End Function

    Private Sub create_SQL_where(ByRef cOper As String, ByVal aParams(,) As String, ByVal dbCMD As OleDbCommand)
        Dim cCmdText_1 As String = ""
        Dim cCmdText_2 As String = ""
        Dim cCmdText_3 As String = ""
        Dim cRowCount As String = ""
        Dim aWhere As String()
        Dim j As Integer

        If Not (aParams Is Nothing) Then
            cCmdText_1 = dbCMD.CommandText
            For j = 0 To aParams.GetUpperBound(0)
                aWhere = aParams(j, 0).ToUpper.Split(",")

                Select Case aWhere(0)
                    Case Is = "RC"
                        cRowCount = aParams(j, 1)
                    Case Is = "W"
                        Select Case aWhere(1)
                            Case Is = "EQ"
                                cCmdText_3 = String.Concat(aParams(j, 1), " = ?")
                            Case Is = "GT"
                                cCmdText_3 = String.Concat(aParams(j, 1), " > ?")
                            Case Is = "LT"
                                cCmdText_3 = String.Concat(aParams(j, 1), " < ?")
                            Case Is = "NE"
                                cCmdText_3 = String.Concat(aParams(j, 1), " <> ?")
                            Case Is = "QR", Is = "Q1", Is = "EX"
                                cCmdText_3 = aParams(j, 1)
                            Case Is = "LK"
                                cCmdText_3 = String.Concat(aParams(j, 1), " LIKE ", aParams(j, 2))
                            Case Is = "FE"
                                cCmdText_3 = String.Concat(aParams(j, 1), " = ", aParams(j, 2))
                        End Select
                        If aWhere(2).IndexOf("(") > -1 Then
                            cCmdText_2 = String.Concat(cCmdText_2, "(")
                        End If
                        cCmdText_2 = String.Concat(cCmdText_2, "(", cCmdText_3, ")")
                        Select Case aWhere(2)
                            Case Is = "A", "(A"
                                cCmdText_2 = cCmdText_2 & " AND "
                            Case Is = "O", "(O"
                                cCmdText_2 = cCmdText_2 & " OR "
                            Case Is = ")", "))", ")))"
                                cCmdText_2 = cCmdText_2 & aWhere(2) & " "
                        End Select
                End Select
            Next

            If cRowCount > "" Then
                cCmdText_1 = cCmdText_1.Replace("SELECT ", String.Concat("SELECT ", cRowCount.Trim, " "))
            End If
            If cCmdText_2 > "" Then
                cCmdText_1 = String.Concat(cCmdText_1, " WHERE ", cCmdText_2)
            End If

            dbCMD.CommandText = cCmdText_1
        End If

    End Sub

    Private Sub create_SQL_parameter(ByVal dbCMD As OleDbCommand, ByVal dt As DataTable, ByVal cTabella As String, ByVal aParams(,) As String)
        Dim aWhereNoParam As New ArrayList
        Dim aWhere As String()
        Dim dc As DataColumn
        Dim j As Integer

        ' Elenco delle condizioni che non prevedono la necessitÓ dei parametri
        aWhereNoParam.Add("FE")
        aWhereNoParam.Add("EX")
        aWhereNoParam.Add("LK")
        aWhereNoParam.Add("Q1")

        'dbCMD = get_oleDBCommand(cOper)

        'If (cOper = update_oper) Or (cOper = insert_oper) Then
        '    For Each dc In dt.Columns
        '        If Not dc.ReadOnly Then
        '            dbCMD.Parameters.Add(create_parameter(dc.ColumnName, dc.DataType, Nothing, Nothing))
        '        End If
        '    Next
        'End If

        If Not (aParams Is Nothing) Then
            For j = 0 To aParams.GetUpperBound(0)
                aWhere = aParams(j, 0).ToUpper.Split(",")

                If (aWhere(0) = "W") And (aWhereNoParam.IndexOf(aWhere(1)) = -1) Then
                    dc = dt.Columns(aParams(j, 1))
                    dbCMD.Parameters.Add(create_parameter(dc.ColumnName, dc.DataType, dc.MaxLength, aParams(j, 2)))
                End If
            Next
        End If

    End Sub

    Private Function create_parameter(ByVal cName As String, ByVal pType As System.Type, ByVal nSize As Integer, ByVal cValue As String) As System.Data.OleDb.OleDbParameter
        Dim pParam As New System.Data.OleDb.OleDbParameter

        With pParam
            .ParameterName = cName
            Select Case LCase(pType.Name)
                Case Is = "integer", "int32"
                    .OleDbType = OleDbType.Integer
                Case Is = "string"
                    .OleDbType = OleDbType.VarWChar
                Case Is = "datetime"
                    .OleDbType = OleDbType.DBTimeStamp
                Case Is = "boolean"
                    .OleDbType = OleDbType.Boolean
                Case Is = "double"
                    .OleDbType = OleDbType.Double
                Case Is = "binary"
                    .OleDbType = OleDbType.Binary
            End Select

            If nSize > 0 Then
                .Size = nSize
            End If
            .SourceColumn = cName
            If cValue = "--" Then
                pParam.SourceVersion = DataRowVersion.Original
            Else
                If Not (cValue Is Nothing) Then
                    .Value = cValue
                Else
                    .Value = DBNull.Value
                End If
            End If
        End With
        Return pParam

    End Function

    Private Function GetDbConnection() As OleDbConnection
        Dim dbconn As OleDbConnection

        If _dbConn Is Nothing Then
            dbconn = New OleDb.OleDbConnection(_DBConnectioinString)
        Else
            dbconn = _dbConn
        End If
        Return dbconn

    End Function

End Class