Public MustInherit Class LDA_BaseClass

    Public _TabellaUpdating As String
    Public _TroppiRecord As Boolean

    Private _DBConnectioinString As String
    Private _DatasetUsed As DataSet
    Private _DeleteOperationKO As Boolean
    Private _LeaveConnOpen As Boolean
    Private _ListaComandi As ArrayList
    Private _ListaParametri As ArrayList
    Private _CmdTimeout As Integer

    Enum TipoOperDS
        TODS_NotDef = -1
        TODS_nooper = 0
        TODS_delete = 1
        TODS_insert = 2
        TODS_select = 3
        TODS_update = 4
        TODS_storpr = 5
    End Enum

    Public Property CmdTimeout() As Integer

        Get
            Return _CmdTimeout
        End Get
        Set(ByVal value As Integer)
            _CmdTimeout = value
        End Set

    End Property


    Public WriteOnly Property LeaveConnOpen() As Boolean

        Set(ByVal value As Boolean)
            _LeaveConnOpen = value
        End Set

    End Property

    Public Sub New(Optional ByVal cDBConnectioinString As String = "")

        DBConnectioinString = cDBConnectioinString

    End Sub

    Public Sub New(ByVal cDBConnectioinString As String, ByRef ds As DataSet)

        DBConnectioinString = cDBConnectioinString
        _DatasetUsed = ds

    End Sub

    Public Property DBConnectioinString() As String

        Get
            Return _DBConnectioinString
        End Get
        Set(ByVal value As String)
            _DBConnectioinString = value
        End Set

    End Property

    Public Property DatasetUsed() As DataSet

        Get
            Return _DatasetUsed
        End Get
        Set(ByVal value As DataSet)
            _DatasetUsed = value
        End Set

    End Property

    Public Property DeleteOperationKO() As Boolean

        Get
            Return _DeleteOperationKO
        End Get
        Set(ByVal value As Boolean)
            _DeleteOperationKO = value
        End Set

    End Property

    Public Property ListaComandi() As ArrayList

        Get
            Return _ListaComandi
        End Get
        Set(ByVal value As ArrayList)
            _ListaComandi = value
        End Set

    End Property

    Public Property ListaParametri() As ArrayList

        Get
            Return _ListaParametri
        End Get
        Set(ByVal value As ArrayList)
            _ListaParametri = value
        End Set

    End Property

    Public ReadOnly Property TroppiRecord() As Boolean

        Get
            Return _TroppiRecord
        End Get

    End Property

    Public MustOverride Sub PreparaComando(ByVal cTabella As String, Optional ByVal aListaParam As ArrayList = Nothing)

    Public MustOverride Sub CreaParametri()

    Public MustOverride Function NomeParametriCMD(ByVal cNomeColonna As String) As String

    Public MustOverride Sub EseguiCaricamento(ByVal cTabella As String, ByVal nMaxRecordToLoad As Integer)

    Public MustOverride Sub EseguiAggiornamento(ByVal cTabella As String)

    Public MustOverride Sub EseguiSP()

    Public MustOverride Function ExecuteScalarS(ByVal cSql As String) As Object

    Public MustOverride Sub ExecuteScalarU(ByVal cSql As String)

    Public Sub PulisciTabella(ByVal cTabella As String)

        _DatasetUsed.Tables(cTabella).Rows.Clear()

    End Sub

    Public Function CreateSQLCommand(ByVal tCmd As TipoOperDS, ByVal ds As DataSet, ByVal cTabella As String, ByVal aListaParametri As ArrayList, ByRef cCommandString As String) As String
        Dim cFieldString As String
        Dim cValueString As String
        Dim lParamNeed As Boolean
        Dim cSubWhere As String
        Dim cSubSort As String
        Dim dc As DataColumn
        Dim cWhere As String
        Dim cSort As String
        Dim cParamName As String

        If tCmd = TipoOperDS.TODS_insert Then
            _TabellaUpdating = cTabella
        End If
        If (cCommandString = "") Then
            cFieldString = ""
            cValueString = ""
            cWhere = ""
            cSort = ""
            For Each dc In ds.Tables.Item(cTabella).Columns
                If (tCmd = TipoOperDS.TODS_delete) Then
                    If Array.IndexOf(ds.Tables(cTabella).PrimaryKey, dc) > -1 Then
                        aListaParametri.Add(New String() {"FEQ", "", dc.ColumnName, Nothing})
                    End If
                ElseIf (tCmd = TipoOperDS.TODS_insert) Then
                    If Not dc.ReadOnly Then
                        cFieldString = String.Concat(cFieldString, dc.ColumnName, ", ")
                        cValueString = String.Concat(cValueString, NomeParametriCMD(dc.ColumnName), ", ")
                        ListaParametri.Add(New String() {tCmd, NomeParametriCMD(dc.ColumnName), Type.GetTypeCode(dc.DataType).ToString, dc.ColumnName, "", Nothing})
                    End If
                ElseIf (tCmd = TipoOperDS.TODS_select) Then
                    cFieldString = String.Concat(cFieldString, dc.ColumnName, ", ")
                ElseIf (tCmd = TipoOperDS.TODS_update) Then
                    If Not dc.ReadOnly Then
                        cFieldString = String.Concat(cFieldString, dc.ColumnName, "=", NomeParametriCMD(dc.ColumnName), ", ")
                        ListaParametri.Add(New String() {tCmd, NomeParametriCMD(dc.ColumnName), Type.GetTypeCode(dc.DataType).ToString, dc.ColumnName, "", Nothing})
                    End If
                    If Array.IndexOf(ds.Tables(cTabella).PrimaryKey, dc) > -1 Then
                        aListaParametri.Add(New String() {"FEQ", "", dc.ColumnName, Nothing})
                    End If
                End If
            Next
            If cFieldString.EndsWith(", ") Then
                cFieldString = cFieldString.Substring(0, cFieldString.Length - 2)
            End If
            If cValueString.EndsWith(", ") Then
                cValueString = cValueString.Substring(0, cValueString.Length - 2)
            End If
            Select Case tCmd
                Case Is = TipoOperDS.TODS_delete
                    cCommandString = String.Concat("DELETE FROM ", cTabella, " ")
                Case Is = TipoOperDS.TODS_insert
                    cCommandString = String.Concat("INSERT INTO ", cTabella, "(", cFieldString, ") VALUES (", cValueString, ")")
                Case Is = TipoOperDS.TODS_select
                    cCommandString = String.Concat("SELECT ", cFieldString, " FROM ", cTabella)
                Case Is = TipoOperDS.TODS_update
                    cCommandString = String.Concat("UPDATE ", cTabella, " SET ", cFieldString)
                Case Else
                    cCommandString = ""
            End Select

            For Each aParam As String() In aListaParametri
                cSubWhere = ""
                cSubSort = ""
                lParamNeed = True
                cParamName = CreaNomeParametro(NomeParametriCMD(aParam(2)))

                If aParam(0) = "FEQ" Then
                    cSubWhere = String.Concat(aParam(2), " = ", cParamName)
                ElseIf aParam(0) = "FGT" Then
                    cSubWhere = String.Concat(aParam(2), " > ", cParamName)
                ElseIf aParam(0) = "FNL" Then
                    cSubWhere = String.Concat(aParam(2), " >= ", cParamName)
                ElseIf aParam(0) = "FLT" Then
                    cSubWhere = String.Concat(aParam(2), " < ", cParamName)
                ElseIf aParam(0) = "FNG" Then
                    cSubWhere = String.Concat(aParam(2), " <= ", cParamName)
                ElseIf aParam(0) = "FNE" Then
                    cSubWhere = String.Concat(aParam(2), " <> ", cParamName)
                ElseIf aParam(0) = "FQR" Then
                    cSubWhere = aParam(2)
                    lParamNeed = False
                ElseIf aParam(0) = "FLK" Then
                    cSubWhere = String.Concat(aParam(2), " LIKE '", aParam(3), "'")
                    lParamNeed = False
                ElseIf aParam(0) = "FFE" Then
                    cSubWhere = String.Concat(aParam(2), " = ", aParam(3))
                    lParamNeed = False
                ElseIf aParam(0) = "FEX" Then
                    cSubWhere = aParam(2)
                    lParamNeed = False
                ElseIf aParam(0) = "SRT" Then
                    cSubSort = aParam(2)
                End If
                If cSubWhere > "" Then
                    cWhere = String.Concat(cWhere, "(", cSubWhere, ")")
                    Select Case aParam(1)
                        Case Is = "A"
                            cWhere = String.Concat(cWhere, " AND ")
                        Case Is = "O"
                            cWhere = String.Concat(cWhere, " Or ")
                    End Select
                    If (ds.Tables.Item(cTabella).Columns.IndexOf(aParam(2)) > -1) And lParamNeed Then
                        dc = ds.Tables.Item(cTabella).Columns(aParam(2))
                        ListaParametri.Add(New String() {tCmd, cParamName, Type.GetTypeCode(dc.DataType).ToString, dc.ColumnName, "O", aParam(3)})
                    ElseIf Not lParamNeed Then

                    Else
                        Throw New Exception(String.Concat("Parametro non creabile: colonna ", aParam(2)))
                    End If
                End If
                If cSubSort > "" Then
                    If cSort = "" Then
                        cSort = cSubSort
                    Else
                        cSort = String.Concat(cSort, ", ", cSubSort)
                    End If
                End If
            Next

            If cWhere > "" Then
                cCommandString = String.Concat(cCommandString, " WHERE ", cWhere)
            End If
            If cSort > "" Then
                cCommandString = String.Concat(cCommandString, " ORDER BY ", cSort)
            End If
        End If
        Return cCommandString

    End Function

    Public Sub AggiornaRecord(ByVal dr As DataRow, ByVal nValue As Integer)
        Dim dc As DataColumn

        If DatasetUsed.Tables(_TabellaUpdating).PrimaryKey.Length = 1 Then
            dc = CType(DatasetUsed.Tables(_TabellaUpdating).PrimaryKey(0), DataColumn)
            dc.ReadOnly = False
            dr(dc.ColumnName) = nValue
            dc.ReadOnly = True
        End If

    End Sub

    Public Function CreaNomeParametro(ByVal cParamProp As String) As String
        Dim cParamTmp As String
        Dim lFound As Boolean
        Dim i As Integer

        i = 0
        lFound = True
        cParamTmp = cParamProp
        While lFound
            If i = 0 Then
                cParamTmp = cParamProp
            Else
                cParamTmp = String.Concat(cParamProp, i)
            End If
            lFound = False
            For Each aValue As String() In ListaParametri
                lFound = lFound Or (aValue(1) = cParamTmp)
            Next
            i += 1
        End While
        Return cParamTmp

    End Function

    Public MustOverride Sub StartTransazione()
    Public MustOverride Sub CommitTransazione()
    Public MustOverride Sub RollBackTransazione()

End Class