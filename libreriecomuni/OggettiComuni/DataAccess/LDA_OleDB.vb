imports System.Data.OleDb

Public Class LDA_OleDB
    Inherits LDA_BaseClass

    Private _daDataAccess As OleDb.OleDbDataAdapter
    Private _dbConn As OleDb.OleDbConnection

    Public Sub New(Optional ByVal cDBConnectioinString As String = "")

        MyBase.New(cDBConnectioinString)

    End Sub

    Public Sub New(ByVal cDBConnectioinString As String, ByRef ds As DataSet)

        MyBase.New(cDBConnectioinString, ds)

    End Sub

    Public Sub New(ByVal dbConn As OleDb.OleDbConnection, ByRef ds As DataSet)

        _DBConn = dbConn
        DatasetUsed = ds

    End Sub

    Public Overrides Sub EseguiSP()

    End Sub

    Public Overrides Sub PreparaComando(ByVal cTabella As String, Optional ByVal aListaParam As ArrayList = Nothing)

        _daDataAccess = New OleDb.OleDbDataAdapter
        For Each tCmd As TipoOperDS In ListaComandi
            If tCmd = TipoOperDS.TODS_delete Then
                _daDataAccess.DeleteCommand = BuildCommand()
                _daDataAccess.DeleteCommand.CommandText = CreateSQLCommand(TipoOperDS.TODS_delete, DatasetUsed, cTabella, aListaParam, "")
            End If
            If tCmd = TipoOperDS.TODS_insert Then
                _daDataAccess.InsertCommand = BuildCommand()
                _daDataAccess.InsertCommand.CommandText = CreateSQLCommand(TipoOperDS.TODS_insert, DatasetUsed, cTabella, aListaParam, "")
            End If
            If tCmd = TipoOperDS.TODS_select Then
                _daDataAccess.SelectCommand = BuildCommand()
                _daDataAccess.SelectCommand.CommandText = CreateSQLCommand(TipoOperDS.TODS_select, DatasetUsed, cTabella, aListaParam, "")
            End If
            If tCmd = TipoOperDS.TODS_update Then
                _daDataAccess.UpdateCommand = BuildCommand()
                _daDataAccess.UpdateCommand.CommandText = CreateSQLCommand(TipoOperDS.TODS_update, DatasetUsed, cTabella, aListaParam, "")
            End If
        Next

    End Sub

    Public Overrides Sub CreaParametri()
        Dim aValue As String()

        For Each aValue In ListaParametri
            Select Case aValue(0)
                Case Is = TipoOperDS.TODS_delete
                    _daDataAccess.DeleteCommand.Parameters.Add(CreaParametro(aValue(1), aValue(2), aValue(3)))
                Case Is = TipoOperDS.TODS_insert
                    _daDataAccess.InsertCommand.Parameters.Add(CreaParametro(aValue(1), aValue(2), aValue(3)))
                Case Is = TipoOperDS.TODS_select
                    _daDataAccess.SelectCommand.Parameters.Add(CreaParametro(aValue(1), aValue(2), aValue(3)))
                Case Is = TipoOperDS.TODS_update
                    _daDataAccess.UpdateCommand.Parameters.Add(CreaParametro(aValue(1), aValue(2), aValue(3)))
            End Select
        Next

    End Sub

    Public Overrides Sub EseguiCaricamento(ByVal cTabella As String, ByVal nMaxRecordToLoad As Integer)
        Dim cMapTable As String

        With _daDataAccess
            Try
                _TroppiRecord = False
                cMapTable = "table" & .TableMappings.Count
                .TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping(cMapTable, cTabella)})
                .TableMappings(cMapTable).DataSetTable = cTabella
                .Fill(DatasetUsed, cMapTable)
                DatasetUsed.Tables(cTabella).AcceptChanges()
            Catch ex As Exception
                Throw ex
            Finally
                If _dbConn Is Nothing Then
                    .SelectCommand.Connection.Close()
                End If
            End Try
        End With

    End Sub

    Public Overrides Sub EseguiAggiornamento(ByVal cTabella As String)
        Dim cTableDS As String

        AddHandler _daDataAccess.RowUpdated, AddressOf daDataAccess_RowUpdated
        Try
            _dbConn.Open()
            cTableDS = "table" & _daDataAccess.TableMappings.Count
            If Not _daDataAccess.TableMappings.Contains(cTabella) Then
                _daDataAccess.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping(cTableDS, cTabella)})
            End If
            _daDataAccess.TableMappings(cTableDS).DataSetTable = cTabella
            _daDataAccess.Update(DatasetUsed, cTabella)
            DatasetUsed.Tables(cTabella).AcceptChanges()
        Catch Err As Exception
            Throw Err
        Finally
            _dbConn.Close()
        End Try

    End Sub

    Public Overrides Function NomeParametriCMD(ByVal cNomeColonna As String) As String

        Return "?"

    End Function

    Private Function CreaParametro(ByVal cNomeParam As String, ByVal cTipoParam As String, ByVal cValoreParam As Object) As OleDbParameter
        Dim pParam As OleDbParameter

        pParam = New OleDbParameter
        With pParam
            .ParameterName = cNomeParam
            If cTipoParam.ToLower = "string" Then
                .OleDbType = OleDbType.VarChar
            ElseIf cTipoParam.ToLower = "int32" Then
                .OleDbType = OleDbType.Numeric
            End If
            If (cValoreParam Is Nothing) Then
                .SourceColumn = cNomeParam
            Else
                .Value = cValoreParam
            End If
        End With
        Return pParam

    End Function

    Private Function BuildCommand() As OleDbCommand
        Dim oleCMD As OleDbCommand

        oleCMD = New OleDbCommand
        If _dbConn Is Nothing Then
            _dbConn = New OleDb.OleDbConnection(DBConnectioinString)
        End If
        oleCMD.Connection = _dbConn
        Return oleCMD

    End Function

    Private Sub daDataAccess_RowUpdated(ByVal sender As Object, ByVal e As OleDbRowUpdatedEventArgs)
        Dim _Command As New OleDbCommand
        Dim nValue As Integer

        If e.StatementType = StatementType.Insert And e.Status = UpdateStatus.Continue Then
            _Command.CommandText = "SELECT @@IDENTITY"
            _Command.Connection = _daDataAccess.InsertCommand.Connection
            nValue = _Command.ExecuteScalar()
            AggiornaRecord(e.Row, nValue)
        End If

    End Sub

    Public Overrides Function ExecuteScalarS(ByVal cSql As String) As Object
        Dim da As New OleDbDataAdapter
        Dim _AlreadyOpened As Boolean
        Dim oRtn As Object

        Try
            da.SelectCommand = SetUpCommand_ES(cSql)
            _AlreadyOpened = (_dbConn.State = ConnectionState.Open)
            If Not _AlreadyOpened Then
                _dbConn.Open()
            End If

            oRtn = da.SelectCommand.ExecuteScalar()
        Catch ex As Exception
            oRtn = -1
            Throw ex
        Finally
            If Not _AlreadyOpened Then
                _dbConn.Close()
            End If
        End Try
        Return oRtn

    End Function

    Public Overrides Sub ExecuteScalarU(ByVal cSql As String)

    End Sub

    Public Overrides Sub StartTransazione()

    End Sub

    Public Overrides Sub CommitTransazione()

    End Sub

    Public Overrides Sub RollBackTransazione()

    End Sub

    Private Function SetUpCommand_ES(ByVal cQueryName As String) As OleDbCommand
        Dim dbCMD As New OleDbCommand

        dbCMD.Connection = _dbConn
        dbCMD.CommandText = cQueryName
        Return dbCMD

    End Function

End Class