Imports System.Globalization
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text

Public Class OGC_utilita

    Public Shared Function FormattaOrario(ByVal tmr_elapsed As TimeSpan, Optional ByVal cSeparator As String = ":") As String

        Return String.Concat(tmr_elapsed.Hours.ToString.PadLeft(2, "0"), cSeparator, _
                             tmr_elapsed.Minutes.ToString.PadLeft(2, "0"), cSeparator, _
                             tmr_elapsed.Seconds.ToString.PadLeft(2, "0"))

    End Function

    Public Shared Function FormattaDataOra(ByVal dDateOra As DateTime) As String

        Return String.Concat(Now.Year.ToString.PadLeft(4, "0"), Now.Month.ToString.PadLeft(2, "0"), Now.Day.ToString.PadLeft(2, "0"), "_", _
                             Now.Hour.ToString.PadLeft(2, "0"), Now.Minute.ToString.PadLeft(2, "0"), Now.Second.ToString.PadLeft(2, "0"), Now.Millisecond.ToString.PadLeft(3, "0"))

    End Function

    Public Shared Function TrasformaSiNo(ByVal lSwitch As Boolean) As String
        Dim cRtn As String

        If lSwitch Then
            cRtn = "Si"
        Else
            cRtn = "No"
        End If
        Return cRtn

    End Function

    Public Shared Function TrasformaMese(ByVal nMese As Integer) As String
        Dim cvalue As String

        Select Case nMese
            Case Is = 1
                cvalue = "gennaio"
            Case Is = 2
                cvalue = "febbraio"
            Case Is = 3
                cvalue = "marzo"
            Case Is = 4
                cvalue = "aprile"
            Case Is = 5
                cvalue = "maggio"
            Case Is = 6
                cvalue = "giugno"
            Case Is = 7
                cvalue = "luglio"
            Case Is = 8
                cvalue = "agosto"
            Case Is = 9
                cvalue = "settembre"
            Case Is = 10
                cvalue = "ottobre"
            Case Is = 11
                cvalue = "novembre"
            Case Is = 12
                cvalue = "dicembre"
            Case Else
                cvalue = ""
        End Select
        Return cvalue

    End Function

    Public Shared Function ConcatenaFolderValueWeb(ByVal ParamArray aFolderLst As String()) As String
        Dim cFolder As String

        cFolder = ConcatenaFolderValue(aFolderLst)
        While cFolder.Contains("/\") Or cFolder.Contains("\")
            cFolder = cFolder.Replace("/\", "/").Replace("\", "/")
        End While
        Return cFolder

    End Function

    Public Shared Function ConcatenaFolderFileValueWeb(ByVal ParamArray aFolderLst As String()) As String
        Dim cFolder As String

        cFolder = ConcatenaFolderFileValue(aFolderLst)
        While cFolder.Contains("/\") Or cFolder.Contains("\")
            cFolder = cFolder.Replace("/\", "/").Replace("\", "/")
        End While
        Return cFolder

    End Function

    Public Shared Function ConcatenaFolderValue(ByVal ParamArray aFolderLst As String()) As String
        Dim cFolder As String
        Dim cTmp As String

        cFolder = ""
        For Each cTmp In aFolderLst
            If cFolder = "" Then
                cFolder = String.Concat(cTmp, "\")
            Else
                cFolder = String.Concat(cFolder, cTmp, "\")
            End If
        Next
        While cFolder.Contains("\\")
            cFolder = cFolder.Replace("\\", "\")
        End While
        Return cFolder

    End Function

    Public Shared Function ConcatenaFolderFileValue(ByVal ParamArray aFolderFileLst As String()) As String
        Dim i As Integer
        Dim cTmp As String

        cTmp = aFolderFileLst(0)
        For i = 1 To aFolderFileLst.Length - 2
            cTmp = ConcatenaFolderValue(cTmp, aFolderFileLst(i))
        Next
        cTmp = String.Concat(StandardFolderValue(cTmp), aFolderFileLst(aFolderFileLst.Length - 1))
        Return cTmp

    End Function

    Public Shared Function StandardFolderValue(ByVal cFolder As String, Optional ByVal lWebFLD As Boolean = False) As String
        Dim cWebSep As String

        If lWebFLD Then
            cWebSep = "/"
        Else
            cWebSep = "\"
        End If
        If Not cFolder.EndsWith(cWebSep) And cFolder.Length > 0 Then
            cFolder = String.Concat(cFolder, cWebSep)
        End If
        Return cFolder

    End Function

    Public Shared Function ValidaCheckSum(ByVal xdDoc As Xml.XmlDocument) As Boolean
        Dim xnCheckSum As Xml.XmlNode
        Dim lCorr As Boolean

        xnCheckSum = xdDoc.ChildNodes(0).SelectSingleNode("checksum")
        If xnCheckSum Is Nothing Then
            lCorr = False
        Else
            xdDoc.ChildNodes(0).RemoveChild(xnCheckSum)
            lCorr = (xnCheckSum.InnerText = HashString(xdDoc.OuterXml))
        End If
        Return lCorr

    End Function

    Public Shared Function GenerateHashPassword(ByVal cPassword As String) As String
        Dim cTmp As String = Replace("EsTIgarrIBiA", ":", "")

        If cPassword = "" Then
            cPassword = "Iniziale123"
        End If
        cPassword = Right(cTmp, 6) + cPassword + Left(cTmp, 6)
        Return HashString(cPassword)

    End Function

    Public Shared Function HashString(ByVal cText As String, Optional ByVal cHashType As String = "md5", Optional ByVal cFormat As String = "") As String
        Dim sha1 As SHA1CryptoServiceProvider
        Dim md5 As MD5CryptoServiceProvider
        Dim sha256 As SHA256Managed
        Dim bytHashValue As Byte()
        Dim hash As StringBuilder
        Dim cRtn As String

        bytHashValue = Nothing
        cHashType = cHashType.ToLower
        If cHashType = "md5" Then
            'genera il valore hash md5
            md5 = New MD5CryptoServiceProvider()
            md5.ComputeHash((New UnicodeEncoding).GetBytes(cText))
            bytHashValue = md5.Hash
        ElseIf cHashType = "sha1" Then
            'genera il valore hash SHA-1
            sha1 = New SHA1CryptoServiceProvider
            sha1.ComputeHash((New UnicodeEncoding).GetBytes(cText))
            bytHashValue = sha1.Hash
        ElseIf cHashType = "sha256" Then
            'genera il valore hash SHA-256
            sha256 = New SHA256Managed
            sha256.ComputeHash((New UnicodeEncoding).GetBytes(cText))
            bytHashValue = sha256.Hash
        End If

        If bytHashValue IsNot Nothing Then
            If cFormat = "" Then
                cRtn = BitConverter.ToString(bytHashValue)
            Else
                hash = New StringBuilder(bytHashValue.Length)
                For i As Integer = 0 To bytHashValue.Length - 1
                    hash.Append(bytHashValue(i).ToString(cFormat))
                Next
                cRtn = hash.ToString
            End If
        Else
            cRtn = ""
        End If
        Return cRtn

    End Function

    Public Shared Function HashFile(ByVal cFileToHash As String, Optional ByVal cHashType As String = "md5", Optional ByVal cFormat As String = "") As String
        Dim buffer() As Byte
        Dim fs As FileStream

        buffer = Nothing
        fs = New FileStream(cFileToHash, FileMode.Open, FileAccess.Read, FileShare.Read, 8192)
        cHashType = cHashType.ToLower
        If cHashType = "md5" Then
            'genera il valore hash md5
            Dim md5 As New MD5CryptoServiceProvider()
            md5.ComputeHash(fs)
            buffer = md5.Hash
        ElseIf cHashType = "sha1" Then
            'genera il valore hash SHA-1
            Dim sha1 As New SHA1CryptoServiceProvider()
            sha1.ComputeHash(fs)
            buffer = sha1.Hash
        ElseIf cHashType = "sha256" Then
            'genera il valore hash SHA-256
            Dim sha256 As New SHA256Managed
            sha256.ComputeHash(fs)
            buffer = sha256.Hash
        End If

        'ricordarsi di chiudere lo stream del file
        fs.Close()

        'converto l'hash del file in una stringa
        Dim hash As New StringBuilder(buffer.Length)
        For i As Integer = 0 To buffer.Length - 1
            hash.Append(buffer(i).ToString("X2"))
        Next

        'la funziona ritorna l'hash del file in formato
        'stringa tutta minuscola
        Return hash.ToString.ToLower

    End Function

    Public Shared Function TrasfBoolean(ByVal lValue As Boolean, Optional ByVal cValue As String = "Si;No") As String
        Dim cRtn As String

        If lValue Then
            cRtn = cValue.Split(";")(0)
        Else
            cRtn = cValue.Split(";")(1)
        End If
        Return cRtn

    End Function

    ' ************************************************************************** '
    ' Questa funzione serve a leggere un file XML con la codifica WINDOWS-1252 e '
    ' a caricarlo in un XMLDocument.                                             '
    ' ************************************************************************** '
    Public Shared Function caricaFileDatiXML(ByVal cFileName As String) As Xml.XmlDocument
        Dim myEncoding As System.Text.Encoding
        Dim sr_xml As System.IO.StreamReader
        Dim fil_xml As Xml.XmlDocument

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)    ' Windows-1252
            sr_xml = New System.IO.StreamReader(cFileName, myEncoding)
            fil_xml = New Xml.XmlDocument
            fil_xml.Load(sr_xml)
            sr_xml.Close()
        Catch ex As Exception
            fil_xml = Nothing
            Throw ex
        End Try
        Return fil_xml

    End Function

    ' ************************************************************************** '
    ' Questa funzione serve a scrivere un file XML con la codifica WINDOWS-1252. '
    ' ************************************************************************** '
    Public Shared Sub SalvaFileDatiXML(ByVal cFileName As String, ByVal xdFile As Xml.XmlDocument)
        Dim myEncoding As System.Text.Encoding
        Dim sw_xml As System.IO.StreamWriter

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)
            sw_xml = New System.IO.StreamWriter(cFileName, False, myEncoding)
            xdFile.Save(sw_xml)
            sw_xml.Close()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Shared Function CleanFolderName(ByVal cFolder As String) As String

        While cFolder.IndexOf("  ") > -1
            cFolder = cFolder.Replace("  ", " ")
        End While
        Return cFolder

    End Function

    'Public Shared Sub SafeXMLSaveFileName(ByVal xdFile As Xml.XmlDocument, ByVal cFileName As String)
    '    Dim cTmpFileName As String

    '    Try
    '        cTmpFileName = String.Concat(StandardFolderValue(System.IO.Path.GetDirectoryName(cFileName)), Timestamp)
    '        xdFile.Save(cTmpFileName)
    '        System.IO.File.Copy(cTmpFileName, cFileName, True)
    '        System.IO.File.Delete(cTmpFileName)
    '    Catch ex As Exception
    '        Throw New Exception("(SafeXMLSaveFileName) Errore salvando il file XML.", ex)
    '    End Try

    'End Sub

    Public Shared Function Timestamp() As String

        Return Now.ToString("yyyyMMddHHmmssfff")

    End Function

    Public Shared Function Timestamp(ByVal dDate As DateTime) As String

        Return dDate.ToString("yyyyMMddhhmmssfff")

    End Function

    Public Shared Function SerializeDataset(ByVal ds As DataSet, ByVal cFolder As String, Optional ByVal cPrefixSer As String = "") As String
        Dim ser As Xml.Serialization.XmlSerializer
        Dim cFileName As String
        Dim fs As IO.FileStream

        Try
            cFileName = String.Concat(StandardFolderValue(cFolder), cPrefixSer, Timestamp, ".xml")
            fs = New IO.FileStream(cFileName, IO.FileMode.Create)
            ser = New Xml.Serialization.XmlSerializer(GetType(DataSet))
            ser.Serialize(fs, ds)
            fs.Close()
        Catch ex As Exception
            cFileName = ""
        End Try
        Return cFileName

    End Function

    Public Shared Function DeserializeDataset(ByVal cFileName As String) As DataSet
        Dim ser As Xml.Serialization.XmlSerializer
        Dim sr As IO.FileStream
        Dim ds As DataSet

        Try
            ds = New DataSet
            ser = New Xml.Serialization.XmlSerializer(GetType(DataSet))
            sr = New IO.FileStream(cFileName, IO.FileMode.Open)
            ds = ser.Deserialize(sr)
            sr.Close()
        Catch ex As Exception
            ds = Nothing
        End Try
        Return ds

    End Function

    Public Shared Function ReadExcelIntoDataset(ByVal xlConnStrs As Dictionary(Of String, String), ByVal cFileName As String, Optional ByVal cNomeFoglio As String = "", Optional ByVal dsTiped As DataSet = Nothing, Optional ByVal cTable As String = "") As Data.DataSet
        Dim cConnStrPref As String
        Dim dati As DataSet

        Try
            If System.IO.Path.GetExtension(cFileName).ToLower = ".xls" Then
                cConnStrPref = xlConnStrs("xls")
            ElseIf System.IO.Path.GetExtension(cFileName).ToLower = ".xlsx" Then
                cConnStrPref = xlConnStrs("xlsx")
            Else
                Throw New Exception("Formato excel non riconosciuto")
            End If
            dati = ReadExcelIntoDataset(cConnStrPref, cFileName, cNomeFoglio, dsTiped, cTable)
        Catch ex As Exception
            dati = Nothing
            Throw ex
        End Try
        Return dati

    End Function

    Public Shared Function ReadExcelIntoDataset(ByVal cConnStrPref As String, ByVal cFileName As String, Optional ByVal cNomeFoglio As String = "", Optional ByVal dsTiped As DataSet = Nothing, Optional ByVal cTable As String = "") As Data.DataSet
        Dim cmdExcel As Data.OleDb.OleDbDataAdapter
        Dim cnExcel As Data.OleDb.OleDbConnection
        Dim dsExcel As Data.DataSet
        Dim schemaTbl As DataTable
        Dim cCommandText As String

        dsExcel = Nothing
        cnExcel = Nothing
        Try
            If (System.IO.File.Exists(cFileName)) Then
                cnExcel = New System.Data.OleDb.OleDbConnection(String.Concat(cConnStrPref, "Data Source=" + cFileName + ";Extended Properties=""Excel 8.0;"""))
                cnExcel.Open()

                ' ************************************************************************** '
                ' Recupero il nome del 1� foglio contenuto nella cartella Excel.             '
                ' ************************************************************************** '
                If cNomeFoglio = "" Then
                    schemaTbl = cnExcel.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, Nothing)
                    If (schemaTbl IsNot Nothing) Then
                        cNomeFoglio = ""
                        For Each dr As DataRow In schemaTbl.Rows
                            If dr("TABLE_NAME").ToString.EndsWith("$") Or dr("TABLE_NAME").ToString.EndsWith("$'") Then
                                cNomeFoglio = dr("TABLE_NAME")
                            End If
                        Next
                    Else
                        Throw New InvalidOperationException("Impossibile recuperare il primo foglio.")
                    End If
                End If
                If Not (cNomeFoglio.EndsWith("$") Or cNomeFoglio.EndsWith("$'")) Then
                    cNomeFoglio = String.Concat(cNomeFoglio, "$")
                End If

                cCommandText = String.Concat("SELECT * FROM [", cNomeFoglio, "]")
                cmdExcel = New Data.OleDb.OleDbDataAdapter(cCommandText, cnExcel)
                Try
                    If dsTiped Is Nothing Then
                        dsExcel = New Data.DataSet
                    Else
                        dsExcel = dsTiped
                    End If
                    If cTable > "" Then
                        cmdExcel.Fill(dsExcel, cTable)
                    Else
                        cmdExcel.Fill(dsExcel)
                    End If
                Catch ex As Exception
                    dsExcel = Nothing
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If cnExcel IsNot Nothing Then
                If cnExcel.State = ConnectionState.Open Then
                    cnExcel.Close()
                End If
            End If
        End Try
        Return dsExcel

    End Function

    Public Shared Function FileNameUnivoco(ByVal cFileName As String) As String
        Dim cFolder As String
        Dim cEstenz As String
        Dim nFile As Integer
        Dim cFile As String

        cFolder = System.IO.Path.GetDirectoryName(cFileName)
        cFile = System.IO.Path.GetFileName(cFileName)
        If cFile.Substring(4, 1) = "_" And cFile.Substring(7, 1) = "_" And cFile.Substring(12, 1) = "_" And cFile.Substring(17, 1) = "_" Then
            cFile = cFile.Insert(13, "00").Insert(8, "00")
        End If
        cFileName = String.Concat(cFolder, "\", cFile)
        If System.IO.File.Exists(cFileName) Then
            If cFile.IndexOf(".") > -1 Then
                cEstenz = cFile.Substring(cFile.LastIndexOf("."))
                cFile = cFile.Substring(0, cFile.LastIndexOf("."))
            Else
                cEstenz = ""
            End If
            nFile = 1
            While System.IO.File.Exists(cFileName)
                nFile += 1
                cFileName = String.Concat(cFolder, "\", cFile, " (", nFile, ")", cEstenz).Replace(" ", "_")
            End While
        End If
        Return cFileName

    End Function

    Public Shared Function FolderNameUnivoco(ByVal cFolderName As String) As String
        Dim cFolder As String
        Dim nFolder As Integer
        Dim cFile As String

        If System.IO.Directory.Exists(cFolderName) Then
            If cFolderName.EndsWith("\") Then
                cFolderName = cFolderName.Substring(0, cFolderName.Length - 1)
            End If
            cFolder = System.IO.Path.GetDirectoryName(cFolderName)
            cFile = System.IO.Path.GetFileName(cFolderName)
            nFolder = 1
            While System.IO.Directory.Exists(cFolderName)
                nFolder += 1
                cFolderName = String.Concat(cFolder, "\", cFile, " (", nFolder, ")").Replace(" ", "_")
            End While
        End If
        Return StandardFolderValue(cFolderName)

    End Function

    Public Shared Function FolderNameStep1(ByVal cFolder As String, Optional ByVal cForce As String = "") As String
        Dim nm As CLS_NameManagement

        nm = New CLS_NameManagement("fld", cFolder, Nothing)
        nm.EseguiStep()
        cFolder = nm.NomeElaborato
        nm = Nothing

        Return cFolder

    End Function

    Public Shared Function GetDateUtente(ByVal cTmp As String) As DateTime
        Dim dTmp As DateTime

        If IsDate(cTmp) Then
            dTmp = cTmp
        Else
            cTmp = cTmp.Replace(".", ":")
            If Not Date.TryParse(cTmp, New CultureInfo("it-IT", False), DateTimeStyles.None, dTmp) Then
                If Not Date.TryParse(cTmp, New CultureInfo("en-US", False), DateTimeStyles.None, dTmp) Then

                End If
            End If
        End If
        Return dTmp

    End Function

    Public Shared Function LoadQuery(ByVal cQueryFileName As String) As String
        Dim sr As System.IO.StreamReader
        Dim cQuery As String

        cQuery = ""
        If System.IO.File.Exists(cQueryFileName) Then
            Try
                sr = New System.IO.StreamReader(cQueryFileName)
                While Not sr.EndOfStream
                    cQuery = String.Concat(cQuery, sr.ReadLine, " ", vbLf)
                End While
                sr.Close()
            Catch ex As Exception
                Throw New Exception(String.Concat("Errore durante la lettura del file ", cQueryFileName))
            End Try
        Else
            Throw New Exception(String.Concat("Il file (", cQueryFileName, ") contenente la query non esiste."))
        End If
        Return cQuery

    End Function

    Public Shared Function ControllaEsistenzaFLD(ByVal cFolder As String) As String

        If Not System.IO.Directory.Exists(cFolder) Then
            System.IO.Directory.CreateDirectory(cFolder)
        End If
        Return cFolder

    End Function

    Public Shared Function GetTimeStamp(ByVal cFolder As String) As String
        Dim cTmp As String

        cTmp = cFolder.Substring(cFolder.LastIndexOf("_") + 1)
        Return cTmp

    End Function

    Public Shared Sub PulisciFolder(ByVal cFolder As String)

        For Each cTmp As String In System.IO.Directory.GetFiles(cFolder)
            System.IO.File.Delete(cTmp)
        Next
        For Each cTmp As String In System.IO.Directory.GetDirectories(cFolder)
            System.IO.Directory.Delete(cTmp, True)
        Next

    End Sub

End Class
