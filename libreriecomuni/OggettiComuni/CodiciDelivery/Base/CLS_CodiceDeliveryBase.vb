﻿Public MustInherit Class CLS_CodiceDeliveryBase

    Public Property CodiceDelivery As String

    Public MustOverride ReadOnly Property IsVuoto As Boolean
    Public MustOverride Sub BuildCodiceDelivery(ByVal cServizio As String, ByVal cCodiceDoc As String)

End Class
