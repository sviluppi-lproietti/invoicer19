﻿Public Class CLS_CodiceDeliveryStandard
    Inherits CLS_CodiceDeliveryBase

    Const EmptyCodiceDelivery = "000000000000000000"

    Public Overrides ReadOnly Property IsVuoto As Boolean

        Get
            Return CodiceDelivery = EmptyCodiceDelivery
        End Get

    End Property


    Public Sub New()

        CodiceDelivery = EmptyCodiceDelivery

    End Sub

    Public Overrides Sub BuildCodiceDelivery(ByVal cServizio As String, ByVal cCodiceDoc As String)
        Dim cBarcode As String
        Dim nTmp As Integer

        CodiceDelivery = String.Concat(cServizio, cCodiceDoc)
        nTmp = CDec(CodiceDelivery) Mod 93
        CodiceDelivery = String.Concat(CodiceDelivery, nTmp.ToString.PadLeft(2, "0"))

    End Sub

End Class
