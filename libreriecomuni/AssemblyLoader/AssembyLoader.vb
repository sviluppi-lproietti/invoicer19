Imports System.Reflection
Imports log4net

<Serializable()>
Public Class Loader
    Inherits MarshalByRefObject

    Public Property VersionePlugIn As Integer

    Public Function CreateInstance(ByVal assemblyName As String) As Object
        Dim cInterfaceName As String
        Dim aInterface As ArrayList
        Dim nInterface As Integer
        Dim asm As Assembly
        Dim b As Byte()

        Try
            aInterface = New ArrayList
            VersionePlugIn = 0
            aInterface.Add("PLUGIN_interfaceV1_0.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_1.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_2.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_3.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_3_1.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_3_2.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_4.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_4_1.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_4_2.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_4_3.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_5_0.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_5_1.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_6_0.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV1_6_1.IPLG_dati_lib")
            aInterface.Add("PLUGIN_interfaceV2_0_0.IPLG_dati_lib")
            aInterface.Add("PlugINs.PlugIN_2_0_0")
            LogHelper.Logger.Debug("AssemblyLoader - CreateInstance " + String.Concat("Plugin da caricare: ", assemblyName))
            b = System.IO.File.ReadAllBytes(assemblyName)
            asm = Assembly.Load(b)
            For Each tipoInt As Type In asm.GetExportedTypes
                nInterface = 1
                For Each cInterfaceName In aInterface
                    If tipoInt.GetInterface(cInterfaceName) IsNot Nothing Then
                        VersionePlugIn = nInterface
                        Return Activator.CreateInstance(tipoInt)
                    End If
                    nInterface += 1
                Next
            Next
            Return Nothing
        Catch ex As Exception
            LogHelper.Logger.Error("AssemblyLoader - CreateInstance", ex)
            Throw ex
            Return Nothing
        End Try

    End Function

End Class

