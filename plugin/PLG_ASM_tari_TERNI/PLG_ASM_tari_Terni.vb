Imports System.Windows.Forms
Imports PLG_ASM_tari_base.PLG_ASM_tari_base
Imports TipiComuni

Public Class PLG_ASM_tari_Terni
    Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib

    Const cns_tsm_sel_con_bollettino = "TSM_sel_con_bollettino"
    Const cns_tsm_sel_con_allegato = "TSM_sel_con_allegato"

    ' Costante indicante il corretto registro IVA della parte UmbriaEnergy Elettricit�
    'Const cns_prot_reg_iva_corr = "UEV"
    Const UpperFileSize = 3000000
    Const LowerFileSize = 2000000

    Const nLunghezzaLinea = 156
    Private _dr_daf_pf As DS_PF_ASM_tari.TBL_dati_fornRow
    ' Costante indicante il corretto registro IVA della parte UmbriaEnergy Elettricit�
    'Const cns_prot_reg_iva_corr = "UEV"
    Private _dr_mai_pf As DS_PF_ASM_tari.TBL_mainRow
    Private _LinkUXX_cod_xxx As Integer
    'Private _dr_uxx As DS_import.TBL_record_UXXRow
    Private _ListaGiri As ArrayList
    Private _TipoCaricamento As Integer
    Private _DbGiriConnString As String
    Private _NuoviToponimi As Boolean
    'Private _data_import As DS_import
    Private _CodiceProgressivo As Integer
    Private _SessioneDati As Xml.XmlNode
    Private _SessioneTrasformazioniFileName As String
    Private _SessioneNuova As Boolean
    Private _SessionePath As String
    Private _CodiceModulo As Integer
    Private _FeedString As String
    Private _QuickDataset As DS_QD_ASM_tari
    Private _PrintFatturaDataset As DS_PF_ASM_tari
    Private _PrintBollettinoDataset As DS_PB_ASM_tari
    Private _PrintF24Dataset As DS_PF24_ASM_tari
    Private _SendEmailDataset As DS_SE_ASM_tari
    'Private _PrintBollettinoTaresDataset As DS_PB_ASM_tares
    Private _RecordCount As Integer
    Private _xmlDati_qd As Xml.XmlDocument
    Private _MessaggiAvanzamento(5) As String
    Private _ErroriLievi As String
    Private _ErroriGravi As String
    Private _SessioneDatiFissiFileName As String
    Private _SessioneDatiPath As String
    'Private _dt_tof As DS_PF_UE_ele.TBL_totali_fatturatiDataTable
    'Private _dr_mai_pf As DS_PF_ASM_ele.TBL_mainRow
    Private _dr_dfa_pf As DS_PF_ASM_tari.TBL_dati_fattRow
    'Private _dr_stm As DS_PF_UE_ele.TBL_strum_misuraRow
    Private _LogDS As DataSet
    Private _ResultAL As ArrayList
    Private _MasterRecord As Integer
    Private _NomeOfferta As String
    Private _TmpDistributore As String
    Private _LinkRXX_cod_txx As Integer

    Enum ListaDataset
        DAS_quick = 0
        DAS_fattura = 1
        DAS_bollettino = 2
        DAS_bollettino_tares = 4
        DAS_send_email = 999
    End Enum

#Region "Elenco delle property del tipo READONLY public"

    ReadOnly Property AlertPlugIn_PRN() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.MessaggioAvviso

        Get
            Dim cValue As String

            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura, ListaDataset.DAS_bollettino, ListaDataset.DAS_bollettino_tares
                    cValue = String.Concat("Attenzione!! Ricordiamo che prima di procedere con la stampa dei moduli selezionati", vbCr, "� necessario verificare i seguenti dati:", vbCr, "- Periodo di riferimento (Bimestre);", vbCr, "- Testo dell'autolettura.", vbLf, vbLf, "Posso procedere con la stampa di quanto selezionato?")
                Case Is = ListaDataset.DAS_send_email
                    cValue = String.Concat("Da modificare!!!!!!!!!!!!!!!!!")
                Case Else
                    cValue = ""
            End Select
            Return cValue
        End Get

    End Property

    ReadOnly Property DataSendByMail() As Xml.XmlNode Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DataSendByMail

        Get
            Return GetDataSendByMail()
        End Get

    End Property

    'ReadOnly Property LinkField_QuickDS_PrintDS() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LinkField_QuickDS_PrintDS

    '    Get
    '        Select Case _CodiceModulo
    '            Case Is = ListaDataset.DAS_fattura
    '                Return "MAI_linkQD"
    '            Case Is = ListaDataset.DAS_bollettino
    '                Return "FXR_linkQD"
    '            Case Else
    '                Return Nothing
    '        End Select
    '    End Get

    'End Property

    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ListaControlliFormNuovaSessione

        Get
            Dim aCtr As ArrayList

            aCtr = New ArrayList
            aCtr.Add(New String() {"LBL_input_1", "Folder con i file dati ......:", "3;16", "155;13"})
            aCtr.Add(New String() {"TXB_input_1", "&1", "160;12", "610;20", "*1"})
            aCtr.Add(New String() {"BTN_fld_input_1", "", "775;10", "27;23"})
            'aCtr.Add(New String() {"LBL_input_2", "File maggiorazione TARES..:", "3;42", "155;13"})
            'aCtr.Add(New String() {"TXB_input_2", "&1", "160;38", "610;20", "198"})
            'aCtr.Add(New String() {"BTN_fil_input_2", "", "775;38", "27;23"})
            aCtr.Add(New String() {"LBL_output_998", "Cartella di destinazione .:", "3;68", "155;13"})
            aCtr.Add(New String() {"TXB_output_998", "&998", "160;64", "610;20", "201"})
            aCtr.Add(New String() {"BTN_fld_output_998", "", "775;64", "27;23"})
            aCtr.Add(New String() {"LBL_output_999", "Descrizione archivio ......:", "3;94", "155;13"})
            aCtr.Add(New String() {"TXB_output_999", "", "160;88", "M;640;122", "202"})
            aCtr.Add(New String() {"SRT", "", "", "", ""})
            Return aCtr
        End Get

    End Property

    ReadOnly Property NomePlugIn() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.NomePlugIn

        Get
            Return "Plug-in per la stampa delle bollette della TIA/TARES per ASM."
        End Get

    End Property

    ReadOnly Property QuickDataset() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset

        Get
            Return _QuickDataset
        End Get

    End Property

    ReadOnly Property QuickDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset_MainTableKey

        Get
            Return "MAI_codice"
        End Get

    End Property

    ReadOnly Property QuickDataset_MainTableName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset_MainTableName

        Get
            Return "TBL_main"
        End Get

    End Property

    ReadOnly Property ResultAL() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ResultAL

        Get
            Return _ResultAL
        End Get

    End Property

#End Region

#Region "Elenco delle property del tipo READONLY Private"

    Private ReadOnly Property PrintDataSet_DataFile(ByVal _FileIdx As Integer)

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode(String.Concat("filename[@codice=", "", _FileIdx, "", "]")).InnerText)
        End Get

    End Property

    Private ReadOnly Property QuickDataSet_IndexFile() As String

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode("filename[@tipo=""0""]").InnerText)
        End Get

    End Property

#End Region

#Region "Elenco delle property del tipo WRITEONLY"

    WriteOnly Property CodiceModulo() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CodiceModulo

        Set(ByVal value As Integer)
            _CodiceModulo = value
        End Set

    End Property

    WriteOnly Property FeedString() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FeedString

        Set(ByVal value As String)
            _FeedString = value
        End Set

    End Property

    WriteOnly Property SessioneDatiPath() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneDatiPath

        Set(ByVal value As String)
            _SessioneDatiPath = value
        End Set

    End Property

    WriteOnly Property SessioneTrasformazioniFileName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneTrasformazioniFileName

        Set(ByVal value As String)
            _SessioneTrasformazioniFileName = value
        End Set

    End Property

    WriteOnly Property SessioneDatiFissiFileName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneDatiFissiFileName

        Set(ByVal value As String)
            _SessioneDatiFissiFileName = value
        End Set

    End Property

    WriteOnly Property SessioneFld() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneFld

        Set(ByVal value As String)
            _SessionePath = value
        End Set

    End Property

    WriteOnly Property SezioneDati() As Xml.XmlNode Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SezioneDati

        Set(ByVal value As Xml.XmlNode)
            _SessioneDati = value
        End Set

    End Property

#End Region

#Region "Elenco delle property del tipo READ e WRITE"

    Property MessaggiAvanzamento() As String() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.MessaggiAvanzamento

        Get
            Return _MessaggiAvanzamento
        End Get
        Set(ByVal value As String())
            _MessaggiAvanzamento = value
        End Set

    End Property

#End Region

#Region "Elenco dei metodi Pubblici"

#End Region

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckDatiSessione
        Dim _lObb As Boolean = False
        'Dim _l101 As Boolean = False
        ' Dim _l102 As Boolean = False
        Dim lRtn As Boolean = True

        For Each aValue As String() In aDati
            Select Case aValue(0)
                Case Is = 1
                    _lObb = True
                    'Case Is = 102
                    '    _l102 = True
            End Select
        Next
        If _SessioneNuova Then
            If Not _lObb Then
                MessageBox.Show("Attenzione! Non sono � stato indicato nessun file contenente i dati. Non posso procedere con la creazione della sessione.")
                lRtn = False
            Else
                'If Not _l102 And lRtn Then
                '    lRtn = MessageBox.Show("Attenzione! Non � stato indicato un file valido per le credenziali Web. Procedo ugualmente con la creazione della sessione?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes
                'End If
            End If
        Else
            'If Not _l102 And lRtn Then
            '    lRtn = MessageBox.Show("Attenzione! Non � stato indicato un file valido per le credenziali Web. Procedo ugualmente con la creazione della sessione?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes
            'End If
        End If
        Return lRtn

    End Function

    Private Sub SetRecordIDX(ByVal xmlIndice As Xml.XmlDocument, ByVal xmlRecord As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xmlIndice.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xmlRecord.AppendChild(xiTmp)

    End Sub

    ReadOnly Property CurrentRecordNumber() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CurrentRecordNumber

        Get
            Return _RecordCount
        End Get

    End Property

    Sub GoToRecordNumber(ByVal nRecord As Integer) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.GoToRecordNumber

        _RecordCount = nRecord

    End Sub


#Region "SetUp per Datagrid di visualizzazione delle righe da stampare"

    Sub ImpostaDGVDati(ByVal DGV As DataGridView) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ImpostaDGVDati

        With DGV.Columns
            .Clear()
            .Add(Col_DaStampare)
            .Add(Col_Errore_code)
            .Add(Col_Errore_desc)
            .Add(Col_Progressivo)
            .Add(Col_NumeroUtente)
            .Add(Col_NumeroFattura)
            .Add(Col_Nominativo)
            .Add(COL_Indirizzo_Recapito)
            .Add(COL_CAP_Recapito)
            .Add(COL_Citta_Recapito)
            .Add(COL_Provincia_Recapito)
            .Add(COL_Nazione_Recapito)
            .Add(COL_Print_F24S)
            .Add(COL_Print_allegato)
            .Add(COL_SpedByMail)
        End With

    End Sub

    Private Function Col_DaStampare() As System.Windows.Forms.DataGridViewCheckBoxColumn
        Dim col As System.Windows.Forms.DataGridViewCheckBoxColumn

        col = New System.Windows.Forms.DataGridViewCheckBoxColumn
        With col
            .HeaderText = "Stampa"
            .DataPropertyName = "DEF_toprint"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .Width = 50
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Errore_code() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errcode"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Errore_desc() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errdesc"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Progressivo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Progressivo"
            .DataPropertyName = "MAI_codice"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 70
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroUtente() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Utente"
            .DataPropertyName = "MAI_num_utente"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 90
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroFattura() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Fattura"
            .DataPropertyName = "MAI_num_fattura"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 110
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Nominativo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nominativo"
            .DataPropertyName = "MAI_nominativo"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Indirizzo_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Indirizzo Recapito"
            .DataPropertyName = "MAI_indirizzo_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_CAP_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "CAP"
            .DataPropertyName = "MAI_cap_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Citta_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Citt�"
            .DataPropertyName = "MAI_citta_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Provincia_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Prov."
            .DataPropertyName = "MAI_provincia_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Nazione_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nazione"
            .DataPropertyName = "MAI_nazione_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Print_F24S()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "F24 S."
            .DataPropertyName = "MAI_print_F24S"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_Print_allegato()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Allegato"
            .DataPropertyName = "MAI_print_all"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_SpedByMail()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Sped. Email"
            .DataPropertyName = "DEF_sendbymail"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

#End Region

    'Private Function RecuperaPeriodoRiferimento(ByVal xiWork As Xml.XmlNode) As String
    '    Dim dTmp1 As DateTime
    '    Dim dTmp2 As DateTime
    '    Dim cRtn As String

    '    cRtn = ""
    '    dTmp1 = GetValueFromXML(xiWork, Costanti.itm_data_ini_fat)
    '    dTmp2 = GetValueFromXML(xiWork, Costanti.itm_data_fin_fat)
    '    If dTmp1.Year = dTmp2.Year And dTmp1.Month = dTmp2.Month Then
    '        cRtn = String.Concat(TrasformaMese(dTmp1.Month), " ", dTmp1.Year)
    '    Else
    '        If dTmp1.Year = dTmp2.Year Then
    '            cRtn = String.Concat(TrasformaMese(dTmp1.Month), " ", TrasformaMese(dTmp2.Month), " ", dTmp1.Year)
    '        Else
    '            cRtn = String.Concat(TrasformaMese(dTmp1.Month), " ", dTmp1.Year, " ", TrasformaMese(dTmp2.Month), " ", dTmp2.Year)
    '        End If
    '    End If
    '    Return cRtn

    'End Function

    Private Function TrasformaMese(ByVal nMese As Integer) As String
        Dim cvalue As String

        Select Case nMese
            Case Is = 1
                cvalue = "gennaio"
            Case Is = 2
                cvalue = "febbraio"
            Case Is = 3
                cvalue = "marzo"
            Case Is = 4
                cvalue = "aprile"
            Case Is = 5
                cvalue = "maggio"
            Case Is = 6
                cvalue = "giugno"
            Case Is = 7
                cvalue = "luglio"
            Case Is = 8
                cvalue = "agosto"
            Case Is = 9
                cvalue = "settembre"
            Case Is = 10
                cvalue = "ottobre"
            Case Is = 11
                cvalue = "novembre"
            Case Is = 12
                cvalue = "dicembre"
            Case Else
                cvalue = ""
        End Select
        Return cvalue

    End Function

    ReadOnly Property ElementiMenuDedicati() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ElementiMenuDedicati
        Get


            Dim SubMenuEntry As ToolStripMenuItem
            Dim MenuEntry As ToolStripMenuItem
            Dim aMenuItem As ArrayList

            aMenuItem = New ArrayList
            MenuEntry = New ToolStripMenuItem("Stampa TIA/TARES")

            'SubMenuEntry = New ToolStripMenuItem("Codice del Gruppo")
            'AddHandler SubMenuEntry.Click, AddressOf CodiceGruppo_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Testo autolettura")
            'AddHandler SubMenuEntry.Click, AddressOf TestoAutolettura_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Stampa autolettura")
            'SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa).ToLower = "si"
            'AddHandler SubMenuEntry.Click, AddressOf StampaAutolettura_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Testo Bonus Sociale")
            'AddHandler SubMenuEntry.Click, AddressOf TestoBonusSociale_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Stampa Bonus Sociale")
            'SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_stampa).ToLower = "si"
            'AddHandler SubMenuEntry.Click, AddressOf StampaBonusSociale_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Stampa Box Commerciale")
            'SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale).ToLower = "si"
            'AddHandler SubMenuEntry.Click, AddressOf StampaBoxCommerciale_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            ''SubMenuEntry = New ToolStripMenuItem("Forza offerta scacciapensieri")
            ''SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si"
            ''AddHandler SubMenuEntry.Click, AddressOf ForzaOffertaScacciapensieri_TSM_click
            ''MenuEntry.DropDownItems.Add(SubMenuEntry)
            'MenuEntry.DropDownItems.Add(New ToolStripSeparator())

            'SubMenuEntry = New ToolStripMenuItem("Dati Fissi")
            'AddHandler SubMenuEntry.Click, AddressOf DatiFissi_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'MenuEntry.DropDownItems.Add(New ToolStripSeparator())

            '' Imposta tutti i record con allegato
            'SubMenuEntry = New ToolStripMenuItem("Allegato su tutti")
            'AddHandler SubMenuEntry.Click, AddressOf AllegatiAll_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            '' Imposta nessun record con allrgato
            'SubMenuEntry = New ToolStripMenuItem("Allegato su Nessuno")
            'AddHandler SubMenuEntry.Click, AddressOf AllegatiNot_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            aMenuItem.Add(New Object() {"n", "TSM_sessione_stampa", MenuEntry})

            'aMenuItem.Add(New Object() {"a", "TSM_selezione", New ToolStripSeparator()})

            '' Seleziona tutti i record con bollettino
            'SubMenuEntry = New ToolStripMenuItem("Con bollettino")
            'SubMenuEntry.Name = cns_tsm_sel_con_bollettino
            'aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

            '' Seleziona tutti i record con allegato
            'SubMenuEntry = New ToolStripMenuItem("Con allegato")
            'SubMenuEntry.Name = cns_tsm_sel_con_allegato
            'aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

            'tsb = New System.Windows.Forms.ToolStripButton
            'tsb.AutoSize = True
            'tsb.ImageTransparentColor = System.Drawing.Color.Magenta
            'AddHandler tsb.Click, AddressOf ForzaOffertaScacciapensieri_TSB_click
            'tsb.Name = "TSB_forza_scacciapensieri"
            'tsb.Text = "Forzatura scacciapensieri"
            'SetButtonForzaturaScacciapensieri(tsb)

            'aMenuItem.Add(New Object() {"a", "TOS_menu", tsb})
            Return aMenuItem
        End Get

    End Property

    'Private Sub BimestreFatturazione_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim xmlDF As New Xml.XmlDocument
    '    Dim frm As FRM_bimestre

    '    Try
    '        frm = New FRM_bimestre
    '        frm.StartPosition = FormStartPosition.CenterParent
    '        frm.TXB_bimestre.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "14")
    '        If frm.ShowDialog = DialogResult.OK Then
    '            SetValueDatiFissi(Costanti.DatiFissi_Fattura, "14", frm.TXB_bimestre.Text)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il valore richiesto.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Sub

    ' ************************************************************************** '
    ' Imposto correttamente la parte di selezione per la stampa dei record. Se   '
    ' non dobbiamo mantenere la selezione o se la dobbiamo mantenere ma il re-   ' 
    ' cord non � stampabile metto a "NON STAMPABILE (False)" il campo .          '
    ' ************************************************************************** '
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetSelectable
        Dim dr_mai_qd As DS_QD_ASM_tari.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_ASM_tari.TBL_mainRow)
        dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
        If cFilter > "" Then
            dr_mai_qd.DEF_selectable = dr_mai_qd.DEF_selectable And dr_mai_qd(cFilter)
        End If

        ' ************************************************************************** '
        ' Pulisce i campi relativi agli errori in quanto ci stiamo occupando di un   '
        ' nuovo modulo.                                                              '
        ' ************************************************************************** '
        If dr_mai_qd.DEF_selectable Then
            dr_mai_qd.Item("DEF_errcode") = 0
            dr_mai_qd.Item("DEF_errdesc") = ""
        End If

    End Sub

    Private Function GetValueDatiFissi(ByVal cTable As String, ByVal cField As String) As String
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim cRtn As String

        Try
            xmlDF.Load(_SessioneDatiFissiFileName)
            xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))

            cRtn = xmlTmp.InnerText
            cRtn = cRtn.Replace("{\TBL_main;MAI_punto_fornitura/}", "&PUNTO_FORNITURA&")
            cRtn = cRtn.Replace("{\TBL_strum_misura;STM_matricola/}", "&MATRICOLA_CONT&")
            xmlDF = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    Private Sub SetValueDatiFissi(ByVal cTable As String, ByVal cField As String, ByVal cValue As String)
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim xaTmp As Xml.XmlAttribute
        Dim xnWork2 As Xml.XmlNode

        cValue = cValue.Replace("&PUNTO_FORNITURA&", "{\TBL_main;MAI_punto_fornitura/}")
        cValue = cValue.Replace("&MATRICOLA_CONT&", "{\TBL_strum_misura;STM_matricola/}")

        xmlDF.Load(_SessioneDatiFissiFileName)
        xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))
        If Not xmlTmp Is Nothing Then
            xmlTmp = xmlTmp.SelectSingleNode("valore")
            xmlTmp.InnerText = cValue
        Else
            'Crea il nodo
            xmlTmp = xmlDF.CreateElement("item")
            xaTmp = xmlDF.CreateAttribute("codice")
            xaTmp.Value = cField
            xmlTmp.Attributes.Append(xaTmp)
            xnWork2 = xmlDF.CreateElement("valore")
            xnWork2.InnerText = cValue
            xmlTmp.AppendChild(xnWork2)
            xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable)).AppendChild(xmlTmp)
        End If
        xmlDF.Save(_SessioneDatiFissiFileName)
        xmlDF = Nothing

    End Sub

    Private Sub AllegatiNot_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        For Each dr_mai_qd As DS_QD_ASM_tari.TBL_mainRow In QuickDataset.Tables(0).Rows
            dr_mai_qd.Item("MAI_print_all") = False
        Next

    End Sub

    Private Sub AllegatiAll_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        For Each dr_mai_qd As DS_QD_ASM_tari.TBL_mainRow In QuickDataset.Tables(0).Rows   '   _QuickDataset.Tables(PlugInMainTable(ListaModuli.MOD_quickdataset)).Rows
            dr_mai_qd.Item("MAI_print_all") = True
        Next

    End Sub

    'Private Sub SetAllegato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim frm As New FRM_set_allegato

    '    frm.StartPosition = FormStartPosition.CenterParent
    '    frm.Show()

    'End Sub

    'Private Sub StampaAutolettura_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
    '    If CType(sender, ToolStripMenuItem).Checked Then
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa, "si")
    '    Else
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa, "no")
    '    End If

    'End Sub

    'Private Sub StampaBoxCommerciale_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
    '    If CType(sender, ToolStripMenuItem).Checked Then
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale, "si")
    '    Else
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale, "no")
    '    End If

    'End Sub

    'Private Sub ForzaOffertaScacciapensieri_TSM_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
    '    If CType(sender, ToolStripMenuItem).Checked Then
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "si")
    '    Else
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "no")
    '    End If

    'End Sub

    'Private Sub ForzaOffertaScacciapensieri_TSB_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    If GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si" Then
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "no")
    '    Else
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "si")
    '    End If
    '    SetButtonForzaturaScacciapensieri(sender)

    'End Sub

    'Private Sub SetButtonForzaturaScacciapensieri(ByVal tsb As ToolStripButton)

    '    If tsb.Text.EndsWith(" Si") Then
    '        tsb.Text = tsb.Text.Replace(" Si", "")
    '    ElseIf tsb.Text.EndsWith(" No") Then
    '        tsb.Text = tsb.Text.Replace(" No", "")
    '    End If
    '    If GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si" Then
    '        tsb.Image = Global.PLG_ASM_tari_TERNI.My.Resources.bullet_green
    '        tsb.Text = String.Concat(tsb.Text, " Si")
    '    Else
    '        tsb.Image = Global.PLG_ASM_tari_TERNI.My.Resources.bullet_red
    '        tsb.Text = String.Concat(tsb.Text, " No")
    '    End If

    'End Sub

    'Private Sub StampaBonusSociale_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
    '    If CType(sender, ToolStripMenuItem).Checked Then
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_stampa, "si")
    '    Else
    '        SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_stampa, "no")
    '    End If

    'End Sub

    'Private Sub CodiceGruppo_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim xmlDF As New Xml.XmlDocument
    '    Dim frm As New FRM_gruppo

    '    Try
    '        frm = New FRM_gruppo
    '        frm.StartPosition = FormStartPosition.CenterParent
    '        frm.TXB_gruppo.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_gruppo)
    '        If frm.ShowDialog = DialogResult.OK Then
    '            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_gruppo, frm.TXB_gruppo.Text)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo dell'autolettura", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Sub

    'Private Sub TestoAutolettura_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim xmlDF As New Xml.XmlDocument
    '    Dim frm As New FRM_autolettura

    '    Try
    '        frm = New FRM_autolettura
    '        frm.StartPosition = FormStartPosition.CenterParent
    '        frm.TXB_autolettura.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_testo)
    '        If frm.ShowDialog = DialogResult.OK Then
    '            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_testo, frm.TXB_autolettura.Text)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo dell'autolettura", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Subv


    'Private Sub TestoBonusSociale_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim xmlDF As New Xml.XmlDocument
    '    Dim frm As New FRM_bonus_soc

    '    Try
    '        frm = New FRM_bonus_soc
    '        frm.StartPosition = FormStartPosition.CenterParent
    '        frm.TXB_bonus_sociale.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_testo)
    '        If frm.ShowDialog = DialogResult.OK Then
    '            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_testo, frm.TXB_bonus_sociale.Text)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo del bonus sociale", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Sub

    'Private Sub ForzaDataScadenza_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim xmlDF As New Xml.XmlDocument
    '    Dim frm As New FRM_forza_scad

    '    Try
    '        frm = New FRM_forza_scad
    '        frm.StartPosition = FormStartPosition.CenterParent
    '        frm.Condizione = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "17")
    '        frm.TXB_data_scad.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "16")
    '        If frm.ShowDialog = DialogResult.OK Then
    '            SetValueDatiFissi(Costanti.DatiFissi_Fattura, "16", frm.TXB_data_scad.Text)
    '            SetValueDatiFissi(Costanti.DatiFissi_Fattura, "17", frm.Condizione)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare i dati per la forzatura della data di scadenza", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Sub

    'Private Sub DatiFissi_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim xmlDF As New Xml.XmlDocument
    '    Dim frm As FRM_datifissi

    '    Try
    '        frm = New FRM_datifissi
    '        frm.StartPosition = FormStartPosition.CenterParent
    '        frm.FileNameDatiFissi = _SessioneDatiFissiFileName
    '        If frm.ShowDialog = DialogResult.OK Then

    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il valore richiesto.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Sub

    Sub RemovePRNDSErrorRecord() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.RemovePRNDSErrorRecord
        Dim nLinkCode As Integer

        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                'For Each dr_mai_pd As DS_QD_ASM_tia_tares.TBL_mainRow In _PrintFatturaDataset.TBL_main.Rows
                '    nLinkCode = dr_mai_pd(LinkField_QuickDS_FullDS)
                '    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                '        dr_mai_pd.Delete()
                '    End If
                'Next
                '_PrintFatturaDataset.TBL_main.AcceptChanges()
            Case Is = ListaDataset.DAS_bollettino
                '_PrintBollettinoDataset.TBL_fat_x_rat.AcceptChanges()
                _PrintF24Dataset.TBL_fat_x_rat.AcceptChanges()
        End Select

    End Sub

    ReadOnly Property LogDSStampa() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LogDSStampa

        Get
            LoadDSLog()
            Return _LogDS
        End Get

    End Property

    Private Sub LoadDSLog()
        Dim dr As DataRow

        For Each dr_qds As DS_QD_ASM_tari.TBL_mainRow In _QuickDataset.TBL_main.Rows
            If dr_qds.DEF_errcode < 0 Then
                dr = _LogDS.Tables(0).NewRow
                dr("LOG_desc_rec_1") = String.Concat("Progressivo: ", dr_qds.MAI_codice, " Nominativo: ", dr_qds.MAI_nominativo, " Numero Fattura: ", dr_qds.MAI_num_fattura)
                dr("LOG_stato") = dr_qds.DEF_errcode
                dr("LOG_desc_err") = dr_qds.DEF_errdesc
                _LogDS.Tables(0).Rows.Add(dr)
            End If
        Next

    End Sub

    'Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetPrintable
    '    Dim dr_mai_qd As DS_QD_ASM_tia_tares.TBL_mainRow

    '    dr_mai_qd = CType(dr, DS_QD_ASM_tia_tares.TBL_mainRow)
    '    Select Case _SelectFromPlugInType
    '        Case Is = cns_tsm_sel_con_bollettino.ToLower
    '            dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_bol
    '        Case Is = cns_tsm_sel_con_allegato.ToLower
    '            dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_all
    '    End Select

    'End Sub

    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.GetSelectedRow
        Dim dr_mai_qd As DS_QD_ASM_tari.TBL_mainRow
        Dim aResult As String()
        Dim cFilter As String

        Select Case _SelectFromPlugInType
            Case Is = "tsm_ric_progressivo"
                cFilter = "MAI_codice"
            Case Else
                cFilter = ""
        End Select
        aResult = New String() {"MAI_codice", ""}
        For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
            If dr_mai_qd(cFilter) = cValue Then
                aResult(1) = dr_mai_qd.MAI_codice
            End If
        Next
        Return aResult

    End Function

    WriteOnly Property FiltroSelezione() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FiltroSelezione

        Set(ByVal value As String)

        End Set

    End Property

    'Public Sub ClearPrintDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ClearPrintDataset

    '    Select Case _CodiceModulo
    '        Case Is = ListaModuli.MOD_fattura
    '            _PrintFatturaDataset = Nothing
    '        Case Is = ListaModuli.MOD_bollettino
    '            _PrintBollettinoDataset = Nothing
    '    End Select

    'End Sub

#Region "Procedure utili per la creazione della sessione di stampa"

    Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CreaDataBaseSessione
        Dim nGiro As Integer
        Dim mng_giri As FC_manage_giri
        Dim cCitta As String
        Dim cToponimo As String
        Dim sr As System.IO.StreamReader
        Dim xdWork As Xml.XmlDocument
        Dim xnWork2 As Xml.XmlNode
        Dim aBlocchi As String()
        Dim cFileName As String
        Dim nRecord As Integer
        Dim nBlocco As String
        Dim cBlocco As String
        Dim chTmp(0) As Char
        Dim nLine As Integer
        Dim cLine As String
        Dim aTemp As String()
        Dim aHeader As String()
        Dim cHeader As String
        Dim nTmp As Integer
        Dim nCodFatt As Integer
        Dim cBolletta As String
        Dim cOffSetChar As String
        Dim cErrorMessage As String
        Dim aValue As String()
        Dim nIntesta As Integer
        Dim cProgressivoRiga As String
        'Dim lOnly199 As Boolean
        Dim i As Integer
        Dim cTmp As String
        'Dim dr_uxx As DS_import.TBL_record_UXXRow
        Dim xiRecord As Xml.XmlNode
        Dim cSortValue As String
        Dim dsExcel As DataSet
        'Dim dr_tar As DS_import.TBL_taresRow
        'Dim dr_f00 As DS_import.TBL_record_F00Row
        Dim cTINumFat As String
        Dim ds_src As DataSet
        Dim dr_fat As DataRow
        Dim nSogId As Integer

        _ResultAL = New ArrayList
        ImpostaMessaggi(New String() {"", "", "", "0", _DatiSessione.Count, "Elaborazione dei file da caricare...."})

        aBlocchi = New String(3) {"", "", "", ""}

        ' ************************************************************************** '
        ' Copia i file dei dati dalla cartella dove sono attualmente nella cartella  '
        ' dati della sessione.                                                       '
        ' ************************************************************************** '
        ImpostaMessaggi(New String() {"", "", "", "0", _DatiSessione.Count, "Elaborazione dei file da caricare...."})
        'cFileNameAllegatoTipo = ""
        For Each aValue In _DatiSessione
            ImportaFileInSessione(aValue(0), aValue(1))
            'If aValue(0) = 190 Then
            '    cFileNameAllegatoTipo = aValue(1)
            'End If
        Next

        ' cFileName = CercaFileName(_DatiSessione, 199)

        Try
            mng_giri = New FC_manage_giri(New OleDb.OleDbConnection(_DbGiriConnString))
            mng_giri.ListaGiri = _ListaGiri
            xdWork = New Xml.XmlDocument
            xdWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "FATTURA", ""))
            'ds_all = CaricaFileDatiToDS_excel(cFileNameAllegatoTipo)
            For Each aValue In _ResultAL
                If (aValue(3) = 1) And (aValue(4) = 0) Then
                    Try
                        ds_src = New DataSet
                        ds_src.ReadXml(String.Concat(_SessionePath, aValue(1)))

                        For Each dr_fat In ds_src.Tables("Fattura").Rows
                            nSogId = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "IDSoggetto")
                            xiRecord = xdWork.CreateNode(Xml.XmlNodeType.Element, "FATTURA_ROW", "")

                            SetRecordIDX(xdWork, xiRecord, "DEF_errcode", "0")
                            SetRecordIDX(xdWork, xiRecord, "MAI_num_utente", nSogId)
                            SetRecordIDX(xdWork, xiRecord, "MAI_num_fattura", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "NumeroFattura"))
                            SetRecordIDX(xdWork, xiRecord, "MAI_nominativo", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "RagioneSociale", ""))
                            SetRecordIDX(xdWork, xiRecord, "MAI_IDFattura", dr_fat("IDFattura"))

                            cCitta = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Comune")
                            cToponimo = String.Concat(RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "TipoElementoTopologico"), " ", _
                                                    RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ElementoTopologico", ""))

                            SetRecordIDX(xdWork, xiRecord, "MAI_indirizzo_recapito", String.Concat(RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "TipoElementoTopologico"), " ", _
                                                                                                    RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ElementoTopologico", ""), " ", _
                                                                                                    RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NumeroCivico", ""), " ", _
                                                                                                    RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "SuffissoCivico", ""), " ", _
                                                                                                    RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Localita", "")))
                            SetRecordIDX(xdWork, xiRecord, "MAI_citta_recapito", cCitta)
                            SetRecordIDX(xdWork, xiRecord, "MAI_cap_recapito", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Cap").ToString().PadLeft(5, "0"))
                            SetRecordIDX(xdWork, xiRecord, "MAI_provincia_recapito", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ProvinciaSigla"))
                            SetRecordIDX(xdWork, xiRecord, "MAI_nazione_recapito", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NazioneSigla"))
                            If CInt(RecuperaValoreDS_UE(ds_src, dr_fat, 0, "RiepilogoIVA", "TotaleFattura")) >= 12 Then
                                SetRecordIDX(xdWork, xiRecord, "MAI_print_F24S", "si")
                            Else
                                SetRecordIDX(xdWork, xiRecord, "MAI_print_F24S", "no")
                            End If

                            'If ds_all Is Nothing Then
                            '    SetRecordIDX(xdWork, xiRecord, "MAI_print_all", "si")
                            'Else
                            '    If ds_all.Tables(0).Select(String.Concat("SOG_ID = ", nSogId)).Length = 1 Then
                            '        SetRecordIDX(xdWork, xiRecord, "MAI_print_all", "si")
                            '    Else
                            '        SetRecordIDX(xdWork, xiRecord, "MAI_print_all", "no")
                            '    End If
                            'End If
                            SetRecordIDX(xdWork, xiRecord, "MAI_filedati", aValue(2))
                            'If RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "Protocollo") = cns_prot_reg_iva_corr Then
                            '    SetRecordIDX(xdWork, xiRecord, "DEF_errcode", "0")
                            '    SetRecordIDX(xdWork, xiRecord, "DEF_errdesc", "")
                            'Else
                            '    SetRecordIDX(xdWork, xiRecord, "DEF_errcode", "-3")
                            '    SetRecordIDX(xdWork, xiRecord, "DEF_errdesc", String.Concat("Protocollo registro IVA non corretto ", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "Protocollo"), " invece di ", cns_prot_reg_iva_corr, "."))
                            'End If
                            SetRecordIDX(xdWork, xiRecord, "DEF_onlyarchott", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "TipoStampa", "DOPPIO INVIO FATTURA", "NO").ToString.ToUpper = "NO")
                            SetRecordIDX(xdWork, xiRecord, "DEF_sendbymail", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "IndirizzoEmailInvioFattura").ToString.Contains("@"))

                            ' ************************************************************************** '
                            ' Impostazione della stringa per l'ordinamento. La richiesta di Bonato � sta '
                            ' ta:                                                                        '
                            ' Citta - Via - NumeroCivico Pari o dispari - numero civico - suffisso -     '
                            ' Ragione sociale.                                                           '
                            ' ************************************************************************** '
                            nGiro = mng_giri.RicercaGiroPLUGIN(cCitta, cToponimo)
                            _NuoviToponimi = _NuoviToponimi Or mng_giri.NuovoToponimo
                            cSortValue = nGiro.ToString.PadLeft(6, "0")
                            cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Comune"))
                            cTmp = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NumeroCivico", "")
                            cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "TipoElementoTopologico"), " ", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ElementoTopologico", ""))
                            If IsNumeric(cTmp) Then
                                If (cTmp Mod 2) = 0 Then
                                    cSortValue = String.Concat(cSortValue, "P")
                                Else
                                    cSortValue = String.Concat(cSortValue, "D")
                                End If
                                ' Inverte l'ordinamento per numero civico, dal pi� grande al pi� piccolo.
                                cTmp = 1000000000 - Integer.Parse(cTmp)
                            Else
                                cSortValue = String.Concat(cSortValue, "N")
                            End If
                            cSortValue = String.Concat(cSortValue, cTmp.PadLeft(10, "0"))
                            cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "SuffissoCivico").PadLeft(6, " "))
                            cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "RagioneSociale", "")).Replace(" ", "")
                            SetRecordIDX(xdWork, xiRecord, "SORTFIELD", cSortValue)
                            xdWork.SelectSingleNode("FATTURA").AppendChild(xiRecord)
                        Next
                        aValue(4) = 1
                    Catch ex As Exception
                        aValue(4) = -1
                    Finally
                        '   xdWork1 = Nothing
                    End Try
                Else
                    aValue(4) = 1
                End If
                ' _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
            Next
            'For Each dr_uxx In _data_import.TBL_record_UXX
            '    'dr_uxx = _data_import.TBL_record_UXX.Rows(_data_import.TBL_record_UXX.Rows.Count - 1)
            '    xiRecord = xdWork.CreateNode(Xml.XmlNodeType.Element, "FATTURA_ROW", "")
            '    SetRecordIDX(xdWork, xiRecord, "MAI_num_utente", dr_uxx.UXX_CODUTE)
            '    SetRecordIDX(xdWork, xiRecord, "MAI_num_fattura", dr_uxx.UXX_NUMFAT)
            '    SetRecordIDX(xdWork, xiRecord, "MAI_nominativo", dr_uxx.UXX_RAGSOC)
            '    SetRecordIDX(xdWork, xiRecord, "MAI_indirizzo_recapito", dr_uxx.UXX_INDIR)
            '    SetRecordIDX(xdWork, xiRecord, "MAI_citta_recapito", dr_uxx.UXX_CITTA)
            '    If dr_uxx.UXX_conbollettino Then
            '        SetRecordIDX(xdWork, xiRecord, "MAI_print_bol", "si")
            '    Else
            '        SetRecordIDX(xdWork, xiRecord, "MAI_print_bol", "no")
            '    End If

            '    If CType(dr_uxx.GetChildRows("TBL_record_UXX_TBL_record_F00")(0), DS_import.TBL_record_F00Row).F00_TOTFATTU < 12 Then
            '        SetRecordIDX(xdWork, xiRecord, "MAI_print_bol_tares", "no")
            '    Else
            '        SetRecordIDX(xdWork, xiRecord, "MAI_print_bol_tares", "si")
            '    End If

            '    SetRecordIDX(xdWork, xiRecord, "MAI_print_all", "si")
            '    If cErrorMessage = "" Then
            '        SetRecordIDX(xdWork, xiRecord, "DEF_errcode", "0")
            '        SetRecordIDX(xdWork, xiRecord, "DEF_errdesc", "")
            '    Else
            '        SetRecordIDX(xdWork, xiRecord, "DEF_errcode", "-1")
            '        SetRecordIDX(xdWork, xiRecord, "DEF_errdesc", cErrorMessage)
            '    End If
            '    SetRecordIDX(xdWork, xiRecord, "DEF_sendbymail", "no")
            '    SetRecordIDX(xdWork, xiRecord, "DEF_onlyarchott", "no")
            '    SetRecordIDX(xdWork, xiRecord, "MAI_filedati", dr_uxx.UXX_PROGRE)

            '    ' impostazione della modalit� di ordinamento.
            '    ' in partenza si era preferito ordinare per numero fattura.
            '    ' la richiesta di bonato � stata invece quella di ordinare per:
            '    ' CAP - Via - NumeroCivico Pari o dispari - numero civico.

            '    ' la citt� � generalmente composta cos�: 05100 - Terni (TR)
            '    ' composizioni diverse daranno origine a ricerche errate.
            '    cCitta = dr_uxx.UXX_CITTA1
            '    If cCitta.IndexOf("-") > 0 Then
            '        cCitta = cCitta.Substring(cCitta.IndexOf("-") + 1).Trim
            '    End If
            '    If cCitta.IndexOf("(") > 0 Then
            '        cCitta = cCitta.Substring(0, cCitta.IndexOf("(") - 1).Trim
            '    End If
            '    cToponimo = dr_uxx.UXX_INDIR1
            '    If cToponimo.IndexOf("/") > 0 Then
            '        cToponimo = cToponimo.Substring(0, cToponimo.IndexOf("/") - 1)
            '    End If
            '    While IsNumeric(cToponimo(cToponimo.Length - 1))
            '        cToponimo = cToponimo.Substring(0, cToponimo.Length - 1)
            '    End While
            '    cToponimo = cToponimo.Trim


            '    nGiro = mng_giri.RicercaGiroPLUGIN(cCitta, cToponimo)
            '    cSortValue = String.Concat(nGiro.ToString.PadLeft(6, "0"), cCitta, cToponimo)
            '    SetRecordIDX(xdWork, xiRecord, "SORTFIELD", cSortValue)
            '    xdWork.SelectSingleNode("FATTURA").AppendChild(xiRecord)
            'Next
            _MessaggiAvanzamento(5) = "Ordinamento dei record in corso..."
            FC_SortFile.SortFileIndiceXML(xdWork, 0, xdWork.SelectSingleNode("FATTURA").ChildNodes.Count - 1)

            ' Rimuove il campo utilizzato per l'ordinamento
            nRecord = 1
            For Each xnWork1 As Xml.XmlNode In xdWork.SelectNodes("FATTURA/FATTURA_ROW/SORTFIELD")
                xnWork2 = xnWork1.ParentNode
                xnWork2.RemoveChild(xnWork1)
                SetRecordIDX(xdWork, xnWork2, "MAI_codice", nRecord)
                nRecord += 1
            Next

            _MessaggiAvanzamento(5) = "Salvataggio dati in corso..."
            xdWork.Save(String.Concat(_SessionePath, "\dati\indice.xml"))
            'xdViaMail.Save(String.Concat(_SessionePath, "\dati\SendByMail.xml"))
            _ResultAL.Add(New String() {"filename", "dati\indice.xml", "0", "0", "1"})

        Catch ex As Exception

        End Try


    End Sub

    Private Sub AggiungiRecordQD(ByVal xdWork2 As Xml.XmlDocument, ByVal cErrorMessage As String)




    End Sub

    Private Function CercaFileName(ByVal _DatiSessione As ArrayList, ByVal nTipo As Integer) As String
        Dim cTmp As String

        cTmp = ""
        For Each aValue As String() In _DatiSessione
            If aValue(0) = nTipo Then
                cTmp = aValue(1)
            End If
        Next
        Return cTmp

    End Function

    Sub ImportaFileInSessione(ByVal nTipoFile As Integer, ByVal cFileName As String)
        Dim nRecordtoAnalized As Integer
        Dim xdWork1 As Xml.XmlDocument
        Dim xdWork2 As Xml.XmlDocument
        Dim nRecordXFile As Integer
        Dim xiWork As Xml.XmlNode
        Dim cDstFile As String
        Dim nRecord As Integer
        Dim nFile As Integer
        Dim nDim As Integer
        Dim xnItem1 As Xml.XmlNode
        Dim xnItem2 As Xml.XmlNode

        ' Nel caso di tipo file 1 allora si tratta di un file xml, indipendentemente dall'estensione 
        ' da suddividere eventualmente in file di pi� piccole dimensioni.
        If nTipoFile = 1 Then
            Try
                'xdWork1 = New Xml.XmlDocument
                'xdWork1.Load(cFileName)
                xdWork1 = caricaFileDatiXML(cFileName)
                For Each xnItem As Xml.XmlNode In xdWork1.SelectNodes("ElencoFattura/Fattura/SezioniDocumenti/DatiServizio/PuntoDiFornitura/GruppoMisura")
                    xnItem1 = xnItem.SelectSingleNode("GruppoMisura")
                    If Not xnItem1 Is Nothing Then
                        xnItem2 = xdWork1.CreateNode(Xml.XmlNodeType.Element, "GruppoMisura1", "")
                        xnItem2.InnerText = xnItem1.InnerText
                        xnItem.AppendChild(xnItem2.Clone)
                        xnItem.RemoveChild(xnItem1)
                        For Each xnItem3 As Xml.XmlNode In xnItem.SelectNodes("StrumentoMisura")
                            xnItem1 = xnItem3.SelectSingleNode("StrumentoMisura")
                            xnItem2 = xdWork1.CreateNode(Xml.XmlNodeType.Element, "StrumentoMisura1", "")
                            xnItem2.InnerText = xnItem1.InnerText
                            xnItem3.AppendChild(xnItem2.Clone)
                            xnItem3.RemoveChild(xnItem1)
                        Next
                    End If
                Next
                For Each xnItem As Xml.XmlNode In xdWork1.SelectNodes("ElencoFattura/Fattura/SezioniDocumenti/Contratto/OT")
                    xnItem1 = xnItem.SelectSingleNode("OT")
                    If Not xnItem1 Is Nothing Then
                        xnItem2 = xdWork1.CreateNode(Xml.XmlNodeType.Element, "OT1", "")
                        xnItem2.InnerText = xnItem1.InnerText
                        xnItem.AppendChild(xnItem2.Clone)
                        xnItem.RemoveChild(xnItem1)
                    End If
                Next
                'xdWork1.Save(cFileName)
                SalvaFileDatiXML(cFileName, xdWork1)
                nDim = New System.IO.FileInfo(cFileName).Length
                If (nDim < UpperFileSize) Then
                    _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
                    cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))
                    'xdWork1.Save(cDstFile)
                    SalvaFileDatiXML(cDstFile, xdWork1)

                    'System.IO.File.Copy(cFileName, cDstFile)
                    _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                    _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
                Else
                    _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                    _MessaggiAvanzamento(5) = "Suddivizione del file nella cartella di archiviazione..."

                    nRecordtoAnalized = xdWork1.SelectNodes("ElencoFattura/Fattura").Count
                    nRecordXFile = Int(LowerFileSize / (nDim / nRecordtoAnalized)) + 1
                    _MessaggiAvanzamento(4) = CInt(_MessaggiAvanzamento(4)) + Int(nRecordtoAnalized / nRecordXFile) + 1
                    nRecord = 0
                    nFile = 1
                    For Each xiWork In xdWork1.SelectNodes("ElencoFattura/Fattura")
                        If nRecord = 0 Then
                            xdWork2 = New Xml.XmlDocument
                            xdWork2.AppendChild(xdWork2.CreateNode(Xml.XmlNodeType.Element, "ElencoFattura", ""))
                        End If
                        xdWork2.SelectSingleNode("ElencoFattura").AppendChild(xdWork2.ImportNode(xiWork, True))
                        nRecord += 1
                        nRecordtoAnalized -= 1
                        If (nRecord = nRecordXFile) Or (nRecordtoAnalized = 0) Then
                            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                            cDstFile = cFileName.Substring(cFileName.LastIndexOf("\") + 1)
                            cDstFile = String.Concat(cDstFile.Substring(0, cDstFile.IndexOf(".")), "_", nFile.ToString.PadLeft(5, "0"), cDstFile.Substring(cDstFile.IndexOf(".")))
                            cDstFile = String.Concat(_SessioneDatiPath, cDstFile)
                            'xdWork2.Save(cDstFile)
                            SalvaFileDatiXML(cDstFile, xdWork2)
                            xdWork2 = Nothing
                            _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
                            nRecord = 0
                            nFile += 1
                        End If
                    Next
                    xdWork1 = Nothing
                End If
            Catch ex As Exception
                MessageBox.Show(String.Concat("Attenzione il file ", cFileName, " non � correttamente formattato.", vbCr, "L'errore � riportato di seguito:", vbCr, ex.Message), "Errore di caricamento di un file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))
                System.IO.File.Copy(cFileName, cDstFile)
                _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, -1})
            End Try
        Else
            _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
            cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

            System.IO.File.Copy(cFileName, cDstFile)
            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
            _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
        End If

    End Sub

    Private Function GetRecordValue(ByVal cLine As String, ByVal aRecordDef As Integer()) As String()
        Dim aValue As String()
        Dim nIdx As Integer
        Dim i As Integer

        ReDim aValue(aRecordDef.Length - 1)
        i = 0
        nIdx = 0
        For Each nPos As Integer In aRecordDef
            aValue(i) = Trim(cLine.Substring(nIdx, aRecordDef(i)))
            nIdx += aRecordDef(i)
            i += 1
        Next
        Return aValue

    End Function

    Private Function String2Number(ByVal cText As String, ByVal nPrec As Integer) As Double
        Dim nRtn As Double

        If cText = "" Then
            nRtn = 0
        Else
            nRtn = cText
        End If
        Return nRtn

    End Function

    Private Function DateDefault(ByVal cText As String) As DateTime

        If cText = "" Then
            cText = "01/01/1900"
        End If
        Return CDate(cText)

    End Function

#End Region

#Region "Caricamento del PRINTDATASET"

    ' ************************************************************************** '
    ' Caricamento del DataSet da utilizzare per la stampa.                       '
    ' ************************************************************************** '
    Public Sub LoadPrintDataset(ByVal nRecordToLoad As Integer) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LoadFullDataset
        Dim dr_mai_qd As DS_QD_ASM_tari.TBL_mainRow
        Dim nRecordToPrint As Integer

        ' ************************************************************************** '
        ' Creazione del Dataset e azzeramento del contatore dei record da stampare.  '
        ' ************************************************************************** '
        nRecordToPrint = 0
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                _PrintFatturaDataset = New DS_PF_ASM_tari
            Case Is = ListaDataset.DAS_bollettino
                '_PrintBollettinoDataset = New DS_PB_ASM_tari
                _PrintF24Dataset = New DS_PF24_ASM_tari
            Case Is = ListaDataset.DAS_send_email
                _SendEmailDataset = New DS_SE_ASM_tari
        End Select

        ' ************************************************************************** '
        ' Caricamento del DataSet da utilizzare per la stampa.                       '
        ' ************************************************************************** '
        _CodiceProgressivo = 0
        While _RecordCount < _QuickDataset.TBL_main.Rows.Count And nRecordToPrint < nRecordToLoad
            dr_mai_qd = _QuickDataset.TBL_main.Rows(_RecordCount)
            If dr_mai_qd.DEF_toprint Then
                Try
                    _ErroriLievi = ""
                    AddRecordToDS(dr_mai_qd)
                    If _ErroriLievi > "" Then
                        dr_mai_qd.DEF_errcode = -2
                        dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori lievi: ", _ErroriLievi)
                    End If
                Catch ex As Exception
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = ex.Message
                End Try
                nRecordToPrint += 1
            End If
            _RecordCount += 1
        End While

        ' ************************************************************************** '
        ' Consolidamento del DataSet da utilizzare per la stampa.                    '
        ' ************************************************************************** '
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                _PrintFatturaDataset.AcceptChanges()
            Case Is = ListaDataset.DAS_bollettino
                '_PrintBollettinoDataset.AcceptChanges()
                _PrintF24Dataset.AcceptChanges()
            Case Is = ListaDataset.DAS_send_email
                _SendEmailDataset.AcceptChanges()
        End Select

    End Sub

    'Private Sub AddRecordToPrint(ByVal dr_mai_qd As DS_QD_ASM_tia_tares.TBL_mainRow)

    '    Try
    '        Select Case _CodiceModulo
    '            Case Is = ListaDataset.DAS_fattura
    '                GetRecordFatturaToPrint(dr_mai_qd.MAI_filedati, dr_mai_qd.MAI_codice, GetFeedString(dr_mai_qd))
    '            Case Is = ListaDataset.DAS_bollettino
    '                GetRecordBollettiniToPrint(dr_mai_qd.MAI_filedati, dr_mai_qd.MAI_codice, GetFeedString(dr_mai_qd))
    '            Case Is = ListaDataset.DAS_bollettino_tares
    '                GetRecordBollettiniTaresToPrint(dr_mai_qd.MAI_filedati, dr_mai_qd.MAI_codice, GetFeedString(dr_mai_qd))
    '        End Select
    '    Catch ex As Exception
    '        Dim cErrorString As String
    '        If ex.Message = "" Then
    '            cErrorString = "Errore caricamento dati, mentra si caricava il dato: SCONOSCIUTO"
    '        Else
    '            cErrorString = String.Concat("Errore caricamento dati, mentra si caricava il dato: ", ex.Message)
    '        End If
    '        Throw New Exception(cErrorString, ex)
    '    End Try

    'End Sub

    Private Function GetFeedString(ByVal dr_mai_qd As DS_QD_ASM_tari.TBL_mainRow) As String
        Dim lAllModuli As Boolean = True
        Dim cRtn As String

        cRtn = ""
        If _FeedString > "" Then
            For i As Integer = 1 To _FeedString.Split(";").Length - 1 Step 2
                If dr_mai_qd.Item(_FeedString.Split(";")(i)) Then
                    cRtn = _FeedString.Split(";")(i + 1)
                Else
                    lAllModuli = False
                End If
            Next
            If lAllModuli Then
                cRtn = _FeedString.Split(";")(0)
            End If
        End If
        Return cRtn

    End Function

#End Region

#Region "Caricamento del PRINTDATSET della fattura"

    Private Sub GetRecordFatturaToPrint(ByVal nCodice As Integer, ByVal nLinkQD As Integer, ByVal cFeedString As String)
        ''Dim dr_mai_tra As DS_import.TBL_mainRow
        ''Dim dr_dfa As DS_PF_ASM_ele.TBL_dati_fattRow
        ''Dim dr_daf As DS_PF_ASM_ele.TBL_dati_fornRow
        'Dim dr_uxx_tra As DS_import.TBL_record_UXXRow

        'If _data_import.TBL_record_UXX.Select(String.Concat("UXX_progre = ", nCodice)).Length = 1 Then
        '    dr_uxx_tra = _data_import.TBL_record_UXX.Select(String.Concat("UXX_progre = ", nCodice))(0)
        '    _PrintFatturaDataset.TBL_record_UXX.ImportRow(dr_uxx_tra)

        '    ' Carico i dati che occorrono cos� come sono memorizzati nel database di transito.
        '    With CType(_PrintFatturaDataset.TBL_record_UXX.Rows(_PrintFatturaDataset.TBL_record_UXX.Rows.Count - 1), DS_PF_ASM_tia_tares.TBL_record_UXXRow)
        '        .UXX_linkQD = nLinkQD
        '        .DEF_alimimb = cFeedString
        '        .DEF_raccolta = 9
        '    End With

        '    For Each dr_ixx As DS_import.TBL_record_IXXRow In dr_uxx_tra.GetChildRows("TBL_record_UXX_TBL_record_IXX")
        '        _PrintFatturaDataset.TBL_record_IXX.ImportRow(dr_ixx)
        '    Next

        '    For Each dr_pxx As DS_import.TBL_record_PXXRow In dr_uxx_tra.GetChildRows("TBL_record_UXX_TBL_record_PXX")
        '        _PrintFatturaDataset.TBL_record_PXX.ImportRow(dr_pxx)
        '    Next

        '    For Each dr_mxx As DS_import.TBL_record_MXXRow In dr_uxx_tra.GetChildRows("TBL_record_UXX_TBL_record_MXX")
        '        _PrintFatturaDataset.TBL_record_MXX.ImportRow(dr_mxx)
        '    Next

        '    For Each dr_f00 As DS_import.TBL_record_F00Row In dr_uxx_tra.GetChildRows("TBL_record_UXX_TBL_record_F00")
        '        _PrintFatturaDataset.TBL_record_F00.ImportRow(dr_f00)
        '    Next

        '    For Each dr_fxx As DS_import.TBL_record_FXXRow In dr_uxx_tra.GetChildRows("TBL_record_UXX_TBL_record_FXX")
        '        _PrintFatturaDataset.TBL_record_FXX.ImportRow(dr_fxx)
        '    Next

        '    For Each dr_txx As DS_import.TBL_record_TXXRow In dr_uxx_tra.GetChildRows("TBL_record_UXX_TBL_record_TXX")
        '        _PrintFatturaDataset.TBL_record_TXX.ImportRow(dr_txx)

        '        For Each dr_rxx As DS_import.TBL_record_RXXRow In dr_txx.GetChildRows("TBL_record_TXX_TBL_record_RXX")
        '            _PrintFatturaDataset.TBL_record_RXX.ImportRow(dr_rxx)
        '        Next
        '    Next

        'End If


    End Sub

    Private Sub LoadFileMdb()
        'Dim oComBuil As OleDb.OleDbCommandBuilder
        'Dim oDa As OleDb.OleDbDataAdapter
        'Dim oCmd As OleDb.OleDbCommand
        'Dim cDBFileName As String
        'Dim cConnACCStr As String

        ''  _data_import = New DS_import
        'cDBFileName = _SessioneDati.SelectSingleNode("filename[@tipo='103']").InnerText
        'cDBFileName = String.Concat(_SessionePath, cDBFileName)
        'cConnACCStr = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cDBFileName, ";")

        'oCmd = New OleDb.OleDbCommand
        'oCmd.Connection = New OleDb.OleDbConnection(cConnACCStr)

        '' Salvataggio della tabella TBL_main
        'oCmd.CommandText = "SELECT * FROM TBL_record_UXX"
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_import.TBL_record_UXX)

        '' Salvataggio della tabella TBL_main
        'oCmd.CommandText = "SELECT * FROM TBL_record_F00"
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_import.TBL_record_F00)

        '' Salvataggio della tabella TBL_main
        'oCmd.CommandText = "SELECT * FROM TBL_record_FXX order by FXX_sort"
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_import.TBL_record_FXX)

        '' Salvataggio della tabella TBL_main
        'oCmd.CommandText = "SELECT * FROM TBL_record_PXX"
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_import.TBL_record_PXX)

        '' Salvataggio della tabella TBL_main
        'oCmd.CommandText = "SELECT * FROM TBL_record_IXX"
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_import.TBL_record_IXX)

        '' Salvataggio della tabella TBL_consumi
        'oCmd.CommandText = "SELECT * FROM TBL_record_MXX"
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_import.TBL_record_MXX)

        '' Salvataggio della tabella TBL_riep_cons
        'oCmd.CommandText = "SELECT * FROM TBL_record_TXX"
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_import.TBL_record_TXX)

        '' Salvataggio della tabella TBL_riep_cons_ind
        'oCmd.CommandText = "SELECT * FROM TBL_record_RXX ORDER BY RXX_da, RXX_sort"
        'oDa = New OleDb.OleDbDataAdapter(oCmd)
        'oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        'oDa.Fill(_data_import.TBL_record_RXX)

        '' Salvataggio della tabella TBL_tares
        ''oCmd.CommandText = "SELECT * FROM TBL_tares"
        ''oDa = New OleDb.OleDbDataAdapter(oCmd)
        ''oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        ''oDa.Fill(_data_import.TBL_tares)

    End Sub

    Private Sub CheckFieldValue(ByVal cValue As String, ByVal cMsgError As String)

        If cValue = "" And cMsgError > "" Then
            AddErroreLieve(cMsgError)
        End If

    End Sub

    Private Function SetDataScadenza(ByVal cFlagTipoPag As String, ByVal dDataForzata As String) As String
        Dim cCondizioneForzaScadenza As String

        cCondizioneForzaScadenza = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "17")
        If (cCondizioneForzaScadenza.Length > 0) And (cCondizioneForzaScadenza.ToUpper.Contains(cFlagTipoPag.ToUpper)) Then
            dDataForzata = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "16")
        End If
        Return dDataForzata

    End Function

#End Region

#Region "Caricamento del PRINTDATSET del bollettino."

    Private Sub GetRecordBollettinoToPrint(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_fat_imp As DataRow, ByVal cFeedString As String)
        '  Dim dr_dfa_tra As DS_import.TBL_dati_fattRow
        ' Dim dr_daf_tra As DS_import.TBL_dati_fornRow
        Dim dr_fxr As DS_PB_ASM_tari.TBL_fat_x_ratRow
        Dim dr_rdo As DS_PB_ASM_tari.TBL_rich_domRow
        Dim dr_fat As DS_PB_ASM_tari.TBL_fattureRow
        Dim dr_rat As DS_PB_ASM_tari.TBL_rateRow
        Dim nRate As Integer

        dr_fat = _PrintBollettinoDataset.TBL_fatture.NewRow
        With dr_fat
            .FAT_codice = _PrintBollettinoDataset.TBL_fatture.Rows.Count + 1
            .FAT_prot_reg_iva = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "Protocollo")
            .FAT_num_fattura = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "NumeroFattura")
            .FAT_data_fattura = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "DataFattura")
            .FAT_nominativo = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "RagioneSociale")
            .FAT_cod_utente = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "IDSoggetto")
            .FAT_ind_rec_1 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "TipoElementoTopologico")
            .FAT_ind_rec_2 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "ElementoTopologico", "")
            .FAT_ind_rec_3 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "NumeroCivico", "")
            .FAT_ind_rec_4 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "SuffissoCivico", "")
            .FAT_ind_rec_5 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "Localita", "")
            .FAT_ind_rec_6 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "Comune")
            .FAT_ind_rec_7 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "Cap").ToString().PadLeft(5, "0")
            .FAT_ind_rec_8 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "ProvinciaSigla")
        End With
        'With dr_fat
        '    .FAT_codice = _PrintBollettinoDataset.TBL_fatture.Rows.Count + 1
        '    .FAT_anno_fattura = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "AnnoFattura")
        '    .FAT_num_fattura = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "NumeroFattura")
        '    If RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "DataNascita") = "" Then
        '        .FAT_cognome = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "RagioneSociale")
        '        .FAT_nome = ""
        '        .FAT_codice_fiscale = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "PartitaIva")
        '        .FAT_data_nascita = ""
        '        .FAT_sesso = ""
        '        .FAT_comune_nasc = ""
        '        .FAT_prov_nasc = ""
        '    Else
        '        .FAT_cognome = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "Cognome")
        '        .FAT_nome = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "Nome")
        '        .FAT_codice_fiscale = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "CodiceFiscale")
        '        .FAT_data_nascita = CDate(RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "DataNascita")).ToString("ddMMyyyy")
        '        .FAT_sesso = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "Sesso")
        '        If RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "NazioneNascita") = "ITALIA" Then
        '            .FAT_comune_nasc = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "ComuneNascita")
        '            .FAT_prov_nasc = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "ProvinciaNascita")
        '        Else
        '            .FAT_comune_nasc = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "NazioneNascita")
        '            .FAT_prov_nasc = "EE"
        '        End If
        '    End If
        '    If RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "ImportoRata") > "" Then
        '        .FAT_importo_totale = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "ImportoRata")
        '    Else
        '        .FAT_importo_totale = 0
        '    End If
        'End With
        _PrintBollettinoDataset.TBL_fatture.Rows.Add(dr_fat)

        dr_rat = _PrintBollettinoDataset.TBL_rate.NewRow
        'With dr_rat
        '    .RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1
        '    .RAT_sezione = "EL"
        '    .RAT_cod_tributo = "3944"
        '    .RAT_codice_ente = "F844"
        '    .RAT_num_immobili = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "NumeroSEZ", "NumeroSEZ")
        '    .RAT_rateizzazione = "0101"
        '    .RAT_anno_riferimento = "2014"
        '    .RAT_codice_identificativo = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "OCR")
        '    .RAT_scadenza = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "DataScadenza")
        '    If RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "ImportoRata") > "" Then
        '        .RAT_importo = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "ImportoRata")
        '    Else
        '        .RAT_importo = 0
        '    End If
        'End With
        With dr_rat
            .RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1
            .RAT_num_rata = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "NumeroRata")
            .RAT_scadenza = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "DataScadenza")
            '.RAT_importo = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "RiepilogoIVA", "TotaleFattura", "0")
            If RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "ImportoRata") > "" Then
                .RAT_importo = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "ImportoRata")
            Else
                .RAT_importo = 0
            End If

            .RAT_ocr = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "OCR")
        End With
        _PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat)

        ' Carico i record nella tabella delle rate.5
        nRate = 1    ' dr_uxx_tra.GetChildRows("TBL_main_TBL_dati_fatt").Length
        For i As Integer = 0 To nRate - 1
            ''dr_dfa_tra = dr_mai_tra.GetChildRows("TBL_main_TBL_dati_fatt")(i)
            ''dr_daf_tra = dr_mai_tra.GetChildRows("TBL_main_TBL_dati_forn")(i)
            'dr_rat = _PrintBollettinoDataset.TBL_rate.NewRow
            'dr_fat.FAT_cod_cli = dr_uxx_tra.UXX_CODUTE       'dr_mai_tra.MAI_cod_cli
            'dr_fat.FAT_num_fattura = dr_uxx_tra.UXX_NUMFAT   ' dr_dfa_tra.DFA_num_fatt
            'dr_fat.FAT_progr_fatt = dr_uxx_tra.UXX_numero_fattura
            'dr_fat.FAT_data_fattura = dr_uxx_tra.UXX_DATEMI  ' dr_dfa_tra.DFA_data_emis
            'With dr_rat
            '    .RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1
            '    .RAT_num_rata = ""
            '    .RAT_scadenza = dr_f00_tra.F00_SCATOT                            ' dr_dfa_tra.DFA_data_scad
            '    .RAT_emissione = dr_uxx_tra.UXX_DATEMI                           'dr_dfa_tra.DFA_data_emis
            '    .RAT_per_rif = String.Concat(dr_uxx_tra.UXX_DA.ToString("dd/MM/yyyy"), " - ", dr_uxx_tra.UXX_A.ToString("dd/MM/yyyy"))                 'dr_dfa_tra.DFA_per_rif
            '    .RAT_matricola = "0"    'dr_mai_tra.MAI_cod_cli
            '    .RAT_importo = dr_f00_tra.F00_totapag                                          'dr_dfa_tra.DFA_totale_fatt
            '    .RAT_ocr = dr_f00_tra.F00_ocr                              'dr_dfa_tra.DFA_ocr
            'End With
            '_PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat)

            '' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
            'dr_fxr = _PrintBollettinoDataset.TBL_fat_x_rat.NewRow
            'dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1
            'dr_fxr.DEF_alimimb = cFeedString
            '' per il bollettino non dovrebbero esserci problemi di raccolta.
            '' Al momento il campo viene valorizzato con questi valori per le fasi successive si vedr�.
            'dr_fxr.DEF_raccolta = 3
            'dr_fxr.FXR_linkQD = nLinkQD
            'dr_fxr.FXR_cod_fat = dr_fat.FAT_codice
            'dr_fxr.FXR_cod_rat = dr_rat.RAT_codice

            '' Mi serve per controllare il codice dell'imbustatrice.
            'If i = nRate - 1 Then
            '    dr_fxr.FXR_ultimo = 1
            'End If
            '_PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr)
        Next

        ' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
        dr_fxr = _PrintBollettinoDataset.TBL_fat_x_rat.NewRow
        dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1
        dr_fxr.DEF_alimimb = cFeedString
        dr_fxr.DEF_raccolta = 3
        dr_fxr.FXR_linkQD = nCodice
        dr_fxr.FXR_cod_fat = dr_fat.FAT_codice
        dr_fxr.FXR_cod_rat = dr_rat.RAT_codice
        _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr)

        ' Carico i record nella richiesta di domiciliazione 
        dr_rdo = _PrintBollettinoDataset.TBL_rich_dom.NewRow
        With dr_rdo
            .RDO_codice = _PrintBollettinoDataset.TBL_rich_dom.Rows.Count + 1
            .RDO_cod_fat = dr_fat.FAT_codice
            .RDO_cod_sia = GetValueDatiFissi("FixBol", "6")
            .RDO_tipo_serv = GetValueDatiFissi("FixBol", "7")
            .RDO_cod_domic = ""
        End With
        _PrintBollettinoDataset.TBL_rich_dom.Rows.Add(dr_rdo)

    End Sub

#End Region

#Region "Caricamento del PRINTDATSET del bollettino Tares."

    'Private Sub GetRecordBollettiniTaresToPrint(ByVal nCodice As Integer, ByVal nLinkQD As Integer, ByVal cFeedString As String)
    '    '  Dim dr_dfa_tra As DS_import.TBL_dati_fattRow
    '    ' Dim dr_daf_tra As DS_import.TBL_dati_fornRow
    '    Dim dr_fxr As DS_PB_ASM_tares.TBL_fat_x_ratRow
    '    'Dim dr_rdo As DS_PB_ASM_tia_tares.TBL_rich_domRow
    '    Dim dr_tar As DS_PB_ASM_tares.TBL_taresRow
    '    'Dim dr_rat As DS_PB_ASM_tia_tares.TBL_rateRow
    '    ' Dim dr_uxx_tra As DS_import.TBL_record_UXXRow
    '    'Dim dr_f00_tra As DS_import.TBL_record_F00Row
    '    'Dim dr_tar_tra As DS_import.TBL_taresRow
    '    'Dim nRate As Integer
    '    Dim cTmp As String
    '    Dim nTmp As Integer
    '    Dim aLetter As String()

    '    aLetter = New String() {"A", "B", "C", "D", "E", "F", "G", "H", "I", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "Z"}
    '    'dr_uxx_tra = _data_import.TBL_record_UXX.FindByUXX_PROGRE(nCodice)
    '    '  dr_tar_tra = dr_uxx_tra.GetChildRows("TBL_record_UXX_TBL_tares")(0)

    '    ' Carico per ciascuna fattura un record sulla tabella delle fatture e tanti record su quella dei bollettini.
    '    ' cos� posso far guidare la stampa dei bollettini da una tabella che originer� successivamente.
    '    dr_tar = _PrintBollettinoTaresDataset.TBL_tares.NewRow
    '    With dr_tar
    '        '.TAR_codice = _PrintBollettinoTaresDataset.TBL_tares.Rows.Count + 1
    '        '.TAR_numfat = dr_uxx_tra.UXX_NUMFAT
    '        '.TAR_progr_fatt = dr_uxx_tra.UXX_numero_fattura
    '        '.TAR_cod_cat = dr_tar_tra.TAR_cod_cat
    '        '.TAR_cod_fis = dr_tar_tra.TAR_cod_fis
    '        '.TAR_nominativo = dr_uxx_tra.UXX_RAGSOC
    '        'If Not IsDBNull(dr_tar_tra("TAR_data_nasc")) Then
    '        '    .TAR_data_nasc = dr_tar_tra.TAR_data_nasc
    '        'End If

    '        '.TAR_sesso = dr_tar_tra.TAR_sesso
    '        '.TAR_comune_nasc = dr_tar_tra.TAR_comune_nasc
    '        '.TAR_prov_nasc = dr_tar_tra.TAR_prov_nasc
    '        '.TAR_mag_num_imm = dr_tar_tra.TAR_mag_num_imm
    '        '.TAR_mag_tipo_tri = dr_tar_tra.TAR_mag_tipo_tri
    '        '.TAR_mag_anno_rif = dr_tar_tra.TAR_mag_anno_rif
    '        '.TAR_mag_importo = dr_tar_tra.TAR_mag_importo

    '        '.TAR_tar_num_imm = dr_tar_tra.TAR_tar_num_imm
    '        '.TAR_tar_tipo_tri = dr_tar_tra.TAR_tar_tipo_tri
    '        '.TAR_tar_anno_rif = dr_tar_tra.TAR_tar_anno_rif
    '        '.TAR_tar_importo = dr_tar_tra.TAR_tar_importo

    '        '.TAR_tri_num_imm = dr_tar_tra.TAR_tri_num_imm
    '        '.TAR_tri_tipo_tri = dr_tar_tra.TAR_tri_tipo_tri
    '        '.TAR_tri_anno_rif = dr_tar_tra.TAR_tri_anno_rif
    '        '.TAR_tri_importo = dr_tar_tra.TAR_tri_importo
    '        '.TAR_importo = .TAR_mag_importo + .TAR_tar_importo + .TAR_tri_importo
    '        '' Calcolo dell'OCR
    '        'cTmp = .TAR_mag_anno_rif.ToString(3)
    '        'cTmp = String.Concat(cTmp, Array.IndexOf(aLetter, .TAR_cod_cat(0).ToString.ToUpper) + 1)
    '        ''cTmp = String.Concat(cTmp, Asc(.TAR_cod_cat(0).ToString.ToUpper) - 64)
    '        'cTmp = String.Concat(cTmp, .TAR_cod_cat.Substring(1))
    '        'cTmp = String.Concat(cTmp, .TAR_numfat.Replace("TI13", "").ToString().PadLeft(10, "0"))
    '        ''cTmp.PadRight(16, "0")
    '        'nTmp = cTmp Mod 93
    '        '.TAR_ocr = String.Concat(cTmp, nTmp.ToString.PadLeft(2, "0"))
    '        '.TAR_print_dm = (.TAR_cod_fis.Length = 16 And .TAR_comune_nasc > "") Or (.TAR_cod_fis.Length = 11)
    '    End With
    '    _PrintBollettinoTaresDataset.TBL_tares.Rows.Add(dr_tar)


    '    ' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
    '    dr_fxr = _PrintBollettinoTaresDataset.TBL_fat_x_rat.NewRow
    '    dr_fxr.FXR_codice = _PrintBollettinoTaresDataset.TBL_fat_x_rat.Rows.Count + 1
    '    dr_fxr.DEF_alimimb = cFeedString
    '    ' per il bollettino non dovrebbero esserci problemi di raccolta.
    '    ' Al momento il campo viene valorizzato con questi valori per le fasi successive si vedr�.
    '    dr_fxr.DEF_raccolta = 3
    '    dr_fxr.FXR_linkQD = nLinkQD
    '    dr_fxr.FXR_cod_fat = dr_tar.TAR_codice
    '    _PrintBollettinoTaresDataset.TBL_fat_x_rat.Rows.Add(dr_fxr)

    'End Sub

#End Region

    Private Function GetValueFromXML(ByVal xmlItem As Xml.XmlNode, ByVal xmlPath As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cDefault
            If Not xmlItem.SelectSingleNode(xmlPath) Is Nothing Then
                cValue = xmlItem.SelectSingleNode(xmlPath).InnerText
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori del file XML.", vbLf, "percorso da cui si tenta il recupero:", vbLf, xmlPath), ex)
        End Try
        Return cValue

    End Function

    Private Function GetValueFromText(ByVal cText As String, ByVal nStart As Integer, ByVal nLength As Integer, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cText.Substring(nStart, nLength)
            If cValue.Trim = "" And cDefault > "" Then
                cValue = cDefault
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori da un testo.", vbLf, "Dati per il recupero: ", vbLf, "- inizio ", nStart, vbLf, "- fine ", nLength), ex)
        End Try
        Return cValue

    End Function

    Private Sub AddErroreLieve(ByVal cMsg As String)

        If _ErroriLievi = "" Then
            _ErroriLievi = cMsg
        Else
            _ErroriLievi = String.Concat(_ErroriLievi, "; ", cMsg)
        End If

    End Sub

    Private Sub ImpostaMessaggi(ByVal aValue As String())
        Dim i As Integer

        For i = 0 To 5
            If aValue(i) > "" Then
                _MessaggiAvanzamento(i) = aValue(i)
            End If
        Next

    End Sub

    Private Function GetTrasformazioni(ByVal cTable As String, ByVal cCodice As String, ByVal nTipo As Integer) As String
        Dim xdTrasf As Xml.XmlDocument
        Dim xiWork As Xml.XmlNode
        Dim cRtn As String

        Try
            xdTrasf = New Xml.XmlDocument
            xdTrasf.Load(_SessioneTrasformazioniFileName)
            xiWork = xdTrasf.SelectSingleNode(String.Concat("translation/", cTable, "/item[codice = '", cCodice, "']"))
            cRtn = ""
            If Not xiWork Is Nothing Then
                If nTipo = 1 Then
                    cRtn = xiWork.SelectSingleNode("descr_breve").InnerText
                End If
                If nTipo = 2 Then
                    cRtn = xiWork.SelectSingleNode("descrizione").InnerText
                End If
            End If
            xdTrasf = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    Private Function GetDataSendByMail() As Xml.XmlNode
        'Dim xnWork As Xml.XmlNode
        'Dim xdWork As Xml.XmlDocument

        'xdWork = New Xml.XmlDocument
        'xnWork = xdWork.CreateNode(Xml.XmlNodeType.Element, "SendByMail", "")
        'Select Case _CodiceModulo
        '    Case Is = ListaModuli.MOD_fattura
        '        With CType(_PrintFatturaDataset.TBL_main.Rows(0), DS_PF_UE_ele.TBL_mainRow)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_codice", .MAI_linkQD)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_cod_utente", .MAI_cod_cli)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_nominativo", .MAI_nome)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_mail_address", "lproietti@gmail.com")
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_codice", .MAI_linkQD.ToString))
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_cod_utente", .MAI_cod_cli.ToString))
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_nominativo", .MAI_nome.ToString))
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_mail_address", "lproietti@gmail.com"))
        '        End With
        'End Select
        'Return xnWork

    End Function

    Private Sub CreateXmlNode(ByVal xdDoc As Xml.XmlDocument, ByVal xnNode As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xdDoc.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xnNode.AppendChild(xiTmp)

    End Sub

#Region "Elenco procedure Generiche"

    Private Function SetExceptionItem(ByVal cProcError As String, ByVal ex As Exception) As Exception
        Dim ex_rtn As Exception

        If ex.InnerException Is Nothing Then
            ex_rtn = New Exception(String.Concat("Errore (", cProcError, "): ", ex.Message), ex)
        Else
            ex_rtn = ex
        End If
        Return ex_rtn

    End Function

#End Region

    Private Function GetFieldValue(ByVal aField As Array, ByVal cField As String, ByVal aValue As String(), Optional ByVal cDefault As String = "") As String
        Dim nIdx As Integer
        Dim cRtn As String

        nIdx = Array.IndexOf(aField, cField)
        If (nIdx > -1) And nIdx < aValue.Length Then
            cRtn = aValue(nIdx)
        Else
            cRtn = cDefault
        End If
        Return cRtn

    End Function

#Region "Caricamento dei dati per la generazione della sessione"

    Public Sub LoadQuickDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LoadQuickDataset
        Dim dr_mai_qd As DS_QD_ASM_tari.TBL_mainRow
        Dim lLoad As Boolean

        _QuickDataset = New DS_QD_ASM_tari
        _xmlDati_qd = New Xml.XmlDocument
        _xmlDati_qd.Load(QuickDataSet_IndexFile)
        ImpostaMessaggi(New String() {"", "", "", "0", _xmlDati_qd.SelectNodes(Costanti.itm_qd_Fattura).Count, "Caricamento dei dati...."})

        For Each xmlItem As Xml.XmlNode In _xmlDati_qd.SelectNodes(Costanti.itm_qd_Fattura)
            lLoad = _TipoCaricamento = 1 Or (_TipoCaricamento = 2 And GetValueFromXML(xmlItem, "DEF_sendbymail").ToLower = "true")
            If lLoad Then
                dr_mai_qd = _QuickDataset.TBL_main.NewRow
                Try
                    dr_mai_qd.MAI_codice = GetValueFromXML(xmlItem, "MAI_codice")
                    dr_mai_qd.DEF_toprint = False
                    dr_mai_qd.DEF_errcode = GetValueFromXML(xmlItem, "DEF_errcode")
                    dr_mai_qd.DEF_errdesc = GetValueFromXML(xmlItem, "DEF_errdesc")
                    dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
                    dr_mai_qd.DEF_sendbymail = GetValueFromXML(xmlItem, "DEF_sendbymail").ToLower = "true"
                    dr_mai_qd.DEF_onlyarchott = GetValueFromXML(xmlItem, "DEF_onlyarchott").ToLower = "si"
                    dr_mai_qd.MAI_num_utente = GetValueFromXML(xmlItem, "MAI_num_utente")
                    dr_mai_qd.MAI_num_fattura = GetValueFromXML(xmlItem, "MAI_num_fattura")
                    dr_mai_qd.MAI_nominativo = GetValueFromXML(xmlItem, "MAI_nominativo")
                    dr_mai_qd.MAI_indirizzo_recapito = GetValueFromXML(xmlItem, "MAI_indirizzo_recapito")
                    dr_mai_qd.MAI_citta_recapito = GetValueFromXML(xmlItem, "MAI_citta_recapito")
                    dr_mai_qd.MAI_cap_recapito = GetValueFromXML(xmlItem, "MAI_cap_recapito")
                    dr_mai_qd.MAI_provincia_recapito = GetValueFromXML(xmlItem, "MAI_provincia_recapito")
                    dr_mai_qd.MAI_nazione_recapito = GetValueFromXML(xmlItem, "MAI_nazione_recapito")
                    dr_mai_qd.MAI_print_F24S = GetValueFromXML(xmlItem, "MAI_print_F24S").ToLower = "si"
                    'dr_mai_qd.MAI_print_bol_tares = GetValueFromXML(xmlItem, "MAI_print_bol_tares").ToLower = "si"
                    dr_mai_qd.MAI_print_all = GetValueFromXML(xmlItem, "MAI_print_all").ToLower = "si"
                    dr_mai_qd.MAI_filedati = GetValueFromXML(xmlItem, "MAI_filedati")
                    dr_mai_qd.MAI_IDFattura = GetValueFromXML(xmlItem, "MAI_IDFattura")
                Catch ex As Exception
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = ex.Message
                End Try
                ImpostaMessaggi(New String() {"", "", "", CInt(_MessaggiAvanzamento(3)) + 1, "", ""})
                _QuickDataset.TBL_main.Rows.Add(dr_mai_qd)
            End If
        Next
        LoadFileMdb()

    End Sub

#End Region

    Public Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckCondPSA
        Dim cValue As String
        Dim lCond As Boolean

        cValue = ""
        lCond = False
        Return lCond

    End Function

    Public Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckCondAPI
        Dim cValue As String
        Dim lCond As Boolean

        cValue = ""
        lCond = False
        Return lCond

    End Function

    Public Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ExecActionPSA

    End Sub

    Public Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ExecActionAPI

    End Sub

    ReadOnly Property CapacitaInvioMail() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CapacitaInvioMail

        Get
            Return True
        End Get

    End Property

    ReadOnly Property DatiAzioniPostStampa() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DatiAzioniPostStampa

        Get
            Dim tmpArrList As ArrayList

            tmpArrList = New ArrayList
            tmpArrList.Add(New String() {"C", "1001", "Consorzio di acquisto"})
            tmpArrList.Add(New String() {"A", "1001", "Estrai file porzione file xml"})

            Return tmpArrList
        End Get

    End Property

    ReadOnly Property DatiAzioniPreInvio() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DatiAzioniPreInvio

        Get
            Dim tmpArrList As ArrayList

            tmpArrList = New ArrayList
            tmpArrList.Add(New String() {"C", "1001", "Consorzio di acquisto"})
            tmpArrList.Add(New String() {"A", "1001", "Allega Porzione di file xml alla mail"})

            Return tmpArrList
        End Get

    End Property

    ReadOnly Property StampaInProprio() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.StampaInProprio

        Get
            Return True
        End Get

    End Property

    Public ReadOnly Property FullDataset() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataset

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return _PrintFatturaDataset
                Case Is = ListaDataset.DAS_bollettino
                    'Return _PrintBollettinoDataset
                    Return _PrintF24Dataset
                Case Is = ListaDataset.DAS_send_email
                    Return _SendEmailDataset
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataSet_MainTableName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataSet_MainTableName

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "TBL_main"
                Case Is = ListaDataset.DAS_bollettino
                    Return "TBL_fat_x_rat"
                Case Is = ListaDataset.DAS_bollettino_tares
                    Return "TBL_fat_x_rat"
                Case Is = ListaDataset.DAS_send_email
                    Return "TBL_main"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataset_MainTableKey

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "MAI_codice"  '                    Return _PrintFatturaDataset
                Case Is = ListaDataset.DAS_bollettino
                    Return "FXR_codice"  '                    Return _PrintBollettinoDataset
                Case Is = ListaDataset.DAS_bollettino_tares
                    Return "FXR_codice"  '                    Return _PrintBollettinoDataset
                Case Is = ListaDataset.DAS_send_email
                    Return "MAI_codice"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    ReadOnly Property LinkField_QuickDS_FullDS() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LinkField_QuickDS_FullDS

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "MAI_linkQD"
                Case Is = ListaDataset.DAS_bollettino
                    Return "FXR_linkQD"
                Case Is = ListaDataset.DAS_bollettino_tares
                    Return "FXR_linkQD"
                Case Is = ListaDataset.DAS_send_email
                    Return "MAI_linkQD"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    ReadOnly Property NuoviToponimi() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.NuoviToponimi

        Get
            Return _NuoviToponimi
        End Get

    End Property

    Public Sub PostPostAnteprima(ByVal aLista As ArrayList) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.PostAnteprima
        'Dim dr_elp As DS_PF_UE_ele.TBL_elenco_podRow
        'Dim nPagine As Integer
        'Dim nElencoPod As Integer
        'Dim nStatusRaccolta As Integer
        'Dim i As Integer

        'If Not _PrintFatturaDataset Is Nothing Then
        '    nElencoPod = -1
        '    For i = 0 To aLista.Count - 1
        '        nStatusRaccolta = CType(_PrintFatturaDataset.TBL_main.Rows(i), DS_PF_UE_ele.TBL_mainRow).DEF_raccolta
        '        Select Case nStatusRaccolta
        '            Case Is = 1
        '                nPagine = aLista(i)
        '            Case Is = 2, 3
        '                nElencoPod += 1
        '                dr_elp = _PrintFatturaDataset.TBL_elenco_pod.Rows(nElencoPod)
        '                dr_elp.ELP_page_num = nPagine + 1
        '                dr_elp.AcceptChanges()
        '                nPagine += aLista(i)
        '        End Select
        '    Next
        'End If

    End Sub

    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String, ByVal cParamSelez As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetPrintable
        Dim dr_mai_qd As DS_QD_ASM_tari.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_ASM_tari.TBL_mainRow)
        Select Case _SelectFromPlugInType
            Case Is = cns_tsm_sel_con_bollettino.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_F24S
            Case Is = cns_tsm_sel_con_allegato.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_all
        End Select

    End Sub


    Public Sub UpdateQuickDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.UpdateQuickDatasetFile
        'Dim dr_prd As DS_PF_UE_ele.TBL_mainRow
        'Dim xnTmp As Xml.XmlNode
        'Dim nLinkCode As Integer

        'If _CodiceModulo = ListaDataset.DAS_fattura Then
        '    _xmlDati_qd = New Xml.XmlDocument
        '    _xmlDati_qd.Load(QuickDataSet_IndexFile)
        '    For Each dr_prd In _PrintFatturaDataset.TBL_main
        '        nLinkCode = dr_prd(LinkField_QuickDS_FullDS)
        '        xnTmp = _xmlDati_qd.SelectSingleNode(String.Concat(Costanti.itm_qd_Fattura, "/MAI_codice[.=", _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).MAI_codice, "]")).ParentNode
        '        xnTmp.SelectSingleNode("DEF_errcode").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode
        '        xnTmp.SelectSingleNode("DEF_errdesc").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errdesc
        '    Next
        '    _xmlDati_qd.Save(QuickDataSet_IndexFile)
        'End If

    End Sub

    WriteOnly Property DBGiriConnectionString() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DBGiriConnectionString

        Set(ByVal value As String)
            _DbGiriConnString = value
        End Set

    End Property

    WriteOnly Property ListaGiri() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ListaGiri

        Set(ByVal value As ArrayList)
            _ListaGiri = value
        End Set

    End Property

    WriteOnly Property SessioneNuova() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneNuova

        Set(ByVal value As Boolean)
            _SessioneNuova = value
        End Set

    End Property

    WriteOnly Property TipoCaricamento() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.TipoCaricamento

        Set(ByVal value As Integer)
            _TipoCaricamento = value
        End Set

    End Property

    Public Shared Function ReadExcelIntoDataset(ByVal xlConnStrs As Dictionary(Of String, String), ByVal cFileName As String, Optional ByVal cNomeFoglio As String = "", Optional ByVal dsTiped As DataSet = Nothing, Optional ByVal cTable As String = "") As Data.DataSet
        Dim cConnStrPref As String
        Dim dati As DataSet

        Try
            If System.IO.Path.GetExtension(cFileName).ToLower = ".xls" Then
                cConnStrPref = xlConnStrs("xls")
            ElseIf System.IO.Path.GetExtension(cFileName).ToLower = ".xlsx" Then
                cConnStrPref = xlConnStrs("xlsx")
            Else
                Throw New Exception("Formato excel non riconosciuto")
            End If
            dati = ReadExcelIntoDataset(cConnStrPref, cFileName, "Foglio1", dsTiped, cTable)
        Catch ex As Exception
            dati = Nothing
            Throw ex
        End Try
        Return dati

    End Function

    Public Shared Function ReadExcelIntoDataset(ByVal cConnStrPref As String, ByVal cFileName As String, Optional ByVal cNomeFoglio As String = "", Optional ByVal dsTiped As DataSet = Nothing, Optional ByVal cTable As String = "") As Data.DataSet
        Dim cmdExcel As Data.OleDb.OleDbDataAdapter
        Dim cnExcel As Data.OleDb.OleDbConnection
        Dim dsExcel As Data.DataSet
        Dim schemaTbl As DataTable
        Dim cCommandText As String

        dsExcel = Nothing
        cnExcel = Nothing
        Try
            If (System.IO.File.Exists(cFileName)) Then
                cnExcel = New System.Data.OleDb.OleDbConnection(String.Concat(cConnStrPref, "Data Source=" + cFileName + ";Extended Properties=""Excel 8.0;"""))
                cnExcel.Open()

                ' ************************************************************************** '
                ' Recupero il nome del 1� foglio contenuto nella cartella Excel.             '
                ' ************************************************************************** '
                If cNomeFoglio = "" Then
                    schemaTbl = cnExcel.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, Nothing)
                    If (schemaTbl IsNot Nothing) Then
                        cNomeFoglio = ""
                        For Each dr As DataRow In schemaTbl.Rows
                            If dr("TABLE_NAME").ToString.EndsWith("$") Or dr("TABLE_NAME").ToString.EndsWith("$'") Then
                                cNomeFoglio = dr("TABLE_NAME")
                            End If
                        Next
                    Else
                        Throw New InvalidOperationException("Impossibile recuperare il primo foglio.")
                    End If
                End If
                If Not (cNomeFoglio.EndsWith("$") Or cNomeFoglio.EndsWith("$'")) Then
                    cNomeFoglio = String.Concat(cNomeFoglio, "$")
                End If

                cCommandText = String.Concat("SELECT * FROM [", cNomeFoglio, "]")
                cmdExcel = New Data.OleDb.OleDbDataAdapter(cCommandText, cnExcel)
                Try
                    If dsTiped Is Nothing Then
                        dsExcel = New Data.DataSet
                    Else
                        dsExcel = dsTiped
                    End If
                    If cTable > "" Then
                        cmdExcel.Fill(dsExcel, cTable)
                    Else
                        cmdExcel.Fill(dsExcel)
                    End If
                Catch ex As Exception
                    dsExcel = Nothing
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If cnExcel IsNot Nothing Then
                If cnExcel.State = ConnectionState.Open Then
                    cnExcel.Close()
                End If
            End If
        End Try
        Return dsExcel

    End Function

    Private Function RecuperaValoreDS_UE(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodiD As Integer, ByVal cTable As String, ByVal cField As String, Optional ByVal cDefault As String = "")
        Dim lFieldExist As Boolean
        Dim nRtnValue As Integer
        Dim cRtnValue As String
        Dim dr_tmp As DataRow

        cRtnValue = cDefault
        lFieldExist = ds_src.Tables.Contains(cTable)
        If lFieldExist Then
            lFieldExist = ds_src.Tables(cTable).Columns.Contains(cField)
        End If
        If lFieldExist Then
            Select Case cTable.ToLower
                Case Is = "DatiIdentificativiFattura".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiIdentificativiFattura").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiIdentificativiFattura")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "PeriodiFatturazione".ToLower
                    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                        If dr_tmp.GetChildRows("SezioniDocumenti_PeriodiFatturazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_PeriodiFatturazione")(0)
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        End If
                    End If
                Case Is = "IntestatarioFattura".ToLower
                    If dr_fat.GetChildRows("Fattura_IntestatarioFattura").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_IntestatarioFattura")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "IndirizzoSedeLegale".ToLower
                    If dr_fat.GetChildRows("Fattura_IndirizzoSedeLegale").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_IndirizzoSedeLegale")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "DatiSpedizione".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiSpedizione").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiSpedizione")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "IndirizzoSpedizione".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiSpedizione").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiSpedizione")(0)
                        If cField = "IndirizzoEmailInvioFattura" Then
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        Else
                            If dr_tmp.GetChildRows("DatiSpedizione_IndirizzoSpedizione").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("DatiSpedizione_IndirizzoSpedizione")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "PuntoDiFornitura".ToLower
                    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                        If dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio")(0)
                            If dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "Dilazione".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        End If
                    End If
                Case Is = "OT".ToLower
                    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                        If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                            If dr_tmp.GetChildRows("Contratto_OT").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("Contratto_OT")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "DatiRichiestaDomiciliazione".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                            If dr_tmp.GetChildRows("Dilazione_DatiRichiestaDomiciliazione").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("Dilazione_DatiRichiestaDomiciliazione")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "Rate".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                            If dr_tmp.GetChildRows("Dilazione_Rate").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("Dilazione_Rate")(nPodiD)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "BancaCliente".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                            If dr_tmp.GetChildRows("Dilazione_DatiDomiciliazione").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("Dilazione_DatiDomiciliazione")(0)
                                If dr_tmp.GetChildRows("DatiDomiciliazione_BancaCliente").Length > 0 Then
                                    dr_tmp = dr_tmp.GetChildRows("DatiDomiciliazione_BancaCliente")(0)
                                    If IsDBNull(dr_tmp(cField)) Then
                                        cRtnValue = cDefault
                                    Else
                                        cRtnValue = dr_tmp(cField)
                                    End If
                                End If
                            End If
                        End If
                    End If
                Case Is = "RiepilogoIVA".ToLower
                    If dr_fat.GetChildRows("Fattura_RiepilogoIVA").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_RiepilogoIVA")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "Contratto".ToLower
                    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                        If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        End If
                    End If
                Case Is = "DatiCatastali".ToLower
                    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                        If dr_tmp.GetChildRows("SezioniDocumenti_UbicazioneFornitura").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_UbicazioneFornitura")(0)
                            If dr_tmp.GetChildRows("UbicazioneFornitura_PartiEdificio").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("UbicazioneFornitura_PartiEdificio")(0)
                                If dr_tmp.GetChildRows("PartiEdificio_DatiCatastali").Length > 0 Then
                                    dr_tmp = dr_tmp.GetChildRows("PartiEdificio_DatiCatastali")(0)
                                    Try
                                        If IsDBNull(dr_tmp(cField)) Then
                                            cRtnValue = cDefault
                                        Else
                                            cRtnValue = dr_tmp(cField)
                                        End If
                                    Catch ex As Exception
                                        cRtnValue = cDefault
                                    End Try
                                End If
                            End If
                        End If
                    End If
            End Select
        ElseIf cTable.ToLower = "TipoPagamento".ToLower Then
            If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                    If cField = "BollettinoPostale" Then
                        If dr_tmp("MezzoPagamento").ToString.ToLower.Replace(" ", "") = "rimessadiretta" Then
                            cRtnValue = "si"
                        Else
                            cRtnValue = "no"
                        End If
                    End If
                End If
            End If
        ElseIf cTable.ToLower = "NumeroPOD".ToLower Then
            If cField.ToLower = "NumeroPOD".ToLower Then
                'cRtnValue = dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length
                ' LP 20130324 Attenzione debbo verificare che nell'elenco dei pod non ci siano ODL che sono rilevati dalla seguente descrizione:
                ' <TipoSezione>Sezioni Valorizzazione Contratto</TipoSezione>
                ' <TipoSezione>Sezioni Valorizzazione Lavoro</TipoSezione>
                nRtnValue = 0
                For Each dr_tmp In dr_fat.GetChildRows("Fattura_SezioniDocumenti")
                    If dr_tmp("TipoSezione").ToString.ToLower = "sezioni valorizzazione contratto" Then
                        nRtnValue += 1
                        ' ElseIf dr_tmp("TipoSezione").ToString.ToLower = cnt_tiposezione_lavoro Then

                    End If
                Next
                cRtnValue = nRtnValue
            End If
        ElseIf cTable.ToLower = "NumeroSEZ".ToLower Then
            If cField.ToLower = "NumeroSEZ".ToLower Then
                cRtnValue = dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length
                ' LP 20130324 Attenzione debbo verificare che nell'elenco dei pod non ci siano ODL che sono rilevati dalla seguente descrizione:
                ' <TipoSezione>Sezioni Valorizzazione Contratto</TipoSezione>
                ' <TipoSezione>Sezioni Valorizzazione Lavoro</TipoSezione>
                'nRtnValue = 0
                'For Each dr_tmp In dr_fat.GetChildRows("Fattura_SezioniDocumenti")
                '    If dr_tmp("TipoSezione").ToString.ToLower = "sezioni valorizzazione contratto" Then
                '        nRtnValue += 1
                '    ElseIf dr_tmp("TipoSezione").ToString.ToLower = "sezioni valorizzazione lavoro" Then

                '    End If
                'Next
                'cRtnValue = nRtnValue
            End If
        ElseIf cTable.ToLower = "NumeroRate".ToLower Then
            cRtnValue = 0
            If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                    cRtnValue = dr_tmp.GetChildRows("Dilazione_Rate").Length
                End If
            End If
        ElseIf cTable.ToLower = "IndirizzoPuntoDiFornitura".ToLower Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio")(0)
                    If dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura").Length > 0 Then
                        dr_tmp = dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura")(0)
                        If dr_tmp.GetChildRows("PuntoDiFornitura_Indirizzo").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("PuntoDiFornitura_Indirizzo")(0)
                            Try
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            Catch ex As Exception
                                cRtnValue = cDefault
                            End Try
                        End If
                    End If
                End If
            End If

        ElseIf cTable.ToLower = "UbicazioneFornitura".ToLower Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_UbicazioneFornitura").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_UbicazioneFornitura")(0)
                    If dr_tmp.GetChildRows("UbicazioneFornitura_Indirizzo").Length > 0 Then
                        dr_tmp = dr_tmp.GetChildRows("UbicazioneFornitura_Indirizzo")(0)
                        Try
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        Catch ex As Exception
                            cRtnValue = cDefault
                        End Try
                    End If
                End If
            End If
        ElseIf cField.ToLower = "IndirizzoEmailInvioFattura".ToLower Then
            If dr_fat.GetChildRows("Fattura_DatiSpedizione").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_DatiSpedizione")(0)
                If cField = "IndirizzoEmailInvioFattura" Then
                    Try
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    Catch ex As Exception
                        cRtnValue = ""
                    End Try
                End If
            End If
        ElseIf cTable.ToLower = "TipoStampa".ToLower Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                    For Each dr As DataRow In dr_tmp.GetChildRows("Contratto_ValoreCampoContratto")
                        If dr("Campo").tolower = cField.ToLower Then
                            cRtnValue = dr("ValoreCampo")
                        End If
                    Next
                End If
            End If
        ElseIf cTable.ToLower = "CodiceConsorzioAcquisto".ToLower Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                    If dr_tmp.GetChildRows("Contratto_AltriSoggettiCorrelati").Length > 0 Then
                        For Each dr As DataRow In dr_tmp.GetChildRows("Contratto_AltriSoggettiCorrelati")
                            If dr("Ruolo").tolower = cField.ToLower Then
                                cRtnValue = dr.GetChildRows("AltriSoggettiCorrelati_Soggetto")(0)("IDSoggetto")
                            End If
                        Next
                    End If
                End If
            End If
        End If
        Return cRtnValue

    End Function

    ' ************************************************************************** '
    ' Questa funzione serve a scrivere un file XML con la codifica WINDOWS-1252. '
    ' ************************************************************************** '
    Private Sub SalvaFileDatiXML(ByVal cFileName As String, ByVal xdFile As Xml.XmlDocument)
        Dim myEncoding As System.Text.Encoding
        Dim sw_xml As System.IO.StreamWriter

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)
            sw_xml = New System.IO.StreamWriter(cFileName, False, myEncoding)
            xdFile.Save(sw_xml)
            sw_xml.Close()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ' ************************************************************************** '
    ' Questa funzione serve a leggere un file XML con la codifica WINDOWS-1252 e '
    ' a caricarlo in un XMLDocument.                                             '
    ' ************************************************************************** '
    Private Function caricaFileDatiXML(ByVal cFileName As String) As Xml.XmlDocument
        Dim myEncoding As System.Text.Encoding
        Dim sr_xml As System.IO.StreamReader
        Dim fil_xml As Xml.XmlDocument

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)    ' Windows-1252
            sr_xml = New System.IO.StreamReader(cFileName, myEncoding)
            fil_xml = New Xml.XmlDocument
            fil_xml.Load(sr_xml)
            sr_xml.Close()
        Catch ex As Exception
            fil_xml = Nothing
            Throw ex
        End Try
        Return fil_xml

    End Function

    Private Sub AddRecordToDS(ByVal dr_mai_qd As DS_QD_ASM_tari.TBL_mainRow)
        Dim ds_src As DataSet
        Dim dr_fat As DataRow
        Dim aDR_fat As DataRow()

        Try
            ds_src = caricaFileDatiDS(PrintDataSet_DataFile(dr_mai_qd.MAI_filedati))
            'ds_src = New DataSet
            'ds_src.ReadXml(PrintDataSet_DataFile(dr_mai_qd.MAI_filedati))
            aDR_fat = ds_src.Tables("Fattura").Select(String.Concat("IDFattura = '", dr_mai_qd.MAI_IDFattura, "'"))
            If aDR_fat.Length > 0 Then
                dr_fat = aDR_fat(0)
                Select Case _CodiceModulo
                    Case Is = ListaDataset.DAS_fattura
                        GetRecordFatturaToPrint(dr_mai_qd.MAI_codice, ds_src, dr_fat, GetFeedString(dr_mai_qd))
                    Case Is = ListaDataset.DAS_bollettino
                        ' GetRecordBollettinoToPrint(dr_mai_qd.MAI_codice, ds_src, dr_fat, GetFeedString(dr_mai_qd))
                        GetRecordF24ToPrint(dr_mai_qd.MAI_codice, ds_src, dr_fat, GetFeedString(dr_mai_qd))
                    Case Is = ListaDataset.DAS_send_email
                        GetRecordEmailToSend(dr_mai_qd.MAI_codice, ds_src, dr_fat)
                End Select
            End If
            ds_src = Nothing
        Catch ex As Exception
            Throw SetExceptionItem("AddRecordToPrint", ex)
        End Try

    End Sub

    Private Sub GetRecordEmailToSend(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_fat As DataRow)
        Dim dr_doc As DS_SE_ASM_tari.TBL_documentiRow
        Dim dr_mai As DS_SE_ASM_tari.TBL_mainRow
        Dim xdViaMail As Xml.XmlDocument
        Dim cDFA_flag_tipo_pag As String
        Dim cDFA_desc_tipo_pag As String
        Dim xnDoc As Xml.XmlNode

        'cDFA_desc_tipo_pag = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Dilazione", "MezzoPagamento")
        'If cDFA_desc_tipo_pag.ToLower.Replace(" ", "") = "rimessadiretta" Then
        '    cDFA_flag_tipo_pag = "R"
        'ElseIf cDFA_desc_tipo_pag.ToLower.Replace(" ", "") = "bonificobancario" Then
        '    cDFA_flag_tipo_pag = "B"
        'ElseIf cDFA_desc_tipo_pag.ToLower.Replace(" ", "") = "domiciliazionebancaria" Then
        '    cDFA_flag_tipo_pag = "D"
        'Else
        '    cDFA_flag_tipo_pag = ""
        'End If


        ' ************************************************************************** '
        ' Creazione del record guida per l'invio della email.                        '
        ' ************************************************************************** '
        dr_mai = _SendEmailDataset.TBL_main.NewRow
        _CodiceProgressivo += 1
        dr_mai.MAI_codice = _CodiceProgressivo
        dr_mai.MAI_linkQD = nCodice
        dr_mai.MAI_nome = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "RagioneSociale")
        dr_mai.MAI_tot_fattura = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "RiepilogoIVA", "TotaleFattura")
        dr_mai.MAI_data_scadenza = "" ' SetDataScadenza(cDFA_flag_tipo_pag, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Rate", "DataScadenza"))
        dr_mai.MAI_registro_iva = "" ' RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "Protocollo")
        dr_mai.MAI_fattura_anno = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "AnnoFattura")
        dr_mai.MAI_fattura_numero = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "NumeroFattura")
        dr_mai.MAI_per_rifer = "" ' DeterminaPeriodoFatturazione(ds_src, dr_fat, 0)
        dr_mai.MAI_multipod = False 'RecuperaValoreDS_UE(ds_src, dr_fat, 0, "NumeroPOD", "NumeroPOD") > 1

        dr_mai.MAI_anno_contratto = "" 'RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Contratto", "AnnoContratto")
        dr_mai.MAI_num_contratto = "" ' RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Contratto", "NumeroContratto")
        dr_mai.MAI_ub_sito_1 = "" 'RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "TipoElementoTopologico")
        dr_mai.MAI_ub_sito_2 = "" 'RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "ElementoTopologico")
        dr_mai.MAI_ub_sito_3 = "" 'RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "NumeroCivico")
        dr_mai.MAI_ub_sito_4 = "" 'RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "SuffissoCivico")
        dr_mai.MAI_ub_sito_5 = "" ' RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "Localita")
        dr_mai.MAI_ub_sito_6 = "" ' RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "Cap")
        dr_mai.MAI_ub_sito_7 = "" ' RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "Comune")
        dr_mai.MAI_ub_sito_8 = "" 'RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "ProvinciaSigla")
        dr_mai.MAI_cod_gruppo = "" ' RecuperaValoreDS_UE(ds_src, dr_fat, 0, "CodiceConsorzioAcquisto", "CONSORZIO/GRUPPO DI ACQUISTO")

        dr_mai.DEF_lista_email = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "IndirizzoEmailInvioFattura").ToString.Replace("$", ";")
        dr_mai.MAI_send_xml = False
        _SendEmailDataset.TBL_main.Rows.Add(dr_mai)

        'xdViaMail = New Xml.XmlDocument
        'xdViaMail.Load(String.Concat(_SessionePath, "dati\SendByMail.xml"))
        'xnDoc = xdViaMail.SelectSingleNode(String.Concat("SendByMail/SendByMailItem/MAI_codice[.='", nCodice, "']")).ParentNode

        'For Each xnTmp As Xml.XmlNode In xnDoc.SelectSingleNode("Documenti").ChildNodes
        '    dr_doc = _SendEmailDataset.TBL_documenti.NewRow
        '    dr_doc.DOC_cod_mai = dr_mai.MAI_codice
        '    dr_doc.DOC_filename = xnTmp.SelectSingleNode("DOC_filename").InnerText
        '    _SendEmailDataset.TBL_documenti.Rows.Add(dr_doc)
        'Next

    End Sub

    Private Sub GetRecordFatturaToPrint(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal cFeedString As String)
        Dim nContrattiFatturati As Integer
        Dim nSezDocumenti As Integer

        ' ************************************************************************** '
        ' Creazione del record guida della stampa della fattura.                     '
        ' ************************************************************************** '
        _dr_mai_pf = _PrintFatturaDataset.TBL_main.NewRow
        _CodiceProgressivo += 1
        _dr_mai_pf.MAI_codice = _CodiceProgressivo
        _MasterRecord = _CodiceProgressivo
        _dr_mai_pf.MAI_linkQD = nCodice
        _dr_mai_pf.DEF_alimimb = cFeedString

        ' ************************************************************************** '
        ' Recuperiamo il numero dei pod per i quli si stamper� la fattura. Il campo  '
        ' di default DEF_raccolta indica se il record attuale fa parte di una raccol '
        ' ta. I valori che pu� assumere sono:                                        '
        ' ---- In caso di raccolta da pi� di un record:                              '
        '      - 1 primo record di una raccolta;                                     '
        '      - 2 record successivi della raccolta                                  '
        '      - 3 ultimo record della raccolta.                                     '
        ' ---- In caso di raccolta da pi� di un record:                              '
        '      - 9 raccolta costituita da un solo record                             '
        ' ************************************************************************** '
        nContrattiFatturati = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "NumeroPOD", "NumeroPOD")     'xiWork.SelectNodes(Costanti.itm_PDR_fatturati).Count
        Select Case nContrattiFatturati
            Case Is = 0
                _dr_mai_pf.DEF_raccolta = 9
                AddErroreLieve("Pod fatturati assenti")
            Case Is = 1
                _dr_mai_pf.DEF_raccolta = 9
            Case Else
                _dr_mai_pf.DEF_raccolta = 1
        End Select

        _dr_mai_pf.DEF_raccolta = 9

        nSezDocumenti = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "NumeroSEZ", "NumeroSEZ")
        ' ************************************************************************** '
        ' Dati dell'intestatario della fattura.                                      ' 
        ' ************************************************************************** '
        _dr_mai_pf.MAI_cod_cli = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "IDSoggetto")
        _dr_mai_pf.MAI_nome = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "RagioneSociale")
        _dr_mai_pf.MAI_cod_fis = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "CodiceFiscale")
        _dr_mai_pf.MAI_par_iva = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "PartitaIva")
        _dr_mai_pf.MAI_cod_gruppo = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "CodiceConsorzioAcquisto", "CONSORZIO/GRUPPO DI ACQUISTO")

        ' ************************************************************************** '
        ' Il codice fiscale e la partita iva sono dati alternativi. Non possono esse '
        ' re presenti allo stesso tempo entrambi.                                    '
        ' ************************************************************************** '
        If _dr_mai_pf.MAI_par_iva > "" Then
            _dr_mai_pf.MAI_cod_fis = ""
            AddErroreLieve("Codice fiscale valorizzato in maniera errata o uguale alla partita IVA")
        End If
        CheckFieldValue(String.Concat(_dr_mai_pf.MAI_cod_fis, _dr_mai_pf.MAI_par_iva), "Partita IVA o Codice Fiscale")

        ' ************************************************************************** '
        ' Indirizzo dell'intestatario.                                               '
        ' ************************************************************************** '
        _dr_mai_pf.MAI_int_sito_1 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSedeLegale", "TipoElementoTopologico")
        _dr_mai_pf.MAI_int_sito_2 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSedeLegale", "ElementoTopologico", "")
        _dr_mai_pf.MAI_int_sito_3 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSedeLegale", "NumeroCivico", "")
        _dr_mai_pf.MAI_int_sito_4 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSedeLegale", "SuffissoCivico", "")
        _dr_mai_pf.MAI_int_sito_5 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSedeLegale", "Localita", "")
        _dr_mai_pf.MAI_int_sito_6 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSedeLegale", "Comune")
        _dr_mai_pf.MAI_int_sito_7 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSedeLegale", "Cap").ToString().PadLeft(5, "0")
        _dr_mai_pf.MAI_int_sito_8 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSedeLegale", "ProvinciaSigla")
        _dr_mai_pf.MAI_int_nazione = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSedeLegale", "NazioneSigla")

        ' ************************************************************************** '
        ' Indirizzo del recapito della fattura.                                      '
        ' ************************************************************************** '
        _dr_mai_pf.MAI_rec_nome = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiSpedizione", "Recapito", _dr_mai_pf.MAI_nome)
        _dr_mai_pf.MAI_rec_sito_1 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "TipoElementoTopologico")
        _dr_mai_pf.MAI_rec_sito_2 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ElementoTopologico")
        _dr_mai_pf.MAI_rec_sito_3 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NumeroCivico")
        _dr_mai_pf.MAI_rec_sito_4 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "SuffissoCivico")
        _dr_mai_pf.MAI_rec_sito_5 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Localita")
        _dr_mai_pf.MAI_rec_sito_6 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Comune")
        _dr_mai_pf.MAI_rec_sito_7 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Cap").ToString().PadLeft(5, "0")
        _dr_mai_pf.MAI_rec_sito_8 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ProvinciaSigla")
        _dr_mai_pf.MAI_rec_nazione = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NazioneSigla")
        _PrintFatturaDataset.TBL_main.Rows.Add(_dr_mai_pf)

        For i As Integer = 0 To nSezDocumenti - 1
            Carica_DatiFattura(ds_src, dr_fat, i)
            Carica_DatiFornitura(ds_src, dr_fat, i, nContrattiFatturati)
            Carica_RigheFatturazione(ds_src, dr_fat, i, 1)
        Next

    End Sub

    ' ************************************************************************** '
    ' Caricamento dei dati riguardanti la fattura.                               '
    ' ************************************************************************** '
    Private Sub Carica_DatiFattura(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer)
        Dim lAlreadyInTable As Boolean
        Dim dr_tmp As DataRow
        Dim nRate As Integer

        Try
            lAlreadyInTable = _PrintFatturaDataset.TBL_dati_fatt.Select(String.Concat("DFA_cod_mai = '", _dr_mai_pf.MAI_codice, "'")).Length > 0
            If lAlreadyInTable Then
                _dr_dfa_pf = _PrintFatturaDataset.TBL_dati_fatt.Select(String.Concat("DFA_cod_mai = ", _dr_mai_pf.MAI_codice))(0)
            Else
                _dr_dfa_pf = _PrintFatturaDataset.TBL_dati_fatt.NewRow
                _dr_dfa_pf.DFA_cod_mai = _dr_mai_pf.MAI_codice
            End If
            '_dr_dfa_pf = _PrintFatturaDataset.TBL_dati_fatt.NewRow
            With _dr_dfa_pf
                .DFA_cod_mai = _dr_mai_pf.MAI_codice
                .DFA_desc_tipo_pag = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Dilazione", "MezzoPagamento")
                If .DFA_desc_tipo_pag.ToLower.Replace(" ", "") = "rimessadiretta" Then
                    .DFA_flag_tipo_pag = "R"
                ElseIf .DFA_desc_tipo_pag.ToLower.Replace(" ", "") = "bonificobancario" Then
                    .DFA_flag_tipo_pag = "B"
                ElseIf .DFA_desc_tipo_pag.ToLower.Replace(" ", "") = "domiciliazionebancaria" Then
                    .DFA_flag_tipo_pag = "D"
                Else
                    .DFA_flag_tipo_pag = ""
                End If
                .DFA_imp_rate = False

                ' ************************************************************************** '
                ' Recupera i dati per il pagamento tramite Bonifico Bancario.                '
                ' ************************************************************************** '
                If .DFA_flag_tipo_pag = "B" Then
                    .DFA_nome_banca = GetTagXML(dr_fat, New String() {"Fattura", "DatiPagamento", "Dilazione", "BancaAppoggio", "Banca"})
                    .DFA_iban = GetTagXML(dr_fat, New String() {"Fattura", "DatiPagamento", "Dilazione", "BancaAppoggio", "IBAN"})
                End If

                ' ************************************************************************** '
                ' Recupera i dati per il pagamento tramite Domiciliazione Bancaria.          '
                ' ************************************************************************** '
                If .DFA_flag_tipo_pag = "D" Then
                    .DFA_nome_banca = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "BancaCliente", "Banca")
                    .DFA_iban = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "BancaCliente", "IBAN")
                End If
                '---------->.DFA_tot_imponibile = GetValueFromXML(xiWork, Costanti.itm_tot_imponibile)
                '---------->.DFA_totale_iva = GetValueFromXML(xiWork, Costanti.itm_tot_iva, "0", "")
                .DFA_totale_fattura = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "RiepilogoIVA", "TotaleFattura")
                .DFA_registro_iva = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "Protocollo")
                .DFA_anno = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "AnnoFattura")
                .DFA_numero = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "NumeroFattura")
                .DFA_anno_rett = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "AnnoFatturaRettificata", 0)
                .DFA_numero_rett = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "NumeroFatturaRettificata")
                .DFA_scadenza = SetDataScadenza(.DFA_flag_tipo_pag, RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Rate", "DataScadenza"))
                .DFA_scadenze = ""
                .DFA_totale_rata_1 = 0
                .DFA_totale_rata_2 = 0
                .DFA_totale_rata_3 = 0
                nRate = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "NumeroRATE", "")
                For i As Integer = 0 To nRate - 1
                    If i = 0 Then
                        .DFA_totale_rata_1 = RecuperaValoreDS_UE(ds_src, dr_fat, i, "Rate", "ImportoRata")
                        .DFA_scaden_rata_1 = RecuperaValoreDS_UE(ds_src, dr_fat, i, "Rate", "DataScadenza")
                    ElseIf i = 1 Then
                        .DFA_totale_rata_2 = RecuperaValoreDS_UE(ds_src, dr_fat, i, "Rate", "ImportoRata")
                        .DFA_scaden_rata_2 = RecuperaValoreDS_UE(ds_src, dr_fat, i, "Rate", "DataScadenza")
                    ElseIf i = 2 Then
                        .DFA_totale_rata_3 = RecuperaValoreDS_UE(ds_src, dr_fat, i, "Rate", "ImportoRata")
                        .DFA_scaden_rata_3 = RecuperaValoreDS_UE(ds_src, dr_fat, i, "Rate", "DataScadenza")
                    End If
                    .DFA_scadenze = String.Concat(.DFA_scadenze, RecuperaValoreDS_UE(ds_src, dr_fat, i, "Rate", "DataScadenza"), " ")
                Next
                .DFA_scadenze = .DFA_scadenze.Trim
                .DFA_emissione = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "DataFattura")
                .DFA_one_rete_des_br = GetTrasformazioni("tbl_totali_fatturati", 1, 1)
                .DFA_one_rete_des = GetTrasformazioni("tbl_totali_fatturati", 1, 2)
                .DFA_one_vend_des_br = GetTrasformazioni("tbl_totali_fatturati", 2, 1)
                .DFA_one_vend_des = GetTrasformazioni("tbl_totali_fatturati", 2, 2)
                .DFA_imposte_des_br = GetTrasformazioni("tbl_totali_fatturati", 3, 1)
                .DFA_imposte_des = GetTrasformazioni("tbl_totali_fatturati", 3, 2)
                .DFA_one_dive_des_br = GetTrasformazioni("tbl_totali_fatturati", 4, 1)
                .DFA_one_dive_des = GetTrasformazioni("tbl_totali_fatturati", 4, 2)
            End With
            If Not lAlreadyInTable Then
                _PrintFatturaDataset.TBL_dati_fatt.Rows.Add(_dr_dfa_pf)
            End If
        Catch ex As Exception
            _ErroriGravi = String.Concat(_ErroriGravi, "Errore nel caricamento dei dati della Fattura (DFA)", ex, vbCr)
            'Throw New Exception("Errore nel caricamento dei dati della Fattura (DFA)", ex)
        End Try

    End Sub

    ' ********************************************************************* '
    ' Valorizza i campi che hanno a che fare con i dati della fornitura.    '
    ' ********************************************************************* '
    Private Sub Carica_DatiFornitura(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer, ByVal nNumPodFatturati As Integer)
        Dim cTmpString As String
        Dim cResidente As String
        Dim cParacadute As String
        Dim dTmp As DateTime
        Dim dr_dca As DS_PF_ASM_tari.TBL_dati_catastaliRow

        Try
            _dr_daf_pf = Nothing
            _NomeOfferta = ""
            ' ********************************************************************* '
            ' Creazione del record per la memorizzazione dei dati della fornitura   '
            ' relativi ad un POD.                                                   '
            ' ********************************************************************* '
            _dr_daf_pf = _PrintFatturaDataset.TBL_dati_forn.NewRow
            With _dr_daf_pf
                .DAF_cod_mai = _dr_mai_pf.MAI_codice

                ' ********************************************************************* '
                ' La descrizione del contratto viene caricata successivamente in dipen- ' 
                ' denza di alcuni parametri.                                            '
                ' ********************************************************************* '
                .DAF_anno_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "AnnoContratto")
                .DAF_num_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "NumeroContratto")
                .DAF_desc_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "TipoContratto")
                .DAF_domestico = .DAF_desc_contratto = "DOMESTICO"
                .DAF_contratto_attivo = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "StatoContratto").ToString.ToUpper = "ATTIVO"
                .DAF_cdc = ""

                ' ********************************************************************* '
                ' Dati recuperati dalla sezione del POD.                                '
                ' ********************************************************************* '
                If _dr_mai_pf.DEF_raccolta = 1 Then
                    '  dr_daf.DAF_pod = String.Concat("Totale POD serviti ", nNumPodFatturati)
                Else
                    ' ********************************************************************* '
                    ' Dati recuperati dalla sezione della fattura.                          '
                    ' ********************************************************************* '
                    .DAF_agev_compostiera = False
                    .DAF_agev_fuorizona = False
                    .DAF_agev_pannolini_ecologici = False
                    .DAF_agev_titolare_residenza_com_tr = False
                    .DAF_casa_a_disposiz = False
                    .DAF_fabbricato_rurale = False
                    .DAF_sogg_estero = False
                    .DAF_esenzione_redd_min = False
                    .DAF_num_comp_port_hand_t8 = False
                    .DAF_agev_isee = False
                    .DAF_num_comp_port_hand = 0

                    ' ********************************************************************* '
                    ' Ubicazione del sito dove viene fornita l'Energia Elettrica.           '
                    ' ********************************************************************* '
                    .DAF_descr_sito_f = "" '--------->GetValueFromXML(xiWorkSezFat, Costanti.itm_pf_descr)

                    .DAF_ub_sito_1 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "UbicazioneFornitura", "TipoElementoTopologico")
                    .DAF_ub_sito_2 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "UbicazioneFornitura", "ElementoTopologico")
                    .DAF_ub_sito_3 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "UbicazioneFornitura", "NumeroCivico")
                    .DAF_ub_sito_4 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "UbicazioneFornitura", "SuffissoCivico")
                    .DAF_ub_sito_5 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "UbicazioneFornitura", "Localita")
                    .DAF_ub_sito_6 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "UbicazioneFornitura", "Cap").ToString().PadLeft(5, "0")
                    .DAF_ub_sito_7 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "UbicazioneFornitura", "Comune")
                    .DAF_ub_sito_8 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "UbicazioneFornitura", "ProvinciaSigla")

                    ' valorizza il comne per la riga del catasto con default l'ubicazione della fornitura.
                    ' dai valori catastali recuperer� eventualmente idati pi� precisi
                    '.DAF_desComune = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "UbicazioneFornitura", "Comune")
                    '.DAF_codiceComune = "-"

                    ' ********************************************************************* '
                    ' Inizio fornitura dell'Energia Elettrica.                              '
                    ' ********************************************************************* '
                    .DAF_codiceComune = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiCatastali", "ComuneCatastale", "-")
                    .DAF_desComune = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiCatastali", "DesComuneCatastale", .DAF_ub_sito_7)
                    .DAF_cat_sezione = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiCatastali", "SezioneCatasto")
                    .DAF_cat_foglio = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiCatastali", "FoglioCatasto")
                    .DAF_cat_particella = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiCatastali", "ParticellaCatasto")
                    .DAF_cat_subalterno = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiCatastali", "SubalternoCatasto")
                    .DAF_cat_categoria = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiCatastali", "CategoriaCatasto")

                    For Each dr As DataRow In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId).GetChildRows("SezioniDocumenti_UbicazioneFornitura")(0).GetChildRows("UbicazioneFornitura_PartiEdificio")
                        dr_dca = _PrintFatturaDataset.TBL_dati_catastali.NewRow
                        dr_dca.DCA_daf_codice = _dr_daf_pf.DAF_codice
                        dr_dca.DCA_cat_sezione = dr.GetChildRows("PartiEdificio_DatiCatastali")(0).Item("SezioneCatasto")
                        dr_dca.DCA_cat_foglio = dr.GetChildRows("PartiEdificio_DatiCatastali")(0).Item("FoglioCatasto")
                        dr_dca.DCA_cat_particella = dr.GetChildRows("PartiEdificio_DatiCatastali")(0).Item("ParticellaCatasto")
                        dr_dca.DCA_cat_subalterno = dr.GetChildRows("PartiEdificio_DatiCatastali")(0).Item("SubalternoCatasto")
                        dr_dca.DCA_cat_categoria = dr.GetChildRows("PartiEdificio_DatiCatastali")(0).Item("CategoriaCatasto")
                        dr_dca.DCA_cat_parteedificio = dr.Item("ParteEdificio")
                        _PrintFatturaDataset.TBL_dati_catastali.Rows.Add(dr_dca)
                    Next

                    ' ********************************************************************* '
                    ' Inizio fornitura dell'Energia Elettrica.                              '
                    ' ********************************************************************* '
                    .DAF_inizio_forn = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "DataAttivazione")

                    ' Rispetto normativa 205/2014/R/eel (articolo 5, comma 1)
                    .DAF_205_2014_R_eel = False
                    If .DAF_domestico Then
                        If DateTime.Parse(.DAF_inizio_forn) < DateTime.Parse("30/09/2015") Then
                            dTmp = DateTime.Parse(.DAF_inizio_forn)
                            If dTmp < DateTime.Parse("01/06/2014") Then
                                dTmp = DateTime.Parse("01/06/2014")
                            End If
                            .DAF_205_2014_R_eel = dTmp.AddMonths(5) > DateTime.Parse(_dr_dfa_pf.DFA_emissione)
                        End If
                    End If

                    If Not .DAF_contratto_attivo Then
                        .DAF_cess_forn = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "PeriodiFatturazione", "DataFineConsumiFatturati")
                    End If

                    .DAF_agev_compostiera = False
                    .DAF_agev_fuorizona = False
                    .DAF_agev_sup_scoperte = False
                    .DAF_agev_ute_stagionale = False
                    For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId).GetChildRows("SezioniDocumenti_Contratto")
                        For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("Contratto_OT")
                            For Each dr_tmp3 As DataRow In dr_tmp2.GetChildRows("OT_ValoreParametroOT")
                                Try
                                    Select Case dr_tmp3("Campo").ToString.ToLower
                                        Case Is = "NUMERO COMPONENTI CALCOLATO ".ToLower      ' "NUMERO COMPONENTI".ToLower, 
                                            .DAF_numero_componenti = dr_tmp3("ValoreCampo")
                                        Case Is = "TIPOLOGIA UTILIZZO".ToLower
                                            If .DAF_domestico Then
                                                .DAF_utilizzo = dr_tmp3("ValoreCampo")
                                            End If
                                        Case Is = "TIPOLOGIA ATTIVITA".ToLower
                                            If Not .DAF_domestico Then
                                                .DAF_utilizzo = dr_tmp3("ValoreCampo")
                                            End If
                                        Case Is = "NUMERO COMPONENTI MAGGIORI CALCOLATO ".ToLower
                                            .DAF_num_com_mag_calc = dr_tmp3("ValoreCampo")
                                        Case Is = "NUMERO COMPONENTI MINORI CALCOLATO ".ToLower
                                            .DAF_num_com_min_calc = dr_tmp3("ValoreCampo")
                                        Case Is = "NUM COMP. MINORI PER SCONTO FAMIGLIE NUMEROSE".ToLower
                                            .DAF_num_com_min_num_calc = dr_tmp3("ValoreCampo")
                                        Case Is = "MQ (VALORE DA CAMPO CONTRATTO)".ToLower
                                            .DAF_mq_calcolo = dr_tmp3("ValoreCampo")
                                        Case Is = "COMPOSTIERA".ToLower
                                            .DAF_agev_compostiera = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "FUORIZONA".ToLower
                                            .DAF_agev_fuorizona = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "PANNOLINI ECOLOGICI".ToLower
                                            .DAF_agev_pannolini_ecologici = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "TITOLARE CON RESIDENZA NEL COMUNE DI TERNI".ToLower
                                            .DAF_agev_titolare_residenza_com_tr = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "CASA A DISPOSIZIONE".ToLower
                                            .DAF_casa_a_disposiz = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "FABBRICATO RURALE".ToLower
                                            .DAF_fabbricato_rurale = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "SOGGETTO RESIDENTE CON DIMORA ALL'ESTERO".ToLower
                                            .DAF_sogg_estero = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "ESENZIONE 100% REDDITO MINIMO".ToLower
                                            .DAF_esenzione_redd_min = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "(T8) NUM COMP PORTATORI DI HANDICAP PF".ToLower
                                            .DAF_num_comp_port_hand_t8 = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "RIDUZIONE (ISEE<=19.100)".ToLower
                                            .DAF_agev_isee = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "NUMERO COMPONENTI PORTATORI HANDICAP".ToLower
                                            .DAF_num_comp_port_hand = dr_tmp3("ValoreCampo")
                                        Case Is = "SUPERFICI SCOPERTE".ToLower
                                            .DAF_agev_sup_scoperte = dr_tmp3("ValoreCampo") = "SI"
                                        Case Is = "UTENZA STAGIONALE".ToLower
                                            .DAF_agev_ute_stagionale = dr_tmp3("ValoreCampo") = "SI"
                                    End Select
                                Catch ex As Exception

                                End Try
                            Next
                            'If dr_tmp2("ComponenteServizio") = "TIPOLOGIA UTILIZZO" Then
                            '    .DAF_utilizzo = dr_tmp2("OT1")
                            'End If
                        Next
                        cResidente = "*"
                        For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("Contratto_ValoreCampoContratto")
                            Try
                                Select Case dr_tmp2("Campo").ToString.ToLower
                                    Case Is = "SUPERFICIE METRI QUADRATI".ToLower
                                        .DAF_mq_calcolo = dr_tmp2("ValoreCampo")
                                    Case Is = "CENTRO DI COSTO".ToLower
                                        .DAF_cdc = dr_tmp2("ValoreCampo")
                                    Case Is = "RESIDENTE".ToLower
                                        If dr_tmp2("DataFine") = "31/12/9999" Then
                                            cResidente = dr_tmp2("ValoreCampo")
                                        End If
                                End Select
                            Catch ex As Exception

                            End Try
                        Next
                        If cResidente <> "*" Then
                            If cResidente = "NO" Then
                                .DAF_desc_contratto = String.Concat(.DAF_desc_contratto, " NON")
                            End If
                            .DAF_desc_contratto = String.Concat(.DAF_desc_contratto, " RESIDENTE")
                        End If
                    Next

                    For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId).GetChildRows("SezioniDocumenti_DatiServizio")(0).GetChildRows("DatiServizio_PuntoDiFornitura")
                        For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("PuntoDiFornitura_ValoreCampoPunto")
                            Try
                                cTmpString = dr_tmp2("Campo").ToString.ToUpper
                                If (cTmpString = "POTENZA IMPEGNATA") Then
                                    .DAF_pot_imp_val = dr_tmp2("ValoreCampo")
                                    'dr_daf.DAF_pot_imp_um = dr_tmp2("UM")
                                    .DAF_pot_imp_um = "kW"
                                ElseIf (cTmpString = "POTENZA DISPONIBILE") Then
                                    .DAF_pot_dis_val = dr_tmp2("ValoreCampo")
                                    'dr_daf.DAF_pot_dis_um = dr_tmp2("UM")
                                    .DAF_pot_dis_um = "kW"
                                ElseIf (cTmpString = "TENSIONE VOLT") Then
                                    .DAF_tensione_forn = dr_tmp2("ValoreCampo")
                                ElseIf (cTmpString = "TIPO FORNITURA EE") Then
                                    .DAF_tensione_tipo = dr_tmp2("ValoreCampo")
                                End If
                            Catch ex As Exception

                            End Try
                        Next
                    Next
                End If
            End With

            'LP 20120510 *************************************************************** '
            ' Modifiche della descrizione del tipo di contratto per la parte domestica.  '
            ' ************************************************************************** '
            If _dr_daf_pf.DAF_desc_contratto.ToUpper.Contains("DOMESTICO") Then
                If _dr_daf_pf.DAF_desc_contratto.ToUpper.Contains("NON RESIDENTE") Then
                    _dr_daf_pf.DAF_desc_contratto = "Utenza Domestica Non Residente"
                ElseIf _dr_daf_pf.DAF_desc_contratto.ToUpper.Contains("RESIDENTE") Then
                    _dr_daf_pf.DAF_desc_contratto = "Utenza Domestica Residente"
                End If
            End If
            With _dr_daf_pf
                .DAF_agevolazioni = .DAF_agev_compostiera Or .DAF_agev_fuorizona Or .DAF_agev_pannolini_ecologici Or .DAF_agev_titolare_residenza_com_tr Or _
                                    .DAF_casa_a_disposiz Or .DAF_fabbricato_rurale Or .DAF_sogg_estero Or .DAF_esenzione_redd_min Or .DAF_num_comp_port_hand_t8 Or _
                                    .DAF_agev_isee Or (.DAF_num_comp_port_hand > 0)
            End With
            _PrintFatturaDataset.TBL_dati_forn.Rows.Add(_dr_daf_pf)
        Catch ex As Exception
            _ErroriGravi = String.Concat(_ErroriGravi, "Carica_DatiFornitura", ex, vbCr)
        End Try

    End Sub

    ' ************************************************************************** '
    ' Recupero le righe di fatturazione da stampare. Le procedure di seguito ser '
    ' vono allo scopo.                                                           '
    ' ************************************************************************** '
    Private Sub Carica_RigheFatturazione(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer, ByVal nNumSez As Integer)
        Dim dr_far As DS_PF_ASM_tari.TBL_fattura_rowRow
        Dim nSezioneStampa As Integer
        Dim nOrdineStampa As Integer
        Dim nRow As Integer
        Dim cTmp As String
        Dim lPrezUniNull As Boolean
        Dim cTmpStr As String
        Dim cVocePrec As String
        Dim cUM As String
        Dim nOldDFA_one_vend As Decimal
        Dim nOldDFA_one_rete As Decimal
        Dim nOldDFA_imposte As Decimal
        Dim nOldDFA_one_dive As Decimal
        Dim dt As DS_PF_ASM_tari.TBL_fattura_rowDataTable
        Dim dv As DataView

        If (_dr_dfa_pf Is Nothing) Then
            _dr_dfa_pf = _PrintFatturaDataset.TBL_dati_fatt.NewRow
            _dr_dfa_pf.DFA_cod_mai = _dr_mai_pf.MAI_codice
            _PrintFatturaDataset.TBL_dati_fatt.Rows.Add(_dr_dfa_pf)
            nOldDFA_one_vend = 0
            nOldDFA_one_rete = 0
            nOldDFA_imposte = 0
            nOldDFA_one_dive = 0
        Else
            nOldDFA_one_vend = _dr_dfa_pf.DFA_one_vend
            nOldDFA_one_rete = _dr_dfa_pf.DFA_one_rete
            nOldDFA_imposte = _dr_dfa_pf.DFA_imposte
            nOldDFA_one_dive = _dr_dfa_pf.DFA_one_dive
        End If
        Try
            _dr_dfa_pf.DFA_totale_abcd = 0
            dt = New DS_PF_ASM_tari.TBL_fattura_rowDataTable
            For Each cSezioneXml As String In New String() {"SezioniDocumenti", "SezioneVociGeneriche"}
                '
                ' nel caso di voci generiche il caricamento dei dati va fatto solo in caso di stato raccolta = 1 e 9 
                '
                If Not ((cSezioneXml = "SezioneVociGeneriche") And (_dr_mai_pf.DEF_raccolta <> 1 And _dr_mai_pf.DEF_raccolta <> 9)) Then
                    If (dr_fat.GetChildRows(String.Concat("Fattura_", cSezioneXml)).Length > 0) And (dr_fat.GetChildRows(String.Concat("Fattura_", cSezioneXml)).Length > nPodId) Then
                        For Each dr_tmp1 As DataRow In dr_fat.GetChildRows(String.Concat("Fattura_", cSezioneXml))(nPodId).GetChildRows(String.Concat(cSezioneXml, "_SezioniStampa"))
                            nSezioneStampa = Costanti.SEZ_no_sezione
                            cTmpStr = dr_tmp1("DescrizioneSezioneStampa").ToString.ToLower
                            If (cTmpStr = "TARIFFA RIFIUTI".ToLower) Then
                                nSezioneStampa = Costanti.SEZ_SezioneVendita
                            ElseIf (cTmpStr = "ADDIZIONALE PROVINCIALE".ToLower) Then
                                nSezioneStampa = Costanti.SEZ_Sezionerete
                                _dr_dfa_pf.DFA_one_rete = nOldDFA_one_rete + dr_tmp1("TotaleSezione")
                                _dr_dfa_pf.DFA_totale_abcd += _dr_dfa_pf.DFA_one_rete
                                '   AddDescTotale("B")
                            ElseIf (cTmpStr = "TOTALE IMPOSTE".ToLower) Or (cTmpStr = "IMPOSTE".ToLower) Then
                                nSezioneStampa = Costanti.SEZ_Imposte
                                _dr_dfa_pf.DFA_imposte = nOldDFA_imposte + dr_tmp1("TotaleSezione")
                                _dr_dfa_pf.DFA_totale_abcd += _dr_dfa_pf.DFA_imposte
                                '   AddDescTotale("C")
                            ElseIf (cSezioneXml = "SezioneVociGeneriche") And ((cTmpStr = "SEZIONE VOCE GENERICA".ToLower) Or (cTmpStr = "SEZIONE VOCI GENERICHE".ToLower)) Then
                                nSezioneStampa = Costanti.SEZ_Rielabora
                            End If

                            If nSezioneStampa > Costanti.SEZ_no_sezione Then
                                For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("SezioniStampa_SottosezioneStampa")
                                    nOrdineStampa = 1
                                    cVocePrec = ""
                                    For Each dr_tmp3 As DataRow In dr_tmp2.GetChildRows("SottosezioneStampa_RigaFattura")
                                        dr_far = dt.NewRow
                                        dr_far.FAR_cod_daf = _dr_daf_pf.DAF_codice
                                        dr_far.FAR_id_sezione = nSezioneStampa
                                        dr_far.FAR_row_desc = False
                                        dr_far.FAR_des_voce = dr_tmp3("Voce")
                                        dr_far.FAR_ord_stampa = nOrdineStampa
                                        If dr_far.FAR_des_voce.ToUpper = "ARROTONDAMENTO" Then
                                            dr_far.FAR_ord_stampa = 1000
                                        End If
                                        dr_far.FAR_dal = dr_tmp3("DataInizio")
                                        dr_far.FAR_al = dr_tmp3("DataFine")
                                        If nSezioneStampa <> Costanti.SEZ_OneriDiversi Then
                                            If Not IsDBNull(dr_tmp3("PrezzoUnitario")) Then
                                                dr_far.FAR_prezzo_uni = dr_tmp3("PrezzoUnitario")
                                            End If
                                        Else
                                            dr_far.FAR_prezzo_uni = 0
                                        End If

                                        dr_far.FAR_quantita1 = Int(dr_tmp3("Qta1"))
                                        dr_far.FAR_quant_2_ex = False

                                        cUM = ""
                                        Try
                                            cUM = dr_tmp3("UM1")
                                        Catch ex As Exception
                                            cUM = ""
                                        End Try

                                        Try
                                            cTmp = dr_tmp3("UM2")
                                            ' 20111006 dr_far.FAR_quantita2 = Int(dr_tmp3("Qta2"))
                                            dr_far.FAR_quantita2 = dr_tmp3("Qta2")
                                            dr_far.FAR_quant_2_ex = True
                                        Catch ex As Exception
                                            cTmp = ""
                                            dr_far.FAR_quantita2 = -1
                                        End Try

                                        If cTmp > "" Then
                                            dr_far.FAR_uni_mis = String.Concat(cUM, "/", cTmp)
                                        Else
                                            dr_far.FAR_uni_mis = cUM
                                        End If
                                        dr_far.FAR_importo = dr_tmp3("Importo")
                                        lPrezUniNull = (dr_far.FAR_id_sezione <> Costanti.SEZ_OneriDiversi)
                                        If lPrezUniNull Then
                                            If IsDBNull(dr_far("FAR_prezzo_uni")) Then
                                                dr_far.FAR_prezzo_uni = ""
                                                lPrezUniNull = False
                                            Else
                                                lPrezUniNull = IsNumeric(dr_far.FAR_prezzo_uni)
                                                If lPrezUniNull Then
                                                    lPrezUniNull = (dr_far.FAR_prezzo_uni = 0)
                                                Else
                                                    dr_far.FAR_prezzo_uni = ""
                                                End If
                                            End If
                                        End If
                                        If Not lPrezUniNull Then
                                            '  _PrintFatturaDataset.TBL_fattura_row.Rows.Add(dr_far)
                                            dt.Rows.Add(dr_far)
                                            nRow += 1
                                        End If
                                    Next
                                Next
                            End If
                        Next
                    End If
                End If
            Next
            dv = New DataView(dt)
            dv.Sort = "FAR_ord_stampa"
            For Each drv As DataRowView In dv
                _PrintFatturaDataset.TBL_fattura_row.ImportRow(drv.Row)
            Next
        Catch ex As Exception
            _ErroriGravi = String.Concat(_ErroriGravi, "Carica_RigheFatturazione", ex, vbCr)
        End Try

    End Sub

    ' ************************************************************************** '
    ' Questa funzione serve a leggere un file XML con la codifica WINDOWS-1252 e '
    ' a caricarlo in un Dataset.                                                 '
    ' ************************************************************************** '
    Private Function caricaFileDatiDS(ByVal cFileName As String) As DataSet
        Dim myEncoding As System.Text.Encoding
        Dim sr_xml As System.IO.StreamReader
        Dim ds_src As DataSet

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)
            sr_xml = New System.IO.StreamReader(cFileName, myEncoding)
            ds_src = New DataSet
            ds_src.ReadXml(sr_xml)
            sr_xml.Close()
        Catch ex As Exception
            ds_src = Nothing
            Throw ex
        End Try
        Return ds_src

    End Function

    Private Function GetTagXML(ByVal dr_exp As DataRow, ByVal aListaTag As String()) As String
        Dim cTagList As String
        Dim aTmp As String()
        Dim dr_tmp As DataRow
        Dim cRtn As String

        cRtn = ""
        If aListaTag.Length = 2 Then
            Try
                cRtn = dr_exp(aListaTag(1))
            Catch ex As Exception
                cRtn = ""
            End Try
        ElseIf aListaTag.Length > 2 Then
            cTagList = String.Concat(aListaTag(0), "_", aListaTag(1))
            If dr_exp.GetChildRows(cTagList).Length > 0 Then
                dr_exp = dr_exp.GetChildRows(cTagList)(0)
                Array.Reverse(aListaTag)
                aTmp = New String() {}
                ReDim aTmp(aListaTag.Length - 2)
                Array.Copy(aListaTag, aTmp, aListaTag.Length - 1)
                ReDim aListaTag(aTmp.Length - 1)
                Array.Copy(aTmp, aListaTag, aTmp.Length)
                Array.Reverse(aListaTag)
                cRtn = GetTagXML(dr_exp, aListaTag)
            End If
        End If
        Return cRtn

    End Function

#Region "Procedura per la stampa dell'F24"

    Private Sub GetRecordF24ToPrint(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_fat_imp As DataRow, ByVal cFeedString As String)
        Dim dr_fxr As DS_PF24_ASM_tari.TBL_fat_x_ratRow
        Dim dr_rdo As DS_PF24_ASM_tari.TBL_rich_domRow
        Dim dr_fat As DS_PF24_ASM_tari.TBL_fattureRow
        Dim dr_rat As DS_PF24_ASM_tari.TBL_rateRow
        Dim nRate As Integer

        dr_fat = _PrintF24Dataset.TBL_fatture.NewRow
        With dr_fat
            .FAT_codice = _PrintF24Dataset.TBL_fatture.Rows.Count + 1
            .FAT_anno_fattura = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "AnnoFattura")
            .FAT_num_fattura = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "NumeroFattura")
            If RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "DataNascita") = "" Then
                .FAT_cognome = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "RagioneSociale")
                .FAT_nome = ""
                .FAT_codice_fiscale = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "PartitaIva")
                .FAT_data_nascita = ""
                .FAT_sesso = ""
                .FAT_comune_nasc = ""
                .FAT_prov_nasc = ""
            Else
                .FAT_cognome = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "Cognome")
                .FAT_nome = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "Nome")
                .FAT_codice_fiscale = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "CodiceFiscale")
                .FAT_data_nascita = CDate(RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "DataNascita")).ToString("ddMMyyyy")
                .FAT_sesso = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "Sesso")
                If RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "NazioneNascita") = "ITALIA" Then
                    .FAT_comune_nasc = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "ComuneNascita")
                    .FAT_prov_nasc = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "ProvinciaNascita")
                Else
                    .FAT_comune_nasc = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "NazioneNascita")
                    .FAT_prov_nasc = "EE"
                End If
            End If
            If RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "ImportoRata") > "" Then
                .FAT_importo_totale = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "ImportoRata")
            Else
                .FAT_importo_totale = 0
            End If
        End With
        _PrintF24Dataset.TBL_fatture.Rows.Add(dr_fat)

        nRate = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "NumeroRATE", "")
        For i As Integer = 0 To nRate - 1
            dr_rat = _PrintF24Dataset.TBL_rate.NewRow
            With dr_rat
                .RAT_codice = _PrintF24Dataset.TBL_rate.Rows.Count + 1
                .RAT_sezione = "EL"
                .RAT_cod_tributo = "3944"
                .RAT_codice_ente = "L117"
                .RAT_num_immobili = RecuperaValoreDS_UE(ds_src, dr_fat_imp, i, "NumeroSEZ", "NumeroSEZ")
                .RAT_rateizzazione = "0101"
                .RAT_anno_riferimento = "2014"
                .RAT_codice_identificativo = RecuperaValoreDS_UE(ds_src, dr_fat_imp, i, "Rate", "OCR")
                .RAT_scadenza = RecuperaValoreDS_UE(ds_src, dr_fat_imp, i, "Rate", "DataScadenza")
                If RecuperaValoreDS_UE(ds_src, dr_fat_imp, i, "Rate", "ImportoRata") > "" Then
                    .RAT_importo = RecuperaValoreDS_UE(ds_src, dr_fat_imp, i, "Rate", "ImportoRata")
                Else
                    .RAT_importo = 0
                End If
            End With
            _PrintF24Dataset.TBL_rate.Rows.Add(dr_rat)

            ' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
            dr_fxr = _PrintF24Dataset.TBL_fat_x_rat.NewRow
            dr_fxr.FXR_codice = _PrintF24Dataset.TBL_fat_x_rat.Rows.Count + 1
            dr_fxr.DEF_alimimb = cFeedString
            If nRate = 1 Then
                dr_fxr.DEF_raccolta = 3
            ElseIf i = 0 Then
                dr_fxr.DEF_raccolta = 1
            ElseIf i = nRate - 1 Then
                dr_fxr.DEF_raccolta = 3
            Else
                dr_fxr.DEF_raccolta = 1
            End If


            dr_fxr.FXR_linkQD = nCodice
            dr_fxr.FXR_cod_fat = dr_fat.FAT_codice
            dr_fxr.FXR_cod_rat = dr_rat.RAT_codice
            _PrintF24Dataset.TBL_fat_x_rat.Rows.Add(dr_fxr)
        Next
        ' Carico i record nella richiesta di domiciliazione 
        dr_rdo = _PrintF24Dataset.TBL_rich_dom.NewRow
        With dr_rdo
            .RDO_codice = _PrintF24Dataset.TBL_rich_dom.Rows.Count + 1
            .RDO_cod_fat = dr_fat.FAT_codice
            .RDO_cod_sia = GetValueDatiFissi("FixBol", "6")
            .RDO_tipo_serv = GetValueDatiFissi("FixBol", "7")
            .RDO_cod_domic = ""
        End With
        _PrintF24Dataset.TBL_rich_dom.Rows.Add(dr_rdo)

    End Sub
#End Region
End Class
