Imports PLG_ASM_soll_tari_AMELIA.Costanti

Public Class FRM_SwitchEcc

    Private _Soll_FileName As String
    Private _QuickDataset_FileName As String

    Public WriteOnly Property Soll_FileName() As String

        Set(ByVal value As String)
            _Soll_FileName = value
        End Set

    End Property

    Public WriteOnly Property QuickDataset_FileName() As String

        Set(ByVal value As String)
            _QuickDataset_FileName = value
        End Set

    End Property

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_carica.Click
        Dim xnIDSoggetto As Xml.XmlNodeList
        Dim xdSoll As Xml.XmlDocument
        Dim data_excel As DataSet
        Dim xnSoll As Xml.XmlNode
        Dim xnSwitchEcc As Xml.XmlNode
        Dim xdDati_qd As Xml.XmlDocument
        Dim xnRecQD As Xml.XmlNode
        Dim nTotale As Decimal

        data_excel = ReadExcel(TextBox1.Text)
        If data_excel IsNot Nothing Then
            xdSoll = New Xml.XmlDocument
            xdDati_qd = New Xml.XmlDocument
            xdSoll.Load(_Soll_FileName)
            xdDati_qd.Load(_QuickDataset_FileName)
            For Each dr As DataRow In data_excel.Tables(0).Rows
                xnIDSoggetto = xdSoll.SelectNodes(String.Concat("EmissioneSolleciti/Sollecito/Cliente/IDSoggetto[.='", dr(0), "']"))

                xnSoll = Nothing
                xnSwitchEcc = Nothing
                If xnIDSoggetto.Count = 0 Then
                    xnSoll = Nothing
                ElseIf xnIDSoggetto.Count = 1 Then
                    xnSoll = xnIDSoggetto(0)
                    While xnSoll.Name <> "Sollecito"
                        xnSoll = xnSoll.ParentNode
                    End While
                Else
                    For Each xnTmp As Xml.XmlNode In xnIDSoggetto
                        xnTmp = xnTmp.ParentNode.ParentNode.SelectSingleNode(String.Concat("RataSollecitata/SezioniDocumenti/Contratto/MatricolaContatore[.='", dr(2), "']"))
                        If xnTmp IsNot Nothing Then
                            xnSoll = xnTmp
                            While xnSoll.Name <> "Sollecito"
                                xnSoll = xnSoll.ParentNode
                            End While
                        End If
                    Next
                End If

                If xnSoll IsNot Nothing Then
                    xnSwitchEcc = xdSoll.SelectSingleNode("SwitchEccedentario")
                    If xnSwitchEcc Is Nothing Then
                        xnSwitchEcc = xdSoll.CreateNode(Xml.XmlNodeType.Element, "SwitchEccedentario", "")
                        xnSwitchEcc.InnerText = "1"
                        xnSoll.AppendChild(xnSwitchEcc)
                    End If
                    xnSwitchEcc.InnerText = 1 - xnSwitchEcc.InnerText
                    ' Aggiorna importo e stampabilit�
                    xnRecQD = xdDati_qd.SelectSingleNode(String.Concat(Costanti.itm_qd_Sollecitato, "/MAI_num_sollecito[.='", xnSoll.SelectSingleNode("IDSollecito").InnerText, "']")).ParentNode

                    nTotale = CDec(xnSoll.SelectSingleNode("TotaleSollecito").InnerText.ToString.Replace(".", ",")) - _
                              CDec(xnSoll.SelectSingleNode("TotaleAnticipazioneRaggrupp").InnerText.ToString.Replace(".", ","))

                    If xnSwitchEcc.InnerText = 1 Then
                        nTotale -= CDec(xnSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText.ToString.Replace(".", ","))
                    End If
                    If xnRecQD.SelectSingleNode("DEF_errcode").InnerText >= "0" Then
                        If Not nTotale > cnt_ThresholdImporto Then
                            xnRecQD.SelectSingleNode("DEF_errcode").InnerText = "1"
                            xnRecQD.SelectSingleNode("DEF_errdesc").InnerText = String.Concat("Il totale dei solleciti � uguale o inferiore a � ", cnt_ThresholdImporto, " per cui non � necessario stampare alcunch�. Totale= � ", Format(nTotale, "#,##0.00"))
                        Else
                            xnRecQD.SelectSingleNode("DEF_errcode").InnerText = "0"
                            xnRecQD.SelectSingleNode("DEF_errdesc").InnerText = ""
                        End If
                    End If
                Else
                    System.Windows.Forms.MessageBox.Show("Non � stato azzerare l'utilizzo dell'importo eccedentario.", "Errore file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error)
                End If
            Next
            xdSoll.Save(_Soll_FileName)
            xdDati_qd.Save(_QuickDataset_FileName)
            System.Windows.Forms.MessageBox.Show("Caricamento terminato. E' necessario effettuare il trefresh del db con l'apposito pulsante.", "Correzione terminata", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error)
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        Else
            System.Windows.Forms.MessageBox.Show("Non � stato possibile leggere informazioni dal file fornito.", "Errore file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error)
        End If


    End Sub

    Private Function ReadExcel(ByVal cFileName As String) As DataSet
        Dim da As OleDb.OleDbDataAdapter
        Dim strConn As String
        Dim ds_exc As DataSet

        Try
            strConn = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cFileName, ";", "Extended Properties=""Excel 8.0;""")
            ds_exc = New DataSet
            da = New OleDb.OleDbDataAdapter("SELECT * FROM [Foglio1$]", strConn)
            da.Fill(ds_exc)
        Catch ex As Exception
            ds_exc = Nothing
        End Try
        Return ds_exc

    End Function

    Private Sub BTN_annulla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_annulla.Click

        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()

    End Sub

End Class