﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using System.Xml;
using System.Data.OleDb;
using TipiComuni;
using System.IO;

namespace PLG_ANE_dir228
{
    public class PLG_ANE_dir228 : PLUGIN_interfaceV1_5_0.IPLG_dati_lib
    {
        private int _CurrentRecordNumber;
        private string _DBGiriConnectionString;
        private ArrayList _ElementiMenuDedicati;
        private string _FiltroSelezione;
        private ArrayList _ListaGiri;
        private DataSet _LogDSStampa;
        private Boolean _NuoviToponimi;
        private string _SessioneFld;
        private Boolean _SessioneNuova;
        private int _TipoCaricamento;
        private string _SessioneDatiFissiFileName;
        private string _SessioneTrasformazioniFileName;
        private string _SessioneDatiPath;
        private string _FeedString;
        private int _CodiceModulo;
        private ArrayList _ResultAL;
        //private DS.DS_import DSImported;
        private DS.DS_quick_db _QuickDataset;
        private XmlDocument _xmlDati_qd;
        private XmlNode _SezioneDati;
        private string _ErroriLievi;
        private string _ErroriGravi;
        private DS.DS_prn_fattura _PrintFatturaDataset;
        private DS.DS_prn_bollettino _PrintBollettinoDataset;
        //private DS.DS_send_email _SendEmailDataset;
        private int _RecordCount;

        public enum ListaDataset
        {
            DAS_quick = 0,
            DAS_fattura = 1,
            DAS_bollettino896 = 2,
            DAS_bollettino896PA = 3,
            DAS_AllegatoPA = 4,
            DAS_F24S = 5,
            DAS_send_email = 999,
        }

        public Boolean CapacitaInvioMail
        {
            get
            {
                return false;
            }
        }

        public DataSet FullDataset
        {
            get
            {
                switch (_CodiceModulo)
                {
                    case (int)ListaDataset.DAS_fattura:
                        return _PrintFatturaDataset;
                        break;
                    case (int)ListaDataset.DAS_bollettino896:
                    case (int)ListaDataset.DAS_bollettino896PA:
                    case (int)ListaDataset.DAS_F24S:
                    case (int)ListaDataset.DAS_AllegatoPA:
                        return _PrintBollettinoDataset;
                        break;
                    //case (int)ListaDataset.DAS_send_email:
                    //    return _SendEmailDataset;
                    default:
                        return null;
                        break;
                };
            }
        }

        public string MessaggioAvviso
        {
            get
            {
                return "";
            }
        }

        public string FullDataSet_MainTableName
        {
            get
            {
                switch (_CodiceModulo)
                {
                    case (int)ListaDataset.DAS_fattura:
                        {
                            return "TBL_main";
                            break;
                        }
                    case (int)ListaDataset.DAS_bollettino896:
                    case (int)ListaDataset.DAS_bollettino896PA:
                    case (int)ListaDataset.DAS_F24S:
                    case (int)ListaDataset.DAS_AllegatoPA:
                        {
                            return "TBL_fat_x_rat";
                            break;
                        }
                    case (int)ListaDataset.DAS_send_email:
                        {
                            return "TBL_main";
                            break;
                        }
                    default:
                        {
                            return "";
                            break;
                        }
                }
            }
        }

        public string FullDataset_MainTableKey
        {
            get
            {
                return "";
            }
        }

        public ArrayList ListaControlliFormNuovaSessione
        {
            get
            {
                ArrayList aCtr;

                aCtr = new ArrayList();
                aCtr.Add(new String[] { "LBL_input_1", "File dati ...................:", "3;16", "155;13" });
                aCtr.Add(new String[] { "TXB_input_1", "&1", "160;12", "610;20", "*1" });
                aCtr.Add(new String[] { "BTN_fld_input_1", "", "775;10", "27;23" });
                //aCtr.Add(new String[] { "LBL_input_3", "File credenziali sito web ...:", "3;42", "155;13" });
                //aCtr.Add(new String[] { "TXB_input_3", "&1", "160;38", "610;20", "102" });
                //aCtr.Add(new String[] { "BTN_fil_input_3", "", "775;36", "27;23" });
                //aCtr.Add(new String[] { "LBL_input_4", "File allegato tipografico ...:", "3;68", "155;13" });
                //aCtr.Add(new String[] { "TXB_input_4", "&1", "160;64", "610;20", "190" });
                //aCtr.Add(new String[] { "BTN_fil_input_4", "", "775;62", "27;23" });
                aCtr.Add(new String[] { "LBL_output_998", "Cartella di destinazione .:", "3;94", "155;13" });
                aCtr.Add(new String[] { "TXB_output_998", "&998", "160;90", "610;20", "201" });
                aCtr.Add(new String[] { "BTN_fld_output_998", "", "775;88", "27;23" });
                aCtr.Add(new String[] { "LBL_output_999", "Descrizione archivio ......:", "3;120", "155;13" });
                aCtr.Add(new String[] { "TXB_output_999", "", "160;116", "M;640;122", "202" });
                //                aCtr.Add(new String[] { "SRT", "", "", "", "" });
                return aCtr;
            }
        }

        public string NomePlugIn
        {
            get
            {
                return "Plug-in per la stampa delle bollette della CoSAP per il comune di Senigallia.";
            }
        }

        public DataSet QuickDataset
        {
            get
            {
                return _QuickDataset;
            }
        }

        public string QuickDataset_MainTableKey
        {
            get
            {
                return "MAI_codice";
            }
        }

        public string QuickDataset_MainTableName
        {
            get
            {
                return "TBL_main";
            }
        }

        public ArrayList ResultAL
        {
            get
            {
                return _ResultAL;
            }
        }

        public ArrayList DatiAzioniPreInvio
        {
            get
            {
                return null;
            }
        }

        public ArrayList DatiAzioniPostStampa
        {
            get
            {
                return null;
            }
        }

        public Boolean StampaInProprio
        {
            get
            {
                return true;
            }
        }


        public int CodiceModulo
        {
            set
            {
                _CodiceModulo = value;
            }
        }

        public string FeedString
        {
            set
            {
                _FeedString = value;
            }
        }

        public string SessioneDatiPath
        {
            set
            {
                _SessioneDatiPath = value;
            }
        }
        public string SessioneTrasformazioniFileName
        {
            set
            {
                _SessioneTrasformazioniFileName = value;
            }
        }
        public string SessioneDatiFissiFileName
        {
            set
            {
                _SessioneDatiFissiFileName = value;

            }
        }

        public string SessioneFld
        {
            set
            {
                _SessioneFld = value;
            }
        }
        public XmlNode SezioneDati
        {
            set
            {
                _SezioneDati = value;
            }
        }
        public string[] MessaggiAvanzamento
        {
            get;
            set;
        }

        public XmlNode DataSendByMail
        {
            get
            {
                return null;
            }
        }

        public int CurrentRecordNumber
        {
            get
            {
                return _RecordCount;
            }
        }



        public ArrayList ElementiMenuDedicati
        {
            get
            {
                return new ArrayList();
            }
        }



        public DataSet LogDSStampa
        {
            get
            {
                return _LogDSStampa;
            }
        }

        public string FiltroSelezione
        {
            set
            {
                _FiltroSelezione = value;
            }
        }


        public string LinkField_QuickDS_FullDS
        {
            get
            {
                switch (_CodiceModulo)
                {
                    case (int)ListaDataset.DAS_fattura:
                        return "MAI_linkQD";
                    case (int)ListaDataset.DAS_bollettino896:
                    case (int)ListaDataset.DAS_bollettino896PA:
                    case (int)ListaDataset.DAS_AllegatoPA:
                    case (int)ListaDataset.DAS_F24S:
                        return "FXR_linkQD";
                    case (int)ListaDataset.DAS_send_email:
                        return "MAI_linkQD";
                    default:
                        return "";
                }
            }
        }


        public Boolean SessioneNuova
        {
            set
            {
                _SessioneNuova = value;
            }
        }

        public int TipoCaricamento
        {
            set
            {
                _TipoCaricamento = value;
            }
        }


        public ArrayList ListaGiri
        {
            set
            {
                _ListaGiri = value;
            }
        }

        public string DBGiriConnectionString
        {
            set
            {
                _DBGiriConnectionString = value;
            }
        }

        public Boolean NuoviToponimi
        {
            get
            {
                return _NuoviToponimi;
            }
        }

        public Boolean CheckCondPSA(int nCond2Check, string cValue2Check)
        {
            return false;
        }

        public Boolean CheckCondAPI(int nCond2Check, string cValue2Check)
        {
            return false;
        }

        public void ExecActionPSA(int nAct2Exec, string cValue2Exec)
        {
        }

        public void ExecActionAPI(string cTipoExecAPI, int nAct2Exec, string cValue2Exec)
        {
        }

        public void LoadFullDataset(int nRecordToLoad)
        {
            DS.DS_quick_db.TBL_mainRow dr_mai_qd;
            Appoggio.G20Entity ele;
            int nRecordToPrint;

            // ************************************************************************** '
            // Creazione del Dataset e azzeramento del contatore dei record da stampare.  '
            // ************************************************************************** '
            nRecordToPrint = 0;
            switch (_CodiceModulo)
            {
                case (int)ListaDataset.DAS_fattura:
                    {
                        _PrintFatturaDataset = new DS.DS_prn_fattura();
                        break;
                    }
                case (int)ListaDataset.DAS_bollettino896:
                case (int)ListaDataset.DAS_bollettino896PA:
                case (int)ListaDataset.DAS_AllegatoPA:
                case (int)ListaDataset.DAS_F24S:
                    {
                        _PrintBollettinoDataset = new DS.DS_prn_bollettino();
                        break;
                    }
                //case (int)ListaDataset.DAS_send_email:
                //    {
                //        _SendEmailDataset = new DS.DS_send_email();
                //        break;
                //    }
            }

            // ************************************************************************** '
            // Caricamento del DataSet da utilizzare per la stampa.                       '
            // ************************************************************************** '
            while (_RecordCount < _QuickDataset.TBL_main.Rows.Count && nRecordToPrint < nRecordToLoad)
            {
                dr_mai_qd = (DS.DS_quick_db.TBL_mainRow)_QuickDataset.TBL_main.Rows[_RecordCount];
                if (dr_mai_qd.DEF_toprint)
                {
                    try
                    {
                        _ErroriLievi = "";
                        _ErroriGravi = "";
                        ele = CaricaRecord(PrintDataSet_DataFile(dr_mai_qd.MAI_filedati), dr_mai_qd.MAI_num_fattura);

                        switch (_CodiceModulo)
                        {
                            case (int)ListaDataset.DAS_fattura:
                                PreparaRecordPerStampaFattura(dr_mai_qd.MAI_codice, ele, GetFeedString(dr_mai_qd));
                                break;
                            case (int)ListaDataset.DAS_bollettino896:
                                if (dr_mai_qd.MAI_print_bol896)
                                    PreparaRecordPerStampaBoll896(dr_mai_qd.MAI_codice, ele, GetFeedString(dr_mai_qd));
                                break;
                            //case (int)ListaDataset.DAS_bollettino896PA:
                            //    if (dr_mai_qd.MAI_print_bolPA)
                            //        PreparaRecordPerStampaBoll896PA(dr_mai_qd.MAI_codice, ds_src, GetFeedString(dr_mai_qd));
                            //    break;
                            //case (int)ListaDataset.DAS_AllegatoPA:
                            //    if (dr_mai_qd.MAI_print_AllPA)
                            //        PreparaRecordPerStampaBoll896PA(dr_mai_qd.MAI_codice, ds_src, GetFeedString(dr_mai_qd));
                            //    break;
                            //case (int)ListaDataset.DAS_F24S:
                            //    if (dr_mai_qd.MAI_print_F24)
                            //        PreparaRecordPerStampaF24Semplificato(dr_mai_qd.MAI_codice, ds_src, GetFeedString(dr_mai_qd));
                            //    break;
                            case (int)ListaDataset.DAS_send_email:
                                //GetRecordEmailToSend(dr_mai_qd.MAI_codice, ds_src, dr_fat)
                                break;
                        }

                        if (_ErroriGravi != "")
                        {
                            dr_mai_qd.DEF_errcode = -1;
                            dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori Gravi: ", _ErroriGravi);
                        }
                        else if (_ErroriLievi != "")
                        {
                            dr_mai_qd.DEF_errcode = -2;
                            dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori lievi: ", _ErroriLievi);
                        }
                        else
                        {
                            dr_mai_qd.DEF_errcode = 0;
                            dr_mai_qd.DEF_errdesc = "";
                        }
                    }
                    catch (Exception ex)
                    {
                        dr_mai_qd.DEF_errcode = -1;
                        dr_mai_qd.DEF_errdesc = ex.Message;
                    }
                    nRecordToPrint += 1;
                }
                _RecordCount += 1;
            }

            // ************************************************************************** '
            // Consolidamento del DataSet da utilizzare per la stampa.                    '
            // ************************************************************************** '
            switch (_CodiceModulo)
            {
                case (int)ListaDataset.DAS_fattura:
                    _PrintFatturaDataset.AcceptChanges();
                    break;
                case (int)ListaDataset.DAS_bollettino896:
                case (int)ListaDataset.DAS_bollettino896PA:
                case (int)ListaDataset.DAS_AllegatoPA:
                case (int)ListaDataset.DAS_F24S:
                    _PrintBollettinoDataset.AcceptChanges();
                    break;
                //case (int)ListaDataset.DAS_send_email:
                //    _SendEmailDataset.AcceptChanges();
                //    break;
            }
        }



        public void UpdateQuickDatasetFile()
        {
        }

        public string[] GetSelectedRow(string _SelectFromPlugInType, string cValue)
        {
            return null;
        }

        public void CreaDataBaseSessione(ArrayList _DatiSessione)
        {
            //DS.DS_import.TBL_tr_20Row dr20;
            //DS.DS_import.TBL_tr_40Row dr40;
            //FC_manage_giri mng_giri;
            XmlDocument xdWork2;
            String cSortValue;
            XmlNode xiRecord;
            int nRecord;
            XmlNode xiWork2;
            //Boolean lPrint896;
            //Boolean lPrint896PA;
            //Boolean lPrintF24;
            string cDstFile;
            List<Appoggio.G20Entity> ListaDocuemnti = new List<Appoggio.G20Entity>();

            _ResultAL = new ArrayList();
            // ************************************************************************** '
            // Copia i file dei dati dalla cartella dove sono attualmente nella cartella  '
            // dati della sessione.                                                       '
            // ************************************************************************** '
            ImpostaMessaggi(new String[] { "", "", "", "0", _DatiSessione.Count.ToString(), "Elaborazione dei file da caricare...." });
            foreach (string[] aValue in _DatiSessione)
            {
                MessaggiAvanzamento[3] = (int.Parse(MessaggiAvanzamento[3]) + 1).ToString();
                if (aValue[1].EndsWith(".pdf"))
                {
                    cDstFile = String.Concat(_SessioneDatiPath, aValue[1].Substring(aValue[1].LastIndexOf("\\") + 1));
                    System.IO.File.Copy(aValue[1], cDstFile);
                    _ResultAL.Add(new String[] { "filename", cDstFile.Replace(_SessioneFld, ""), MessaggiAvanzamento[3], "1", "0" });
                }
                else if (aValue[1].EndsWith(".asc"))
                {
                    List<Appoggio.G20Entity> appo = ImportaFileInSessione(int.Parse(aValue[0]), aValue[1]);
                    appo.Select(c => { c.NumeroFile = MessaggiAvanzamento[3]; return c; }).ToList();
                    ListaDocuemnti = ListaDocuemnti.Concat(appo).ToList();
                    cDstFile = String.Concat(_SessioneDatiPath, aValue[1].Substring(aValue[1].LastIndexOf("\\") + 1));
                    System.IO.File.Copy(aValue[1], cDstFile);
                    _ResultAL.Add(new String[] { "filename", cDstFile.Replace(_SessioneFld, ""), MessaggiAvanzamento[3], "1", "0" });
                }
                else
                    throw new Exception("Tipo di file sconosciuto.");
                // ListaDocuemnti.Join<


            }

            // ************************************************************************** '
            // Caricamento dei dati dei giri che eventualmente sono stati selezionati.    '
            // ************************************************************************** '
            _NuoviToponimi = false;
            ImpostaMessaggi(new String[] { "", "", "", "0", "1", "Caricamento dei dati dei giri di consegna..." });
            //mng_giri = new FC_manage_giri(new OleDbConnection(_DBGiriConnectionString));
            //mng_giri.ListaGiri = _ListaGiri;
            xdWork2 = new XmlDocument();
            xdWork2.AppendChild(xdWork2.CreateNode(XmlNodeType.Element, "FATTURA", ""));

            // ************************************************************************** '
            // Creazione del file indice: creazione e ordinamento dei record recuperati e '
            // salvataggio dei nuovi toponimi rintracciati.                               '
            // ************************************************************************** '
            ImpostaMessaggi(new String[] { "", "", "", "0", _ResultAL.Count.ToString(), "Elaborazione dei file della cartella di archiviazione..." });
            foreach (Appoggio.G20Entity ele in ListaDocuemnti)
            {
                //dr20 = (DS.DS_import.TBL_tr_20Row)dr10.GetChildRows("TBL_tr_10TBL_tr_20")[0];
                //dr40 = (DS.DS_import.TBL_tr_40Row)dr10.GetChildRows("TBL_tr_10TBL_tr_40")[0];
                //lPrint896 = false;
                //lPrint896PA = false;
                //lPrintF24 = false;

                xiRecord = xdWork2.CreateNode(XmlNodeType.Element, "FATTURA_ROW", "");

                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_num_utente", ele.CodiceUtente));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_num_fattura", ele.NumeroDocumento));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_nominativo", ele.Denominazione));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_indirizzo_recapito", ele.IndirizzoSpedizione));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_citta_recapito", ele.ComuneSpedizione));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_cap_recapito", ele.CAPSpedizione));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_provincia_recapito", ele.ProvinciaSpedizione));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_print_bol896", "true"));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "DEF_errcode", "0"));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "DEF_errdesc", ""));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "DEF_onlyarchott", "false"));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "DEF_sendbymail", "false"));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_filedati", ele.NumeroFile));
                cSortValue = "111111";
                cSortValue += ele.ProvinciaSpedizione.ToLower().ToString();
                cSortValue += ele.ComuneSpedizione.ToLower().ToString();
                cSortValue += ele.IndirizzoSpedizione.ToLower().ToString();
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "SORTFIELD", cSortValue));
                xdWork2.SelectSingleNode("FATTURA").AppendChild(xiRecord);

                //  MessaggiAvanzamento[3] = (int.Parse(MessaggiAvanzamento[3]) + 1).ToString();
            }

            // ************************************************************************** '
            // ************************************************************************** '
            //ImpostaMessaggi(new string[] { "", "", "", "0", "1", "Salvataggio nuove vie/toponimi nel database..." });
            //try
            //{
            //    mng_giri.InserisciCittaToponimiPLUGIN();
            //    mng_giri = null;
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}


            // ************************************************************************** '
            // ************************************************************************** '
            MessaggiAvanzamento[5] = "Ordinamento dei record in corso...";
            FC_SortFile.SortFileIndiceXML(xdWork2, 0, xdWork2.SelectSingleNode("FATTURA").ChildNodes.Count - 1);

            // ************************************************************************** '
            // Rimuove il campo utilizzato per l'ordinamento e genera il file che servirà ' 
            // per l'invio delle fatture via mail.                                        '
            // ************************************************************************** '
            nRecord = 1;
            foreach (XmlNode xiWork1 in xdWork2.SelectNodes("FATTURA/FATTURA_ROW/SORTFIELD"))
            {
                xiWork2 = xiWork1.ParentNode;
                xiWork2.RemoveChild(xiWork1);
                xiWork2.AppendChild(SetRecordIDX(xdWork2, "MAI_codice", nRecord.ToString()));
                nRecord += 1;
            }

            // ************************************************************************** '
            // Salva il file indice nella sessione.                                       '
            // ************************************************************************** '
            xdWork2.Save(String.Concat(_SessioneFld, @"\dati\indice.xml"));
            _ResultAL.Add(new String[] { "filename", @"dati\indice.xml", "0", "0", "1" });
        }

        public void LoadQuickDataset()
        {
            DS.DS_quick_db.TBL_mainRow dr_mai_qd;
            Boolean lLoad;

            _QuickDataset = new DS.DS_quick_db();

            _xmlDati_qd = new XmlDocument();
            _xmlDati_qd.Load(QuickDataSet_IndexFile);
            ImpostaMessaggi(new String[] { "", "", "", "0", _xmlDati_qd.SelectNodes(PLG_costanti.itm_qd_Fattura).Count.ToString(), "Caricamento dei dati...." });

            foreach (XmlNode xmlItem in _xmlDati_qd.SelectNodes(PLG_costanti.itm_qd_Fattura))
            {
                lLoad = (_TipoCaricamento == 1) || (_TipoCaricamento == 2 && Boolean.Parse(GetValueFromXML(xmlItem, "DEF_sendbymail")));

                if (lLoad)
                {
                    dr_mai_qd = (DS.DS_quick_db.TBL_mainRow)_QuickDataset.TBL_main.NewRow();
                    try
                    {
                        dr_mai_qd.MAI_codice = int.Parse(GetValueFromXML(xmlItem, "MAI_codice"));
                        dr_mai_qd.DEF_toprint = false;
                        dr_mai_qd.DEF_errcode = short.Parse(GetValueFromXML(xmlItem, "DEF_errcode"));
                        dr_mai_qd.DEF_errdesc = GetValueFromXML(xmlItem, "DEF_errdesc");
                        dr_mai_qd.DEF_selectable = ((dr_mai_qd.DEF_errcode == 0) || (dr_mai_qd.DEF_errcode == -2));
                        dr_mai_qd.DEF_onlyarchott = Boolean.Parse(GetValueFromXML(xmlItem, "DEF_onlyarchott"));
                        dr_mai_qd.DEF_sendbymail = Boolean.Parse(GetValueFromXML(xmlItem, "DEF_sendbymail"));
                        dr_mai_qd.MAI_num_utente = GetValueFromXML(xmlItem, "MAI_num_utente");
                        dr_mai_qd.MAI_num_fattura = GetValueFromXML(xmlItem, "MAI_num_fattura");
                        //dr_mai_qd.MAI_chiave_doc = decimal.Parse(GetValueFromXML(xmlItem, "MAI_chiave_doc"));
                        dr_mai_qd.MAI_tipo_fattura = GetValueFromXML(xmlItem, "MAI_tipo_fattura");
                        dr_mai_qd.MAI_nominativo = GetValueFromXML(xmlItem, "MAI_nominativo");
                        dr_mai_qd.MAI_indirizzo_recapito = GetValueFromXML(xmlItem, "MAI_indirizzo_recapito");
                        dr_mai_qd.MAI_citta_recapito = GetValueFromXML(xmlItem, "MAI_citta_recapito");
                        dr_mai_qd.MAI_cap_recapito = GetValueFromXML(xmlItem, "MAI_cap_recapito");
                        dr_mai_qd.MAI_provincia_recapito = GetValueFromXML(xmlItem, "MAI_provincia_recapito");
                        dr_mai_qd.MAI_nazione_recapito = GetValueFromXML(xmlItem, "MAI_nazione_recapito");
                        dr_mai_qd.MAI_print_bol896 = Boolean.Parse((GetValueFromXML(xmlItem, "MAI_print_bol896").ToLower()));
                        // dr_mai_qd.MAI_print_bolPA = Boolean.Parse((GetValueFromXML(xmlItem, "MAI_print_bolPA").ToLower()));
                        //dr_mai_qd.MAI_print_AllPA = Boolean.Parse((GetValueFromXML(xmlItem, "MAI_print_AllPA").ToLower()));
                        //dr_mai_qd.MAI_print_F24 = Boolean.Parse((GetValueFromXML(xmlItem, "MAI_print_F24").ToLower()));
                        dr_mai_qd.MAI_filedati = int.Parse(GetValueFromXML(xmlItem, "MAI_filedati"));
                        //dr_mai_qd.MAI_IDFattura = GetValueFromXML(xmlItem, "MAI_IDFattura");
                    }
                    catch (Exception ex)
                    {
                        dr_mai_qd.DEF_errcode = -1;
                        dr_mai_qd.DEF_errdesc = ex.Message;
                    }
                    ImpostaMessaggi(new String[] { "", "", "", (MessaggiAvanzamento[3] + 1).ToString(), "", "" });
                    _QuickDataset.TBL_main.Rows.Add(dr_mai_qd);
                }
            }
        }


        public void PostAnteprima(ArrayList aLista)
        {

        }

        public void SetPrintable(DataRow dr, string _SelectFromPlugInType, string cParamSelez)
        {
        }

        public void SetSelectable(DataRow dr, string cFilter)
        {
        }

        public void RemovePRNDSErrorRecord()
        {
        }

        public void GoToRecordNumber(int nRecord)
        {
            _RecordCount = nRecord;
        }

        public void ImpostaDGVDati(DataGridView DGV)
        {
            DGV.Columns.Clear();
            if (_TipoCaricamento == 1)
                DGV.Columns.Add(Col_DaStampare());
            else if (_TipoCaricamento == 2)
                DGV.Columns.Add(Col_DaInviare()); ;
            DGV.Columns.Add(Col_Errore_code());
            DGV.Columns.Add(Col_Errore_desc());
            DGV.Columns.Add(Col_Progressivo());
            DGV.Columns.Add(Col_NumeroUtente());
            DGV.Columns.Add(Col_NumeroFattura());
            //DGV.Columns.Add(Col_TipoFattura());
            DGV.Columns.Add(Col_Nominativo());
            DGV.Columns.Add(COL_Indirizzo_Recapito());
            DGV.Columns.Add(COL_CAP_Recapito());
            DGV.Columns.Add(COL_Citta_Recapito());
            DGV.Columns.Add(COL_Provincia_Recapito());
            DGV.Columns.Add(COL_Print_Bollettino896());
            //DGV.Columns.Add(COL_Print_Bollettino896PA());
            //DGV.Columns.Add(COL_Print_AllegatoPA());
            //DGV.Columns.Add(COL_Print_BollettinoF24());
            DGV.Columns.Add(COL_SpedByMail());
        }

        private DataGridViewCheckBoxColumn Col_DaStampare()
        {
            DataGridViewCheckBoxColumn col;

            col = new DataGridViewCheckBoxColumn();
            col.HeaderText = "Stampa";
            col.DataPropertyName = "DEF_toprint";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Width = 50;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewCheckBoxColumn Col_DaInviare()
        {
            DataGridViewCheckBoxColumn col;

            col = new DataGridViewCheckBoxColumn();
            col.HeaderText = "Invia";
            col.DataPropertyName = "DEF_toprint";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Width = 50;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_Errore_code()
        {
            DataGridViewTextBoxColumn col;

            col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            col.HeaderText = "Errore";
            col.DataPropertyName = "DEF_errcode";
            col.Visible = false;
            return col;
        }

        private DataGridViewTextBoxColumn Col_Errore_desc()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Errore";
            col.DataPropertyName = "DEF_errdesc";
            col.Visible = false;
            return col;
        }

        private DataGridViewTextBoxColumn Col_Progressivo()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Progressivo";
            col.DataPropertyName = "MAI_codice";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            col.Width = 70;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_NumeroUtente()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Num. Utente";
            col.DataPropertyName = "MAI_num_utente";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            col.Width = 90;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_NumeroFattura()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Num. Fattura";
            col.DataPropertyName = "MAI_num_fattura";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            col.Width = 110;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_TipoFattura()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Tipo Fattura";
            col.DataPropertyName = "MAI_tipo_fattura";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Width = 110;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_Nominativo()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Nominativo";
            col.DataPropertyName = "MAI_nominativo";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_Indirizzo_Recapito()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Indirizzo Recapito";
            col.DataPropertyName = "MAI_indirizzo_recapito";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_CAP_Recapito()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "CAP";
            col.DataPropertyName = "MAI_cap_recapito";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_Citta_Recapito()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Città";
            col.DataPropertyName = "MAI_citta_recapito";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_Provincia_Recapito()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Prov.";
            col.DataPropertyName = "MAI_provincia_recapito";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_Print_Bollettino896()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Boll. 896";
            col.DataPropertyName = "MAI_print_bol896";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";
            return col;
        }

        private DataGridViewTextBoxColumn COL_Print_Bollettino896PA()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Boll. 896 PA";
            col.DataPropertyName = "MAI_print_bolPA";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";
            return col;
        }

        private DataGridViewTextBoxColumn COL_Print_AllegatoPA()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Allegato PA";
            col.DataPropertyName = "MAI_print_AllPA";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";
            return col;
        }
        private DataGridViewTextBoxColumn COL_Print_BollettinoF24()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "F24 Sempl.";
            col.DataPropertyName = "MAI_print_F24";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";
            return col;
        }

        private DataGridViewTextBoxColumn COL_SpedByMail()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Sped. Email";
            col.DataPropertyName = "DEF_sendbymail";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";

            return col;
        }

        public Boolean CheckDatiSessione(ArrayList aDati)
        {
            return true;
        }

        #region "Funzioni per la creazione dei dati della sessione."

        // ***************************
        //
        //
        // ***************************
        private List<Appoggio.G20Entity> ImportaFileInSessione(int nTipoFile, string cFileName)
        {
            string cTipoRecord;
            StreamReader sr;
            string cLinea;
            decimal nTmp;

            sr = new StreamReader(cFileName);
            List<Appoggio.G20Entity> lista = new List<Appoggio.G20Entity>();
            Appoggio.G20Entity a = null;
            Appoggio.G80Entity b = null;

            while (!sr.EndOfStream)
            {
                cLinea = sr.ReadLine();
                if (cLinea != "")
                {
                    cTipoRecord = cLinea.Substring(0, 3);

                    switch (cTipoRecord)
                    {
                        case "G20":
                            a = new Appoggio.G20Entity();
                            a.Progressivo = decimal.Parse(cLinea.Substring(3, 9).Trim());
                            a.CodiceUtente = cLinea.Substring(208, 22).Trim();
                            a.SDataDocumento = cLinea.Substring(12, 8).Trim();
                            a.NumeroDocumento = cLinea.Substring(20, 25).Trim();
                            a.Denominazione = cLinea.Substring(245, 75).Trim();
                            a.Indirizzo = cLinea.Substring(347, 90).Trim();
                            a.Localita = cLinea.Substring(437, 60).Trim();
                            a.CAP = cLinea.Substring(497, 5).Trim();
                            a.Comune = cLinea.Substring(502, 60).Trim();
                            a.Provincia = cLinea.Substring(562, 2).Trim();
                            a.CF = cLinea.Substring(320, 16).Trim();
                            a.PI = cLinea.Substring(336, 11).Trim();
                            a.DenominazioneSpedizione = cLinea.Substring(579, 75);
                            a.IndirizzoSpedizione = cLinea.Substring(654, 90);
                            a.LocalitaSpedizione = cLinea.Substring(744, 60);
                            a.CAPSpedizione = cLinea.Substring(804, 5);
                            a.ComuneSpedizione = cLinea.Substring(809, 60);
                            a.ProvinciaSpedizione = cLinea.Substring(869, 2);
                            lista.Add(a);
                            break;
                        case "G80":
                            b = new Appoggio.G80Entity();
                            b.Progressivo = decimal.Parse(cLinea.Substring(3, 9).Trim());

                            b.SImportoRata = cLinea.Substring(14, 11).Trim();
                            b.SDataScadenza = cLinea.Substring(50, 8).Trim();
                            b.NumeroDocumento = cLinea.Substring(265, 30).Trim();
                            nTmp = decimal.Parse(cLinea.Substring(25, 16).Trim());
                            b.OCR = cLinea.Substring(25, 16).Trim() + (nTmp % 93).ToString("00");
                            //if (a.Progressivo == b.Progressivo)
                            a.DatiDocumento = b;
                            //else
                            //    throw new Exception("Progressivo non coincidente");
                            break;
                    }
                }
            }
            sr.Close();
            // DSImported = new DS.DS_import();
            //sr = new StreamReader(cFileName);
            //while (!sr.EndOfStream)
            //{
            //    cLinea = sr.ReadLine();
            //    if (cLinea != "")
            //    {
            //        cLineaDivisa = cLinea.Split('|');
            //        cTabella = "";

            //        cTipoRecord = cLineaDivisa[0];
            //        nStart = 0;
            //        switch (cTipoRecord)
            //        {
            //            case "10":
            //                {
            //                    cTabella = "TBL_tr_10";
            //                    nStart = 1;
            //                    break;
            //                }
            //            case "20":
            //                {
            //                    cTabella = "TBL_tr_20";
            //                    break;
            //                }
            //            case "25":
            //                {
            //                    cTabella = "TBL_tr_25";
            //                    break;
            //                }
            //            case "30":
            //                {
            //                    cTabella = "TBL_tr_30";
            //                    break;
            //                }
            //            case "40":
            //                {
            //                    cTabella = "TBL_tr_40";
            //                    break;
            //                }
            //            case "42":
            //                {
            //                    cTabella = "TBL_tr_42";
            //                    break;
            //                }
            //            case "44":
            //                {
            //                    cTabella = "TBL_tr_44";
            //                    break;
            //                }
            //            case "46":
            //                {
            //                    cTabella = "TBL_tr_46";
            //                    break;
            //                }
            //            case "50":
            //                {
            //                    cTabella = "TBL_tr_50";
            //                    break;
            //                }
            //            case "60":
            //                {
            //                    cTabella = "TBL_tr_60";
            //                    break;
            //                }
            //            case "99":
            //                {
            //                    cTabella = "TBL_tr_99";
            //                    break;
            //                }
            //        }

            //        if (cTabella != "")
            //        {
            //            nColumn = DSImported.Tables[cTabella].Columns.Count;
            //            dr = DSImported.Tables[cTabella].NewRow();
            //            for (int i = 1; i < nColumn; i++)
            //            {
            //                if (cLineaDivisa.Length > i)
            //                    dr[i - nStart] = cLineaDivisa[i];
            //            }
            //            DSImported.Tables[cTabella].Rows.Add(dr);
            //        }
            //    }
            //}
            //sr.Close();

            //
            // Aggiusta i campi in attesa di chiarimenti
            //
            //foreach (DS.DS_import.TBL_tr_10Row dr10 in DSImported.TBL_tr_10.Rows)
            //{
            //    if (dr10.T10_rec_nominativo == "")
            //    {
            //        dr10.T10_rec_nominativo = dr10.T10_nominativo;
            //        dr10.T10_rec_indirizzo = dr10.T10_res_indirizzo;
            //        dr10.T10_rec_cap = dr10.T10_res_cap;
            //        dr10.T10_rec_comune = dr10.T10_res_comune;
            //        dr10.T10_rec_provincia = dr10.T10_res_provincia;
            //    }
            //}

            ////
            //// Aggiusta i campi delle tabelle
            //// 
            //foreach (DS.DS_import.TBL_tr_20Row dr20 in DSImported.TBL_tr_20.Rows)
            //{
            //    dr20.campo05 = dr20.campo05.PadLeft(7, '0');
            //}

            //foreach (DataTable dt in DSImported.Tables)
            //{
            //    SalvaTabelle(dt);
            //}
            return lista;
        }

        private void SalvaTabelle(DataTable dt)
        {
            string cConnACCStr = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", PrintDataSet_DataFile(77), ";");

            OleDbCommand oCmd = new OleDbCommand();
            OleDbDataAdapter oDa = new OleDbDataAdapter(oCmd);
            OleDbCommandBuilder oComBuil = new OleDbCommandBuilder(oDa);

            oCmd.Connection = new OleDbConnection(cConnACCStr);

            // Salvataggio della tabella TBL_main
            oCmd.CommandText = "SELECT * FROM " + dt.TableName;
            oDa = new OleDbDataAdapter(oCmd);
            oComBuil = new OleDbCommandBuilder(oDa);
            oDa.Update(dt);
        }

        #endregion

        // ***************************
        //
        // Funzioni di servizio
        //
        // ***************************
        private void ImpostaMessaggi(String[] aValue)
        {
            for (int i = 0; i <= 5; i++)
            {
                if (aValue[i] != "")
                    MessaggiAvanzamento[i] = aValue[i];
            }
        }

        private XmlNode SetRecordIDX(XmlDocument xmlIndice, String cField, String cValue)
        {
            XmlNode xiTmp;
            xiTmp = xmlIndice.CreateNode(XmlNodeType.Element, cField, "");
            xiTmp.InnerText = cValue;
            return xiTmp;
        }

        private string GetValueFromXML(XmlNode xmlItem, string xmlPath, string cDefault = "", string cMsgError = "")
        {
            String cValue;

            cValue = "";
            try
            {
                cValue = cDefault;
                if (xmlItem.SelectSingleNode(xmlPath) != null)
                    cValue = xmlItem.SelectSingleNode(xmlPath).InnerText;

                if (cValue == "" && cMsgError != "")
                    AddErroreLieve(cMsgError);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("Errore nella procedura di recupero dei valori del file XML.", "\n", "percorso da cui si tenta il recupero:", "\n", xmlPath), ex);
            }
            return cValue;

        }

        private void AddErroreLieve(string cMsg)
        {
            if (_ErroriLievi == "")
                _ErroriLievi = cMsg;
            else
                _ErroriLievi = String.Concat(_ErroriLievi, "; ", cMsg);

        }

        private string PrintDataSet_DataFile(int _FileIdx)
        {
            return string.Concat(_SessioneFld, _SezioneDati.SelectSingleNode(String.Concat("filename[@codice=", "", _FileIdx, "", "]")).InnerText);
        }

        //
        //
        //
        //
        private string QuickDataSet_IndexFile
        {
            get
            {
                return string.Concat(_SessioneFld, _SezioneDati.SelectSingleNode("filename[@tipo=\"0\"]").InnerText);
            }
        }

        private Appoggio.G20Entity CaricaRecord(string cFileName, string nCodice)
        {
            List<Appoggio.G20Entity> appo = ImportaFileInSessione(0, cFileName);
            Appoggio.G20Entity ele = null;

            foreach (Appoggio.G20Entity e in appo)
                if (e.NumeroDocumento == nCodice)
                    ele = e;
            return ele;
        }

        private void CaricaTabella(DataTable dt, decimal nCodice)
        {
            string cConnACCStr = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", PrintDataSet_DataFile(77), ";");

            OleDbCommand oCmd = new OleDbCommand();
            OleDbDataAdapter oDa = new OleDbDataAdapter(oCmd);
            OleDbCommandBuilder oComBuil = new OleDbCommandBuilder(oDa);

            oCmd.Connection = new OleDbConnection(cConnACCStr);

            // Caricamento della tabella TBL_main
            if (dt.TableName.ToLower() == "TBL_tr_10".ToLower())
                oCmd.CommandText = "SELECT * FROM " + dt.TableName + " WHERE T10_ID_documento = '" + nCodice.ToString() + "'";
            else if (dt.TableName.ToLower() == "TBL_tr_99".ToLower())
                oCmd.CommandText = "SELECT * FROM " + dt.TableName;
            else
                oCmd.CommandText = "SELECT * FROM " + dt.TableName + " WHERE T" + dt.TableName.Substring(7, 2) + "_cod_t10 = '" + nCodice.ToString() + "'"; ;
            oDa = new OleDbDataAdapter(oCmd);
            oComBuil = new OleDbCommandBuilder(oDa);
            oDa.Fill(dt);
        }

        //
        // Preparazione del dataset per la stampa della fattura.
        //
        private void PreparaRecordPerStampaFattura(int nCodice, Appoggio.G20Entity ele, String cFeedString)
        {
            DS.DS_prn_fattura.TBL_fattureRow dr_fat;
            DS.DS_prn_fattura.TBL_mainRow dr_mai;

            //
            // Carica i dati del record principale
            //
            dr_mai = (DS.DS_prn_fattura.TBL_mainRow)_PrintFatturaDataset.TBL_main.NewRow();
            dr_mai.MAI_linkQD = nCodice;
            dr_mai.DEF_raccolta = 9;
            dr_mai.DEF_alimimb = "3";
            dr_mai.MAI_cod_ute = "1";
            _PrintFatturaDataset.TBL_main.AddTBL_mainRow(dr_mai);

            dr_fat = (DS.DS_prn_fattura.TBL_fattureRow)_PrintFatturaDataset.TBL_fatture.NewRow();
            dr_fat.FAT_codice = _PrintFatturaDataset.TBL_fatture.Rows.Count + 1;
            dr_fat.FAT_cod_mai = dr_mai.MAI_codice;
            dr_fat.FAT_num_fattura = ele.NumeroDocumento;
            dr_fat.FAT_nomefile = _SessioneFld + "Dati\\" + ele.NumeroDocumento + ".pdf";
            _PrintFatturaDataset.TBL_fatture.Rows.Add(dr_fat);
        }

        //
        // Preparazione del dataset per la stampa del Bollettino 896.
        //
        private void PreparaRecordPerStampaBoll896(int nCodice, Appoggio.G20Entity ele, String cFeedString)
        {
            DS.DS_prn_bollettino.TBL_fattureRow dr_fat;
            DS.DS_prn_bollettino.TBL_rateRow dr_rat;
            DS.DS_prn_bollettino.TBL_fat_x_ratRow dr_fxr;

            dr_fat = (DS.DS_prn_bollettino.TBL_fattureRow)_PrintBollettinoDataset.TBL_fatture.NewRow();
            dr_fat.FAT_codice = _PrintBollettinoDataset.TBL_fatture.Rows.Count + 1;
            dr_fat.FAT_prot_reg_iva = "";
            dr_fat.FAT_num_fattura = ele.NumeroDocumento;
            dr_fat.FAT_data_fattura = ele.DataDocumento.ToString("dd/MM/yyyy");
            dr_fat.FAT_nominativo = ele.Denominazione;
            dr_fat.FAT_cod_utente = ele.CodiceUtente;
            dr_fat.FAT_codice_fiscale = ele.CF;
            dr_fat.FAT_partita_iva = ele.PI;
            dr_fat.FAT_ind_rec_1 = ele.Indirizzo;
            dr_fat.FAT_ind_rec_2 = ele.CAP;
            dr_fat.FAT_ind_rec_3 = ele.Provincia;
            dr_fat.FAT_ind_rec_4 = ele.Comune;
            if ((dr_fat.FAT_codice_fiscale == dr_fat.FAT_partita_iva) && (dr_fat.FAT_codice_fiscale.Length == 11))
                dr_fat.FAT_codice_fiscale = "";
            _PrintBollettinoDataset.TBL_fatture.Rows.Add(dr_fat);

            //foreach (DS.DS_import.TBL_tr_30Row drtmp in dsSrc.TBL_tr_30.Rows)
            //{
            //    if (drtmp.campo03 != "0")
            //    {
            dr_rat = (DS.DS_prn_bollettino.TBL_rateRow)_PrintBollettinoDataset.TBL_rate.NewRow();
            dr_rat.RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1;
            dr_rat.RAT_num_rata = "1";
            dr_rat.RAT_scadenza = ele.DatiDocumento.DataScadenza.ToString("dd/MM/yyyy");
            dr_rat.RAT_importo = ele.DatiDocumento.ImportoRata;
            //nTmp = decimal.Parse(drtmp.campo06);
            dr_rat.RAT_ocr = ele.DatiDocumento.OCR; // "DA DEFINIRE"; //drtmp.campo06 + (nTmp % 93).ToString("00");
            _PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat);

            //
            // Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
            //
            dr_fxr = (DS.DS_prn_bollettino.TBL_fat_x_ratRow)_PrintBollettinoDataset.TBL_fat_x_rat.NewRow();
            dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1;
            dr_fxr.DEF_alimimb = cFeedString;
            dr_fxr.DEF_raccolta = 3;
            dr_fxr.FXR_linkQD = nCodice;
            dr_fxr.FXR_cod_fat = dr_fat.FAT_codice;
            dr_fxr.FXR_cod_rat = dr_rat.RAT_codice;
            _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr);
            //    }
            //}
        }


        private string GetValueDatiFissi(string cTable, int nField)
        {
            XmlDocument xmlDF;
            XmlNode xmlTmp;
            string cRtn;

            try
            {
                xmlDF = new XmlDocument();
                xmlDF.Load(_SessioneDatiFissiFileName);

                xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", nField.ToString(), "]"));

                cRtn = xmlTmp.InnerText;
            }
            catch (Exception ex)
            {
                cRtn = "";
            }
            return cRtn;

        }

        private string GetFeedString(DS.DS_quick_db.TBL_mainRow dr_mai_qd)
        {
            Boolean lAllModuli = true;
            String cRtn;

            cRtn = "";
            if (_FeedString != "")
            {
                for (int i = 1; i <= _FeedString.Split(';').Length - 1; i = i + 2)
                    if (Boolean.Parse(dr_mai_qd[_FeedString.Split(';')[i]].ToString()))
                        cRtn = _FeedString.Split(';')[i + 1];
                    else
                        lAllModuli = false;

                if (lAllModuli)
                    cRtn = _FeedString.Split(';')[0];
            }
            return cRtn;
        }

        private string RiduciLunghezzaStringa(string cValue, int nLunghezza)
        {
            if (cValue.Length > nLunghezza)
                cValue = cValue.Substring(0, nLunghezza);
            return cValue;
        }
    }
}

