﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PLG_ANE_dir228.Appoggio
{
    public class G80Entity
    {
        public string TipoRecord { get; set; }
        public decimal Progressivo { get; set; }
        public string ProgressivoRata { get; set; }
        public string SImportoRata
        {
            set
            {
                ImportoRata = decimal.Parse(value) / 100;
            }
        }
        public string CIR { get; set; }
        public DateTime DataScadenza { get; set; }
        public string SDataScadenza
        {
            set
            {
                DataScadenza = new DateTime(int.Parse(value.Substring(4, 4)), int.Parse(value.Substring(2, 2)), int.Parse(value.Substring(0, 2)));
            }
        }
        public string AnnoRata { get; set; }
        public string ImportoInCifre { get; set; }
        public string AuxDigit { get; set; }
        public string ApplicationCode { get; set; }
        public string IUV { get; set; }
        public string CodiceAvviso { get; set; }
        public string NumeroDocumento { get; set; }

        public string OCR { get; set; }
        public Decimal ImportoRata { get; set; }
    }
}
