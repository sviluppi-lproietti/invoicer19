﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PLG_ANE_dir228.Appoggio
{
    public class G20Entity
    {

        public G20Entity()
        {
            DenominazioneSpedizione = "";
            IndirizzoSpedizione = "";
            LocalitaSpedizione = "";
            CAPSpedizione = "";
            ComuneSpedizione = "";
            ProvinciaSpedizione = "";
        }
        public string NumeroFile { get; set; }
        public string TipoRecord { get; set; }
        public decimal Progressivo { get; set; }

        public DateTime DataDocumento { get; set; }

        public String SDataDocumento
        {
            set
            {
                DataDocumento = new DateTime(int.Parse(value.Substring(4, 4)), int.Parse(value.Substring(2, 2)), int.Parse(value.Substring(0, 2)));
            }
        }
        public string NumeroDocumento { get; set; }
        public string DescrizioneDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public string TotaleDocumento { get; set; }
        public string CodiceUtente { get; set; }
        public string CodiceAnagrafica { get; set; }
        public string Denominazione { get; set; }
        public string CF { get; set; }
        public string PI { get; set; }
        public string Indirizzo { get; set; }
        public string Localita { get; set; }
        public string CAP { get; set; }
        public string Comune { get; set; }
        public string Provincia { get; set; }
        public string CodiceAnagraficaSpedizione { get; set; }
        public string DenominazioneSpedizione
        {
            get
            {
                if (_DenominazioneSpedizione == "")
                    return Denominazione;
                else
                    return _DenominazioneSpedizione;
            }
            set
            {
                _DenominazioneSpedizione = value.Trim();
            }
        }

        public string IndirizzoSpedizione
        {
            get
            {
                if (_IndirizzoSpedizione == "")
                    return Indirizzo;
                else
                    return _IndirizzoSpedizione;
            }
            set
            {
                _IndirizzoSpedizione = value.Trim();
            }
        }

        public string LocalitaSpedizione
        {
            get
            {
                if (_LocalitaSpedizione == "")
                    return Localita;
                else
                    return _LocalitaSpedizione;
            }
            set
            {
                _LocalitaSpedizione = value.Trim();
            }
        }
        public string CAPSpedizione
        {
            get
            {
                if (_CAPSpedizione == "")
                    return CAP;
                else
                    return _CAPSpedizione;
            }
            set
            {
                _CAPSpedizione = value.Trim();
            }
        }

        public string ComuneSpedizione
        {
            get
            {
                if (_ComuneSpedizione == "")
                    return Comune;
                else
                    return _ComuneSpedizione;
            }
            set
            {
                _ComuneSpedizione = value.Trim();
            }
        }
        public string ProvinciaSpedizione
        {
            get
            {
                if (_ProvinciaSpedizione == "")
                    return Provincia;
                else
                    return _ProvinciaSpedizione;
            }
            set
            {
                _ProvinciaSpedizione = value.Trim();
            }
        }
        public string PeriodoRiferimento { get; set; }
        public string InizioPeriodoRiferimento { get; set; }
        public string FinePeriodoRiferimento { get; set; }
        public string FlagAddebitoConto { get; set; }
        public string CCAddebito { get; set; }
        public string ABIAddebito { get; set; }
        public string CABAddebito { get; set; }
        public string CINAddebito { get; set; }
        public string BancaAddebito { get; set; }
        public string CodiceAvviamentoBancario { get; set; }
        public string CodiceDebitoreEsterno { get; set; }
        public string NumeroDocumentoPrecedente { get; set; }
        public string DataDocumentoPrecedente { get; set; }
        public string DataNotificaDocumentoPrecedente { get; set; }
        public string CodicePartita { get; set; }
        public string TotaleDebitoPreEmissione { get; set; }
        public string TotaleSgravato { get; set; }
        public string TotalePagato { get; set; }
        public string TotaleResiduoPreEmissione { get; set; }
        public string Interno { get; set; }
        public string EsponenteInterno { get; set; }
        public string InternoSpedizione { get; set; }
        public string EsponenteInternoSpedizione { get; set; }
        public string IndirizzoEmail { get; set; }
        public string IndirizzoEmailCertificato { get; set; }
        public string Riemesso { get; set; }
        public string Cognome { get; set; }
        public string Nome { get; set; }
        public string RagioneSociale { get; set; }
        public string TipoInvioDocumento { get; set; }
        public string TipologiaUtenza { get; set; }
        public string ModelloComunicazione { get; set; }
        public string CodiceAttivazioneMail { get; set; }
        public string TotaleDovutoComplessivo { get; set; }
        public string ImportoOriginale { get; set; }
        public string ImportoFinale { get; set; }
        public string Conguaglio { get; set; }
        public string DataUltimaPagamentiConsiderati { get; set; }
        public string DenominazioneContinuaSuRiga { get; set; }
        public string DenominazioneSpedizioneContinuaSuStringa { get; set; }
        public string TotaleImportoDichiarato { get; set; }
        public string TotaleImportoAccertato { get; set; }
        public string TotalePagatoFuoriProcesso { get; set; }
        public string TotaleSconto { get; set; }
        public string TotaleImportoNonArrotondato { get; set; }
        public string CodiceACS { get; set; }
        public string IdMandatoSDD { get; set; }
        public string FlagDaNonEmettere { get; set; }
        public string CronologicoNumero { get; set; }
        public string CronologicoData { get; set; }

        private string _DenominazioneSpedizione;
        private string _IndirizzoSpedizione;
        private string _LocalitaSpedizione;
        private string _CAPSpedizione;
        private string _ComuneSpedizione;
        private string _ProvinciaSpedizione;
        private DateTime _DataDocumento;
        public G80Entity DatiDocumento;
    }
}
