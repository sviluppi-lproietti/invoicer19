Imports System
Imports System.Windows.Forms
Imports Oggetticomuni
Imports Oggetticomuni.CLS_log_srv
Imports Oggetticomuni.TC2_enumeratori
Imports Oggetticomuni.TC2_enumeratori.eLogLevel
Imports Oggetticomuni.TC2_utilita
Imports Oggetticomuni.TC2_utilitaXML

<Serializable()> _
Public Class PLG_multiservizi
    Inherits MarshalByRefObject
    Implements PlugINs.PlugIN_2_0_0

    Private _FileDati As ArrayList
    Private _CodiceModulo As Integer
    Private _CodiceProgressivo As Integer
    Private _DeliveryDataset As DS_DD_multiservizi
    Private _DicRecordCount As Dictionary(Of Integer, Integer)
    Private _FeedString As String
    Private _SessioneNuova As Boolean
    Private _QuickDataset As DS_QD_multiservizi
    Private _RecordCount As Integer
    Private _MessaggiAvanzamento(5) As String
    Private _ErroriLievi As String
    Private _ErroriGravi As String
    Private _LogDS As DataSet
    Private _Loggingobj As CLS_log_srv
    Private _OLEDB_prefixStrs As Dictionary(Of String, String)
    Private _TipoAmb As TipoAmbiente
    Private _ErrorePlugin As String
    'Private _Folders As Dictionary(Of String, String)
    'Private _Files As Dictionary(Of String, String)
    Private _SessioneFLD As String
    Private _TempFLD As String

    Const TipoFile = 101

    WriteOnly Property SessioneFLD As String Implements PlugINs.PlugIN_2_0_0.SessioneFld
        Set(ByVal value As String)
            _SessioneFLD = value
        End Set
    End Property

    WriteOnly Property TempFLD As String Implements PlugINs.PlugIN_2_0_0.TempFLD
        Set(ByVal value As String)
            _TempFLD = value
        End Set
    End Property

#Region "Elenco delle property public"

    ReadOnly Property CapacitaInvioMail() As Boolean Implements PlugINs.PlugIN_2_0_0.CapacitaInvioMail

        Get
            Return False
        End Get

    End Property

    ReadOnly Property DataSendByMail() As Xml.XmlNode Implements PlugINs.PlugIN_2_0_0.DataSendByMail

        Get
            Return Nothing
        End Get

    End Property

    ReadOnly Property NuoviToponimi() As Boolean Implements PlugINs.PlugIN_2_0_0.NuoviToponimi

        Get
            Return False
        End Get

    End Property

    ReadOnly Property StampaInProprio() As Boolean Implements PlugINs.PlugIN_2_0_0.StampaInProprio

        Get
            Return True
        End Get

    End Property

    ReadOnly Property CurrentRecordNumber() As Integer Implements PlugINs.PlugIN_2_0_0.CurrentRecordNumber

        Get
            Return _RecordCount
        End Get

    End Property

    ReadOnly Property ElementiMenuDedicati() As ArrayList Implements PlugINs.PlugIN_2_0_0.ElementiMenuDedicati

        Get
            Return New ArrayList
        End Get

    End Property

    Public ReadOnly Property MessaggioAvviso() As String Implements PlugINs.PlugIN_2_0_0.MessaggioAvviso

        Get
            Dim cValue As String

            Select Case _CodiceModulo
                Case Else
                    cValue = ""
            End Select
            Return cValue
        End Get

    End Property

    ReadOnly Property LinkField_QuickDS_FullDS() As String Implements PlugINs.PlugIN_2_0_0.LinkField_QuickDS_FullDS

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_send_email
                    Return "MAI_linkQD"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataset() As DataSet Implements PlugINs.PlugIN_2_0_0.FullDataset

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_data_delivery
                    Return _DeliveryDataset
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataSet_MainTableName() As String Implements PlugINs.PlugIN_2_0_0.FullDataSet_MainTableName

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_data_delivery
                    Return "TBL_delivery_data"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataset_MainTableKey() As String Implements PlugINs.PlugIN_2_0_0.FullDataset_MainTableKey

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_send_email
                    Return "MAI_codice"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    Public ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList Implements PlugINs.PlugIN_2_0_0.ListaControlliFormNuovaSessione

        Get
            Dim _ListaControlliForm As ArrayList

            _ListaControlliForm = New ArrayList
            _ListaControlliForm.Add(New String() {"LBL_input_1", "File excel (elenco documenti) ......:", "3;16", "205;13"})
            _ListaControlliForm.Add(New String() {"TXB_input_1", "&1", "210;12", "560;20", String.Concat("*", TipoFile)})
            _ListaControlliForm.Add(New String() {"BTN_fil_input_1", "", "775;10", "27;23"})

            _ListaControlliForm.Add(New String() {"LBL_output_2", "Nome sessione di consegna ........:", "3;42", "205;13"})
            _ListaControlliForm.Add(New String() {"TXB_output_2", "", "210;38", "560;20", "*1"})

            _ListaControlliForm.Add(New String() {"LBL_output_3", "Descrizione archivio ......:", "3;68", "205;13"})
            _ListaControlliForm.Add(New String() {"TXB_output_3", "", "210;64", "M;560;122", "2"})

            _ListaControlliForm.Add(New String() {"LBL_output_4", "Data presa in carico documenti ...:", "3;194", "205;13"})
            _ListaControlliForm.Add(New String() {"TXB_output_4", "", "210;190", "560;20", "*52"})

            _ListaControlliForm.Add(New String() {"NODISP", "", "210;216", "560;20", "51"})

            Return _ListaControlliForm
        End Get

    End Property

    WriteOnly Property SessioneNuova() As Boolean Implements PlugINs.PlugIN_2_0_0.SessioneNuova

        Set(ByVal value As Boolean)
            _SessioneNuova = value
        End Set

    End Property

    Public ReadOnly Property NomePlugIn() As String Implements PlugINs.PlugIN_2_0_0.NomePlugIn

        Get
            Return "Plug-in per la produzione dei dati per la Consegna Certa per il cliente Multiservizi."
        End Get

    End Property

    Public ReadOnly Property QuickDataset() As DataSet Implements PlugINs.PlugIN_2_0_0.QuickDataset

        Get
            Return _QuickDataset
        End Get

    End Property

    Public ReadOnly Property QuickDataset_MainTableKey() As String Implements PlugINs.PlugIN_2_0_0.QuickDataset_MainTableKey

        Get
            Return "MAI_codice"
        End Get

    End Property

    Public ReadOnly Property QuickDataset_MainTableName() As String Implements PlugINs.PlugIN_2_0_0.QuickDataset_MainTableName

        Get
            Return "TBL_main"
        End Get

    End Property

    Public ReadOnly Property DatiAzioniPostStampa() As ArrayList Implements PlugINs.PlugIN_2_0_0.DatiAzioniPostStampa

        Get
            Return New ArrayList
        End Get

    End Property

    Public ReadOnly Property DatiAzioniPreInvio() As ArrayList Implements PlugINs.PlugIN_2_0_0.DatiAzioniPreInvio

        Get
            Return New ArrayList
        End Get

    End Property

    Private ReadOnly Property PrintDataSet_DataFile(ByVal _FileIdx As Integer)

        Get
            Dim cTmp As String

            cTmp = ""
            For Each aTemp() As String In _FileDati
                If aTemp(0) = TipoFile And aTemp(1) = _FileIdx Then
                    cTmp = String.Concat(_SessioneFLD, aTemp(2))
                End If
            Next
            Return cTmp
        End Get

    End Property

    ReadOnly Property LogDSStampa() As DataSet Implements PlugINs.PlugIN_2_0_0.LogDSStampa

        Get
            Return _LogDS
        End Get

    End Property

    Property CodiceModulo() As Integer Implements PlugINs.PlugIN_2_0_0.CodiceModulo

        Get
            Return _CodiceModulo
        End Get
        Set(ByVal value As Integer)
            _CodiceModulo = value
        End Set

    End Property

    WriteOnly Property FeedString() As String Implements PlugINs.PlugIN_2_0_0.FeedString

        Set(ByVal value As String)
            _FeedString = value
        End Set

    End Property

    Property MessaggiAvanzamento() As String() Implements PlugINs.PlugIN_2_0_0.MessaggiAvanzamento

        Get
            Return _MessaggiAvanzamento
        End Get
        Set(ByVal value As String())
            _MessaggiAvanzamento = value
        End Set

    End Property

    WriteOnly Property ListaGiri() As ArrayList Implements PlugINs.PlugIN_2_0_0.ListaGiri

        Set(ByVal value As ArrayList)

        End Set

    End Property

    WriteOnly Property DBGiriConnectionString() As String Implements PlugINs.PlugIN_2_0_0.DBGiriConnectionString

        Set(ByVal value As String)

        End Set

    End Property

    WriteOnly Property FiltroSelezione() As String Implements PlugINs.PlugIN_2_0_0.FiltroSelezione

        Set(ByVal value As String)

        End Set

    End Property

#End Region

#Region "Metodi Pubblici - FUNCTION"

    Public Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PlugINs.PlugIN_2_0_0.CheckCondPSA

        Return False

    End Function

    Public Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PlugINs.PlugIN_2_0_0.CheckCondAPI

        Return False

    End Function

    Public Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean Implements PlugINs.PlugIN_2_0_0.CheckDatiSessione
        Dim _lObb As Boolean = False
        Dim _l101 As Boolean = False
        Dim _l102 As Boolean = False
        Dim lRtn As Boolean = True

        For Each aValue As String() In aDati
            Select Case aValue(0)
                Case Is = 102
                    _l102 = True
            End Select
        Next
        If _SessioneNuova Then
            If Not _l102 Then
                MessageBox.Show("Attenzione! Non � stato indicato un file valido contenente i dati di recapito.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error)
                lRtn = False
            Else
                lRtn = True
            End If
        Else
            lRtn = True
        End If
        Return lRtn

    End Function

    Public Function DeliveryDataTranslate(ByVal cField As String) As String Implements PlugINs.PlugIN_2_0_0.DeliveryDataTranslate
        Dim aFieldTra As String()
        Dim aField As String()
        Dim cRtn As String

        aField = New String() {"DEL_nominativo"}
        aFieldTra = New String() {"DED_num_contratto"}

        cRtn = ""
        If Array.IndexOf(aField, cField) > -1 Then
            cRtn = aFieldTra(Array.IndexOf(aField, cField))
        End If
        Return cRtn

    End Function

    Public Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String() Implements PlugINs.PlugIN_2_0_0.GetSelectedRow

        Return New String() {"", ""}

    End Function

#End Region

#Region "Metodi Pubblici - SUB"

    Public Sub CreaDataBaseSessione(ByVal _DatiCreazioneSessione As ArrayList) Implements PlugINs.PlugIN_2_0_0.CreaDataBaseSessione
        Dim xdGuida As Xml.XmlDocument
        Dim cFileNameExcel As String
        Dim sfXML As FC_XMLSortFile
        Dim xiGuida As Xml.XmlNode
        Dim xiAppo As Xml.XmlNode
        Dim cSrcFile As String
        Dim cDstFile As String
        Dim nRecord As Integer
        Dim dsExcel As DataSet
        Dim aValue As String()
        Dim dr_xls As DataRow
        Dim nFile As Integer

        ' _ResultAL = New ArrayList
        Try
            _Loggingobj.ScriviMessaggioLog(LL_informational, 0, "PLG_multi_acq", "CreaDataBaseSessione", "INIZIO - Creazione della sessione di consegna")
            cFileNameExcel = ""
            _FileDati = New ArrayList
            ' ************************************************************************** '
            ' Carica i dati necessari del file di input.                                 '

            '' ************************************************************************** '
            '' Copia i file dei dati dalla cartella dove sono attualmente nella cartella  '
            '' dati della sessione.                                                       '
            '' ************************************************************************** '
            For Each aValue In _DatiCreazioneSessione
                If aValue(0) = 52 Then
                    '        _FileDati.Add(New String() {aValue(0), -1, aValue(1)})
                ElseIf aValue(0) = TipoFile Then
                    '        cSrcFile = String.Concat(cSrcFLD, aValue(1))
                    '        cDstFile = String.Concat(cDstFLD, "dati\", aValue(1))
                    '        _Loggingobj.ScriviMessaggioLog(LL_debugging, 0, "PLG_multi_acq", "CreaDataBaseSessione", String.Concat("Copia file da: ", cSrcFile))
                    '        _Loggingobj.ScriviMessaggioLog(LL_debugging, 0, "PLG_multi_acq", "CreaDataBaseSessione", String.Concat("            a: ", cDstFile))
                    'cFileNameExcel = cDstFile
                    cFileNameExcel = ConcatenaFolderFileValue(_SessioneFLD, aValue(1))
                    '        nFile = 1
                    '        System.IO.File.Copy(cSrcFile, cDstFile)
                    '        _FileDati.Add(New String() {TipoFile, nFile, cDstFile})
                End If
            Next

            ' ************************************************************************** '
            ' Caricamento del file dati e successiva generazione del Quickdataset e ag-  '
            ' giornamento dei dati della sessione.                                       '
            ' ************************************************************************** '
            If System.IO.File.Exists(cFileNameExcel) And cFileNameExcel > "" Then
                _Loggingobj.ScriviMessaggioLog(LL_debugging, 0, "PLG_multi_acq", "CreaDataBaseSessione", "Creazione del quickdataset.")
                dsExcel = ReadExcelIntoDataset(_OLEDB_prefixStrs, cFileNameExcel)
                xdGuida = New Xml.XmlDocument
                xdGuida.AppendChild(xdGuida.CreateNode(Xml.XmlNodeType.Element, "DOCUMENTO", ""))
                _Loggingobj.ScriviMessaggioLog(LL_debugging, 0, "PLG_multi_acq", "CreaDataBaseSessione", String.Concat("Record nella tabella dei dati: ", dsExcel.Tables(0).Rows.Count))
                For Each dr_xls In dsExcel.Tables(0).Rows
                    If Not IsDBNull(dr_xls(0)) Then
                        xiGuida = xdGuida.CreateNode(Xml.XmlNodeType.Element, "DOCUMENTO_ROW", "")
                        CreateNodeXML(xiGuida, "DEF_errcode", 0)
                        CreateNodeXML(xiGuida, "DEF_errdesc", "")
                        CreateNodeXML(xiGuida, "MAI_num_doc_uff", dr_xls(0))
                        CreateNodeXML(xiGuida, "MAI_conto_contr", dr_xls(1))
                        CreateNodeXML(xiGuida, "MAI_filedati", 1)
                        CreateNodeXML(xiGuida, "SORTFIELD", String.Concat(dr_xls(1).ToString.PadLeft(10, "0"), dr_xls(0).ToString.PadLeft(12, "0")))

                        xdGuida.SelectSingleNode("DOCUMENTO").AppendChild(xiGuida)
                    Else
                        _Loggingobj.ScriviMessaggioLog(LL_debugging, 0, "PLG_multi_acq", "CreaDataBaseSessione", "Record vuoto, saltato")
                    End If
                Next

                ' ************************************************************************** '
                ' Ordinamento dei record del file xml                                        '
                ' ************************************************************************** '
                '_Loggingobj.ScriviMessaggioLog(LogLevel.LL_debugging, 0, "PLG_multi_acq", "CreaDataBaseSessione", "Ordinamentod dei record del Quickdataset ")
                'sfXML = New FC_XMLSortFile(xdGuida, "DOCUMENTO")
                'sfXML.OrdinaAscendente()
                'sfXML = Nothing

                _Loggingobj.ScriviMessaggioLog(LL_debugging, 0, "PLG_multi_acq", "CreaDataBaseSessione", "Pulizia del file xml del quickdataset")
                ' ************************************************************************** '
                ' Rimuove il campo utilizzato per l'ordinamento e genera il file che servir� ' 
                ' per l'invio delle fatture via mail.                                        '
                ' ************************************************************************** '
                nRecord = 1
                For Each xiGuida In xdGuida.SelectNodes("DOCUMENTO/DOCUMENTO_ROW/SORTFIELD")
                    xiAppo = xiGuida.ParentNode
                    xiAppo.RemoveChild(xiGuida)
                    CreateNodeXML(xiAppo, "MAI_codice", nRecord)
                    nRecord += 1
                Next

                _Loggingobj.ScriviMessaggioLog(LL_debugging, 0, "PLG_multi_acq", "CreaDataBaseSessione", "Salvataggio dei dati del quickdataset")
                ' ************************************************************************** '
                ' Salva il file indice nella sessione.                                       '
                ' ************************************************************************** '
                xdGuida.Save(String.Concat(_SessioneFLD, "dati\indice.xml"))
                _FileDati.Add(New String() {"100", -1, "dati\indice.xml"})
            Else
                xdGuida = Nothing
                Throw New Exception("File dati non presente.")
            End If
        Catch ex As Exception
            Throw ex
        End Try
        _Loggingobj.ScriviMessaggioLog(LL_informational, 0, "PLG_multi_acq", "CreaDataBaseSessione", "FINE - Creazione della sessione di consegna")

    End Sub

    Public Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PlugINs.PlugIN_2_0_0.ExecActionPSA

    End Sub

    Public Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PlugINs.PlugIN_2_0_0.ExecActionAPI

    End Sub

    Public Sub GoToRecordNumber(ByVal nRecord As Integer) Implements PlugINs.PlugIN_2_0_0.GoToRecordNumber

        _RecordCount = nRecord

    End Sub

    Public Sub ImpostaDGVDati(ByVal DGV As DataGridView) Implements PlugINs.PlugIN_2_0_0.ImpostaDGVDati

    End Sub

    ' ************************************************************************** '
    ' Questa funzione si occupa di caricare il data set che guida la stampa o    '
    ' l'invio:                                                                   '    
    ' 1 - Caricamento dei record per la stampa: carica tutti i record            '
    ' 2 - Caricamento dei record per l'invio: carica solo i record con           '
    '     DEF_sendbymail = TRUE                                                  '
    ' ************************************************************************** '
    Public Sub LoadQuickDataset(ByVal cQuickDataSetIndex_FileName As String) Implements PlugINs.PlugIN_2_0_0.LoadQuickDataset
        Dim dr_mai_qd As DS_QD_multiservizi.TBL_mainRow
        Dim xdDati_qd As Xml.XmlDocument

        Try
            _QuickDataset = New DS_QD_multiservizi
            xdDati_qd = New Xml.XmlDocument
            xdDati_qd.Load(cQuickDataSetIndex_FileName)
            For Each xnItem As Xml.XmlNode In xdDati_qd.SelectNodes(Costanti.itm_qd_Fattura)
                dr_mai_qd = _QuickDataset.TBL_main.NewRow
                Try
                    dr_mai_qd.MAI_codice = GetValueFromXML(xnItem, "MAI_codice")
                    dr_mai_qd.DEF_toprint = True
                    dr_mai_qd.DEF_errcode = GetValueFromXML(xnItem, "DEF_errcode")
                    dr_mai_qd.DEF_errdesc = GetValueFromXML(xnItem, "DEF_errdesc")
                    dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0) Or (dr_mai_qd.DEF_errcode = -2)
                    dr_mai_qd.MAI_num_doc_uff = GetValueFromXML(xnItem, "MAI_num_doc_uff")
                    dr_mai_qd.MAI_conto_contr = GetValueFromXML(xnItem, "MAI_conto_contr")
                    dr_mai_qd.MAI_filedati = GetValueFromXML(xnItem, "MAI_filedati")
                Catch ex As Exception
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = ex.Message
                End Try
                _QuickDataset.TBL_main.Rows.Add(dr_mai_qd)
            Next
            _Files("SerializedQuickDatasetFIL") = SerializeDataset(Me.QuickDataset, Me.Folders("TempFLD"))
        Catch ex As Exception
            Throw New Exception("Errore in fase di caricamento del dataset.", ex)
        End Try

    End Sub

    Public Sub LoadFullDataset(ByVal nRecordToLoad As Integer) Implements PlugINs.PlugIN_2_0_0.LoadFullDataset
        Dim dr_mai_qd As DS_QD_multiservizi.TBL_mainRow
        Dim nRecordToPrint As Integer
        Dim dsExcel As DataSet
        Dim dr_doc As DataRow
        Dim cFilter As String

        ' ************************************************************************** '
        ' Creazione del Dataset e azzeramento del contatore dei record da stampare.  '
        ' ************************************************************************** '
        nRecordToPrint = 0
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_data_delivery
                _DeliveryDataset = New DS_DD_multiservizi
        End Select

        ' ************************************************************************** '
        ' Caricamento del DataSet da utilizzare per la stampa.                       '
        ' ************************************************************************** '
        dsExcel = ReadExcelIntoDataset(_OLEDB_prefixStrs, PrintDataSet_DataFile(1))
        _CodiceProgressivo = 0
        While GetRecordCount() < _QuickDataset.TBL_main.Rows.Count And nRecordToPrint < nRecordToLoad
            dr_mai_qd = _QuickDataset.TBL_main.Rows(GetRecordCount)
            Try
                _ErroriLievi = ""
                _ErroriGravi = ""
                cFilter = String.Concat("[", dsExcel.Tables(0).Columns(0), "] = '", dr_mai_qd.MAI_num_doc_uff, "' and [", dsExcel.Tables(0).Columns(1), "] = '", dr_mai_qd.MAI_conto_contr, "'")
                If dsExcel.Tables(0).Select(cFilter).Length > 0 Then
                    dr_doc = dsExcel.Tables(0).Select(cFilter)(0)
                    Select Case _CodiceModulo
                        Case Is = ListaDataset.DAS_data_delivery
                            GetRecordDataDelivery(dr_mai_qd.MAI_codice, dr_doc)
                    End Select
                End If
                'Catch ex As Exception
                '    Throw SetExceptionItem("AddRecordToPrint", ex)
                'End Try
                If _ErroriGravi > "" Then
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori Gravi: ", _ErroriGravi)
                ElseIf _ErroriLievi > "" Then
                    dr_mai_qd.DEF_errcode = -2
                    dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori lievi: ", _ErroriLievi)
                Else
                    dr_mai_qd.DEF_errcode = 0
                    dr_mai_qd.DEF_errdesc = ""
                End If
            Catch ex As Exception
                dr_mai_qd.DEF_errcode = -1
                dr_mai_qd.DEF_errdesc = ex.Message
            End Try
            nRecordToPrint += 1
            SetRecordCount(GetRecordCount() + 1)
        End While

        ' ************************************************************************** '
        ' Consolidamento del DataSet da utilizzare per la stampa.                    '
        ' ************************************************************************** '
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_data_delivery
                _DeliveryDataset.AcceptChanges()
        End Select
        _Files("SerializedFullDatasetFIL") = SerializeDataset(Me.FullDataset, Me.Folders("TempFLD"))

    End Sub

    ReadOnly Property FullDataset_Serialized As String Implements PlugINs.PlugIN_2_0_0.FullDataset_Serialized

        Get
            Return SerializeDataset(Me.FullDataset, _TempFLD)
        End Get

    End Property

    ReadOnly Property QuickDataset_Serialized As String Implements PlugINs.PlugIN_2_0_0.QuickDataset_Serialized
        Get
            Return SerializeDataset(Me.QuickDataset, _TempFLD)
        End Get
    End Property

    '
    ' Metodo non necessario. Lo lascio per mantenere la compatibilit�. 
    '
    Public Sub PostAnteprima(ByVal aLista As ArrayList) Implements PlugINs.PlugIN_2_0_0.PostAnteprima

    End Sub

    Public Sub RemovePRNDSErrorRecord() Implements PlugINs.PlugIN_2_0_0.RemovePRNDSErrorRecord

    End Sub

    Public Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String, ByVal cParamSelez As String) Implements PlugINs.PlugIN_2_0_0.SetPrintable

    End Sub

    ' ************************************************************************** '
    ' Imposto correttamente la parte di selezione per la stampa dei record. Se   '
    ' non dobbiamo mantenere la selezione o se la dobbiamo mantenere ma il re-   ' 
    ' cord non � stampabile metto a "NON STAMPABILE (False)" il campo .          '
    ' ************************************************************************** '
    Public Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String) Implements PlugINs.PlugIN_2_0_0.SetSelectable
        Dim dr_mai_qd As DS_QD_multiservizi.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_multiservizi.TBL_mainRow)
        dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0) Or (dr_mai_qd.DEF_errcode = -2)
        If cFilter > "" Then
            dr_mai_qd.DEF_selectable = dr_mai_qd.DEF_selectable And dr_mai_qd(cFilter)
        End If

        ' ************************************************************************** '
        ' Pulisce i campi relativi agli errori in quanto ci stiamo occupando di un   '
        ' nuovo modulo.                                                              '
        ' ************************************************************************** '
        If dr_mai_qd.DEF_selectable Then
            dr_mai_qd.Item("DEF_errcode") = 0
            dr_mai_qd.Item("DEF_errdesc") = ""
        End If

    End Sub

    Public Sub UpdateQuickDataset() Implements PlugINs.PlugIN_2_0_0.UpdateQuickDatasetFile

    End Sub

#End Region

#Region "Procedure utili per la creazione della sessione di stampa"

    'Sub ImportaFileInSessione(ByVal nTipoFile As Integer, ByVal cFileName As String)
    '    Dim nRecordtoAnalized As Integer
    '    Dim xdWork1 As Xml.XmlDocument
    '    Dim xdWork2 As Xml.XmlDocument
    '    Dim nRecordXFile As Integer
    '    Dim xiWork As Xml.XmlNode
    '    Dim cDstFile As String
    '    Dim nRecord As Integer
    '    Dim nFile As Integer
    '    Dim nDim As Integer
    '    Dim xnItem1 As Xml.XmlNode
    '    Dim xnItem2 As Xml.XmlNode

    '    ' Nel caso di tipo file 1 allora si tratta di un file xml, indipendentemente dall'estensione 
    '    ' da suddividere eventualmente in file di pi� piccole dimensioni.
    '    If nTipoFile = 1 Then
    '        Try
    '            'xdWork1 = New Xml.XmlDocument
    '            'xdWork1.Load(cFileName)
    '            xdWork1 = caricaFileDatiXML(cFileName)
    '            For Each xnItem As Xml.XmlNode In xdWork1.SelectNodes("ElencoFattura/Fattura/SezioniDocumenti/DatiServizio/PuntoDiFornitura/GruppoMisura")
    '                xnItem1 = xnItem.SelectSingleNode("GruppoMisura")
    '                If Not xnItem1 Is Nothing Then
    '                    xnItem2 = xdWork1.CreateNode(Xml.XmlNodeType.Element, "GruppoMisura1", "")
    '                    xnItem2.InnerText = xnItem1.InnerText
    '                    xnItem.AppendChild(xnItem2.Clone)
    '                    xnItem.RemoveChild(xnItem1)
    '                    For Each xnItem3 As Xml.XmlNode In xnItem.SelectNodes("StrumentoMisura")
    '                        xnItem1 = xnItem3.SelectSingleNode("StrumentoMisura")
    '                        xnItem2 = xdWork1.CreateNode(Xml.XmlNodeType.Element, "StrumentoMisura1", "")
    '                        xnItem2.InnerText = xnItem1.InnerText
    '                        xnItem3.AppendChild(xnItem2.Clone)
    '                        xnItem3.RemoveChild(xnItem1)
    '                    Next
    '                End If
    '            Next
    '            For Each xnItem As Xml.XmlNode In xdWork1.SelectNodes("ElencoFattura/Fattura/SezioniDocumenti/Contratto/OT")
    '                xnItem1 = xnItem.SelectSingleNode("OT")
    '                If Not xnItem1 Is Nothing Then
    '                    xnItem2 = xdWork1.CreateNode(Xml.XmlNodeType.Element, "OT1", "")
    '                    xnItem2.InnerText = xnItem1.InnerText
    '                    xnItem.AppendChild(xnItem2.Clone)
    '                    xnItem.RemoveChild(xnItem1)
    '                End If
    '            Next
    '            'xdWork1.Save(cFileName)
    '            SalvaFileDatiXML(cFileName, xdWork1)
    '            nDim = New System.IO.FileInfo(cFileName).Length
    '            If (nDim < UpperFileSize) Then
    '                _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
    '                cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))
    '                'xdWork1.Save(cDstFile)
    '                SalvaFileDatiXML(cDstFile, xdWork1)

    '                'System.IO.File.Copy(cFileName, cDstFile)
    '                _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
    '                _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
    '            Else
    '                _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
    '                _MessaggiAvanzamento(5) = "Suddivizione del file nella cartella di archiviazione..."

    '                nRecordtoAnalized = xdWork1.SelectNodes("ElencoFattura/Fattura").Count
    '                nRecordXFile = Int(LowerFileSize / (nDim / nRecordtoAnalized)) + 1
    '                _MessaggiAvanzamento(4) = CInt(_MessaggiAvanzamento(4)) + Int(nRecordtoAnalized / nRecordXFile) + 1
    '                nRecord = 0
    '                nFile = 1
    '                For Each xiWork In xdWork1.SelectNodes("ElencoFattura/Fattura")
    '                    If nRecord = 0 Then
    '                        xdWork2 = New Xml.XmlDocument
    '                        xdWork2.AppendChild(xdWork2.CreateNode(Xml.XmlNodeType.Element, "ElencoFattura", ""))
    '                    End If
    '                    xdWork2.SelectSingleNode("ElencoFattura").AppendChild(xdWork2.ImportNode(xiWork, True))
    '                    nRecord += 1
    '                    nRecordtoAnalized -= 1
    '                    If (nRecord = nRecordXFile) Or (nRecordtoAnalized = 0) Then
    '                        _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
    '                        cDstFile = cFileName.Substring(cFileName.LastIndexOf("\") + 1)
    '                        cDstFile = String.Concat(cDstFile.Substring(0, cDstFile.IndexOf(".")), "_", nFile.ToString.PadLeft(5, "0"), cDstFile.Substring(cDstFile.IndexOf(".")))
    '                        cDstFile = String.Concat(_SessioneDatiPath, cDstFile)
    '                        'xdWork2.Save(cDstFile)
    '                        SalvaFileDatiXML(cDstFile, xdWork2)
    '                        xdWork2 = Nothing
    '                        _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
    '                        nRecord = 0
    '                        nFile += 1
    '                    End If
    '                Next
    '                xdWork1 = Nothing
    '            End If
    '        Catch ex As Exception
    '            MessageBox.Show(String.Concat("Attenzione il file ", cFileName, " non � correttamente formattato.", vbCr, "L'errore � riportato di seguito:", vbCr, ex.Message), "Errore di caricamento di un file", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))
    '            System.IO.File.Copy(cFileName, cDstFile)
    '            _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, -1})
    '        End Try
    '    Else
    '        _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
    '        cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

    '        System.IO.File.Copy(cFileName, cDstFile)
    '        _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
    '        _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
    '    End If

    'End Sub

#End Region

#Region "Caricamento del PRINTDATASET"


#End Region

#Region "Caricamento del PRINTDATSET della fattura"

    Private Sub CheckFieldValue(ByVal cValue As String, ByVal cMsgError As String)

        If cValue = "" And cMsgError > "" Then
            AddErroreLieve(cMsgError)
        End If

    End Sub

#End Region

    Private Sub AddErroreLieve(ByVal cMsg As String)

        If _ErroriLievi = "" Then
            _ErroriLievi = cMsg
        Else
            _ErroriLievi = String.Concat(_ErroriLievi, "; ", cMsg)
        End If

    End Sub

    Private Sub ImpostaMessaggi(ByVal aValue As String())
        Dim i As Integer

        For i = 0 To 5
            If aValue(i) > "" Then
                _MessaggiAvanzamento(i) = aValue(i)
            End If
        Next

    End Sub

    Private Function SetExceptionItem(ByVal cProcError As String, ByVal ex As Exception) As Exception
        Dim ex_rtn As Exception

        If ex.InnerException Is Nothing Then
            ex_rtn = New Exception(String.Concat("Errore (", cProcError, "): ", ex.Message), ex)
        Else
            ex_rtn = ex
        End If
        Return ex_rtn

    End Function

    Private Sub AddRecordToDS(ByVal dr_mai_qd As DS_QD_multiservizi.TBL_mainRow)
        Dim dsExcel As DataSet
        Dim dr_doc As DataRow
        Dim cFilter As String

        Try
            If dsExcel Is Nothing Then
                dsExcel = ReadExcelIntoDataset(_OLEDB_prefixStrs, PrintDataSet_DataFile(dr_mai_qd.MAI_filedati))
            End If
            cFilter = String.Concat("[", dsExcel.Tables(0).Columns(0), "] = '", dr_mai_qd.MAI_num_doc_uff, "' and [", dsExcel.Tables(0).Columns(1), "] = '", dr_mai_qd.MAI_conto_contr, "'")
            If dsExcel.Tables(0).Select(cFilter).Length > 0 Then
                dr_doc = dsExcel.Tables(0).Select(cFilter)(0)
                Select Case _CodiceModulo
                    Case Is = ListaDataset.DAS_data_delivery
                        GetRecordDataDelivery(dr_mai_qd.MAI_codice, dr_doc)
                End Select
            End If
        Catch ex As Exception
            Throw SetExceptionItem("AddRecordToPrint", ex)
        End Try

    End Sub

    Private Function GetRecordCount() As Integer

        If _DicRecordCount Is Nothing Then
            _DicRecordCount = New Dictionary(Of Integer, Integer)
        End If
        If Not _DicRecordCount.ContainsKey(_CodiceModulo) Then
            _DicRecordCount.Add(_CodiceModulo, 0)
        End If
        Return _DicRecordCount(_CodiceModulo)

    End Function

    Private Sub SetRecordCount(ByVal nValue As Integer)

        If _DicRecordCount Is Nothing Then
            _DicRecordCount = New Dictionary(Of Integer, Integer)
        End If
        If Not _DicRecordCount.ContainsKey(_CodiceModulo) Then
            _DicRecordCount.Add(_CodiceModulo, 0)
        End If
        _DicRecordCount(_CodiceModulo) = nValue

    End Sub

    Private Sub GetRecordDataDelivery(ByVal nCodice As Integer, ByVal dr_fat As DataRow)
        Dim dr_ded As DS_DD_multiservizi.TBL_delivery_dataRow

        dr_ded = _DeliveryDataset.TBL_delivery_data.NewRow
        dr_ded.DED_barcode = String.Concat(dr_fat(1).ToString.PadLeft(10, "0"), dr_fat(0).ToString.PadLeft(12, "0"))
        dr_ded.DED_num_documento = dr_fat(0)
        dr_ded.DED_num_contratto = dr_fat(1)
        _DeliveryDataset.TBL_delivery_data.Rows.Add(dr_ded)

    End Sub

    Public WriteOnly Property Loggingobj() As CLS_log_srv Implements PlugINs.PlugIN_2_0_0.Loggingobj

        Set(ByVal value As CLS_log_srv)
            _Loggingobj = value
        End Set

    End Property

    Public WriteOnly Property TipoAmb() As Object Implements PlugINs.PlugIN_2_0_0.TipoAmb

        Set(ByVal value As Object)
            _TipoAmb = CType(value, TipoAmbiente)
        End Set

    End Property

    'Public ReadOnly Property FullDataset_Serialized() As String Implements PLUGIN_interfaceV1_6_1.IPLG_dati_lib.FullDataset_Serialized

    '    Get
    '        Return SerializeDataset(Me.FullDataset, _TempFLD)
    '    End Get

    'End Property

    'Public ReadOnly Property QuickDataset_Serialized() As String Implements PLUGIN_interfaceV1_6_1.IPLG_dati_lib.QuickDataset_Serialized

    '    Get
    '        If _SerializedQD = "" Then
    '            _SerializedQD = SerializeDataset(Me.QuickDataset, _TempFLD)
    '        End If
    '        Return _SerializedQD
    '    End Get

    'End Property

    Public Property FileDati() As ArrayList Implements PlugINs.PlugIN_2_0_0.FileDati

        Get
            Return _FileDati
        End Get
        Set(ByVal value As ArrayList)
            _FileDati = value
        End Set

    End Property

    Public WriteOnly Property OLEDB_prefixStrs() As Dictionary(Of String, String) Implements PlugINs.PlugIN_2_0_0.OLEDB_prefixStrs

        Set(ByVal value As Dictionary(Of String, String))
            _OLEDB_prefixStrs = value
        End Set

    End Property

    Public ReadOnly Property DescrizionePlugIn() As String Implements PlugINs.PlugIN_2_0_0.DescrizionePlugIn

        Get
            Return ""
        End Get

    End Property

    Function ValidaFileSessione(ByVal aDatiCreazioneSessione As ArrayList) As Boolean Implements PlugINs.PlugIN_2_0_0.ValidaFileSessione
        Dim cFileNameExcel As String
        Dim aFields As String()
        Dim dsExcel As DataSet
        Dim lValido As Boolean
        Dim aValue As String()
        Dim dc As DataColumn
        Dim cTmp As String
        Dim i As Integer

        Try
            cFileNameExcel = ""
            For Each aValue In aDatiCreazioneSessione
                If aValue(0) = TipoFile Then
                    cFileNameExcel = aValue(1)
                End If
            Next
            dsExcel = ReadExcelIntoDataset(_OLEDB_prefixStrs, cFileNameExcel)
            If dsExcel.Tables(0).Rows.Count = 0 Then
                lValido = False
                _ErrorePlugin = "Attenzione! Il file dei dati inviato risulta essere vuoto. Non posso procedere con la creazione della sessione."
            Else
                'aFields = New String() {"N. doc. ufficial", "Conto contr."}
                aFields = New String() {"N# doc# ufficiale", "Conto contr#", "Data Emissione"}
                For i = 0 To aFields.Length - 1
                    aFields(i) = aFields(i).ToLower
                Next
                lValido = True
                For Each dc In dsExcel.Tables(0).Columns
                    lValido = lValido And ((Array.IndexOf(aFields, dc.ColumnName.ToLower) > -1) Or (Array.IndexOf(aFields, dc.ColumnName.ToLower.Replace("#", ".")) > -1))
                Next

                If Not lValido Then
                    _ErrorePlugin = String.Concat("Attenzione i nomii delle colonne del foglio Excel non sono corrette.", vbLf, "Nome delle colonne atteso:", vbLf)
                    For Each cTmp In aFields
                        _ErrorePlugin = String.Concat(_ErrorePlugin, """", cTmp, """, ")
                    Next
                    _ErrorePlugin = String.Concat(_ErrorePlugin.Substring(0, _ErrorePlugin.Length - 2), ".", vbLf, "Nome delle colonne nel file:", vbLf)
                    For Each dc In dsExcel.Tables(0).Columns
                        _ErrorePlugin = String.Concat(_ErrorePlugin, """", dc.ColumnName, """, ")
                    Next
                    _ErrorePlugin = String.Concat(_ErrorePlugin.Substring(0, _ErrorePlugin.Length - 2), ".")
                End If
            End If
        Catch ex As Exception
            lValido = False
            _ErrorePlugin = String.Concat("Errore non specificato. Errore: ", ex.Message)
        End Try
        Return lValido

    End Function

    ReadOnly Property ErrorePlugIn() As String Implements PlugINs.PlugIN_2_0_0.ErrorePlugIn

        Get
            Return _ErrorePlugin
        End Get

    End Property

    Property Files As CLS_FileFolderDic Implements PlugINs.PlugIN_2_0_0.Files
    Property Folders As CLS_FileFolderDic Implements PlugINs.PlugIN_2_0_0.Folders

End Class