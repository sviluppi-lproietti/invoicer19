﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PLG_ASM_tari_anthea.Domain.Base
{
    public class rigaBase
    {
        String RawLine { get; set; }

        protected string[] aValue
        {
            get
            {
                return RawLine.Split(';');
            }
        }

        public rigaBase(string cLinea)
        {
            RawLine = cLinea;
        }
    }
}
