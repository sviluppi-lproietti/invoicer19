﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PLG_ASM_tari_anthea.Domain.Base;

namespace WindowsFormsApplication6.Entity
{
    public class row_050_40Entity : rigaBase
    {
        public row_050_40Entity(string cLinea) : base(cLinea)
        {

        }

        public string codice_fattura { get { return aValue[2]; } }
        public string descrizione_voce { get { return aValue[3]; } }
        public string unita_misura_importo { get { return aValue[4]; } }
        public decimal importo { get { return decimal.Parse(aValue[5]); } }
        public string codice_iva { get { return aValue[6]; } }
    }
}
