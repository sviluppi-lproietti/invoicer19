﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PLG_ASM_tari_anthea.Domain.Base;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class row_050_50Entity : rigaBase
    {

        public row_050_50Entity(String cLinea)
            : base(cLinea)
        {
        }

        public String codice_fattura
        {
            get
            {
                return aValue[2];
            }
        }

        public String descrizione_voce
        {
            get
            {
                return aValue[3];
            }
        }

        public String unita_misura_importo
        {
            get
            {
                return aValue[4];
            }
        }

        public Decimal importo
        {
            get
            {
                return decimal.Parse(aValue[5]);
            }
        }

        public String codice_iva
        {
            get
            {
                return aValue[6];
            }
        }
    }
}
