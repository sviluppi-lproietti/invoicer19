﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PLG_ASM_tari_anthea.Domain.Base;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class row_030_10Entity : rigaBase
    {
        public row_030_10Entity(String cLinea)
            : base(cLinea)
        { }

        public String CODICE_FATTURA
        {
            get
            {
                return aValue[2];
            }
        }

        public String UTENZA
        {
            get
            {
                return aValue[3];
            }
        }

        public String DESCRIZIONE_VOCE
        {
            get
            {
                return aValue[4];
            }
        }

        public String UNITA_MISURA_QUANTITA
        {
            get
            {
                return aValue[5];
            }
        }

        public String QUANTITA
        {
            get
            {
                return aValue[6];
            }
        }

        public String UNITA_MISURA_PREZZO
        {
            get
            {
                return aValue[7];
            }
        }

        public String PREZZO
        {
            get
            {
                return aValue[8];
            }
        }

        public String UNITA_MISURA_IMPORTO
        {
            get
            {
                return aValue[9];
            }
        }

        public String IMPORTO
        {
            get
            {
                return aValue[10];
            }
        }

        public String CODICE_IVA
        {
            get
            {
                return aValue[11];
            }
        }

    }
}
