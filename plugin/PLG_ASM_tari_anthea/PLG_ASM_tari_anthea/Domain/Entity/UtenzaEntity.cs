﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class UtenzaEntity
    {
        public row_000_00Entity Intestazionefattura { get; set; }
        public ArrayList ElencoRate { get; set; }
        public ArrayList ElencoContratti { get; set; }
        public Dictionary<String, row_030_35Entity> DatiCatastali { get; set; }
        public row_040_00Entity TotaleUtenze { get; set; }
        public row_025_00Entity NoteFattura { get; set; }
        public row_050_10Entity TributoProvinciale { get; set; }
        public row_050_25Entity Arrotondamento { get; set; }
        public row_050_30Entity TotaleDocumento { get; set; }
        public row_050_50Entity TotaleDaPagare { get; set; }
        public ArrayList ListaSolleciti { get; set; }

        public UtenzaEntity()
        {
            ElencoRate = new ArrayList();
            ElencoContratti = new ArrayList();
            DatiCatastali = new Dictionary<String, row_030_35Entity>();
            ListaSolleciti = new ArrayList();
        }

        public Boolean IsNotaCredito
        {
            get
            {
                return Intestazionefattura.flag_tipo_documento.ToLower() == "c";
            }
        }

        public void ConsolidaContratti()
        {
            //Dictionary<String, ContrattoEntity> aContratti;
            //List<ContrattoEntity> aDuplicati;
            //Boolean lDuplicato;
            //ContrattoEntity contr;
            //Boolean lInsert;
            //int i;
            
            foreach (ContrattoEntity con in this.ElencoContratti)
            {
                if (this.DatiCatastali.ContainsKey(con.DatiFornitura.UTENZA))
                    con.DatiCatastali = this.DatiCatastali[con.DatiFornitura.UTENZA];

            }

        }
    }
}