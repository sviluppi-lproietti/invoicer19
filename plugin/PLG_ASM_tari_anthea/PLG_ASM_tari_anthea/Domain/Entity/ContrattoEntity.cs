﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class ContrattoEntity
    {
        public int ProgressivoContratto { get; set; }
        public row_030_00Entity DatiFornitura { get; set; }
        public row_030_35Entity DatiCatastali { get; set; }
        public ArrayList DettaglioUtenza { get; set; }

        public ContrattoEntity()
        {
            DettaglioUtenza = new ArrayList();
        }
    }


}
