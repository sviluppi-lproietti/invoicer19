﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PLG_ASM_tari_anthea.Domain.Base;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class row_030_20Entity : rigaBase
    {
        public row_030_20Entity(string cLinea) : base(cLinea)
        {

        }

        public string codice_fattura { get; set; }
        public string descrizione_totale { get; set; }
        public string unita_misura { get; set; }
        public decimal importo_utenza { get; set; }
    }
}
