﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PLG_ASM_tari_anthea.Domain.Base;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class row_000_00Entity : rigaBase
    {
        public row_000_00Entity(String cLinea)
            : base(cLinea)
        {

        }

        public string codice_fattura
        {
            get
            {
                return aValue[2];
            }
        }

        public String numero_fattura
        {
            get
            {
                return aValue[3];
            }
        }

        public String data_fattura
        {
            get
            {
                return aValue[4];
            }
        }

        public String utente_id
        {
            get
            {
                return aValue[5];
            }
        }

        public String utente
        {
            get
            {
                return aValue[6];
            }
        }

        public String indirizzoResidenza
        {
            get
            {
                return aValue[7];
            }
        }

        public String civicoResidenza
        {
            get
            {
                return aValue[8];
            }
        }

        public String capResidenza
        {
            get
            {
                return aValue[9].PadLeft(5, '0');
            }
        }

        public String comuneResidenza
        {
            get
            {
                return aValue[10];
            }
        }

        public String provinciaResidenza
        {
            get
            {
                return aValue[11].Replace("(", "").Replace(")", "");
            }
        }

        public String codice_fiscale
        {
            get
            {
                return aValue[12];
            }
        }

        public String partita_iva
        {
            get
            {
                return aValue[13];
            }
        }

        public String indirizzoRecapito
        {
            get
            {
                return aValue[14];
            }
        }

        public String civicoRecapito
        {
            get
            {
                return aValue[15];
            }
        }

        public String capRecapito
        {
            get
            {
                return aValue[16].PadLeft(5, '0');
            }
        }

        public String comuneRecapito
        {
            get
            {
                return aValue[17];
            }
        }

        public String provinciaRecapito
        {
            get
        {
            return aValue[18].Replace("(", "").Replace(")", "");
}
        }

        public decimal IMPORTO_FATTURA
        {
            get
            {
                return decimal.Parse(aValue[20]);
            }
        }

        public String SESSO
        {
            get
            {
                return aValue[26];
            }
        }

        public String DATA_DI_NASCITA
        {
            get
            {
                return aValue[27];
            }
        }

        public String COMUNE_STATO_DI_NASCITA
        {
            get
            {
                return aValue[28];
            }
        }

        public String PROVINCIA_DI_NASCITA
        {
            get
            {
                return aValue[29];
            }
        }

        public String codice_comune
        {
            get
            {
                return aValue[30];
            }
        }

        public String COGNOME_RAGSOC
        {
            get
            {
                return aValue[31];
            }
        }

        public String NOME
        {
            get
            {
                return aValue[32];
            }
        }

        public String indirizzo_presso
        {
            get
            {
                return aValue[34];
            }
        }

        public String flag_tipo_documento
        {
            get
            {
                return aValue[35];
            }
        }
    }
}
