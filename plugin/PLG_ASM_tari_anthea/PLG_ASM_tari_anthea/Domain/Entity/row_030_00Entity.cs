﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PLG_ASM_tari_anthea.Domain.Base;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class row_030_00Entity : rigaBase
    {

        public row_030_00Entity(String cLinea)
            : base(cLinea)
        {
        }

        public String CODICE_FATTURA
        {
            get
            {
                return aValue[2];
            }
        }

        public String UTENZA
        {
            get
            {
                return aValue[3];
            }
        }

        public String UBICAZIONE
        {
            get
            {
                return aValue[4];
            }
        }

        public String PERIODO_DAL
        {
            get
            {
                return aValue[5];
            }
        }

        public String PERIODO_AL
        {
            get
            {
                return aValue[6];
            }
        }

        public String GIORNI_APPLICAZIONE
        {
            get
            {
                return aValue[7];
            }
        }

        public String TIPO
        {
            get
            {
                return aValue[8];
            }
        }

        public String CATEGORIA_DESCR_BREVE
        {
            get
            {
                return aValue[9];
            }
        }

        public String CATEGORIA_DESCR_LUNGA
        {
            get
            {
                String cTmp;

                cTmp = aValue[10].ToLower().Replace("componenti", "").Trim();
                if (cTmp == "")
                    cTmp = "-1";

                return cTmp;
            }
        }

        public String CAMPO_SIMBOLO
        {
            get
            {
                return aValue[11];
            }
        }

        public String TOTALE_UTENZA
        {
            get
            {
                return aValue[12];
            }
        }

        public String FLAG_DATI_CATASTALI
        {
            get
            {
                return aValue[13];
            }
        }

        public String SUPERFICIE
        {
            get
            {
                return aValue[14];
            }
        }

        public String LISTINO_Parte_Fissa
        {
            get
            {
                return aValue[15];
            }
        }

        public String LISTINO_Parte_Variabile
        {
            get
            {
                return aValue[16];
            }
        }

        public String PERCENTUALE_APPLICAZIONE
        {
            get
            {
                return aValue[17];
            }
        }

    }
}
