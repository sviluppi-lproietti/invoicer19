﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PLG_ASM_tari_anthea.Domain.Base;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class row_025_00Entity : rigaBase
    {
        public row_025_00Entity(String cLinea)
            : base(cLinea)
        { }

        public String codice_fattura
        {
            get
            {
                return aValue[2];
            }
        }

        public String note_fattura
        {
            get
            {
                return aValue[3];
            }
        }
    }
}
