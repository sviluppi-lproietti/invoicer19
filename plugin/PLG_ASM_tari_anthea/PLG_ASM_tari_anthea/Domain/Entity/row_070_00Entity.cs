﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PLG_ASM_tari_anthea.Domain.Base;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class row_070_00Entity : rigaBase
    {
        public row_070_00Entity(string cLinea)
            : base(cLinea)
        { }

        public string CODICE_FATTURA
        { get { return aValue[2]; } }

        public string NUM_FATTURA
        { get { return aValue[3]; } }

        public string DATA_FATTURA
        { get { return aValue[4]; } }

        public string DATA_SCADENZA
        { get { return aValue[5]; } }

        public string SIMBOLO_IMPORTO_FATT
        { get { return aValue[6]; } }

        public Decimal IMPORTO_FATTURA
        { get { return Decimal.Parse(aValue[7]); } }

        public string SIMBOLO_IMPORTO_DOVUTO
        { get { return aValue[8]; } }

        public Decimal IMPORTO_DOVUTO
        { get { return Decimal.Parse(aValue[9]); } }

        public string SIMBOLO_IMPORTO_SCAD
        { get { return aValue[2]; } }

        public Decimal IMPORTO_SCAD
        { get { return Decimal.Parse(aValue[10]); } }

        public string NR_RATA
        { get { return aValue[11]; } }

        public string ANNO_RIFERIMENTO
        { get { return aValue[12]; } }

        public string SIMBOLO_TOTALE_IMPORTO
        { get { return aValue[13]; } }

        public Decimal TOTALE_IMPORTO
        { get { return Decimal.Parse(aValue[14]); } }

        public string SIMBOLO_TOTALE_DOVUTO
        { get { return aValue[15]; } }

        public Decimal TOTALE_DOVUTO
        { get { return Decimal.Parse(aValue[16]); } }

    }
}
