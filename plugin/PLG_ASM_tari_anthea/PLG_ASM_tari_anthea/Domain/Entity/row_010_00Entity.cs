﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PLG_ASM_tari_anthea.Domain.Base;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class row_010_00Entity : rigaBase
    {

        public row_010_00Entity(String cLinea)
            : base(cLinea)
        { }

        public String codice_fattura
        {
            get
            {

                return aValue[2];
            }
        }

        public String codice_ocr
        {
            get
            {
                return aValue[3];
            }
        }

        public String campo_simbolo
        {
            get
            {
                return aValue[4];
            }
        }

        public Decimal importo_rata
        {
            get
            {
                return Decimal.Parse(aValue[5]);
            }
        }

        public String data_scadenza
        {
            get
            {
                return aValue[6];
            }
        }

        public String DESCRIZIONE_RATA
        {
            get
            {
                return aValue[7];
            }
        }

        public String CODICE_COMUNE
        {
            get
            {
                return aValue[8];
            }
        }

        public String CODICE_TRIBUTO
        {
            get
            {
                return aValue[9];
            }
        }

        public String ACCONTO_SALDO
        {
            get
            {
                return aValue[10];
            }
        }

        public String N_IMMOBILI
        {
            get
            {
                return aValue[11];
            }
        }

        public String RATEIZZAZIONE
        {
            get
            {
                return aValue[12];
            }
        }

        public String ANNO
        {
            get
            {
                return aValue[13];
            }
        }

        public String CODICE_SEZIONE
        {
            get
            {
                return aValue[14];
            }
        }

        public String CIN_IMPORTO
        {
            get
            {
                return aValue[15];
            }
        }

        public String CIN_INTERMEDIO
        {
            get
            {
                return aValue[16];
            }
        }

        public String CIN_COMPLESSIVO
        {
            get
            {
                return aValue[17];
            }
        }

        public String CODIFICA_BOLLETTINO
        {
            get
            {
                return aValue[18];
            }
        }

        public String NUMERO_RATA
        {
            get
            {
                return aValue[19];
            }
        }

    }
}
