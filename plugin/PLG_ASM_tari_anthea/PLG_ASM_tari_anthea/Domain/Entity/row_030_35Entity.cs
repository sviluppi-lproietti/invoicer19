﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PLG_ASM_tari_anthea.Domain.Base;

namespace PLG_ASM_tari_anthea.Domain.Entity
{
    public class row_030_35Entity : rigaBase
    {

        public row_030_35Entity(String cLinea)
            : base(cLinea)
        {
        }

        public String CODICE_FATTURA
        {
            get
            {

                return aValue[2];
            }
        }

        public String UTENZA
        {
            get
            {
                return aValue[3];
            }
        }

        public String COMUNE_AMMINISTRATIVO
        {
            get
            {
                return aValue[4];
            }
        }

        public String COMUNE_CATASTALE
        {
            get
            {
                return aValue[5];
            }
        }

        public String CODICE_COMUNE_CATASTALE
        {
            get
            {
                return aValue[6];
            }
        }

        public String TIPO_CATASTO
        {
            get
            {
                return aValue[7];
            }
        }

        public String SEZIONE
        {
            get
            {
                return aValue[8];
            }
        }

        public String FOGLIO
        {
            get
            {
                return aValue[9];
            }
        }

        public String SUBALTERNO
        {
            get
            {
                return aValue[10];
            }
        }

        public String PARTICELLA
        {
            get
            {
                return aValue[11];
            }
        }

        public String SEGUE_PARTICELLA
        {
            get
            {
                return aValue[12];
            }
        }

        public String PROPRIETARIO
        {
            get
            {
                return aValue[13];
            }
        }

        public String CF_PROPRIETARIO
        {
            get
            {
                return aValue[14];
            }
        }

        public String CATEGORIA_1
        {
            get
            {
                return aValue[15];
            }
        }

        public String TITOLO_OCCUPAZIONE
        {
            get
            {
                return aValue[16];
            }
        }

        public String CATEGORIA_2
        {
            get
            {
                return aValue[17];
            }
        }

        public String DESTINAZIONE_USO
        {
            get
            {
                return aValue[18];
            }
        }

    }
}
