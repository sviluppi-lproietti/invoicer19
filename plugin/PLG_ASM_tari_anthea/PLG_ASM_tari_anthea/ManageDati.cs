﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PLG_ASM_tari_anthea.Domain.Entity;

namespace PLG_ASM_tari_anthea
{
    public class ManageDati
    {
        public Dictionary<String, UtenzaEntity> ListaUtenze { get; set; }

        private String _FileName { get; set; }

        public ManageDati()
        {
            ListaUtenze = new Dictionary<String, UtenzaEntity>();
        }

        public void LoadFileDati(string fn)
        {
            System.IO.StreamReader sr;
            ContrattoEntity co;
            UtenzaEntity ut;
            String cLinea;
            String[] aValue;

            _FileName = fn;
            sr = new System.IO.StreamReader(_FileName, Encoding.GetEncoding("iso-8859-1"));
            ut = null;
            co = null;
            while (!sr.EndOfStream)
            {
                cLinea = sr.ReadLine();
                if (cLinea != "")
                {
                    aValue = cLinea.Split(';');
                    if (aValue[0] == "000" && aValue[1] == "00")
                    {
                        ut = new UtenzaEntity();
                        ut.Intestazionefattura = new row_000_00Entity(cLinea);
                        ListaUtenze.Add(ut.Intestazionefattura.numero_fattura, ut);
                    }
                    else if (aValue[0] == "010" && aValue[1] == "00")
                        ut.ElencoRate.Add(new row_010_00Entity(cLinea));
                    else if (aValue[0] == "025" && aValue[1] == "00")
                        ut.NoteFattura = new row_025_00Entity(cLinea);
                    else if (aValue[0] == "030" && aValue[1] == "00")
                    {
                        co = new ContrattoEntity();
                        co.ProgressivoContratto = ut.ElencoContratti.Count + 1;
                        ut.ElencoContratti.Add(co);
                        co.DatiFornitura = new row_030_00Entity(cLinea);
                    }
                    else if (aValue[0] == "030" && aValue[1] == "10")
                        co.DettaglioUtenza.Add(new row_030_10Entity(cLinea));
                    else if (aValue[0] == "030" && aValue[1] == "35")
                    {
                        row_030_35Entity tmp;

                        tmp = new row_030_35Entity(cLinea);
                        if (!ut.DatiCatastali.ContainsKey(tmp.UTENZA))
                            ut.DatiCatastali.Add(tmp.UTENZA, tmp);
                    }
                    else if (aValue[0] == "040" && aValue[1] == "00")
                        ut.TotaleUtenze = new row_040_00Entity(cLinea);
                    else if (aValue[0] == "050" && aValue[1] == "10")
                        ut.TributoProvinciale = new row_050_10Entity(cLinea);
                    else if (aValue[0] == "050" && aValue[1] == "25")
                        ut.Arrotondamento = new row_050_25Entity(cLinea);
                    else if (aValue[0] == "050" && aValue[1] == "30")
                        ut.TotaleDocumento = new row_050_30Entity(cLinea);
                    else if (aValue[0] == "050" && aValue[1] == "50")
                        ut.TotaleDaPagare = new row_050_50Entity(cLinea);
                    else if (aValue[0] == "070" && aValue[1] == "00")
                        ut.ListaSolleciti.Add(new row_070_00Entity(cLinea));
                }
            }

            //
            // Consolida i contratti utilizzando le regole. 
            //
            foreach (UtenzaEntity ut1 in ListaUtenze.Values)
                ut1.ConsolidaContratti();
            sr.Close();

        }
    }
}
