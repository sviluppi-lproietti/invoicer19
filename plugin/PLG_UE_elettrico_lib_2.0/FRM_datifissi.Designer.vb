<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_datifissi
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMB_tabelle_list = New System.Windows.Forms.ComboBox
        Me.CMB_variable = New System.Windows.Forms.ComboBox
        Me.TXB_valore = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.BTN_ignora = New System.Windows.Forms.Button
        Me.BTN_salva = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'CMB_tabelle_list
        '
        Me.CMB_tabelle_list.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_tabelle_list.FormattingEnabled = True
        Me.CMB_tabelle_list.Location = New System.Drawing.Point(118, 12)
        Me.CMB_tabelle_list.Name = "CMB_tabelle_list"
        Me.CMB_tabelle_list.Size = New System.Drawing.Size(338, 21)
        Me.CMB_tabelle_list.TabIndex = 1
        '
        'CMB_variable
        '
        Me.CMB_variable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CMB_variable.FormattingEnabled = True
        Me.CMB_variable.Location = New System.Drawing.Point(118, 39)
        Me.CMB_variable.Name = "CMB_variable"
        Me.CMB_variable.Size = New System.Drawing.Size(338, 21)
        Me.CMB_variable.TabIndex = 2
        '
        'TXB_valore
        '
        Me.TXB_valore.Location = New System.Drawing.Point(118, 66)
        Me.TXB_valore.Name = "TXB_valore"
        Me.TXB_valore.Size = New System.Drawing.Size(338, 20)
        Me.TXB_valore.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Tabella dati fissi"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Variabile"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Valore"
        '
        'BTN_ignora
        '
        Me.BTN_ignora.Location = New System.Drawing.Point(242, 92)
        Me.BTN_ignora.Name = "BTN_ignora"
        Me.BTN_ignora.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignora.TabIndex = 8
        Me.BTN_ignora.Text = "Ignora"
        Me.BTN_ignora.UseVisualStyleBackColor = True
        '
        'BTN_salva
        '
        Me.BTN_salva.Location = New System.Drawing.Point(137, 92)
        Me.BTN_salva.Name = "BTN_salva"
        Me.BTN_salva.Size = New System.Drawing.Size(75, 23)
        Me.BTN_salva.TabIndex = 7
        Me.BTN_salva.Text = "Salva"
        Me.BTN_salva.UseVisualStyleBackColor = True
        '
        'FRM_datifissi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 122)
        Me.Controls.Add(Me.BTN_ignora)
        Me.Controls.Add(Me.BTN_salva)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXB_valore)
        Me.Controls.Add(Me.CMB_variable)
        Me.Controls.Add(Me.CMB_tabelle_list)
        Me.Name = "FRM_datifissi"
        Me.Text = "Gestione valori dati fissi"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMB_tabelle_list As System.Windows.Forms.ComboBox
    Friend WithEvents CMB_variable As System.Windows.Forms.ComboBox
    Friend WithEvents TXB_valore As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents BTN_ignora As System.Windows.Forms.Button
    Friend WithEvents BTN_salva As System.Windows.Forms.Button
End Class
