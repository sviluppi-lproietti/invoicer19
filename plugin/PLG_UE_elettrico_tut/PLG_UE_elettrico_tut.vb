Imports System.Windows.Forms
Imports TipiComuni

Public Class PLG_UE_elettrico_tut
    Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib

    Enum eSortRiepValue
        SRV_totale_spesa = 10000
        SRV_ricalcoli = 10000
        SRV_materia = 20000
        SRV_trasporto = 30000
        SRV_oneri = 40000
        SRV_imposte_iva = 50000
        SRV_totale_imposte_iva = 60000
        SRV_totale_fattura = 80000
        SRV_altre_partite = 90000
        SRV_altro = 100000
        SRV_totale_pagare = 110000
    End Enum

    Const UpperFileSize = 3000000
    Const LowerFileSize = 2000000

    Const cns_tsm_sel_con_bollettino = "TSM_sel_con_bollettino"
    Const cns_tsm_sel_con_allegato = "TSM_sel_con_allegato"
    Const cnt_tiposezione_lavoro = "sezioni valorizzazione lavoro"

    ' Costante indicante il corretto registro IVA della parte UmbriaEnergy Elettricit� TUTELATO
    Const cns_prot_reg_iva_corr = "UET"

    Private _lPeakOnOff As Boolean
    Private _CodiceProgressivo As Integer
    Private _SessioneDati As Xml.XmlNode
    Private _SessioneTrasformazioniFileName As String
    Private _SessioneNuova As Boolean
    Private _SessionePath As String
    Private _CodiceModulo As Integer
    Private _FeedString As String
    Private _QuickDataset As DS_QD_UE_ele
    Private _PrintFatturaDataset As DS_PF_UE_ele
    Private _PrintBollettinoDataset As DS_PB_UE_ele
    Private _SendEmailDataset As DS_SE_UE_ele
    Private _RecordCount As Integer
    Private _xmlDati_qd As Xml.XmlDocument
    Private _MessaggiAvanzamento(5) As String
    Private _ErroriGravi As String
    Private _ErroriLievi As String
    Private _DataOneriAmmGas As DateTime
    Private _DataImposteGas As DateTime
    Private _DataLastResort As DateTime
    Private _SessioneDatiFissiFileName As String
    Private _SessioneDatiPath As String
    Private _dt_tof As DS_PF_UE_ele.TBL_totali_fatturatiDataTable
    Private _dr_mai_pf As DS_PF_UE_ele.TBL_mainRow
    Private _dr_dfa_pf As DS_PF_UE_ele.TBL_dati_fattRow
    Private _dr_stm As DS_PF_UE_ele.TBL_strum_misuraRow
    Private _LogDS As DataSet
    Private _ResultAL As ArrayList
    Private _MasterRecord As Integer
    Private _NomeOfferta As String
    Private _TmpDistributore As String
    Private _TipoCaricamento As Integer
    Private _ListaGiri As ArrayList
    Private _DbGiriConnString As String
    Private _NuoviToponimi As Boolean
    Private _GRP_orario As Boolean
    Private _ModalitaFatturazione As String
    Private _DataMultiPOS As DateTime

    Enum ListaDataset
        DAS_quick = 0
        DAS_fattura = 1
        DAS_bollettino = 2
        DAS_send_email = 999
    End Enum

    ' ************************************************************************** '
    ' Elenco delle propriet� dell'interfaccia di accesso al Plug IN.             '
    ' ************************************************************************** '

#Region "Elenco delle propriet� publiche del plug in che vengono implementate dall'interfaccia"

    Property MessaggiAvanzamento() As String() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.MessaggiAvanzamento

        Get
            Return _MessaggiAvanzamento
        End Get
        Set(ByVal value As String())
            _MessaggiAvanzamento = value
        End Set

    End Property

    ReadOnly Property CurrentRecordNumber() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CurrentRecordNumber

        Get
            Return _RecordCount
        End Get

    End Property

    ReadOnly Property DataSendByMail() As Xml.XmlNode Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DataSendByMail

        Get
            Return GetDataSendByMail()
        End Get

    End Property

    ReadOnly Property ElementiMenuDedicati() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ElementiMenuDedicati

        Get
            Dim aMenuItem As ArrayList
            Dim MenuEntry As ToolStripMenuItem
            Dim SubMenuEntry As ToolStripMenuItem
            Dim tsb As System.Windows.Forms.ToolStripButton

            aMenuItem = New ArrayList
            If _TipoCaricamento = 1 Then
                MenuEntry = New ToolStripMenuItem("Stampa Elettrico")

                SubMenuEntry = New ToolStripMenuItem("Bimestre di fatturazione")
                AddHandler SubMenuEntry.Click, AddressOf BimestreFatturazione_click
                MenuEntry.DropDownItems.Add(SubMenuEntry)

                SubMenuEntry = New ToolStripMenuItem("Forza data scadenza")
                AddHandler SubMenuEntry.Click, AddressOf ForzaDataScadenza_click
                MenuEntry.DropDownItems.Add(SubMenuEntry)

                SubMenuEntry = New ToolStripMenuItem("Testo autolettura")
                AddHandler SubMenuEntry.Click, AddressOf TestoAutolettura_click
                MenuEntry.DropDownItems.Add(SubMenuEntry)

                SubMenuEntry = New ToolStripMenuItem("Stampa autolettura")
                SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa).ToLower = "si"
                AddHandler SubMenuEntry.Click, AddressOf StampaAutolettura_click
                MenuEntry.DropDownItems.Add(SubMenuEntry)

                SubMenuEntry = New ToolStripMenuItem("Stampa Box Commerciale")
                SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale).ToLower = "si"
                AddHandler SubMenuEntry.Click, AddressOf StampaBoxCommerciale_click
                MenuEntry.DropDownItems.Add(SubMenuEntry)

                MenuEntry.DropDownItems.Add(New ToolStripSeparator())

                SubMenuEntry = New ToolStripMenuItem("Dati Fissi")
                AddHandler SubMenuEntry.Click, AddressOf DatiFissi_click
                MenuEntry.DropDownItems.Add(SubMenuEntry)

                MenuEntry.DropDownItems.Add(New ToolStripSeparator())

                ' Imposta tutti i record con allegato
                SubMenuEntry = New ToolStripMenuItem("Allegato su tutti")
                AddHandler SubMenuEntry.Click, AddressOf AllegatiAll_click
                MenuEntry.DropDownItems.Add(SubMenuEntry)

                ' Imposta nessun record con allegato
                SubMenuEntry = New ToolStripMenuItem("Allegato su Nessuno")
                AddHandler SubMenuEntry.Click, AddressOf AllegatiNot_click
                MenuEntry.DropDownItems.Add(SubMenuEntry)

                aMenuItem.Add(New Object() {"n", "TSM_sessione_stampa", MenuEntry})

                aMenuItem.Add(New Object() {"a", "TSM_selezione", New ToolStripSeparator()})

                ' Seleziona tutti i record con bollettino
                SubMenuEntry = New ToolStripMenuItem("Con bollettino")
                SubMenuEntry.Name = cns_tsm_sel_con_bollettino
                aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

                ' Seleziona tutti i record con allegato
                SubMenuEntry = New ToolStripMenuItem("Con allegato")
                SubMenuEntry.Name = cns_tsm_sel_con_allegato
                aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

                tsb = New System.Windows.Forms.ToolStripButton
                tsb.AutoSize = True
                tsb.ImageTransparentColor = System.Drawing.Color.Magenta
                AddHandler tsb.Click, AddressOf ForzaOffertaScacciapensieri_TSB_click
                tsb.Name = "TSB_forza_scacciapensieri"
                tsb.Text = "Forzatura scacciapensieri"
                SetButtonForzaturaScacciapensieri(tsb)

                aMenuItem.Add(New Object() {"a", "TOS_menu", tsb})
            End If
            Return aMenuItem
        End Get

    End Property

    ReadOnly Property FullDataset() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataset

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return _PrintFatturaDataset
                Case Is = ListaDataset.DAS_bollettino
                    Return _PrintBollettinoDataset
                Case Is = ListaDataset.DAS_send_email
                    Return _SendEmailDataset
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    ReadOnly Property FullDataSet_MainTableName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataSet_MainTableName

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "TBL_main"
                Case Is = ListaDataset.DAS_bollettino
                    Return "TBL_fat_x_rat"
                Case Is = ListaDataset.DAS_send_email
                    Return "TBL_main"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    ReadOnly Property FullDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataset_MainTableKey

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "MAI_codice"
                Case Is = ListaDataset.DAS_bollettino
                    Return "FXR_codice"
                Case Is = ListaDataset.DAS_send_email
                    Return "MAI_codice"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    ReadOnly Property LinkField_QuickDS_FullDS() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LinkField_QuickDS_FullDS

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "MAI_linkQD"
                Case Is = ListaDataset.DAS_bollettino
                    Return "FXR_linkQD"
                Case Is = ListaDataset.DAS_send_email
                    Return "MAI_linkQD"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ListaControlliFormNuovaSessione

        Get
            Dim aCtr As ArrayList

            aCtr = New ArrayList
            aCtr.Add(New String() {"LBL_input_1", "Folder con i file dati ......:", "3;16", "155;13"})
            aCtr.Add(New String() {"TXB_input_1", "&1", "160;12", "610;20", "*1"})
            aCtr.Add(New String() {"BTN_fld_input_1", "", "775;10", "27;23"})
            aCtr.Add(New String() {"LBL_input_3", "File credenziali sito web ...:", "3;42", "155;13"})
            aCtr.Add(New String() {"TXB_input_3", "&1", "160;38", "610;20", "102"})
            aCtr.Add(New String() {"BTN_fil_input_3", "", "775;36", "27;23"})
            aCtr.Add(New String() {"LBL_input_4", "File allegato tipografico ...:", "3;68", "155;13"})
            aCtr.Add(New String() {"TXB_input_4", "&1", "160;64", "610;20", "190"})
            aCtr.Add(New String() {"BTN_fil_input_4", "", "775;62", "27;23"})
            aCtr.Add(New String() {"LBL_output_998", "Cartella di destinazione .:", "3;94", "155;13"})
            aCtr.Add(New String() {"TXB_output_998", "&998", "160;90", "610;20", "201"})
            aCtr.Add(New String() {"BTN_fld_output_998", "", "775;88", "27;23"})
            aCtr.Add(New String() {"LBL_output_999", "Descrizione archivio ......:", "3;120", "155;13"})
            aCtr.Add(New String() {"TXB_output_999", "", "160;116", "M;640;122", "202"})
            aCtr.Add(New String() {"SRT", "", "", "", ""})
            Return aCtr
        End Get

    End Property

    ReadOnly Property LogDS() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LogDSStampa

        Get
            LoadDSLog()
            Return _LogDS
        End Get

    End Property

    ReadOnly Property MessaggioAvviso() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.MessaggioAvviso

        Get
            Dim cValue As String

            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura, ListaDataset.DAS_bollettino
                    cValue = String.Concat("Attenzione!! Ricordiamo che prima di procedere con la stampa dei moduli selezionati", vbCr, "� necessario verificare i seguenti dati:", vbCr, "- Periodo di riferimento (Bimestre);", vbCr, "- Testo dell'autolettura.", vbLf, vbLf, "Posso procedere con la stampa di quanto selezionato?")
                Case Is = ListaDataset.DAS_send_email
                    cValue = String.Concat("Da modificare!!!!!!!!!!!!!!!!!")
                Case Else
                    cValue = ""
            End Select
            Return cValue
        End Get

    End Property

    ReadOnly Property NomePlugIn() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.NomePlugIn

        Get
            Return "Plug-in per la stampa delle bollette di UmbriaEnergy dell'elettrico del mercato Libero."
        End Get

    End Property

    ReadOnly Property QuickDataset() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset

        Get
            Return _QuickDataset
        End Get

    End Property

    ReadOnly Property QuickDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset_MainTableKey

        Get
            Return "MAI_codice"
        End Get

    End Property

    ReadOnly Property QuickDataset_MainTableName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset_MainTableName

        Get
            Return "TBL_main"
        End Get

    End Property

    ReadOnly Property ResultAL() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ResultAL

        Get
            Return _ResultAL
        End Get

    End Property

    WriteOnly Property CodiceModulo() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CodiceModulo

        Set(ByVal value As Integer)
            _CodiceModulo = value
        End Set

    End Property

    WriteOnly Property FeedString() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FeedString

        Set(ByVal value As String)
            _FeedString = value
        End Set

    End Property

    WriteOnly Property FiltroSelezione() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FiltroSelezione

        Set(ByVal value As String)

        End Set

    End Property

    WriteOnly Property SessioneFld() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneFld

        Set(ByVal value As String)
            _SessionePath = value
        End Set

    End Property

    WriteOnly Property SezioneDati() As Xml.XmlNode Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SezioneDati

        Set(ByVal value As Xml.XmlNode)
            _SessioneDati = value
        End Set

    End Property

    WriteOnly Property SessioneDatiFissiFileName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneDatiFissiFileName

        Set(ByVal value As String)
            _SessioneDatiFissiFileName = value
        End Set

    End Property

    WriteOnly Property SessioneTrasformazioniFileName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneTrasformazioniFileName

        Set(ByVal value As String)
            _SessioneTrasformazioniFileName = value
        End Set

    End Property

    WriteOnly Property SessioneDatiPath() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneDatiPath

        Set(ByVal value As String)
            _SessioneDatiPath = value
        End Set

    End Property

    WriteOnly Property SessioneNuova() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneNuova

        Set(ByVal value As Boolean)
            _SessioneNuova = value
        End Set

    End Property

    WriteOnly Property TipoCaricamento() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.TipoCaricamento

        Set(ByVal value As Integer)
            _TipoCaricamento = value
        End Set

    End Property

#End Region

    ' ************************************************************************** '
    ' Elenco dei metodi dell'interfaccia di accesso al Plug IN.                  '
    ' ************************************************************************** '
#Region "Elenco delle propriet� publiche del plug in che vengono implementate dall'interfaccia"

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckDatiSessione
        Dim _lObb As Boolean = False
        Dim _l101 As Boolean = False
        Dim _l102 As Boolean = False
        Dim lRtn As Boolean = True

        For Each aValue As String() In aDati
            Select Case aValue(0)
                Case Is = 1
                    _lObb = True
                Case Is = 102
                    _l102 = True
            End Select
        Next
        If _SessioneNuova Then
            If Not _lObb Then
                MessageBox.Show("Attenzione! Non sono � stato indicato nessun file contenente i dati. Non posso procedere con la creazione della sessione.")
                lRtn = False
            Else
                If Not _l102 And lRtn Then
                    lRtn = MessageBox.Show("Attenzione! Non � stato indicato un file valido per le credenziali Web. Procedo ugualmente con la creazione della sessione?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes
                End If
            End If
        Else
            If Not _l102 And lRtn Then
                lRtn = MessageBox.Show("Attenzione! Non � stato indicato un file valido per le credenziali Web. Procedo ugualmente con la creazione della sessione?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes
            End If
        End If
        Return lRtn

    End Function

    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.GetSelectedRow
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow
        Dim aResult As String()
        Dim cFilter As String

        Select Case _SelectFromPlugInType
            Case Is = "tsm_ric_progressivo"
                cFilter = "MAI_codice"
            Case Else
                cFilter = ""
        End Select
        aResult = New String() {"MAI_codice", ""}
        For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
            If dr_mai_qd(cFilter) = cValue Then
                aResult(1) = dr_mai_qd.MAI_codice
            End If
        Next
        Return aResult

    End Function

    Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CreaDataBaseSessione
        Dim mng_giri As FC_manage_giri
        Dim xdWork1 As Xml.XmlDocument
        Dim xdWork2 As Xml.XmlDocument
        Dim xiRecord As Xml.XmlNode
        Dim xiWork1 As Xml.XmlNode
        Dim xiWork2 As Xml.XmlNode
        Dim cSortValue As String
        Dim cToponimo As String
        Dim nRecord As Integer
        Dim aValue As String()
        Dim ds_src As DataSet
        Dim dr_fat As DataRow
        Dim nGiro As Integer
        Dim cCitta As String
        Dim cTmp As String
        Dim ds_all As DataSet
        Dim nSogId As Integer
        Dim cFileNameAllegatoTipo As String

        _ResultAL = New ArrayList
        ' ************************************************************************** '
        ' Copia i file dei dati dalla cartella dove sono attualmente nella cartella  '
        ' dati della sessione.                                                       '
        ' ************************************************************************** '
        ImpostaMessaggi(New String() {"", "", "", "0", _DatiSessione.Count, "Elaborazione dei file da caricare...."})
        cFileNameAllegatoTipo = ""
        For Each aValue In _DatiSessione
            ImportaFileInSessione(aValue(0), aValue(1))
            If aValue(0) = 190 Then
                cFileNameAllegatoTipo = aValue(1)
            End If
        Next

        ' ************************************************************************** '
        ' Caricamento dei dati dei giri che eventualmente sono stati selezionati.    '
        ' ************************************************************************** '
        _NuoviToponimi = False
        ImpostaMessaggi(New String() {"", "", "", "0", 1, "Caricamento dei dati dei giri di consegna..."})
        mng_giri = New FC_manage_giri(New OleDb.OleDbConnection(_DbGiriConnString))
        mng_giri.ListaGiri = _ListaGiri

        ' ************************************************************************** '
        ' Creazione del file indice: creazione e ordinamento dei record recuperati e '
        ' salvataggio dei nuovi toponimi rintracciati.                               '
        ' ************************************************************************** '
        ImpostaMessaggi(New String() {"", "", "", "0", _ResultAL.Count, "Elaborazione dei file della cartella di archiviazione..."})
        xdWork2 = New Xml.XmlDocument
        xdWork2.AppendChild(xdWork2.CreateNode(Xml.XmlNodeType.Element, "FATTURA", ""))
        ds_all = CaricaFileDatiToDS_excel(cFileNameAllegatoTipo)
        For Each aValue In _ResultAL
            If (aValue(3) = 1) And (aValue(4) = 0) Then
                Try
                    ds_src = caricaFileDatiDS(String.Concat(_SessionePath, aValue(1)))
                    'ds_src = New DataSet
                    'ds_src.ReadXml(String.Concat(_SessionePath, aValue(1)))
                    For Each dr_fat In ds_src.Tables("Fattura").Rows
                        nSogId = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "IDSoggetto")
                        xiRecord = xdWork2.CreateNode(Xml.XmlNodeType.Element, "FATTURA_ROW", "")
                        SetRecordIDX(xdWork2, xiRecord, "MAI_num_utente", nSogId)
                        SetRecordIDX(xdWork2, xiRecord, "MAI_num_fattura", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "NumeroFattura"))
                        SetRecordIDX(xdWork2, xiRecord, "MAI_nominativo", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "RagioneSociale", ""))
                        SetRecordIDX(xdWork2, xiRecord, "MAI_IDFattura", dr_fat("IDFattura"))

                        cCitta = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Comune")
                        cToponimo = String.Concat(RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "TipoElementoTopologico"), " ", _
                                                RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ElementoTopologico", ""))

                        SetRecordIDX(xdWork2, xiRecord, "MAI_indirizzo_recapito", String.Concat(RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "TipoElementoTopologico"), " ", _
                                                                                                RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ElementoTopologico", ""), " ", _
                                                                                                RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NumeroCivico", ""), " ", _
                                                                                                RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "SuffissoCivico", ""), " ", _
                                                                                                RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Localita", "")))
                        SetRecordIDX(xdWork2, xiRecord, "MAI_citta_recapito", cCitta)
                        SetRecordIDX(xdWork2, xiRecord, "MAI_cap_recapito", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Cap"))
                        SetRecordIDX(xdWork2, xiRecord, "MAI_provincia_recapito", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ProvinciaSigla"))
                        SetRecordIDX(xdWork2, xiRecord, "MAI_nazione_recapito", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NazioneSigla"))
                        SetRecordIDX(xdWork2, xiRecord, "MAI_print_bol", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "TipoPagamento", "BollettinoPostale", "no"))
                        If ds_all Is Nothing Then
                            SetRecordIDX(xdWork2, xiRecord, "MAI_print_all", "si")
                        Else
                            If ds_all.Tables(0).Select(String.Concat("SOG_ID = ", nSogId)).Length = 1 Then
                                SetRecordIDX(xdWork2, xiRecord, "MAI_print_all", "si")
                            Else
                                SetRecordIDX(xdWork2, xiRecord, "MAI_print_all", "no")
                            End If
                        End If
                        SetRecordIDX(xdWork2, xiRecord, "MAI_filedati", aValue(2))
                        If RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "Protocollo") = cns_prot_reg_iva_corr Then
                            SetRecordIDX(xdWork2, xiRecord, "DEF_errcode", "0")
                            SetRecordIDX(xdWork2, xiRecord, "DEF_errdesc", "")
                        Else
                            SetRecordIDX(xdWork2, xiRecord, "DEF_errcode", "-3")
                            SetRecordIDX(xdWork2, xiRecord, "DEF_errdesc", String.Concat("Protocollo registro IVA non corretto ", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "Protocollo"), " invece di ", cns_prot_reg_iva_corr, "."))
                        End If
                        SetRecordIDX(xdWork2, xiRecord, "DEF_sendbymail", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "IndirizzoEmailInvioFattura").ToString.Contains("@"))
                        SetRecordIDX(xdWork2, xiRecord, "DEF_onlyarchott", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "TipoStampa", "DOPPIO INVIO FATTURA", "NO").ToString.ToUpper = "NO")

                        ' ************************************************************************** '
                        ' Impostazione della stringa per l'ordinamento. La richiesta di Bonato � sta '
                        ' ta:                                                                        '
                        ' Citta - Via - NumeroCivico Pari o dispari - numero civico - suffisso -     '
                        ' Ragione sociale.                                                           '
                        ' ************************************************************************** '
                        nGiro = mng_giri.RicercaGiroPLUGIN(cCitta, cToponimo)
                        _NuoviToponimi = _NuoviToponimi Or mng_giri.NuovoToponimo
                        cSortValue = nGiro.ToString.PadLeft(6, "0")
                        cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Comune"))
                        cTmp = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NumeroCivico", "")
                        cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "TipoElementoTopologico"), " ", RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ElementoTopologico", ""))
                        If IsNumeric(cTmp) Then
                            If (cTmp Mod 2) = 0 Then
                                cSortValue = String.Concat(cSortValue, "P")
                            Else
                                cSortValue = String.Concat(cSortValue, "D")
                            End If
                            ' Inverte l'ordinamento per numero civico, dal pi� grande al pi� piccolo.
                            cTmp = 1000000000 - Integer.Parse(cTmp)
                        Else
                            cSortValue = String.Concat(cSortValue, "N")
                        End If
                        cSortValue = String.Concat(cSortValue, cTmp.PadLeft(10, "0"))
                        cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "SuffissoCivico").PadLeft(6, " "))
                        cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "RagioneSociale", "")).Replace(" ", "")
                        SetRecordIDX(xdWork2, xiRecord, "SORTFIELD", cSortValue)
                        xdWork2.SelectSingleNode("FATTURA").AppendChild(xiRecord)
                    Next
                    aValue(4) = 1
                Catch ex As Exception
                    aValue(4) = -1
                Finally
                    xdWork1 = Nothing
                End Try
            Else
                aValue(4) = 1
            End If
            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
        Next

        ' ************************************************************************** '
        ' ************************************************************************** '
        ImpostaMessaggi(New String() {"", "", "", "0", 1, "Salvataggio nuove vie/toponimi nel database..."})
        Try
            mng_giri.InserisciCittaToponimiPLUGIN()
            mng_giri = Nothing
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


        ' ************************************************************************** '
        ' ************************************************************************** '
        _MessaggiAvanzamento(5) = "Ordinamento dei record in corso..."
        FC_SortFile.SortFileIndiceXML(xdWork2, 0, xdWork2.SelectSingleNode("FATTURA").ChildNodes.Count - 1)

        ' ************************************************************************** '
        ' Rimuove il campo utilizzato per l'ordinamento e genera il file che servir� ' 
        ' per l'invio delle fatture via mail.                                        '
        ' ************************************************************************** '
        nRecord = 1
        For Each xiWork1 In xdWork2.SelectNodes("FATTURA/FATTURA_ROW/SORTFIELD")
            xiWork2 = xiWork1.ParentNode
            xiWork2.RemoveChild(xiWork1)
            SetRecordIDX(xdWork2, xiWork2, "MAI_codice", nRecord)
            nRecord += 1
        Next

        ' ************************************************************************** '
        ' Salva il file indice nella sessione.                                       '
        ' ************************************************************************** '
        xdWork2.Save(String.Concat(_SessionePath, "\dati\indice.xml"))
        _ResultAL.Add(New String() {"filename", "dati\indice.xml", "0", "0", "1"})
        xdWork2 = Nothing

    End Sub

    Sub GoToRecordNumber(ByVal nRecord As Integer) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.GoToRecordNumber

        _RecordCount = nRecord

    End Sub

    Sub ImpostaDGVDati(ByVal DGV As DataGridView) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ImpostaDGVDati

        With DGV.Columns
            .Clear()
            If _TipoCaricamento = 1 Then
                .Add(Col_DaStampare)
            End If
            If _TipoCaricamento = 2 Then
                .Add(Col_DaInviare)
            End If
            .Add(Col_Errore_code)
            .Add(Col_Errore_desc)
            .Add(Col_Progressivo)
            .Add(Col_NumeroUtente)
            .Add(Col_NumeroFattura)
            .Add(Col_Nominativo)
            .Add(COL_Indirizzo_Recapito)
            .Add(COL_CAP_Recapito)
            .Add(COL_Citta_Recapito)
            .Add(COL_Provincia_Recapito)
            .Add(COL_Nazione_Recapito)
            .Add(COL_Print_Bollettino)
            .Add(COL_Print_allegato)
            .Add(COL_SpedByMail)
        End With

    End Sub

    ' ************************************************************************** '
    ' Questa funzione si occupa di caricare il data set che guida la stampa o    '
    ' l'invio:                                                                   '    
    ' 1 - Caricamento dei record per la stampa: carica tutti i record            '
    ' 2 - Caricamento dei record per l'invio: carica solo i record con           '
    '     DEF_sendbymail = TRUE                                                  '
    ' ************************************************************************** '
    Sub LoadQuickDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LoadQuickDataset
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow
        Dim lLoad As Boolean

        _QuickDataset = New DS_QD_UE_ele
        _xmlDati_qd = New Xml.XmlDocument
        _xmlDati_qd.Load(QuickDataSet_IndexFile)
        ImpostaMessaggi(New String() {"", "", "", "0", _xmlDati_qd.SelectNodes(Costanti.itm_qd_Fattura).Count, "Caricamento dei dati...."})

        For Each xmlItem As Xml.XmlNode In _xmlDati_qd.SelectNodes(Costanti.itm_qd_Fattura)
            lLoad = _TipoCaricamento = 1 Or (_TipoCaricamento = 2 And CBool(GetValueFromXML(xmlItem, "DEF_sendbymail")))
            If lLoad Then
                dr_mai_qd = _QuickDataset.TBL_main.NewRow
                Try
                    dr_mai_qd.MAI_codice = GetValueFromXML(xmlItem, "MAI_codice")
                    dr_mai_qd.DEF_toprint = False
                    dr_mai_qd.DEF_errcode = GetValueFromXML(xmlItem, "DEF_errcode")
                    dr_mai_qd.DEF_errdesc = GetValueFromXML(xmlItem, "DEF_errdesc")
                    dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0) Or (dr_mai_qd.DEF_errcode = -2)
                    dr_mai_qd.DEF_sendbymail = GetValueFromXML(xmlItem, "DEF_sendbymail")
                    dr_mai_qd.DEF_onlyarchott = GetValueFromXML(xmlItem, "DEF_onlyarchott")
                    dr_mai_qd.MAI_num_utente = GetValueFromXML(xmlItem, "MAI_num_utente")
                    dr_mai_qd.MAI_num_fattura = GetValueFromXML(xmlItem, "MAI_num_fattura")
                    dr_mai_qd.MAI_nominativo = GetValueFromXML(xmlItem, "MAI_nominativo")
                    dr_mai_qd.MAI_indirizzo_recapito = GetValueFromXML(xmlItem, "MAI_indirizzo_recapito")
                    dr_mai_qd.MAI_citta_recapito = GetValueFromXML(xmlItem, "MAI_citta_recapito")
                    dr_mai_qd.MAI_cap_recapito = GetValueFromXML(xmlItem, "MAI_cap_recapito")
                    dr_mai_qd.MAI_provincia_recapito = GetValueFromXML(xmlItem, "MAI_provincia_recapito")
                    dr_mai_qd.MAI_nazione_recapito = GetValueFromXML(xmlItem, "MAI_nazione_recapito")
                    dr_mai_qd.MAI_print_bol = GetValueFromXML(xmlItem, "MAI_print_bol").ToLower = "si"
                    dr_mai_qd.MAI_print_all = GetValueFromXML(xmlItem, "MAI_print_all").ToLower = "si"
                    dr_mai_qd.MAI_filedati = GetValueFromXML(xmlItem, "MAI_filedati")
                    dr_mai_qd.MAI_IDFattura = GetValueFromXML(xmlItem, "MAI_IDFattura")
                Catch ex As Exception
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = ex.Message
                End Try
                ImpostaMessaggi(New String() {"", "", "", CInt(_MessaggiAvanzamento(3)) + 1, "", ""})
                _QuickDataset.TBL_main.Rows.Add(dr_mai_qd)
            End If
        Next

    End Sub

    Sub LoadFullDataset(ByVal nRecordToLoad As Integer) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LoadFullDataset
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow
        Dim nRecordToPrint As Integer

        ' ************************************************************************** '
        ' Creazione del Dataset e azzeramento del contatore dei record da stampare.  '
        ' ************************************************************************** '
        nRecordToPrint = 0
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                _PrintFatturaDataset = New DS_PF_UE_ele
            Case Is = ListaDataset.DAS_bollettino
                _PrintBollettinoDataset = New DS_PB_UE_ele
            Case Is = ListaDataset.DAS_send_email
                _SendEmailDataset = New DS_SE_UE_ele
        End Select

        ' ************************************************************************** '
        ' Caricamento del DataSet da utilizzare per la stampa.                       '
        ' ************************************************************************** '
        _CodiceProgressivo = 0
        While _RecordCount < _QuickDataset.TBL_main.Rows.Count And nRecordToPrint < nRecordToLoad
            dr_mai_qd = _QuickDataset.TBL_main.Rows(_RecordCount)
            If dr_mai_qd.DEF_toprint Then
                Try
                    _ErroriLievi = ""
                    _ErroriGravi = ""
                    AddRecordToDS(dr_mai_qd)
                    'If _CodiceModulo = ListaDataset.DAS_fattura Or _CodiceModulo = ListaDataset.DAS_bollettino Then
                    '    AddRecordToPrint(dr_mai_qd)
                    'ElseIf _CodiceModulo = ListaDataset.DAS_send_email Then
                    '    AddRecordToSend(dr_mai_qd)
                    'End If
                    If _ErroriGravi > "" Then
                        dr_mai_qd.DEF_errcode = -1
                        dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori Gravi: ", _ErroriGravi)
                    ElseIf _ErroriLievi > "" Then
                        dr_mai_qd.DEF_errcode = -2
                        dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori lievi: ", _ErroriLievi)
                    Else
                        dr_mai_qd.DEF_errcode = 0
                        dr_mai_qd.DEF_errdesc = ""
                    End If
                Catch ex As Exception
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = ex.Message
                End Try
                nRecordToPrint += 1
            End If
            _RecordCount += 1
        End While

        ' ************************************************************************** '
        ' Consolidamento del DataSet da utilizzare per la stampa.                    '
        ' ************************************************************************** '
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                _PrintFatturaDataset.AcceptChanges()
            Case Is = ListaDataset.DAS_bollettino
                _PrintBollettinoDataset.AcceptChanges()
            Case Is = ListaDataset.DAS_send_email
                _SendEmailDataset.AcceptChanges()
        End Select

    End Sub

    Sub PostPostAnteprima(ByVal aLista As ArrayList) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.PostAnteprima
        Dim dr_elp As DS_PF_UE_ele.TBL_elenco_podRow
        Dim nPagine As Integer
        Dim nElencoPod As Integer
        Dim nStatusRaccolta As Integer
        Dim i As Integer

        If Not _PrintFatturaDataset Is Nothing Then
            nElencoPod = -1
            For i = 0 To aLista.Count - 1
                nStatusRaccolta = CType(_PrintFatturaDataset.TBL_main.Rows(i), DS_PF_UE_ele.TBL_mainRow).DEF_raccolta
                Select Case nStatusRaccolta
                    Case Is = 1
                        nPagine = aLista(i)
                    Case Is = 2, 3
                        nElencoPod += 1
                        dr_elp = _PrintFatturaDataset.TBL_elenco_pod.Rows(nElencoPod)
                        dr_elp.ELP_page_num = nPagine + 1
                        dr_elp.AcceptChanges()
                        nPagine += aLista(i)
                End Select
            Next
        End If

    End Sub

    Sub RemovePRNDSErrorRecord() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.RemovePRNDSErrorRecord
        Dim nLinkCode As Integer

        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                For Each dr_mai_pd As DS_PF_UE_ele.TBL_mainRow In _PrintFatturaDataset.TBL_main.Rows
                    nLinkCode = dr_mai_pd(LinkField_QuickDS_FullDS)
                    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                        dr_mai_pd.Delete()
                    End If
                Next
                _PrintFatturaDataset.TBL_main.AcceptChanges()
            Case Is = ListaDataset.DAS_bollettino
                For Each dr_mai_pd As DS_PB_UE_ele.TBL_fat_x_ratRow In _PrintBollettinoDataset.TBL_fat_x_rat.Rows
                    nLinkCode = dr_mai_pd(LinkField_QuickDS_FullDS)
                    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                        dr_mai_pd.Delete()
                    End If
                Next
                _PrintBollettinoDataset.TBL_fat_x_rat.AcceptChanges()
        End Select

    End Sub

    ' ************************************************************************** '
    ' Imposto correttamente la parte di selezione per la stampa dei record. Se   '
    ' non dobbiamo mantenere la selezione o se la dobbiamo mantenere ma il re-   ' 
    ' cord non � stampabile metto a "NON STAMPABILE (False)" il campo .          '
    ' ************************************************************************** '
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetSelectable
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_UE_ele.TBL_mainRow)
        dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0) Or (dr_mai_qd.DEF_errcode = -2)
        If cFilter > "" Then
            dr_mai_qd.DEF_selectable = dr_mai_qd.DEF_selectable And dr_mai_qd(cFilter)
        End If

        ' ************************************************************************** '
        ' Pulisce i campi relativi agli errori in quanto ci stiamo occupando di un   '
        ' nuovo modulo.                                                              '
        ' ************************************************************************** '
        If dr_mai_qd.DEF_selectable Then
            dr_mai_qd.Item("DEF_errcode") = 0
            dr_mai_qd.Item("DEF_errdesc") = ""
        End If

    End Sub

    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String, ByVal cParamSelez As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetPrintable
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_UE_ele.TBL_mainRow)
        Select Case _SelectFromPlugInType
            Case Is = cns_tsm_sel_con_bollettino.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_bol
            Case Is = cns_tsm_sel_con_allegato.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_all
        End Select

    End Sub

    Sub UpdateQuickDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.UpdateQuickDatasetFile
        Dim dr_prd As DS_PF_UE_ele.TBL_mainRow
        Dim xnTmp As Xml.XmlNode
        Dim nLinkCode As Integer

        If _CodiceModulo = ListaDataset.DAS_fattura Then
            _xmlDati_qd = New Xml.XmlDocument
            _xmlDati_qd.Load(QuickDataSet_IndexFile)
            For Each dr_prd In _PrintFatturaDataset.TBL_main
                nLinkCode = dr_prd(LinkField_QuickDS_FullDS)
                xnTmp = _xmlDati_qd.SelectSingleNode(String.Concat(Costanti.itm_qd_Fattura, "/MAI_codice[.=", _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).MAI_codice, "]")).ParentNode
                xnTmp.SelectSingleNode("DEF_errcode").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode
                xnTmp.SelectSingleNode("DEF_errdesc").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errdesc
            Next
            _xmlDati_qd.Save(QuickDataSet_IndexFile)
            _xmlDati_qd.RemoveAll()
            GC.Collect()
            _xmlDati_qd = Nothing
        End If

    End Sub

#End Region

    ' ************************************************************************** '
    ' Tutte le properit� di seguito sono private e non fanno parte dell'interfac '
    ' cia.                                                                       '
    ' ************************************************************************** '
#Region "Elenco delle property Private"

    Private ReadOnly Property PrintDataSet_DataFile(ByVal _FileIdx As Integer)

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode(String.Concat("filename[@codice=", "", _FileIdx, "", "]")).InnerText)
        End Get

    End Property

    Private ReadOnly Property QuickDataSet_IndexFile() As String

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode("filename[@tipo=""0""]").InnerText)
        End Get

    End Property

#End Region

    ' ************************************************************************** '
    ' Tutti i metodi di seguito sono private e non fanno parte dell'interfaccia. '
    ' ************************************************************************** '
#Region "Elenco dei metodi Private"

    Private Sub SetRecordIDX(ByVal xmlIndice As Xml.XmlDocument, ByVal xmlRecord As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xmlIndice.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xmlRecord.AppendChild(xiTmp)

    End Sub

#End Region

#Region "SetUp per Datagrid di visualizzazione delle righe da stampare"

    Private Function Col_DaStampare() As System.Windows.Forms.DataGridViewCheckBoxColumn
        Dim col As System.Windows.Forms.DataGridViewCheckBoxColumn

        col = New System.Windows.Forms.DataGridViewCheckBoxColumn
        With col
            .HeaderText = "Stampa"
            .DataPropertyName = "DEF_toprint"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .Width = 50
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_DaInviare() As System.Windows.Forms.DataGridViewCheckBoxColumn
        Dim col As System.Windows.Forms.DataGridViewCheckBoxColumn

        col = New System.Windows.Forms.DataGridViewCheckBoxColumn
        With col
            .HeaderText = "Invia"
            .DataPropertyName = "DEF_toprint"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .Width = 50
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Errore_code() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errcode"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Errore_desc() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errdesc"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Progressivo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Progressivo"
            .DataPropertyName = "MAI_codice"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 70
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroUtente() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Utente"
            .DataPropertyName = "MAI_num_utente"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 90
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroFattura() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Fattura"
            .DataPropertyName = "MAI_num_fattura"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 110
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Nominativo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nominativo"
            .DataPropertyName = "MAI_nominativo"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Indirizzo_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Indirizzo Recapito"
            .DataPropertyName = "MAI_indirizzo_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_CAP_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "CAP"
            .DataPropertyName = "MAI_cap_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Citta_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Citt�"
            .DataPropertyName = "MAI_citta_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Provincia_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Prov."
            .DataPropertyName = "MAI_provincia_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Nazione_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nazione"
            .DataPropertyName = "MAI_nazione_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Print_Bollettino()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Bollettino"
            .DataPropertyName = "MAI_print_bol"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_Print_allegato()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Allegato"
            .DataPropertyName = "MAI_print_all"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_SpedByMail()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Sped. Email"
            .DataPropertyName = "DEF_sendbymail"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

#End Region

    Private Function RecuperaPeriodoRiferimento(ByVal xiWork As Xml.XmlNode) As String
        Dim dTmp1 As DateTime
        Dim dTmp2 As DateTime
        Dim cRtn As String

        cRtn = ""
        dTmp1 = GetValueFromXML(xiWork, Costanti.itm_data_ini_fat)
        dTmp2 = GetValueFromXML(xiWork, Costanti.itm_data_fin_fat)
        If dTmp1.Year = dTmp2.Year And dTmp1.Month = dTmp2.Month Then
            cRtn = String.Concat(FC_utilita.TrasformaMese(dTmp1.Month), " ", dTmp1.Year)
        Else
            If dTmp1.Year = dTmp2.Year Then
                cRtn = String.Concat(FC_utilita.TrasformaMese(dTmp1.Month), " ", FC_utilita.TrasformaMese(dTmp2.Month), " ", dTmp1.Year)
            Else
                cRtn = String.Concat(FC_utilita.TrasformaMese(dTmp1.Month), " ", dTmp1.Year, " ", FC_utilita.TrasformaMese(dTmp2.Month), " ", dTmp2.Year)
            End If
        End If
        Return cRtn

    End Function

    Private Sub BimestreFatturazione_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As FRM_bimestre

        Try
            frm = New FRM_bimestre
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_bimestre.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "14")
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, "14", frm.TXB_bimestre.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il valore richiesto.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Function GetValueDatiFissi(ByVal cTable As String, ByVal cField As String) As String
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim cRtn As String

        Try
            xmlDF.Load(_SessioneDatiFissiFileName)
            xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))

            cRtn = xmlTmp.InnerText
            cRtn = cRtn.Replace("{\TBL_main;MAI_punto_fornitura/}", "&PUNTO_FORNITURA&")
            cRtn = cRtn.Replace("{\TBL_strum_misura;STM_matricola/}", "&MATRICOLA_CONT&")
            xmlDF = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    Private Sub SetValueDatiFissi(ByVal cTable As String, ByVal cField As String, ByVal cValue As String)
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim xaTmp As Xml.XmlAttribute
        Dim xnWork2 As Xml.XmlNode

        cValue = cValue.Replace("&PUNTO_FORNITURA&", "{\TBL_main;MAI_punto_fornitura/}")
        cValue = cValue.Replace("&MATRICOLA_CONT&", "{\TBL_strum_misura;STM_matricola/}")

        xmlDF.Load(_SessioneDatiFissiFileName)
        xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))
        If Not xmlTmp Is Nothing Then
            xmlTmp = xmlTmp.SelectSingleNode("valore")
            xmlTmp.InnerText = cValue
        Else
            'Crea il nodo
            xmlTmp = xmlDF.CreateElement("item")
            xaTmp = xmlDF.CreateAttribute("codice")
            xaTmp.Value = cField
            xmlTmp.Attributes.Append(xaTmp)
            xnWork2 = xmlDF.CreateElement("valore")
            xnWork2.InnerText = cValue
            xmlTmp.AppendChild(xnWork2)
            xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable)).AppendChild(xmlTmp)
        End If
        xmlDF.Save(_SessioneDatiFissiFileName)
        xmlDF = Nothing

    End Sub

    Private Sub AllegatiNot_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        For Each dr_mai_qd As DS_QD_UE_ele.TBL_mainRow In QuickDataset.Tables(0).Rows
            dr_mai_qd.Item("MAI_print_all") = False
        Next

    End Sub

    Private Sub AllegatiAll_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        For Each dr_mai_qd As DS_QD_UE_ele.TBL_mainRow In QuickDataset.Tables(0).Rows   '   _QuickDataset.Tables(PlugInMainTable(ListaModuli.MOD_quickdataset)).Rows
            dr_mai_qd.Item("MAI_print_all") = True
        Next

    End Sub

    Private Sub SetAllegato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New FRM_set_allegato

        frm.StartPosition = FormStartPosition.CenterParent
        frm.Show()

    End Sub

    Private Sub StampaAutolettura_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
        If CType(sender, ToolStripMenuItem).Checked Then
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa, "si")
        Else
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa, "no")
        End If

    End Sub

    Private Sub StampaBoxCommerciale_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
        If CType(sender, ToolStripMenuItem).Checked Then
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale, "si")
        Else
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale, "no")
        End If

    End Sub

    Private Sub ForzaOffertaScacciapensieri_TSM_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
        If CType(sender, ToolStripMenuItem).Checked Then
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "si")
        Else
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "no")
        End If

    End Sub

    Private Sub ForzaOffertaScacciapensieri_TSB_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si" Then
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "no")
        Else
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "si")
        End If
        SetButtonForzaturaScacciapensieri(sender)

    End Sub

    Private Sub SetButtonForzaturaScacciapensieri(ByVal tsb As ToolStripButton)

        If tsb.Text.EndsWith(" Si") Then
            tsb.Text = tsb.Text.Replace(" Si", "")
        ElseIf tsb.Text.EndsWith(" No") Then
            tsb.Text = tsb.Text.Replace(" No", "")
        End If
        If GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si" Then
            tsb.Image = Global.PLG_UE_elettrico_tut.My.Resources.bullet_green
            tsb.Text = String.Concat(tsb.Text, " Si")
        Else
            tsb.Image = Global.PLG_UE_elettrico_tut.My.Resources.bullet_red
            tsb.Text = String.Concat(tsb.Text, " No")
        End If

    End Sub

    Private Sub TestoAutolettura_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_autolettura

        Try
            frm = New FRM_autolettura
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_autolettura.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_testo)
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_testo, frm.TXB_autolettura.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo dell'autolettura", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ForzaDataScadenza_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_forza_scad

        Try
            frm = New FRM_forza_scad
            frm.StartPosition = FormStartPosition.CenterParent
            frm.Condizione = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "17")
            frm.TXB_data_scad.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "16")
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, "16", frm.TXB_data_scad.Text)
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, "17", frm.Condizione)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare i dati per la forzatura della data di scadenza", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub DatiFissi_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As FRM_datifissi

        Try
            frm = New FRM_datifissi
            frm.StartPosition = FormStartPosition.CenterParent
            frm.FileNameDatiFissi = _SessioneDatiFissiFileName
            If frm.ShowDialog = DialogResult.OK Then

            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il valore richiesto.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub LoadDSLog()
        Dim dr As DataRow

        For Each dr_qds As DS_QD_UE_ele.TBL_mainRow In _QuickDataset.TBL_main.Rows
            If dr_qds.DEF_errcode < 0 Then
                dr = _LogDS.Tables(0).NewRow
                dr("LOG_desc_rec_1") = String.Concat("Progressivo: ", dr_qds.MAI_codice, " Nominativo: ", dr_qds.MAI_nominativo, " Numero Fattura: ", dr_qds.MAI_num_fattura)
                dr("LOG_stato") = dr_qds.DEF_errcode
                dr("LOG_desc_err") = dr_qds.DEF_errdesc
                _LogDS.Tables(0).Rows.Add(dr)
            End If
        Next

    End Sub

#Region "Procedure utili per la creazione della sessione di stampa"

    Sub ImportaFileInSessione(ByVal nTipoFile As Integer, ByVal cFileName As String)
        Dim nRecordtoAnalized As Integer
        Dim xdWork1 As Xml.XmlDocument
        Dim xdWork2 As Xml.XmlDocument
        Dim nRecordXFile As Integer
        Dim xiWork As Xml.XmlNode
        Dim cDstFile As String
        Dim nRecord As Integer
        Dim nFile As Integer
        Dim nDim As Integer
        Dim xnItem1 As Xml.XmlNode
        Dim xnItem2 As Xml.XmlNode

        ' Nel caso di tipo file 1 allora si tratta di un file xml, indipendentemente dall'estensione 
        ' da suddividere eventualmente in file di pi� piccole dimensioni.
        If nTipoFile = 1 Then
            Try
                xdWork1 = caricaFileDatiXML(cFileName)
                For Each xnItem As Xml.XmlNode In xdWork1.SelectNodes("ElencoFattura/Fattura/SezioniDocumenti/DatiServizio/PuntoDiFornitura/GruppoMisura")
                    xnItem1 = xnItem.SelectSingleNode("GruppoMisura")
                    If Not xnItem1 Is Nothing Then
                        xnItem2 = xdWork1.CreateNode(Xml.XmlNodeType.Element, "GruppoMisura1", "")
                        xnItem2.InnerText = xnItem1.InnerText
                        xnItem.AppendChild(xnItem2.Clone)
                        xnItem.RemoveChild(xnItem1)
                        For Each xnItem3 As Xml.XmlNode In xnItem.SelectNodes("StrumentoMisura")
                            xnItem1 = xnItem3.SelectSingleNode("StrumentoMisura")
                            xnItem2 = xdWork1.CreateNode(Xml.XmlNodeType.Element, "StrumentoMisura1", "")
                            xnItem2.InnerText = xnItem1.InnerText
                            xnItem3.AppendChild(xnItem2.Clone)
                            xnItem3.RemoveChild(xnItem1)
                        Next
                    End If
                Next
                For Each xnItem As Xml.XmlNode In xdWork1.SelectNodes("ElencoFattura/Fattura/SezioniDocumenti/Contratto/OT")
                    xnItem1 = xnItem.SelectSingleNode("OT")
                    If Not xnItem1 Is Nothing Then
                        xnItem2 = xdWork1.CreateNode(Xml.XmlNodeType.Element, "OT1", "")
                        xnItem2.InnerText = xnItem1.InnerText
                        xnItem.AppendChild(xnItem2.Clone)
                        xnItem.RemoveChild(xnItem1)
                    End If
                Next
                SalvaFileDatiXML(cFileName, xdWork1)
                'xdWork1.Save(cFileName)
                nDim = New System.IO.FileInfo(cFileName).Length
                If (nDim < UpperFileSize) Then
                    _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
                    cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))
                    'xdWork1.Save(cDstFile)
                    SalvaFileDatiXML(cDstFile, xdWork1)
                    'System.IO.File.Copy(cFileName, cDstFile)
                    _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                    _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
                Else
                    _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                    _MessaggiAvanzamento(5) = "Suddivizione del file nella cartella di archiviazione..."

                    nRecordtoAnalized = xdWork1.SelectNodes("ElencoFattura/Fattura").Count
                    nRecordXFile = Int(LowerFileSize / (nDim / nRecordtoAnalized)) + 1
                    _MessaggiAvanzamento(4) = CInt(_MessaggiAvanzamento(4)) + Int(nRecordtoAnalized / nRecordXFile) + 1
                    nRecord = 0
                    nFile = 1
                    For Each xiWork In xdWork1.SelectNodes("ElencoFattura/Fattura")
                        If nRecord = 0 Then
                            xdWork2 = New Xml.XmlDocument
                            xdWork2.AppendChild(xdWork2.CreateNode(Xml.XmlNodeType.Element, "ElencoFattura", ""))
                        End If
                        xdWork2.SelectSingleNode("ElencoFattura").AppendChild(xdWork2.ImportNode(xiWork, True))
                        nRecord += 1
                        nRecordtoAnalized -= 1
                        If (nRecord = nRecordXFile) Or (nRecordtoAnalized = 0) Then
                            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                            cDstFile = cFileName.Substring(cFileName.LastIndexOf("\") + 1)
                            cDstFile = String.Concat(cDstFile.Substring(0, cDstFile.IndexOf(".")), "_", nFile.ToString.PadLeft(5, "0"), cDstFile.Substring(cDstFile.IndexOf(".")))
                            cDstFile = String.Concat(_SessioneDatiPath, cDstFile)
                            '  xdWork2.Save(cDstFile)
                            SalvaFileDatiXML(cDstFile, xdWork2)
                            xdWork2 = Nothing
                            _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
                            nRecord = 0
                            nFile += 1
                        End If
                    Next
                    xdWork1 = Nothing
                End If
            Catch ex As Exception
                MessageBox.Show(String.Concat("Attenzione il file ", cFileName, " non � correttamente formattato.", vbCr, "L'errore � riportato di seguito:", vbCr, ex.Message), "Errore di caricamento di un file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))
                System.IO.File.Copy(cFileName, cDstFile)
                _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, -1})
            End Try
        Else
            _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
            cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

            System.IO.File.Copy(cFileName, cDstFile)
            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
            _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
        End If

    End Sub

    'Private Sub SortFileIndice1(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer)
    '    Dim nPivot As Integer
    '    Dim k As Integer

    '    If nFst < nEnd Then
    '        nPivot = (nFst + nEnd) / 2
    '        k = dividi(xiWork, nFst, nEnd, nPivot)
    '        SortFileIndice1(xiWork, nFst, k - 1)
    '        SortFileIndice1(xiWork, k + 1, nEnd)
    '    End If

    'End Sub

    'Function dividi(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer, ByVal nPivot As Integer) As Integer
    '    Dim i As Integer = nFst
    '    Dim j As Integer = nEnd
    '    Dim xiWork_1 As Xml.XmlNode
    '    Dim xiWork_2 As Xml.XmlNode
    '    Dim nPivotPlace As Integer

    '    If nPivot < nEnd Then
    '        ' sposto il valore del pivot alla fine dell file xml
    '        swapItem(xiWork, nPivot, nEnd)
    '    End If

    '    xiWork_1 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nEnd).SelectSingleNode("SORTFIELD")
    '    nPivotPlace = nFst
    '    For i = nFst To nEnd - 1
    '        xiWork_2 = xiWork.SelectSingleNode("FATTURA").ChildNodes(i).SelectSingleNode("SORTFIELD")
    '        If (xiWork_2.InnerText <= xiWork_1.InnerText) Then
    '            If i <> nPivotPlace Then
    '                swapItem(xiWork, nPivotPlace, i)
    '            End If
    '            nPivotPlace += 1
    '        End If
    '    Next
    '    If nPivotPlace < nEnd Then
    '        swapItem(xiWork, nPivotPlace, nEnd)
    '    End If
    '    Return nPivotPlace

    'End Function

    'Private Sub swapItem(ByVal xiWork As Xml.XmlDocument, ByVal nIdx1 As Integer, ByVal nIdx2 As Integer)
    '    Dim xiWork_1 As Xml.XmlNode
    '    Dim xiWork_2 As Xml.XmlNode
    '    Dim xiWork_3 As Xml.XmlNode

    '    xiWork_1 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx1)
    '    xiWork_2 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx2)
    '    xiWork.SelectSingleNode("FATTURA").RemoveChild(xiWork_1)
    '    xiWork.SelectSingleNode("FATTURA").RemoveChild(xiWork_2)
    '    xiWork_3 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx1 - 1)
    '    xiWork.SelectSingleNode("FATTURA").InsertAfter(xiWork_2, xiWork_3)
    '    xiWork_3 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx2 - 1)
    '    xiWork.SelectSingleNode("FATTURA").InsertAfter(xiWork_1, xiWork_3)

    'End Sub

#End Region

    Private Sub AddRecordToDS(ByVal dr_mai_qd As DS_QD_UE_ele.TBL_mainRow)
        Dim ds_src As DataSet
        Dim dr_fat As DataRow
        Dim aDR_fat As DataRow()

        Try
            ds_src = caricaFileDatiDS(PrintDataSet_DataFile(dr_mai_qd.MAI_filedati))
            aDR_fat = ds_src.Tables("Fattura").Select(String.Concat("IDFattura = '", dr_mai_qd.MAI_IDFattura, "'"))
            If aDR_fat.Length > 0 Then
                dr_fat = aDR_fat(0)
                FatturaInGestione = dr_fat
                Select Case _CodiceModulo
                    Case Is = ListaDataset.DAS_fattura
                        GetRecordFatturaToPrint(dr_mai_qd.MAI_codice, ds_src, dr_fat, GetFeedString(dr_mai_qd))
                    Case Is = ListaDataset.DAS_bollettino
                        If dr_mai_qd.MAI_print_bol Then
                            GetRecordBollettiniToPrint(dr_mai_qd.MAI_codice, ds_src, dr_fat, GetFeedString(dr_mai_qd))
                        End If
                    Case Is = ListaDataset.DAS_send_email
                        GetRecordEmailToSend(dr_mai_qd.MAI_codice, ds_src, dr_fat)
                End Select
            End If
            ds_src = Nothing
        Catch ex As Exception
            Throw SetExceptionItem("AddRecordToPrint", ex)
        End Try

    End Sub

#Region "Caricamento del PRINTDATASET"

    Private Function GetFeedString(ByVal dr_mai_qd As DS_QD_UE_ele.TBL_mainRow) As String
        Dim lAllModuli As Boolean = True
        Dim cRtn As String

        cRtn = ""
        If _FeedString > "" Then
            For i As Integer = 1 To _FeedString.Split(";").Length - 1 Step 2
                If dr_mai_qd.Item(_FeedString.Split(";")(i)) Then
                    cRtn = _FeedString.Split(";")(i + 1)
                Else
                    lAllModuli = False
                End If
            Next
            If lAllModuli Then
                cRtn = _FeedString.Split(";")(0)
            End If
        End If
        Return cRtn

    End Function

#End Region

#Region "Caricamento del PRINTDATSET della fattura"

    Private Sub GetRecordFatturaToPrint(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal cFeedString As String)
        Dim dt_mai As DS_PF_UE_ele.TBL_mainDataTable
        Dim dr_mai As DS_PF_UE_ele.TBL_mainRow
        Dim dr_mai_tmp As DS_PF_UE_ele.TBL_mainRow
        Dim dr_daf As DS_PF_UE_ele.TBL_dati_fornRow
        Dim nSezDocumenti As Integer
        Dim nPodFatturati As Integer
        Dim nIdPuntoForn As Integer
        Dim aSortPdr As ArrayList
        Dim lOdl As Boolean
        Dim nRow As Integer
        Dim lPrimaUnica As Boolean
        ' Dim dr_sezione_documenti As DataRow

        ' ************************************************************************** '
        ' Creazione del record guida della stampa della fattura.                     '
        ' ************************************************************************** '
        _dr_mai_pf = _PrintFatturaDataset.TBL_main.NewRow
        _CodiceProgressivo += 1
        _dr_mai_pf.MAI_codice = _CodiceProgressivo
        _MasterRecord = _CodiceProgressivo
        _dr_mai_pf.MAI_linkQD = nCodice
        _dr_mai_pf.DEF_alimimb = cFeedString

        ' ************************************************************************** '
        ' Recuperiamo il numero dei pod per i quli si stamper� la fattura. Il campo  '
        ' di default DEF_raccolta indica se il record attuale fa parte di una raccol '
        ' ta. I valori che pu� assumere sono:                                        '
        ' ---- In caso di raccolta da pi� di un record:                              '
        '      - 1 primo record di una raccolta;                                     '
        '      - 2 record successivi della raccolta                                  '
        '      - 3 ultimo record della raccolta.                                     '
        ' ---- In caso di raccolta da pi� di un record:                              '
        '      - 9 raccolta costituita da un solo record                             '
        ' ************************************************************************** '
        nPodFatturati = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "NumeroPOD", "NumeroPOD")     'xiWork.SelectNodes(Costanti.itm_PDR_fatturati).Count
        Select Case nPodFatturati
            Case Is = 0
                _dr_mai_pf.DEF_raccolta = 9
                AddErroreLieve("Pod fatturati assenti")
            Case Is = 1
                _dr_mai_pf.DEF_raccolta = 9
            Case Else
                _dr_mai_pf.DEF_raccolta = 1
        End Select
        lPrimaUnica = True

        nSezDocumenti = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "NumeroSEZ", "NumeroSEZ")
        ' ************************************************************************** '
        ' Dati dell'intestatario della fattura.                                      ' 
        ' ************************************************************************** '
        _dr_mai_pf.MAI_cod_cli = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "IDSoggetto")
        _dr_mai_pf.MAI_nome = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "RagioneSociale")
        _dr_mai_pf.MAI_nome_brev = _dr_mai_pf.MAI_nome.Substring(0, Math.Min(25, _dr_mai_pf.MAI_nome.Length))
        _dr_mai_pf.MAI_cod_fis = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "CodiceFiscale")
        _dr_mai_pf.MAI_par_iva = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "PartitaIva")
        _dr_mai_pf.MAI_cod_gruppo = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "CodiceConsorzioAcquisto", "CONSORZIO/GRUPPO DI ACQUISTO")

        ' ************************************************************************** '
        ' Il codice fiscale e la partita iva sono dati alternativi. Non possono esse '
        ' re presenti allo stesso tempo entrambi.                                    '
        ' ************************************************************************** '
        If _dr_mai_pf.MAI_par_iva > "" Then
            _dr_mai_pf.MAI_cod_fis = ""
            AddErroreLieve("Codice fiscale valorizzato in maniera errata o uguale alla partita IVA")
        End If
        CheckFieldValue(String.Concat(_dr_mai_pf.MAI_cod_fis, _dr_mai_pf.MAI_par_iva), "Partita IVA o Codice Fiscale")

        ' ************************************************************************** '
        ' Indirizzo dell'intestatario.                                               '
        ' ************************************************************************** '
        _dr_mai_pf.MAI_int_sito_1 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "TipoElementoTopologico")
        _dr_mai_pf.MAI_int_sito_2 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ElementoTopologico", "")
        _dr_mai_pf.MAI_int_sito_3 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NumeroCivico", "")
        _dr_mai_pf.MAI_int_sito_4 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "SuffissoCivico", "")
        _dr_mai_pf.MAI_int_sito_5 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Localita", "")
        _dr_mai_pf.MAI_int_sito_6 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Comune")
        _dr_mai_pf.MAI_int_sito_7 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Cap")
        _dr_mai_pf.MAI_int_sito_8 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ProvinciaSigla")
        _dr_mai_pf.MAI_int_nazione = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NazioneSigla")

        ' ************************************************************************** '
        ' Indirizzo del recapito della fattura.                                      '
        ' ************************************************************************** '
        _dr_mai_pf.MAI_rec_nome = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiSpedizione", "Recapito", _dr_mai_pf.MAI_nome)
        If _dr_mai_pf.MAI_rec_nome = _dr_mai_pf.MAI_nome Then
            _dr_mai_pf.MAI_rec_nome = ""
        End If
        _dr_mai_pf.MAI_rec_sito_1 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "TipoElementoTopologico")
        _dr_mai_pf.MAI_rec_sito_2 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ElementoTopologico")
        _dr_mai_pf.MAI_rec_sito_3 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NumeroCivico")
        _dr_mai_pf.MAI_rec_sito_4 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "SuffissoCivico")
        _dr_mai_pf.MAI_rec_sito_5 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Localita")
        _dr_mai_pf.MAI_rec_sito_6 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Comune")
        _dr_mai_pf.MAI_rec_sito_7 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "Cap")
        _dr_mai_pf.MAI_rec_sito_8 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "ProvinciaSigla")
        _dr_mai_pf.MAI_rec_nazione = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "NazioneSigla")

        _PrintFatturaDataset.TBL_main.Rows.Add(_dr_mai_pf)

        If _dr_mai_pf.DEF_raccolta = 1 Then
            Carica_DatiFattura(ds_src, dr_fat, 0, lPrimaUnica)
            Carica_DatiFornitura(ds_src, dr_fat, 0, nPodFatturati)
            '
            ' Credo che il totale del totale sia da esporre solo in caso di tipo fattura unica o prima foglio.
            ' 
            Totalizzazione_Parziale_Tipo1(dr_fat.GetChildRows("Fattura_RigheRiepilogo20Servizio"), _dr_mai_pf.MAI_codice, lPrimaUnica)
            Totalizzazione_Parziale_Tipo2(dr_fat.GetChildRows("Fattura_RiepilogoIVA")(0), _dr_mai_pf.MAI_codice, lPrimaUnica)
        End If

        nRow = 1
        aSortPdr = New ArrayList
        _DataMultiPOS = DateTime.Parse("01/01/1900")
        For i As Integer = 0 To nSezDocumenti - 1
            Me.PodInCaricamento = i
            'SezioneDocumentoPodInGestione = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(Me.PodInCaricamento)
            'dr_sezione_documenti = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(Me.PodInCaricamento)

            _dr_mai_pf.MAI_forza_scaccia = False
            If _dr_mai_pf.DEF_raccolta = 9 Then
                _dr_mai_pf.MAI_punto_fornitura = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "PuntoDiFornitura", "IDPunto")
            Else
                nIdPuntoForn = RecuperaValoreDS_UE(ds_src, dr_fat, i, "PuntoDiFornitura", "IDPunto")
                _dr_mai_pf = Nothing
                If _PrintFatturaDataset.TBL_main.Select(String.Concat("MAI_punto_fornitura = '", nIdPuntoForn, "'")).Length = 1 Then
                    _dr_mai_pf = _PrintFatturaDataset.TBL_main.Select(String.Concat("MAI_punto_fornitura = '", nIdPuntoForn, "'"))(0)
                Else
                    dt_mai = _PrintFatturaDataset.TBL_main.Clone
                    dr_mai = _PrintFatturaDataset.TBL_main.FindByMAI_codice(_MasterRecord)
                    dt_mai.ImportRow(dr_mai)
                    _dr_mai_pf = dt_mai.Rows(0)
                    _CodiceProgressivo += 1
                    _dr_mai_pf.MAI_codice = _CodiceProgressivo
                    _dr_mai_pf.MAI_punto_fornitura = RecuperaValoreDS_UE(ds_src, dr_fat, i, "PuntoDiFornitura", "IDPunto")
                    _PrintFatturaDataset.TBL_main.ImportRow(_dr_mai_pf)
                    _dr_mai_pf = _PrintFatturaDataset.TBL_main.Rows(_PrintFatturaDataset.TBL_main.Rows.Count - 1)
                End If
                If (i + 1 = nSezDocumenti) And (_dr_mai_pf.MAI_codice = _CodiceProgressivo) Then
                    _dr_mai_pf.DEF_raccolta = 3
                Else
                    _dr_mai_pf.DEF_raccolta = 2
                End If
                lPrimaUnica = False
            End If
            lOdl = SezioneDocumentoPodInGestione.Item("TipoSezione").ToString.ToLower = cnt_tiposezione_lavoro
            _NomeOfferta = ""
            Carica_DatiFattura(ds_src, dr_fat, i, lPrimaUnica)
            Carica_DatiFornitura(ds_src, dr_fat, i, nPodFatturati)
            Carica_prelievo_annuo(SezioneDocumentoPodInGestione)
            If lOdl Then
                AggiungiODL(ds_src, dr_fat, i)
                If _dr_dfa_pf.DFA_fat_dettaglio Then
                    If nPodFatturati = 1 Then
                        Carica_RigheFatturazione(SezioneDocumentoPodInGestione, dr_fat, i, 1)
                    Else
                        Carica_RigheFatturazione(SezioneDocumentoPodInGestione, dr_fat, i, -1)
                    End If
                End If
            Else
                Carica_StrumentoMisura(_dr_mai_pf.MAI_codice, ds_src, dr_fat, i)
                Carica_Consumi(ds_src, dr_fat, i)
                If _dr_dfa_pf.DFA_fat_dettaglio Then
                    Carica_RigheFatturazione(SezioneDocumentoPodInGestione, dr_fat, i, -1)
                Else
                    Carica_RigheFatturazione(SezioneDocumentoPodInGestione, dr_fat, i, -1, False)
                End If
                ' Calcola i totali per le bollette parziali.

                Carica_DatiDistributore(ds_src, dr_fat, i)
                Carica_MessaggiFatturazione_contr(dr_fat, i)
                Carica_UltimiConsumi(ds_src, i)

                nRow += 1
                If nPodFatturati > 1 Then
                    dr_daf = _PrintFatturaDataset.TBL_dati_forn.Rows(_PrintFatturaDataset.TBL_dati_forn.Rows.Count - 1)
                    aSortPdr.Add(New String() {_dr_mai_pf.MAI_codice, dr_daf.DAF_ub_sito_6, dr_daf.DAF_ub_sito_7, String.Concat(dr_daf.DAF_ub_sito_1, " ", dr_daf.DAF_ub_sito_2, " ", dr_daf.DAF_ub_sito_3, " ", dr_daf.DAF_ub_sito_4)})
                End If
            End If
            Totalizzazione_Parziale_Tipo1(SezioneDocumentoPodInGestione.GetChildRows("SezioniDocumenti_RiepilogoStampaxSezione")(0).GetChildRows("RiepilogoStampaxSezione_RigheRiepilogo"), _dr_mai_pf.MAI_codice, lPrimaUnica)
            Totalizzazione_Parziale_Tipo2(SezioneDocumentoPodInGestione.GetChildRows("SezioniDocumenti_RiepilogoIVA")(0), _dr_mai_pf.MAI_codice, lPrimaUnica)
            Carica_MessaggiFatturazione_raggr(ds_src, dr_fat, i)
            _dr_mai_pf.MAI_per_rifer = DeterminaPeriodoFatturazione(ds_src, dr_fat, i)
            If (_dr_mai_pf.DEF_raccolta > 1) And (_dr_mai_pf.DEF_raccolta <> 9) Then
                Carica_DatiRiepilogo()
            End If
            _dr_dfa_pf.DFA_tipo_fattura = "ORDINARIA"
            '
            ' Determina se ci sono rettifiche per verificare che il tipo di fattura sia Con rettifica
            '
            Dim aDr() As DataRow
            aDr = _PrintFatturaDataset.TBL_consumi.Select(String.Concat("CON_cod_mai = ", _dr_mai_pf.MAI_codice, " and CON_tipo_oper = 2"))
            If aDr.Length > 0 Then
                _dr_dfa_pf.DFA_tipo_fattura = "ORDINARIA CON RETTIFICA"
            End If
        Next

        If aSortPdr.Count > 0 Then
            aSortPdr.Sort(New OrdinaPDR)
        End If
        GetCredenzialiWeb()
        If _dr_mai_pf.DEF_raccolta <> 9 Then
            dr_mai_tmp = _PrintFatturaDataset.TBL_main.Rows(_PrintFatturaDataset.TBL_main.Rows.Count - 1)
            dr_mai_tmp.DEF_raccolta = 3
            dr_mai_tmp.AcceptChanges()
            dr_mai_tmp = _PrintFatturaDataset.TBL_main.FindByMAI_codice(_MasterRecord)
            dr_mai_tmp.MAI_per_rifer = TrasformaPeriodoFatturazione(_DataMultiPOS)
            dr_mai_tmp.AcceptChanges()
        End If

    End Sub

    Private Function DeterminaPeriodoFatturazione(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPdrId As Integer) As String
        Dim dData1 As DateTime
        Dim dData2 As DateTime
        Dim nBimestre As Integer
        Dim nSezioneStampa As Integer
        Dim cTmpStr As String
        Dim cVocePrec As String
        Dim cTmp As String

        Try
            _DataImposteGas = DateTime.Parse("01/01/1900")
            _DataOneriAmmGas = DateTime.Parse("01/01/1900")
            _DataLastResort = DateTime.Parse("01/01/1900")
            dData1 = RecuperaValoreDS_UE(ds_src, dr_fat, nPdrId, "PeriodiFatturazione", "DataFineRipetitiviFatturati", "01/01/1900")
            If dData1.ToString("dd/MM/yyyy") = "01/01/1900" Then
                dData1 = RecuperaValoreDS_UE(ds_src, dr_fat, nPdrId, "PeriodiFatturazione", "DataFineConsumiFatturati", "01/01/1900")
                If dData1.ToString("dd/MM/yyyy") = "01/01/1900" Then
                    dData1 = RecuperaValoreDS_UE(ds_src, dr_fat, nPdrId, "PeriodiFatturazione", "DataFineOneriDiversi", "01/01/1900")
                    If dData1.ToString("dd/MM/yyyy") = "01/01/1900" Then
                        For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPdrId).GetChildRows("SezioniDocumenti_SezioniStampa")
                            nSezioneStampa = -1
                            cTmpStr = dr_tmp1("DescrizioneSezioneStampa").ToString.ToLower
                            If (cTmpStr = "TOTALE SERVIZI DI VENDITA".ToLower) Or (cTmpStr = "VENDITA".ToLower) Then
                                nSezioneStampa = Costanti.SEZ_spese_materia_energia
                            ElseIf (cTmpStr = "TOTALE SERVIZI DI RETE".ToLower) Or (cTmpStr = "DISTRIBUZIONE".ToLower) Then
                                nSezioneStampa = Costanti.SEZ_trasporto
                            ElseIf (cTmpStr = "TOTALE IMPOSTE".ToLower) Or (cTmpStr = "IMPOSTE".ToLower) Then
                                nSezioneStampa = Costanti.SEZ_oneri_sistema
                            ElseIf (cTmpStr = "ONERI DIVERSI DA QUELLI DOVUTI PER LA FORNITURA DI GAS".ToLower) Then
                                nSezioneStampa = Costanti.SEZ_imposte_iva
                            End If
                            If nSezioneStampa > -1 Then
                                For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("SezioniStampa_SottosezioneStampa")
                                    For Each dr_tmp3 As DataRow In dr_tmp2.GetChildRows("SottosezioneStampa_RigaFattura")
                                        cVocePrec = dr_tmp3("Voce")
                                        If IsDate(_DataLastResort) Then
                                            If _DataLastResort < dr_tmp3("DataFine") Then
                                                _DataLastResort = dr_tmp3("DataFine")
                                            End If
                                        Else
                                            _DataLastResort = dr_tmp3("DataFine")
                                        End If

                                        ' Le date _DataImposteGas e _DataOneriAmmGas verranno riutilizzate nella determinazione del periodo di fatturazione.
                                        'If nSezioneStampa = Costanti.SEZ_OneriDiversi And cVocePrec = "ONERI AMMINISTRATIVI GAS" Then
                                        '    If IsDate(_DataOneriAmmGas) Then
                                        '        If _DataOneriAmmGas < dr_tmp3("DataFine") Then
                                        '            _DataOneriAmmGas = dr_tmp3("DataFine")
                                        '        End If
                                        '    Else
                                        '        _DataOneriAmmGas = dr_tmp3("DataFine")
                                        '    End If
                                        'End If
                                        'If nSezioneStampa = Costanti.SEZ_Imposte Then
                                        '    If IsDate(_DataImposteGas) Then
                                        '        If _DataImposteGas < dr_tmp3("DataFine") Then
                                        '            _DataImposteGas = dr_tmp3("DataFine")
                                        '        End If
                                        '    Else
                                        '        _DataImposteGas = dr_tmp3("DataFine")
                                        '    End If
                                        'End If
                                    Next
                                Next
                            End If
                        Next

                        'If _DataOneriAmmGas.ToString("dd/MM/yyyy") = "01/01/1900" Then
                        '    If _DataImposteGas.ToString("dd/MM/yyyy") = "01/01/1900" Then
                        If _DataLastResort.ToString("dd/MM/yyyy") = "01/01/1900" Then
                            Throw New Exception("Errore determinando il periodo di fatturazione")
                        Else
                            dData2 = _DataLastResort
                            'End If
                            '    Else
                            'dData2 = _DataImposteGas
                            '    End If
                            'Else
                            'dData2 = _DataOneriAmmGas
                        End If
                    Else
                        dData2 = dData1
                    End If
                Else
                    dData2 = dData1
                End If
            Else
                dData2 = dData1
            End If
            If (_DataMultiPOS < dData2) And dData2.Year <> 9999 Then
                _DataMultiPOS = dData2
            ElseIf dData2.Year = 9999 Then
                dData2 = _DataMultiPOS
            End If
            cTmp = TrasformaPeriodoFatturazione(dData2)

        Catch ex As Exception
            cTmp = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_Periodo_fatturazione).ToLower
        End Try
        Return cTmp

    End Function

    Private Function TrasformaPeriodoFatturazione(ByVal dData2 As DateTime) As String
        Dim nBimestre As Integer
        Dim cTmp As String

        cTmp = ""
        If _ModalitaFatturazione = "MENSILE" Then
            cTmp = String.Concat(FC_utilita.TrasformaMese(dData2.Month), " ", dData2.Year).ToUpper  ' _ModalitaFatturazione
        End If
        If _ModalitaFatturazione = "BIMESTRALE" Then
            nBimestre = Int(dData2.Month / 2 + 0.5)
            cTmp = String.Concat(nBimestre, "� BIMESTRE ", dData2.Year) '_ModalitaFatturazione
        End If
        Return cTmp

    End Function

    Private Sub AggiungiODL(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer)
        Dim dr_odl As DS_PF_UE_ele.TBL_odlRow
        Dim dr_tmp1 As DataRow
        Dim dr_tmp As DataRow

        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId)

        If dr_tmp("TipoSezione").ToString.ToLower = cnt_tiposezione_lavoro Then
            If dr_tmp.GetChildRows("SezioniDocumenti_ODL").Length > 0 Then
                dr_tmp1 = dr_tmp.GetChildRows("SezioniDocumenti_ODL")(0)
                dr_odl = _PrintFatturaDataset.TBL_odl.NewRow
                dr_odl.ODL_cod_mai = _dr_mai_pf.MAI_codice
                dr_odl.ODL_anno = dr_tmp1("AnnoODL")
                dr_odl.ODL_numero = dr_tmp1("NumeroODL")
                _PrintFatturaDataset.TBL_odl.Rows.Add(dr_odl)
            End If

        End If

    End Sub

    Private Sub Carica_DatiRiepilogo()
        Dim dr_tfa As DS_PF_UE_ele.TBL_tot_fat_t2Row
        Dim dr_elp As DS_PF_UE_ele.TBL_elenco_podRow
        Dim dr_daf As DS_PF_UE_ele.TBL_dati_fornRow
        Dim dr_rii As DS_PF_UE_ele.TBL_rias_impRow
        Dim nTotale As Decimal

        dr_daf = _PrintFatturaDataset.TBL_dati_forn.Rows(_PrintFatturaDataset.TBL_dati_forn.Rows.Count - 1)
        If _PrintFatturaDataset.TBL_elenco_pod.Select(String.Concat("ELP_cod_mai = ", _dr_mai_pf.MAI_linkQD, " AND ELP_pod = '", dr_daf.DAF_pod, "'")).Length > 0 Then
            dr_elp = _PrintFatturaDataset.TBL_elenco_pod.Select(String.Concat("ELP_cod_mai = ", _dr_mai_pf.MAI_linkQD, " AND ELP_pod = '", dr_daf.DAF_pod, "'"))(0)
            dr_elp.Delete()
        End If
        dr_elp = _PrintFatturaDataset.TBL_elenco_pod.NewRow
        dr_elp.ELP_cod_mai = _dr_mai_pf.MAI_linkQD
        dr_elp.ELP_pod = dr_daf.DAF_pod
        dr_elp.ELP_descr_sito_f = dr_daf.DAF_descr_sito_f
        dr_elp.ELP_anno_contratto = dr_daf.DAF_anno_contratto
        dr_elp.ELP_num_contratto = dr_daf.DAF_num_contratto
        dr_elp.ELP_ub_sito_1 = dr_daf.DAF_ub_sito_1
        dr_elp.ELP_ub_sito_2 = dr_daf.DAF_ub_sito_2
        dr_elp.ELP_ub_sito_3 = dr_daf.DAF_ub_sito_3
        dr_elp.ELP_ub_sito_4 = dr_daf.DAF_ub_sito_4
        dr_elp.ELP_ub_sito_5 = dr_daf.DAF_ub_sito_5
        dr_elp.ELP_ub_sito_6 = dr_daf.DAF_ub_sito_6
        dr_elp.ELP_ub_sito_7 = dr_daf.DAF_ub_sito_7
        dr_elp.ELP_ub_sito_8 = dr_daf.DAF_ub_sito_8
        _PrintFatturaDataset.TBL_elenco_pod.Rows.Add(dr_elp)

        nTotale = 0
        For Each dr_tfa In _PrintFatturaDataset.TBL_tot_fat_t2.Select(String.Concat("TFA_cod_mai = ", _dr_mai_pf.MAI_codice))
            If dr_tfa.TFA_riassunto And dr_tfa.TFA_sortfield <> eSortRiepValue.SRV_totale_pagare Then
                dr_rii = _PrintFatturaDataset.TBL_rias_imp.NewRow
                dr_rii.RII_cod_elp = dr_elp.ELP_codice
                dr_rii.RII_descrizione = dr_tfa.TFA_descrizione
                dr_rii.RII_totale = dr_tfa.TFA_importo
                nTotale += dr_tfa.TFA_importo
                _PrintFatturaDataset.TBL_rias_imp.Rows.Add(dr_rii)
            ElseIf dr_tfa.TFA_riassunto And dr_tfa.TFA_sortfield = eSortRiepValue.SRV_totale_pagare Then
                dr_elp.ELP_totale_pod = dr_tfa.TFA_importo
            End If
            'If Not (dr_tfa.TFA_descrizione = "Totale fattura".ToUpper) Then
            '    If dr_tfa.TFA_tipo_esp = 1 Then
            '        dr_rii = _PrintFatturaDataset.TBL_rias_imp.NewRow
            '        dr_rii.RII_cod_elp = dr_elp.ELP_codice
            '        dr_rii.RII_descrizione = dr_tfa.TFA_descrizione
            '        dr_rii.RII_totale = dr_tfa.TFA_importo
            '        nTotale += dr_tfa.TFA_importo
            '        _PrintFatturaDataset.TBL_rias_imp.Rows.Add(dr_rii)
            '    ElseIf dr_tfa.TFA_tipo_esp = 2 Then
            '        dr_rii = _PrintFatturaDataset.TBL_rias_imp.NewRow
            '        dr_rii.RII_cod_elp = dr_elp.ELP_codice
            '        dr_rii.RII_descrizione = String.Concat("Imponibile ", dr_tfa.TFA_descrizione)
            '        dr_rii.RII_totale = dr_tfa.TFA_importo
            '        nTotale += dr_tfa.TFA_importo
            '        _PrintFatturaDataset.TBL_rias_imp.Rows.Add(dr_rii)
            '        dr_rii = _PrintFatturaDataset.TBL_rias_imp.NewRow
            '        dr_rii.RII_cod_elp = dr_elp.ELP_codice
            '        dr_rii.RII_descrizione = dr_tfa.TFA_descrizione
            '        dr_rii.RII_totale = dr_tfa.TFA_iva
            '        nTotale += dr_tfa.TFA_iva
            '        _PrintFatturaDataset.TBL_rias_imp.Rows.Add(dr_rii)
            '        'ElseIf dr_tfa.TFA_tipo_esp = 4 Then
            '        '    dr_rii = _PrintFatturaDataset.TBL_rias_imp.NewRow
            '        '    dr_rii.RII_cod_elp = dr_elp.ELP_codice
            '        '    dr_rii.RII_descrizione = dr_tfa.TFA_descrizione
            '        '    dr_rii.RII_totale = dr_tfa.TFA_importo
            '        '    nTotale += dr_tfa.TFA_importo
            '        '    _PrintFatturaDataset.TBL_rias_imp.Rows.Add(dr_rii)
            '    End If
            'End If
        Next


    End Sub

    '
    ' Genera i record per la visualizzazione del grafico.
    ' TFA_tipo_riep = 1
    ' TFA_tipo_esp = 1 
    '
    Private Sub Totalizzazione_Parziale_Tipo1(ByVal Riepilogo As DataRow(), ByVal nCodMain As Integer, ByVal lPrimaUnica As Boolean)
        Dim dt_tfa As DS_PF_UE_ele.TBL_tot_fat_t1DataTable
        Dim dr_tfa1 As DS_PF_UE_ele.TBL_tot_fat_t1Row
        Dim dr_totale_imposte As DS_PF_UE_ele.TBL_tot_fat_t1Row
        Dim dr_tfa2 As DS_PF_UE_ele.TBL_tot_fat_t1Row
        Dim dc_tfa_codice As DataColumn
        Dim nMaxTFA_codice As Integer
        Dim cRelation1 As String
        Dim cRelation2 As String
        Dim dv_srt As DataView
        Dim dr_tmp As DataRow
        Dim i As Integer

        Try
            '
            ' Recupero il valore massimo della colonna TFA_codice.
            '
            nMaxTFA_codice = 0
            For Each drTmp As DS_PF_UE_ele.TBL_tot_fat_t1Row In _PrintFatturaDataset.TBL_tot_fat_t1.Rows
                nMaxTFA_codice = Math.Max(nMaxTFA_codice, drTmp.TFA_codice)
            Next

            '
            ' Crea la tabella di appoggio.
            ' Reimposta anche il contatore del numero delle righe in modo che l'importazione non vada in errore
            '
            dt_tfa = New DS_PF_UE_ele.TBL_tot_fat_t1DataTable
            dc_tfa_codice = dt_tfa.Columns("TFA_codice")
            dc_tfa_codice.AutoIncrementSeed = nMaxTFA_codice + 1
            dc_tfa_codice.AutoIncrementSeed += 100

            '
            ' Carica i dati del riepilogo di TIPO 1
            '
            For Each dr_tmp In Riepilogo
                dr_tfa1 = dt_tfa.NewRow
                dr_tfa1.TFA_cod_mai = nCodMain
                dr_tfa1.TFA_tipo_riep = 1
                dr_tfa1.TFA_descrizione = dr_tmp.Item("RigaRiepilogo")
                If (dr_tfa1.TFA_descrizione.ToLower = "SPESA PER LA MATERIA ENERGIA".ToLower) Then
                    dr_tfa1.TFA_tipo_esp = 1
                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_materia
                ElseIf (dr_tfa1.TFA_descrizione.ToLower = "SPESA PER IL TRASPORTO E LA GESTIONE DEL CONTATORE".ToLower) Then
                    dr_tfa1.TFA_tipo_esp = 2
                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_trasporto
                ElseIf (dr_tfa1.TFA_descrizione.ToLower = "SPESA PER ONERI DI SISTEMA".ToLower) Then
                    dr_tfa1.TFA_tipo_esp = 3
                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_oneri
                ElseIf (dr_tfa1.TFA_descrizione.ToLower = "TOTALE IMPOSTE E IVA".ToLower) Then
                    dr_tfa1.TFA_tipo_esp = 4
                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_totale_imposte_iva
                    dr_totale_imposte = dr_tfa1
                ElseIf (dr_tfa1.TFA_descrizione.ToLower = "RICALCOLI".ToLower) Then
                    dr_tfa1.TFA_tipo_esp = 5
                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_ricalcoli
                ElseIf (dr_tfa1.TFA_descrizione.ToLower = "ALTRE PARTITE".ToLower) Then
                    dr_tfa1.TFA_tipo_esp = 6
                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_altre_partite

                    '
                    ' Verifica se tra le voci del riepilogo 2 esiste una voce per il canone
                    '
                    dr_tfa1.TFA_importo = TrovaValoreCanoneRai() * -1
                Else
                    dr_tfa1.TFA_tipo_esp = 1000
                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_altro
                End If

                dr_tfa1.TFA_importo += dr_tmp.Item("Importo")
                i = dr_tfa1.TFA_codice
                If (dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_altre_partite And dr_tfa1.TFA_importo <> 0) Or dr_tfa1.TFA_sortfield <> eSortRiepValue.SRV_altre_partite Then
                    dt_tfa.Rows.Add(dr_tfa1)
                End If
            Next

            '
            ' Se il record del Totale Imposte ed Iva non esiste allora lo creo.
            '
            If dr_totale_imposte Is Nothing Then
                dr_totale_imposte = dt_tfa.NewRow
                dr_totale_imposte.TFA_cod_mai = nCodMain
                dr_totale_imposte.TFA_tipo_riep = 1
                dr_totale_imposte.TFA_descrizione = "TOTALE IMPOSTE E IVA"
                dr_totale_imposte.TFA_tipo_esp = 4
                dr_totale_imposte.TFA_sortfield = eSortRiepValue.SRV_totale_imposte_iva
                dr_totale_imposte.TFA_importo = 0
                dr_totale_imposte.TFA_used = False
            End If

            '
            ' Sommo gli importi dell'IVA al record Totale Imposte ed IVA
            '
            If _dr_mai_pf.DEF_raccolta > 1 Or _dr_mai_pf.DEF_raccolta = 9 Then
                For Each cRelation As String In New String() {"RiepilogoIVA_CorrispettiviIVA;CorrispettiviIVA_RigaRiepilogoIVA"}
                    cRelation1 = cRelation.Split(";")(0)
                    cRelation2 = cRelation.Split(";")(1)

                    '
                    ' Verifica la presenza dei record richiesti
                    '
                    If SezioneDocumentoPodInGestione.GetChildRows("SezioniDocumenti_RiepilogoIVA")(0).GetChildRows(cRelation1).Length = 1 Then
                        For Each dr_tmp1 As DataRow In SezioneDocumentoPodInGestione.GetChildRows("SezioniDocumenti_RiepilogoIVA")(0).GetChildRows(cRelation1)(0).GetChildRows(cRelation2)

                            '
                            ' Sommo gli importi dell'IVA al record Totale Imposte ed IVA
                            '
                            dr_totale_imposte.TFA_importo += dr_tmp1.Item("Imposta")
                            dr_totale_imposte.TFA_used = True
                        Next
                    End If
                Next
            End If
            If dr_totale_imposte.TFA_used And dr_totale_imposte.RowState = DataRowState.Detached Then
                dt_tfa.Rows.Add(dr_totale_imposte)
            End If

            '
            ' Ordina i record per una corretta visualizzazione
            '
            dv_srt = New DataView(dt_tfa)
            dv_srt.Sort = "TFA_sortfield"

            '
            ' Caricati i record sulla tabella temporanea allora procedo con il consolidamento dei dati. Non sono ammesse righe doppie.
            '
            i = 1
            While i < dt_tfa.Rows.Count
                dr_tfa1 = CType(dv_srt(i - 1).Row, DS_PF_UE_ele.TBL_tot_fat_t1Row)
                dr_tfa2 = CType(dv_srt(i).Row, DS_PF_UE_ele.TBL_tot_fat_t1Row)

                '
                ' Se i due record consecutivi sono identici allora procedo con la loro somma
                '
                If (dr_tfa1.TFA_tipo_esp <> 1000) And (dr_tfa1.TFA_tipo_esp = dr_tfa2.TFA_tipo_esp) Then
                    dr_tfa1.TFA_importo += dr_tfa2.TFA_importo
                    dr_tfa2.Delete()
                    i -= 1
                End If
                i += 1
            End While

            '
            ' Importa i record risultanti nella tabella originale
            '
            For Each drv As DataRowView In dv_srt
                dr_tfa1 = CType(drv.Row, DS_PF_UE_ele.TBL_tot_fat_t1Row)
                _PrintFatturaDataset.TBL_tot_fat_t1.ImportRow(dr_tfa1)
            Next
        Catch ex As Exception
            Throw New Exception("Errore sulla prcedura: Totalizzazione_Parziale_Tipo1", ex)
        End Try

    End Sub

    Private Sub Totalizzazione_Parziale_Tipo2(ByVal FatturaPDR As DataRow, ByVal nCodMain As Integer, ByVal lPrimaUnica As Boolean)
        Dim adr_tfa_tmp2() As DS_PF_UE_ele.TBL_tot_fat_t2Row
        Dim adrtmp1() As DataRow
        Dim dt_tfa As DS_PF_UE_ele.TBL_tot_fat_t2DataTable
        Dim dr_tfa1 As DS_PF_UE_ele.TBL_tot_fat_t2Row
        Dim dr_tfa2 As DS_PF_UE_ele.TBL_tot_fat_t2Row
        Dim nRigheAddizionaliAccise As Integer
        Dim dc_tfa_codice As DataColumn
        Dim nMaxTFA_codice As Integer
        Dim cRelation1 As String
        Dim cRelation2 As String
        'Dim dr_tmp1 As DataRow
        'Dim dr_tmp2 As DataRow
        Dim nTotale As Decimal
        Dim dv_srt As DataView
        Dim dr_tfa_TotaleSpesa As DS_PF_UE_ele.TBL_tot_fat_t2Row
        Dim dr_tfa_totale_bolletta As DS_PF_UE_ele.TBL_tot_fat_t2Row
        Dim nRow As Integer
        Dim aRelazioni As String()
        Dim i As Integer
        Dim cCodiceAbbonamentoRAI As String

        Try
            cCodiceAbbonamentoRAI = ""
            '
            ' Recupero il valore massimo della colonna TFA_codice.
            '
            nMaxTFA_codice = 0
            For Each drTmp As DS_PF_UE_ele.TBL_tot_fat_t2Row In _PrintFatturaDataset.TBL_tot_fat_t2.Rows
                nMaxTFA_codice = Math.Max(nMaxTFA_codice, drTmp.TFA_codice)
            Next

            '
            ' Crea la tabella di appoggio.
            ' Reimposta anche il contatore del numero delle righe in modo che l'importazione non vada in errore
            '
            dt_tfa = New DS_PF_UE_ele.TBL_tot_fat_t2DataTable
            dc_tfa_codice = dt_tfa.Columns("TFA_codice")
            dc_tfa_codice.AutoIncrementSeed = nMaxTFA_codice + 1
            dc_tfa_codice.AutoIncrementSeed += 100

            '
            ' Recupero dalla tabella dei totali riepilogo di tipo 1 i valori e li copio in questa tabella.
            ' Raggruppando le voci di SPESA GAS, Trasporto, Oneri, Altre partite e li indico in TOTALE SPESA 
            '
            '
            ' Dichiarazione del record pe ril totale della spesa
            '
            dr_tfa_TotaleSpesa = dt_tfa.NewRow
            dr_tfa_TotaleSpesa.TFA_cod_mai = nCodMain
            dr_tfa_TotaleSpesa.TFA_tipo_esp = 1
            dr_tfa_TotaleSpesa.TFA_sortfield = eSortRiepValue.SRV_totale_spesa
            dr_tfa_TotaleSpesa.TFA_riassunto = True
            dr_tfa_TotaleSpesa.TFA_descrizione = "Totale Spesa energia/trasporto/oneri di sistema".ToUpper
            dr_tfa_TotaleSpesa.TFA_importo = 0
            dr_tfa_TotaleSpesa.TFA_iva = 0
            dr_tfa_TotaleSpesa.TFA_used = False
            dt_tfa.Rows.Add(dr_tfa_TotaleSpesa)

            '
            ' Aggiunge il totale della fattura
            '
            dr_tfa_totale_bolletta = dt_tfa.NewRow
            dr_tfa_totale_bolletta.TFA_cod_mai = nCodMain
            dr_tfa_totale_bolletta.TFA_tipo_esp = 1
            dr_tfa_totale_bolletta.TFA_sortfield = eSortRiepValue.SRV_totale_fattura
            dr_tfa_totale_bolletta.TFA_riassunto = True
            dr_tfa_totale_bolletta.TFA_descrizione = "Totale fattura".ToUpper
            dr_tfa_totale_bolletta.TFA_importo = _dr_dfa_pf.DFA_totale_fattura ' _dr_dfa_pf.DFA_TotaleAssoggettatoIVA
            dr_tfa_totale_bolletta.TFA_iva = 0
            dt_tfa.Rows.Add(dr_tfa_totale_bolletta)

            '
            ' Aggiunge il totale della fattura
            '
            dr_tfa1 = dt_tfa.NewRow
            dr_tfa1.TFA_cod_mai = nCodMain
            dr_tfa1.TFA_tipo_esp = 1
            dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_totale_pagare
            dr_tfa1.TFA_riassunto = True
            dr_tfa1.TFA_descrizione = "Totale da pagare".ToUpper
            dr_tfa1.TFA_importo = _dr_dfa_pf.DFA_totale_fattura ' _dr_dfa_pf.DFA_TotaleAssoggettatoIVA
            dr_tfa1.TFA_iva = 0
            dt_tfa.Rows.Add(dr_tfa1)

            'For Each dr_tfa_tmp As DS_PF_UE_ele.TBL_tot_fat_t1Row In _PrintFatturaDataset.TBL_tot_fat_t1.Select(String.Concat("TFA_cod_mai = ", nCodMain))
            '    If dr_tfa_tmp.TFA_sortfield <> eSortRiepValue.SRV_totale_imposte_iva Then
            '        dr_tfa2.TFA_importo += dr_tfa_tmp.TFA_importo
            '    Else
            '        dr_tfa1 = dt_tfa.NewRow
            '        For Each cTmp As String In New String() {"TFA_cod_mai", "TFA_sortfield", "TFA_descrizione", "TFA_importo"}
            '            dr_tfa1(cTmp) = dr_tfa_tmp(cTmp)
            '        Next
            '        dr_tfa1.TFA_tipo_riep = 2
            '        dr_tfa1.TFA_tipo_esp = 1
            '        dr_tfa1.TFA_iva = 0
            '        dr_tfa1.TFA_aliquota = 0
            '        dt_tfa.Rows.Add(dr_tfa1)
            '    End If
            'Next

            If _dr_mai_pf.DEF_raccolta = 1 Then
                adrtmp1 = FatturaInGestione.GetChildRows("Fattura_RigheRiepilogo20Servizio")
            Else
                adrtmp1 = SezioneDocumentoPodInGestione.GetChildRows("SezioniDocumenti_RiepilogoStampaxSezione")(0).GetChildRows("RiepilogoStampaxSezione_RigheRiepilogo20Contratto")
            End If

            For Each dr_tmp1 As DataRow In adrtmp1
                '
                ' Somma le seguenti componenti:
                ' - SPESA PER LA MATERIA ENERGIA
                ' - SPESA PER IL TRASPORTO E LA GESTIONE DEL CONTATORE
                ' - SPESA PER ONERI DI SISTEMA
                '
                If "24,25,26,".Contains(dr_tmp1.Item("CodiceRigaRiepilogo").ToString() + ",") Then
                    dr_tfa_TotaleSpesa.TFA_importo += dr_tmp1.Item("Importo")
                    dr_tfa_TotaleSpesa.TFA_used = True

                    ' Ricalcoli
                ElseIf "27,".Contains(dr_tmp1.Item("CodiceRigaRiepilogo").ToString() + ",") Then
                    If dr_tmp1.Item("Importo") <> 0 Then
                        dr_tfa1 = dt_tfa.NewRow
                        dr_tfa1.TFA_cod_mai = nCodMain
                        dr_tfa1.TFA_tipo_esp = 1
                        dr_tfa1.TFA_descrizione = dr_tmp1.Item("RigaRiepilogo")
                        dr_tfa1.TFA_importo = dr_tmp1.Item("Importo")
                        dr_tfa1.TFA_iva = 0
                        dr_tfa1.TFA_aliquota = 0
                        dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_ricalcoli
                        dr_tfa1.TFA_riassunto = True
                        dr_tfa1.TFA_used = True
                        dt_tfa.Rows.Add(dr_tfa1)
                    End If
                ElseIf "28,".Contains(dr_tmp1.Item("CodiceRigaRiepilogo").ToString() + ",") Then
                    dr_tfa1 = dt_tfa.NewRow
                    dr_tfa1.TFA_cod_mai = nCodMain
                    dr_tfa1.TFA_tipo_esp = 1
                    dr_tfa1.TFA_descrizione = dr_tmp1.Item("RigaRiepilogo")
                    dr_tfa1.TFA_importo = dr_tmp1.Item("Importo")
                    dr_tfa1.TFA_iva = 0
                    dr_tfa1.TFA_aliquota = 0
                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_altro
                    dr_tfa1.TFA_riassunto = True
                    dr_tfa1.TFA_used = True
                    dr_tfa_totale_bolletta.TFA_importo -= dr_tfa1.TFA_importo
                    'dt_tfa.Rows.Add(dr_tfa1)
                ElseIf "30,".Contains(dr_tmp1.Item("CodiceRigaRiepilogo").ToString() + ",") Then
                    dr_tfa1 = dt_tfa.NewRow
                    dr_tfa1.TFA_cod_mai = nCodMain
                    dr_tfa1.TFA_tipo_esp = 1
                    dr_tfa1.TFA_descrizione = dr_tmp1.Item("RigaRiepilogo")
                    dr_tfa1.TFA_importo = dr_tmp1.Item("Importo")
                    dr_tfa1.TFA_iva = 0
                    dr_tfa1.TFA_aliquota = 0
                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_totale_imposte_iva
                    dr_tfa1.TFA_riassunto = True
                    dr_tfa1.TFA_used = True
                    dt_tfa.Rows.Add(dr_tfa1)
                Else
                    dr_tfa1 = dt_tfa.NewRow
                    dr_tfa1.TFA_cod_mai = nCodMain
                    dr_tfa1.TFA_tipo_esp = 1
                    dr_tfa1.TFA_descrizione = dr_tmp1.Item("RigaRiepilogo")
                    dr_tfa1.TFA_importo = dr_tmp1.Item("Importo")
                    dr_tfa1.TFA_iva = 0
                    dr_tfa1.TFA_aliquota = 0
                    dr_tfa1.TFA_used = True
                    dr_tfa1.TFA_riassunto = True
                    dt_tfa.Rows.Add(dr_tfa1)
                End If

                '
                ' Qualora fossero presenti aggancia le righe dei subtotali
                '
                nRow = 0
                For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("RigheRiepilogo20Contratto_RigaFattura")
                    nRow += 1
                    dr_tfa2 = dt_tfa.NewRow
                    dr_tfa2.TFA_cod_mai = nCodMain
                    dr_tfa2.TFA_tipo_esp = 2
                    If dr_tmp1.Item("CodiceRigaRiepilogo") = 30 Then
                        dr_tfa2.TFA_descrizione = dr_tmp2.Item("Voce") + " " + dr_tmp2.Item("Scaglione") + " dal " + dr_tmp2.Item("DataInizio") + " al " + dr_tmp2.Item("DataFine")
                        If dr_tfa2.TFA_descrizione.Length > 94 Then
                            dr_tfa2.TFA_descrizione = dr_tmp2.Item("Voce") + " dal " + dr_tmp2.Item("DataInizio") + " al " + dr_tmp2.Item("DataFine")
                        End If
                    Else
                        dr_tfa2.TFA_descrizione = dr_tmp2.Item("Voce")
                    End If

                    '
                    ' Il giro per il recupero del codice abbonamento � arzigogoalto per non dover ripertere 100 volte il processo di Prima lettera con il caps loc.
                    '
                    If (dr_tmp2.Item("Voce") = "RATA CANONE RAI" Or dr_tmp2.Item("Voce") = "CANONE DI ABBONAMENTO ALLA TELEVISIONE PER USO PRIVATO") Then
                        cCodiceAbbonamentoRAI = dr_tmp2("NoteRiga")
                    Else
                        cCodiceAbbonamentoRAI = ""
                    End If
                    dr_tfa2.TFA_descrizione = dr_tfa2.TFA_descrizione.Substring(0, 1).ToUpper + dr_tfa2.TFA_descrizione.Substring(1).ToLower
                    If cCodiceAbbonamentoRAI <> "" Then
                        dr_tfa2.TFA_descrizione += " " + cCodiceAbbonamentoRAI
                    End If
                    dr_tfa2.TFA_importo = dr_tmp2.Item("Importo")
                    dr_tfa2.TFA_iva = 0
                    dr_tfa2.TFA_aliquota = 0
                    dr_tfa2.TFA_sortfield = dr_tfa1.TFA_sortfield - 5000 + nRow
                    dr_tfa2.TFA_riassunto = False
                    dr_tfa2.TFA_used = True
                    If dr_tmp2.Item("Importo") <> 0 Then
                        dt_tfa.Rows.Add(dr_tfa2)
                    End If
                Next
            Next
            _dr_dfa_pf.DFA_dett_accise = False
            '
            ' Imposto il valore del campo TFA_codice in modo da non generare chiavi univoche
            '
            'dc_tfa_codice = dt_tfa.Columns("TFA_codice")
            'dc_tfa_codice.AutoIncrementSeed = _PrintFatturaDataset.TBL_tot_fat_t2.Rows.Count + 1

            '
            ' Inserimento delle righe del dettaglio IVA
            '
            If _dr_mai_pf.DEF_raccolta > 1 Or _dr_mai_pf.DEF_raccolta = 9 Then
                nRigheAddizionaliAccise = 0
                For Each cRelation As String In New String() {"RiepilogoIVA_CorrispettiviIVA;CorrispettiviIVA_RigaRiepilogoIVA", _
                                                              "RiepilogoIVA_CorrispettiviFC;CorrispettiviFC_RecuperoFattureSospese", "RiepilogoIVA_CorrispettiviFC;CorrispettiviFC_AltreVociFC"}
                    cRelation1 = cRelation.Split(";")(0)
                    cRelation2 = cRelation.Split(";")(1)

                    '
                    ' Verifica la presenza dei record richiesti
                    '
                    If FatturaPDR.GetChildRows(cRelation1).Length = 1 Then
                        For Each dr_tmp1 As DataRow In FatturaPDR.GetChildRows(cRelation1)(0).GetChildRows(cRelation2)
                            If cRelation1.EndsWith("_CorrispettiviIVA") Then
                                adr_tfa_tmp2 = dt_tfa.Select(String.Concat("TFA_cod_iva = '", dr_tmp1.Item("Aliquota"), "'"))
                            Else
                                adr_tfa_tmp2 = dt_tfa.Select(String.Concat("TFA_cod_iva = '", dr_tmp1.Item("ArticoloIVA"), "'"))
                            End If

                            '
                            ' Verifico che se c'� almento un record con la stessa ivatura di quello che sto analizzando.
                            '
                            If (adr_tfa_tmp2.Length = 0) Then
                                dr_tfa1 = dt_tfa.NewRow
                                dr_tfa1.TFA_cod_mai = nCodMain
                                If cRelation1.EndsWith("_CorrispettiviIVA") Then
                                    dr_tfa1.TFA_descrizione = dr_tmp1.Item("ArticoloIVA")
                                    dr_tfa1.TFA_importo = dr_tmp1.Item("Imponibile")
                                    dr_tfa1.TFA_iva = dr_tmp1.Item("Imposta")
                                    dr_tfa1.TFA_aliquota = dr_tmp1.Item("Aliquota")
                                    dr_tfa1.TFA_tipo_esp = 3
                                    dr_tfa1.TFA_cod_iva = dr_tmp1.Item("Aliquota")
                                    '
                                    ' Imposta il tipo di visualizzazione necessario.
                                    '
                                    For Each dr_tmp2 As DataRow In FatturaPDR.GetChildRows("RiepilogoIVA_ElencoArticoliIva")
                                        If (dr_tfa1.TFA_cod_iva = dr_tmp2("ArticoloIva")) Then
                                            dr_tfa1.TFA_descrizione = dr_tmp2("DescrizioneArticoloIva")
                                            If (dr_tfa1.TFA_iva = 0 And Not IsNumeric(dr_tfa1.TFA_cod_iva)) Then
                                                dr_tfa1.TFA_tipo_esp = 1
                                            Else
                                                dr_tfa1.TFA_tipo_esp = 3
                                            End If
                                        End If
                                    Next
                                Else
                                    dr_tfa1.TFA_descrizione = dr_tmp1.Item("Voce")
                                    dr_tfa1.TFA_importo = dr_tmp1("TotaleRigaIva")
                                    dr_tfa1.TFA_iva = 0
                                    dr_tfa1.TFA_aliquota = 0
                                    dr_tfa1.TFA_tipo_esp = 4
                                    dr_tfa1.TFA_cod_iva = dr_tmp1.Item("ArticoloIVA")
                                End If

                                If IsNumeric(dr_tfa1.TFA_cod_iva) Then
                                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_imposte_iva + dr_tfa1.TFA_aliquota
                                ElseIf dr_tfa1.TFA_descrizione.ToUpper = "PARTITE GIA ASSOGETTATE A IVA" Then
                                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_imposte_iva + 500
                                Else
                                    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_imposte_iva
                                End If
                                dr_tfa1.TFA_riassunto = False

                                dt_tfa.Rows.Add(dr_tfa1)
                                nRigheAddizionaliAccise += 1
                            Else
                                dr_tfa1 = adr_tfa_tmp2(0)
                                If cRelation1.EndsWith("_CorrispettiviIVA") Then
                                    dr_tfa1.TFA_importo += dr_tmp1.Item("Imponibile")
                                    dr_tfa1.TFA_iva += dr_tmp1.Item("Imposta")
                                Else
                                    dr_tfa1.TFA_importo += dr_tmp1("TotaleRigaIva")
                                End If
                            End If
                        Next
                    End If
                Next

                '
                ' Esposizione delle anticipazioni
                '
                aRelazioni = New String() {"Fattura_RiepilogoIVA", "RiepilogoIVA_CorrispettiviFC", "CorrispettiviFC_RecuperoAnticipazioni"}

                adrtmp1 = FatturaInGestione.GetChildRows(aRelazioni(0))
                i = 1
                While (i < aRelazioni.Length - 1) And adrtmp1.Length = 1
                    adrtmp1 = adrtmp1(0).GetChildRows(aRelazioni(i))
                    i += 1
                End While
                If adrtmp1.Length = 1 Then
                    For Each dr_tmp1 As DataRow In adrtmp1(0).GetChildRows(aRelazioni(aRelazioni.Length - 1))
                        dr_tfa1 = dt_tfa.NewRow
                        dr_tfa1.TFA_cod_mai = nCodMain
                        dr_tfa1.TFA_descrizione = dr_tmp1.Item("Voce")
                        dr_tfa1.TFA_importo = dr_tmp1("TotaleRigaIva")
                        dr_tfa1.TFA_iva = 0
                        dr_tfa1.TFA_aliquota = 0
                        dr_tfa1.TFA_tipo_esp = 2
                        dr_tfa1.TFA_cod_iva = dr_tmp1.Item("ArticoloIVA")
                        dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_altre_partite
                        dt_tfa.Rows.Add(dr_tfa1)
                        dr_tfa_totale_bolletta.TFA_importo += dr_tmp1("TotaleRigaIva") * -1
                    Next
                End If

            End If

            ''
            '' Aggiunge le righe delle voci generiche che vanno in fondo alla fattura.
            ''
            'While nRecord < _PrintFatturaDataset.TBL_fattura_row.Rows.Count
            '    dr_far = _PrintFatturaDataset.TBL_fattura_row.Rows(nRecord)
            '    If dr_far.FAR_id_sez_orig = -37 And Not dr_far.FAR_row_desc Then
            '        dr_tfa1 = dt_tfa.NewRow
            '        dr_tfa1.TFA_cod_mai = nCodMain
            '        dr_tfa1.TFA_tipo_riep = 2
            '        dr_tfa1.TFA_tipo_esp = 1
            '        dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_altro
            '        dr_tfa1.TFA_descrizione = dr_far.FAR_des_voce.ToLower
            '        dr_tfa1.TFA_importo = dr_far.FAR_importo
            '        dr_tfa1.TFA_iva = 0
            '        dt_tfa.Rows.Add(dr_tfa1)
            '        dr_far.Delete()
            '        nRecord -= 1
            '    ElseIf dr_far.FAR_id_sez_orig = -37 And dr_far.FAR_row_desc Then
            '        cDesc = dr_far.FAR_des_voce
            '        dr_far.Delete()
            '        nRecord -= 1
            '    End If
            '    nRecord += 1
            'End While

            ''
            '' Aggiunge riga per il totale delle addizionali e delle accise.
            ''
            'nTotale = 0
            'nRecord = 0
            'For Each dr_far In _PrintFatturaDataset.TBL_fattura_row.Rows
            '    If dr_far.FAR_id_sez_orig = 37 And Not dr_far.FAR_row_desc Then
            '        nTotale += dr_far.FAR_importo
            '        nRecord += 1
            '    End If
            'Next
            '_dr_dfa_pf.DFA_dett_accise = nRecord + nRigheAddizionaliAccise > 12
            'If (_dr_dfa_pf.DFA_dett_accise) Then
            '    '
            '    ' Aggiunge il totale della fattura
            '    '
            '    dr_tfa1 = dt_tfa.NewRow
            '    dr_tfa1.TFA_cod_mai = nCodMain
            '    dr_tfa1.TFA_tipo_riep = 2
            '    dr_tfa1.TFA_tipo_esp = 4
            '    dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_imposte_iva
            '    dr_tfa1.TFA_descrizione = "Totale accise, addizionali (*)".ToUpper
            '    dr_tfa1.TFA_importo = nTotale
            '    dr_tfa1.TFA_iva = 0
            '    dt_tfa.Rows.Add(dr_tfa1)
            'Else
            '    nRecord = 0

            '    cDesc = ""
            '    While nRecord < _PrintFatturaDataset.TBL_fattura_row.Rows.Count
            '        dr_far = _PrintFatturaDataset.TBL_fattura_row.Rows(nRecord)
            '        If dr_far.FAR_id_sez_orig = 37 And Not dr_far.FAR_row_desc Then
            '            dr_tfa1 = dt_tfa.NewRow
            '            dr_tfa1.TFA_cod_mai = nCodMain
            '            dr_tfa1.TFA_tipo_riep = 2
            '            dr_tfa1.TFA_tipo_esp = 4
            '            dr_tfa1.TFA_sortfield = eSortRiepValue.SRV_imposte_iva
            '            dr_tfa1.TFA_descrizione = String.Concat(cDesc, " ", dr_far.FAR_des_voce.ToLower, " su ", dr_far.FAR_uni_mis, " ", dr_far.FAR_quantita1, " a ", dr_far.FAR_prezzo_uni, " �/", dr_far.FAR_uni_mis)
            '            dr_tfa1.TFA_importo = dr_far.FAR_importo
            '            dr_tfa1.TFA_iva = 0
            '            dt_tfa.Rows.Add(dr_tfa1)
            '            dr_far.Delete()
            '            nRecord -= 1
            '        ElseIf dr_far.FAR_id_sez_orig = 37 And dr_far.FAR_row_desc Then
            '            cDesc = dr_far.FAR_des_voce
            '            dr_far.Delete()
            '            nRecord -= 1
            '        End If
            '        nRecord += 1
            '    End While
            'End If


            '
            ' Procedo con l'ordinamento dei record dei totali e con il loro inserimento nella tabella dei totali della fattura.
            '
            dv_srt = New DataView(dt_tfa)
            dv_srt.Sort = "TFA_sortfield"
            nTotale = 0
            'If Not lPrimaUnica Then
            '    _dr_dfa_pf.DFA_totale_fattura = 0
            'End If
            _dr_dfa_pf.DFA_dett_accise = dt_tfa.Rows.Count > 15
            For Each drv_tfa As DataRowView In dv_srt
                dr_tfa1 = CType(drv_tfa.Row, DS_PF_UE_ele.TBL_tot_fat_t2Row)
                nTotale += dr_tfa1.TFA_iva
                _PrintFatturaDataset.TBL_tot_fat_t2.ImportRow(dr_tfa1)
                'If Not lPrimaUnica Then
                '    '
                '    ' Calcola il totale della sottofattura in caso di fatture multipos/pdr
                '    '
                '    _dr_dfa_pf.DFA_totale_fattura += dr_tfa1.TFA_importo + dr_tfa1.TFA_iva
                'End If
            Next
            'adr_tfa_tmp2 = _PrintFatturaDataset.TBL_tot_fat_t2.Select(String.Concat("TFA_cod_mai = ", nCodMain, " and TFA_sortfield = ", CInt(eSortRiepValue.SRV_totale_imposte_iva)))
            'If adr_tfa_tmp2.Length = 1 Then
            '    adr_tfa_tmp2(0).TFA_importo += nTotale
            '    _PrintFatturaDataset.TBL_tot_fat_t2.AcceptChanges()
            'End If
            'adr_tfa_tmp1 = _PrintFatturaDataset.TBL_tot_fat_t1.Select(String.Concat("TFA_cod_mai = ", nCodMain, " and TFA_sortfield = ", CInt(eSortRiepValue.SRV_totale_imposte_iva)))
            'If adr_tfa_tmp1.Length = 1 Then
            '    adr_tfa_tmp1(0).TFA_importo = adr_tfa_tmp2(0).TFA_importo
            '    _PrintFatturaDataset.TBL_tot_fat_t1.AcceptChanges()
            'End If
            'If Not lPrimaUnica Then
            '    '
            '    ' Questa parte potrebbe non servire in futuro, qualora la totalizzazione dell'iva sia fatta in automatico dalla produzione dela fattura.
            '    '
            '    Dim cCondition As String = "TFA_cod_mai=" + nCodMain.ToString() + " and TFA_descrizione = 'TOTALE IMPOSTE E IVA'"
            '    If (_PrintFatturaDataset.TBL_tot_fat_t2.Select(cCondition).Length = 1) Then
            '        dr_tfa1 = _PrintFatturaDataset.TBL_tot_fat_t2.Select(cCondition)(0)
            '    Else
            '        dr_tfa1 = _PrintFatturaDataset.TBL_tot_fat_t2.NewRow
            '        dr_tfa1.TFA_cod_mai = nCodMain
            '        dr_tfa1.TFA_tipo_riep = 1
            '        dr_tfa1.TFA_descrizione = "TOTALE IMPOSTE E IVA"
            '        _PrintFatturaDataset.TBL_tot_fat_t2.Rows.Add(dr_tfa1)
            '    End If
            '    dr_tfa1.TFA_importo = nTotale
            'End If
        Catch ex As Exception
            Throw New Exception("Errore sulla prcedura: Totalizzazione_Parziale_Tipo2", ex)
        End Try

    End Sub

    Private Sub CheckFieldValue(ByVal cValue As String, ByVal cMsgError As String)

        If cValue = "" And cMsgError > "" Then
            AddErroreLieve(cMsgError)
        End If

    End Sub

    Private Sub Carica_DatiDistributore(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer)
        Dim dr_dis As DS_PF_UE_ele.TBL_distributoriRow
        Dim dr_tmp As DataRow
        Dim dr_tmp1 As DataRow

        Try
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId)
                If dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio")(0)
                    If dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura").Length > 0 Then
                        dr_tmp = dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura")(0)
                        If dr_tmp.GetChildRows("PuntoDiFornitura_SocietaDistribuzione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("PuntoDiFornitura_SocietaDistribuzione")(0)
                            If dr_tmp.GetChildRows("SocietaDistribuzione_DatiDistributore").Length > 0 Then
                                dr_tmp1 = dr_tmp.GetChildRows("SocietaDistribuzione_DatiDistributore")(0)
                                dr_dis = _PrintFatturaDataset.TBL_distributori.NewRow
                                dr_dis.DIS_cod_mai = _dr_mai_pf.MAI_codice
                                dr_dis.DIS_nome = dr_tmp1("RagioneSociale")
                                _PrintFatturaDataset.TBL_distributori.Rows.Add(dr_dis)
                            End If
                            If dr_dis IsNot Nothing Then
                                dr_dis.DIS_telefono_guasti = dr_tmp("NumeroVerdeGuasti")
                            End If
                        End If
                    End If

                End If
            End If
            'xiWork.SelectNodes(Costanti.itm_COM_DIS_nome).Count = 1 Then
            '                dr_dis = _PrintFatturaDataset.TBL_distributori.NewRow
            '                dr_dis.DIS_cod_mai = _dr_mai_pf.MAI_codice
            '                dr_dis.DIS_nome = GetValueFromXML(xiWork, Costanti.itm_COM_DIS_nome, "", "Nome del distributore")
            '                dr_dis.DIS_telefono_guasti = GetValueFromXML(xiWork, Costanti.itm_COM_DIS_telefono, "", "Telefono del distributore")
            '                _PrintFatturaDataset.TBL_distributori.Rows.Add(dr_dis)
            '            Else
            '                For Each xnTmp As Xml.XmlNode In xiWork.SelectNodes(Costanti.SEZ_elenco_distrib)
            '                    If _TmpDistributore.Contains(GetValueFromXML(xnTmp, Costanti.itm_DIS_nome)) Then
            '                        dr_dis = _PrintFatturaDataset.TBL_distributori.NewRow
            '                        dr_dis.DIS_cod_mai = _dr_mai_pf.MAI_codice
            '                        dr_dis.DIS_nome = GetValueFromXML(xnTmp, Costanti.itm_DIS_nome, "", "Nome del distributore")
            '                        dr_dis.DIS_telefono_guasti = GetValueFromXML(xnTmp, Costanti.itm_DIS_telefono, "", "Telefono del distributore")
            '                        _PrintFatturaDataset.TBL_distributori.Rows.Add(dr_dis)
            '                    End If
            '                Next
            '            End If
        Catch ex As Exception
            SetExceptionItem("Carica_DatiDistributore", ex)
        End Try

    End Sub

    Private Sub Carica_Consumi(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer)
        Dim dt_conAPP As DS_PF_UE_ele.TBL_consumiDataTable
        Dim dt_con As DS_PF_UE_ele.TBL_consumiDataTable
        Dim aDR_con() As DS_PF_UE_ele.TBL_consumiRow
        Dim dr_con As DS_PF_UE_ele.TBL_consumiRow
        Dim nRowCon_Codice As Integer
        Dim nFasceEUnica As Integer
        Dim cTmpString As String
        Dim nTipoOper As Integer
        Dim nTotPerc As Decimal
        Dim dr_tmp2 As DataRow
        Dim nTotale As Decimal
        Dim dv_con As DataView
        Dim lInsert As Boolean
        Dim dTmp As DateTime
        Dim lExit As Boolean
        Dim nDif As Integer
        Dim i As Integer

        Try
            ' ************************************************************************** '
            ' Caricamento dei dati dei consumi.                                          '
            ' ************************************************************************** '
            _dr_dfa_pf.DFA_xfasce = False
            nRowCon_Codice = 1
            If _PrintFatturaDataset.TBL_consumi.Rows.Count > 0 Then
                nRowCon_Codice = _PrintFatturaDataset.TBL_consumi.Compute("MAX(CON_codice)", Nothing) + 1
            End If
            dt_conAPP = New DS_PF_UE_ele.TBL_consumiDataTable
            dt_conAPP.Columns("CON_codice").AutoIncrementSeed = nRowCon_Codice
            For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId).GetChildRows("SezioniDocumenti_ConsumiFatturati")
                nTipoOper = -1
                lInsert = False
                'LP 20120510 *************************************************************** '
                ' Vista la richiesta di modificare l'esposizione delle letture da parte di   '
                ' UE Prima valuto se il record deve essere inserito in tabella e poi even-   ' 
                ' tualmente lo inserisco. La modifica riportata all'interno era gila stata e '
                ' seguita in precedenza.                                                     '
                ' ************************************************************************** '
                cTmpString = dr_tmp1("OperazioneCalcolo").ToString.ToLower
                If (cTmpString = "fatturazione") Then
                    nTipoOper = 0
                    lInsert = True
                ElseIf (cTmpString = "rettifica") Then
                    nTipoOper = 2
                    If dr_tmp1.GetChildRows("ConsumiFatturati_Motivi").Length > 0 Then
                        dr_tmp2 = dr_tmp1.GetChildRows("ConsumiFatturati_Motivi")(0)
                        If (_dr_dfa_pf.DFA_cod_motivo = 0) Or (_dr_dfa_pf.DFA_cod_motivo = dr_tmp2("codice")) Then
                            _dr_dfa_pf.DFA_cod_motivo = dr_tmp2("codice")
                            _dr_dfa_pf.DFA_motivo_ricalcolo = dr_tmp2("descrizione")
                        Else
                            Throw New Exception("Troppi motivi per ricalcolo")
                        End If
                    End If
                    lInsert = True
                ElseIf (cTmpString = "rettifica - rifatturazione") Then
                    nTipoOper = 3
                End If
                If lInsert Then
                    'LP 20110516 *************************************************************** '
                    ' Rimozione richiesta per problemi di UE nel passaggio dei dati.             '
                    ' ************************************************************************** '
                    cTmpString = dr_tmp1("FunzioneConsumo")
                    lInsert = Not ((cTmpString = "POT_A6_MT") Or (cTmpString = "POT_A6_BT"))

                    cTmpString = dr_tmp1("Fascia")
                    lInsert = lInsert And Not (cTmpString.Contains("PEAK") And Not _lPeakOnOff)
                End If

                'LP 20120516 *************************************************************** '
                ' Se la riga deve essere creata procedo con la creazione, altrimenti la sal- ' 
                ' to e procedo con la succcessiva.                                           '
                ' ************************************************************************** '
                If lInsert Then
                    dr_con = dt_conAPP.NewRow
                    dr_con.CON_cod_mai = _dr_mai_pf.MAI_codice
                    dr_con.CON_tipo_oper = nTipoOper
                    dr_con.CON_cod_tipo_con = 1
                    dr_con.CON_tipo_con = dr_tmp1("TipoConsumo")
                    If dr_con.CON_tipo_con.ToString.ToLower.Contains("stimato") Then
                        dr_con.CON_cod_tipo_con = 2
                    End If
                    Try
                        If dr_tmp1("Provenienza").ToString.ToLower = "auto lettura" Then
                            dr_con.CON_tipo_con = String.Concat(dr_con.CON_tipo_con, " autolettura")
                        End If
                    Catch ex As Exception

                    End Try
                    dr_con.CON_dal = dr_tmp1("DataInizio")
                    dr_con.CON_al = dr_tmp1("DataFine")

                    dr_con.CON_id_fconsumo = dr_tmp1("FunzioneConsumo").ToString.ToUpper
                    dr_con.CON_des_fconsumo = dr_tmp1("FunzioneConsumo")
                    dr_con.CON_des_fcon = dr_tmp1("FunzioneConsumo")
                    If (dr_con.CON_id_fconsumo.ToString.ToLower = "att") Or (dr_con.CON_id_fconsumo.ToString.ToLower = "rea") Then
                        dr_con.CON_des_fcon = String.Concat("ENERGIA ", dr_con.CON_des_fconsumo)
                    End If

                    Select Case dr_con.CON_id_fconsumo.ToLower
                        Case Is = "att"
                            dr_con.CON_des_fconsumo = "Attiva"
                        Case Is = "pot"
                            dr_con.CON_des_fconsumo = "Potenza"
                        Case Is = "rea"
                            dr_con.CON_des_fconsumo = "Reattiva"
                    End Select

                    dr_con.CON_id_fascia = dr_tmp1("Fascia")
                    dr_con.CON_des_fascia = dr_tmp1("Fascia")
                    _dr_dfa_pf.DFA_xfasce = _dr_dfa_pf.DFA_xfasce Or (dr_con.CON_id_fascia = "F1" Or dr_con.CON_id_fascia = "F2" Or dr_con.CON_id_fascia = "F3")
                    If dr_tmp1("LetturaPrecedente") = "" Then
                        dr_con.CON_lettura_da = 0
                    Else
                        dr_con.CON_lettura_da = dr_tmp1("LetturaPrecedente")
                    End If
                    If dr_tmp1("LetturaAttuale") = "" Then
                        dr_con.CON_lettura_a = 0
                    Else
                        dr_con.CON_lettura_a = dr_tmp1("LetturaAttuale")
                    End If
                    dr_con.CON_consumo = dr_tmp1("ValoreConsumo")
                    If dr_con.CON_tipo_oper > 1 Then
                        dr_con.CON_consumo = dr_tmp1("ValoreConsumo") * -1
                    End If
                    dr_con.CON_um_consumo = dr_tmp1("UM")
                    dr_con.CON_costante = 1
                    If dr_con.CON_id_fconsumo.ToLower = "att" Then
                        dr_con.CON_costante = _dr_stm.STM_cost_att
                        nTotale += dr_con.CON_consumo
                    End If
                    If dr_con.CON_id_fconsumo.ToLower = "rea" Then
                        dr_con.CON_costante = _dr_stm.STM_cost_rea
                    End If
                    If dr_con.CON_id_fconsumo.ToLower = "pot" Then
                        dr_con.CON_costante = _dr_stm.STM_cost_pot
                    End If
                    'LP 20120516 *************************************************************** '
                    ' Se il contatore � orario invece di far uscire 0 sulla lettura a si deve in ' 
                    ' dicare il valore del consumo in modo che i conti siano corretti.           '
                    ' ************************************************************************** '
                    If _GRP_orario Then
                        dr_con.CON_lettura_a = dr_con.CON_consumo
                    End If
                    dt_conAPP.Rows.Add(dr_con)
                End If
            Next

            If _dr_dfa_pf.DFA_xfasce Then
                i = 0
                While i < dt_conAPP.Rows.Count
                    aDR_con = dt_conAPP.Select(String.Concat("(CON_tipo_oper = ", dt_conAPP.Rows(i)("CON_tipo_oper"), ")and  (CON_dal = '", dt_conAPP.Rows(i)("CON_dal"), "') and (CON_al = '", dt_conAPP.Rows(i)("CON_al"), "')"))
                    nTotale = 0
                    nTotPerc = 0
                    nFasceEUnica = 0
                    For Each dr_con In aDR_con
                        If dr_con.CON_id_fascia.ToUpper = "F1" Or dr_con.CON_id_fascia.ToUpper = "F2" Or dr_con.CON_id_fascia.ToUpper = "F3" Then
                            nFasceEUnica = nFasceEUnica Or 1
                        End If
                        If dr_con.CON_id_fascia.ToUpper = "FU" Then
                            nFasceEUnica = nFasceEUnica Or 2
                        End If
                        If (dr_con.CON_id_fconsumo.ToLower = "att") And (dr_con.CON_id_fascia.ToUpper <> "FU") Then
                            nTotale += dr_con.CON_consumo
                        End If
                    Next
                    If (nFasceEUnica And 1) = 1 And (nFasceEUnica And 2) = 2 Then
                        For Each dr_con In aDR_con
                            If dr_con.CON_id_fascia.ToUpper = "FU" Then
                                dr_con.CON_cod_tipo_con = -100
                            End If
                        Next
                    End If
                    If nTotale > 0 Then
                        For Each dr_con In aDR_con
                            If dr_con.CON_id_fconsumo.ToLower = "att" And dr_con.CON_id_fascia.ToUpper = "F1" Then
                                dr_con.CON_perc = Math.Round(dr_con.CON_consumo / nTotale * 100, 0)
                                nTotPerc += dr_con.CON_perc
                            End If
                            If dr_con.CON_id_fconsumo.ToLower = "att" And dr_con.CON_id_fascia.ToUpper = "F2" Then
                                dr_con.CON_perc = 100 - nTotPerc
                                nTotPerc += dr_con.CON_perc
                            End If
                        Next
                        If Not (nTotPerc = 100) Then
                            If nTotPerc > 100 Then
                                nDif = -1
                            Else
                                nDif = 1
                            End If
                            For Each dr_con In aDR_con
                                If dr_con.CON_id_fconsumo.ToLower = "att" And Not (nTotPerc = 100) And (dr_con.CON_id_fascia = "F1" Or dr_con.CON_id_fascia = "F2") Then
                                    dr_con.CON_perc += (1 * nDif)
                                    nTotPerc += (1 * nDif)
                                End If
                            Next
                        End If
                    End If
                    i += aDR_con.Length
                End While
            End If
            dt_conAPP.AcceptChanges()

            ' ************************************************************************** '
            ' Caricamento dei dati per le tabelle sul retro.                             '
            '                                                                            '
            ' Ordinamento della tabella dei consumi, calcolo del totale dei consumi e ri '
            ' levazione della tipologia di fattura.                                      '
            ' _dr_dfa_pf.DFA_tipo_fat 1 => Conguaglio                                    '
            '                         2 => Acconto                                       '
            '                         3 => Conguaglio + Acconto                          '
            ' ************************************************************************** '
            dv_con = New DataView(dt_conAPP)
            dv_con.Sort = "CON_tipo_oper DESC,CON_dal,CON_al,CON_des_fcon,CON_id_fascia"

            For Each drv As DataRowView In dv_con
                dr_con = CType(drv.Row, DS_PF_UE_ele.TBL_consumiRow)
                If Not (dr_con.CON_cod_tipo_con = -100) Then
                    If dr_con.CON_tipo_oper = 0 Or dr_con.CON_tipo_oper = 2 Then
                        If (dr_con.CON_id_fconsumo.ToLower = "att") And (dr_con.CON_id_fascia.ToUpper = "FU" Or dr_con.CON_id_fascia.ToUpper = "F1" Or dr_con.CON_id_fascia.ToUpper = "F2" Or dr_con.CON_id_fascia.ToUpper = "F3") Then
                            If _dr_dfa_pf.DFA_con_um = "" Then
                                _dr_dfa_pf.DFA_con_um = dr_con.CON_um_consumo
                            End If
                            ' il calcolo del totale � necessari modificarlo in quanto i consumi gi� fatturati sono ora con valore negativo.
                            'If dr_con.CON_tipo_oper = 0 Then
                            '    _dr_dfa_pf.DFA_con_val += dr_con.CON_consumo
                            'Else
                            '    _dr_dfa_pf.DFA_con_val -= dr_con.CON_consumo
                            'End If
                            _dr_dfa_pf.DFA_con_val += dr_con.CON_consumo
                        End If
                    End If

                    dr_con.CON_codice = dr_con.CON_codice * -1
                    _PrintFatturaDataset.TBL_consumi.ImportRow(dr_con)

                    dr_con = _PrintFatturaDataset.TBL_consumi.Rows(_PrintFatturaDataset.TBL_consumi.Rows.Count - 1)
                    dr_con.CON_codice = nRowCon_Codice
                    nRowCon_Codice += 1
                End If
            Next
            _PrintFatturaDataset.TBL_consumi.AcceptChanges()

            '
            ' Determinazione del tipo fattura.
            '
            i = dv_con.Count - 1
            _dr_dfa_pf.DFA_tipo_fat = Costanti.TipoFattura_NoTi
            While (i > -1) And Not ((_dr_dfa_pf.DFA_tipo_fat And Costanti.TipoFattura_Cong) = Costanti.TipoFattura_Cong)
                dr_con = CType(dv_con(i).Row, DS_PF_UE_ele.TBL_consumiRow)
                If dr_con.CON_tipo_oper = 0 Then
                    If _dr_dfa_pf.DFA_tipo_fat = Costanti.TipoFattura_NoTi Then
                        'If dr_con.CON_tipo_con.ToLower.StartsWith("rilevato") Then
                        '    _dr_dfa_pf.DFA_tipo_fat = Costanti.TipoFattura_Cong
                        'End If
                        If dr_con.CON_tipo_con.ToLower.StartsWith("stimato") Then
                            _dr_dfa_pf.DFA_tipo_fat = Costanti.TipoFattura_Acco
                        End If
                    Else
                        If dr_con.CON_tipo_con.ToLower.StartsWith("rilevato") Then
                            _dr_dfa_pf.DFA_tipo_fat = _dr_dfa_pf.DFA_tipo_fat Or Costanti.TipoFattura_Cong
                        End If
                        If dr_con.CON_tipo_con.ToLower.StartsWith("stimato") Then
                            _dr_dfa_pf.DFA_tipo_fat = _dr_dfa_pf.DFA_tipo_fat Or Costanti.TipoFattura_Acco
                        End If
                    End If
                ElseIf dr_con.CON_tipo_oper >= 2 Then
                    _dr_dfa_pf.DFA_tipo_fat = Costanti.TipoFattura_Cong
                End If
                i -= 1
            End While

            'If (_dr_dfa_pf.DFA_tipo_fat = Costanti.TipoFattura_NoTi) Then
            '    _dr_dfa_pf.DFA_tipo_fat = Costanti.TipoFattura_Cong
            'End If

            ' ************************************************************************** '
            ' Individuazione delle letture che andranno sul fronte.                      '
            ' ************************************************************************** '
            dt_con = New DS_PF_UE_ele.TBL_consumiDataTable
            For Each dr_con In dt_conAPP.Rows
                If (dr_con.CON_id_fconsumo.ToLower = "att" And dr_con.CON_tipo_oper = 0) And Not (dr_con.CON_cod_tipo_con = -100) Then
                    dt_con.ImportRow(dr_con)
                    dr_con.CON_codice = dr_con.CON_codice * -1
                End If
            Next
            dt_con.AcceptChanges()

            If dt_con.Rows.Count <= 1 Then
                If dt_con.Rows.Count = 1 Then
                    dr_con = dt_con.Rows(0)
                    dr_con.CON_tipo_oper = -1
                End If
            Else
                dv_con = New DataView(dt_con)
                dv_con.Sort = "CON_al DESC,CON_des_fcon,CON_id_fascia"

                i = 0
                dr_con = CType(dv_con(i).Row, DS_PF_UE_ele.TBL_consumiRow)
                dTmp = dr_con.CON_al

                lExit = False
                While Not lExit
                    i += 1
                    If dv_con.Count > i Then
                        dr_con = CType(dv_con(i).Row, DS_PF_UE_ele.TBL_consumiRow)
                        If dr_con.CON_al <> dTmp Then
                            dr_con.Delete()
                            i -= 1
                        End If
                    Else
                        lExit = True
                    End If
                End While
            End If
            dt_con.AcceptChanges()

            dv_con = New DataView(dt_con)
            dv_con.Sort = "CON_dal, CON_al, CON_des_fcon, CON_id_fascia"
            For Each drv As DataRowView In dv_con
                dr_con = CType(drv.Row, DS_PF_UE_ele.TBL_consumiRow)
                dr_con.CON_tipo_oper = -1
                If dr_con.CON_tipo_con.ToLower = "rilevato con autolettura" Then
                    dr_con.CON_tipo_con = dr_con.CON_tipo_con.Replace(" con ", " ")
                End If
                _PrintFatturaDataset.TBL_consumi.ImportRow(dr_con)
                dr_con = _PrintFatturaDataset.TBL_consumi.Rows(_PrintFatturaDataset.TBL_consumi.Rows.Count - 1)
                dr_con.CON_codice = nRowCon_Codice
                nRowCon_Codice += 1
            Next
            _PrintFatturaDataset.TBL_consumi.AcceptChanges()
        Catch ex As Exception
            _ErroriGravi = String.Concat(_ErroriGravi, "Carica_Consumi", ex, vbCr)
        End Try

    End Sub

    ' ************************************************************************** '
    ' Caricamento dei dati riguardanti la fattura.                               '
    ' ************************************************************************** '
    Private Sub Carica_DatiFattura(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer, ByVal lPrimaUnica As Boolean)
        Dim lAlreadyInTable As Boolean

        Try
            lAlreadyInTable = _PrintFatturaDataset.TBL_dati_fatt.Select(String.Concat("DFA_cod_mai = '", _dr_mai_pf.MAI_codice, "'")).Length > 0
            If lAlreadyInTable Then
                _dr_dfa_pf = _PrintFatturaDataset.TBL_dati_fatt.Select(String.Concat("DFA_cod_mai = ", _dr_mai_pf.MAI_codice))(0)
            Else
                _dr_dfa_pf = _PrintFatturaDataset.TBL_dati_fatt.NewRow
                _dr_dfa_pf.DFA_cod_mai = _dr_mai_pf.MAI_codice
            End If
            With _dr_dfa_pf
                .DFA_cod_mai = _dr_mai_pf.MAI_codice
                .DFA_desc_tipo_pag = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Dilazione", "MezzoPagamento")
                If .DFA_desc_tipo_pag.ToLower.Replace(" ", "") = "rimessadiretta" Then
                    .DFA_flag_tipo_pag = "R"
                ElseIf .DFA_desc_tipo_pag.ToLower.Replace(" ", "") = "bonificobancario" Then
                    .DFA_flag_tipo_pag = "B"
                ElseIf .DFA_desc_tipo_pag.ToLower.Replace(" ", "") = "domiciliazionebancaria" Then
                    .DFA_flag_tipo_pag = "D"
                Else
                    .DFA_flag_tipo_pag = ""
                End If
                .DFA_imp_rate = False

                ' ************************************************************************** '
                ' Recupera i dati per il pagamento tramite Bonifico Bancario.                '
                ' ************************************************************************** '
                If .DFA_flag_tipo_pag = "B" Then
                    .DFA_nome_banca = GetTagXML(dr_fat, New String() {"Fattura", "DatiPagamento", "Dilazione", "BancaAppoggio", "Banca"})
                    .DFA_iban = GetTagXML(dr_fat, New String() {"Fattura", "DatiPagamento", "Dilazione", "BancaAppoggio", "IBAN"})
                End If

                ' ************************************************************************** '
                ' Recupera i dati per il pagamento tramite Domiciliazione Bancaria.          '
                ' ************************************************************************** '
                If .DFA_flag_tipo_pag = "D" Then
                    .DFA_nome_banca = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "BancaCliente", "Banca")
                    .DFA_iban = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "BancaCliente", "IBAN")
                End If

                .DFA_totale_fattura = RecuperaValoreXML(lPrimaUnica, "RiepilogoIVA", "TotaleFattura")

                .DFA_TotaleAssoggettatoIVA = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "CorrispettiviIVA", "TotaleAssoggettatoIVA", "0")
                .DFA_registro_iva = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "Protocollo")
                .DFA_anno = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "AnnoFattura")
                .DFA_numero = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "NumeroFattura")
                .DFA_anno_rett = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "AnnoFatturaRettificata", 0)
                .DFA_numero_rett = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "NumeroFatturaRettificata")
                .DFA_scadenza = SetDataScadenza(.DFA_flag_tipo_pag, RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Rate", "DataScadenza"))
                .DFA_emissione = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "DatiIdentificativiFattura", "DataFattura")
                .DFA_fat_dettaglio = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "FatturaDettaglio", "").ToString().ToUpper() = "SI"
            End With
            If Not lAlreadyInTable Then
                _PrintFatturaDataset.TBL_dati_fatt.Rows.Add(_dr_dfa_pf)
            End If
        Catch ex As Exception
            _ErroriGravi = String.Concat(_ErroriGravi, "Errore nel caricamento dei dati della Fattura (DFA)", ex, vbCr)
            'Throw New Exception("Errore nel caricamento dei dati della Fattura (DFA)", ex)
        End Try

    End Sub



    ' ********************************************************************* '
    ' Valorizza i campi che hanno a che fare con i dati della fornitura.    '
    ' ********************************************************************* '
    Private Sub Carica_DatiFornitura(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer, ByVal nNumPodFatturati As Integer)
        Dim dr_daf As DS_PF_UE_ele.TBL_dati_fornRow
        Dim cTmpString As String
        Dim cResidente As String
        Dim cParacadute As String
        Dim dTmp As DateTime

        Try
            dr_daf = Nothing
            _NomeOfferta = ""
            ' ********************************************************************* '
            ' Creazione del record per la memorizzazione dei dati della fornitura   '
            ' relativi ad un POD.                                                   '
            ' ********************************************************************* '
            dr_daf = _PrintFatturaDataset.TBL_dati_forn.NewRow
            With dr_daf
                .DAF_cod_mai = _dr_mai_pf.MAI_codice

                If _dr_mai_pf.DEF_raccolta > 1 Then
                    ' ********************************************************************* '
                    ' La descrizione del contratto viene caricata successivamente in dipen- ' 
                    ' denza di alcuni parametri.                                            '
                    ' ********************************************************************* '
                    .DAF_anno_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "AnnoContratto")
                    .DAF_num_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "NumeroContratto")
                    .DAF_desc_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "TipoContratto")
                    .DAF_contratto_attivo = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "StatoContratto").ToString.ToUpper = "ATTIVO"
                    .DAF_domestico = .DAF_desc_contratto.ToLower.Contains("domestico")
                Else
                    .DAF_desc_contratto = Costanti.itm_fattura_multi
                    .DAF_anno_contratto = ""
                    .DAF_num_contratto = ""
                    .DAF_contratto_attivo = True
                    .DAF_domestico = False
                End If
                .DAF_cdc = ""
                .DAF_nome_offerta = "MERCATO DI MAGGIOR TUTELA"

                ' ********************************************************************* '
                ' Dati recuperati dalla sezione del POD.                                '
                ' ********************************************************************* '
                If _dr_mai_pf.DEF_raccolta = 1 Then
                    dr_daf.DAF_pod = String.Concat("Totale POD serviti ", nNumPodFatturati)
                Else
                    dr_daf.DAF_pod = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "PuntoDiFornitura", "POD-PDR")

                    ' ********************************************************************* '
                    ' Dati recuperati dalla sezione della fattura.                          '
                    ' ********************************************************************* '

                    ' ********************************************************************* '
                    ' Ubicazione del sito dove viene fornita l'Energia Elettrica.           '
                    ' ********************************************************************* '
                    .DAF_descr_sito_f = ""

                    .DAF_ub_sito_1 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "IndirizzoPuntoDiFornitura", "TipoElementoTopologico")
                    .DAF_ub_sito_2 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "IndirizzoPuntoDiFornitura", "ElementoTopologico")
                    .DAF_ub_sito_3 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "IndirizzoPuntoDiFornitura", "NumeroCivico")
                    .DAF_ub_sito_4 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "IndirizzoPuntoDiFornitura", "SuffissoCivico")
                    .DAF_ub_sito_5 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "IndirizzoPuntoDiFornitura", "Localita")
                    .DAF_ub_sito_6 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "IndirizzoPuntoDiFornitura", "Cap")
                    .DAF_ub_sito_7 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "IndirizzoPuntoDiFornitura", "Comune")
                    .DAF_ub_sito_8 = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "IndirizzoPuntoDiFornitura", "ProvinciaSigla")

                    ' ********************************************************************* '
                    ' Inizio fornitura dell'Energia Elettrica.                              '
                    ' ********************************************************************* '
                    .DAF_inizio_forn = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "Contratto", "DataAttivazione")

                    ' Rispetto normativa 205/2014/R/eel (articolo 5, comma 1)
                    .DAF_205_2014_R_eel = False
                    If .DAF_domestico Then
                        If DateTime.Parse(.DAF_inizio_forn) < DateTime.Parse("30/09/2015") Then
                            dTmp = DateTime.Parse(.DAF_inizio_forn)
                            If dTmp < DateTime.Parse("01/06/2014") Then
                                dTmp = DateTime.Parse("01/06/2014")
                            End If
                            .DAF_205_2014_R_eel = dTmp.AddMonths(5) > DateTime.Parse(_dr_dfa_pf.DFA_emissione)
                        End If
                    End If

                    If Not .DAF_contratto_attivo Then
                        .DAF_cess_forn = RecuperaValoreDS_UE(ds_src, dr_fat, nPodId, "PeriodiFatturazione", "DataFineConsumiFatturati")
                    End If

                    ' ********************************************************************* '
                    ' Caricamento dei dati dalla tabella del contratto.                     '
                    ' ********************************************************************* '
                    'For Each xnWork1 In xiWorkGlob.SelectNodes(Costanti.itm_campi_contr)
                    '    Select Case xnWork1.SelectSingleNode("DESCR_CAMPO").InnerText
                    '        Case Is = "RESIDENTE"
                    '            If GetValueFromXML(xnWork1, "VALORE_CAMPO").ToUpper = "NO" Then
                    '                dr_daf.DAF_desc_contratto = String.Concat(dr_daf.DAF_desc_contratto, " NON")
                    '            End If
                    '            dr_daf.DAF_desc_contratto = String.Concat(dr_daf.DAF_desc_contratto, " ", GetValueFromXML(xnWork1, "DESCR_CAMPO"))
                    '        Case Is = "CENTRO DI COSTO"
                    '            dr_daf.DAF_cdc = GetValueFromXML(xnWork1, "VALORE_CAMPO")
                    '        Case Is = "DICHIARAZIONE D'INTENTO IVA"
                    '            _dr_dfa_pf.DFA_int_iva = GetValueFromXML(xnWork1, "VALORE_CAMPO")
                    '    End Select
                    'Next

                    _dr_dfa_pf.DFA_int_iva = ""
                    ' ********************************************************************* '
                    ' Caricamenti valori dalle opzioni tariffarie.                          '
                    ' ********************************************************************* '
                    For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId).GetChildRows("SezioniDocumenti_PuntoDiFornitura")
                        For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("PuntoDiFornitura_GruppoMisura")

                            For Each dr_tmp3 As DataRow In dr_tmp2.GetChildRows("GruppoMisura_StrumentoMisura")


                            Next
                        Next
                    Next

                    Dim dr_sez_doc As DataRow
                    dr_sez_doc = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId)
                    ' ********************************************************************* '
                    ' Recupero costo medio della fornitura e della materia prima.           '
                    ' ********************************************************************* '
                    Try
                        dr_daf.DAF_CMU_bolletta = dr_sez_doc.Item("CostoMedioUnitarioBolletta")
                    Catch ex As Exception

                    End Try
                    Try
                        dr_daf.DAF_CMU_SpesaEnergia = dr_sez_doc.Item("CostoMedioUnitarioSpesaEnergia")
                    Catch ex As Exception

                    End Try
                    _lPeakOnOff = False
                    For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId).GetChildRows("SezioniDocumenti_Contratto")
                        For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("Contratto_OT")
                            For Each dr_tmp3 As DataRow In dr_tmp2.GetChildRows("OT_ValoreParametroOT")
                                Try
                                    Select Case dr_tmp3("Campo").ToString.ToLower
                                        Case Is = "NOME OFFERTA".ToLower
                                            dr_daf.DAF_nome_offerta = dr_tmp3("ValoreCampo")
                                            _NomeOfferta = dr_daf.DAF_nome_offerta.ToLower
                                            If GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si" Then
                                                _NomeOfferta = "SCACCIAPENSIERI"
                                            End If
                                            _NomeOfferta = _NomeOfferta.ToLower
                                        Case Is = "PREZZO FISSO PEAK OFF".ToLower
                                            _lPeakOnOff = True
                                        Case Is = "AMMISSIONE BONUS SOCIALE ELETTRICO".ToLower
                                            _dr_dfa_pf.DFA_bonus_dis_eco = dr_tmp3("ValoreCampo").ToString.ToUpper = "SI"
                                            _dr_dfa_pf.DFA_bonus_de_txt = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_soc_de)
                                        Case Is = "DATA LIMITE RICHIESTA RINNOVO BONUS SOCIALE (COMUNE)".ToLower
                                            _dr_dfa_pf.DFA_bonus_rinnovo = dr_tmp3("ValoreCampo")
                                    End Select
                                Catch ex As Exception

                                End Try
                            Next
                            If dr_tmp2("ComponenteServizio") = "TRASPORTO" Then
                                dr_daf.DAF_tar_distr = dr_tmp2("OT1")
                            End If
                        Next
                        cResidente = "*"
                        For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("Contratto_ValoreCampoContratto")
                            Try
                                Select Case dr_tmp2("Campo").ToString.ToLower
                                    Case Is = "DICHIARAZIONE D'INTENTO IVA".ToLower
                                        _dr_dfa_pf.DFA_int_iva = dr_tmp2("ValoreCampo")
                                    Case Is = "CENTRO DI COSTO".ToLower
                                        dr_daf.DAF_cdc = dr_tmp2("ValoreCampo")
                                    Case Is = "RESIDENTE".ToLower
                                        If dr_tmp2("DataFine") = "31/12/9999" Then
                                            cResidente = dr_tmp2("ValoreCampo")
                                        End If
                                End Select
                            Catch ex As Exception

                            End Try
                        Next
                        If cResidente <> "*" Then
                            If cResidente = "NO" Then
                                dr_daf.DAF_desc_contratto = String.Concat(dr_daf.DAF_desc_contratto, " NON")
                            End If
                            dr_daf.DAF_desc_contratto = String.Concat(dr_daf.DAF_desc_contratto, " RESIDENTE")
                        End If
                    Next

                    For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId).GetChildRows("SezioniDocumenti_DatiServizio")(0).GetChildRows("DatiServizio_PuntoDiFornitura")
                        For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("PuntoDiFornitura_ValoreCampoPunto")
                            Try
                                cTmpString = dr_tmp2("Campo").ToString.ToUpper
                                If (cTmpString = "POTENZA IMPEGNATA") Then
                                    dr_daf.DAF_pot_imp_val = dr_tmp2("ValoreCampo")
                                    'dr_daf.DAF_pot_imp_um = dr_tmp2("UM")
                                    dr_daf.DAF_pot_imp_um = "kW"
                                ElseIf (cTmpString = "POTENZA DISPONIBILE") Then
                                    dr_daf.DAF_pot_dis_val = dr_tmp2("ValoreCampo")
                                    'dr_daf.DAF_pot_dis_um = dr_tmp2("UM")
                                    dr_daf.DAF_pot_dis_um = "kW"
                                ElseIf (cTmpString = "TENSIONE VOLT") Then
                                    dr_daf.DAF_tensione_forn = dr_tmp2("ValoreCampo")
                                ElseIf (cTmpString = "TIPO FORNITURA EE") Then
                                    dr_daf.DAF_tensione_tipo = dr_tmp2("ValoreCampo")
                                End If
                            Catch ex As Exception

                            End Try
                        Next
                    Next
                End If
            End With

            'LP 20120510 *************************************************************** '
            ' Modifiche della descrizione del tipo di contratto per la parte domestica.  '
            ' ************************************************************************** '
            If dr_daf.DAF_desc_contratto.ToUpper.Contains("DOMESTICO") Then
                If dr_daf.DAF_desc_contratto.ToUpper.Contains("NON RESIDENTE") Then
                    dr_daf.DAF_desc_contratto = "Utenza Domestica Non Residente"
                ElseIf dr_daf.DAF_desc_contratto.ToUpper.Contains("RESIDENTE") Then
                    dr_daf.DAF_desc_contratto = "Utenza Domestica Residente"
                End If
            End If
            _PrintFatturaDataset.TBL_dati_forn.Rows.Add(dr_daf)
        Catch ex As Exception
            _ErroriGravi = String.Concat(_ErroriGravi, "Carica_DatiFornitura", ex, vbCr)
            'Throw SetExceptionItem("Carica_DatiFornitura", ex)
        End Try

    End Sub

    ' ********************************************************************* '
    ' Recupera i dati del prelievo annuo previsto.                          '
    ' ********************************************************************* '
    Private Sub Carica_prelievo_annuo(ByVal SezioneDocumento As DataRow)
        Dim dt_prelievo As DS_PF_UE_ele.TBL_prelievo_annuoDataTable
        Dim dr_pra1 As DS_PF_UE_ele.TBL_prelievo_annuoRow
        Dim dDataFine As DateTime

        dDataFine = DateTime.MinValue
        For Each dr1 As DataRow In SezioneDocumento.GetChildRows("SezioniDocumenti_PrelievoAnnuoPrevisto")
            If dDataFine <= dr1.Item("DataFine") Then
                dt_prelievo = New DS_PF_UE_ele.TBL_prelievo_annuoDataTable
                For Each dr2 As DataRow In dr1.GetChildRows("PrelievoAnnuoPrevisto_Fasce")
                    dr_pra1 = dt_prelievo.NewRow
                    dr_pra1.PRA_cod_mai = _dr_mai_pf.MAI_codice
                    dr_pra1.PRA_fascia = dr2("FasciaConsumo")
                    dr_pra1.PRA_prelievo = dr2("ValorePrelievoAnnuoPrevistoFascia")
                    dt_prelievo.Rows.Add(dr_pra1)
                Next
                dDataFine = dr1.Item("DataFine")
            End If
        Next
        If Not (dDataFine = DateTime.MinValue) Then
            For Each dr_pra1 In dt_prelievo.Rows
                _PrintFatturaDataset.TBL_prelievo_annuo.ImportRow(dr_pra1)
            Next
        End If

    End Sub

    Private Function DatoInCorsoValidita(ByVal dVerifica As DateTime, ByVal dDa As DateTime, ByVal dA As DateTime) As Boolean
        Dim lRtn As Boolean

        Return lRtn

    End Function

    ' ************************************************************************** '
    ' Recupero le righe di fatturazione da stampare. Le procedure di seguito ser '
    ' vono allo scopo.                                                           '
    ' ************************************************************************** '
    Private Sub Carica_RigheFatturazione(ByVal SezioneDocumenti As DataRow, ByVal dr_fat As DataRow, ByVal nPodId As Integer, ByVal nNumSez As Integer, Optional ByVal lAll As Boolean = True)
        Dim dr_far_desc As DS_PF_UE_ele.TBL_fattura_rowRow
        Dim dr_far As DS_PF_UE_ele.TBL_fattura_rowRow
        Dim nSezioneStampa As Integer
        Dim nOrdineStampa As Integer
        Dim nRow As Integer
        Dim cTmp As String
        Dim lPrezUniNull As Boolean
        Dim cTmpStr As String
        Dim cVocePrec As String
        Dim lQuoPot As Boolean
        Dim cUM As String

        Try
            For Each dr_tmp1 As DataRow In SezioneDocumenti.GetChildRows("SezioniDocumenti_RiepilogoStampaxSezione")(0).GetChildRows("RiepilogoStampaxSezione_RigheRiepilogo20Contratto")
                nSezioneStampa = -1
                If dr_tmp1("CodiceRigaRiepilogo") = 37 Then
                    nSezioneStampa = Costanti.SEZ_imposte_iva
                End If
                If nSezioneStampa > -1 Then
                    nOrdineStampa = 1
                    cVocePrec = ""
                    For Each dr_tmp3 As DataRow In dr_tmp1.GetChildRows("RigheRiepilogo20Contratto_RigaFattura")
                        If cVocePrec <> dr_tmp3("Voce") Then
                            If cVocePrec > "" And nRow = 0 Then
                                dr_far_desc.Delete()
                            End If
                            cVocePrec = dr_tmp3("Voce")
                            dr_far_desc = CostruisciRigaFattura(True, dr_tmp3, nSezioneStampa, nOrdineStampa, dr_tmp1("CodiceRigaRiepilogo"))
                            nRow = 0
                        End If
                        dr_far = CostruisciRigaFattura(False, dr_tmp3, nSezioneStampa, nOrdineStampa, dr_tmp1("CodiceRigaRiepilogo"))
                        If dr_far IsNot Nothing Then
                            _PrintFatturaDataset.TBL_fattura_row.Rows.Add(dr_far)
                            nRow += 1
                        End If
                    Next
                    If nRow = 0 And Not (dr_far_desc Is Nothing) Then
                        dr_far_desc.Delete()
                    End If
                End If
            Next

            If lAll Or True Or SezioneDocumenti.GetChildRows("SezioniDocumenti_ElementiDettaglio").Length > 0 Then
                If SezioneDocumenti.GetChildRows("SezioniDocumenti_ElementiDettaglio").Length > 0 Then
                    For Each dr_tmp1 As DataRow In SezioneDocumenti.GetChildRows("SezioniDocumenti_ElementiDettaglio")(0).GetChildRows("ElementiDettaglio_RigaRiepilogoElementiDettaglio")
                        nSezioneStampa = -1
                        cTmpStr = dr_tmp1("RigaRiepilogoElementiDettaglio1").ToString.ToLower
                        If (cTmpStr = "SPESA PER LA MATERIA GAS NATURALE".ToLower) Then
                            nSezioneStampa = Costanti.SEZ_spese_materia_energia
                            _dr_dfa_pf.DFA_totale_a_des = cTmpStr
                            _dr_dfa_pf.DFA_totale_a_imp = dr_tmp1("ImportoRigaRiepilogoElementiDettaglio")
                            AddDescTotaleABCD("A")
                            AddDescTotaleABC("A")
                        ElseIf (cTmpStr = "SPESA PER IL TRASPORTO E LA GESTIONE".ToLower) Then
                            nSezioneStampa = Costanti.SEZ_trasporto
                            _dr_dfa_pf.DFA_totale_b_des = cTmpStr
                            _dr_dfa_pf.DFA_totale_b_imp = dr_tmp1("ImportoRigaRiepilogoElementiDettaglio")
                            AddDescTotaleABCD("B")
                            AddDescTotaleABC("B")
                        ElseIf (cTmpStr = "SPESA PER ONERI DI SISTEMA".ToLower) Then
                            nSezioneStampa = Costanti.SEZ_oneri_sistema
                            _dr_dfa_pf.DFA_totale_c_des = cTmpStr
                            _dr_dfa_pf.DFA_totale_c_imp = dr_tmp1("ImportoRigaRiepilogoElementiDettaglio")
                            AddDescTotaleABCD("C")
                            AddDescTotaleABC("C")
                        ElseIf (cTmpStr = "TOTALE IMPOSTE E IVA".ToLower) Then
                            nSezioneStampa = Costanti.SEZ_imposte_iva
                            _dr_dfa_pf.DFA_totale_d_des = cTmpStr
                            _dr_dfa_pf.DFA_totale_d_imp = dr_tmp1("ImportoRigaRiepilogoElementiDettaglio")
                            AddDescTotaleABCD("D")
                        End If
                        If nSezioneStampa > -1 Then
                            For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("RigaRiepilogoElementiDettaglio_SottosezioniElementiDettaglio")
                                nOrdineStampa = 1
                                cVocePrec = ""
                                For Each dr_tmp3 As DataRow In dr_tmp2.GetChildRows("SottosezioniElementiDettaglio_RigaFattura")
                                    If cVocePrec <> dr_tmp3("Voce") Then
                                        If cVocePrec > "" And nRow = 0 Then
                                            dr_far_desc.Delete()
                                        End If
                                        cVocePrec = dr_tmp3("Voce")
                                        dr_far_desc = CostruisciRigaFattura(True, dr_tmp3, nSezioneStampa, nOrdineStampa, dr_tmp1("CodiceRigaRiepilogo"))
                                        nRow = 0
                                    End If
                                    dr_far = CostruisciRigaFattura(False, dr_tmp3, nSezioneStampa, nOrdineStampa, dr_tmp1("CodiceRigaRiepilogo"))
                                    If dr_far IsNot Nothing Then
                                        _PrintFatturaDataset.TBL_fattura_row.Rows.Add(dr_far)
                                        nRow += 1
                                    End If
                                Next
                                If nRow = 0 And Not (dr_far_desc Is Nothing) Then
                                    dr_far_desc.Delete()
                                End If
                            Next
                        End If
                    Next
                End If
                '_dr_dfa_pf.DFA_totale_abcd_imp = _dr_dfa_pf.DFA_totale_a_imp + _dr_dfa_pf.DFA_totale_b_imp + _dr_dfa_pf.DFA_totale_c_imp + _dr_dfa_pf.DFA_totale_d_imp
                '_dr_dfa_pf.DFA_totale_abc_imp = _dr_dfa_pf.DFA_totale_a_imp + _dr_dfa_pf.DFA_totale_b_imp + _dr_dfa_pf.DFA_totale_c_imp

                If (dr_fat.GetChildRows("Fattura_SezioneVociGeneriche").Length > 0) And (_dr_mai_pf.DEF_raccolta = 1 Or _dr_mai_pf.DEF_raccolta = 9) Then
                    For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_SezioneVociGeneriche")(0).GetChildRows("SezioneVociGeneriche_SezioniStampa")
                        nSezioneStampa = -1
                        cTmpStr = dr_tmp1("DescrizioneSezioneStampa").ToString.ToLower
                        If (cTmpStr = "SEZIONE VOCI GENERICHE".ToLower) Then
                            nSezioneStampa = Costanti.SEZ_voci_gen
                            ' _dr_dfa_pf.DFA_one_dive = dr_tmp1("TotaleSezione")
                            ' _dr_dfa_pf.DFA_totale_abcd += _dr_dfa_pf.DFA_one_dive
                            ' AddDescTotale("D")
                        End If

                        If nSezioneStampa > -1 Then
                            For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("SezioniStampa_SottosezioneStampa")
                                nOrdineStampa = 1
                                cVocePrec = ""
                                For Each dr_tmp3 As DataRow In dr_tmp2.GetChildRows("SottosezioneStampa_RigaFattura")
                                    If cVocePrec <> dr_tmp3("Voce") Then
                                        If cVocePrec > "" And nRow = 0 Then
                                            dr_far_desc.Delete()
                                        End If
                                        cVocePrec = dr_tmp3("Voce")
                                        dr_far_desc = CostruisciRigaFattura(True, dr_tmp3, nSezioneStampa, nOrdineStampa, -37)
                                        nRow = 0
                                    End If
                                    dr_far = CostruisciRigaFattura(False, dr_tmp3, nSezioneStampa, nOrdineStampa, -37)
                                    If dr_far IsNot Nothing Then
                                        _PrintFatturaDataset.TBL_fattura_row.Rows.Add(dr_far)
                                        nRow += 1
                                    End If
                                Next
                                If nRow = 0 And Not (dr_far_desc Is Nothing) Then
                                    dr_far_desc.Delete()
                                End If
                            Next
                        End If
                    Next
                End If
            End If

            For Each dr_far In _PrintFatturaDataset.TBL_fattura_row.Select(String.Concat("FAR_cod_mai = ", _dr_mai_pf.MAI_codice))
                If Not dr_far.FAR_row_desc Then
                    For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId).GetChildRows("SezioniDocumenti_RiepilogoStampaxSezione")(0).GetChildRows("RiepilogoStampaxSezione_RigheRiepilogo")
                        If dr_far.FAR_iva = dr_tmp1("RigaRiepilogo") Then
                            dr_far.FAR_iva = dr_tmp1("ArticoloIVA")
                        End If
                    Next
                    For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_RiepilogoIVA")(0).GetChildRows("RiepilogoIVA_CorrispettiviIVA")(0).GetChildRows("CorrispettiviIVA_RigaRiepilogoIVA")
                        If IsNumeric(dr_far.FAR_iva) Then
                            If dr_far.FAR_iva = dr_tmp1("ArticoloIVA") Then
                                dr_far.FAR_iva = dr_tmp1("ArticoloIVA")
                                dr_far.FAR_iva = String.Concat(dr_far.FAR_iva, " %")
                            End If
                        End If
                    Next
                    If dr_fat.GetChildRows("Fattura_RiepilogoIVA")(0).GetChildRows("RiepilogoIVA_CorrispettiviFC").Length > 0 Then
                        For Each dr_tmp1 As DataRow In dr_fat.GetChildRows("Fattura_RiepilogoIVA")(0).GetChildRows("RiepilogoIVA_CorrispettiviFC")(0).GetChildRows("CorrispettiviFC_RecuperoAnticipazioni")
                            If dr_far.FAR_iva = dr_tmp1("voce") Then
                                dr_far.FAR_iva = dr_tmp1("ArticoloIVA")
                                If IsNumeric(dr_far.FAR_iva) Then
                                    dr_far.FAR_iva = String.Concat(dr_far.FAR_iva, " %")
                                End If
                            End If
                        Next
                    End If
                End If
            Next
        Catch ex As Exception
            _ErroriGravi = String.Concat(_ErroriGravi, "Carica_RigheFatturazione", ex, vbCr)
        End Try

    End Sub

    Private Function CostruisciRigaFattura(ByVal lRigaDescr As Boolean, ByVal dr_tmp3 As DataRow, ByVal nSezioneStampa As Integer, ByVal nOrdineStampa As Integer, ByVal nSezioneOriginale As Integer) As DS_PF_UE_ele.TBL_fattura_rowRow
        Dim dr_far As DS_PF_UE_ele.TBL_fattura_rowRow
        Dim lPrezUniNull As Boolean
        Dim cTmp As String
        Dim cUM As String

        dr_far = _PrintFatturaDataset.TBL_fattura_row.NewRow
        dr_far.FAR_cod_mai = _dr_mai_pf.MAI_codice
        dr_far.FAR_id_sezione = nSezioneStampa
        dr_far.FAR_row_desc = lRigaDescr
        dr_far.FAR_des_voce = dr_tmp3("Voce")
        dr_far.FAR_ord_stampa = nOrdineStampa
        dr_far.FAR_id_sez_orig = nSezioneOriginale
        If lRigaDescr Then
            ' LP 02032013 ********************************************************* '
            '        ' In alcuni casi � necessario forzare le condizioni di esposizione del- '
            '        ' la dilazione di pagamento. Nel caso specifico quando l'utente ha un   '
            '        ' periodo di  fatturazione con SCACCIAPENSIERI, che � terminato nel pe- '
            '        ' riodo precedente di fatturazione.                                     '
            '        ' ********************************************************************* '
            If dr_far.FAR_des_voce.ToLower.Contains("scacciapensieri") Then
                _dr_mai_pf.MAI_forza_scaccia = True
            End If
            ' LP 02032013 ********************************************************* '
        Else
            dr_far.FAR_dal = dr_tmp3("DataInizio")
            dr_far.FAR_al = dr_tmp3("DataFine")

            If nSezioneStampa <> Costanti.SEZ_oneri_sistema Then
                If Not IsDBNull(dr_tmp3("PrezzoUnitario")) Then
                    dr_far.FAR_prezzo_uni = dr_tmp3("PrezzoUnitario")
                End If
            Else
                dr_far.FAR_prezzo_uni = 0
            End If
            dr_far.FAR_id_sez_orig = nSezioneOriginale
            dr_far.FAR_quantita1 = dr_tmp3("Qta1")
            dr_far.FAR_quant_2_ex = False

            '
            ' Recupera 
            '
            cUM = ""
            Try
                cUM = dr_tmp3("UM1")
            Catch ex As Exception
                cUM = ""
            End Try

            Try
                cTmp = dr_tmp3("UM2")
                dr_far.FAR_quantita2 = Int(dr_tmp3("Qta2"))
                dr_far.FAR_quant_2_ex = True
            Catch ex As Exception
                cTmp = ""
                dr_far.FAR_quantita2 = -1
            End Try

            If cTmp > "" Then
                dr_far.FAR_uni_mis = String.Concat(cUM, "/", cTmp)
            Else
                dr_far.FAR_uni_mis = cUM
            End If

            dr_far.FAR_importo = dr_tmp3("Importo")
            dr_far.FAR_iva = dr_tmp3("AliquotaIVA")
            cTmp = GetTrasformazioni("valori_iva", dr_far.FAR_iva, 2)
            If cTmp > "" Then
                dr_far.FAR_iva = cTmp
            End If
            lPrezUniNull = (dr_far.FAR_id_sezione <> Costanti.SEZ_oneri_sistema)
            If lPrezUniNull Then
                If IsDBNull(dr_far.FAR_prezzo_uni) Then
                    dr_far.FAR_prezzo_uni = ""
                    lPrezUniNull = False
                Else
                    lPrezUniNull = IsNumeric(dr_far.FAR_prezzo_uni)
                    If lPrezUniNull Then
                        lPrezUniNull = (dr_far.FAR_prezzo_uni = 0)
                    Else
                        dr_far.FAR_prezzo_uni = ""
                    End If
                End If
            End If
            If lPrezUniNull Then
                dr_far = Nothing
                '_PrintFatturaDataset.TBL_fattura_row.Rows.Add(dr_far)
            End If
        End If
        Return dr_far

    End Function

    Private Sub Carica_RigheInsoluti(ByVal xiWork1 As Xml.XmlNode)
        Dim dr_ins As DS_PF_UE_ele.TBL_insolutiRow

        Try
            For Each xiWork2 As Xml.XmlNode In xiWork1.SelectNodes(Costanti.itm_insoluti)
                If xiWork2.SelectSingleNode("SCADENZA_RATA").InnerText <> _dr_dfa_pf.DFA_scadenza Then
                    dr_ins = _PrintFatturaDataset.TBL_insoluti.NewRow
                    dr_ins.INS_cod_mai = _dr_mai_pf.MAI_codice
                    dr_ins.INS_tipo = 1
                    dr_ins.INS_scadenza = GetValueFromXML(xiWork2, "SCADENZA_RATA")
                    dr_ins.INS_fattura = GetValueFromXML(xiWork2, "RIFERIMENTO_FATTURA").Replace("Fattura:", "").Trim
                    dr_ins.INS_imp_rata = GetValueFromXML(xiWork2, "IMPORTO_RATA")
                    dr_ins.INS_num_rata = GetValueFromXML(xiWork2, "NUMERO_RATA")
                    _PrintFatturaDataset.TBL_insoluti.Rows.Add(dr_ins)
                End If
            Next
        Catch ex As Exception
            Throw SetExceptionItem("Carica_RigheInsoluti", ex)
        End Try

    End Sub

    Private Sub Carica_StrumentoMisura(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer)
        Dim dt_stm As DS_PF_UE_ele.TBL_strum_misuraDataTable
        Dim dr_stm As DS_PF_UE_ele.TBL_strum_misuraRow
        Dim lRecordIns As Boolean
        Dim dr_tmp1 As DataRow
        Dim dr_tmp2 As DataRow
        Dim dr_tmp3 As DataRow

        lRecordIns = False
        dt_stm = New DS_PF_UE_ele.TBL_strum_misuraDataTable
        If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
            dr_tmp1 = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodId)
            If dr_tmp1.GetChildRows("SezioniDocumenti_DatiServizio").Length > 0 Then
                dr_tmp1 = dr_tmp1.GetChildRows("SezioniDocumenti_DatiServizio")(0)
                If dr_tmp1.GetChildRows("DatiServizio_PuntoDiFornitura").Length > 0 Then
                    dr_tmp1 = dr_tmp1.GetChildRows("DatiServizio_PuntoDiFornitura")(0)
                    If dr_tmp1.GetChildRows("PuntoDiFornitura_GruppoMisura").Length > 0 Then
                        For Each dr_tmp2 In dr_tmp1.GetChildRows("PuntoDiFornitura_GruppoMisura")
                            dr_stm = dt_stm.NewRow

                            dr_stm.STM_cod_mai = _dr_mai_pf.MAI_codice
                            dr_stm.STM_dal = dr_tmp2("DataInizio")
                            dr_stm.STM_al = dr_tmp2("DataFine")
                            dr_stm.STM_tipo_grp = dr_tmp2("GruppoMisura1")
                            dr_stm.STM_grp_orario = dr_stm.STM_tipo_grp = "GM EE - ORARIO"
                            dr_stm.STM_tipo_stm = ""
                            dr_stm.STM_cod_tipo_grp = -1
                            If dr_tmp2.GetChildRows("GruppoMisura_StrumentoMisura").Length > 0 Then
                                dr_tmp3 = dr_tmp2.GetChildRows("GruppoMisura_StrumentoMisura")(0)
                                dr_stm.STM_matricola = dr_tmp3("Matricola")

                                'If dr_tmp1.GetChildRows("StrumentoMisura_StrumentoMisura").Length > 0 Then
                                '    Dim dr_tmp2 As DataRow
                                '    dr_tmp2 = dr_tmp1.GetChildRows("StrumentoMisura_StrumentoMisura")(0)

                                dr_stm.STM_tipo_stm = dr_tmp3("StrumentoMisura1")
                                _dr_mai_pf.MAI_elet_gest = dr_stm.STM_tipo_stm.ToUpper = "CONTATORE ELETTRONICO GESTITO PER FASCE (EF)"
                                'End If

                                dr_stm.STM_cost_att = 1
                                dr_stm.STM_cost_rea = 1
                                dr_stm.STM_cost_pot = 1
                                For Each dr_tmp4 As DataRow In dr_tmp3.GetChildRows("StrumentoMisura_ValoreCampiSM")
                                    Select Case dr_tmp4("Campo")
                                        Case Is = "COSTANTE ATTIVA"
                                            dr_stm.STM_cost_att = dr_tmp4("ValoreCampo")
                                        Case Is = "COSTANTE REATTIVA"
                                            dr_stm.STM_cost_rea = dr_tmp4("ValoreCampo")
                                        Case Is = "COSTANTE POTENZA"
                                            dr_stm.STM_cost_pot = dr_tmp4("ValoreCampo")
                                    End Select
                                Next
                            End If
                            If ((dr_stm.STM_dal <= _dr_dfa_pf.DFA_emissione) And (_dr_dfa_pf.DFA_emissione <= dr_stm.STM_al)) Or _
                               ((CDate(dr_stm.STM_dal) <= CDate(_dr_dfa_pf.DFA_emissione)) And (CDate(_dr_dfa_pf.DFA_emissione) <= CDate(dr_stm.STM_al))) Then
                                ' 20110204****************************************************************** '
                                ' Dopo richiesta di Astolfi verifichiamo solo che la costante attiva sia =1  ' 
                                ' per non stamparla.                                                         '
                                ' ************************************************************************** '
                                dr_stm.STM_no_cost = (dr_stm.STM_cost_att = 1) 'And _dr_stm.STM_cost_rea = 1 And _dr_stm.STM_cost_pot = 1)
                                dt_stm.Rows.Add(dr_stm)
                                _GRP_orario = dr_stm.STM_grp_orario
                                lRecordIns = True
                            End If
                        Next
                    End If
                End If
            End If
        End If

        If lRecordIns Then
            dr_stm = dt_stm.Rows(dt_stm.Rows.Count - 1)
            _PrintFatturaDataset.TBL_strum_misura.ImportRow(dr_stm)
            _dr_stm = dr_stm
        Else
            dr_stm = _PrintFatturaDataset.TBL_strum_misura.NewRow
            With dr_stm
                .STM_cod_mai = _dr_mai_pf.MAI_codice
                .STM_dal = _dr_dfa_pf.DFA_emissione
                .STM_al = "31/12/9999"
                .STM_cod_tipo_grp = -1
                .STM_tipo_grp = ""
                .STM_matricola = ""
                .STM_tipo_stm = ""
                .STM_cost_att = 1
                .STM_cost_rea = 0
                .STM_cost_pot = 0
                .STM_no_cost = True
            End With
            _PrintFatturaDataset.TBL_strum_misura.Rows.Add(dr_stm)
            _dr_stm = dr_stm

        End If
        _dr_stm.AcceptChanges()

    End Sub

    ' ************************************************************************** '
    ' Caricamento dei messaggi di fatturazione.                                  '
    ' ************************************************************************** '
    Private Sub Carica_MessaggiFatturazione_contr(ByVal dr_fat As DataRow, ByVal nPdrId As Integer)
        Dim dr_msg As DS_PF_UE_ele.TBL_messaggiRow
        Dim nCodicceMsg As Integer
        Dim lInsert As Boolean
        Dim drTmp As DataRow
        Dim i As Integer
        Dim j As Integer
        Dim aSize As New ArrayList
        Dim nSize As Integer
        Dim cAppo As String

        '  <Messaggi>
        '  <TipoMessaggio>BONUS SOCIALE GAS</TipoMessaggio>
        '  <TestoMessaggio>La sua fornitura � ammessa alla compensazione della spesa per la fornitura di gas naturale (cosidetto bonus sociale gas ) ai sensi del decreto legge n. 185/08. La richiesta di rinnovo deve essere effettuata entro il 04/2011.</TestoMessaggio>
        '</Messaggi>

        If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPdrId).GetChildRows("SezioniDocumenti_Messaggi").Length > 0 Then
                For Each drTmp In dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPdrId).GetChildRows("SezioniDocumenti_Messaggi")
                    If drTmp("TipoMessaggio") = "VARIAZIONI DEI CONSUMI MEDI GIORNALIERI - EE" Then
                        RiempiTabellaStoricoConsumi(drTmp("TestoMessaggio").ToString)
                        lInsert = False
                    Else
                        dr_msg = _PrintFatturaDataset.TBL_messaggi.NewRow
                        If drTmp("TipoMessaggio") = "BONUS SOCIALE ELETTRICO" Then
                            nCodicceMsg = 13
                        ElseIf drTmp("TipoMessaggio") = "SERVIZIO GUASTI" Then
                            nCodicceMsg = 18
                        End If
                        With dr_msg
                            .MSG_cod_mai = _dr_mai_pf.MAI_codice
                            .MSG_cod_msg = nCodicceMsg
                            .MSG_tipo_msg = "C"
                            .MSG_messaggio = drTmp("TestoMessaggio").ToString.Replace(Chr(13), vbCrLf)
                            ' ************************************************************************** '
                            ' Escludo i seguenti messaggi perch� non debbono essere inseriti:            '
                            ' 13 ==> Messaggio per l'indicazione del bonus Sociale per disagio economico '
                            ' 18 ==> Messaggio per la segnalazione dei guasti;                           '
                            ' ************************************************************************** '
                            lInsert = True
                            Select Case .MSG_cod_msg
                                Case Is = 13
                                    _dr_dfa_pf.DFA_bonus_dis_eco = True
                                    _dr_dfa_pf.DFA_bonus_de_txt = .MSG_messaggio
                                    lInsert = False
                                Case Is = 18
                                    _TmpDistributore = .MSG_messaggio
                                    lInsert = False
                            End Select
                        End With
                    End If
                    If lInsert Then
                        _PrintFatturaDataset.TBL_messaggi.Rows.Add(dr_msg)
                    End If
                Next
            End If
        End If
        _PrintFatturaDataset.TBL_messaggi.AcceptChanges()

    End Sub

    Private Sub RiempiTabellaStoricoConsumi(ByVal cTesto As String)
        Dim dr_stc1 As DS_PF_UE_ele.TBL_storico_cons_fasciaRow
        Dim dr_stc2 As DS_PF_UE_ele.TBL_storico_cons_totRow

        For Each cLine As String In cTesto.Split(vbCrLf)
            cLine = cLine.Trim
            If Not cLine.ToLower.StartsWith("dal") And Not cLine.ToLower.StartsWith("storico") And cLine > "" Then
                'dr_stc = _PrintFatturaDataset.TBL_storico_consumi.NewRow
                'dr_stc.STC_cod_mai = _dr_mai_pf.MAI_codice
                'dr_stc.STC_data_da = cLine.Split(";")(0)
                'dr_stc.STC_data_a = cLine.Split(";")(1)
                'dr_stc.STC_giorni = cLine.Split(";")(2)
                'dr_stc.STC_attiva_f1 = Int(String.Concat("0", cLine.Split(";")(3)).Replace(".", ","))
                'dr_stc.STC_attiva_f2 = Int(String.Concat("0", cLine.Split(";")(5)).Replace(".", ","))
                'dr_stc.STC_attiva_f3 = Int(String.Concat("0", cLine.Split(";")(7)).Replace(".", ","))
                'dr_stc.STC_cons_tot = Int(String.Concat("0", cLine.Split(";")(9)).Replace(".", ","))
                'dr_stc.STC_cons_med = String.Concat("0", cLine.Split(";")(4)).Replace(".", ",")
                '_PrintFatturaDataset.TBL_storico_consumi.Rows.Add(dr_stc)
                dr_stc2 = _PrintFatturaDataset.TBL_storico_cons_tot.NewRow
                dr_stc2.STC_cod_mai = _dr_mai_pf.MAI_codice
                dr_stc2.STC_data_da = cLine.Split(";")(0)
                dr_stc2.STC_data_a = cLine.Split(";")(1)
                dr_stc2.STC_giorni = cLine.Split(";")(2)
                dr_stc2.STC_fascia = "Attiva F1"
                dr_stc2.STC_cons_tot = Int(String.Concat("0", cLine.Split(";")(9)).Replace(".", ","))
                dr_stc2.STC_cons_med = Int(String.Concat("0", cLine.Split(";")(10)).Replace(".", ","))
                _PrintFatturaDataset.TBL_storico_cons_tot.Rows.Add(dr_stc2)

                dr_stc1 = _PrintFatturaDataset.TBL_storico_cons_fascia.NewRow
                dr_stc1.STC_cod_mai = _dr_mai_pf.MAI_codice
                dr_stc1.STC_data_da = cLine.Split(";")(0)
                'dr_stc.STC_data_a = cLine.Split(";")(1)
                dr_stc1.STC_giorni = cLine.Split(";")(2)
                dr_stc1.STC_fascia = "Attiva F1"
                dr_stc1.STC_attiva_cons = Int(String.Concat("0", cLine.Split(";")(3)).Replace(".", ","))
                _PrintFatturaDataset.TBL_storico_cons_fascia.Rows.Add(dr_stc1)

                dr_stc1 = _PrintFatturaDataset.TBL_storico_cons_fascia.NewRow
                dr_stc1.STC_cod_mai = _dr_mai_pf.MAI_codice
                dr_stc1.STC_data_da = cLine.Split(";")(0)
                ' dr_stc.STC_data_a = cLine.Split(";")(1)
                dr_stc1.STC_giorni = cLine.Split(";")(2)
                dr_stc1.STC_fascia = "Attiva F2"
                dr_stc1.STC_attiva_cons = Int(String.Concat("0", cLine.Split(";")(5)).Replace(".", ","))
                _PrintFatturaDataset.TBL_storico_cons_fascia.Rows.Add(dr_stc1)

                dr_stc1 = _PrintFatturaDataset.TBL_storico_cons_fascia.NewRow
                dr_stc1.STC_cod_mai = _dr_mai_pf.MAI_codice
                dr_stc1.STC_data_da = cLine.Split(";")(0)
                'dr_stc.STC_data_a = cLine.Split(";")(1)
                dr_stc1.STC_giorni = cLine.Split(";")(2)
                dr_stc1.STC_fascia = "Attiva F3"
                dr_stc1.STC_attiva_cons = Int(String.Concat("0", cLine.Split(";")(7)).Replace(".", ","))
                _PrintFatturaDataset.TBL_storico_cons_fascia.Rows.Add(dr_stc1)
            End If
        Next

    End Sub

    ' ************************************************************************** '
    ' Caricamento dei messaggi di fatturazione.                                  '
    ' ************************************************************************** '
    Private Sub Carica_MessaggiFatturazione_raggr(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodId As Integer)
        Dim dr_ins As DS_PF_UE_ele.TBL_insolutiRow
        Dim dr_msg As DS_PF_UE_ele.TBL_messaggiRow
        Dim dr_tmp1 As DataRow

        If dr_fat.GetChildRows("Fattura_Messaggi").Length > 0 Then
            dr_tmp1 = dr_fat.GetChildRows("Fattura_Messaggi")(0)
            For Each dr_tmp As DataRow In dr_tmp1.GetChildRows("Messaggi_MessaggiPiedeFattura")
                Select Case dr_tmp("TipoMessaggio").ToString
                    Case Is = "SITUAZIONE PAGAMENTI BOLLETTE PRECEDENTI"
                        dr_ins = _PrintFatturaDataset.TBL_insoluti.NewRow
                        dr_ins.INS_cod_mai = _dr_mai_pf.MAI_codice
                        dr_ins.INS_tipo = 2
                        dr_ins.INS_messaggio = dr_tmp("TestoMessaggio").ToString.Replace(Chr(13), vbCrLf)
                        _PrintFatturaDataset.TBL_insoluti.Rows.Add(dr_ins)
                    Case Is = "DIRITTO RICHIESTA RATEIZZAZIONE FATTURA"
                        _dr_dfa_pf.DFA_rateizzazione = dr_tmp("TestoMessaggio").ToString.Replace(Chr(13), vbCrLf)
                    Case Is = "MODALITA DI FATTURAZIONE"
                        _ModalitaFatturazione = dr_tmp("TestoMessaggio").ToString.Replace("MOD. FATT. ", "")
                End Select

            Next

        End If
        _PrintFatturaDataset.TBL_messaggi.AcceptChanges()

    End Sub

    Private Function AddRecordTBL_righefatt(ByVal nSezione As Integer, ByVal lRowDesc As Boolean, ByVal cDescrizioneVoce As String, ByVal nOrdineStampa As Integer) As DS_PF_UE_ele.TBL_fattura_rowRow
        Dim dr_far As DS_PF_UE_ele.TBL_fattura_rowRow

        dr_far = _PrintFatturaDataset.TBL_fattura_row.NewRow
        dr_far.FAR_cod_mai = _dr_mai_pf.MAI_codice
        dr_far.FAR_id_sezione = nSezione
        dr_far.FAR_row_desc = lRowDesc
        dr_far.FAR_des_voce = cDescrizioneVoce
        dr_far.FAR_ord_stampa = nOrdineStampa

        Return dr_far

    End Function

    Private Sub AddDescTotaleABCD(ByVal cSez As String)
        Dim cTmp As String
        Dim nChar As Integer
        Dim lExit As Boolean

        If Not _dr_dfa_pf.DFA_totale_abcd_des.Contains(cSez) Then
            If _dr_dfa_pf.DFA_totale_abcd_des = "" Then
                _dr_dfa_pf.DFA_totale_abcd_des = cSez
            Else
                cTmp = String.Concat(_dr_dfa_pf.DFA_totale_abcd_des.Replace(" + ", "^"), "^")
                nChar = 0
                lExit = cTmp(nChar) > cSez.ToCharArray(0, 1)
                While nChar < cTmp.Length And Not lExit
                    lExit = cTmp(nChar) > cSez.ToCharArray(0, 1)
                    nChar += 2
                End While
                If nChar < cTmp.Length Then
                    cTmp = cTmp.Insert(nChar, String.Concat(cSez, "^"))
                Else
                    cTmp = String.Concat(cTmp, cSez, "^")
                End If
                cTmp = cTmp.Substring(0, cTmp.Length - 1)

                _dr_dfa_pf.DFA_totale_abcd_des = cTmp.Replace("^", " + ")
            End If
        End If

    End Sub

    Private Sub AddDescTotaleABC(ByVal cSez As String)
        Dim cTmp As String
        Dim nChar As Integer
        Dim lExit As Boolean

        If Not _dr_dfa_pf.DFA_totale_abcd_des.Contains(cSez) Then
            If _dr_dfa_pf.DFA_totale_abcd_des = "" Then
                _dr_dfa_pf.DFA_totale_abcd_des = cSez
            Else
                cTmp = String.Concat(_dr_dfa_pf.DFA_totale_abcd_des.Replace(" + ", "^"), "^")
                nChar = 0
                lExit = cTmp(nChar) > cSez.ToCharArray(0, 1)
                While nChar < cTmp.Length And Not lExit
                    lExit = cTmp(nChar) > cSez.ToCharArray(0, 1)
                    nChar += 2
                End While
                If nChar < cTmp.Length Then
                    cTmp = cTmp.Insert(nChar, String.Concat(cSez, "^"))
                Else
                    cTmp = String.Concat(cTmp, cSez, "^")
                End If
                cTmp = cTmp.Substring(0, cTmp.Length - 1)

                _dr_dfa_pf.DFA_totale_abcd_des = cTmp.Replace("^", " + ")
            End If
        End If

    End Sub

    Private Function SetDataScadenza(ByVal cFlagTipoPag As String, ByVal dDataForzata As String) As String
        Dim cCondizioneForzaScadenza As String

        cCondizioneForzaScadenza = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "17")
        If (cCondizioneForzaScadenza.Length > 0) And (cCondizioneForzaScadenza.ToUpper.Contains(cFlagTipoPag.ToUpper)) Then
            dDataForzata = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "16")
        End If
        Return dDataForzata

    End Function

#End Region

#Region "Caricamento del PRINTDATSET del bollettino."

    Private Sub GetRecordBollettiniToPrint(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_fat_imp As DataRow, ByVal cFeedString As String)
        Dim dr_fxr As DS_PB_UE_ele.TBL_fat_x_ratRow
        Dim dr_rdo As DS_PB_UE_ele.TBL_rich_domRow
        Dim dr_fat As DS_PB_UE_ele.TBL_fattureRow
        Dim dr_rat As DS_PB_UE_ele.TBL_rateRow

        ' Carico per ciascuna fattura un record sulla tabella delle fatture e tanti record su quella dei bollettini.
        ' cos� posso far guidare la stampa dei bollettini da una tabella che originer� successivamente.
        dr_fat = _PrintBollettinoDataset.TBL_fatture.NewRow
        With dr_fat
            .FAT_codice = _PrintBollettinoDataset.TBL_fatture.Rows.Count + 1
            .FAT_prot_reg_iva = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "Protocollo")
            .FAT_num_fattura = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "NumeroFattura")
            .FAT_data_fattura = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiIdentificativiFattura", "DataFattura")
            .FAT_nominativo = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "RagioneSociale")
            .FAT_cod_utente = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IntestatarioFattura", "IDSoggetto")
            .FAT_ind_rec_1 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "TipoElementoTopologico")
            .FAT_ind_rec_2 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "ElementoTopologico", "")
            .FAT_ind_rec_3 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "NumeroCivico", "")
            .FAT_ind_rec_4 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "SuffissoCivico", "")
            .FAT_ind_rec_5 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "Localita", "")
            .FAT_ind_rec_6 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "Comune")
            .FAT_ind_rec_7 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "Cap")
            .FAT_ind_rec_8 = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "IndirizzoSpedizione", "ProvinciaSigla")
        End With
        _PrintBollettinoDataset.TBL_fatture.Rows.Add(dr_fat)

        ' Carico i record nella tabella delle rate.
        '-------------> xmlRate = xmlFattura.SelectNodes(String.Concat(Costanti.itm_Testata_Fattura_row, "/RATA/RATA_ROW"))
        '        For i As Integer = 0 To xmlRate.Count - 1

        'xmlTMP = xmlRate(i)

        dr_rat = _PrintBollettinoDataset.TBL_rate.NewRow
        With dr_rat
            .RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1
            .RAT_num_rata = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "NumeroRata")
            .RAT_scadenza = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "DataScadenza")
            .RAT_importo = 0
            For i As Integer = 0 To RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "NumeroRate", "") - 1
                If RecuperaValoreDS_UE(ds_src, dr_fat_imp, i, "Rate", "ImportoRata") > "" Then
                    .RAT_importo += RecuperaValoreDS_UE(ds_src, dr_fat_imp, i, "Rate", "ImportoRata")
                End If
            Next


            .RAT_ocr = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "Rate", "OCR")
        End With
        _PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat)

        ' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
        dr_fxr = _PrintBollettinoDataset.TBL_fat_x_rat.NewRow
        dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1
        dr_fxr.DEF_alimimb = cFeedString
        ' per il bollettino non dovrebbero esserci problemi di raccolta.
        ' Al momento il campo viene valorizzato con questi valori per le fasi successive si vedr�.
        dr_fxr.DEF_raccolta = 3
        dr_fxr.FXR_linkQD = nCodice
        dr_fxr.FXR_cod_fat = dr_fat.FAT_codice
        dr_fxr.FXR_cod_rat = dr_rat.RAT_codice
        dr_fxr.FXR_cod_gruppo = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "CodiceConsorzioAcquisto", "CONSORZIO/GRUPPO DI ACQUISTO")

        ' Mi serve per controllare il codice dell'imbustatrice.
        '   If i = xmlRate.Count - 1 Then
        'dr_fxr.FXR_ultimo = 1
        '    End If
        _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr)
        'Next

        ' Carico i record nella richiesta di domiciliazione 
        '------------->For Each xmlTMP In xmlFattura.SelectNodes(String.Concat(Costanti.itm_Testata_Fattura_row, "/RICHIESTA_DOMICILIAZIONE/RICHIESTA_DOMICILIAZIONE_ROW"))
        dr_rdo = _PrintBollettinoDataset.TBL_rich_dom.NewRow
        With dr_rdo
            .RDO_codice = _PrintBollettinoDataset.TBL_rich_dom.Rows.Count + 1
            .RDO_cod_fat = dr_fat.FAT_codice
            .RDO_cod_sia = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiRichiestaDomiciliazione", "IDSIA") 'GetValueFromXML(xmlTMP, "CODICE_SIA")
            .RDO_tipo_serv = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiRichiestaDomiciliazione", "TipoServizio") 'GetValueFromXML(xmlTMP, "TIPO_SERVIZIO")
            .RDO_cod_domic = RecuperaValoreDS_UE(ds_src, dr_fat_imp, 0, "DatiRichiestaDomiciliazione", "IDDomiciliazione") 'GetValueFromXML(xmlTMP, "CODICE_DOMICILIAZIONE")
        End With
        _PrintBollettinoDataset.TBL_rich_dom.Rows.Add(dr_rdo)
        '-------------> Next
    End Sub

#End Region

#Region "Caricamento del SENDDATASET"

    'Public Sub LoadSendDataset() Implements PLUGIN_interfaceV1_4_1.IPLG_dati_lib.LoadSendDataset
    '    Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow
    '    Dim nRecordToSend As Integer

    '    ' ************************************************************************** '
    '    ' Creazione del Dataset e azzeramento del contatore dei record da inviare.   '
    '    ' ************************************************************************** '
    '    nRecordToSend = 0
    '    _SendEmailDataset = New DS_SE_UE_ele

    '    ' ************************************************************************** '
    '    ' Caricamento del DataSet da utilizzare per la stampa.                       '
    '    ' ************************************************************************** '
    '    _CodiceProgressivo = 0
    '    While _RecordCount < _QuickDataset.TBL_main.Rows.Count And nRecordToSend < 1
    '        dr_mai_qd = _QuickDataset.TBL_main.Rows(_RecordCount)
    '        If dr_mai_qd.DEF_toprint Then
    '            Try
    '                _ErroriLievi = ""
    '                AddRecordToSend(dr_mai_qd)
    '                If _ErroriLievi > "" Then
    '                    dr_mai_qd.DEF_errcode = -2
    '                    dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori lievi: ", _ErroriLievi)
    '                End If
    '            Catch ex As Exception
    '                dr_mai_qd.DEF_errcode = -1
    '                dr_mai_qd.DEF_errdesc = ex.Message
    '            End Try
    '            nRecordToSend += 1
    '        End If
    '        _RecordCount += 1
    '    End While

    '    ' ************************************************************************** '
    '    ' Consolidamento del DataSet da utilizzare per la stampa.                    '
    '    ' ************************************************************************** '
    '    _SendEmailDataset.AcceptChanges()

    'End Sub

    Private Sub GetRecordEmailToSend(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_fat As DataRow)
        Dim dr_doc As DS_SE_UE_ele.TBL_documentiRow
        Dim dr_mai As DS_SE_UE_ele.TBL_mainRow
        Dim xdViaMail As Xml.XmlDocument
        Dim cDFA_flag_tipo_pag As String
        Dim cDFA_desc_tipo_pag As String
        Dim xnDoc As Xml.XmlNode

        cDFA_desc_tipo_pag = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Dilazione", "MezzoPagamento")
        If cDFA_desc_tipo_pag.ToLower.Replace(" ", "") = "rimessadiretta" Then
            cDFA_flag_tipo_pag = "R"
        ElseIf cDFA_desc_tipo_pag.ToLower.Replace(" ", "") = "bonificobancario" Then
            cDFA_flag_tipo_pag = "B"
        ElseIf cDFA_desc_tipo_pag.ToLower.Replace(" ", "") = "domiciliazionebancaria" Then
            cDFA_flag_tipo_pag = "D"
        Else
            cDFA_flag_tipo_pag = ""
        End If


        ' ************************************************************************** '
        ' Creazione del record guida per l'invio della email.                        '
        ' ************************************************************************** '
        dr_mai = _SendEmailDataset.TBL_main.NewRow
        _CodiceProgressivo += 1
        dr_mai.MAI_codice = _CodiceProgressivo
        dr_mai.MAI_linkQD = nCodice
        dr_mai.MAI_nome = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IntestatarioFattura", "RagioneSociale")
        dr_mai.MAI_tot_fattura = RecuperaValoreXML(True, "RiepilogoIVA", "TotaleFattura") 'RecuperaValoreDS_UE(ds_src, dr_fat, 0, "RiepilogoIVA", "TotaleFattura")
        dr_mai.MAI_data_scadenza = SetDataScadenza(cDFA_flag_tipo_pag, RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Rate", "DataScadenza"))
        dr_mai.MAI_registro_iva = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "Protocollo")
        dr_mai.MAI_anno = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "AnnoFattura")
        dr_mai.MAI_numero = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "DatiIdentificativiFattura", "NumeroFattura")
        dr_mai.MAI_per_rifer = DeterminaPeriodoFatturazione(ds_src, dr_fat, 0)
        dr_mai.MAI_multipod = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "NumeroPOD", "NumeroPOD") > 1

        dr_mai.MAI_anno_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Contratto", "AnnoContratto")
        dr_mai.MAI_num_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Contratto", "NumeroContratto")
        dr_mai.MAI_ub_sito_1 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "TipoElementoTopologico")
        dr_mai.MAI_ub_sito_2 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "ElementoTopologico")
        dr_mai.MAI_ub_sito_3 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "NumeroCivico")
        dr_mai.MAI_ub_sito_4 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "SuffissoCivico")
        dr_mai.MAI_ub_sito_5 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "Localita")
        dr_mai.MAI_ub_sito_6 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "Cap")
        dr_mai.MAI_ub_sito_7 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "Comune")
        dr_mai.MAI_ub_sito_8 = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoPuntoDiFornitura", "ProvinciaSigla")
        dr_mai.MAI_cod_gruppo = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "CodiceConsorzioAcquisto", "CONSORZIO/GRUPPO DI ACQUISTO")

        dr_mai.DEF_lista_email = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "IndirizzoSpedizione", "IndirizzoEmailInvioFattura").ToString.Replace("$", ";")
        dr_mai.MAI_send_xml = False
        _SendEmailDataset.TBL_main.Rows.Add(dr_mai)

        xdViaMail = New Xml.XmlDocument
        xdViaMail.Load(String.Concat(_SessionePath, "dati\SendByMail.xml"))
        xnDoc = xdViaMail.SelectSingleNode(String.Concat("SendByMail/SendByMailItem/MAI_codice[.='", nCodice, "']")).ParentNode

        For Each xnTmp As Xml.XmlNode In xnDoc.SelectSingleNode("Documenti").ChildNodes
            dr_doc = _SendEmailDataset.TBL_documenti.NewRow
            dr_doc.DOC_cod_mai = dr_mai.MAI_codice
            dr_doc.DOC_filename = xnTmp.SelectSingleNode("DOC_filename").InnerText
            _SendEmailDataset.TBL_documenti.Rows.Add(dr_doc)
        Next

    End Sub

#End Region

    Private Function GetValueFromXML(ByVal xmlItem As Xml.XmlNode, ByVal xmlPath As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cDefault
            If Not xmlItem.SelectSingleNode(xmlPath) Is Nothing Then
                cValue = xmlItem.SelectSingleNode(xmlPath).InnerText
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori del file XML.", vbLf, "percorso da cui si tenta il recupero:", vbLf, xmlPath), ex)
        End Try
        Return cValue

    End Function

    Private Sub AddErroreLieve(ByVal cMsg As String)

        If _ErroriLievi = "" Then
            _ErroriLievi = cMsg
        Else
            _ErroriLievi = String.Concat(_ErroriLievi, "; ", cMsg)
        End If

    End Sub

    Private Sub ImpostaMessaggi(ByVal aValue As String())
        Dim i As Integer

        For i = 0 To 5
            If aValue(i) > "" Then
                _MessaggiAvanzamento(i) = aValue(i)
            End If
        Next

    End Sub

    Private Function GetTrasformazioni(ByVal cTable As String, ByVal cCodice As String, ByVal nTipo As Integer) As String
        Dim xdTrasf As Xml.XmlDocument
        Dim xiWork As Xml.XmlNode
        Dim cRtn As String

        Try
            xdTrasf = New Xml.XmlDocument
            xdTrasf.Load(_SessioneTrasformazioniFileName)
            xiWork = xdTrasf.SelectSingleNode(String.Concat("translation/", cTable, "/item[codice = '", cCodice, "']"))
            cRtn = ""
            If Not xiWork Is Nothing Then
                If nTipo = 1 Then
                    cRtn = xiWork.SelectSingleNode("descr_breve").InnerText
                End If
                If nTipo = 2 Then
                    cRtn = xiWork.SelectSingleNode("descrizione").InnerText
                End If
            End If
            xdTrasf = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function


    Private Sub GetCredenzialiWeb()
        Dim strCredWeb As System.IO.StreamReader
        Dim nColPwd As Integer = 0
        Dim nColCF As Integer = 0
        Dim nColPI As Integer = 0
        Dim cFileName As String
        Dim cPassword As String
        Dim i As Integer = 0
        Dim cLine As String

        Try
            cFileName = String.Concat(_SessionePath, _SessioneDati.SelectSingleNode(String.Concat("filename[@tipo=", "", 102, "", "]")).InnerText)
            strCredWeb = New System.IO.StreamReader(cFileName)

            ' Dalla prima riga legge i nomi della colonna.
            cLine = strCredWeb.ReadLine

            ' recupera il numero di colonna della PASSWORD, del Codice Fiscale, della Partita IVA
            For Each cTmp As String In cLine.Split(";")
                'If cTmp.ToLower = "codice_fiscale" Then
                ' nColCF = i
                ' End If
                ' If cTmp.ToLower = "partita_iva" Then
                ' nColPI = i
                ' End If
                'If cTmp.ToLower = "password" Then
                'nColPwd = i
                'End If
                If cTmp.ToLower = "nomeutente" Or cTmp.ToLower = "CodFiscPIVA".ToLower Then
                    nColCF = i
                    nColPI = i
                End If
                If cTmp.ToLower = "sito_pw_iniziale" Or cTmp.ToLower = "Password".ToLower Then
                    nColPwd = i
                End If
                i += 1
            Next
            cPassword = ""
            If (nColCF * nColPwd) > 0 Then
                While Not strCredWeb.EndOfStream
                    cLine = strCredWeb.ReadLine
                    If _dr_mai_pf.MAI_par_iva > "" And cLine.Split(";")(nColPI) = _dr_mai_pf.MAI_par_iva Then
                        cPassword = cLine.Split(";")(nColPwd)
                    End If
                    If _dr_mai_pf.MAI_cod_fis > "" And cLine.Split(";")(nColCF) = _dr_mai_pf.MAI_cod_fis Then
                        cPassword = cLine.Split(";")(nColPwd)
                    End If
                End While
            End If
            strCredWeb.Close()
        Catch ex As Exception
            cPassword = ""
            AddErroreLieve("Recupero credenziali accesso web")
        End Try
        _dr_mai_pf.MAI_wa_username = "NON UTILIZZATO"
        _dr_mai_pf.MAI_wa_password = cPassword
        _dr_mai_pf.MAI_web_access = _dr_mai_pf.MAI_wa_password > ""

    End Sub

    Private Function GetDataSendByMail() As Xml.XmlNode
        Dim xnWork As Xml.XmlNode
        Dim xdWork As Xml.XmlDocument

        xdWork = New Xml.XmlDocument
        xnWork = xdWork.CreateNode(Xml.XmlNodeType.Element, "SendByMail", "")
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                With CType(_PrintFatturaDataset.TBL_main.Rows(0), DS_PF_UE_ele.TBL_mainRow)
                    CreateXmlNode(xdWork, xnWork, "SND_EML_codice", .MAI_linkQD)
                    CreateXmlNode(xdWork, xnWork, "SND_EML_cod_utente", .MAI_cod_cli)
                    CreateXmlNode(xdWork, xnWork, "SND_EML_nominativo", .MAI_nome)
                    CreateXmlNode(xdWork, xnWork, "SND_EML_mail_address", "lproietti@gmail.com")
                    'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_codice", .MAI_linkQD.ToString))
                    'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_cod_utente", .MAI_cod_cli.ToString))
                    'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_nominativo", .MAI_nome.ToString))
                    'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_mail_address", "lproietti@gmail.com"))
                End With
        End Select
        Return xnWork

    End Function

    Private Sub CreateXmlNode(ByVal xdDoc As Xml.XmlDocument, ByVal xnNode As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xdDoc.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xnNode.AppendChild(xiTmp)

    End Sub

#Region "Elenco procedure Generiche"

    Private Function SetExceptionItem(ByVal cProcError As String, ByVal ex As Exception) As Exception
        Dim ex_rtn As Exception

        If ex.InnerException Is Nothing Then
            ex_rtn = New Exception(String.Concat("Errore (", cProcError, "): ", ex.Message), ex)
        Else
            ex_rtn = ex
        End If
        Return ex_rtn

    End Function

#End Region

    Private Function RecuperaValoreDS_UE(ByVal ds_src As DataSet, ByVal dr_fat As DataRow, ByVal nPodiD As Integer, ByVal cTable As String, ByVal cField As String, Optional ByVal cDefault As String = "")
        Dim lFieldExist As Boolean
        Dim nRtnValue As Integer
        Dim cRtnValue As String
        Dim dr_tmp As DataRow

        cRtnValue = cDefault
        lFieldExist = ds_src.Tables.Contains(cTable)
        If lFieldExist Then
            lFieldExist = ds_src.Tables(cTable).Columns.Contains(cField)
        End If
        If lFieldExist Then
            Select Case cTable.ToLower
                Case Is = "DatiIdentificativiFattura".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiIdentificativiFattura").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiIdentificativiFattura")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "PeriodiFatturazione".ToLower
                    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                        If dr_tmp.GetChildRows("SezioniDocumenti_PeriodiFatturazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_PeriodiFatturazione")(0)
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        End If
                    End If
                Case Is = "IntestatarioFattura".ToLower
                    If dr_fat.GetChildRows("Fattura_IntestatarioFattura").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_IntestatarioFattura")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "IndirizzoSedeLegale".ToLower
                    If dr_fat.GetChildRows("Fattura_IndirizzoSedeLegale").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_IndirizzoSedeLegale")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "DatiSpedizione".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiSpedizione").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiSpedizione")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "IndirizzoSpedizione".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiSpedizione").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiSpedizione")(0)
                        If dr_tmp.GetChildRows("DatiSpedizione_IndirizzoSpedizione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("DatiSpedizione_IndirizzoSpedizione")(0)
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        Else
                            If dr_tmp.GetChildRows("DatiSpedizione_IndirizzoSpedizione").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("DatiSpedizione_IndirizzoSpedizione")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "PuntoDiFornitura".ToLower
                    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                        If dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio")(0)
                            If dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "Dilazione".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        End If
                    End If
                Case Is = "OT".ToLower
                    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                        If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                            If dr_tmp.GetChildRows("Contratto_OT").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("Contratto_OT")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "DatiRichiestaDomiciliazione".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                            If dr_tmp.GetChildRows("Dilazione_DatiRichiestaDomiciliazione").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("Dilazione_DatiRichiestaDomiciliazione")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "Rate".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                            If dr_tmp.GetChildRows("Dilazione_Rate").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("Dilazione_Rate")(nPodiD)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "BancaCliente".ToLower
                    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                            If dr_tmp.GetChildRows("Dilazione_DatiDomiciliazione").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("Dilazione_DatiDomiciliazione")(0)
                                If dr_tmp.GetChildRows("DatiDomiciliazione_BancaCliente").Length > 0 Then
                                    dr_tmp = dr_tmp.GetChildRows("DatiDomiciliazione_BancaCliente")(0)
                                    If IsDBNull(dr_tmp(cField)) Then
                                        cRtnValue = cDefault
                                    Else
                                        cRtnValue = dr_tmp(cField)
                                    End If
                                End If
                            End If
                        End If
                    End If
                Case Is = "RiepilogoIVA".ToLower
                    If dr_fat.GetChildRows("Fattura_RiepilogoIVA").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_RiepilogoIVA")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "CorrispettiviIVA".ToLower
                    If dr_fat.GetChildRows("Fattura_RiepilogoIVA").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_RiepilogoIVA")(0)
                        If dr_tmp.GetChildRows("RiepilogoIVA_CorrispettiviIVA").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("RiepilogoIVA_CorrispettiviIVA")(0)
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        End If
                    End If
                Case Is = "Contratto".ToLower
                    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                        If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        End If
                    End If
            End Select
        ElseIf cTable.ToLower = "FatturaDettaglio".ToLower Then
            cRtnValue = "NO"
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_ElementiDettaglio").Length > 0 Then
                    cRtnValue = "SI"
                End If
            End If
        ElseIf cTable.ToLower = "TipoStampa".ToLower Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                    For Each dr As DataRow In dr_tmp.GetChildRows("Contratto_ValoreCampoContratto")
                        If dr("Campo").tolower = cField.ToLower Then
                            cRtnValue = dr("ValoreCampo")
                        End If
                    Next
                End If
            End If
        ElseIf cTable.ToLower = "CodiceConsorzioAcquisto".ToLower Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                    If dr_tmp.GetChildRows("Contratto_AltriSoggettiCorrelati").Length > 0 Then
                        For Each dr As DataRow In dr_tmp.GetChildRows("Contratto_AltriSoggettiCorrelati")
                            If dr("Ruolo").tolower = cField.ToLower Then
                                cRtnValue = dr.GetChildRows("AltriSoggettiCorrelati_Soggetto")(0)("IDSoggetto")
                            End If
                        Next
                    End If
                End If
            End If

        ElseIf cTable.ToLower = "TipoPagamento".ToLower Then
            If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                    If cField = "BollettinoPostale" Then
                        If dr_tmp("MezzoPagamento").ToString.ToLower.Replace(" ", "") = "rimessadiretta" Then
                            cRtnValue = "si"
                        Else
                            cRtnValue = "no"
                        End If
                    End If
                End If
            End If
        ElseIf cTable.ToLower = "NumeroPOD".ToLower Then
            If cField.ToLower = "NumeroPOD".ToLower Then
                'cRtnValue = dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length
                ' LP 20130324 Attenzione debbo verificare che nell'elenco dei pod non ci siano ODL che sono rilevati dalla seguente descrizione:
                ' <TipoSezione>Sezioni Valorizzazione Contratto</TipoSezione>
                ' <TipoSezione>Sezioni Valorizzazione Lavoro</TipoSezione>
                nRtnValue = 0
                For Each dr_tmp In dr_fat.GetChildRows("Fattura_SezioniDocumenti")
                    If dr_tmp("TipoSezione").ToString.ToLower = "sezioni valorizzazione contratto" Then
                        nRtnValue += 1
                    ElseIf dr_tmp("TipoSezione").ToString.ToLower = cnt_tiposezione_lavoro Then

                    End If
                Next
                cRtnValue = nRtnValue
            End If
        ElseIf cTable.ToLower = "NumeroSEZ".ToLower Then
            If cField.ToLower = "NumeroSEZ".ToLower Then
                cRtnValue = dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length
                ' LP 20130324 Attenzione debbo verificare che nell'elenco dei pod non ci siano ODL che sono rilevati dalla seguente descrizione:
                ' <TipoSezione>Sezioni Valorizzazione Contratto</TipoSezione>
                ' <TipoSezione>Sezioni Valorizzazione Lavoro</TipoSezione>
                'nRtnValue = 0
                'For Each dr_tmp In dr_fat.GetChildRows("Fattura_SezioniDocumenti")
                '    If dr_tmp("TipoSezione").ToString.ToLower = "sezioni valorizzazione contratto" Then
                '        nRtnValue += 1
                '    ElseIf dr_tmp("TipoSezione").ToString.ToLower = "sezioni valorizzazione lavoro" Then

                '    End If
                'Next
                'cRtnValue = nRtnValue
            End If
        ElseIf cTable.ToLower = "NumeroRate".ToLower Then
            If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                    cRtnValue = dr_tmp.GetChildRows("Dilazione_Rate").Length
                End If
            End If
        ElseIf cTable.ToLower = "IndirizzoPuntoDiFornitura".ToLower Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio")(0)
                    If dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura").Length > 0 Then
                        dr_tmp = dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura")(0)
                        If dr_tmp.GetChildRows("PuntoDiFornitura_Indirizzo").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("PuntoDiFornitura_Indirizzo")(0)
                            Try
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            Catch ex As Exception
                                cRtnValue = cDefault
                            End Try
                        End If
                    End If
                End If
            End If
        ElseIf cField.ToLower = "IndirizzoEmailInvioFattura".ToLower Then
            If dr_fat.GetChildRows("Fattura_DatiSpedizione").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_DatiSpedizione")(0)
                If cField = "IndirizzoEmailInvioFattura" Then
                    Try
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    Catch ex As Exception
                        cRtnValue = ""
                    End Try
                End If
            End If
        ElseIf cTable.ToLower = "TipoStampa".ToLower Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                    For Each dr As DataRow In dr_tmp.GetChildRows("Contratto_ValoreCampoContratto")
                        If dr("Campo").tolower = cField.ToLower Then
                            cRtnValue = dr("ValoreCampo")
                        End If
                    Next
                End If
            End If
        ElseIf cTable.ToLower = "CodiceConsorzioAcquisto".ToLower Then
            If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                    If dr_tmp.GetChildRows("Contratto_AltriSoggettiCorrelati").Length > 0 Then
                        For Each dr As DataRow In dr_tmp.GetChildRows("Contratto_AltriSoggettiCorrelati")
                            If dr("Ruolo").tolower = cField.ToLower Then
                                cRtnValue = dr.GetChildRows("AltriSoggettiCorrelati_Soggetto")(0)("IDSoggetto")
                            End If
                        Next
                    End If
                End If
            End If
        End If
        Return cRtnValue

    End Function


    Private ReadOnly Property AzioniPostStampa_filename() As String

        Get
            Return String.Concat(_SessioneDatiPath, "azioni_poststampa.xml")
        End Get

    End Property

    WriteOnly Property ListaGiri() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ListaGiri

        Set(ByVal value As ArrayList)
            _ListaGiri = value
        End Set

    End Property

    WriteOnly Property DBGiriConnectionString() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DBGiriConnectionString

        Set(ByVal value As String)
            _DbGiriConnString = value
        End Set

    End Property

    ReadOnly Property NuoviToponimi() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.NuoviToponimi

        Get
            Return _NuoviToponimi
        End Get

    End Property

    ReadOnly Property CapacitaInvioMail() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CapacitaInvioMail

        Get
            Return True
        End Get

    End Property

    ' ************************************************************************** '
    ' Questa funzione serve a leggere un file XML con la codifica WINDOWS-1252 e '
    ' a caricarlo in un Dataset.                                                 '
    ' ************************************************************************** '
    Private Function caricaFileDatiDS(ByVal cFileName As String) As DataSet
        Dim myEncoding As System.Text.Encoding
        Dim sr_xml As System.IO.StreamReader
        Dim ds_src As DataSet

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)
            sr_xml = New System.IO.StreamReader(cFileName, myEncoding)
            ds_src = New DataSet
            ds_src.ReadXml(sr_xml)
            sr_xml.Close()
        Catch ex As Exception
            ds_src = Nothing
            Throw ex
        End Try
        Return ds_src

    End Function

    ' ************************************************************************** '
    ' Questa funzione serve a leggere un file XML con la codifica WINDOWS-1252 e '
    ' a caricarlo in un XMLDocument.                                             '
    ' ************************************************************************** '
    Private Function caricaFileDatiXML(ByVal cFileName As String) As Xml.XmlDocument
        Dim myEncoding As System.Text.Encoding
        Dim sr_xml As System.IO.StreamReader
        Dim fil_xml As Xml.XmlDocument

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)    ' Windows-1252
            sr_xml = New System.IO.StreamReader(cFileName, myEncoding)
            fil_xml = New Xml.XmlDocument
            fil_xml.Load(sr_xml)
            sr_xml.Close()
        Catch ex As Exception
            fil_xml = Nothing
            Throw ex
        End Try
        Return fil_xml

    End Function

    ' ************************************************************************** '
    ' Questa funzione serve a scrivere un file XML con la codifica WINDOWS-1252. '
    ' ************************************************************************** '
    Private Sub SalvaFileDatiXML(ByVal cFileName As String, ByVal xdFile As Xml.XmlDocument)
        Dim myEncoding As System.Text.Encoding
        Dim sw_xml As System.IO.StreamWriter

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)
            sw_xml = New System.IO.StreamWriter(cFileName, False, myEncoding)
            xdFile.Save(sw_xml)
            sw_xml.Close()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckCondPSA
        Dim cValue As String
        Dim lCond As Boolean

        cValue = ""
        lCond = False
        If nCond2Check = 1001 Then
            If _CodiceModulo = ListaDataset.DAS_fattura Then
                cValue = FullDataset.Tables(FullDataSet_MainTableName).Rows(0).Item("MAI_cod_gruppo")
            ElseIf _CodiceModulo = ListaDataset.DAS_bollettino Then
                cValue = FullDataset.Tables(FullDataSet_MainTableName).Rows(0).Item("FXR_cod_gruppo")
            End If
            If cValue > "" Then
                lCond = (cValue = cValue2Check)
            End If
        End If
        Return lCond

    End Function

    Public Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckCondAPI
        Dim cValue As String
        Dim lCond As Boolean

        cValue = ""
        lCond = False
        If nCond2Check = 1001 Then
            If _CodiceModulo = ListaDataset.DAS_send_email Then
                cValue = FullDataset.Tables(FullDataSet_MainTableName).Rows(0).Item("MAI_cod_gruppo")
            End If
            If cValue > "" Then
                lCond = (cValue = cValue2Check)
            End If
        End If
        Return lCond

    End Function

    Public Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ExecActionPSA

        If nAct2Exec = 1001 Then
            EstraiXmlPart(cValue2Exec)
        End If

    End Sub

    Public Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ExecActionAPI

        If nAct2Exec = 1001 Then
            AllegaFileXml(cTipoExecAPI, cValue2Exec)
        End If

    End Sub

    Private Function XMLPart_filemame(ByVal cAnno As String, ByVal cRegIva As String, ByVal cNumero As String) As String
        Dim cTmp As String

        cTmp = String.Concat(_SessionePath, "Estrazione\", cAnno, "_", cRegIva, "_", cNumero, ".xml")
        Return cTmp

    End Function

    Private Sub EstraiXmlPart(ByVal cValue2Exec As String)
        Dim dr_dfa As DS_PF_UE_ele.TBL_dati_fattRow
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow
        Dim cFileName As String
        Dim nRecord As Integer
        Dim nLinkQd As Integer
        Dim ds_src As DataSet
        Dim dr_fat As DataRow

        Try
            nLinkQd = -1
            If _CodiceModulo = ListaDataset.DAS_fattura Then
                nLinkQd = FullDataset.Tables(FullDataSet_MainTableName).Rows(0).Item("MAI_linkQD")
            ElseIf _CodiceModulo = ListaDataset.DAS_bollettino Then
                nLinkQd = FullDataset.Tables(FullDataSet_MainTableName).Rows(0).Item("FXR_linkQD")
            End If
            If nLinkQd > -1 Then
                dr_mai_qd = _QuickDataset.TBL_main.Select(String.Concat("MAI_codice = ", nLinkQd))(0)
                ds_src = caricaFileDatiDS(PrintDataSet_DataFile(dr_mai_qd.MAI_filedati))

                nRecord = 0
                While nRecord < ds_src.Tables("Fattura").Rows.Count
                    dr_fat = ds_src.Tables("Fattura").Rows(nRecord)
                    If dr_fat("IDFattura") <> dr_mai_qd.MAI_IDFattura Then
                        dr_fat.Delete()
                    Else
                        dr_dfa = FullDataset.Tables("TBL_dati_fatt").Rows(0)
                        cFileName = XMLPart_filemame(dr_dfa.DFA_anno, dr_dfa.DFA_registro_iva, dr_dfa.DFA_numero)
                        nRecord += 1
                    End If
                End While
                ds_src.AcceptChanges()
                ds_src.WriteXml(cFileName)
                ds_src = Nothing
            End If
        Catch ex As Exception
            Throw SetExceptionItem("EstraiXmlPart", ex)
        End Try

    End Sub

    Public Sub AllegaFileXml(ByVal cTipoExecAPI As String, ByVal cValue2Exec As String)
        Dim dr_doc As DS_SE_UE_ele.TBL_documentiRow
        Dim dr_mai As DS_SE_UE_ele.TBL_mainRow
        Dim cFileName As String

        ' ************************************************************************** '
        ' Creazione del record guida per l'invio della email.                        '
        ' ************************************************************************** '
        dr_mai = _SendEmailDataset.TBL_main.Rows(0)
        If cTipoExecAPI = "A" Then
            cFileName = XMLPart_filemame(dr_mai.MAI_anno, dr_mai.MAI_registro_iva, dr_mai.MAI_numero)
            If System.IO.File.Exists(cFileName) Then
                dr_mai.MAI_send_xml = True
                dr_doc = _SendEmailDataset.TBL_documenti.NewRow
                dr_doc.DOC_cod_mai = dr_mai.MAI_codice
                dr_doc.DOC_filename = cFileName
                _SendEmailDataset.TBL_documenti.Rows.Add(dr_doc)
            End If
        ElseIf cTipoExecAPI = "B" Then
            dr_mai.MAI_send_xml = True
        End If
        _SendEmailDataset.AcceptChanges()

    End Sub

    ReadOnly Property DatiAzioniPostStampa() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DatiAzioniPostStampa

        Get
            Dim tmpArrList As ArrayList

            tmpArrList = New ArrayList
            tmpArrList.Add(New String() {"C", "1001", "Consorzio di acquisto"})
            tmpArrList.Add(New String() {"A", "1001", "Estrai file porzione file xml"})

            Return tmpArrList
        End Get

    End Property

    ReadOnly Property DatiAzioniPreInvio() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DatiAzioniPreInvio

        Get
            Dim tmpArrList As ArrayList

            tmpArrList = New ArrayList
            tmpArrList.Add(New String() {"C", "1001", "Consorzio di acquisto"})
            tmpArrList.Add(New String() {"A", "1001", "Allega Porzione di file xml alla mail"})

            Return tmpArrList
        End Get

    End Property

    ReadOnly Property StampaInProprio() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.StampaInProprio

        Get
            Return True
        End Get

    End Property

    Public Class OrdinaPDR
        Implements IComparer

        ' Calls CaseInsensitiveComparer.Compare with the parameters reversed.
        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer _
           Implements IComparer.Compare
            Dim nRtn As Integer

            nRtn = 0
            If x(1) < y(1) Then
                nRtn = -1
            ElseIf x(1) > y(1) Then
                nRtn = 1
            Else
                If x(2) < y(2) Then
                    nRtn = -1
                ElseIf x(2) > y(2) Then
                    nRtn = 1
                Else
                    If x(3) < y(3) Then
                        nRtn = -1
                    ElseIf x(3) > y(3) Then
                        nRtn = 1
                    End If
                End If
            End If
            Return nRtn

        End Function 'IComparer.Compare

    End Class

    Private Function GetTagXML(ByVal dr_exp As DataRow, ByVal aListaTag As String()) As String
        Dim cTagList As String
        Dim aTmp As String()
        Dim dr_tmp As DataRow
        Dim cRtn As String

        cRtn = ""
        If aListaTag.Length = 2 Then
            Try
                cRtn = dr_exp(aListaTag(1))
            Catch ex As Exception
                cRtn = ""
            End Try
        ElseIf aListaTag.Length > 2 Then
            cTagList = String.Concat(aListaTag(0), "_", aListaTag(1))
            If dr_exp.GetChildRows(cTagList).Length > 0 Then
                dr_exp = dr_exp.GetChildRows(cTagList)(0)
                Array.Reverse(aListaTag)
                aTmp = New String() {}
                ReDim aTmp(aListaTag.Length - 2)
                Array.Copy(aListaTag, aTmp, aListaTag.Length - 1)
                ReDim aListaTag(aTmp.Length - 1)
                Array.Copy(aTmp, aListaTag, aTmp.Length)
                Array.Reverse(aListaTag)
                cRtn = GetTagXML(dr_exp, aListaTag)
            End If
        End If
        Return cRtn

    End Function

    ' ************************************************************************** '
    ' Questa funzione serve a leggere un file XML con la codifica WINDOWS-1252 e '
    ' a caricarlo in un XMLDocument.                                             '
    ' ************************************************************************** '
    Private Function CaricaFileDatiToDS_excel(ByVal cFileName As String) As DataSet
        Dim dsExcel As DataSet
        Dim dbConn As String
        Dim oConn As OleDb.OleDbConnection
        Dim oCmd As OleDb.OleDbCommand
        Dim oDA As OleDb.OleDbDataAdapter
        Dim cFoglionome As String

        If cFileName > "" Then
            cFoglionome = "Foglio1"
            dbConn = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cFileName, ";Extended Properties=""Excel 8.0;HDR=YES;IMEX=1"";")

            oConn = New OleDb.OleDbConnection(dbConn)
            oConn.Open()
            oCmd = New OleDb.OleDbCommand(String.Concat("SELECT * FROM [", cFoglionome, "$]"), oConn)
            oCmd.CommandType = CommandType.Text
            oDA = New OleDb.OleDbDataAdapter
            oDA.SelectCommand = oCmd

            dsExcel = New DataSet()
            oDA.Fill(dsExcel)
            oConn.Close()
        Else
            dsExcel = Nothing
        End If
        Return dsExcel

    End Function

    Private Function RecuperaValoreXML(ByVal lGenerale As Boolean, ByVal cSottoTabella As String, ByVal cField As String, Optional ByVal cDefault As String = "")
        Dim cRelazione As String
        Dim cRtnValue As String
        Dim dr_tmp As DataRow

        Me.DatiGenerali = lGenerale
        cRelazione = Me.RecordDati.Table.TableName + "_" + cSottoTabella
        cRtnValue = cDefault
        If RecordDati.GetChildRows(cRelazione).Length > 0 Then
            dr_tmp = RecordDati.GetChildRows(cRelazione)(0)
            If Not IsDBNull(dr_tmp(cField)) Then
                cRtnValue = dr_tmp(cField)
            End If
        End If
        Return cRtnValue

    End Function

    Private Property DatiGenerali As Boolean
    Private Property PodInCaricamento As Integer
    Private Property FatturaInGestione As DataRow

    Private ReadOnly Property SezioneDocumentoPodInGestione As DataRow

        Get
            Return FatturaInGestione.GetChildRows("Fattura_SezioniDocumenti")(Me.PodInCaricamento)
        End Get

    End Property

    Private ReadOnly Property RecordDati As DataRow

        Get
            If DatiGenerali Then
                Return FatturaInGestione
            Else
                Return SezioneDocumentoPodInGestione
            End If

        End Get

    End Property

    Private Function TrovaValoreCanoneRai() As Decimal
        Dim adrtmp1() As DataRow
        Dim nValore As Decimal

        If _dr_mai_pf.DEF_raccolta = 1 Then
            adrtmp1 = FatturaInGestione.GetChildRows("Fattura_RigheRiepilogo20Servizio")
        Else
            adrtmp1 = SezioneDocumentoPodInGestione.GetChildRows("SezioniDocumenti_RiepilogoStampaxSezione")(0).GetChildRows("RiepilogoStampaxSezione_RigheRiepilogo20Contratto")
        End If

        nValore = 0
        For Each dr_tmp1 As DataRow In adrtmp1
            If "28,".Contains(dr_tmp1.Item("CodiceRigaRiepilogo").ToString() + ",") Then
                For Each dr_tmp2 As DataRow In dr_tmp1.GetChildRows("RigheRiepilogo20Contratto_RigaFattura")
                    If dr_tmp2.Item("Voce").ToString.ToUpper = "CANONE DI ABBONAMENTO ALLA TELEVISIONE PER USO PRIVATO".ToUpper Then
                        nValore = dr_tmp2.Item("Importo")
                    End If
                Next
            End If
        Next
        Return nValore

    End Function

    Private Sub Carica_UltimiConsumi(ByVal dr_src As DataSet, ByVal i As Integer)



    End Sub

End Class
