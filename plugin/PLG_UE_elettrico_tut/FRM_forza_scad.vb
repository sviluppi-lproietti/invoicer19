Public Class FRM_forza_scad

    Private _Condizione As String

    Public Property Condizione() As String

        Get
            Return _Condizione
        End Get
        Set(ByVal value As String)
            _Condizione = value
            CHB_dom_ban.Checked = value.Contains("B")
            CHB_dom_ban.Checked = value.Contains("D")
            CHB_rim_dir.Checked = value.Contains("R")
        End Set

    End Property


    Private Sub BTN_salva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_salva.Click

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub CHB_dom_ban_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CHB_dom_ban.CheckedChanged

        SetCondizione()

    End Sub

    Private Sub CHB_rim_dir_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CHB_rim_dir.CheckedChanged

        SetCondizione()

    End Sub

    Private Sub SetCondizione()

        _Condizione = ""
        If CHB_bon_ban.Checked Then
            _Condizione = String.Concat(_Condizione, "B")
        End If
        If CHB_dom_ban.Checked Then
            _Condizione = String.Concat(_Condizione, "D")
        End If
        If CHB_rim_dir.Checked Then
            _Condizione = String.Concat(_Condizione, "R")
        End If

    End Sub

    Private Sub CHB_bon_ban_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CHB_bon_ban.CheckedChanged

        SetCondizione()

    End Sub

End Class