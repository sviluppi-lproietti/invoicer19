<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_dati_protocollo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.TXB_num_prot = New System.Windows.Forms.TextBox
        Me.BTN_salva = New System.Windows.Forms.Button
        Me.BTN_ignora = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.TXB_data_prot = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Numero del protocollo"
        '
        'TXB_num_prot
        '
        Me.TXB_num_prot.Location = New System.Drawing.Point(149, 9)
        Me.TXB_num_prot.Multiline = True
        Me.TXB_num_prot.Name = "TXB_num_prot"
        Me.TXB_num_prot.Size = New System.Drawing.Size(136, 21)
        Me.TXB_num_prot.TabIndex = 1
        '
        'BTN_salva
        '
        Me.BTN_salva.Location = New System.Drawing.Point(68, 66)
        Me.BTN_salva.Name = "BTN_salva"
        Me.BTN_salva.Size = New System.Drawing.Size(75, 23)
        Me.BTN_salva.TabIndex = 3
        Me.BTN_salva.Text = "Salva"
        Me.BTN_salva.UseVisualStyleBackColor = True
        '
        'BTN_ignora
        '
        Me.BTN_ignora.Location = New System.Drawing.Point(166, 66)
        Me.BTN_ignora.Name = "BTN_ignora"
        Me.BTN_ignora.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignora.TabIndex = 4
        Me.BTN_ignora.Text = "Ignora"
        Me.BTN_ignora.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(115, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Data del protocollo"
        '
        'TXB_data_prot
        '
        Me.TXB_data_prot.Location = New System.Drawing.Point(149, 36)
        Me.TXB_data_prot.Multiline = True
        Me.TXB_data_prot.Name = "TXB_data_prot"
        Me.TXB_data_prot.Size = New System.Drawing.Size(136, 21)
        Me.TXB_data_prot.TabIndex = 2
        '
        'FRM_dati_protocollo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(293, 98)
        Me.Controls.Add(Me.TXB_data_prot)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.BTN_ignora)
        Me.Controls.Add(Me.BTN_salva)
        Me.Controls.Add(Me.TXB_num_prot)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FRM_dati_protocollo"
        Me.Text = "Dati protocollo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXB_num_prot As System.Windows.Forms.TextBox
    Friend WithEvents BTN_salva As System.Windows.Forms.Button
    Friend WithEvents BTN_ignora As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TXB_data_prot As System.Windows.Forms.TextBox
End Class
