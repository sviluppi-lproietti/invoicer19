<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_SwitchEcc
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.BTN_carica = New System.Windows.Forms.Button
        Me.BTN_annulla = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(548, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(26, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(150, 6)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(392, 20)
        Me.TextBox1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(141, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "File con record da annullare:"
        '
        'BTN_carica
        '
        Me.BTN_carica.Location = New System.Drawing.Point(134, 32)
        Me.BTN_carica.Name = "BTN_carica"
        Me.BTN_carica.Size = New System.Drawing.Size(150, 28)
        Me.BTN_carica.TabIndex = 3
        Me.BTN_carica.Text = "Carica i record da annullare"
        Me.BTN_carica.UseVisualStyleBackColor = True
        '
        'BTN_annulla
        '
        Me.BTN_annulla.Location = New System.Drawing.Point(330, 32)
        Me.BTN_annulla.Name = "BTN_annulla"
        Me.BTN_annulla.Size = New System.Drawing.Size(150, 28)
        Me.BTN_annulla.TabIndex = 4
        Me.BTN_annulla.Text = "Annulla"
        Me.BTN_annulla.UseVisualStyleBackColor = True
        '
        'FRM_SwitchEcc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(584, 72)
        Me.Controls.Add(Me.BTN_annulla)
        Me.Controls.Add(Me.BTN_carica)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "FRM_SwitchEcc"
        Me.Text = "Annulla importi eccedentari"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BTN_carica As System.Windows.Forms.Button
    Friend WithEvents BTN_annulla As System.Windows.Forms.Button
End Class
