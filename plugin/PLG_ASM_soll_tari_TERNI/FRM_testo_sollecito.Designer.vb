<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_testo_sollecito
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.TXB_testo_sollecito = New System.Windows.Forms.TextBox
        Me.BTN_salva = New System.Windows.Forms.Button
        Me.BTN_ignora = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Testo"
        '
        'TXB_testo_sollecito
        '
        Me.TXB_testo_sollecito.Location = New System.Drawing.Point(12, 25)
        Me.TXB_testo_sollecito.Multiline = True
        Me.TXB_testo_sollecito.Name = "TXB_testo_sollecito"
        Me.TXB_testo_sollecito.Size = New System.Drawing.Size(705, 326)
        Me.TXB_testo_sollecito.TabIndex = 1
        '
        'BTN_salva
        '
        Me.BTN_salva.Location = New System.Drawing.Point(241, 383)
        Me.BTN_salva.Name = "BTN_salva"
        Me.BTN_salva.Size = New System.Drawing.Size(75, 23)
        Me.BTN_salva.TabIndex = 2
        Me.BTN_salva.Text = "Salva"
        Me.BTN_salva.UseVisualStyleBackColor = True
        '
        'BTN_ignora
        '
        Me.BTN_ignora.Location = New System.Drawing.Point(365, 383)
        Me.BTN_ignora.Name = "BTN_ignora"
        Me.BTN_ignora.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignora.TabIndex = 3
        Me.BTN_ignora.Text = "Ignora"
        Me.BTN_ignora.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 367)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(318, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "&&IMPORTO_TOTALE_SOLL&& = indica l'importo totale del sollecito"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 354)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(112, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Variabili disponibili"
        '
        'FRM_testo_sollecito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(729, 418)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.BTN_ignora)
        Me.Controls.Add(Me.BTN_salva)
        Me.Controls.Add(Me.TXB_testo_sollecito)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FRM_testo_sollecito"
        Me.Text = "Testo lettera sollecito"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXB_testo_sollecito As System.Windows.Forms.TextBox
    Friend WithEvents BTN_salva As System.Windows.Forms.Button
    Friend WithEvents BTN_ignora As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
