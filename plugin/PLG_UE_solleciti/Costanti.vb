Public Class Costanti

    Public Const DatiFissi_Fattura = "FixFat"
    Public Const KEY_DF_autolettura_stampa = 10
    Public Const KEY_DF_autolettura_testo = 11

    Public Const itm_qd_Sollecitato = "SOLLECITATO/SOLLECITATO_ROW"

    ' Costante per la ricerca del numero delle fatture da caricare.
    Public Const itm_Fattura = "EMISSIONE/EMISSIONE_ROW"

    ' Costante per la localizzazione dei dati nel 
    Public Const itm_Testata_Fattura_row = "FATTURA/TESTATA_FATTURA/TESTATA_FATTURA_ROW"

    ' Dati dell'intestatario della fattura
    Public Const itm_nome = itm_Testata_Fattura_row + "/RAGIONE_SOCIALE_INTESTATARIO"
    Public Const itm_cod_cli = itm_Testata_Fattura_row + "/ID_INTESTATARIO"
    Public Const itm_ind_SedeLeg_1 = itm_Testata_Fattura_row + "/TIPO_ELETOPO_SEDELEGALE"
    Public Const itm_ind_SedeLeg_2 = itm_Testata_Fattura_row + "/ELETOPO_SEDELEGALE"
    Public Const itm_ind_SedeLeg_3 = itm_Testata_Fattura_row + "/NUMERO_SEDELEGALE"
    Public Const itm_ind_SedeLeg_4 = itm_Testata_Fattura_row + "/SUFFISSO_SEDELEGALE"
    Public Const itm_ind_SedeLeg_5 = itm_Testata_Fattura_row + "/LOCALITA_SEDELEGALE"
    Public Const itm_ind_SedeLeg_6 = itm_Testata_Fattura_row + "/COMUNE_SEDELEGALE"
    Public Const itm_ind_SedeLeg_7 = itm_Testata_Fattura_row + "/CAP_SEDELEGALE"
    Public Const itm_ind_SedeLeg_8 = itm_Testata_Fattura_row + "/PROVINCIA_SIGLA_SEDELEGALE"
    Public Const itm_int_nazione = itm_Testata_Fattura_row + "/NAZIONE_SEDELEGALE"

    ' Dati del recapito della fatture
    Public Const itm_rec_nome = itm_Testata_Fattura_row + "/RECAPITO_FATTURA"
    Public Const itm_rec_sito_1 = itm_Testata_Fattura_row + "/TIPO_ELETOPO_SPEDIZIONE"
    Public Const itm_rec_sito_2 = itm_Testata_Fattura_row + "/ELETOPO_SPEDIZIONE"
    Public Const itm_rec_sito_3 = itm_Testata_Fattura_row + "/NUMERO_SPEDIZIONE"
    Public Const itm_rec_sito_4 = itm_Testata_Fattura_row + "/SUFFISSO_SPEDIZIONE"
    Public Const itm_rec_sito_5 = itm_Testata_Fattura_row + "/LOCALITA_SPEDIZIONE"
    Public Const itm_rec_sito_6 = itm_Testata_Fattura_row + "/COMUNE_SPEDIZIONE"
    Public Const itm_rec_sito_7 = itm_Testata_Fattura_row + "/CAP_SPEDIZIONE"
    Public Const itm_rec_sito_8 = itm_Testata_Fattura_row + "/PROVINCIA_SIGLA_SPEDIZIONE"
    Public Const itm_rec_nazione = itm_Testata_Fattura_row + "/NAZIONE_SPEDIZIONE"

    Public Const itm_print_bol = itm_Testata_Fattura_row + "/STAMPA_BOLLETTINO"

    


    Public Const itm_parametri = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/OPZIONI_TARIFFARIE/OPZIONI_TARIFFARIE_ROW/PARAMETRI_OT/PARAMETRI_OT_ROW" 'DATA_INIZIO_CONSUMO"
    Public Const itm_campi_contr = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/CAMPI_CONTRATTO/CAMPI_CONTRATTO_ROW"
    Public Const itm_sito_forn = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/SITO_FORNITURA/SITO_FORNITURA_ROW/ZONE_SITO/ZONE_SITO_ROW"
    Public Const itm_campi_punto_forn = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/CAMPI_PUNTO_FORNITURA/CAMPI_PUNTO_FORNITURA_ROW"

    Public Const itm_ocr = itm_Testata_Fattura_row + "/RATA/RATA_ROW/OCR_RATA"

    ' *****************************
    Public Const itm_des_offerta = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/OPZIONI_TARIFFARIE/OPZIONI_TARIFFARIE_ROW"
    Public Const itm_data_ini_fat = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/DATE_COMPETENZA/DATE_COMPETENZA_ROW/INIZIO_RIPETITIVI" 'DATA_INIZIO_CONSUMO"
    Public Const itm_data_fin_fat = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/DATE_COMPETENZA/DATE_COMPETENZA_ROW/FINE_RIPETITIVI"    'DATA_FINE_CONSUMO"
    Public Const itm_punto_fornitura = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/ID_PUNTO_FORNITURA"

    Public Const itm_gruppo_misura = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/DESCR_TIPO_GRUPPO"
    Public Const itm_strumento_misura = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/DESCR_TIPO_STRUMENTO"
    Public Const itm_matr_strumento = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/MATRICOLA"
    Public Const itm_costruttore = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/DESCR_COSTRUTTORE"
    Public Const itm_strum_anno = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/ANNO_COSTRUZIONE"
    Public Const itm_strum_sigla = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/SIGLA"
    Public Const itm_prot_reg_iva = itm_Testata_Fattura_row + "/LABEL_PROTOCOLLO_REG_IVA"
    Public Const itm_pot_disp = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/CAMPI_PUNTO_FORNITURA/CAMPI_PUNTO_FORNITURA_ROW"

    Public Const itm_descr_opz_tar = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/OPZIONI_TARIFFARIE/OPZIONI_TARIFFARIE_ROW/DESCR_OPZIONE_TARIFFARIA"




    '' Costanti per il recupero dei dati della sezione "La tua bolletta in sintesi"



    Public Const itm_tbs_pod_matricola = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/MATRICOLA"

    Public Const itm_COM_Testata_Fattura_row = itm_Fattura + "/" + itm_Testata_Fattura_row
    '' Costanti per la ricerca dei dati della fornitura
    '' Flag 4
    Public Const itm_anno_fattura = itm_Testata_Fattura_row + "/ANNO_FATTURA"
    '' Flag 5
    Public Const itm_num_fattura = itm_Testata_Fattura_row + "/NUMERO_FATTURA"
    '' Flag 6
    Public Const itm_data_emiss = itm_Testata_Fattura_row + "/DATA_FATTURA"
    '' Flag 7
    Public Const itm_tbs_totale = itm_Testata_Fattura_row + "/TOTALE_FATTURA"
    '' Flag 9    
    Public Const itm_num_ute = itm_Testata_Fattura_row + "/ID_INTESTATARIO"
    
    ''Flag 11
    Public Const itm_cf = itm_Testata_Fattura_row + "/CODICE_FISCALE_INTESTATARIO"
    ''Flag 12
    Public Const itm_piva = itm_Testata_Fattura_row + "/PARTITA_IVA_INTESTATARIO"
    ''Flag 13
    
    '' Flag 36
    Public Const itm_flag_tipo_pag = itm_Testata_Fattura_row + "/FLAG_TIPO_PAGAMENTO"
    '' Flag 37
    Public Const itm_descr_tipo_pag = itm_Testata_Fattura_row + "/TIPO_PAGAMENTO"
    '' Flag 81
    Public Const itm_tot_imponibile = itm_Testata_Fattura_row + "/PIEDE_FATTURA/PIEDE_FATTURA_ROW/IMPORTO_IMPONIBILE"
    '' Flag 82
    Public Const itm_tot_iva = itm_Testata_Fattura_row + "/PIEDE_FATTURA/PIEDE_FATTURA_ROW/IMPORTO_IVA"
    '' Flag 83
    Public Const itm_tot_fattura = itm_Testata_Fattura_row + "/PIEDE_FATTURA/PIEDE_FATTURA_ROW/IMPORTO_TOTALE"
    '' Flag 86
    Public Const itm_scadenza = itm_Testata_Fattura_row + "/RATA/RATA_ROW/DATA_SCADENZA"
    '' Flag 98
    Public Const itm_descrizione_bc = itm_Testata_Fattura_row + "/BANCA_APPOGGIO/NOME_BANCA"
    '' Flag 113
    Public Const itm_nome_banca = itm_Testata_Fattura_row + "/BANCA_APPOGGIO/BANCA_APPOGGIO_ROW/NOME_BANCA"
    '' Flag 114
    Public Const itm_iban = itm_Testata_Fattura_row + "/BANCA_APPOGGIO/BANCA_APPOGGIO_ROW/CODICE_IBAN"
    '' Flag 132


    '' Flag 160

    '' Flag 361
    Public Const itm_valore_fatturato = "SEZIONE_LETTURE_CONSUMI/SEZIONE_LETTURE_CONSUMI_ROW/LETTURA/LETTURA_ROW/CONSUMO/CONSUMO_ROW/CONVAL/CONVAL_ROW/VALORE_CONSUMO"
    '' Flag 362
    Public Const itm_vf_unita_mis = "SEZIONE_LETTURE_CONSUMI/SEZIONE_LETTURE_CONSUMI_ROW/LETTURA/LETTURA_ROW/CONSUMO/CONSUMO_ROW/CONVAL/CONVAL_ROW/UNITA_MISURA_CONSUMO"


    ' Societ� Distributrice
    Public Const itm_dis_rag_soc = itm_Testata_Fattura_row + "/AZIENDA/AZIENDA_ROW/RAGIONE_SOCIALE_AZIENDA"
    Public Const itm_dis_sitoweb = itm_Testata_Fattura_row + "/AZIENDA/AZIENDA_ROW/SITO_INTERNET_AZIENDA"
    Public Const itm_dis_email = itm_Testata_Fattura_row + "/AZIENDA/AZIENDA_ROW/EMAIL_AZIENDA"
    Public Const itm_dis_fax = itm_Testata_Fattura_row + "/AZIENDA/AZIENDA_ROW/FAX_AZIENDA"
    Public Const itm_dis_sg_rag_soc = itm_Testata_Fattura_row + "/ELENCO_DISTRIBUTORI/ELENCO_DISTRIBUTORI_ROW/RAGIONE_SOCIALE_DISTRIBUTORE"

    'Public Const itm_gruppo_misura = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/DESCR_TIPO_GRUPPO"
    'Public Const itm_strumento_misura = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/DESCR_TIPO_STRUMENTO"
    'Public Const itm_matr_strumento = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/MATRICOLA"
    'Public Const itm_costruttore = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/DESCR_COSTRUTTORE"
    'Public Const itm_strum_anno = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/ANNO_COSTRUZIONE"
    'Public Const itm_strum_sigla = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO/GRUPPO_MISURA_PUNTO_ROW/COMPOSIZIONE_GRUPPO_MISURA/COMPOSIZIONE_GRUPPO_MISURA_ROW/STRUMENTO_MISURA/STRUMENTO_MISURA_ROW/SIGLA"
    'Public Const itm_prot_uev = itm_Testata_Fattura_row + "/LABEL_PROTOCOLLO_REG_IVA"
    'Public Const itm_pot_disp = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/CAMPI_PUNTO_FORNITURA/CAMPI_PUNTO_FORNITURA_ROW"

    Public Const itm_GruppoMisura = "GRUPPO_MISURA_PUNTO_ROW"
    'Public Const itm_GruppiMisura = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO"
    Public Const itm_GruppiMisura = "CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO"
    Public Const itm_stm_dal = "DAL"
    Public Const itm_stm_al = "AL"

    Public Const itm_insoluti = itm_Testata_Fattura_row + "/INSOLUTI/INSOLUTI_ROW"

    Public Const itm_let_mis_ext = "CONVAL/CONVAL_ROW/"

    Public Const itm_POD_fatturati = itm_Testata_Fattura_row + "/RIEPILOGO_X_POD/RIEPILOGOXPOD_ROW"
    Public Const itm_SEZ_fatt = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW"

    ' ************************************************************************************ '
    ' Costanti utili per il recupero delle informazioni sui dati della fornitura:          '
    ' - Codice del POD;                                                                    '
    ' - ubicazione del POD;                                                                '
    ' - inizio della gestione;                                                             '
    ' ************************************************************************************ '

    ' ************************************************************************************ '
    ' Tag per il recupero delle informazioni dalla sezione del POD.                        '
    ' ************************************************************************************ '
    Public Const itm_POD_idubicazione = "ID_UBICAZIONE"
    Public Const itm_POD_numero = "POD_PDR"
    Public Const itm_descr_contratto = "CONTRATTO/CONTRATTO_ROW/DESCR_TIPO_CONTRATTO"
    Public Const itm_anno_contratto = "CONTRATTO/CONTRATTO_ROW/ANNO_CONTRATTO"
    Public Const itm_num_contratto = "CONTRATTO/CONTRATTO_ROW/ID_CONTRATTO"

    ' ************************************************************************************ '
    ' Tag per il recupero delle informazioni dalla sezione della fattura.                  '
    ' ************************************************************************************ '
    ' ************************************************************************************ ' 
    ' LP 12/08/2010 Modificato il recupero dell'ubicazione del punto di fornitura in quan- '
    ' to il tag "PUNTO_FORNITURA/PUNTO_FORNITURA_ROW" non fornisce tutti i dati necessari, ' 
    ' mentre il tag "SITO_FORNITURA/SITO_FORNITURA_ROW" si.                                '
    ' ************************************************************************************ ' 
    ' Private Const SEZ_dati_ub_pf = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/"
    ' Private Const EST_ub_pf = "PUNTO"
    Private Const SEZ_dati_ub_pf = "CONTRATTO/CONTRATTO_ROW/SITO_FORNITURA/SITO_FORNITURA_ROW/"
    Private Const EST_ub_pf = "_SITO"
    Public Const itm_pf_sito_1 = SEZ_dati_ub_pf + "TIPO_ELETOPO" + EST_ub_pf
    Public Const itm_pf_sito_2 = SEZ_dati_ub_pf + "ELETOPO" + EST_ub_pf
    Public Const itm_pf_sito_3 = SEZ_dati_ub_pf + "NUMERO" + EST_ub_pf
    Public Const itm_pf_sito_4 = SEZ_dati_ub_pf + "SUFFISSO" + EST_ub_pf
    Public Const itm_pf_sito_5 = SEZ_dati_ub_pf + "CAP" + EST_ub_pf
    Public Const itm_pf_sito_6 = SEZ_dati_ub_pf + "COMUNE" + EST_ub_pf
    Public Const itm_pf_sito_7 = SEZ_dati_ub_pf + "PROVINCIA_SIGLA" + EST_ub_pf
    Public Const itm_pf_sito_8 = SEZ_dati_ub_pf + "NAZIONE" + EST_ub_pf

    Public Const itm_SEZ_contratto = "CONTRATTO/CONTRATTO_ROW/"
    Public Const itm_inizio_gestione = itm_SEZ_contratto + "DATA_INIZIO_GESTIONE"

    Public Const itm_DIS_nome = itm_SEZ_contratto + "/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/AZIENDA_PROPRIETARIA_PUNTO"
    Public Const itm_DIS_telefono = itm_SEZ_contratto + "PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/NUMERO_VERDE"

    ' Righe di fatturazione
    Public Const itm_SEZ_righe_fatt = "SEZ_STAMPA_RFATTURA/SEZ_STAMPA_RFATTURA_ROW"

    Public Const itm_SEZ_msg = "MESSAGGI_CONTRATTO/MESSAGGI_CONTRATTO_ROW"

    ' righe dei consumi 
    Public Const itm_COM_sezione_consumi = "SEZIONE_LETTURE_CONSUMI/SEZIONE_LETTURE_CONSUMI_ROW"

    Public Const itm_fattura_multi = "MULTIPOS"
    Public Const cnt_ThresholdImporto = 15

End Class
