﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Public Class row_025_00Entity
    Inherits rowBase

    Public Sub New(ByVal cLinea As String)
        MyBase.New(cLinea)

    End Sub

    Public ReadOnly Property codice_fattura As String

        Get
            Return aValue(2)
        End Get

    End Property

    Public ReadOnly Property note_fattura As String
        Get
            Return aValue(3)
        End Get

    End Property

End Class


