﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Public Class row_050_25Entity
    Inherits rowBase

    Public Sub New(ByVal cLinea As String)
        MyBase.New(cLinea)

    End Sub

    Public ReadOnly Property codice_fattura As String

        Get
            Return aValue(2)
        End Get

    End Property

    Public ReadOnly Property descrizione_voce As String

        Get
            Return aValue(3)
        End Get

    End Property

    Public ReadOnly Property unita_misura_importo As String
        Get
            Return aValue(4)
        End Get

    End Property

    Public ReadOnly Property importo As Decimal
        Get
            Return aValue(5)
        End Get

    End Property

    Public ReadOnly Property codice_iva As String
        Get
            Return aValue(6)
        End Get

    End Property

End Class
