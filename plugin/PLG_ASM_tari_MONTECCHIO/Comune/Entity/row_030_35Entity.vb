﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Public Class row_030_35Entity
    Inherits rowBase

    Public Sub New(ByVal cLinea As String)
        MyBase.New(cLinea)

    End Sub

    Public ReadOnly Property CODICE_FATTURA As String

        Get
            Return aValue(2)
        End Get

    End Property
    Public ReadOnly Property UTENZA As String

        Get
            Return aValue(3)
        End Get

    End Property
    Public ReadOnly Property COMUNE_AMMINISTRATIVO As String

        Get
            Return aValue(4)
        End Get

    End Property
    Public ReadOnly Property COMUNE_CATASTALE As String

        Get
            Return aValue(5)
        End Get

    End Property
    Public ReadOnly Property CODICE_COMUNE_CATASTALE As String

        Get
            Return aValue(6)
        End Get

    End Property
    Public ReadOnly Property TIPO_CATASTO As String

        Get
            Return aValue(7)
        End Get

    End Property
    Public ReadOnly Property SEZIONE As String

        Get
            Return aValue(8)
        End Get

    End Property
    Public ReadOnly Property FOGLIO As String

        Get
            Return aValue(9)
        End Get

    End Property
    Public ReadOnly Property SUBALTERNO As String

        Get
            Return aValue(10)
        End Get

    End Property
    Public ReadOnly Property PARTICELLA As String

        Get
            Return aValue(11)
        End Get

    End Property
    Public ReadOnly Property SEGUE_PARTICELLA As String

        Get
            Return aValue(12)
        End Get

    End Property
    Public ReadOnly Property PROPRIETARIO As String

        Get
            Return aValue(13)
        End Get

    End Property
    Public ReadOnly Property CF_PROPRIETARIO As String

        Get
            Return aValue(14)
        End Get

    End Property
    Public ReadOnly Property CATEGORIA_1 As String

        Get
            Return aValue(15)
        End Get

    End Property
    Public ReadOnly Property TITOLO_OCCUPAZIONE As String

        Get
            Return aValue(16)
        End Get

    End Property
    Public ReadOnly Property CATEGORIA_2 As String

        Get
            Return aValue(17)
        End Get

    End Property
    Public ReadOnly Property DESTINAZIONE_USO As String

        Get
            Return aValue(18)
        End Get

    End Property

End Class
