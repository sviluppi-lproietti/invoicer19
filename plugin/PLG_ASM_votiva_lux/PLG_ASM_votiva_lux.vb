Imports System.Windows.Forms

Public Class PLG_ASM_votiva_lux
    Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib

    Const UpperFileSize = 3000000
    Const LowerFileSize = 2000000

    Const cns_tsm_sel_con_bollettino = "TSM_sel_con_bollettino"
    Const cns_tsm_sel_con_allegato = "TSM_sel_con_allegato"

    Private _TipoCaricamento As Integer
    Private _SessioneNuova As Boolean
    Private _CodiceProgressivo As Integer
    Private _SessioneDati As Xml.XmlNode
    Private _SessioneTrasformazioniFileName As String
    Private _SessionePath As String
    Private _CodiceModulo As Integer
    Private _FeedString As String
    Private _QuickDataset As DS_QD_ASM_vol
    Private _PrintFatturaDataset As DS_PF_ASM_vol
    Private _PrintBollettinoDataset As DS_PB_ASM_vol
    Private _RecordCount As Integer
    Private _xmlDati_qd As Xml.XmlDocument
    Private _MessaggiAvanzamento(5) As String
    Private _ErroriLievi As String
    Private _SessioneDatiFissiFileName As String
    Private _SessioneDatiPath As String
    Private _LogDS As DataSet
    Private _ResultAL As ArrayList
    Private _MasterRecord As Integer
    Private _data_trans As DS_trans
    Private _ListaGiri As ArrayList
    Private _DbGiriConnString As String
    Private _NuoviToponimi As Boolean
    Private _SendEmailDataset As DS_SE_asm_vol

    Enum ListaDataset
        DAS_quick = 0
        DAS_fattura = 1
        DAS_bollettino = 2
        DAS_send_email = 999
    End Enum

    ReadOnly Property ResultAL() As ArrayList Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.ResultAL

        Get
            Return _ResultAL
        End Get

    End Property

    WriteOnly Property SessioneDatiPath() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.SessioneDatiPath

        Set(ByVal value As String)
            _SessioneDatiPath = value
        End Set

    End Property

    WriteOnly Property SessioneTrasformazioniFileName() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.SessioneTrasformazioniFileName

        Set(ByVal value As String)
            _SessioneTrasformazioniFileName = value
        End Set

    End Property

    WriteOnly Property SessioneDatiFissiFileName() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.SessioneDatiFissiFileName

        Set(ByVal value As String)
            _SessioneDatiFissiFileName = value
        End Set

    End Property

    WriteOnly Property SessioneFld() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.SessioneFld

        Set(ByVal value As String)
            _SessionePath = value
        End Set

    End Property

    WriteOnly Property SezioneDati() As Xml.XmlNode Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.SezioneDati

        Set(ByVal value As Xml.XmlNode)
            _SessioneDati = value
        End Set

    End Property

    WriteOnly Property CodiceModulo() As Integer Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.CodiceModulo

        Set(ByVal value As Integer)
            _CodiceModulo = value
        End Set

    End Property

    ReadOnly Property NomePlugIn() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.NomePlugIn

        Get
            Return "Plug-in per la stampa delle fatture della Luce Votiva di ASM."
        End Get

    End Property

    Property MessaggiAvanzamento() As String() Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.MessaggiAvanzamento

        Get
            Return _MessaggiAvanzamento
        End Get
        Set(ByVal value As String())
            _MessaggiAvanzamento = value
        End Set

    End Property

    ReadOnly Property MessaggioAvviso() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.MessaggioAvviso

        Get
            Dim cValue As String

            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura, ListaDataset.DAS_bollettino
                    cValue = String.Concat("Attenzione!! Ricordiamo che prima di procedere con la stampa dei moduli selezionati", vbCr, "� necessario verificare i seguenti dati:", vbCr, "- numero del protocollo;", vbCr, "- data del protocollo.", vbLf, vbLf, "Posso procedere con la stampa di quanto selezionato?")
                Case Is = ListaDataset.DAS_send_email
                    cValue = String.Concat("Da modificare!!!!!!!!!!!!!!!!!")
                Case Else
                    cValue = ""
            End Select
            Return cValue
        End Get

    End Property

    ReadOnly Property ListaControlliForm() As ArrayList Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.ListaControlliFormNuovaSessione

        Get
            Dim aCtr As ArrayList

            aCtr = New ArrayList
            aCtr.Add(New String() {"LBL_input_1", "File contenente i solleciti ...:", "3;16", "155;13"})
            aCtr.Add(New String() {"TXB_input_1", "&1", "160;12", "610;20", "199"})
            aCtr.Add(New String() {"BTN_fil_input_1", "", "775;10", "27;23"})
            aCtr.Add(New String() {"LBL_output_998", "Cartella di destinazione .:", "3;42", "155;13"})
            aCtr.Add(New String() {"TXB_output_998", "&998", "160;38", "610;20", "201"})
            aCtr.Add(New String() {"BTN_fld_output_998", "", "775;36", "27;23"})
            aCtr.Add(New String() {"LBL_output_999", "Descrizione archivio ......:", "3;68", "155;13"})
            aCtr.Add(New String() {"TXB_output_999", "", "160;64", "M;640;122", "202"})
            Return aCtr
        End Get

    End Property

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.CheckDatiSessione
        Dim _lObb As Boolean
        Dim lRtn As Boolean

        _lObb = False
        For Each aValue As String() In aDati
            Select Case aValue(0)
                Case Is = 199
                    _lObb = True
            End Select
        Next
        lRtn = True
        If _SessioneNuova Then
            If Not _lObb Then
                MessageBox.Show("Attenzione! Non sono � stato indicato nessun file contenente i dati. Non posso procedere con la creazione della sessione.")
                lRtn = False
            End If
        End If
        Return lRtn

    End Function

    Private Sub SetRecordIDX(ByVal xmlIndice As Xml.XmlDocument, ByVal xmlRecord As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xmlIndice.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xmlRecord.AppendChild(xiTmp)

    End Sub

    Public ReadOnly Property QuickDataset() As DataSet Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.QuickDataset

        Get
            Return _QuickDataset
        End Get

    End Property

    Public ReadOnly Property QuickDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.QuickDataset_MainTableKey

        Get
            Return "MAI_codice"
        End Get

    End Property

    Public ReadOnly Property QuickDataset_MainTableName() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.QuickDataset_MainTableName

        Get
            Return "TBL_main"
        End Get

    End Property

    ReadOnly Property CurrentRecordNumber() As Integer Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.CurrentRecordNumber

        Get
            Return _RecordCount
        End Get

    End Property

    Sub GoToRecordNumber(ByVal nRecord As Integer) Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.GoToRecordNumber

        _RecordCount = nRecord

    End Sub

    WriteOnly Property FeedString() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.FeedString

        Set(ByVal value As String)
            _FeedString = value
        End Set

    End Property

#Region "SetUp per Datagrid di visualizzazione delle righe da stampare"

    Sub ImpostaDGVDati(ByVal DGV As DataGridView) Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.ImpostaDGVDati

        With DGV.Columns
            .Clear()
            If _TipoCaricamento = 1 Then
                .Add(Col_DaStampare)
            End If
            If _TipoCaricamento = 2 Then
                .Add(Col_DaInviare)
            End If
            .Add(Col_Errore_code)
            .Add(Col_Errore_desc)
            .Add(Col_Progressivo)
            .Add(Col_NumeroUtente)
            .Add(Col_NumeroFattura)
            .Add(Col_Nominativo)
            .Add(COL_Indirizzo_Recapito)
            .Add(COL_CAP_Recapito)
            .Add(COL_Citta_Recapito)
            .Add(COL_Print_Bollettino)
            .Add(COL_SpedByMail)
        End With

    End Sub

    Private Function Col_DaStampare() As System.Windows.Forms.DataGridViewCheckBoxColumn
        Dim col As System.Windows.Forms.DataGridViewCheckBoxColumn

        col = New System.Windows.Forms.DataGridViewCheckBoxColumn
        With col
            .HeaderText = "Stampa"
            .DataPropertyName = "DEF_toprint"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .Width = 50
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_DaInviare() As System.Windows.Forms.DataGridViewCheckBoxColumn
        Dim col As System.Windows.Forms.DataGridViewCheckBoxColumn

        col = New System.Windows.Forms.DataGridViewCheckBoxColumn
        With col
            .HeaderText = "Invia"
            .DataPropertyName = "DEF_toprint"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .Width = 50
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Errore_code() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errcode"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Errore_desc() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errdesc"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Progressivo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Progressivo"
            .DataPropertyName = "MAI_codice"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 70
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroUtente() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Utente"
            .DataPropertyName = "MAI_num_utente"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 90
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroFattura() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Fattura"
            .DataPropertyName = "MAI_num_fattura"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 90
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Nominativo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nominativo"
            .DataPropertyName = "MAI_nominativo"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Indirizzo_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Indirizzo Recapito"
            .DataPropertyName = "MAI_indirizzo_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_CAP_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "CAP"
            .DataPropertyName = "MAI_cap_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Citta_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Citt�"
            .DataPropertyName = "MAI_citta_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Print_Bollettino()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Bollettino"
            .DataPropertyName = "MAI_print_bol"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_Print_allegato()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Allegato"
            .DataPropertyName = "MAI_print_all"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_SpedByMail()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Sped. Email"
            .DataPropertyName = "DEF_sendbymail"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

#End Region

    Public ReadOnly Property ElementiMenuDedicati() As ArrayList Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.ElementiMenuDedicati

        Get
            Dim MenuEntry As ToolStripMenuItem
            Dim aMenuItem As ArrayList

            aMenuItem = New ArrayList
            MenuEntry = New ToolStripMenuItem("Stampa Solleciti")

            aMenuItem.Add(New Object() {"n", "TSM_sessione_stampa", MenuEntry})

            Return aMenuItem

        End Get

    End Property
    
    ' ************************************************************************** '
    ' Imposto correttamente la parte di selezione per la stampa dei record. Se   '
    ' non dobbiamo mantenere la selezione o se la dobbiamo mantenere ma il re-   ' 
    ' cord non � stampabile metto a "NON STAMPABILE (False)" il campo .          '
    ' ************************************************************************** '
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String) Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.SetSelectable
        Dim dr_mai_qd As DS_QD_ASM_vol.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_ASM_vol.TBL_mainRow)
        dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
        If cFilter > "" Then
            dr_mai_qd.DEF_selectable = dr_mai_qd.DEF_selectable And dr_mai_qd(cFilter)
        End If

        ' ************************************************************************** '
        ' Pulisce i campi relativi agli errori in quanto ci stiamo occupando di un   '
        ' nuovo modulo.                                                              '
        ' ************************************************************************** '
        If dr_mai_qd.DEF_selectable Then
            dr_mai_qd.Item("DEF_errcode") = 0
            dr_mai_qd.Item("DEF_errdesc") = ""
        End If

    End Sub

    Private Function GetValueDatiFissi(ByVal cTable As String, ByVal cField As String) As String
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim cRtn As String

        Try
            xmlDF.Load(_SessioneDatiFissiFileName)
            xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))

            cRtn = xmlTmp.InnerText
            cRtn = cRtn.Replace("{\TBL_main;MAI_tot_sol;2;#,##0.00/}", "&IMPORTO_TOTALE_SOLL&")
            xmlDF = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    Private Sub SetValueDatiFissi(ByVal cTable As String, ByVal cField As String, ByVal cValue As String)
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim xaTmp As Xml.XmlAttribute
        Dim xnWork2 As Xml.XmlNode

        cValue = cValue.Replace("&IMPORTO_TOTALE_SOLL&", "{\TBL_main;MAI_tot_sol;2;#,##0.00/}")

        xmlDF.Load(_SessioneDatiFissiFileName)
        xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))
        If Not xmlTmp Is Nothing Then
            xmlTmp = xmlTmp.SelectSingleNode("valore")
            xmlTmp.InnerText = cValue
        Else
            'Crea il nodo
            xmlTmp = xmlDF.CreateElement("item")
            xaTmp = xmlDF.CreateAttribute("codice")
            xaTmp.Value = cField
            xmlTmp.Attributes.Append(xaTmp)
            xnWork2 = xmlDF.CreateElement("valore")
            xnWork2.InnerText = cValue
            xmlTmp.AppendChild(xnWork2)
            xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable)).AppendChild(xmlTmp)
        End If
        xmlDF.Save(_SessioneDatiFissiFileName)
        xmlDF = Nothing

    End Sub

    Sub RemovePRNDSErrorRecord() Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.RemovePRNDSErrorRecord
        Dim nLinkCode As Integer

        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                For Each dr_mai_pd As DS_PF_ASM_vol.TBL_mainRow In _PrintFatturaDataset.TBL_main.Rows
                    nLinkCode = dr_mai_pd(LinkField_QuickDS_FullDS)
                    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                        dr_mai_pd.Delete()
                    End If
                Next
                _PrintFatturaDataset.TBL_main.AcceptChanges()
            Case Is = ListaDataset.DAS_bollettino
                For Each dr_mai_pd As DS_PB_ASM_vol.TBL_fat_x_ratRow In _PrintBollettinoDataset.TBL_fat_x_rat.Rows
                    nLinkCode = dr_mai_pd(LinkField_QuickDS_FullDS)
                    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                        dr_mai_pd.Delete()
                    End If
                Next
                _PrintBollettinoDataset.TBL_fat_x_rat.AcceptChanges()
        End Select

    End Sub

    ReadOnly Property LogDSStampa() As DataSet Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.LogDSStampa

        Get
            LoadDSLog()
            Return _LogDS
        End Get

    End Property

    Private Sub LoadDSLog()
        Dim dr As DataRow

        For Each dr_qds As DS_QD_ASM_vol.TBL_mainRow In _QuickDataset.TBL_main.Rows
            If dr_qds.DEF_errcode < 0 Then
                dr = _LogDS.Tables(0).NewRow
                dr("LOG_desc_rec_1") = String.Concat("Progressivo: ", dr_qds.MAI_codice, " Nominativo: ", dr_qds.MAI_nominativo, " Numero Fattura: ", dr_qds.MAI_num_fattura)
                dr("LOG_stato") = dr_qds.DEF_errcode
                dr("LOG_desc_err") = dr_qds.DEF_errdesc
                _LogDS.Tables(0).Rows.Add(dr)
            End If
        Next

    End Sub

    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String) Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.SetPrintable
        Dim dr_mai_qd As DS_QD_ASM_vol.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_ASM_vol.TBL_mainRow)
        Select Case _SelectFromPlugInType
            Case Is = cns_tsm_sel_con_bollettino.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_bol
            Case Is = cns_tsm_sel_con_allegato.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_all
        End Select

    End Sub

    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String() Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.GetSelectedRow
        Dim dr_mai_qd As DS_QD_ASM_vol.TBL_mainRow
        Dim aResult As String()
        Dim cFilter As String

        Select Case _SelectFromPlugInType
            Case Is = "tsm_ric_progressivo"
                cFilter = "MAI_codice"
        End Select
        aResult = New String() {"MAI_codice", ""}
        For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
            If dr_mai_qd(cFilter) = cValue Then
                aResult(1) = dr_mai_qd.MAI_codice
            End If
        Next
        Return aResult

    End Function

    WriteOnly Property FiltroSelezione() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.FiltroSelezione

        Set(ByVal value As String)

        End Set

    End Property

#Region "Procedure utili per la creazione della sessione di stampa"

    Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList) Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.CreaDataBaseSessione
        Dim aValue As String()
        Dim xdWork1 As Xml.XmlDocument
        Dim xdWork2 As Xml.XmlDocument
        Dim nRecord As Integer
        Dim xiWork1 As Xml.XmlNode
        Dim xiWork2 As Xml.XmlNode
        Dim xiRecord As Xml.XmlNode
        Dim cSortValue As String
        Dim dsDati As DataSet
        Dim nRow As Integer
        Dim dr As DataRow
        Dim aRowFatt As DataRow()
        Dim dr_mai As DS_trans.TBL_mainRow
        Dim dr_fat As DS_trans.TBL_fatturaRow

        _ResultAL = New ArrayList
        ' Copia i file dei dati dalla cartella dove sono attualmente 
        ' nella cartella dati della sessione
        ImpostaMessaggi(New String() {"", "", "", "0", _DatiSessione.Count, "Elaborazione dei file da caricare...."})

        ' Copia dei file indicati nella loro posizione
        For Each aValue In _DatiSessione
            ImportaFileInSessione(aValue(0), aValue(1))
        Next

        ImpostaMessaggi(New String() {"", "", "", "", CInt(_MessaggiAvanzamento(4)) + _ResultAL.Count, "Elaborazione dei file della cartella di archiviazione..."})
        xdWork2 = New Xml.XmlDocument
        xdWork2.AppendChild(xdWork2.CreateNode(Xml.XmlNodeType.Element, "FATTURA", ""))
        For Each aValue In _ResultAL
            If (aValue(3) = 199) Then
                Try
                    dsDati = CaricaFileDatiToDS(String.Concat(_SessionePath, aValue(1)))
                    ' Carica i dati dal database
                    _data_trans = New DS_trans
                    nRow = 0
                    While dsDati.Tables(0).Rows.Count > nRow
                        dr = dsDati.Tables(0).Rows(nRow)
                        aRowFatt = dsDati.Tables(0).Select(String.Concat("ANNO_FAT = '", dr("ANNO_FAT"), "' and NUM_FAT = '", dr("NUM_FAT"), "'"))

                        ' Estrazione dei dati
                        ExtractData(aRowFatt)
                        dr_mai = _data_trans.TBL_main.Rows(_data_trans.TBL_main.Rows.Count - 1)
                        dr_fat = _data_trans.TBL_fattura.Rows(_data_trans.TBL_fattura.Rows.Count - 1)
                        xiRecord = xdWork2.CreateNode(Xml.XmlNodeType.Element, "FATTURA_ROW", "")
                        SetRecordIDX(xdWork2, xiRecord, "MAI_num_utente", dr_mai.MAI_sog_id)
                        SetRecordIDX(xdWork2, xiRecord, "MAI_num_fattura", dr_fat.FAT_num_fat)
                        SetRecordIDX(xdWork2, xiRecord, "MAI_nominativo", dr_mai.MAI_nome)
                        SetRecordIDX(xdWork2, xiRecord, "MAI_indirizzo_recapito", dr_mai.MAI_rec_indirizzo)
                        SetRecordIDX(xdWork2, xiRecord, "MAI_citta_recapito", dr_mai.MAI_rec_localita)
                        SetRecordIDX(xdWork2, xiRecord, "MAI_cap_recapito", dr_mai.MAI_rec_cap)
                        SetRecordIDX(xdWork2, xiRecord, "MAI_print_bol", "si")
                        SetRecordIDX(xdWork2, xiRecord, "MAI_filedati", dr_mai.MAI_codice)
                        SetRecordIDX(xdWork2, xiRecord, "DEF_errcode", "0")
                        SetRecordIDX(xdWork2, xiRecord, "DEF_errdesc", "")
                        SetRecordIDX(xdWork2, xiRecord, "DEF_onlyarchott", dr_mai.MAI_email_add > "")
                        SetRecordIDX(xdWork2, xiRecord, "DEF_sendbymail", dr_mai.MAI_email_add.Contains("@"))

                        ' impostazione della modalit� di ordinamento.
                        ' in partenza si era preferito ordinare per numero fattura.
                        ' la richiesta di bonato � stata invece quella di ordinare per:
                        ' CAP - Via - NumeroCivico Pari o dispari - numero civico.

                        ' Inizializzo la variabile di ordinamento come indicato sopra
                        cSortValue = dr_mai.MAI_int_cap

                        cSortValue = String.Concat(cSortValue, dr_mai.MAI_int_indirizzo)
                        If dr_mai.MAI_rec_nome = "" Then
                            cSortValue = String.Concat(cSortValue, dr_mai.MAI_nome)
                        Else
                            cSortValue = String.Concat(cSortValue, dr_mai.MAI_rec_nome)
                        End If
                        SetRecordIDX(xdWork2, xiRecord, "SORTFIELD", cSortValue)
                        xdWork2.SelectSingleNode("FATTURA").AppendChild(xiRecord)
                        nRow += aRowFatt.Length
                    End While
                    SaveDataBaseFile()










                    'nSollecId = -1
                    'For Each dr_exc As DataRow In dsDati.Tables(0).Rows
                    '    If nSollecId <> dr_exc.Item("NUM_SOLLECITO") Then
                    '        xiRecord = xdWork2.CreateNode(Xml.XmlNodeType.Element, "FATTURA_ROW", "")
                    '        SetRecordIDX(xdWork2, xiRecord, "MAI_num_utente", dr_exc.Item("ID_SOGGETTO"))
                    '        SetRecordIDX(xdWork2, xiRecord, "MAI_num_sollecito", dr_exc.Item("NUM_SOLLECITO"))
                    '        SetRecordIDX(xdWork2, xiRecord, "MAI_nominativo", dr_exc.Item("RAGIONE_SOCIALE"))
                    '        SetRecordIDX(xdWork2, xiRecord, "MAI_indirizzo_recapito", String.Concat(dr_exc.Item("ELETOPO_DESSF"), " ", dr_exc.Item("IND_NUMSF"), " ", dr_exc.Item("IND_SUFSF")))
                    '        SetRecordIDX(xdWork2, xiRecord, "MAI_citta_recapito", dr_exc.Item("COM_DESSF"))
                    '        SetRecordIDX(xdWork2, xiRecord, "MAI_cap_recapito", dr_exc.Item("CAP_IDSF"))
                    '        SetRecordIDX(xdWork2, xiRecord, "MAI_provincia_recapito", dr_exc.Item("PRO_SIGLASF"))
                    '        'SetRecordIDX(xdWork2, xiRecord, "MAI_nazione_recapito", dr_exc.Item("DECODE_INDIDSPEFAT"))
                    '        SetRecordIDX(xdWork2, xiRecord, "MAI_print_bol", "si")
                    '        SetRecordIDX(xdWork2, xiRecord, "MAI_filedati", 1)
                    '        SetRecordIDX(xdWork2, xiRecord, "DEF_errcode", "0")
                    '        SetRecordIDX(xdWork2, xiRecord, "DEF_errdesc", "")

                    '        ' impostazione della modalit� di ordinamento.
                    '        ' in partenza si era preferito ordinare per numero fattura.
                    '        ' la richiesta di bonato � stata invece quella di ordinare per:
                    '        ' CAP - Via - NumeroCivico Pari o dispari - numero civico.

                    '        'cSortValue = xmlRecord.SelectSingleNode("MAI_num_fattura").InnerText

                    '        ' Inizializzo la variabile di ordinamento come indicato sopra
                    '        cSortValue = dr_exc.Item("COM_DESSF")

                    '        cSortValue = String.Concat(cSortValue, dr_exc.Item("ELETOPO_DESSF"))
                    '        cTmp = dr_exc.Item("IND_NUMSF")
                    '        If IsNumeric(cTmp) Then
                    '            If (cTmp Mod 2) = 0 Then
                    '                cSortValue = String.Concat(cSortValue, "P")
                    '            Else
                    '                cSortValue = String.Concat(cSortValue, "D")
                    '            End If
                    '            ' Inverte l'ordinamento per numero civico, dal pi� grande al pi� piccolo.
                    '            cTmp = 1000000000 - Integer.Parse(cTmp)
                    '        Else
                    '            cSortValue = String.Concat(cSortValue, "N")
                    '        End If
                    '        cSortValue = String.Concat(cSortValue, cTmp.PadLeft(10, "0"))
                    '        'cSortValue = String.Concat(cSortValue, GetValueFromXML(xiFattura, Costanti.itm_rec_sito_4).PadLeft(6, " "))
                    '        cSortValue = String.Concat(cSortValue, dr_exc.Item("RAGIONE_SOCIALE")).Replace(" ", "")
                    '        SetRecordIDX(xdWork2, xiRecord, "SORTFIELD", cSortValue)
                    '        xdWork2.SelectSingleNode("FATTURA").AppendChild(xiRecord)
                    '        nSollecId = dr_exc.Item("NUM_SOLLECITO")
                    '    End If
                    'Next
                    aValue(4) = 1
                Catch ex As Exception
                    aValue(4) = -1
                Finally
                    xdWork1 = Nothing
                End Try
            Else
                aValue(4) = 1
            End If
            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
        Next
        _MessaggiAvanzamento(5) = "Ordinamento dei record in corso..."
        SortFileIndice(xdWork2, 0, xdWork2.SelectSingleNode("FATTURA").ChildNodes.Count - 1)

        ' Rimuove il campo utilizzato per l'ordinamento
        nRecord = 1
        For Each xiWork1 In xdWork2.SelectNodes("FATTURA/FATTURA_ROW/SORTFIELD")
            xiWork2 = xiWork1.ParentNode
            xiWork2.RemoveChild(xiWork1)
            SetRecordIDX(xdWork2, xiWork2, "MAI_codice", nRecord)
            nRecord += 1
        Next

        ' Salva il file
        xdWork2.Save(String.Concat(_SessionePath, "\dati\indice.xml"))
        _ResultAL.Add(New String() {"filename", "dati\indice.xml", "0", "0", "1"})
        xdWork2 = Nothing

    End Sub

    Private Function CaricaFileDatiToDS(ByVal cFileName As String) As DataSet
        Dim sr As System.IO.StreamReader
        Dim dc As DataColumn
        Dim dsCSV As DataSet
        Dim cLine As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim i As Integer
        Dim cSplitChar As String


        '
        sr = New System.IO.StreamReader(cFileName)
        cLine = sr.ReadLine
        If cLine.Contains(vbTab) Then
            cSplitChar = Chr(9)
        ElseIf cLine.Contains(";") Then
            cSplitChar = ";"
        Else
            Throw New Exception("Carattere di separazione non noto")
        End If
        dsCSV = New DataSet()
        dt = New DataTable
        For Each cValue As String In cLine.Split(cSplitChar)
            dc = New DataColumn(cValue, System.Type.GetType("System.String"))
            dt.Columns.Add(dc)
        Next
        dsCSV.Tables.Add(dt)
        While Not sr.EndOfStream
            cLine = sr.ReadLine
            If cLine.Replace(Chr(9), " ").Trim > "" Then
                dr = dt.NewRow
                For i = 0 To dt.Columns.Count - 1
                    dr(i) = cLine.Split(cSplitChar)(i)
                Next
                dt.Rows.Add(dr)
            Else
                cLine = cLine.Replace(Chr(9), "")
            End If
        End While
        Return dsCSV

    End Function

    Private Sub ExtractData(ByVal aRowFatt As DataRow())
        Dim nRowAn As Integer
        Dim drAnal As DataRow
        Dim dr_mai As DS_trans.TBL_mainRow
        Dim dr_fat As DS_trans.TBL_fatturaRow
        Dim dr_det As DS_trans.TBL_det_fatRow
        Dim dr_msg As DS_trans.TBL_messaggiRow
        Dim nTotIva As Decimal
        Dim cTmp As String

        Try
            nRowAn = 0
            nTotIva = 0
            drAnal = aRowFatt(nRowAn)
            dr_mai = _data_trans.TBL_main.NewRow
            dr_mai.MAI_codice = _data_trans.TBL_main.Rows.Count + 1
            dr_mai.MAI_sog_id = GetValueFromDR(drAnal, "SOG_ID")
            dr_mai.MAI_nome = GetValueFromDR(drAnal, "NOME_SOGGETTO")
            dr_mai.MAI_pariva = ""
            dr_mai.MAI_codfis = ""
            dr_mai.MAI_int_cap = ""
            dr_mai.MAI_int_indirizzo = ""
            dr_mai.MAI_int_localita = ""
            dr_mai.MAI_rec_nome = ""
            _data_trans.TBL_main.Rows.Add(dr_mai)

            dr_fat = _data_trans.TBL_fattura.NewRow
            dr_fat.FAT_codice = _data_trans.TBL_fattura.Rows.Count + 1
            dr_fat.FAT_cod_mai = dr_mai.MAI_codice
            dr_fat.FAT_anno_fat = GetValueFromDR(drAnal, "ANNO_FAT")
            dr_fat.FAT_num_fat = GetValueFromDR(drAnal, "NUM_FAT")
            dr_fat.FAT_protocollo = GetValueFromDR(drAnal, "PROTOCOLLO")
            dr_fat.FAT_data_fat = GetValueFromDR(drAnal, "DT_FAT")
            dr_fat.FAT_scadenza = GetValueFromDR(drAnal, "DT_SCA")

            _data_trans.TBL_fattura.Rows.Add(dr_fat)
            For Each drAnal In aRowFatt
                Select Case drAnal("TIPO_RIGA")
                    Case Is = "D"
                        Dim cDt_Fine As String

                        dr_det = _data_trans.TBL_det_fat.NewRow
                        dr_det.DFA_codice = _data_trans.TBL_det_fat.Rows.Count + 1
                        dr_det.DFA_cod_mai = dr_mai.MAI_codice
                        dr_det.DFA_descr_voce = GetValueFromDR(drAnal, "DESCRIZIONE_VOCE")
                        dr_det.DFA_contratto = GetValueFromDR(drAnal, "CONTRATTO")
                        If Not dr_det.DFA_contratto = "00000" Then
                            dr_det.DFA_data_ini = GetValueFromDR(drAnal, "DT_INIZIO")
                            cDt_Fine = GetValueFromDR(drAnal, "DT_FINE")
                            If cDt_Fine <> "31/12/9999" Then
                                dr_det.DFA_data_fin = cDt_Fine
                            End If
                            dr_det.DFA_nome_defunto = GetValueFromDR(drAnal, "NOME_DEFUNTO")
                            dr_det.DFA_ubicazione = GetValueFromDR(drAnal, "UBICAZIONE")
                            If IsNumeric(GetValueFromDR(drAnal, "NUMERO_LAMPADE")) Then
                                dr_det.DFA_num_lamp = GetValueFromDR(drAnal, "NUMERO_LAMPADE")
                            End If

                            dr_det.DFA_tariffa = GetValueFromDR(drAnal, "TARIFFA")
                            If IsNumeric(GetValueFromDR(drAnal, "GIORNI_COMPETENZA")) Then
                                dr_det.DFA_gg_comp = GetValueFromDR(drAnal, "GIORNI_COMPETENZA")
                            End If
                        Else
                            dr_det.DFA_contratto = "00000"
                        End If
                        dr_det.DFA_iva = GetValueFromDR(drAnal, "IVA").Replace("%", "")
                        dr_det.DFA_imponibile = GetValueFromDR(drAnal, "IMPONIBILE")
                        dr_det.DFA_imp_iva = GetValueFromDR(drAnal, "IMP_IVA")
                        nTotIva += dr_det.DFA_imp_iva
                        _data_trans.TBL_det_fat.Rows.Add(dr_det)
                    Case Is = "T"
                        dr_mai.MAI_pariva = GetValueFromDR(drAnal, "PARIVA")
                        dr_mai.MAI_codfis = GetValueFromDR(drAnal, "CODFIS")
                        dr_mai.MAI_int_cap = GetValueFromDR(drAnal, "CAP").PadLeft(5, "0").Substring(0, 5)
                        dr_mai.MAI_int_indirizzo = GetValueFromDR(drAnal, "INDIRIZZO_SOGGETTO")
                        dr_mai.MAI_int_localita = GetValueFromDR(drAnal, "LOCALITA")
                        dr_mai.MAI_rec_nome = ""

                        dr_fat.FAT_tipo_pag = GetValueFromDR(drAnal, "TIPO_PAGAMENTO")
                        Dim nTmp As Decimal = GetValueFromDR(drAnal, "TOT_FAT", "0")
                        dr_fat.FAT_totale_fat = nTmp
                        dr_fat.FAT_ocr = GetValueFromDR(drAnal, "OCR").PadLeft(16, "0")

                        dr_det = _data_trans.TBL_det_fat.NewRow
                        dr_det.DFA_codice = _data_trans.TBL_det_fat.Rows.Count + 1
                        dr_det.DFA_cod_mai = dr_mai.MAI_codice
                        dr_det.DFA_contratto = "00000"
                        dr_det.DFA_descr_voce = "Totale IVA"
                        dr_det.DFA_imponibile = nTotIva
                        dr_det.DFA_iva = 0
                        dr_det.DFA_imp_iva = 0
                        _data_trans.TBL_det_fat.Rows.Add(dr_det)
                    Case Is = "V"
                        If GetValueFromDR(drAnal, "CONTRATTO") = "00000" Then
                            dr_msg = _data_trans.TBL_messaggi.NewRow
                            dr_msg.MSG_cod_mai = dr_mai.MAI_codice
                            dr_msg.MSG_descr_voce = GetValueFromDR(drAnal, "DESCRIZIONE_VOCE")
                            _data_trans.TBL_messaggi.Rows.Add(dr_msg)
                        End If
                    Case Is = "Z"
                        dr_mai.MAI_rec_nome = GetValueFromDR(drAnal, "NOME_SOGGETTO", "")
                        cTmp = GetValueFromDR(drAnal, "INDIRIZZO_SOGGETTO", "")

                        If cTmp.Contains("@") Then
                            dr_mai.MAI_email_add = cTmp
                            dr_mai.MAI_rec_cap = GetValueFromDR(drAnal, "CAP").PadLeft(5, "0").Substring(0, 5)
                            dr_mai.MAI_rec_indirizzo = dr_mai.MAI_int_indirizzo
                            dr_mai.MAI_rec_localita = GetValueFromDR(drAnal, "LOCALITA")
                        ElseIf cTmp > "" Then
                            dr_mai.MAI_email_add = ""
                            dr_mai.MAI_rec_cap = GetValueFromDR(drAnal, "CAP").PadLeft(5, "0").Substring(0, 5)
                            dr_mai.MAI_rec_indirizzo = GetValueFromDR(drAnal, "INDIRIZZO_SOGGETTO")
                            dr_mai.MAI_rec_localita = GetValueFromDR(drAnal, "LOCALITA")
                        End If
                        If dr_mai.MAI_rec_nome.Trim = "" Then
                            dr_mai.MAI_rec_nome = dr_mai.MAI_nome
                        End If
                End Select
            Next
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Sub ImportaFileInSessione(ByVal nTipoFile As Integer, ByVal cFileName As String)
        Dim nRecordtoAnalized As Integer
        Dim xdWork1 As Xml.XmlDocument
        Dim xdWork2 As Xml.XmlDocument
        Dim nRecordXFile As Integer
        Dim xiWork As Xml.XmlNode
        Dim cDstFile As String
        Dim nRecord As Integer
        Dim nFile As Integer
        Dim nDim As Integer

        ' Nel caso di tipo file 1 allora si tratta di un file xml, indipendentemente dall'estensione 
        ' da suddividere eventualmente in file di pi� piccole dimensioni.
        Select Case nTipoFile
            Case Is = 1
                Try
                    xdWork1 = New Xml.XmlDocument
                    xdWork1.Load(cFileName)
                    nDim = New System.IO.FileInfo(cFileName).Length
                    If (nDim < UpperFileSize) Then
                        _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
                        cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

                        System.IO.File.Copy(cFileName, cDstFile)
                        _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                        _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
                    Else
                        _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                        _MessaggiAvanzamento(5) = "Suddivizione del file nella cartella di archiviazione..."

                        nRecordtoAnalized = xdWork1.SelectNodes(Costanti.itm_Fattura).Count
                        nRecordXFile = Int(LowerFileSize / (nDim / nRecordtoAnalized)) + 1
                        _MessaggiAvanzamento(4) = CInt(_MessaggiAvanzamento(4)) + Int(nRecordtoAnalized / nRecordXFile) + 1
                        nRecord = 0
                        nFile = 1
                        For Each xiWork In xdWork1.SelectNodes(Costanti.itm_Fattura)
                            If nRecord = 0 Then
                                xdWork2 = New Xml.XmlDocument
                                xdWork2.AppendChild(xdWork2.CreateNode(Xml.XmlNodeType.Element, "EMISSIONE", ""))
                            End If
                            xdWork2.SelectSingleNode("EMISSIONE").AppendChild(xdWork2.ImportNode(xiWork, True))
                            nRecord += 1
                            nRecordtoAnalized -= 1
                            If (nRecord = nRecordXFile) Or (nRecordtoAnalized = 0) Then
                                _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                                cDstFile = cFileName.Substring(cFileName.LastIndexOf("\") + 1)
                                cDstFile = String.Concat(cDstFile.Substring(0, cDstFile.IndexOf(".")), "_", nFile.ToString.PadLeft(5, "0"), cDstFile.Substring(cDstFile.IndexOf(".")))
                                cDstFile = String.Concat(_SessioneDatiPath, cDstFile)
                                xdWork2.Save(cDstFile)
                                xdWork2 = Nothing
                                _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
                                nRecord = 0
                                nFile += 1
                            End If
                        Next
                        xdWork1 = Nothing
                    End If
                Catch ex As Exception
                    cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))
                    System.IO.File.Copy(cFileName, cDstFile)
                    _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, -1})
                End Try
            Case Is = 199
                _MessaggiAvanzamento(5) = "Copia del file nella cartella di temporanea..."
                Dim _WorkPath As String
                _WorkPath = _SessioneDatiPath.Replace("\dati\", "\work\")
                cDstFile = String.Concat(_WorkPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

                System.IO.File.Copy(cFileName, cDstFile)
                _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
            Case Else
                _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
                cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

                System.IO.File.Copy(cFileName, cDstFile)
                _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
        End Select

    End Sub

    Private Sub SaveDataBaseFile()
        Dim oComBuil As OleDb.OleDbCommandBuilder
        Dim oDa As OleDb.OleDbDataAdapter
        Dim oCmd As OleDb.OleDbCommand
        Dim cDBFileName As String
        Dim cConnACCStr As String

        cDBFileName = _SessioneDati.SelectSingleNode("filename[@tipo='103']").InnerText
        cDBFileName = String.Concat(_SessionePath, cDBFileName)
        cConnACCStr = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cDBFileName, ";")

        oCmd = New OleDb.OleDbCommand
        oCmd.Connection = New OleDb.OleDbConnection(cConnACCStr)

        ' Salvataggio della tabella TBL_main
        oCmd.CommandText = "SELECT * FROM TBL_main"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Update(_data_trans.TBL_main)

        ' Salvataggio della tabella TBL_fattura
        oCmd.CommandText = "SELECT * FROM TBL_fattura"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Update(_data_trans.TBL_fattura)

        ' Salvataggio della tabella TBL_fattura
        oCmd.CommandText = "SELECT * FROM TBL_det_fat"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Update(_data_trans.TBL_det_fat)

        ' Salvataggio della tabella TBL_fattura
        oCmd.CommandText = "SELECT * FROM TBL_messaggi"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Update(_data_trans.TBL_messaggi)
        oCmd.Connection.Close()

    End Sub

    Private Sub SortFileIndice(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer)
        Dim nPivot As Integer
        Dim k As Integer

        If nFst < nEnd Then
            nPivot = (nFst + nEnd) / 2
            k = dividi(xiWork, nFst, nEnd, nPivot)
            SortFileIndice(xiWork, nFst, k - 1)
            SortFileIndice(xiWork, k + 1, nEnd)
        End If

    End Sub

    Function dividi(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer, ByVal nPivot As Integer) As Integer
        Dim i As Integer = nFst
        Dim j As Integer = nEnd
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim nPivotPlace As Integer

        If nPivot < nEnd Then
            ' sposto il valore del pivot alla fine dell file xml
            swapItem(xiWork, nPivot, nEnd)
        End If

        xiWork_1 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nEnd).SelectSingleNode("SORTFIELD")
        nPivotPlace = nFst
        For i = nFst To nEnd - 1
            xiWork_2 = xiWork.SelectSingleNode("FATTURA").ChildNodes(i).SelectSingleNode("SORTFIELD")
            If (xiWork_2.InnerText <= xiWork_1.InnerText) Then
                If i <> nPivotPlace Then
                    swapItem(xiWork, nPivotPlace, i)
                End If
                nPivotPlace += 1
            End If
        Next
        If nPivotPlace < nEnd Then
            swapItem(xiWork, nPivotPlace, nEnd)
        End If
        Return nPivotPlace

    End Function

    Private Sub swapItem(ByVal xiWork As Xml.XmlDocument, ByVal nIdx1 As Integer, ByVal nIdx2 As Integer)
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim xiWork_3 As Xml.XmlNode

        xiWork_1 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx1)
        xiWork_2 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx2)
        xiWork.SelectSingleNode("FATTURA").RemoveChild(xiWork_1)
        xiWork.SelectSingleNode("FATTURA").RemoveChild(xiWork_2)
        xiWork_3 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx1 - 1)
        xiWork.SelectSingleNode("FATTURA").InsertAfter(xiWork_2, xiWork_3)
        xiWork_3 = xiWork.SelectSingleNode("FATTURA").ChildNodes(nIdx2 - 1)
        xiWork.SelectSingleNode("FATTURA").InsertAfter(xiWork_1, xiWork_3)

    End Sub

#End Region

    Private ReadOnly Property QuickDataSet_IndexFile() As String

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode("filename[@tipo=""0""]").InnerText)
        End Get

    End Property

    Public Sub LoadQuickDataset() Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.LoadQuickDataset
        Dim dr_mai_qd As DS_QD_ASM_vol.TBL_mainRow
        Dim lLoad As Boolean

        _QuickDataset = New DS_QD_ASM_vol
        _xmlDati_qd = New Xml.XmlDocument
        _xmlDati_qd.Load(QuickDataSet_IndexFile)
        ImpostaMessaggi(New String() {"", "", "", "0", _xmlDati_qd.SelectNodes(Costanti.itm_qd_Fattura).Count, "Caricamento dei dati...."})

        For Each xmlItem As Xml.XmlNode In _xmlDati_qd.SelectNodes(Costanti.itm_qd_Fattura)
            lLoad = _TipoCaricamento = 1 Or (_TipoCaricamento = 2 And CBool(GetValueFromXML(xmlItem, "DEF_sendbymail")))

            If lLoad Then

                dr_mai_qd = _QuickDataset.TBL_main.NewRow
                Try
                    dr_mai_qd.MAI_codice = GetValueFromXML(xmlItem, "MAI_codice")
                    dr_mai_qd.DEF_toprint = False
                    dr_mai_qd.DEF_errcode = GetValueFromXML(xmlItem, "DEF_errcode")
                    dr_mai_qd.DEF_errdesc = GetValueFromXML(xmlItem, "DEF_errdesc")
                    dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
                    dr_mai_qd.DEF_onlyarchott = CBool(GetValueFromXML(xmlItem, "DEF_onlyarchott"))
                    dr_mai_qd.DEF_sendbymail = CBool(GetValueFromXML(xmlItem, "DEF_sendbymail"))
                    dr_mai_qd.MAI_num_utente = GetValueFromXML(xmlItem, "MAI_num_utente")
                    dr_mai_qd.MAI_num_fattura = GetValueFromXML(xmlItem, "MAI_num_fattura")
                    dr_mai_qd.MAI_nominativo = GetValueFromXML(xmlItem, "MAI_nominativo")
                    dr_mai_qd.MAI_indirizzo_recapito = GetValueFromXML(xmlItem, "MAI_indirizzo_recapito")
                    dr_mai_qd.MAI_citta_recapito = GetValueFromXML(xmlItem, "MAI_citta_recapito")
                    dr_mai_qd.MAI_cap_recapito = GetValueFromXML(xmlItem, "MAI_cap_recapito")
                    dr_mai_qd.MAI_print_bol = GetValueFromXML(xmlItem, "MAI_print_bol").ToLower = "si"
                    dr_mai_qd.MAI_print_all = True 'GetValueFromXML(xmlItem, "MAI_print_all").ToLower = "si"
                    dr_mai_qd.MAI_filedati = GetValueFromXML(xmlItem, "MAI_filedati")
                Catch ex As Exception
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = ex.Message
                End Try
                ImpostaMessaggi(New String() {"", "", "", CInt(_MessaggiAvanzamento(3)) + 1, "", ""})
                _QuickDataset.TBL_main.Rows.Add(dr_mai_qd)
            End If
        Next
        LoadFileMdb()

    End Sub

#Region "Caricamento del PRINTDATASET"

    ' ************************************************************************** '
    ' Caricamento del DataSet da utilizzare per la stampa.                       '
    ' ************************************************************************** '
    Public Sub LoadFullDataset(ByVal nRecordToLoad As Integer) Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.LoadFullDataset
        Dim dr_mai_qd As DS_QD_ASM_vol.TBL_mainRow
        Dim nRecordToPrint As Integer

        ' ************************************************************************** '
        ' Creazione del Dataset e azzeramento del contatore dei record da stampare.  '
        ' ************************************************************************** '
        nRecordToPrint = 0
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                _PrintFatturaDataset = New DS_PF_ASM_vol
            Case Is = ListaDataset.DAS_bollettino
                _PrintBollettinoDataset = New DS_PB_ASM_vol
            Case Is = ListaDataset.DAS_send_email
                _SendEmailDataset = New DS_SE_asm_vol
        End Select

        ' ************************************************************************** '
        ' Caricamento del DataSet da utilizzare per la stampa.                       '
        ' ************************************************************************** '
        _CodiceProgressivo = 0
        'LoadFileMdb()  'CaricaFileDatiToDS(PrintDataSet_DataFile(1))
        While _RecordCount < _QuickDataset.TBL_main.Rows.Count And nRecordToPrint < nRecordToLoad
            dr_mai_qd = _QuickDataset.TBL_main.Rows(_RecordCount)
            If dr_mai_qd.DEF_toprint Then
                Try
                    _ErroriLievi = ""
                    AddRecordToPrint(dr_mai_qd, Nothing)
                    If _ErroriLievi > "" Then
                        dr_mai_qd.DEF_errcode = -2
                        dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori Lievi: ", _ErroriLievi)
                    End If
                Catch ex As Exception
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = ex.Message
                End Try
                nRecordToPrint += 1
            End If
            _RecordCount += 1
        End While

        ' ************************************************************************** '
        ' Consolidamento del DataSet da utilizzare per la stampa.                    '
        ' ************************************************************************** '
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                _PrintFatturaDataset.AcceptChanges()
            Case Is = ListaDataset.DAS_bollettino
                _PrintBollettinoDataset.AcceptChanges()
            Case Is = ListaDataset.DAS_send_email
                _SendEmailDataset.AcceptChanges()
        End Select

    End Sub

    Private Sub LoadFileMdb()
        Dim oComBuil As OleDb.OleDbCommandBuilder
        Dim oDa As OleDb.OleDbDataAdapter
        Dim oCmd As OleDb.OleDbCommand
        Dim cDBFileName As String
        Dim cConnACCStr As String

        _data_trans = New DS_trans
        cDBFileName = _SessioneDati.SelectSingleNode("filename[@tipo='103']").InnerText
        cDBFileName = String.Concat(_SessionePath, cDBFileName)
        cConnACCStr = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cDBFileName, ";")

        oCmd = New OleDb.OleDbCommand
        oCmd.Connection = New OleDb.OleDbConnection(cConnACCStr)

        ' Salvataggio della tabella TBL_main
        oCmd.CommandText = "SELECT * FROM TBL_main"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(_data_trans.TBL_main)

        ' Salvataggio della tabella TBL_fattura
        oCmd.CommandText = "SELECT * FROM TBL_fattura"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(_data_trans.TBL_fattura)

        ' Salvataggio della tabella TBL_det_fat
        oCmd.CommandText = "SELECT * FROM TBL_det_fat"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(_data_trans.TBL_det_fat)

        ' Salvataggio della tabella TBL_messaggi
        oCmd.CommandText = "SELECT * FROM TBL_messaggi"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(_data_trans.TBL_messaggi)
        oCmd.Connection.Close()

    End Sub

    Private Sub AddRecordToPrint(ByVal dr_mai_qd As DS_QD_ASM_vol.TBL_mainRow, ByVal dtExcel As DataTable)

        Try
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    GetRecordFatturaToPrint(dr_mai_qd.MAI_filedati, dr_mai_qd.MAI_codice, GetFeedString(dr_mai_qd))
                Case Is = ListaDataset.DAS_bollettino
                    GetRecordBollettiniToPrint(dr_mai_qd.MAI_filedati, dr_mai_qd.MAI_codice, GetFeedString(dr_mai_qd))
                Case Is = ListaDataset.DAS_send_email
                    GetRecordEmailToSend(dr_mai_qd.MAI_filedati, dr_mai_qd.MAI_codice, GetFeedString(dr_mai_qd))
            End Select
        Catch ex As Exception
            Dim cErrorString As String
            If ex.Message = "" Then
                cErrorString = "Errore caricamento dati, mentra si caricava il dato: SCONOSCIUTO"
            Else
                cErrorString = String.Concat("Errore caricamento dati, mentra si caricava il dato: ", ex.Message)
            End If
            Throw New Exception(cErrorString, ex)
        End Try

    End Sub

    Private Function GetFeedString(ByVal dr_mai_qd As DS_QD_ASM_vol.TBL_mainRow) As String
        Dim lAllModuli As Boolean = True
        Dim cRtn As String

        cRtn = ""
        If _FeedString > "" Then
            For i As Integer = 1 To _FeedString.Split(";").Length - 1 Step 2
                If dr_mai_qd.Item(_FeedString.Split(";")(i)) Then
                    cRtn = _FeedString.Split(";")(i + 1)
                Else
                    lAllModuli = False
                End If
            Next
            If lAllModuli Then
                cRtn = _FeedString.Split(";")(0)
            End If
        End If
        Return cRtn

    End Function

    Private ReadOnly Property PrintDataSet_DataFile(ByVal _FileIdx As Integer)

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode(String.Concat("filename[@codice=", "", _FileIdx, "", "]")).InnerText)
        End Get

    End Property

#End Region

#Region "Caricamento del PRINTDATSET della fattura"

    Private Sub GetRecordFatturaToPrint(ByVal nCodice As Integer, ByVal nLinkQD As Integer, ByVal cFeedString As String)
        Dim dr_mai_tra As DS_trans.TBL_mainRow

        dr_mai_tra = _data_trans.TBL_main.FindByMAI_codice(nCodice)
        If Not dr_mai_tra Is Nothing Then
            ' Carico i dati che occorrono cos� come sono memorizzati nel database di transito.
            _PrintFatturaDataset.TBL_main.ImportRow(dr_mai_tra)
            With CType(_PrintFatturaDataset.TBL_main.Rows(_PrintFatturaDataset.TBL_main.Rows.Count - 1), DS_PF_ASM_vol.TBL_mainRow)
                .MAI_linkQD = nLinkQD
                .DEF_alimimb = cFeedString
                .DEF_raccolta = 9
            End With

            For Each dr_fat_tra As DS_trans.TBL_fatturaRow In dr_mai_tra.GetChildRows("TBL_main_TBL_fattura")
                _PrintFatturaDataset.TBL_fattura.ImportRow(dr_fat_tra)
            Next

            'For Each dr_def_tra As DS_trans.TBL_det_fatRow In dr_mai_tra.GetChildRows("TBL_main_TBL_det_fat")
            For Each dr_def_tra As DS_trans.TBL_det_fatRow In _data_trans.TBL_det_fat.Select(String.Concat("DFA_cod_mai = ", nCodice))
                _PrintFatturaDataset.TBL_det_fat.ImportRow(dr_def_tra)
            Next
            For Each dr_msg_tra As DS_trans.TBL_messaggiRow In _data_trans.TBL_messaggi.Select(String.Concat("MSG_cod_mai = ", nCodice))
                _PrintFatturaDataset.TBL_messaggi.ImportRow(dr_msg_tra)
            Next
        End If






        'Dim dr_mai_ps As DS_PF_ASM_vol.TBL_mainRow
        'Dim dr_ras As DS_PF_ASM_vol.TBL_rate_sollRow
        'Dim dr_pop As DS_PF_ASM_vol.TBL_pod_pdrRow

        '' ************************************************************************** '
        '' Creazione del record guida della stampa della fattura.                     '
        '' ************************************************************************** '
        'dr_mai_ps = _PrintSollecitoDataset.TBL_main.NewRow
        '_CodiceProgressivo += 1
        'dr_mai_ps.MAI_codice = _CodiceProgressivo
        '_MasterRecord = _CodiceProgressivo
        'dr_mai_ps.MAI_linkQD = nCodice
        'dr_mai_ps.DEF_alimimb = cFeedString

        '' ************************************************************************** '
        '' Recuperiamo il numero dei pod per i quli si stamper� la fattura. Il campo  '
        '' di default DEF_raccolta indica se il record attuale fa parte di una raccol '
        '' ta. I valori che pu� assumere sono:                                        '
        '' ---- In caso di raccolta da pi� di un record:                              '
        ''      - 1 primo record di una raccolta;                                     '
        ''      - 2 record successivi della raccolta                                  '
        ''      - 3 ultimo record della raccolta.                                     '
        '' ---- In caso di raccolta da pi� di un record:                              '
        ''      - 9 raccolta costituita da un solo record                             '
        '' ************************************************************************** '
        'dr_mai_ps.DEF_raccolta = 9

        '' ************************************************************************** '
        '' Dati dell'intestatario della fattura.                                      ' 
        '' ************************************************************************** '
        'dr_mai_ps.MAI_cod_cli = aDr_exc(0).Item("ID_SOGGETTO")
        'dr_mai_ps.MAI_nome = aDr_exc(0).Item("RAGIONE_SOCIALE")
        ''_dr_mai_pf.MAI_cod_fis = GetValueFromXML(xiWork, Costanti.itm_cf)
        ''_dr_mai_pf.MAI_par_iva = GetValueFromXML(xiWork, Costanti.itm_piva)

        '' ************************************************************************** '
        '' Indirizzo dell'intestatario.                                               '
        '' ************************************************************************** '
        ''_dr_mai_pf.MAI_int_sito_1 = GetValueFromXML(xiWork, Costanti.itm_ind_SedeLeg_1)
        ''_dr_mai_pf.MAI_int_sito_2 = GetValueFromXML(xiWork, Costanti.itm_ind_SedeLeg_2)
        ''_dr_mai_pf.MAI_int_sito_3 = GetValueFromXML(xiWork, Costanti.itm_ind_SedeLeg_3)
        ''_dr_mai_pf.MAI_int_sito_4 = GetValueFromXML(xiWork, Costanti.itm_ind_SedeLeg_4)
        ''_dr_mai_pf.MAI_int_sito_5 = GetValueFromXML(xiWork, Costanti.itm_ind_SedeLeg_5)
        ''_dr_mai_pf.MAI_int_sito_6 = GetValueFromXML(xiWork, Costanti.itm_ind_SedeLeg_6)
        ''_dr_mai_pf.MAI_int_sito_7 = GetValueFromXML(xiWork, Costanti.itm_ind_SedeLeg_7)
        ''_dr_mai_pf.MAI_int_sito_8 = GetValueFromXML(xiWork, Costanti.itm_ind_SedeLeg_8)
        ''_dr_mai_pf.MAI_int_nazione = GetValueFromXML(xiWork, Costanti.itm_int_nazione)

        '' ************************************************************************** '
        '' Indirizzo del recapito della fattura.                                      '
        '' ************************************************************************** '
        'dr_mai_ps.MAI_rec_nome = dr_mai_ps.MAI_nome
        'If dr_mai_ps.MAI_rec_nome = dr_mai_ps.MAI_nome Then
        '    dr_mai_ps.MAI_rec_nome = ""
        'End If

        'dr_mai_ps.MAI_rec_sito_1 = aDr_exc(0).Item("ELETOPO_DESSF")
        'dr_mai_ps.MAI_rec_sito_2 = ""
        'dr_mai_ps.MAI_rec_sito_3 = aDr_exc(0).Item("IND_NUMSF")
        'dr_mai_ps.MAI_rec_sito_4 = GetValueFromDR(aDr_exc(0), "IND_SUFSF")
        'dr_mai_ps.MAI_rec_sito_5 = ""
        'dr_mai_ps.MAI_rec_sito_6 = aDr_exc(0).Item("CAP_IDSF")
        'dr_mai_ps.MAI_rec_sito_7 = aDr_exc(0).Item("COM_DESSF")
        'dr_mai_ps.MAI_rec_sito_8 = aDr_exc(0).Item("PRO_SIGLASF")
        'dr_mai_ps.MAI_rec_nazione = ""
        'dr_mai_ps.MAI_tot_sol = 0
        'dr_mai_ps.MAI_cod_tipo_ser = TipoServizio(aDr_exc(0)("SER_DES"))
        '_PrintSollecitoDataset.TBL_main.Rows.Add(dr_mai_ps)

        'If dr_mai_ps.MAI_cod_tipo_ser = 1 Then
        '    dr_mai_ps.MAI_tipo_sollecito = "POD"
        'End If
        'If dr_mai_ps.MAI_cod_tipo_ser = 2 Then
        '    dr_mai_ps.MAI_tipo_sollecito = "PDR"
        'End If
        'If dr_mai_ps.MAI_cod_tipo_ser = -1 Then
        '    Throw New Exception("Descrizione del servizio non associabile.")
        'End If

        'If aDr_exc.Length > 0 Then
        '    dr_pop = _PrintSollecitoDataset.TBL_pod_pdr.NewRow
        '    dr_pop.POP_cod_mai = dr_mai_ps.MAI_codice
        '    dr_pop.POP_pod_pdr = aDr_exc(0)("PDR_POD")
        '    dr_pop.POP_rec_for_1 = aDr_exc(0).Item("ELETOPO_DES")
        '    dr_pop.POP_rec_for_2 = ""
        '    dr_pop.POP_rec_for_3 = aDr_exc(0).Item("IND_NUM")
        '    dr_pop.POP_rec_for_4 = GetValueFromDR(aDr_exc(0), "IND_SUF")
        '    dr_pop.POP_rec_for_5 = ""
        '    dr_pop.POP_rec_for_6 = aDr_exc(0).Item("CAP_ID")
        '    dr_pop.POP_rec_for_7 = aDr_exc(0).Item("COM_DES")
        '    dr_pop.POP_rec_for_8 = aDr_exc(0).Item("PRO_SIGLA")
        '    _PrintSollecitoDataset.TBL_pod_pdr.Rows.Add(dr_pop)
        'End If


        'For Each dr As DataRow In aDr_exc
        '    dr_ras = _PrintSollecitoDataset.TBL_rate_soll.NewRow
        '    dr_ras.RAS_cod_mai = dr_mai_ps.MAI_codice
        '    If TipoServizio(dr("SER_DES")) = 1 Then
        '        dr_ras.RAS_num_doc = String.Concat(dr("FAT_ANNO"), "/UEV/", dr("FAT_NUM"))
        '    End If
        '    If TipoServizio(dr("SER_DES")) = 2 Then
        '        dr_ras.RAS_num_doc = String.Concat(dr("FAT_ANNO"), "/GN/", dr("FAT_NUM"))
        '    End If
        '    If TipoServizio(dr("SER_DES")) = -1 Then
        '        Throw New Exception("Descrizione del servizio non associabile.")
        '    End If
        '    dr_ras.RAS_data_emis = CDate(dr("FAT_DT").ToString.Replace(",", ""))
        '    dr_ras.RAS_data_scad = CDate(dr("RATA_DTSCA").ToString.Replace(",", ""))
        '    dr_ras.RAS_imp_totale = dr("RATA_IMP")
        '    dr_ras.RAS_imp_residuo = dr("RATA_IMP")
        '    dr_mai_ps.MAI_tot_sol += dr_ras.RAS_imp_residuo
        '    _PrintSollecitoDataset.TBL_rate_soll.Rows.Add(dr_ras)
        'Next

    End Sub

    Private Function TipoServizio(ByVal cDesServizio As String) As Integer
        Dim nTmp As Integer

        nTmp = -1
        If cDesServizio = "ELETTRICITA'" Then
            nTmp = 1
        End If
        If cDesServizio = "GAS" Then
            nTmp = 2
        End If
        Return nTmp

    End Function
    Private Sub CheckFieldValue(ByVal cValue As String, ByVal cMsgError As String)

        If cValue = "" And cMsgError > "" Then
            AddErroreLieve(cMsgError)
        End If

    End Sub

    Private Function ClonePF_mai_record() As DS_PF_ASM_vol.TBL_mainRow
        Dim dt_mai As DS_PF_ASM_vol.TBL_mainDataTable
        Dim dr_mai As DS_PF_ASM_vol.TBL_mainRow

        dt_mai = _PrintFatturaDataset.TBL_main.Clone
        dr_mai = _PrintFatturaDataset.TBL_main.FindByMAI_codice(_MasterRecord)
        dt_mai.ImportRow(dr_mai)
        Return dt_mai.Rows(0)

    End Function

#End Region

#Region "Caricamento del PRINTDATSET del bollettino."

    Private Sub GetRecordBollettiniToPrint(ByVal nCodice As Integer, ByVal nLinkQD As Integer, ByVal cFeedString As String)
        Dim dr_fxr As DS_PB_ASM_vol.TBL_fat_x_ratRow
        Dim dr_fat As DS_PB_ASM_vol.TBL_fattureRow
        Dim dr_fat_tra As DS_trans.TBL_fatturaRow
        Dim dr_rat As DS_PB_ASM_vol.TBL_rateRow
        Dim dr_mai_tra As DS_trans.TBL_mainRow

        dr_mai_tra = _data_trans.TBL_main.FindByMAI_codice(nCodice)
        If Not dr_mai_tra Is Nothing Then
            dr_fat_tra = CType(dr_mai_tra.GetChildRows("TBL_main_TBL_fattura")(0), DS_trans.TBL_fatturaRow)

            ' Carico per ciascuna fattura un record sulla tabella delle fatture e tanti record su quella dei bollettini.
            ' cos� posso far guidare la stampa dei bollettini da una tabella che originer� successivamente.
            dr_fat = _PrintBollettinoDataset.TBL_fatture.NewRow
            With dr_fat
                .FAT_codice = _PrintBollettinoDataset.TBL_fatture.Rows.Count + 1
                .FAT_nominativo = dr_mai_tra.MAI_nome
                .FAT_num_fattura = dr_fat_tra.FAT_num_fat
                .FAT_prot_reg_iva = dr_fat_tra.FAT_protocollo
                .FAT_data_fattura = dr_fat_tra.FAT_data_fat
                .FAT_ind_rec_1 = dr_mai_tra.MAI_rec_indirizzo
                .FAT_ind_rec_2 = ""
                .FAT_ind_rec_3 = ""
                .FAT_ind_rec_4 = ""
                .FAT_ind_rec_5 = ""
                .FAT_ind_rec_6 = dr_mai_tra.MAI_int_cap
                .FAT_ind_rec_7 = dr_mai_tra.MAI_int_localita
                .FAT_ind_rec_8 = ""
            End With
            _PrintBollettinoDataset.TBL_fatture.Rows.Add(dr_fat)

            dr_rat = _PrintBollettinoDataset.TBL_rate.NewRow
            With dr_rat
                .RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1
                .RAT_num_rata = dr_fat_tra.FAT_num_fat
                .RAT_scadenza = dr_fat_tra.FAT_scadenza
                .RAT_importo = dr_fat_tra.FAT_totale_fat
                .RAT_ocr = dr_fat_tra.FAT_ocr
                ' correzione del valore dell'OCR
                If .RAT_ocr.Length = 16 Then
                    Dim nTmp As Integer
                    nTmp = .RAT_ocr
                    nTmp = nTmp - Int(nTmp / 93) * 93
                    .RAT_ocr = String.Concat(.RAT_ocr, nTmp.ToString.PadLeft(2, "0"))
                End If
            End With
            _PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat)

            ' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
            dr_fxr = _PrintBollettinoDataset.TBL_fat_x_rat.NewRow
            dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1
            dr_fxr.FXR_linkQD = nLinkQD
            dr_fxr.FXR_cod_fat = dr_fat.FAT_codice
            dr_fxr.FXR_cod_rat = dr_rat.RAT_codice

            ' Mi serve per controllare il codice dell'imbustatrice.
            'If i = xmlRate.Count - 1 Then
            dr_fxr.FXR_ultimo = 1
            ' Else
            '     dr_fxr.FXR_ultimo = 0
            ' End If
            dr_fxr.DEF_alimimb = ""
            dr_fxr.DEF_raccolta = 9
            _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr)
        End If

    End Sub

    Private Sub GetRecordEmailToSend(ByVal nCodice As Integer, ByVal nLinkQD As Integer, ByVal cFeedString As String)
        Dim dr_fxr As DS_PB_ASM_vol.TBL_fat_x_ratRow
        Dim dr_fat As DS_PB_ASM_vol.TBL_fattureRow
        Dim dr_fat_tra As DS_trans.TBL_fatturaRow
        Dim dr_rat As DS_PB_ASM_vol.TBL_rateRow
        Dim dr_mai_tra As DS_trans.TBL_mainRow
        Dim dr_doc As DS_SE_asm_vol.TBL_documentiRow
        Dim dr_mai As DS_SE_asm_vol.TBL_mainRow
        Dim xdViaMail As Xml.XmlDocument
        Dim xnDoc As Xml.XmlNode
        Dim cDFA_flag_tipo_pag As String
        Dim cDFA_desc_tipo_pag As String

        dr_mai_tra = _data_trans.TBL_main.FindByMAI_codice(nCodice)
        If Not dr_mai_tra Is Nothing Then
            dr_fat_tra = CType(dr_mai_tra.GetChildRows("TBL_main_TBL_fattura")(0), DS_trans.TBL_fatturaRow)

            dr_mai = _SendEmailDataset.TBL_main.NewRow
            _CodiceProgressivo += 1
            dr_mai.MAI_codice = _CodiceProgressivo
            dr_mai.MAI_linkQD = nLinkQD
            dr_mai.MAI_nome = dr_mai_tra.MAI_nome
            dr_mai.MAI_tot_fattura = dr_fat_tra.FAT_totale_fat
            dr_mai.MAI_data_scadenza = dr_fat_tra.FAT_scadenza
            dr_mai.MAI_registro_iva = dr_fat_tra.FAT_protocollo
            dr_mai.MAI_anno = "2012"
            dr_mai.MAI_numero = dr_fat_tra.FAT_num_fat

            dr_mai.MAI_per_rifer = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "14")
            dr_mai.MAI_multipod = False

            'dr_mai.MAI_anno_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Contratto", "AnnoContratto")
            'dr_mai.MAI_num_contratto = RecuperaValoreDS_UE(ds_src, dr_fat, 0, "Contratto", "NumeroContratto")
            dr_mai.MAI_ub_sito_1 = dr_mai_tra.MAI_int_indirizzo
            dr_mai.MAI_ub_sito_2 = ""
            dr_mai.MAI_ub_sito_3 = ""
            dr_mai.MAI_ub_sito_4 = ""
            dr_mai.MAI_ub_sito_5 = ""
            dr_mai.MAI_ub_sito_6 = ""
            dr_mai.MAI_ub_sito_7 = ""
            dr_mai.MAI_ub_sito_8 = ""

            dr_mai.DEF_lista_email = dr_mai_tra.MAI_email_add
            _SendEmailDataset.TBL_main.Rows.Add(dr_mai)

            xdViaMail = New Xml.XmlDocument
            xdViaMail.Load(String.Concat(_SessionePath, "dati\SendByMail.xml"))
            xnDoc = xdViaMail.SelectSingleNode(String.Concat("SendByMail/SendByMailItem/MAI_codice[.='", nLinkQD, "']")).ParentNode

            For Each xnTmp As Xml.XmlNode In xnDoc.SelectSingleNode("Documenti").ChildNodes
                dr_doc = _SendEmailDataset.TBL_documenti.NewRow
                dr_doc.DOC_cod_mai = dr_mai.MAI_codice
                dr_doc.DOC_filename = xnTmp.SelectSingleNode("DOC_filename").InnerText
                _SendEmailDataset.TBL_documenti.Rows.Add(dr_doc)
            Next

        End If

    End Sub

#End Region

    Private Function GetValueFromXML(ByVal xmlItem As Xml.XmlNode, ByVal xmlPath As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cDefault
            If Not xmlItem.SelectSingleNode(xmlPath) Is Nothing Then
                cValue = xmlItem.SelectSingleNode(xmlPath).InnerText
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori del file XML.", vbLf, "percorso da cui si tenta il recupero:", vbLf, xmlPath), ex)
        End Try
        Return cValue

    End Function

    Private Function GetValueFromDR(ByVal dr As DataRow, ByVal cField As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cDefault
            If Not IsDBNull(dr.Item(cField)) Then
                cValue = dr.Item(cField)
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori dal record.", vbLf, "percorso da cui si tenta il recupero:", vbLf, cField), ex)
        End Try
        Return cValue

    End Function

    Private Sub AddErroreLieve(ByVal cMsg As String)

        If _ErroriLievi = "" Then
            _ErroriLievi = cMsg
        Else
            _ErroriLievi = String.Concat(_ErroriLievi, "; ", cMsg)
        End If

    End Sub

    Private Sub ImpostaMessaggi(ByVal aValue As String())
        Dim i As Integer

        For i = 0 To 5
            If aValue(i) > "" Then
                _MessaggiAvanzamento(i) = aValue(i)
            End If
        Next

    End Sub

    Private Function GetTrasformazioni(ByVal cTable As String, ByVal cCodice As String, ByVal nTipo As Integer) As String
        Dim xdTrasf As Xml.XmlDocument
        Dim xiWork As Xml.XmlNode
        Dim cRtn As String

        Try
            xdTrasf = New Xml.XmlDocument
            xdTrasf.Load(_SessioneTrasformazioniFileName)
            xiWork = xdTrasf.SelectSingleNode(String.Concat("translation/", cTable, "/item[codice = '", cCodice, "']"))
            cRtn = ""
            If Not xiWork Is Nothing Then
                If nTipo = 1 Then
                    cRtn = xiWork.SelectSingleNode("descr_breve").InnerText
                End If
                If nTipo = 2 Then
                    cRtn = xiWork.SelectSingleNode("descrizione").InnerText
                End If
            End If
            xdTrasf = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    ' Nessuna azione al momento necessaria.
    Public Sub PostAnteprima(ByVal aLista As ArrayList) Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.PostAnteprima

    End Sub

    Public Function PostStampa() As ArrayList Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.PostStampa

        Return New ArrayList

    End Function

    WriteOnly Property ListaGiri() As ArrayList Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.ListaGiri

        Set(ByVal value As ArrayList)
            _ListaGiri = value
        End Set

    End Property

    WriteOnly Property DBGiriConnectionString() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.DBGiriConnectionString

        Set(ByVal value As String)
            _DbGiriConnString = value
        End Set

    End Property

    ReadOnly Property NuoviToponimi() As Boolean Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.NuoviToponimi

        Get
            Return _NuoviToponimi
        End Get

    End Property

    ReadOnly Property CapacitaInvioMail() As Boolean Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.CapacitaInvioMail

        Get
            Return True
        End Get

    End Property

    WriteOnly Property SessioneNuova() As Boolean Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.SessioneNuova

        Set(ByVal value As Boolean)
            _SessioneNuova = value
        End Set

    End Property

    WriteOnly Property TipoCaricamento() As Integer Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.TipoCaricamento

        Set(ByVal value As Integer)
            _TipoCaricamento = value
        End Set

    End Property

    Public ReadOnly Property FullDataset() As DataSet Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.FullDataset

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return _PrintFatturaDataset
                Case Is = ListaDataset.DAS_bollettino
                    Return _PrintBollettinoDataset
                Case Is = ListaDataset.DAS_send_email
                    Return _SendEmailDataset
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataSet_MainTableName() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.FullDataSet_MainTableName

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "TBL_main"
                Case Is = ListaDataset.DAS_bollettino
                    Return "TBL_fat_x_rat"
                Case Is = ListaDataset.DAS_send_email
                    Return "TBL_main"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.FullDataset_MainTableKey

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "TBL_main"
                Case Is = ListaDataset.DAS_bollettino
                    Return "TBL_fat_x_rat"
                Case Is = ListaDataset.DAS_send_email
                    Return "MAI_codice"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    ReadOnly Property LinkField_QuickDS_FullDS() As String Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.LinkField_QuickDS_FullDS

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "MAI_linkQD"
                Case Is = ListaDataset.DAS_bollettino
                    Return "FXR_linkQD"
                Case Is = ListaDataset.DAS_send_email
                    Return "MAI_linkQD"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    Public Sub UpdateQuickDataset() Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.UpdateQuickDatasetFile
        Dim dr_prd As DS_PF_ASM_vol.TBL_mainRow
        Dim xnTmp As Xml.XmlNode
        Dim nLinkCode As Integer

        If _CodiceModulo = ListaDataset.DAS_fattura Then
            _xmlDati_qd = New Xml.XmlDocument
            _xmlDati_qd.Load(QuickDataSet_IndexFile)
            For Each dr_prd In _PrintFatturaDataset.TBL_main
                nLinkCode = dr_prd(LinkField_QuickDS_FullDS)
                xnTmp = _xmlDati_qd.SelectSingleNode(String.Concat(Costanti.itm_qd_Fattura, "/MAI_codice[.=", _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).MAI_codice, "]")).ParentNode
                xnTmp.SelectSingleNode("DEF_errcode").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode
                xnTmp.SelectSingleNode("DEF_errdesc").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errdesc
            Next
            _xmlDati_qd.Save(QuickDataSet_IndexFile)
        End If
    End Sub

    ReadOnly Property DataSendByMail() As Xml.XmlNode Implements PLUGIN_interfaceV1_4_2.IPLG_dati_lib.DataSendByMail

        Get
            Return GetDataSendByMail()
        End Get

    End Property

    Private Function GetDataSendByMail() As Xml.XmlNode
        Dim xnWork As Xml.XmlNode
        Dim xdWork As Xml.XmlDocument

        xdWork = New Xml.XmlDocument
        xnWork = xdWork.CreateNode(Xml.XmlNodeType.Element, "SendByMail", "")
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                With CType(_PrintFatturaDataset.TBL_main.Rows(0), DS_PF_ASM_vol.TBL_mainRow)
                    CreateXmlNode(xdWork, xnWork, "SND_EML_codice", .MAI_linkQD)
                    CreateXmlNode(xdWork, xnWork, "SND_EML_cod_utente", .MAI_sog_id)
                    CreateXmlNode(xdWork, xnWork, "SND_EML_nominativo", .MAI_nome)
                    CreateXmlNode(xdWork, xnWork, "SND_EML_mail_address", "lproietti@gmail.com")
                    'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_codice", .MAI_linkQD.ToString))
                    'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_cod_utente", .MAI_cod_cli.ToString))
                    'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_nominativo", .MAI_nome.ToString))
                    'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_mail_address", "lproietti@gmail.com"))
                End With
        End Select
        Return xnWork

    End Function

    Private Sub CreateXmlNode(ByVal xdDoc As Xml.XmlDocument, ByVal xnNode As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xdDoc.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xnNode.AppendChild(xiTmp)

    End Sub

End Class
