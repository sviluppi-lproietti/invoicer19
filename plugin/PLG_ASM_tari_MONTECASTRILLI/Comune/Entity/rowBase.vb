﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Public MustInherit Class rowBase

    Protected Property RawLine As String

    Protected ReadOnly Property aValue As String()

        Get
            Return RawLine.Split(";")
        End Get

    End Property

    Public Sub New(ByVal cLinea As String)

        RawLine = cLinea

    End Sub

End Class
