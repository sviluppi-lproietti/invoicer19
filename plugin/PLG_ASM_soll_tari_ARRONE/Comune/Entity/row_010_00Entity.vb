﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Public Class row_010_00Entity
    Inherits rowBase

    Public Sub New(ByVal cLinea As String)
        MyBase.New(cLinea)

    End Sub

    Public ReadOnly Property codice_fattura As String

        Get
            Return aValue(2)
        End Get

    End Property

    Public ReadOnly Property codice_ocr As String

        Get
            Return aValue(3)
        End Get

    End Property

    Public ReadOnly Property campo_simbolo As String

        Get
            Return aValue(4)
        End Get

    End Property

    Public ReadOnly Property importo_rata As Decimal

        Get
            Return aValue(5)
        End Get

    End Property

    Public ReadOnly Property data_scadenza As String

        Get
            Return aValue(6)
        End Get

    End Property

    Public ReadOnly Property DESCRIZIONE_RATA As String

        Get
            Return aValue(7)
        End Get

    End Property

    Public ReadOnly Property CODICE_COMUNE As String

        Get
            Return aValue(8)
        End Get

    End Property

    Public ReadOnly Property CODICE_TRIBUTO As String

        Get
            Return aValue(9)
        End Get

    End Property

    Public ReadOnly Property ACCONTO_SALDO As String

        Get
            Return aValue(10)
        End Get

    End Property

    Public ReadOnly Property N_IMMOBILI As String

        Get
            Return aValue(11)
        End Get

    End Property

    Public ReadOnly Property RATEIZZAZIONE As String

        Get
            Return aValue(12)
        End Get

    End Property

    Public ReadOnly Property ANNO As String

        Get
            Return aValue(13)
        End Get

    End Property

    Public ReadOnly Property CODICE_SEZIONE As String

        Get
            Return aValue(14)
        End Get

    End Property

    Public ReadOnly Property CIN_IMPORTO As String

        Get
            Return aValue(15)
        End Get

    End Property

    Public ReadOnly Property CIN_INTERMEDIO As String

        Get
            Return aValue(16)
        End Get

    End Property

    Public ReadOnly Property CIN_COMPLESSIVO As String

        Get
            Return aValue(17)
        End Get

    End Property

    Public ReadOnly Property CODIFICA_BOLLETTINO As String

        Get
            Return aValue(18)
        End Get

    End Property

    Public ReadOnly Property NUMERO_RATA As String

        Get
            Return aValue(19)
        End Get

    End Property

    Public ReadOnly Property ANNO_sollecito As String

        Get
            Return aValue(21)
        End Get

    End Property

End Class