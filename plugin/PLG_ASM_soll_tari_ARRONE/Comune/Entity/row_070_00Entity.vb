﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Public Class row_070_00Entity
    Inherits rowBase

    Public Sub New(ByVal cLinea As String)
        MyBase.New(cLinea)

    End Sub

    Public ReadOnly Property CODICE_FATTURA As String

        Get
            Return aValue(2)
        End Get

    End Property

    Public ReadOnly Property NUM_FATTURA As String

        Get
            Return aValue(3)
        End Get

    End Property

    Public ReadOnly Property DATA_FATTURA As String

        Get
            Return aValue(4)
        End Get

    End Property

    Public ReadOnly Property DATA_SCADENZA As String

        Get
            Return aValue(5)
        End Get

    End Property

    Public ReadOnly Property SIMBOLO_IMPORTO_FATT As String

        Get
            Return aValue(6)
        End Get

    End Property

    Public ReadOnly Property IMPORTO_FATTURA As Decimal

        Get
            Return Decimal.Parse(aValue(7))
        End Get

    End Property

    Public ReadOnly Property SIMBOLO_IMPORTO_DOVUTO As String

        Get
            Return aValue(8)
        End Get

    End Property

    Public ReadOnly Property IMPORTO_DOVUTO As Decimal

        Get
            Return Decimal.Parse(aValue(9))
        End Get

    End Property

    Public ReadOnly Property SIMBOLO_IMPORTO_SCAD As String

        Get
            Return aValue(10)
        End Get

    End Property

    Public ReadOnly Property IMPORTO_SCAD As Decimal

        Get
            Return Decimal.Parse(aValue(11))
        End Get

    End Property

    Public ReadOnly Property NR_RATA As String

        Get
            Return aValue(12)
        End Get

    End Property

    Public ReadOnly Property ANNO_RIFERIMENTO As String

        Get
            Return aValue(13)
        End Get

    End Property

    Public ReadOnly Property SIMBOLO_TOTALE_IMPORTO As String

        Get
            Return aValue(14)
        End Get

    End Property

    Public ReadOnly Property TOTALE_IMPORTO As Decimal

        Get
            Return Decimal.Parse(aValue(15))
        End Get

    End Property

    Public ReadOnly Property SIMBOLO_TOTALE_DOVUTO As String

        Get
            Return aValue(16)
        End Get

    End Property

    Public ReadOnly Property TOTALE_DOVUTO As Decimal

        Get
            Return Decimal.Parse(aValue(17))
        End Get

    End Property

End Class

