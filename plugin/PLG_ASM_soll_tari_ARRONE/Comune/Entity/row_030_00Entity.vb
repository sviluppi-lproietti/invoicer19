﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Public Class row_030_00Entity
    Inherits rowBase

    Public Sub New(ByVal cLinea As String)
        MyBase.New(cLinea)

    End Sub

    Public ReadOnly Property CODICE_FATTURA As String

        Get
            Return aValue(2)
        End Get

    End Property

    Public ReadOnly Property UTENZA As String

        Get
            Return aValue(3)
        End Get

    End Property

    Public ReadOnly Property UBICAZIONE As String

        Get
            Return aValue(4)
        End Get

    End Property

    Public ReadOnly Property PERIODO_DAL As String

        Get
            Return aValue(5)
        End Get

    End Property

    Public ReadOnly Property PERIODO_AL As String

        Get
            Return aValue(6)
        End Get

    End Property
    Public ReadOnly Property GIORNI_APPLICAZIONE As String

        Get
            Return aValue(7)
        End Get

    End Property
    Public ReadOnly Property TIPO As String

        Get
            Return aValue(8)
        End Get

    End Property

    Public ReadOnly Property CATEGORIA_DESCR_BREVE As String

        Get
            Return aValue(9)
        End Get

    End Property

    Public ReadOnly Property CATEGORIA_DESCR_LUNGA As String

        Get
            Dim cTmp As String

            cTmp = aValue(10).ToLower.Replace("componenti", "").Trim
            If (cTmp = "") Then
                cTmp = -1
            End If

            Return cTmp
        End Get

    End Property

    Public ReadOnly Property CAMPO_SIMBOLO As String

        Get
            Return aValue(11)
        End Get

    End Property

    Public ReadOnly Property TOTALE_UTENZA As String

        Get
            Return aValue(12)
        End Get

    End Property

    Public ReadOnly Property FLAG_DATI_CATASTALI As String

        Get
            Return aValue(13)
        End Get

    End Property

    Public ReadOnly Property SUPERFICIE As String

        Get
            Return aValue(14)
        End Get

    End Property

    Public ReadOnly Property LISTINO_Parte_Fissa As String

        Get
            Return aValue(15)
        End Get

    End Property

    Public ReadOnly Property LISTINO_Parte_Variabile As String

        Get
            Return aValue(16)
        End Get

    End Property

    Public ReadOnly Property PERCENTUALE_APPLICAZIONE As String

        Get
            Return aValue(17)
        End Get

    End Property

End Class