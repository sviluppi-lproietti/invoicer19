﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Collections
Imports System.linq

Public Class UtenzaEntity

    Public Sub New()

        ElencoRate = New ArrayList
        ElencoContratti = New ArrayList
        DatiCatastali = New Dictionary(Of String, row_030_35Entity)

    End Sub

    Public Property Intestazionefattura As row_000_00Entity
    Public Property ElencoRate As ArrayList
    Public Property ElencoContratti As ArrayList
    Public Property DatiCatastali As Dictionary(Of String, row_030_35Entity)
    Public ReadOnly Property PrintF24 As Boolean

        Get
            Return Me.ElencoRate.Count > 0
        End Get

    End Property

    Public ReadOnly Property IsNotaCredito As Boolean
        Get
            Return Intestazionefattura.flag_tipo_documento.ToLower = "c"
        End Get
    End Property

    Public Property TotaleUtenze As row_040_00Entity
    Public Property NoteFattura As row_025_00Entity
    Public Property TributoProvinciale As row_050_10Entity
    Public Property Arrotondamento As row_050_25Entity
    Public Property TotaleDocumento As row_050_30Entity
    Public Property TotaleIncassato As row_050_40Entity
    Public Property TotaleDaPagare As row_050_50Entity

    Public ReadOnly Property SendByMail As Boolean

        Get
            Return Intestazionefattura.email.Contains("@")
        End Get

    End Property

    Public Sub ConsolidaContratti()
        Dim aContratti As Dictionary(Of String, ContrattoEntity)
        Dim aDuplicati As List(Of ContrattoEntity)
        Dim lDuplicato As Boolean
        Dim contr As ContrattoEntity
        Dim lInsert As Boolean
        Dim i As Integer

        '
        ' Se c'è più di un contratto in archivio allora procedo con il consolidamento.
        '
        'If (Me.ElencoContratti.Count > 1) Then
        '    aContratti = New Dictionary(Of String, ContrattoEntity)
        '    aDuplicati = New List(Of ContrattoEntity)
        '    lDuplicato = False
        '    For Each con As ContrattoEntity In Me.ElencoContratti
        '        lInsert = True
        '        For Each k As KeyValuePair(Of String, ContrattoEntity) In aContratti
        '            lInsert = lInsert And (k.Value.DatiFornitura.UTENZA <> con.DatiFornitura.UTENZA)
        '        Next
        '        If lInsert Then
        '            aContratti.Add(con.DatiFornitura.UTENZA, con)
        '        Else
        '            aDuplicati.Add(con)
        '            lDuplicato = True
        '        End If
        '    Next
        '    If lDuplicato Then
        '        For Each c As ContrattoEntity In aDuplicati
        '            contr = aContratti(c.DatiFornitura.UTENZA)
        '            For Each d As row_030_10Entity In c.DettaglioUtenza
        '                contr.DettaglioUtenza.Add(d)
        '            Next
        '            contr.DatiCatastali = c.DatiCatastali
        '            i = 0
        '            While i < Me.ElencoContratti.Count
        '                If (CType(Me.ElencoContratti(i), ContrattoEntity).ProgressivoContratto = c.ProgressivoContratto) Then
        '                    Me.ElencoContratti.RemoveAt(i)
        '                    i -= 1
        '                End If
        '                i += 1
        '            End While
        '        Next

        '    End If
        'End If

        '
        ' Associo i dati catastali ai contratti rimasti
        '
        For Each con As ContrattoEntity In Me.ElencoContratti
            If (Me.DatiCatastali.ContainsKey(con.DatiFornitura.UTENZA)) Then
                con.DatiCatastali = Me.DatiCatastali(con.DatiFornitura.UTENZA)
            End If
        Next

    End Sub


End Class
