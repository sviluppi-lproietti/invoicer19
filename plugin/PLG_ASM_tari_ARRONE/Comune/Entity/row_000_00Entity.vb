﻿Imports System
Imports System.Collections.Generic
Imports System.Text

Public Class row_000_00Entity
    Inherits rowBase

    Public Sub New(ByVal cLinea As String)
        MyBase.New(cLinea)

    End Sub

    Public ReadOnly Property codice_fattura As String

        Get
            Return aValue(2)
        End Get

    End Property

    Public ReadOnly Property numero_fattura As String
        Get
            Return aValue(3)
        End Get

    End Property

    Public ReadOnly Property data_fattura As String
        Get
            Return aValue(4)
        End Get

    End Property

    Public ReadOnly Property utente_id As String
        Get
            Return aValue(5)
        End Get

    End Property

    Public ReadOnly Property utente As String
        Get
            Return aValue(6)
        End Get

    End Property

    Public ReadOnly Property indirizzoResidenza As String
        Get
            Return aValue(7)
        End Get

    End Property

    Public ReadOnly Property civicoResidenza As String
        Get
            Return aValue(8)
        End Get

    End Property

    Public ReadOnly Property capResidenza As String
        Get
            Return aValue(9).PadLeft(5, "0")
        End Get

    End Property

    Public ReadOnly Property comuneResidenza As String
        Get
            Return aValue(10)
        End Get

    End Property

    Public ReadOnly Property provinciaResidenza As String
        Get
            Return aValue(11).Replace("(", "").Replace(")", "")
        End Get

    End Property

    Public ReadOnly Property codice_fiscale As String
        Get
            Return aValue(12)
        End Get

    End Property

    Public ReadOnly Property partita_iva As String
        Get
            Return aValue(13)
        End Get

    End Property

    Public ReadOnly Property indirizzoRecapito As String
        Get
            Return aValue(14)
        End Get

    End Property

    Public ReadOnly Property civicoRecapito As String
        Get
            Return aValue(15)
        End Get

    End Property

    Public ReadOnly Property capRecapito As String
        Get
            Return aValue(16).PadLeft(5, "0")
        End Get

    End Property

    Public ReadOnly Property comuneRecapito As String
        Get
            Return aValue(17)
        End Get

    End Property

    Public ReadOnly Property provinciaRecapito As String
        Get
            Return aValue(18).Replace("(", "").Replace(")", "")
        End Get

    End Property

    Public ReadOnly Property SESSO As String
        Get
            Return aValue(26)
        End Get

    End Property
    Public ReadOnly Property DATA_DI_NASCITA As String
        Get
            Return aValue(27)
        End Get

    End Property

    Public ReadOnly Property COMUNE_STATO_DI_NASCITA As String
        Get
            Return aValue(28)
        End Get

    End Property
    Public ReadOnly Property PROVINCIA_DI_NASCITA As String
        Get
            Return aValue(29)
        End Get

    End Property

    Public ReadOnly Property codice_comune As String
        Get
            Return aValue(30)
        End Get

    End Property

    Public ReadOnly Property COGNOME_RAGSOC As String
        Get
            Return aValue(31)
        End Get

    End Property

    Public ReadOnly Property NOME As String
        Get
            Return aValue(32)
        End Get

    End Property

    Public ReadOnly Property indirizzo_presso As String
        Get
            Return aValue(34)
        End Get

    End Property

    Public ReadOnly Property flag_tipo_documento As String
        Get
            Return aValue(35)
        End Get

    End Property

    Public ReadOnly Property anno_fattura As String

        Get
            Return "20" + numero_fattura.Split("-")(2)
        End Get

    End Property

    Public ReadOnly Property progr_fattura As String
        Get
            Return numero_fattura.Split("-")(1)
        End Get
    End Property

    Public ReadOnly Property email As String

        Get
            Return aValue(39)
        End Get

    End Property

End Class


