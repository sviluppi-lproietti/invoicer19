﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Collections

Public Class LoadFileDati

    Public Property ListaUtenze As Dictionary(Of String, UtenzaEntity)
    Private Property _FileName As String

    Public Sub New(ByVal cFileName As String)

        _FileName = cFileName
        ListaUtenze = New Dictionary(Of String, UtenzaEntity)

    End Sub

    Public Sub LoadFile()
        Dim sr As System.IO.StreamReader
        Dim co As ContrattoEntity
        Dim ut As UtenzaEntity
        Dim cLinea As String
        Dim aValue As String()

        sr = New System.IO.StreamReader(_FileName, Encoding.GetEncoding("iso-8859-1"))
        While (Not sr.EndOfStream)
            cLinea = sr.ReadLine
            If cLinea > "" Then
                aValue = cLinea.Split(";")
                If aValue(0) = "000" And aValue(1) = "00" Then
                    ut = New UtenzaEntity()
                    ut.Intestazionefattura = New row_000_00Entity(cLinea)
                    ListaUtenze.Add(ut.Intestazionefattura.numero_fattura, ut)
                ElseIf aValue(0) = "010" And aValue(1) = "00" Then
                    ut.ElencoRate.Add(New row_010_00Entity(cLinea))
                ElseIf aValue(0) = "025" And aValue(1) = "00" Then
                    ut.NoteFattura = New row_025_00Entity(cLinea)
                ElseIf aValue(0) = "030" And aValue(1) = "00" Then
                    co = New ContrattoEntity()
                    co.ProgressivoContratto = ut.ElencoContratti.Count + 1
                    ut.ElencoContratti.Add(co)
                    co.DatiFornitura = New row_030_00Entity(cLinea)
                ElseIf aValue(0) = "030" And aValue(1) = "10" Then
                    co.DettaglioUtenza.Add(New row_030_10Entity(cLinea))
                ElseIf aValue(0) = "030" And aValue(1) = "35" Then
                    co.DatiCatastali = New row_030_35Entity(cLinea)
                    co.ListaDatiCatastali.Add(New row_030_35Entity(cLinea))
                    'If (Not ut.DatiCatastali.ContainsKey(tmp.UTENZA)) Then
                    '    ut.DatiCatastali.Add(tmp.UTENZA, tmp)
                    'End If
                ElseIf aValue(0) = "040" And aValue(1) = "00" Then
                    ut.TotaleUtenze = New row_040_00Entity(cLinea)
                ElseIf aValue(0) = "050" And aValue(1) = "10" Then
                    ut.TributoProvinciale = New row_050_10Entity(cLinea)
                ElseIf aValue(0) = "050" And aValue(1) = "25" Then
                    ut.Arrotondamento = New row_050_25Entity(cLinea)
                ElseIf aValue(0) = "050" And aValue(1) = "30" Then
                    ut.TotaleDocumento = New row_050_30Entity(cLinea)
                ElseIf aValue(0) = "050" And aValue(1) = "40" Then
                    ut.TotaleIncassato = New row_050_40Entity(cLinea)
                ElseIf aValue(0) = "050" And aValue(1) = "50" Then
                    ut.TotaleDaPagare = New row_050_50Entity(cLinea)
                End If
            End If
        End While
        For Each ut In ListaUtenze.Values
            ut.ConsolidaContratti()
        Next
        sr.Close()
    End Sub

End Class

'        public row_000_00Entity IntestazioneFattura { get; set; }
'        public row_010_00Entity RateBollettino { get; set; }
'        public row_030_00Entity ScadenzeSolleciti { get; set; }
'        public row_030_10Entity DettaglioUtenza { get; set; }
'        public row_030_20Entity TotaleUtenza { get; set; }
'        public row_040_00Entity TotaleUtenze { get; set; }
'        public row_050_00Entity TotaleImponibile { get; set; }
'        public row_050_10Entity TributoProvinciale { get; set; }
'        public row_050_20Entity TotaleIva { get; set; }
'        public row_050_25Entity TotaleArrotondamento { get; set; }
'        public row_050_30Entity TotaleDocuemnto { get; set; }
'        public row_050_40Entity TotaleIncassato { get; set; }
'        public row_050_50Entity TotaleDaPagare { get; set; }
'        public Dictionary<string, UtenzaEntity> ListaUtenze { get; set; }

'        public void AddRawLine(string cLine)
'        {
'            string[] aValue = cLine.Split(';');

'            if (aValue(0] == "000" && aValue(1] == "00")
'                IntestazioneFattura = new row_000_00Entity(cLine);
'            else if (aValue(0] == "010" && aValue(1] == "00")
'                RateBollettino = new row_010_00Entity(cLine);
'            else if (aValue(0] == "030" && aValue(1] == "00")
'                ScadenzeSolleciti = new row_030_00Entity(cLine);
'            else if (aValue(0] == "030" && aValue(1] == "10")
'            {
'                row_030_10Entity tmp = new row_030_10Entity(cLine);
'                if (ListaUtenze.ContainsKey(tmp.utenza))
'                {
'                    Dictionary = ListaUtenze[tmp.utenza];
'                }

'            }
'            else if (aValue(0] == "030" && aValue(1] == "20")
'                TotaleUtenza = new row_030_20Entity(cLine);
'            else if (aValue(0] == "040" && aValue(1] == "00")
'                TotaleUtenze = new row_040_00Entity(cLine);
'            else if (aValue(0] == "050" && aValue(1] == "00")
'                TotaleImponibile = new row_050_00Entity(cLine);
'            else if (aValue(0] == "050" && aValue(1] == "10")
'                TributoProvinciale = new row_050_10Entity(cLine);
'            else if (aValue(0] == "050" && aValue(1] == "20")
'                TotaleIva = new row_050_20Entity(cLine);
'            else if (aValue(0] == "050" && aValue(1] == "25")
'                TotaleArrotondamento = new row_050_25Entity(cLine);
'            else if (aValue(0] == "050" && aValue(1] == "30")
'                TotaleDocuemnto = new row_050_30Entity(cLine);
'            else if (aValue(0] == "050" && aValue(1] == "40")
'                TotaleIncassato = new row_050_40Entity(cLine);
'            else if (aValue(0] == "050" && aValue(1] == "50")
'                TotaleDaPagare = new row_050_50Entity(cLine);
'            else
'                throw new Exception("Tipo riga non trovato: " + aValue(0] + "-" + aValue(1]);
'        }
'    }
'}
