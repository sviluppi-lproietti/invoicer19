<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_forza_scad
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BTN_salva = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.TXB_data_scad = New System.Windows.Forms.TextBox
        Me.CHB_dom_ban = New System.Windows.Forms.CheckBox
        Me.CHB_rim_dir = New System.Windows.Forms.CheckBox
        Me.CHB_bon_ban = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'BTN_salva
        '
        Me.BTN_salva.Location = New System.Drawing.Point(50, 107)
        Me.BTN_salva.Name = "BTN_salva"
        Me.BTN_salva.Size = New System.Drawing.Size(75, 23)
        Me.BTN_salva.TabIndex = 0
        Me.BTN_salva.Text = "Salva"
        Me.BTN_salva.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(157, 107)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Ignora"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Data scadenza da forzare"
        '
        'TXB_data_scad
        '
        Me.TXB_data_scad.Location = New System.Drawing.Point(147, 6)
        Me.TXB_data_scad.Name = "TXB_data_scad"
        Me.TXB_data_scad.Size = New System.Drawing.Size(100, 20)
        Me.TXB_data_scad.TabIndex = 3
        '
        'CHB_dom_ban
        '
        Me.CHB_dom_ban.AutoSize = True
        Me.CHB_dom_ban.Location = New System.Drawing.Point(15, 61)
        Me.CHB_dom_ban.Name = "CHB_dom_ban"
        Me.CHB_dom_ban.Size = New System.Drawing.Size(138, 17)
        Me.CHB_dom_ban.TabIndex = 4
        Me.CHB_dom_ban.Text = "Domiciliazione Bancaria"
        Me.CHB_dom_ban.UseVisualStyleBackColor = True
        '
        'CHB_rim_dir
        '
        Me.CHB_rim_dir.AutoSize = True
        Me.CHB_rim_dir.Location = New System.Drawing.Point(15, 84)
        Me.CHB_rim_dir.Name = "CHB_rim_dir"
        Me.CHB_rim_dir.Size = New System.Drawing.Size(186, 17)
        Me.CHB_rim_dir.TabIndex = 5
        Me.CHB_rim_dir.Text = "Rimessa diretta (bollettino postale)"
        Me.CHB_rim_dir.UseVisualStyleBackColor = True
        '
        'CHB_bon_ban
        '
        Me.CHB_bon_ban.AutoSize = True
        Me.CHB_bon_ban.Location = New System.Drawing.Point(15, 38)
        Me.CHB_bon_ban.Name = "CHB_bon_ban"
        Me.CHB_bon_ban.Size = New System.Drawing.Size(109, 17)
        Me.CHB_bon_ban.TabIndex = 6
        Me.CHB_bon_ban.Text = "Bonifico Bancario"
        Me.CHB_bon_ban.UseVisualStyleBackColor = True
        '
        'FRM_forza_scad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 142)
        Me.Controls.Add(Me.CHB_bon_ban)
        Me.Controls.Add(Me.CHB_rim_dir)
        Me.Controls.Add(Me.CHB_dom_ban)
        Me.Controls.Add(Me.TXB_data_scad)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.BTN_salva)
        Me.Name = "FRM_forza_scad"
        Me.Text = "Forza data di scadenza"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BTN_salva As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXB_data_scad As System.Windows.Forms.TextBox
    Friend WithEvents CHB_dom_ban As System.Windows.Forms.CheckBox
    Friend WithEvents CHB_rim_dir As System.Windows.Forms.CheckBox
    Friend WithEvents CHB_bon_ban As System.Windows.Forms.CheckBox
End Class
