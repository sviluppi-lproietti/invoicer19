Public Class FRM_datifissi

    Private _xmlDatiFissi As Xml.XmlDocument
    Private _FileNameDatiFissi As String
    Private _Modified As Boolean

    Property FileNameDatiFissi() As String

        Get
            Return _FileNameDatiFissi
        End Get
        Set(ByVal value As String)
            _FileNameDatiFissi = value

            _xmlDatiFissi = New Xml.XmlDocument
            _xmlDatiFissi.Load(_FileNameDatiFissi)

            ' Imposta i valori delle tabelle
            Dim xmlItem As Xml.XmlNode
            CMB_tabelle_list.Items.Clear()
            CMB_tabelle_list.Items.Add("--- Seleziona una tabella ---")
            For Each xmlItem In _xmlDatiFissi.SelectSingleNode("DatiFissi").ChildNodes
                CMB_tabelle_list.Items.Add(xmlItem.Attributes("nometabella").Value)
            Next
            CMB_tabelle_list.SelectedIndex = 0
            SettaControlli()
        End Set

    End Property

    Private Sub SettaControlli()

        CMB_variable.Enabled = (CMB_tabelle_list.SelectedIndex > 0)
        TXB_valore.Enabled = (CMB_tabelle_list.SelectedIndex > 0) And CMB_variable.SelectedIndex > 0

    End Sub

    Private Sub CMB_tabelle_list_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMB_tabelle_list.SelectedIndexChanged
        Dim xmlItem As Xml.XmlNode

        SettaControlli()
        If CMB_tabelle_list.SelectedIndex = 0 Then
            CMB_variable.Items.Clear()
            TXB_valore.Text = ""
        Else
            CMB_variable.Items.Clear()
            CMB_variable.Items.Add("--- Seleziona un valore ---")
            For Each xmlItem In _xmlDatiFissi.SelectSingleNode("DatiFissi").ChildNodes(CMB_tabelle_list.SelectedIndex - 1)
                If xmlItem.Name.ToLower = "item" Then
                    If (xmlItem.Attributes("generico").Value = 1) Then
                        CMB_variable.Items.Add(xmlItem.Attributes("descrizione").Value)
                    End If
                End If
            Next
            CMB_variable.SelectedIndex = 0
        End If

    End Sub

    Private Sub CMB_variable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMB_variable.SelectedIndexChanged
        Dim xmlItem As Xml.XmlNode

        SettaControlli()
        If CMB_variable.SelectedIndex = 0 Then
            TXB_valore.Text = ""
        Else
            xmlItem = _xmlDatiFissi.SelectSingleNode("DatiFissi").ChildNodes(CMB_tabelle_list.SelectedIndex - 1).ChildNodes(CMB_variable.SelectedIndex - 1)
            TXB_valore.Text = xmlItem.SelectSingleNode("valore").InnerText
            _Modified = False
        End If

    End Sub

    Private Sub BTN_salva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_salva.Click

        _xmlDatiFissi.SelectSingleNode("DatiFissi").ChildNodes(CMB_tabelle_list.SelectedIndex - 1).ChildNodes(CMB_variable.SelectedIndex - 1).SelectSingleNode("valore").InnerText = TXB_valore.Text
        _xmlDatiFissi.Save(_FileNameDatiFissi)

    End Sub

    Private Sub TXB_valore_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TXB_valore.TextChanged

        _Modified = True

    End Sub

End Class