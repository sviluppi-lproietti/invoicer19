﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using System.Xml;
using System.Data.OleDb;
using TipiComuni;
using System.IO;

namespace PLG_SEN_cosap
{
    public class PLG_SEN_cosap : PLUGIN_interfaceV1_5_0.IPLG_dati_lib
    {
        private int _CurrentRecordNumber;
        private string _DBGiriConnectionString;
        private ArrayList _ElementiMenuDedicati;
        private string _FiltroSelezione;
        private ArrayList _ListaGiri;
        private DataSet _LogDSStampa;
        private Boolean _NuoviToponimi;
        private string _SessioneFld;
        private Boolean _SessioneNuova;
        private int _TipoCaricamento;
        private string _SessioneDatiFissiFileName;
        private string _SessioneTrasformazioniFileName;
        private string _SessioneDatiPath;
        private string _FeedString;
        private int _CodiceModulo;
        private ArrayList _ResultAL;
        private DS.DS_import DSImported;
        private DS.DS_quick_db _QuickDataset;
        private XmlDocument _xmlDati_qd;
        private XmlNode _SezioneDati;
        private string _ErroriLievi;
        private string _ErroriGravi;
        private DS.DS_prn_fattura _PrintFatturaDataset;
        private DS.DS_prn_bollettino _PrintBollettinoDataset;
        private DS.DS_send_email _SendEmailDataset;
        private int _RecordCount;

        public enum ListaDataset
        {
            DAS_quick = 0,
            DAS_fattura = 1,
            DAS_bollettino896 = 2,
            DAS_bollettino896PA = 3,
            DAS_AllegatoPA = 4,
            DAS_F24S = 5,
            DAS_send_email = 999,
        }

        public Boolean CapacitaInvioMail
        {
            get
            {
                return false;
            }
        }

        public DataSet FullDataset
        {
            get
            {
                switch (_CodiceModulo)
                {
                    case (int)ListaDataset.DAS_fattura:
                        return _PrintFatturaDataset;
                        break;
                    case (int)ListaDataset.DAS_bollettino896:
                    case (int)ListaDataset.DAS_bollettino896PA:
                    case (int)ListaDataset.DAS_F24S:
                    case (int)ListaDataset.DAS_AllegatoPA:
                        return _PrintBollettinoDataset;
                        break;
                    case (int)ListaDataset.DAS_send_email:
                        return _SendEmailDataset;
                    default:
                        return null;
                        break;
                };
            }
        }

        public string MessaggioAvviso
        {
            get
            {
                return "";
            }
        }

        public string FullDataSet_MainTableName
        {
            get
            {
                switch (_CodiceModulo)
                {
                    case (int)ListaDataset.DAS_fattura:
                        {
                            return "TBL_main";
                            break;
                        }
                    case (int)ListaDataset.DAS_bollettino896:
                    case (int)ListaDataset.DAS_bollettino896PA:
                    case (int)ListaDataset.DAS_F24S:
                    case (int)ListaDataset.DAS_AllegatoPA:
                        {
                            return "TBL_fat_x_rat";
                            break;
                        }
                    case (int)ListaDataset.DAS_send_email:
                        {
                            return "TBL_main";
                            break;
                        }
                    default:
                        {
                            return "";
                            break;
                        }
                }
            }
        }

        public string FullDataset_MainTableKey
        {
            get
            {
                return "";
            }
        }

        public ArrayList ListaControlliFormNuovaSessione
        {
            get
            {
                ArrayList aCtr;

                aCtr = new ArrayList();
                aCtr.Add(new String[] { "LBL_input_1", "File dati ...................:", "3;16", "155;13" });
                aCtr.Add(new String[] { "TXB_input_1", "&1", "160;12", "610;20", "102" });
                aCtr.Add(new String[] { "BTN_fil_input_1", "", "775;10", "27;23" });
                //aCtr.Add(new String[] { "LBL_input_3", "File credenziali sito web ...:", "3;42", "155;13" });
                //aCtr.Add(new String[] { "TXB_input_3", "&1", "160;38", "610;20", "102" });
                //aCtr.Add(new String[] { "BTN_fil_input_3", "", "775;36", "27;23" });
                //aCtr.Add(new String[] { "LBL_input_4", "File allegato tipografico ...:", "3;68", "155;13" });
                //aCtr.Add(new String[] { "TXB_input_4", "&1", "160;64", "610;20", "190" });
                //aCtr.Add(new String[] { "BTN_fil_input_4", "", "775;62", "27;23" });
                aCtr.Add(new String[] { "LBL_output_998", "Cartella di destinazione .:", "3;94", "155;13" });
                aCtr.Add(new String[] { "TXB_output_998", "&998", "160;90", "610;20", "201" });
                aCtr.Add(new String[] { "BTN_fld_output_998", "", "775;88", "27;23" });
                aCtr.Add(new String[] { "LBL_output_999", "Descrizione archivio ......:", "3;120", "155;13" });
                aCtr.Add(new String[] { "TXB_output_999", "", "160;116", "M;640;122", "202" });
                //                aCtr.Add(new String[] { "SRT", "", "", "", "" });
                return aCtr;
            }
        }

        public string NomePlugIn
        {
            get
            {
                return "Plug-in per la stampa delle bollette della CoSAP per il comune di Senigallia.";
            }
        }

        public DataSet QuickDataset
        {
            get
            {
                return _QuickDataset;
            }
        }

        public string QuickDataset_MainTableKey
        {
            get
            {
                return "MAI_codice";
            }
        }

        public string QuickDataset_MainTableName
        {
            get
            {
                return "TBL_main";
            }
        }

        public ArrayList ResultAL
        {
            get
            {
                return _ResultAL;
            }
        }

        public ArrayList DatiAzioniPreInvio
        {
            get
            {
                return null;
            }
        }

        public ArrayList DatiAzioniPostStampa
        {
            get
            {
                return null;
            }
        }

        public Boolean StampaInProprio
        {
            get
            {
                return true;
            }
        }


        public int CodiceModulo
        {
            set
            {
                _CodiceModulo = value;
            }
        }

        public string FeedString
        {
            set
            {
                _FeedString = value;
            }
        }

        public string SessioneDatiPath
        {
            set
            {
                _SessioneDatiPath = value;
            }
        }
        public string SessioneTrasformazioniFileName
        {
            set
            {
                _SessioneTrasformazioniFileName = value;
            }
        }
        public string SessioneDatiFissiFileName
        {
            set
            {
                _SessioneDatiFissiFileName = value;

            }
        }

        public string SessioneFld
        {
            set
            {
                _SessioneFld = value;
            }
        }
        public XmlNode SezioneDati
        {
            set
            {
                _SezioneDati = value;
            }
        }
        public string[] MessaggiAvanzamento
        {
            get;
            set;
        }

        public XmlNode DataSendByMail
        {
            get
            {
                return null;
            }
        }

        public int CurrentRecordNumber
        {
            get
            {
                return _RecordCount;
            }
        }



        public ArrayList ElementiMenuDedicati
        {
            get
            {
                return new ArrayList();
            }
        }



        public DataSet LogDSStampa
        {
            get
            {
                return _LogDSStampa;
            }
        }

        public string FiltroSelezione
        {
            set
            {
                _FiltroSelezione = value;
            }
        }


        public string LinkField_QuickDS_FullDS
        {
            get
            {
                switch (_CodiceModulo)
                {
                    case (int)ListaDataset.DAS_fattura:
                        return "MAI_linkQD";
                    case (int)ListaDataset.DAS_bollettino896:
                    case (int)ListaDataset.DAS_bollettino896PA:
                    case (int)ListaDataset.DAS_AllegatoPA:
                    case (int)ListaDataset.DAS_F24S:
                        return "FXR_linkQD";
                    case (int)ListaDataset.DAS_send_email:
                        return "MAI_linkQD";
                    default:
                        return "";
                }
            }
        }


        public Boolean SessioneNuova
        {
            set
            {
                _SessioneNuova = value;
            }
        }

        public int TipoCaricamento
        {
            set
            {
                _TipoCaricamento = value;
            }
        }


        public ArrayList ListaGiri
        {
            set
            {
                _ListaGiri = value;
            }
        }

        public string DBGiriConnectionString
        {
            set
            {
                _DBGiriConnectionString = value;
            }
        }

        public Boolean NuoviToponimi
        {
            get
            {
                return _NuoviToponimi;
            }
        }

        public Boolean CheckCondPSA(int nCond2Check, string cValue2Check)
        {
            return false;
        }

        public Boolean CheckCondAPI(int nCond2Check, string cValue2Check)
        {
            return false;
        }

        public void ExecActionPSA(int nAct2Exec, string cValue2Exec)
        {
        }

        public void ExecActionAPI(string cTipoExecAPI, int nAct2Exec, string cValue2Exec)
        {
        }

        public void LoadFullDataset(int nRecordToLoad)
        {
            DS.DS_import ds_src;
            DS.DS_quick_db.TBL_mainRow dr_mai_qd;
            int nRecordToPrint;

            // ************************************************************************** '
            // Creazione del Dataset e azzeramento del contatore dei record da stampare.  '
            // ************************************************************************** '
            nRecordToPrint = 0;
            switch (_CodiceModulo)
            {
                case (int)ListaDataset.DAS_fattura:
                    {
                        _PrintFatturaDataset = new DS.DS_prn_fattura();
                        break;
                    }
                case (int)ListaDataset.DAS_bollettino896:
                case (int)ListaDataset.DAS_bollettino896PA:
                case (int)ListaDataset.DAS_AllegatoPA:
                case (int)ListaDataset.DAS_F24S:
                    {
                        _PrintBollettinoDataset = new DS.DS_prn_bollettino();
                        break;
                    }
                case (int)ListaDataset.DAS_send_email:
                    {
                        _SendEmailDataset = new DS.DS_send_email();
                        break;
                    }
            }

            // ************************************************************************** '
            // Caricamento del DataSet da utilizzare per la stampa.                       '
            // ************************************************************************** '
            while (_RecordCount < _QuickDataset.TBL_main.Rows.Count && nRecordToPrint < nRecordToLoad)
            {
                dr_mai_qd = (DS.DS_quick_db.TBL_mainRow)_QuickDataset.TBL_main.Rows[_RecordCount];
                if (dr_mai_qd.DEF_toprint)
                {
                    try
                    {
                        _ErroriLievi = "";
                        _ErroriGravi = "";
                        ds_src = CaricaRecord(PrintDataSet_DataFile(77), dr_mai_qd.MAI_chiave_doc);

                        switch (_CodiceModulo)
                        {
                            case (int)ListaDataset.DAS_fattura:
                                PreparaRecordPerStampaFattura(dr_mai_qd.MAI_codice, ds_src, GetFeedString(dr_mai_qd));
                                break;
                            case (int)ListaDataset.DAS_bollettino896:
                                if (dr_mai_qd.MAI_print_bol896)
                                    PreparaRecordPerStampaBoll896(dr_mai_qd.MAI_codice, ds_src, GetFeedString(dr_mai_qd));
                                break;
                            case (int)ListaDataset.DAS_bollettino896PA:
                                if (dr_mai_qd.MAI_print_bolPA)
                                    PreparaRecordPerStampaBoll896PA(dr_mai_qd.MAI_codice, ds_src, GetFeedString(dr_mai_qd));
                                break;
                            case (int)ListaDataset.DAS_AllegatoPA:
                                if (dr_mai_qd.MAI_print_AllPA)
                                    PreparaRecordPerStampaBoll896PA(dr_mai_qd.MAI_codice, ds_src, GetFeedString(dr_mai_qd));
                                break;
                            case (int)ListaDataset.DAS_F24S:
                                if (dr_mai_qd.MAI_print_F24)
                                    PreparaRecordPerStampaF24Semplificato(dr_mai_qd.MAI_codice, ds_src, GetFeedString(dr_mai_qd));
                                break;
                            case (int)ListaDataset.DAS_send_email:
                                //GetRecordEmailToSend(dr_mai_qd.MAI_codice, ds_src, dr_fat)
                                break;
                        }

                        if (_ErroriGravi != "")
                        {
                            dr_mai_qd.DEF_errcode = -1;
                            dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori Gravi: ", _ErroriGravi);
                        }
                        else if (_ErroriLievi != "")
                        {
                            dr_mai_qd.DEF_errcode = -2;
                            dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori lievi: ", _ErroriLievi);
                        }
                        else
                        {
                            dr_mai_qd.DEF_errcode = 0;
                            dr_mai_qd.DEF_errdesc = "";
                        }
                    }
                    catch (Exception ex)
                    {
                        dr_mai_qd.DEF_errcode = -1;
                        dr_mai_qd.DEF_errdesc = ex.Message;
                    }
                    nRecordToPrint += 1;
                }
                _RecordCount += 1;
            }

            // ************************************************************************** '
            // Consolidamento del DataSet da utilizzare per la stampa.                    '
            // ************************************************************************** '
            switch (_CodiceModulo)
            {
                case (int)ListaDataset.DAS_fattura:
                    _PrintFatturaDataset.AcceptChanges();
                    break;
                case (int)ListaDataset.DAS_bollettino896:
                case (int)ListaDataset.DAS_bollettino896PA:
                case (int)ListaDataset.DAS_AllegatoPA:
                case (int)ListaDataset.DAS_F24S:
                    _PrintBollettinoDataset.AcceptChanges();
                    break;
                case (int)ListaDataset.DAS_send_email:
                    _SendEmailDataset.AcceptChanges();
                    break;
            }
        }



        public void UpdateQuickDatasetFile()
        {
        }

        public string[] GetSelectedRow(string _SelectFromPlugInType, string cValue)
        {
            return null;
        }

        public void CreaDataBaseSessione(ArrayList _DatiSessione)
        {
            DS.DS_import.TBL_tr_20Row dr20;
            DS.DS_import.TBL_tr_40Row dr40;
            //FC_manage_giri mng_giri;
            XmlDocument xdWork2;
            String cSortValue;
            XmlNode xiRecord;
            int nRecord;
            XmlNode xiWork2;
            Boolean lPrint896;
            Boolean lPrint896PA;
            Boolean lPrintF24;

            _ResultAL = new ArrayList();
            // ************************************************************************** '
            // Copia i file dei dati dalla cartella dove sono attualmente nella cartella  '
            // dati della sessione.                                                       '
            // ************************************************************************** '
            ImpostaMessaggi(new String[] { "", "", "", "0", _DatiSessione.Count.ToString(), "Elaborazione dei file da caricare...." });
            foreach (string[] aValue in _DatiSessione)
                ImportaFileInSessione(int.Parse(aValue[0]), aValue[1]);

            // ************************************************************************** '
            // Caricamento dei dati dei giri che eventualmente sono stati selezionati.    '
            // ************************************************************************** '
            _NuoviToponimi = false;
            ImpostaMessaggi(new String[] { "", "", "", "0", "1", "Caricamento dei dati dei giri di consegna..." });
            //mng_giri = new FC_manage_giri(new OleDbConnection(_DBGiriConnectionString));
            //mng_giri.ListaGiri = _ListaGiri;
            xdWork2 = new XmlDocument();
            xdWork2.AppendChild(xdWork2.CreateNode(XmlNodeType.Element, "FATTURA", ""));

            // ************************************************************************** '
            // Creazione del file indice: creazione e ordinamento dei record recuperati e '
            // salvataggio dei nuovi toponimi rintracciati.                               '
            // ************************************************************************** '
            ImpostaMessaggi(new String[] { "", "", "", "0", _ResultAL.Count.ToString(), "Elaborazione dei file della cartella di archiviazione..." });
            foreach (DS.DS_import.TBL_tr_10Row dr10 in DSImported.TBL_tr_10.Rows)
            {
                dr20 = (DS.DS_import.TBL_tr_20Row)dr10.GetChildRows("TBL_tr_10TBL_tr_20")[0];
                dr40 = (DS.DS_import.TBL_tr_40Row)dr10.GetChildRows("TBL_tr_10TBL_tr_40")[0];
                lPrint896 = false;
                lPrint896PA = false;
                lPrintF24 = false;

                xiRecord = xdWork2.CreateNode(XmlNodeType.Element, "FATTURA_ROW", "");
                switch (dr40.campo05)
                {
                    case "PASSI CARRABILI":
                        xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_tipo_fattura", "PASSI CARRABILI"));
                        lPrint896PA = true;
                        break;
                    case "TARES/TARI":
                        xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_tipo_fattura", "TARES/TARI"));
                        lPrintF24 = true;
                        break;
                    case "CANONE OCCUPAZIONE PERMANENTE SPAZI AREE PUBBLICHE":
                        xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_tipo_fattura", "COSAP PERM."));
                        lPrint896PA = true;
                        break;
                    case "CANONE OCCUPAZIONE TEMPORANEA SPAZI AREE PUBBLICHE":
                        xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_tipo_fattura", "COSAP TEMP."));
                        lPrint896 = true;
                        break;
                    case "IMPOSTA COMUNALE SULLA PUBBLICITA'":
                        xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_tipo_fattura", "PUBBLICITA'"));
                        lPrint896 = true;
                        break;
                    default:
                        throw new Exception("Tipo documento pagamento non riconosciuto: " + dr40.campo05 + ".");
                }

                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_num_utente", dr10.T10_matricola));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_num_fattura", dr20.campo05));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_chiave_doc", dr10.T10_ID_documento));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_nominativo", dr10.T10_nominativo));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_indirizzo_recapito", dr10.T10_rec_indirizzo));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_citta_recapito", dr10.T10_rec_comune));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_cap_recapito", dr10.T10_rec_cap));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_provincia_recapito", dr10.T10_rec_provincia));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_print_bol896", lPrint896.ToString()));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_print_bolPA", lPrint896PA.ToString()));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_print_F24", lPrintF24.ToString()));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "MAI_print_AllPA", lPrint896PA.ToString()));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "DEF_errcode", "0"));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "DEF_errdesc", ""));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "DEF_onlyarchott", "false"));
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "DEF_sendbymail", "false"));

                cSortValue = "111111";
                //cSortValue += dr10.T10_rec_cap.ToString();
                cSortValue += dr10.T10_rec_provincia.ToString();
                cSortValue += dr10.T10_rec_comune.ToString();
                cSortValue += dr10.T10_rec_indirizzo.ToLower().ToString();
                xiRecord.AppendChild(SetRecordIDX(xdWork2, "SORTFIELD", cSortValue));
                xdWork2.SelectSingleNode("FATTURA").AppendChild(xiRecord);

                //  MessaggiAvanzamento[3] = (int.Parse(MessaggiAvanzamento[3]) + 1).ToString();
            }

            // ************************************************************************** '
            // ************************************************************************** '
            //ImpostaMessaggi(new string[] { "", "", "", "0", "1", "Salvataggio nuove vie/toponimi nel database..." });
            //try
            //{
            //    mng_giri.InserisciCittaToponimiPLUGIN();
            //    mng_giri = null;
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}


            // ************************************************************************** '
            // ************************************************************************** '
            MessaggiAvanzamento[5] = "Ordinamento dei record in corso...";
            FC_SortFile.SortFileIndiceXML(xdWork2, 0, xdWork2.SelectSingleNode("FATTURA").ChildNodes.Count - 1);

            // ************************************************************************** '
            // Rimuove il campo utilizzato per l'ordinamento e genera il file che servirà ' 
            // per l'invio delle fatture via mail.                                        '
            // ************************************************************************** '
            nRecord = 1;
            foreach (XmlNode xiWork1 in xdWork2.SelectNodes("FATTURA/FATTURA_ROW/SORTFIELD"))
            {
                xiWork2 = xiWork1.ParentNode;
                xiWork2.RemoveChild(xiWork1);
                xiWork2.AppendChild(SetRecordIDX(xdWork2, "MAI_codice", nRecord.ToString()));
                nRecord += 1;
            }

            // ************************************************************************** '
            // Salva il file indice nella sessione.                                       '
            // ************************************************************************** '
            xdWork2.Save(String.Concat(_SessioneFld, @"\dati\indice.xml"));
            _ResultAL.Add(new String[] { "filename", @"dati\indice.xml", "0", "0", "1" });
        }

        public void LoadQuickDataset()
        {
            DS.DS_quick_db.TBL_mainRow dr_mai_qd;
            Boolean lLoad;

            _QuickDataset = new DS.DS_quick_db();

            _xmlDati_qd = new XmlDocument();
            _xmlDati_qd.Load(QuickDataSet_IndexFile);
            ImpostaMessaggi(new String[] { "", "", "", "0", _xmlDati_qd.SelectNodes(PLG_costanti.itm_qd_Fattura).Count.ToString(), "Caricamento dei dati...." });

            foreach (XmlNode xmlItem in _xmlDati_qd.SelectNodes(PLG_costanti.itm_qd_Fattura))
            {
                lLoad = (_TipoCaricamento == 1) || (_TipoCaricamento == 2 && Boolean.Parse(GetValueFromXML(xmlItem, "DEF_sendbymail")));

                if (lLoad)
                {
                    dr_mai_qd = (DS.DS_quick_db.TBL_mainRow)_QuickDataset.TBL_main.NewRow();
                    try
                    {
                        dr_mai_qd.MAI_codice = int.Parse(GetValueFromXML(xmlItem, "MAI_codice"));
                        dr_mai_qd.DEF_toprint = false;
                        dr_mai_qd.DEF_errcode = short.Parse(GetValueFromXML(xmlItem, "DEF_errcode"));
                        dr_mai_qd.DEF_errdesc = GetValueFromXML(xmlItem, "DEF_errdesc");
                        dr_mai_qd.DEF_selectable = ((dr_mai_qd.DEF_errcode == 0) || (dr_mai_qd.DEF_errcode == -2));
                        dr_mai_qd.DEF_onlyarchott = Boolean.Parse(GetValueFromXML(xmlItem, "DEF_onlyarchott"));
                        dr_mai_qd.DEF_sendbymail = Boolean.Parse(GetValueFromXML(xmlItem, "DEF_sendbymail"));
                        dr_mai_qd.MAI_num_utente = GetValueFromXML(xmlItem, "MAI_num_utente");
                        dr_mai_qd.MAI_num_fattura = GetValueFromXML(xmlItem, "MAI_num_fattura");
                        dr_mai_qd.MAI_chiave_doc = decimal.Parse(GetValueFromXML(xmlItem, "MAI_chiave_doc"));
                        dr_mai_qd.MAI_tipo_fattura = GetValueFromXML(xmlItem, "MAI_tipo_fattura");
                        dr_mai_qd.MAI_nominativo = GetValueFromXML(xmlItem, "MAI_nominativo");
                        dr_mai_qd.MAI_indirizzo_recapito = GetValueFromXML(xmlItem, "MAI_indirizzo_recapito");
                        dr_mai_qd.MAI_citta_recapito = GetValueFromXML(xmlItem, "MAI_citta_recapito");
                        dr_mai_qd.MAI_cap_recapito = GetValueFromXML(xmlItem, "MAI_cap_recapito");
                        dr_mai_qd.MAI_provincia_recapito = GetValueFromXML(xmlItem, "MAI_provincia_recapito");
                        dr_mai_qd.MAI_nazione_recapito = GetValueFromXML(xmlItem, "MAI_nazione_recapito");
                        dr_mai_qd.MAI_print_bol896 = Boolean.Parse((GetValueFromXML(xmlItem, "MAI_print_bol896").ToLower()));
                        dr_mai_qd.MAI_print_bolPA = Boolean.Parse((GetValueFromXML(xmlItem, "MAI_print_bolPA").ToLower()));
                        dr_mai_qd.MAI_print_AllPA = Boolean.Parse((GetValueFromXML(xmlItem, "MAI_print_AllPA").ToLower()));
                        dr_mai_qd.MAI_print_F24 = Boolean.Parse((GetValueFromXML(xmlItem, "MAI_print_F24").ToLower()));
                        //dr_mai_qd.MAI_filedati = int.Parse(GetValueFromXML(xmlItem, "MAI_filedati"));
                        //dr_mai_qd.MAI_IDFattura = GetValueFromXML(xmlItem, "MAI_IDFattura");
                    }
                    catch (Exception ex)
                    {
                        dr_mai_qd.DEF_errcode = -1;
                        dr_mai_qd.DEF_errdesc = ex.Message;
                    }
                    ImpostaMessaggi(new String[] { "", "", "", (MessaggiAvanzamento[3] + 1).ToString(), "", "" });
                    _QuickDataset.TBL_main.Rows.Add(dr_mai_qd);
                }
            }
        }


        public void PostAnteprima(ArrayList aLista)
        {

        }

        public void SetPrintable(DataRow dr, string _SelectFromPlugInType, string cParamSelez)
        {
        }

        public void SetSelectable(DataRow dr, string cFilter)
        {
        }

        public void RemovePRNDSErrorRecord()
        {
        }

        public void GoToRecordNumber(int nRecord)
        {
            _RecordCount = nRecord;
        }

        public void ImpostaDGVDati(DataGridView DGV)
        {
            DGV.Columns.Clear();
            if (_TipoCaricamento == 1)
                DGV.Columns.Add(Col_DaStampare());
            else if (_TipoCaricamento == 2)
                DGV.Columns.Add(Col_DaInviare()); ;
            DGV.Columns.Add(Col_Errore_code());
            DGV.Columns.Add(Col_Errore_desc());
            DGV.Columns.Add(Col_Progressivo());
            DGV.Columns.Add(Col_NumeroUtente());
            DGV.Columns.Add(Col_NumeroFattura());
            DGV.Columns.Add(Col_TipoFattura());
            DGV.Columns.Add(Col_Nominativo());
            DGV.Columns.Add(COL_Indirizzo_Recapito());
            DGV.Columns.Add(COL_CAP_Recapito());
            DGV.Columns.Add(COL_Citta_Recapito());
            DGV.Columns.Add(COL_Provincia_Recapito());
            DGV.Columns.Add(COL_Print_Bollettino896());
            DGV.Columns.Add(COL_Print_Bollettino896PA());
            DGV.Columns.Add(COL_Print_AllegatoPA());
            DGV.Columns.Add(COL_Print_BollettinoF24());
            DGV.Columns.Add(COL_SpedByMail());
        }

        private DataGridViewCheckBoxColumn Col_DaStampare()
        {
            DataGridViewCheckBoxColumn col;

            col = new DataGridViewCheckBoxColumn();
            col.HeaderText = "Stampa";
            col.DataPropertyName = "DEF_toprint";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Width = 50;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewCheckBoxColumn Col_DaInviare()
        {
            DataGridViewCheckBoxColumn col;

            col = new DataGridViewCheckBoxColumn();
            col.HeaderText = "Invia";
            col.DataPropertyName = "DEF_toprint";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Width = 50;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_Errore_code()
        {
            DataGridViewTextBoxColumn col;

            col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            col.HeaderText = "Errore";
            col.DataPropertyName = "DEF_errcode";
            col.Visible = false;
            return col;
        }

        private DataGridViewTextBoxColumn Col_Errore_desc()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Errore";
            col.DataPropertyName = "DEF_errdesc";
            col.Visible = false;
            return col;
        }

        private DataGridViewTextBoxColumn Col_Progressivo()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Progressivo";
            col.DataPropertyName = "MAI_codice";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            col.Width = 70;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_NumeroUtente()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Num. Utente";
            col.DataPropertyName = "MAI_num_utente";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            col.Width = 90;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_NumeroFattura()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Num. Fattura";
            col.DataPropertyName = "MAI_num_fattura";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            col.Width = 110;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_TipoFattura()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.HeaderText = "Tipo Fattura";
            col.DataPropertyName = "MAI_tipo_fattura";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Width = 110;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn Col_Nominativo()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Nominativo";
            col.DataPropertyName = "MAI_nominativo";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_Indirizzo_Recapito()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Indirizzo Recapito";
            col.DataPropertyName = "MAI_indirizzo_recapito";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_CAP_Recapito()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "CAP";
            col.DataPropertyName = "MAI_cap_recapito";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_Citta_Recapito()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Città";
            col.DataPropertyName = "MAI_citta_recapito";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_Provincia_Recapito()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Prov.";
            col.DataPropertyName = "MAI_provincia_recapito";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.ReadOnly = true;
            return col;
        }

        private DataGridViewTextBoxColumn COL_Print_Bollettino896()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Boll. 896";
            col.DataPropertyName = "MAI_print_bol896";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";
            return col;
        }

        private DataGridViewTextBoxColumn COL_Print_Bollettino896PA()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Boll. 896 PA";
            col.DataPropertyName = "MAI_print_bolPA";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";
            return col;
        }

        private DataGridViewTextBoxColumn COL_Print_AllegatoPA()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Allegato PA";
            col.DataPropertyName = "MAI_print_AllPA";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";
            return col;
        }
        private DataGridViewTextBoxColumn COL_Print_BollettinoF24()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "F24 Sempl.";
            col.DataPropertyName = "MAI_print_F24";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";
            return col;
        }

        private DataGridViewTextBoxColumn COL_SpedByMail()
        {
            DataGridViewTextBoxColumn col;

            col = new DataGridViewTextBoxColumn();
            col.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            col.HeaderText = "Sped. Email";
            col.DataPropertyName = "DEF_sendbymail";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 120;
            col.ReadOnly = true;
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Tag = "boolean";

            return col;
        }

        public Boolean CheckDatiSessione(ArrayList aDati)
        {
            return true;
        }

        #region "Funzioni per la creazione dei dati della sessione."

        // ***************************
        //
        //
        // ***************************
        private void ImportaFileInSessione(int nTipoFile, string cFileName)
        {
            String[] cLineaDivisa;
            string cTipoRecord;
            string cTabella;
            StreamReader sr;
            string cLinea;
            int nColumn;
            int nStart;
            DataRow dr;

            DSImported = new DS.DS_import();
            sr = new StreamReader(cFileName);
            while (!sr.EndOfStream)
            {
                cLinea = sr.ReadLine();
                if (cLinea != "")
                {
                    cLineaDivisa = cLinea.Split('|');
                    cTabella = "";

                    cTipoRecord = cLineaDivisa[0];
                    nStart = 0;
                    switch (cTipoRecord)
                    {
                        case "10":
                            {
                                cTabella = "TBL_tr_10";
                                nStart = 1;
                                break;
                            }
                        case "20":
                            {
                                cTabella = "TBL_tr_20";
                                break;
                            }
                        case "25":
                            {
                                cTabella = "TBL_tr_25";
                                break;
                            }
                        case "30":
                            {
                                cTabella = "TBL_tr_30";
                                break;
                            }
                        case "40":
                            {
                                cTabella = "TBL_tr_40";
                                break;
                            }
                        case "42":
                            {
                                cTabella = "TBL_tr_42";
                                break;
                            }
                        case "44":
                            {
                                cTabella = "TBL_tr_44";
                                break;
                            }
                        case "46":
                            {
                                cTabella = "TBL_tr_46";
                                break;
                            }
                        case "50":
                            {
                                cTabella = "TBL_tr_50";
                                break;
                            }
                        case "60":
                            {
                                cTabella = "TBL_tr_60";
                                break;
                            }
                        case "99":
                            {
                                cTabella = "TBL_tr_99";
                                break;
                            }
                    }

                    if (cTabella != "")
                    {
                        nColumn = DSImported.Tables[cTabella].Columns.Count;
                        dr = DSImported.Tables[cTabella].NewRow();
                        for (int i = 1; i < nColumn; i++)
                        {
                            if (cLineaDivisa.Length > i)
                                dr[i - nStart] = cLineaDivisa[i];
                        }
                        DSImported.Tables[cTabella].Rows.Add(dr);
                    }
                }
            }
            sr.Close();

            //
            // Aggiusta i campi in attesa di chiarimenti
            //
            foreach (DS.DS_import.TBL_tr_10Row dr10 in DSImported.TBL_tr_10.Rows)
            {
                if (dr10.T10_rec_nominativo == "")
                {
                    dr10.T10_rec_nominativo = dr10.T10_nominativo;
                    dr10.T10_rec_indirizzo = dr10.T10_res_indirizzo;
                    dr10.T10_rec_cap = dr10.T10_res_cap;
                    dr10.T10_rec_comune = dr10.T10_res_comune;
                    dr10.T10_rec_provincia = dr10.T10_res_provincia;
                }
            }

            //
            // Aggiusta i campi delle tabelle
            // 
            foreach (DS.DS_import.TBL_tr_20Row dr20 in DSImported.TBL_tr_20.Rows)
            {
                dr20.campo05 = dr20.campo05.PadLeft(7, '0');
            }

            foreach (DataTable dt in DSImported.Tables)
            {
                SalvaTabelle(dt);
            }

        }

        private void SalvaTabelle(DataTable dt)
        {
            string cConnACCStr = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", PrintDataSet_DataFile(77), ";");

            OleDbCommand oCmd = new OleDbCommand();
            OleDbDataAdapter oDa = new OleDbDataAdapter(oCmd);
            OleDbCommandBuilder oComBuil = new OleDbCommandBuilder(oDa);

            oCmd.Connection = new OleDbConnection(cConnACCStr);

            // Salvataggio della tabella TBL_main
            oCmd.CommandText = "SELECT * FROM " + dt.TableName;
            oDa = new OleDbDataAdapter(oCmd);
            oComBuil = new OleDbCommandBuilder(oDa);
            oDa.Update(dt);
        }

        #endregion

        // ***************************
        //
        // Funzioni di servizio
        //
        // ***************************
        private void ImpostaMessaggi(String[] aValue)
        {
            for (int i = 0; i <= 5; i++)
            {
                if (aValue[i] != "")
                    MessaggiAvanzamento[i] = aValue[i];
            }
        }

        private XmlNode SetRecordIDX(XmlDocument xmlIndice, String cField, String cValue)
        {
            XmlNode xiTmp;
            xiTmp = xmlIndice.CreateNode(XmlNodeType.Element, cField, "");
            xiTmp.InnerText = cValue;
            return xiTmp;
        }

        private string GetValueFromXML(XmlNode xmlItem, string xmlPath, string cDefault = "", string cMsgError = "")
        {
            String cValue;

            cValue = "";
            try
            {
                cValue = cDefault;
                if (xmlItem.SelectSingleNode(xmlPath) != null)
                    cValue = xmlItem.SelectSingleNode(xmlPath).InnerText;

                if (cValue == "" && cMsgError != "")
                    AddErroreLieve(cMsgError);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("Errore nella procedura di recupero dei valori del file XML.", "\n", "percorso da cui si tenta il recupero:", "\n", xmlPath), ex);
            }
            return cValue;

        }

        private void AddErroreLieve(string cMsg)
        {
            if (_ErroriLievi == "")
                _ErroriLievi = cMsg;
            else
                _ErroriLievi = String.Concat(_ErroriLievi, "; ", cMsg);

        }

        private string PrintDataSet_DataFile(int _FileIdx)
        {
            return string.Concat(_SessioneFld, _SezioneDati.SelectSingleNode(String.Concat("filename[@codice=", "", _FileIdx, "", "]")).InnerText);
        }

        //
        //
        //
        //
        private string QuickDataSet_IndexFile
        {
            get
            {
                return string.Concat(_SessioneFld, _SezioneDati.SelectSingleNode("filename[@tipo=\"0\"]").InnerText);
            }
        }

        private DS.DS_import CaricaRecord(string cFileName, decimal nCodice)
        {
            DSImported = new DS.DS_import();

            foreach (DataTable dt in DSImported.Tables)
            {
                CaricaTabella(dt, nCodice);
            }

            return DSImported;
        }

        private void CaricaTabella(DataTable dt, decimal nCodice)
        {
            string cConnACCStr = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", PrintDataSet_DataFile(77), ";");

            OleDbCommand oCmd = new OleDbCommand();
            OleDbDataAdapter oDa = new OleDbDataAdapter(oCmd);
            OleDbCommandBuilder oComBuil = new OleDbCommandBuilder(oDa);

            oCmd.Connection = new OleDbConnection(cConnACCStr);

            // Caricamento della tabella TBL_main
            if (dt.TableName.ToLower() == "TBL_tr_10".ToLower())
                oCmd.CommandText = "SELECT * FROM " + dt.TableName + " WHERE T10_ID_documento = '" + nCodice.ToString() + "'";
            else if (dt.TableName.ToLower() == "TBL_tr_99".ToLower())
                oCmd.CommandText = "SELECT * FROM " + dt.TableName;
            else
                oCmd.CommandText = "SELECT * FROM " + dt.TableName + " WHERE T" + dt.TableName.Substring(7, 2) + "_cod_t10 = '" + nCodice.ToString() + "'"; ;
            oDa = new OleDbDataAdapter(oCmd);
            oComBuil = new OleDbCommandBuilder(oDa);
            oDa.Fill(dt);
        }

        //
        // Preparazione del dataset per la stampa della fattura.
        //
        private void PreparaRecordPerStampaFattura(int nCodice, DS.DS_import dsSrc, String cFeedString)
        {
            DS.DS_prn_fattura.TBL_righe_fattRow dr_rfa;
            DS.DS_prn_fattura.TBL_dati_fattRow dr_dfa;
            DS.DS_prn_fattura.TBL_mainRow dr_mai;
            DS.DS_prn_fattura.TBL_rid_magRow dr_rom;
            DS.DS_import.TBL_tr_10Row d10;
            DS.DS_import.TBL_tr_20Row d20;
            DS.DS_import.TBL_tr_25Row d25;
            DS.DS_import.TBL_tr_30Row d30;
            DS.DS_import.TBL_tr_40Row d401;
            DS.DS_prn_fattura.TBL_rateRow dr_rat;
            DS.DS_prn_fattura.TBL_rate_bisRow dr_rat_bis;
            int nRow;

            d10 = (DS.DS_import.TBL_tr_10Row)dsSrc.TBL_tr_10.Rows[0];
            d20 = (DS.DS_import.TBL_tr_20Row)dsSrc.TBL_tr_20.Rows[0];
            if (dsSrc.TBL_tr_25.Rows.Count > 0)
                d25 = (DS.DS_import.TBL_tr_25Row)dsSrc.TBL_tr_25.Rows[0];
            else
                d25 = null;
            d30 = null;
            foreach (DS.DS_import.TBL_tr_30Row dr_tmp in dsSrc.TBL_tr_30.Rows)
                if (dr_tmp.campo03 == "4")
                    d30 = dr_tmp;
            if (d30 == null)
                d30 = (DS.DS_import.TBL_tr_30Row)dsSrc.TBL_tr_30.Rows[0];

            //
            // Carica i dati del record principale
            //
            dr_mai = (DS.DS_prn_fattura.TBL_mainRow)_PrintFatturaDataset.TBL_main.NewRow();
            dr_mai.MAI_linkQD = nCodice;
            dr_mai.DEF_raccolta = 9;
            dr_mai.DEF_alimimb = "3";
            dr_mai.MAI_cod_fis_identificativo = d10.T10_ID_documento;
            dr_mai.MAI_cod_ute = d10.T10_matricola;
            dr_mai.MAI_nominativo = RiduciLunghezzaStringa(d10.T10_nominativo, 30);
            dr_mai.MAI_cod_fis = d10.campo07;
            dr_mai.MAI_par_iva = d10.campo08;
            if ((dr_mai.MAI_cod_fis == dr_mai.MAI_par_iva) && (dr_mai.MAI_cod_fis.Length == 11))
                dr_mai.MAI_cod_fis = "";

            dr_mai.MAI_rec_sito_1 = RiduciLunghezzaStringa(d10.T10_rec_nominativo, 45);
            dr_mai.MAI_rec_sito_2 = RiduciLunghezzaStringa(d10.T10_rec_indirizzo, 45);
            dr_mai.MAI_rec_sito_3 = d10.T10_rec_cap;
            dr_mai.MAI_rec_sito_4 = d10.T10_rec_comune;
            dr_mai.MAI_rec_sito_5 = d10.T10_rec_provincia;
            _PrintFatturaDataset.TBL_main.AddTBL_mainRow(dr_mai);

            //
            // Dati del documento
            //
            dr_dfa = (DS.DS_prn_fattura.TBL_dati_fattRow)_PrintFatturaDataset.TBL_dati_fatt.NewRow();
            dr_dfa.DFA_cod_mai = dr_mai.MAI_codice;

            dr_dfa.DFA_anno = short.Parse(d20.campo03);
            dr_dfa.DFA_numero = d20.campo05;
            dr_dfa.DFA_data_emissione = d20.campo06;
            dr_dfa.DFA_data_scadenza = d30.campo04;
            dr_dfa.DFA_totale_dovuto = decimal.Parse(d20.campo13);
            dr_dfa.DFA_imp_tassa = decimal.Parse(d20.campo20);
            dr_dfa.DFA_imp_addizionali = decimal.Parse(d20.campo21);
            dr_dfa.DFA_totale_pagare =  decimal.Parse(d20.campo22);

            dr_dfa.DFA_totale_pagare = decimal.Parse(d30.campo05);
            dr_dfa.DFA_arrotondamento = decimal.Parse(d20.campo12);
            _PrintFatturaDataset.TBL_dati_fatt.AddTBL_dati_fattRow(dr_dfa);

            //
            // Dati delle righe della fattura
            //
            nRow = 1;
            foreach (DS.DS_import.TBL_tr_40Row d40 in dsSrc.TBL_tr_40.Rows)
            {
                switch (d40.campo05)
                {
                    case "PASSI CARRABILI":
                        dr_mai.MAI_tipo_tributo = 1;
                        break;
                    case "TARES/TARI":
                        dr_mai.MAI_tipo_tributo = 2;
                        if (d25 != null)
                        {
                            dr_mai.MAI_tipo_tributo = 6;
                            dr_dfa.DFA_importoannoprec = decimal.Parse(d25.campo13);
                            dr_dfa.DFA_giapagato = decimal.Parse(d20.campo17);
                        }

                        break;
                    case "CANONE OCCUPAZIONE TEMPORANEA SPAZI AREE PUBBLICHE":
                        dr_mai.MAI_tipo_tributo = 3;
                        break;
                    case "IMPOSTA COMUNALE SULLA PUBBLICITA'":
                        dr_mai.MAI_tipo_tributo = 4;
                        break;
                    case "CANONE OCCUPAZIONE PERMANENTE SPAZI AREE PUBBLICHE":
                        dr_mai.MAI_tipo_tributo = 5;
                        break;
                    default:
                        throw new Exception("Tipo documento pagamento non riconosciuto: " + d40.campo05 + ".");
                }
                dr_rfa = (DS.DS_prn_fattura.TBL_righe_fattRow)_PrintFatturaDataset.TBL_righe_fatt.NewRow();
                dr_rfa.RFA_cod_mai = dr_mai.MAI_codice;
                dr_rfa.RFA_progressivo = nRow.ToString();
                dr_rfa.RFA_descrizione_classe_uso = (d40.campo07 + " " + d40.campo08).Replace("TARSU", "TARI");
                dr_rfa.RFA_indirizzo = d40.campo32;
                dr_rfa.RFA_dal = d40.campo09;
                dr_rfa.RFA_al = d40.campo10;
                dr_rfa.RFA_sup_assoggettabile = d40.campo14;
                dr_rfa.RFA_giorni_occ = decimal.Parse(d40.campo11);
                dr_rfa.RFA_autorizzazione = d40.campo59;
                dr_rfa.RFA_rid_1_desc = d40.campo39;
                dr_rfa.RFA_rid_1_perc = d40.campo41;
                if (d40.campo40 == "0")
                    dr_rfa.RFA_rid_1_perc += " %";
                dr_rfa.RFA_rid_1_dal = d40.campo42;
                dr_rfa.RFA_rid_1_al = d40.campo43;
                dr_rfa.RFA_foglio = d40.campo34;
                dr_rfa.RFA_numero = d40.campo35;
                dr_rfa.RFA_subalterno = d40.campo36;
                dr_rfa.RFA_unitaria_fissa = decimal.Parse(d40.campo17);
                dr_rfa.RFA_unitaria_variabile = decimal.Parse(d40.campo18);
                dr_rfa.RFA_totale_fissa = decimal.Parse(d40.campo19);
                dr_rfa.RFA_totale_variabile = decimal.Parse(d40.campo20);
                dr_rfa.RFA_totale_add = decimal.Parse(d40.campo26);
                dr_rfa.RFA_num_occ = int.Parse(d40.campo15);
                dr_rfa.RFA_totale_utenza = decimal.Parse(d40.campo21) + decimal.Parse(d40.campo22) + decimal.Parse(d40.campo23);
                dr_rfa.RFA_domestica = d40.campo07.ToUpper() == "UTENZE DOMESTICHE";
                _PrintFatturaDataset.TBL_righe_fatt.AddTBL_righe_fattRow(dr_rfa);
                if (d40.campo39 != "")
                {
                    dr_rom = (DS.DS_prn_fattura.TBL_rid_magRow)_PrintFatturaDataset.TBL_rid_mag.NewRow();
                    dr_rom.ROM_cod_rfa = dr_rfa.RFA_codice;
                    dr_rom.ROM_descrizione = d40.campo39;
                    dr_rom.ROM_valore = d40.campo41;
                    dr_rom.ROM_dal = d40.campo42;
                    dr_rom.ROM_al = d40.campo43;
                    _PrintFatturaDataset.TBL_rid_mag.AddTBL_rid_magRow(dr_rom);
                }
                if (d40.campo44 != "")
                {
                    dr_rom = (DS.DS_prn_fattura.TBL_rid_magRow)_PrintFatturaDataset.TBL_rid_mag.NewRow();
                    dr_rom.ROM_cod_rfa = dr_rfa.RFA_codice;
                    dr_rom.ROM_descrizione = d40.campo44;
                    dr_rom.ROM_valore = d40.campo46;
                    dr_rom.ROM_dal = d40.campo47;
                    dr_rom.ROM_al = d40.campo48;
                    _PrintFatturaDataset.TBL_rid_mag.AddTBL_rid_magRow(dr_rom);
                }
                if (d40.campo49 != "")
                {
                    dr_rom = (DS.DS_prn_fattura.TBL_rid_magRow)_PrintFatturaDataset.TBL_rid_mag.NewRow();
                    dr_rom.ROM_cod_rfa = dr_rfa.RFA_codice;
                    dr_rom.ROM_descrizione = d40.campo49;
                    dr_rom.ROM_valore = d40.campo51;
                    dr_rom.ROM_dal = d40.campo52;
                    dr_rom.ROM_al = d40.campo53;
                    _PrintFatturaDataset.TBL_rid_mag.AddTBL_rid_magRow(dr_rom);
                }
                if (d40.campo54 != "")
                {
                    dr_rom = (DS.DS_prn_fattura.TBL_rid_magRow)_PrintFatturaDataset.TBL_rid_mag.NewRow();
                    dr_rom.ROM_cod_rfa = dr_rfa.RFA_codice;
                    dr_rom.ROM_descrizione = d40.campo54;
                    dr_rom.ROM_valore = d40.campo56;
                    dr_rom.ROM_dal = d40.campo57;
                    dr_rom.ROM_al = d40.campo58;
                    _PrintFatturaDataset.TBL_rid_mag.AddTBL_rid_magRow(dr_rom);
                }
                nRow += 1;
            }

            if (dr_mai.MAI_tipo_tributo == 3 || dr_mai.MAI_tipo_tributo == 5)
            {
                //
                // Aggiungo dati per allegato PA
                //

                DS.DS_prn_fattura.TBL_fattureRow dr_fat;

                //  DS.DS_import.TBL_tr_50Row d50;

                d10 = (DS.DS_import.TBL_tr_10Row)dsSrc.TBL_tr_10.Rows[0];
                d20 = (DS.DS_import.TBL_tr_20Row)dsSrc.TBL_tr_20.Rows[0];
                d30 = null;
                foreach (DS.DS_import.TBL_tr_30Row dr_tmp in dsSrc.TBL_tr_30.Rows)
                    if (dr_tmp.campo03 == "0")
                        d30 = dr_tmp;
                d401 = (DS.DS_import.TBL_tr_40Row)dsSrc.TBL_tr_40.Rows[0];
                //d50 = (DS.DS_import.TBL_tr_50Row)dsSrc.TBL_tr_50.Rows[0];

                dr_fat = (DS.DS_prn_fattura.TBL_fattureRow)_PrintFatturaDataset.TBL_fatture.NewRow();
                dr_fat.FAT_codice = _PrintFatturaDataset.TBL_fatture.Rows.Count + 1;
                dr_fat.FAT_cod_mai = dr_mai.MAI_codice;
                dr_fat.FAT_prot_reg_iva = "";
                dr_fat.FAT_num_fattura = d20.campo05;
                dr_fat.FAT_data_fattura = d20.campo06;
                dr_fat.FAT_nominativo = d10.T10_nominativo.PadRight(40, ' ');
                dr_fat.FAT_cod_utente = d10.T10_matricola;
                dr_fat.FAT_ind_rec_1 = d10.T10_res_indirizzo;
                dr_fat.FAT_ind_rec_2 = d10.T10_res_cap;
                dr_fat.FAT_ind_rec_3 = d10.T10_res_provincia;
                dr_fat.FAT_codice_fiscale = d10.campo07.PadRight(16, ' ');
                dr_fat.FAT_ind_rec_4 = d10.T10_res_comune;

                _PrintFatturaDataset.TBL_fatture.Rows.Add(dr_fat);

                foreach (DS.DS_import.TBL_tr_30Row drtmp in dsSrc.TBL_tr_30.Rows)
                {
                    if (drtmp.campo03 != "0")
                    {
                        dr_rat_bis = (DS.DS_prn_fattura.TBL_rate_bisRow)_PrintFatturaDataset.TBL_rate_bis.NewRow();
                        dr_rat_bis.RAT_codice = _PrintFatturaDataset.TBL_rate_bis.Rows.Count + 1;
                        dr_rat_bis.RAT_cod_mai = dr_mai.MAI_codice;
                        dr_rat_bis.RAT_num_rata = drtmp.campo03;
                        dr_rat_bis.RAT_scadenza = drtmp.campo04;
                        dr_rat_bis.RAT_importo = decimal.Parse(drtmp.campo05);
                        dr_rat_bis.RAT_ocr = drtmp.campo11;
                        dr_rat_bis.RAT_codice = _PrintFatturaDataset.TBL_rate_bis.Rows.Count + 1;
                        _PrintFatturaDataset.TBL_rate_bis.Rows.Add(dr_rat_bis);
                    }
                }
                dr_rat = (DS.DS_prn_fattura.TBL_rateRow)_PrintFatturaDataset.TBL_rate.NewRow();
                dr_rat.RAT_codice = _PrintFatturaDataset.TBL_rate.Rows.Count + 1;
                dr_rat.RAT_cod_mai = dr_mai.MAI_codice;
                dr_rat.RAT_num_rata = "1";
                dr_rat.RAT_scadenza = d30.campo04;
                dr_rat.RAT_importo = decimal.Parse(d20.campo13);
                dr_rat.RAT_ocr = d30.campo11;
                //dr_rat.RAT_r01_codice_ente = d50.campo14.Replace(" ", "");
                dr_rat.RAT_IUV = d30.campo12;
                dr_rat.RAT_num_avviso = d30.campo11;
                dr_rat.RAT_causale = "COSAP 2016"; // d40.campo05;
                ////switch (d40.campo05)
                ////{
                ////    case "PASSI CARRABILI":
                ////        break;
                ////    case "TARES/TARI":
                ////        break;
                ////    case "CANONE OCCUPAZIONE TEMPORANEA SPAZI AREE PUBBLICHE":
                ////        break;
                ////    case "IMPOSTA COMUNALE SULLA PUBBLICITA'":
                ////        dr_rat.RAT_causale = GetValueDatiFissi(PLG_costanti.DatiFissi_Bol896PA, PLG_costanti.KEY_causale_pub).PadRight(110, ' ').ToUpper();
                ////        break;
                ////    case "CANONE OCCUPAZIONE PERMANENTE SPAZI AREE PUBBLICHE":
                ////        dr_rat.RAT_causale = GetValueDatiFissi(PLG_costanti.DatiFissi_Bol896PA, PLG_costanti.KEY_causale_cosap_perm).PadRight(110, ' ').ToUpper();
                ////        break;
                ////    default:
                ////        dr_rat.RAT_causale = "";
                ////        break;
                ////}

                _PrintFatturaDataset.TBL_rate.Rows.Add(dr_rat);
            }
            if (dr_mai.MAI_tipo_tributo == 4)
            {
                foreach (DS.DS_import.TBL_tr_30Row drtmp in dsSrc.TBL_tr_30.Rows)
                {
                    if (drtmp.campo03 != "0")
                    {
                        dr_rat = (DS.DS_prn_fattura.TBL_rateRow)_PrintFatturaDataset.TBL_rate.NewRow();
                        dr_rat.RAT_codice = _PrintFatturaDataset.TBL_rate_bis.Rows.Count + 1;
                        dr_rat.RAT_cod_mai = dr_mai.MAI_codice;
                        dr_rat.RAT_num_rata = drtmp.campo03;
                        dr_rat.RAT_scadenza = drtmp.campo04;
                        dr_rat.RAT_importo = decimal.Parse(drtmp.campo05);
                        dr_rat.RAT_ocr = drtmp.campo11;
                        dr_rat.RAT_codice = _PrintFatturaDataset.TBL_rate.Rows.Count + 1;
                        _PrintFatturaDataset.TBL_rate.Rows.Add(dr_rat);
                    }
                }
            }
        }

        //
        // Preparazione del dataset per la stampa del Bollettino 896.
        //
        private void PreparaRecordPerStampaBoll896(int nCodice, DS.DS_import dsSrc, String cFeedString)
        {
            DS.DS_prn_bollettino.TBL_fattureRow dr_fat;
            DS.DS_prn_bollettino.TBL_rateRow dr_rat;
            DS.DS_prn_bollettino.TBL_fat_x_ratRow dr_fxr;
            DS.DS_import.TBL_tr_10Row d10;
            DS.DS_import.TBL_tr_20Row d20;
            //DS.DS_import.TBL_tr_30Row d30;
            DS.DS_import.TBL_tr_40Row d40;
            decimal nTmp;

            d10 = (DS.DS_import.TBL_tr_10Row)dsSrc.TBL_tr_10.Rows[0];
            d20 = (DS.DS_import.TBL_tr_20Row)dsSrc.TBL_tr_20.Rows[0];
            //d30 = (DS.DS_import.TBL_tr_30Row)dsSrc.TBL_tr_30.Rows[0];
            d40 = (DS.DS_import.TBL_tr_40Row)dsSrc.TBL_tr_40.Rows[0];

            dr_fat = (DS.DS_prn_bollettino.TBL_fattureRow)_PrintBollettinoDataset.TBL_fatture.NewRow();
            switch (d40.campo05)
            {
                case "PASSI CARRABILI":
                    dr_fat.FAT_tipo_tributo = 1;
                    break;
                case "TARES/TARI":
                    dr_fat.FAT_tipo_tributo = 2;
                    break;
                case "CANONE OCCUPAZIONE TEMPORANEA SPAZI AREE PUBBLICHE":
                    dr_fat.FAT_tipo_tributo = 3;
                    break;
                case "IMPOSTA COMUNALE SULLA PUBBLICITA'":
                    dr_fat.FAT_tipo_tributo = 4;
                    break;
                case "CANONE OCCUPAZIONE PERMANENTE SPAZI AREE PUBBLICHE":
                    dr_fat.FAT_tipo_tributo = 5;
                    break;
                default:
                    throw new Exception("Tipo documento pagamento non riconosciuto: " + d40.campo05 + ".");
            }
            dr_fat.FAT_codice = _PrintBollettinoDataset.TBL_fatture.Rows.Count + 1;
            dr_fat.FAT_prot_reg_iva = "";
            dr_fat.FAT_num_fattura = d20.campo05;
            dr_fat.FAT_data_fattura = d20.campo06;
            dr_fat.FAT_nominativo = d10.T10_nominativo;
            dr_fat.FAT_cod_utente = d10.T10_matricola;
            dr_fat.FAT_codice_fiscale = d10.campo07;
            dr_fat.FAT_partita_iva = d10.campo08;
            dr_fat.FAT_anno_fattura = d20.campo03;
            dr_fat.FAT_ind_rec_1 = d10.T10_res_indirizzo;
            dr_fat.FAT_ind_rec_2 = d10.T10_res_cap;
            dr_fat.FAT_ind_rec_3 = d10.T10_res_provincia;
            dr_fat.FAT_ind_rec_4 = d10.T10_res_comune;
            if ((dr_fat.FAT_codice_fiscale == dr_fat.FAT_partita_iva) && (dr_fat.FAT_codice_fiscale.Length == 11))
                dr_fat.FAT_codice_fiscale = "";
            _PrintBollettinoDataset.TBL_fatture.Rows.Add(dr_fat);

            foreach (DS.DS_import.TBL_tr_30Row drtmp in dsSrc.TBL_tr_30.Rows)
            {
                if (drtmp.campo03 != "0")
                {
                    dr_rat = (DS.DS_prn_bollettino.TBL_rateRow)_PrintBollettinoDataset.TBL_rate.NewRow();
                    dr_rat.RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1;
                    dr_rat.RAT_num_rata = drtmp.campo03;
                    dr_rat.RAT_scadenza = drtmp.campo04;
                    dr_rat.RAT_importo = decimal.Parse(drtmp.campo05);
                    dr_rat.RAT_IUV = drtmp.campo12;
                    dr_rat.RAT_num_avviso = drtmp.campo11;
                    nTmp = decimal.Parse(drtmp.campo06);

                    dr_rat.RAT_ocr = drtmp.campo06 + (nTmp % 93).ToString("00");
                    _PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat);

                    //
                    // Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
                    //
                    dr_fxr = (DS.DS_prn_bollettino.TBL_fat_x_ratRow)_PrintBollettinoDataset.TBL_fat_x_rat.NewRow();
                    dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1;
                    dr_fxr.DEF_alimimb = cFeedString;
                    dr_fxr.DEF_raccolta = 3;
                    dr_fxr.FXR_linkQD = nCodice;
                    dr_fxr.FXR_cod_fat = dr_fat.FAT_codice;
                    dr_fxr.FXR_cod_rat = dr_rat.RAT_codice;
                    _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr);
                }
            }
        }

        //
        // Preparazione del dataset per la stampa del Bollettino 896 della PA.
        //
        private void PreparaRecordPerStampaBoll896PA(int nCodice, DS.DS_import dsSrc, String cFeedString)
        {
            DS.DS_prn_bollettino.TBL_fattureRow dr_fat;
            DS.DS_prn_bollettino.TBL_rateRow dr_rat;
            DS.DS_prn_bollettino.TBL_fat_x_ratRow dr_fxr;
            DS.DS_import.TBL_tr_10Row d10;
            DS.DS_import.TBL_tr_20Row d20;
            DS.DS_import.TBL_tr_30Row d30;
            DS.DS_import.TBL_tr_40Row d40;
            //  DS.DS_import.TBL_tr_50Row d50;
            Decimal nTmp;

            d10 = (DS.DS_import.TBL_tr_10Row)dsSrc.TBL_tr_10.Rows[0];
            d20 = (DS.DS_import.TBL_tr_20Row)dsSrc.TBL_tr_20.Rows[0];
            d30 = null;
            foreach (DS.DS_import.TBL_tr_30Row dr_tmp in dsSrc.TBL_tr_30.Rows)
                if (dr_tmp.campo03 == "0")
                    d30 = dr_tmp;
            d40 = (DS.DS_import.TBL_tr_40Row)dsSrc.TBL_tr_40.Rows[0];
            //d50 = (DS.DS_import.TBL_tr_50Row)dsSrc.TBL_tr_50.Rows[0];

            dr_fat = (DS.DS_prn_bollettino.TBL_fattureRow)_PrintBollettinoDataset.TBL_fatture.NewRow();
            dr_fat.FAT_codice = _PrintBollettinoDataset.TBL_fatture.Rows.Count + 1;
            dr_fat.FAT_prot_reg_iva = "";
            dr_fat.FAT_num_fattura = d20.campo05;
            dr_fat.FAT_data_fattura = d20.campo06;
            dr_fat.FAT_nominativo = d10.T10_nominativo.PadRight(40, ' ');
            dr_fat.FAT_cod_utente = d10.T10_matricola;
            dr_fat.FAT_ind_rec_1 = d10.T10_res_indirizzo;
            dr_fat.FAT_ind_rec_2 = d10.T10_res_cap;
            dr_fat.FAT_ind_rec_3 = d10.T10_res_provincia;
            dr_fat.FAT_codice_fiscale = d10.campo07.PadRight(16, ' ');
            dr_fat.FAT_ind_rec_4 = d10.T10_res_comune;

            _PrintBollettinoDataset.TBL_fatture.Rows.Add(dr_fat);

            foreach (DS.DS_import.TBL_tr_30Row drtmp in dsSrc.TBL_tr_30.Rows)
            {
                if (((dsSrc.TBL_tr_30.Rows.Count > 2) && (drtmp.campo03 != "0")) || ((dsSrc.TBL_tr_30.Rows.Count == 2) && (drtmp.campo03 == "0")))
                {
                    dr_rat = (DS.DS_prn_bollettino.TBL_rateRow)_PrintBollettinoDataset.TBL_rate.NewRow();
                    dr_rat.RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1;
                    dr_rat.RAT_unica = dsSrc.TBL_tr_30.Rows.Count == 2;
                    dr_rat.RAT_num_rata = drtmp.campo03;
                    dr_rat.RAT_scadenza = drtmp.campo04;
                    dr_rat.RAT_IUV = drtmp.campo12;
                    dr_rat.RAT_num_avviso = drtmp.campo11;
                    dr_rat.RAT_importo = decimal.Parse(drtmp.campo05);
                    nTmp = decimal.Parse(drtmp.campo06);

                    dr_rat.RAT_ocr = drtmp.campo06 + (nTmp % 93).ToString("00");
                    _PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat);

                    //
                    // Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
                    //
                    dr_fxr = (DS.DS_prn_bollettino.TBL_fat_x_ratRow)_PrintBollettinoDataset.TBL_fat_x_rat.NewRow();
                    dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1;
                    dr_fxr.DEF_alimimb = cFeedString;
                    dr_fxr.DEF_raccolta = 3;
                    dr_fxr.FXR_linkQD = nCodice;
                    dr_fxr.FXR_cod_fat = dr_fat.FAT_codice;
                    dr_fxr.FXR_cod_rat = dr_rat.RAT_codice;
                    _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr);
                }
            }
            //dr_rat = (DS.DS_prn_bollettino.TBL_rateRow)_PrintBollettinoDataset.TBL_rate.NewRow();
            //dr_rat.RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1;
            //dr_rat.RAT_num_rata = "1";
            //dr_rat.RAT_scadenza = d30.campo04;
            //dr_rat.RAT_importo = decimal.Parse(d20.campo13);
            //dr_rat.RAT_ocr = d30.campo11;
            ////dr_rat.RAT_r01_codice_ente = d50.campo14.Replace(" ", "");
            //dr_rat.RAT_IUV = d30.campo12;
            //dr_rat.RAT_num_avviso = d30.campo11;
            //dr_rat.RAT_causale = "COSAP 2016"; // d40.campo05;
            ////switch (d40.campo05)
            ////{
            ////    case "PASSI CARRABILI":
            ////        break;
            ////    case "TARES/TARI":
            ////        break;
            ////    case "CANONE OCCUPAZIONE TEMPORANEA SPAZI AREE PUBBLICHE":
            ////        break;
            ////    case "IMPOSTA COMUNALE SULLA PUBBLICITA'":
            ////        dr_rat.RAT_causale = GetValueDatiFissi(PLG_costanti.DatiFissi_Bol896PA, PLG_costanti.KEY_causale_pub).PadRight(110, ' ').ToUpper();
            ////        break;
            ////    case "CANONE OCCUPAZIONE PERMANENTE SPAZI AREE PUBBLICHE":
            ////        dr_rat.RAT_causale = GetValueDatiFissi(PLG_costanti.DatiFissi_Bol896PA, PLG_costanti.KEY_causale_cosap_perm).PadRight(110, ' ').ToUpper();
            ////        break;
            ////    default:
            ////        dr_rat.RAT_causale = "";
            ////        break;
            ////}

            //_PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat);

            ////
            //// Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
            ////
            //dr_fxr = (DS.DS_prn_bollettino.TBL_fat_x_ratRow)_PrintBollettinoDataset.TBL_fat_x_rat.NewRow();
            //dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1;
            //dr_fxr.DEF_alimimb = cFeedString;
            //dr_fxr.DEF_raccolta = 3;
            //dr_fxr.FXR_linkQD = nCodice;
            //dr_fxr.FXR_cod_fat = dr_fat.FAT_codice;
            //dr_fxr.FXR_cod_rat = dr_rat.RAT_codice;
            //_PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr);
        }

        private string GetValueDatiFissi(string cTable, int nField)
        {
            XmlDocument xmlDF;
            XmlNode xmlTmp;
            string cRtn;

            try
            {
                xmlDF = new XmlDocument();
                xmlDF.Load(_SessioneDatiFissiFileName);

                xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", nField.ToString(), "]"));

                cRtn = xmlTmp.InnerText;
            }
            catch (Exception ex)
            {
                cRtn = "";
            }
            return cRtn;

        }

        //
        // Preparazione del dataset per la stampa del modello F24 Semplificato.
        //
        private void PreparaRecordPerStampaF24Semplificato(int nCodice, DS.DS_import dsSrc, String cFeedString)
        {
            DS.DS_prn_bollettino.TBL_fattureRow dr_fat;
            DS.DS_prn_bollettino.TBL_rateRow dr_rat;
            DS.DS_prn_bollettino.TBL_fat_x_ratRow dr_fxr;
            DS.DS_import.TBL_tr_10Row d10;
            DS.DS_import.TBL_tr_20Row d20;
            DS.DS_import.TBL_tr_30Row d30;
            DS.DS_import.TBL_tr_40Row d40;
            DS.DS_import.TBL_tr_50Row d50;

            d10 = (DS.DS_import.TBL_tr_10Row)dsSrc.TBL_tr_10.Rows[0];
            d20 = (DS.DS_import.TBL_tr_20Row)dsSrc.TBL_tr_20.Rows[0];
            d30 = null;
            foreach (DS.DS_import.TBL_tr_30Row dr_tmp in dsSrc.TBL_tr_30.Rows)
                if (dr_tmp.campo03 == "4")
                    d30 = dr_tmp;
            if (d30 == null)
                d30 = (DS.DS_import.TBL_tr_30Row)dsSrc.TBL_tr_30.Rows[0];
            d40 = (DS.DS_import.TBL_tr_40Row)dsSrc.TBL_tr_40.Rows[0];
            d50 = (DS.DS_import.TBL_tr_50Row)dsSrc.TBL_tr_50.Rows[0];

            dr_fat = (DS.DS_prn_bollettino.TBL_fattureRow)_PrintBollettinoDataset.TBL_fatture.NewRow();
            dr_fat.FAT_codice = _PrintBollettinoDataset.TBL_fatture.Rows.Count + 1;
            dr_fat.FAT_anno_fattura = d20.campo03;
            dr_fat.FAT_num_fattura = d20.campo05;
            if (d50.campo06 == "")
            {
                dr_fat.FAT_cognome = d50.campo04;
                dr_fat.FAT_nome = d50.campo05;
                dr_fat.FAT_codice_fiscale = d50.campo03;
                dr_fat.FAT_data_nascita = "";
                dr_fat.FAT_sesso = "";
                dr_fat.FAT_comune_nasc = "";
                dr_fat.FAT_prov_nasc = "";
            }
            else
            {
                dr_fat.FAT_cognome = d50.campo04; ;
                dr_fat.FAT_nome = d50.campo05;
                dr_fat.FAT_codice_fiscale = d50.campo03;
                dr_fat.FAT_data_nascita = d50.campo06;
                dr_fat.FAT_sesso = d50.campo07;
                dr_fat.FAT_comune_nasc = d50.campo08;
                dr_fat.FAT_prov_nasc = d50.campo09;
            }
            if (d20.campo13 != "")
                dr_fat.FAT_importo_totale = decimal.Parse(d20.campo13);
            else
                dr_fat.FAT_importo_totale = 0;
            dr_fat.FAT_importo_totale_zeroize = (dr_fat.FAT_importo_totale * 100).ToString().PadLeft(8, '0');
            _PrintBollettinoDataset.TBL_fatture.Rows.Add(dr_fat);

            dr_rat = (DS.DS_prn_bollettino.TBL_rateRow)_PrintBollettinoDataset.TBL_rate.NewRow();
            dr_rat.RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1;
            dr_rat.RAT_ocr = d50.campo11;
            dr_rat.RAT_ocr_gs1128 = d50.campo11;     //d20.campo23;
            dr_rat.RAT_scadenza = d30.campo04;

            //
            // Dati Riga 1
            //
            dr_rat.RAT_r01_sezione = d50.campo12.Replace(" ", "");
            dr_rat.RAT_r01_cod_tributo = d50.campo13;
            dr_rat.RAT_r01_codice_ente = d50.campo14.Replace(" ", "");
            dr_rat.RAT_r01_num_immobili = d50.campo15;
            dr_rat.RAT_r01_rateizzazione = d50.campo16;
            dr_rat.RAT_r01_anno_riferimento = d50.campo17;
            if (d50.campo58 != "")
                dr_rat.RAT_r01_detrazioni = decimal.Parse(d50.campo58);
            if (d50.campo18 != "")
                dr_rat.RAT_r01_importo = decimal.Parse(d50.campo18.Replace(" ", ","));
            if (d30 != null)
                dr_rat.RAT_r01_importo = decimal.Parse(d30.campo05.Replace(" ", ","));

            //
            // Dati Riga 2
            //
            if (d50.campo19 != "")
            {
                dr_rat.RAT_r02_sezione = d50.campo19.Replace(" ", "");
                dr_rat.RAT_r02_cod_tributo = d50.campo20;
                dr_rat.RAT_r02_codice_ente = d50.campo21.Replace(" ", "");
                dr_rat.RAT_r02_num_immobili = d50.campo22;
                dr_rat.RAT_r02_rateizzazione = d50.campo23;
                dr_rat.RAT_r02_anno_riferimento = d50.campo24;
                if (d50.campo59 != "")
                    dr_rat.RAT_r02_detrazioni = decimal.Parse(d50.campo59);
                if (d50.campo25 != "")
                    dr_rat.RAT_r02_importo = decimal.Parse(d50.campo25.Replace(" ", ","));

            }

            //
            // Dati Riga 3
            //
            if (d50.campo27 != "")
            {
                dr_rat.RAT_r03_sezione = d50.campo27.Replace(" ", "");
                dr_rat.RAT_r03_cod_tributo = d50.campo28;
                dr_rat.RAT_r03_codice_ente = d50.campo29.Replace(" ", "");
                dr_rat.RAT_r03_num_immobili = d50.campo30;
                dr_rat.RAT_r03_rateizzazione = d50.campo31;
                dr_rat.RAT_r03_anno_riferimento = d50.campo32;
                if (d50.campo60 != "")
                    dr_rat.RAT_r03_detrazioni = decimal.Parse(d50.campo60);
                if (d50.campo33 != "")
                    dr_rat.RAT_r03_importo = decimal.Parse(d50.campo33.Replace(" ", ","));
            }

            //
            // Dati Riga 4
            //
            if (d50.campo34 != "")
            {
                dr_rat.RAT_r04_sezione = d50.campo34.Replace(" ", "");
                dr_rat.RAT_r04_cod_tributo = d50.campo35;
                dr_rat.RAT_r04_codice_ente = d50.campo36.Replace(" ", "");
                dr_rat.RAT_r04_num_immobili = d50.campo37;
                dr_rat.RAT_r04_rateizzazione = d50.campo38;
                dr_rat.RAT_r04_anno_riferimento = d50.campo39;
                if (d50.campo61 != "")
                    dr_rat.RAT_r04_detrazioni = decimal.Parse(d50.campo61);
                if (d50.campo40 != "")
                    dr_rat.RAT_r04_importo = decimal.Parse(d50.campo40.Replace(" ", ","));
            }

            //
            // Dati Riga 5
            //
            if (d50.campo41 != "")
            {
                dr_rat.RAT_r05_sezione = d50.campo41.Replace(" ", "");
                dr_rat.RAT_r05_cod_tributo = d50.campo42;
                dr_rat.RAT_r05_codice_ente = d50.campo43.Replace(" ", "");
                dr_rat.RAT_r05_num_immobili = d50.campo44;
                dr_rat.RAT_r05_rateizzazione = d50.campo45;
                dr_rat.RAT_r05_anno_riferimento = d50.campo46;
                if (d50.campo62 != "")
                    dr_rat.RAT_r05_detrazioni = decimal.Parse(d50.campo62);
                if (d50.campo47 != "")
                    dr_rat.RAT_r05_importo = decimal.Parse(d50.campo47.Replace(" ", ","));
            }

            //
            // Totale Generale
            //
            dr_rat.RAT_importo = decimal.Parse(d50.campo26.Replace(" ", ","));
            dr_rat.RAT_importo = decimal.Parse(d30.campo05.Replace(" ", ","));
            dr_rat.RAT_importo_totale_zeroize = decimal.Truncate((dr_rat.RAT_importo * 100)).ToString().PadLeft(8, '0');
            _PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat);

            //' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
            dr_fxr = (DS.DS_prn_bollettino.TBL_fat_x_ratRow)_PrintBollettinoDataset.TBL_fat_x_rat.NewRow();
            dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1;
            dr_fxr.DEF_alimimb = cFeedString;
            dr_fxr.DEF_raccolta = 3;

            dr_fxr.FXR_linkQD = nCodice;
            dr_fxr.FXR_cod_fat = dr_fat.FAT_codice;
            dr_fxr.FXR_cod_rat = dr_rat.RAT_codice;
            dr_fxr.FXR_cod_fis_identificativo = d10.T10_ID_documento;
            _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr);
        }

        private string GetFeedString(DS.DS_quick_db.TBL_mainRow dr_mai_qd)
        {
            Boolean lAllModuli = true;
            String cRtn;

            cRtn = "";
            if (_FeedString != "")
            {
                for (int i = 1; i <= _FeedString.Split(';').Length - 1; i = i + 2)
                    if (Boolean.Parse(dr_mai_qd[_FeedString.Split(';')[i]].ToString()))
                        cRtn = _FeedString.Split(';')[i + 1];
                    else
                        lAllModuli = false;

                if (lAllModuli)
                    cRtn = _FeedString.Split(';')[0];
            }
            return cRtn;
        }

        private string RiduciLunghezzaStringa(string cValue, int nLunghezza)
        {
            if (cValue.Length > nLunghezza)
                cValue = cValue.Substring(0, nLunghezza);
            return cValue;
        }
    }
}

