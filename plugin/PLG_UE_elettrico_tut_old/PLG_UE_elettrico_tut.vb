Imports System.Windows.Forms
Imports TipiComuni

Public Class PLG_UE_elettrico_tut
    Implements PLUGIN_interfaceV1_3.IPLG_dati_lib

    Const UpperFileSize = 3000000
    Const LowerFileSize = 2000000

    Const cns_tsm_sel_con_bollettino = "TSM_sel_con_bollettino"
    Const cns_tsm_sel_con_allegato = "TSM_sel_con_allegato"

    ' Costante indicante il corretto registro IVA della parte UmbriaEnergy Elettricit�
    Const cns_prot_reg_iva_corr = "UEV"

    Private _CodiceProgressivo As Integer
    Private _SessioneDati As Xml.XmlNode
    Private _SessioneTrasformazioniFileName As String
    Private _SessioneNuova As Boolean
    Private _SessionePath As String
    Private _CodiceModulo As Integer
    Private _FeedString As String
    Private _QuickDataset As DS_QD_UE_ele
    Private _PrintFatturaDataset As DS_PF_UE_ele
    Private _PrintBollettinoDataset As DS_PB_UE_ele
    Private _RecordCount As Integer
    Private _xmlDati_qd As Xml.XmlDocument
    Private _MessaggiAvanzamento(5) As String
    Private _ErroriLievi As String
    Private _SessioneDatiFissiFileName As String
    Private _SessioneDatiPath As String
    Private _LogDS As DataSet
    Private _ResultAL As ArrayList
    Private _MasterRecord As Integer
    Private _NomeOfferta As String
    Private _TmpDistributore As String
    Private data_import As DS_import

    Enum ListaModuli
        MOD_quickdataset = 0
        MOD_fattura = 1
        MOD_bollettino = 2
    End Enum

#Region "Elenco delle property del tipo READONLY public"

    ReadOnly Property AlertPlugIn_PRN() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.AlertPlugIn_PRN

        Get
            Return String.Concat("Attenzione!! Ricordiamo che prima di procedere con la stampa dei moduli selezionati", vbCr, "� necessario verificare i seguenti dati:", vbCr, "- Periodo di riferimento (Bimestre);", vbCr, "- Testo dell'autolettura.", vbLf, vbLf, "Posso procedere con la stampa di quanto selezionato?")
        End Get

    End Property

    ReadOnly Property AlertPlugIn_SND() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.AlertPlugIn_SND

        Get
            Return String.Concat("Da modificare!!!!!!!!!!!!!!!!!")
        End Get

    End Property

    ReadOnly Property DataSendByMail() As Xml.XmlNode Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.DataSendByMail

        Get
            Return GetDataSendByMail()
        End Get

    End Property

    ReadOnly Property LinkField_QuickDS_PrintDS() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.LinkField_QuickDS_PrintDS

        Get
            Select Case _CodiceModulo
                Case Is = ListaModuli.MOD_fattura
                    Return "MAI_linkQD"
                Case Is = ListaModuli.MOD_bollettino
                    Return "FXR_linkQD"
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.ListaControlliFormNuovaSessione

        Get
            Dim aCtr As ArrayList

            aCtr = New ArrayList
            aCtr.Add(New String() {"LBL_input_1", "File fatturazione......:", "3;16", "155;13"})
            aCtr.Add(New String() {"TXB_input_1", "&1", "160;12", "610;20", "199"})
            aCtr.Add(New String() {"BTN_fil_input_1", "", "775;10", "27;23"})
            aCtr.Add(New String() {"LBL_input_2", "File trasparenza......:", "3;42", "155;13"})
            aCtr.Add(New String() {"TXB_input_2", "&1", "160;38", "610;20", "198"})
            aCtr.Add(New String() {"BTN_fil_input_2", "", "775;36", "27;23"})
            aCtr.Add(New String() {"LBL_input_3", "File credenziali sito web ...:", "3;68", "155;13"})
            aCtr.Add(New String() {"TXB_input_3", "&1", "160;64", "610;20", "102"})
            aCtr.Add(New String() {"BTN_fil_input_3", "", "775;62", "27;23"})
            aCtr.Add(New String() {"LBL_output_998", "Cartella di destinazione .:", "3;94", "155;13"})
            aCtr.Add(New String() {"TXB_output_998", "&998", "160;90", "610;20", "201"})
            aCtr.Add(New String() {"BTN_fld_output_998", "", "775;88", "27;23"})
            aCtr.Add(New String() {"LBL_output_999", "Descrizione archivio ......:", "3;120", "155;13"})
            aCtr.Add(New String() {"TXB_output_999", "", "160;116", "M;640;122", "202"})
            Return aCtr
        End Get

    End Property

    ReadOnly Property NomePlugIn() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.NomePlugIn

        Get
            Return "Plug-in per la stampa delle bollette di UmbriaEnergy dell'elettrico del mercato Tutelato."
        End Get

    End Property

    ReadOnly Property PrintDataset() As DataSet Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.PrintDataset

        Get
            Select Case _CodiceModulo
                Case Is = ListaModuli.MOD_fattura
                    Return _PrintFatturaDataset
                Case Is = ListaModuli.MOD_bollettino
                    Return _PrintBollettinoDataset
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    ReadOnly Property PrintDataSet_MainTableName() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.PrintDataSet_MainTableName

        Get
            Select Case _CodiceModulo
                Case Is = ListaModuli.MOD_fattura
                    Return "TBL_main"
                Case Is = ListaModuli.MOD_bollettino
                    Return "TBL_fat_x_rat"
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    ReadOnly Property QuickDataset() As DataSet Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.QuickDataset

        Get
            Return _QuickDataset
        End Get

    End Property

    ReadOnly Property QuickDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.QuickDataset_MainTableKey

        Get
            Return "MAI_codice"
        End Get

    End Property

    ReadOnly Property QuickDataset_MainTableName() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.QuickDataset_MainTableName

        Get
            Return "TBL_main"
        End Get

    End Property

    ReadOnly Property ResultAL() As ArrayList Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.ResultAL

        Get
            Return _ResultAL
        End Get

    End Property

    ReadOnly Property VersionePlugIn() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.VersionePlugIn

        Get
            Return "1.3.0"
        End Get

    End Property

#End Region

#Region "Elenco delle property del tipo READONLY Private"

    Private ReadOnly Property PrintDataSet_DataFile(ByVal _FileIdx As Integer)

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode(String.Concat("filename[@codice=", "", _FileIdx, "", "]")).InnerText)
        End Get

    End Property

    Private ReadOnly Property QuickDataSet_IndexFile() As String

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode("filename[@tipo=""0""]").InnerText)
        End Get

    End Property

#End Region

#Region "Elenco delle property del tipo WRITEONLY"

    WriteOnly Property CodiceModulo() As Integer Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.CodiceModulo

        Set(ByVal value As Integer)
            _CodiceModulo = value
        End Set

    End Property

    WriteOnly Property FeedString() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.FeedString

        Set(ByVal value As String)
            _FeedString = value
        End Set

    End Property

    WriteOnly Property SessioneDatiPath() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.SessioneDatiPath

        Set(ByVal value As String)
            _SessioneDatiPath = value
        End Set

    End Property

    WriteOnly Property SessioneTrasformazioniFileName() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.SessioneTrasformazioniFileName

        Set(ByVal value As String)
            _SessioneTrasformazioniFileName = value
        End Set

    End Property

    WriteOnly Property SessioneDatiFissiFileName() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.SessioneDatiFissiFileName

        Set(ByVal value As String)
            _SessioneDatiFissiFileName = value
        End Set

    End Property

    WriteOnly Property SessioneFld() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.SessioneFld

        Set(ByVal value As String)
            _SessionePath = value
        End Set

    End Property

    WriteOnly Property SezioneDati() As Xml.XmlNode Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.SezioneDati

        Set(ByVal value As Xml.XmlNode)
            _SessioneDati = value
        End Set

    End Property

#End Region

#Region "Elenco delle property del tipo READ e WRITE"

    Property MessaggiAvanzamento() As String() Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.MessaggiAvanzamento

        Get
            Return _MessaggiAvanzamento
        End Get
        Set(ByVal value As String())
            _MessaggiAvanzamento = value
        End Set

    End Property

#End Region

#Region "Elenco dei metodi Pubblici"

#End Region

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.CheckDatiSessione
        Dim nObb As Integer
        Dim lObb As Boolean
        Dim l102 As Boolean
        Dim lRtn As Boolean

        nObb = 0
        l102 = False
        For Each aValue As String() In aDati
            Select Case aValue(0)
                Case Is = 199, 198
                    nObb += 1
                Case Is = 102
                    l102 = True
            End Select
        Next
        lObb = nObb = 2
        lRtn = True
        If _SessioneNuova Then
            If Not lObb Then
                MessageBox.Show("Attenzione! Non sono � stato indicato nessun file contenente i dati. Non posso procedere con la creazione della sessione.")
                lRtn = False
            ElseIf Not l102 Then
                lRtn = MessageBox.Show("Attenzione! Non � stato indicato un file valido per le credenziali Web. Procedo ugualmente con la creazione della sessione?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes
            End If
        Else
            If Not l102 And lRtn Then
                lRtn = MessageBox.Show("Attenzione! Non � stato indicato un file valido per le credenziali Web. Procedo ugualmente con il caricamento della sessione?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes
            End If
        End If
        Return lRtn

    End Function

    Private Sub SetRecordIDX(ByVal xmlIndice As Xml.XmlDocument, ByVal xmlRecord As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xmlIndice.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xmlRecord.AppendChild(xiTmp)

    End Sub

    Property CurrentRecordNumber() As Integer Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.CurrentRecordNumber

        Get
            Return _RecordCount
        End Get
        Set(ByVal value As Integer)
            _RecordCount = value
        End Set

    End Property

    Sub GoToRecordNumber(ByVal nRecord As Integer) Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.GoToRecordNumber

        _RecordCount = nRecord

    End Sub

#Region "SetUp per Datagrid di visualizzazione delle righe da stampare"

    Sub ImpostaDGVDati(ByVal DGV As DataGridView) Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.ImpostaDGVDati

        With DGV.Columns
            .Clear()
            .Add(Col_DaStampare)
            .Add(Col_Errore_code)
            .Add(Col_Errore_desc)
            .Add(Col_Progressivo)
            .Add(Col_NumeroUtente)
            .Add(Col_NumeroFattura)
            .Add(Col_Nominativo)
            .Add(COL_Indirizzo_Recapito)
            .Add(COL_CAP_Recapito)
            .Add(COL_Citta_Recapito)
            .Add(COL_Provincia_Recapito)
            .Add(COL_Nazione_Recapito)
            .Add(COL_Print_Bollettino)
            .Add(COL_Print_allegato)
            .Add(COL_SpedByMail)
        End With

    End Sub

    Private Function Col_DaStampare() As System.Windows.Forms.DataGridViewCheckBoxColumn
        Dim col As System.Windows.Forms.DataGridViewCheckBoxColumn

        col = New System.Windows.Forms.DataGridViewCheckBoxColumn
        With col
            .HeaderText = "Stampa"
            .DataPropertyName = "DEF_toprint"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .Width = 50
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Errore_code() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errcode"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Errore_desc() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errdesc"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Progressivo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Progressivo"
            .DataPropertyName = "MAI_codice"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 70
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroUtente() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Utente"
            .DataPropertyName = "MAI_num_utente"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 90
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroFattura() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Fattura"
            .DataPropertyName = "MAI_num_fattura"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 110
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Nominativo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nominativo"
            .DataPropertyName = "MAI_nominativo"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Indirizzo_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Indirizzo Recapito"
            .DataPropertyName = "MAI_indirizzo_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_CAP_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "CAP"
            .DataPropertyName = "MAI_cap_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Citta_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Citt�"
            .DataPropertyName = "MAI_citta_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Provincia_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Prov."
            .DataPropertyName = "MAI_provincia_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Nazione_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nazione"
            .DataPropertyName = "MAI_nazione_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Print_Bollettino()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Bollettino"
            .DataPropertyName = "MAI_print_bol"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_Print_allegato()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Allegato"
            .DataPropertyName = "MAI_print_all"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_SpedByMail()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Sped. Email"
            .DataPropertyName = "DEF_sendbymail"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

#End Region

    Private Function RecuperaPeriodoRiferimento(ByVal xiWork As Xml.XmlNode) As String
        Dim dTmp1 As DateTime
        Dim dTmp2 As DateTime
        Dim cRtn As String

        cRtn = ""
        dTmp1 = GetValueFromXML(xiWork, Costanti.itm_data_ini_fat)
        dTmp2 = GetValueFromXML(xiWork, Costanti.itm_data_fin_fat)
        If dTmp1.Year = dTmp2.Year And dTmp1.Month = dTmp2.Month Then
            cRtn = String.Concat(TrasformaMese(dTmp1.Month), " ", dTmp1.Year)
        Else
            If dTmp1.Year = dTmp2.Year Then
                cRtn = String.Concat(TrasformaMese(dTmp1.Month), " ", TrasformaMese(dTmp2.Month), " ", dTmp1.Year)
            Else
                cRtn = String.Concat(TrasformaMese(dTmp1.Month), " ", dTmp1.Year, " ", TrasformaMese(dTmp2.Month), " ", dTmp2.Year)
            End If
        End If
        Return cRtn

    End Function

    Private Function TrasformaMese(ByVal nMese As Integer) As String
        Dim cvalue As String

        Select Case nMese
            Case Is = 1
                cvalue = "gennaio"
            Case Is = 2
                cvalue = "febbraio"
            Case Is = 3
                cvalue = "marzo"
            Case Is = 4
                cvalue = "aprile"
            Case Is = 5
                cvalue = "maggio"
            Case Is = 6
                cvalue = "giugno"
            Case Is = 7
                cvalue = "luglio"
            Case Is = 8
                cvalue = "agosto"
            Case Is = 9
                cvalue = "settembre"
            Case Is = 10
                cvalue = "ottobre"
            Case Is = 11
                cvalue = "novembre"
            Case Is = 12
                cvalue = "dicembre"
            Case Else
                cvalue = ""
        End Select
        Return cvalue

    End Function

    Function ElementiMenuDedicati() As ArrayList Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.ElementiMenuDedicati
        Dim SubMenuEntry As ToolStripMenuItem
        Dim MenuEntry As ToolStripMenuItem
        Dim aMenuItem As ArrayList

        aMenuItem = New ArrayList
        MenuEntry = New ToolStripMenuItem("Stampa Elettrico Tutelato")

        'SubMenuEntry = New ToolStripMenuItem("Bimestre di fatturazione")
        'AddHandler SubMenuEntry.Click, AddressOf BimestreFatturazione_click
        'MenuEntry.DropDownItems.Add(SubMenuEntry)

        'SubMenuEntry = New ToolStripMenuItem("Forza data scadenza")
        'AddHandler SubMenuEntry.Click, AddressOf ForzaDataScadenza_click
        'MenuEntry.DropDownItems.Add(SubMenuEntry)

        SubMenuEntry = New ToolStripMenuItem("Codice del Gruppo")
        AddHandler SubMenuEntry.Click, AddressOf CodiceGruppo_click
        MenuEntry.DropDownItems.Add(SubMenuEntry)

        SubMenuEntry = New ToolStripMenuItem("Testo autolettura")
        AddHandler SubMenuEntry.Click, AddressOf TestoAutolettura_click
        MenuEntry.DropDownItems.Add(SubMenuEntry)

        SubMenuEntry = New ToolStripMenuItem("Stampa autolettura")
        SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa).ToLower = "si"
        AddHandler SubMenuEntry.Click, AddressOf StampaAutolettura_click
        MenuEntry.DropDownItems.Add(SubMenuEntry)

        SubMenuEntry = New ToolStripMenuItem("Testo Bonus Sociale")
        AddHandler SubMenuEntry.Click, AddressOf TestoBonusSociale_click
        MenuEntry.DropDownItems.Add(SubMenuEntry)

        SubMenuEntry = New ToolStripMenuItem("Stampa Bonus Sociale")
        SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_stampa).ToLower = "si"
        AddHandler SubMenuEntry.Click, AddressOf StampaBonusSociale_click
        MenuEntry.DropDownItems.Add(SubMenuEntry)

        'SubMenuEntry = New ToolStripMenuItem("Stampa Box Commerciale")
        'SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale).ToLower = "si"
        'AddHandler SubMenuEntry.Click, AddressOf StampaBoxCommerciale_click
        'MenuEntry.DropDownItems.Add(SubMenuEntry)

        ''SubMenuEntry = New ToolStripMenuItem("Forza offerta scacciapensieri")
        ''SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si"
        ''AddHandler SubMenuEntry.Click, AddressOf ForzaOffertaScacciapensieri_TSM_click
        ''MenuEntry.DropDownItems.Add(SubMenuEntry)
        'MenuEntry.DropDownItems.Add(New ToolStripSeparator())

        'SubMenuEntry = New ToolStripMenuItem("Dati Fissi")
        'AddHandler SubMenuEntry.Click, AddressOf DatiFissi_click
        'MenuEntry.DropDownItems.Add(SubMenuEntry)

        'MenuEntry.DropDownItems.Add(New ToolStripSeparator())

        '' Imposta tutti i record con allegato
        'SubMenuEntry = New ToolStripMenuItem("Allegato su tutti")
        'AddHandler SubMenuEntry.Click, AddressOf AllegatiAll_click
        'MenuEntry.DropDownItems.Add(SubMenuEntry)

        '' Imposta nessun record con allrgato
        'SubMenuEntry = New ToolStripMenuItem("Allegato su Nessuno")
        'AddHandler SubMenuEntry.Click, AddressOf AllegatiNot_click
        'MenuEntry.DropDownItems.Add(SubMenuEntry)

        aMenuItem.Add(New Object() {"n", "TSM_sessione_stampa", MenuEntry})

        'aMenuItem.Add(New Object() {"a", "TSM_selezione", New ToolStripSeparator()})

        '' Seleziona tutti i record con bollettino
        'SubMenuEntry = New ToolStripMenuItem("Con bollettino")
        'SubMenuEntry.Name = cns_tsm_sel_con_bollettino
        'aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

        '' Seleziona tutti i record con allegato
        'SubMenuEntry = New ToolStripMenuItem("Con allegato")
        'SubMenuEntry.Name = cns_tsm_sel_con_allegato
        'aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

        'tsb = New System.Windows.Forms.ToolStripButton
        'tsb.AutoSize = True
        'tsb.ImageTransparentColor = System.Drawing.Color.Magenta
        'AddHandler tsb.Click, AddressOf ForzaOffertaScacciapensieri_TSB_click
        'tsb.Name = "TSB_forza_scacciapensieri"
        'tsb.Text = "Forzatura scacciapensieri"
        'SetButtonForzaturaScacciapensieri(tsb)

        'aMenuItem.Add(New Object() {"a", "TOS_menu", tsb})
        Return aMenuItem

    End Function

    Private Sub BimestreFatturazione_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As FRM_bimestre

        Try
            frm = New FRM_bimestre
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_bimestre.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "14")
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, "14", frm.TXB_bimestre.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il valore richiesto.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    ' ************************************************************************** '
    ' Imposto correttamente la parte di selezione per la stampa dei record. Se   '
    ' non dobbiamo mantenere la selezione o se la dobbiamo mantenere ma il re-   ' 
    ' cord non � stampabile metto a "NON STAMPABILE (False)" il campo .          '
    ' ************************************************************************** '
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String) Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.SetSelectable
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_UE_ele.TBL_mainRow)
        dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
        If cFilter > "" Then
            dr_mai_qd.DEF_selectable = dr_mai_qd.DEF_selectable And dr_mai_qd(cFilter)
        End If

        ' ************************************************************************** '
        ' Pulisce i campi relativi agli errori in quanto ci stiamo occupando di un   '
        ' nuovo modulo.                                                              '
        ' ************************************************************************** '
        If dr_mai_qd.DEF_selectable Then
            dr_mai_qd.Item("DEF_errcode") = 0
            dr_mai_qd.Item("DEF_errdesc") = ""
        End If

    End Sub

    Private Function GetValueDatiFissi(ByVal cTable As String, ByVal cField As String) As String
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim cRtn As String

        Try
            xmlDF.Load(_SessioneDatiFissiFileName)
            xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))

            cRtn = xmlTmp.InnerText
            cRtn = cRtn.Replace("{\TBL_main;MAI_punto_fornitura/}", "&PUNTO_FORNITURA&")
            cRtn = cRtn.Replace("{\TBL_strum_misura;STM_matricola/}", "&MATRICOLA_CONT&")
            xmlDF = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    Private Sub SetValueDatiFissi(ByVal cTable As String, ByVal cField As String, ByVal cValue As String)
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim xaTmp As Xml.XmlAttribute
        Dim xnWork2 As Xml.XmlNode

        cValue = cValue.Replace("&PUNTO_FORNITURA&", "{\TBL_main;MAI_punto_fornitura/}")
        cValue = cValue.Replace("&MATRICOLA_CONT&", "{\TBL_strum_misura;STM_matricola/}")

        xmlDF.Load(_SessioneDatiFissiFileName)
        xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))
        If Not xmlTmp Is Nothing Then
            xmlTmp = xmlTmp.SelectSingleNode("valore")
            xmlTmp.InnerText = cValue
        Else
            'Crea il nodo
            xmlTmp = xmlDF.CreateElement("item")
            xaTmp = xmlDF.CreateAttribute("codice")
            xaTmp.Value = cField
            xmlTmp.Attributes.Append(xaTmp)
            xnWork2 = xmlDF.CreateElement("valore")
            xnWork2.InnerText = cValue
            xmlTmp.AppendChild(xnWork2)
            xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable)).AppendChild(xmlTmp)
        End If
        xmlDF.Save(_SessioneDatiFissiFileName)
        xmlDF = Nothing

    End Sub

    Private Sub AllegatiNot_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        For Each dr_mai_qd As DS_QD_UE_ele.TBL_mainRow In QuickDataset.Tables(0).Rows
            dr_mai_qd.Item("MAI_print_all") = False
        Next

    End Sub

    Private Sub AllegatiAll_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        For Each dr_mai_qd As DS_QD_UE_ele.TBL_mainRow In QuickDataset.Tables(0).Rows   '   _QuickDataset.Tables(PlugInMainTable(ListaModuli.MOD_quickdataset)).Rows
            dr_mai_qd.Item("MAI_print_all") = True
        Next

    End Sub

    Private Sub SetAllegato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New FRM_set_allegato

        frm.StartPosition = FormStartPosition.CenterParent
        frm.Show()

    End Sub

    Private Sub StampaAutolettura_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
        If CType(sender, ToolStripMenuItem).Checked Then
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa, "si")
        Else
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa, "no")
        End If

    End Sub

    Private Sub StampaBoxCommerciale_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
        If CType(sender, ToolStripMenuItem).Checked Then
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale, "si")
        Else
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale, "no")
        End If

    End Sub

    Private Sub ForzaOffertaScacciapensieri_TSM_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
        If CType(sender, ToolStripMenuItem).Checked Then
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "si")
        Else
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "no")
        End If

    End Sub

    Private Sub ForzaOffertaScacciapensieri_TSB_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si" Then
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "no")
        Else
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri, "si")
        End If
        SetButtonForzaturaScacciapensieri(sender)

    End Sub

    Private Sub SetButtonForzaturaScacciapensieri(ByVal tsb As ToolStripButton)

        If tsb.Text.EndsWith(" Si") Then
            tsb.Text = tsb.Text.Replace(" Si", "")
        ElseIf tsb.Text.EndsWith(" No") Then
            tsb.Text = tsb.Text.Replace(" No", "")
        End If
        If GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si" Then
            tsb.Image = Global.PLG_UE_elettrico_tut.My.Resources.bullet_green
            tsb.Text = String.Concat(tsb.Text, " Si")
        Else
            tsb.Image = Global.PLG_UE_elettrico_tut.My.Resources.bullet_red
            tsb.Text = String.Concat(tsb.Text, " No")
        End If

    End Sub

    Private Sub StampaBonusSociale_click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        CType(sender, ToolStripMenuItem).Checked = Not CType(sender, ToolStripMenuItem).Checked
        If CType(sender, ToolStripMenuItem).Checked Then
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_stampa, "si")
        Else
            SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_stampa, "no")
        End If

    End Sub

    Private Sub CodiceGruppo_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_gruppo

        Try
            frm = New FRM_gruppo
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_gruppo.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_gruppo)
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_gruppo, frm.TXB_gruppo.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo dell'autolettura", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TestoAutolettura_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_autolettura

        Try
            frm = New FRM_autolettura
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_autolettura.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_testo)
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_testo, frm.TXB_autolettura.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo dell'autolettura", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TestoBonusSociale_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_bonus_soc

        Try
            frm = New FRM_bonus_soc
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_bonus_sociale.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_testo)
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_testo, frm.TXB_bonus_sociale.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo del bonus sociale", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ForzaDataScadenza_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_forza_scad

        Try
            frm = New FRM_forza_scad
            frm.StartPosition = FormStartPosition.CenterParent
            frm.Condizione = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "17")
            frm.TXB_data_scad.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "16")
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, "16", frm.TXB_data_scad.Text)
                SetValueDatiFissi(Costanti.DatiFissi_Fattura, "17", frm.Condizione)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare i dati per la forzatura della data di scadenza", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub DatiFissi_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As FRM_datifissi

        Try
            frm = New FRM_datifissi
            frm.StartPosition = FormStartPosition.CenterParent
            frm.FileNameDatiFissi = _SessioneDatiFissiFileName
            If frm.ShowDialog = DialogResult.OK Then

            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il valore richiesto.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub RemovePRNDSErrorRecord() Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.RemovePRNDSErrorRecord
        Dim nLinkCode As Integer

        Select Case _CodiceModulo
            Case Is = ListaModuli.MOD_fattura
                For Each dr_mai_pd As DS_PF_UE_ele.TBL_mainRow In _PrintFatturaDataset.TBL_main.Rows
                    nLinkCode = dr_mai_pd(LinkField_QuickDS_PrintDS)
                    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                        dr_mai_pd.Delete()
                    End If
                Next
                _PrintFatturaDataset.TBL_main.AcceptChanges()
            Case Is = ListaModuli.MOD_bollettino
                For Each dr_mai_pd As DS_PB_UE_ele.TBL_fat_x_ratRow In _PrintBollettinoDataset.TBL_fat_x_rat.Rows
                    nLinkCode = dr_mai_pd(LinkField_QuickDS_PrintDS)
                    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                        dr_mai_pd.Delete()
                    End If
                Next
                _PrintBollettinoDataset.TBL_fat_x_rat.AcceptChanges()
        End Select

    End Sub

    Property LogDS() As DataSet Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.LogDSStampa

        Get
            LoadDSLog()
            Return _LogDS
        End Get
        Set(ByVal value As DataSet)
            _LogDS = value
        End Set

    End Property

    Private Sub LoadDSLog()
        Dim dr As DataRow

        For Each dr_qds As DS_QD_UE_ele.TBL_mainRow In _QuickDataset.TBL_main.Rows
            If dr_qds.DEF_errcode < 0 Then
                dr = _LogDS.Tables(0).NewRow
                dr("LOG_desc_rec_1") = String.Concat("Progressivo: ", dr_qds.MAI_codice, " Nominativo: ", dr_qds.MAI_nominativo, " Numero Fattura: ", dr_qds.MAI_num_fattura)
                dr("LOG_stato") = dr_qds.DEF_errcode
                dr("LOG_desc_err") = dr_qds.DEF_errdesc
                _LogDS.Tables(0).Rows.Add(dr)
            End If
        Next

    End Sub

    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String) Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.SetPrintable
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_UE_ele.TBL_mainRow)
        Select Case _SelectFromPlugInType
            Case Is = cns_tsm_sel_con_bollettino.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_bol
            Case Is = cns_tsm_sel_con_allegato.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_all
        End Select

    End Sub

    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String() Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.GetSelectedRow
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow
        Dim aResult As String()
        Dim cFilter As String

        Select Case _SelectFromPlugInType
            Case Is = "tsm_ric_progressivo"
                cFilter = "MAI_codice"
            Case Else
                cFilter = ""
        End Select
        aResult = New String() {"MAI_codice", ""}
        For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
            If dr_mai_qd(cFilter) = cValue Then
                aResult(1) = dr_mai_qd.MAI_codice
            End If
        Next
        Return aResult

    End Function

    WriteOnly Property FiltroSelezione() As String Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.FiltroSelezione

        Set(ByVal value As String)

        End Set

    End Property

    Public Sub ClearPrintDataset() Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.ClearPrintDataset

        Select Case _CodiceModulo
            Case Is = ListaModuli.MOD_fattura
                _PrintFatturaDataset = Nothing
            Case Is = ListaModuli.MOD_bollettino
                _PrintBollettinoDataset = Nothing
        End Select

    End Sub

#Region "Procedure utili per la creazione della sessione di stampa"

    Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList) Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.CreaDataBaseSessione
        Dim sr As System.IO.StreamReader
        Dim xdWork As Xml.XmlDocument
        Dim xnWork2 As Xml.XmlNode
        Dim aBlocchi As String()
        'Dim nCharRead As Integer
        Dim cFileName As String
        Dim nRecord As Integer
        Dim nBlocco As String
        Dim cBlocco As String
        Dim chTmp(0) As Char
        Dim cTmp As String
        Dim nLine As Integer
        Dim cLine As String
        'Dim cConnACCStr As String
        'Dim cDBFileName As String
        'Dim oComBuil As OleDb.OleDbCommandBuilder
        'Dim oDa As OleDb.OleDbDataAdapter
        'Dim oCmd As OleDb.OleDbCommand
        Dim dr_tra As DS_import.TBL_trasparenzaRow
        'Dim i As Integer
        Dim aTemp As String()
        Dim aHeader As String()
        Dim cHeader As String
        Dim nSezione As Integer
        Dim nTmp As Integer
        Dim nCodFatt As Integer
        Dim dr_dfa As DS_import.TBL_dati_fattRow
        Dim cBolletta As String
        Dim cOffSetChar As String
        Dim cErrorMessage As String
        Dim aValue As String()

        _ResultAL = New ArrayList
        ImpostaMessaggi(New String() {"", "", "", "0", _DatiSessione.Count, "Elaborazione dei file da caricare...."})

        aBlocchi = New String(3) {"", "", "", ""}

        ' Copia dei file indicati nella loro posizione
        For Each aValue In _DatiSessione
            ImportaFileInSessione(aValue(0), aValue(1))
        Next

        data_import = New DS_import
        nLine = 0
        xdWork = New Xml.XmlDocument
        xdWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "FATTURA", ""))

        _MessaggiAvanzamento(5) = "Caricamento dati fatturazione in corso..."
        ' Carica il primo file con i dati dei contratti provenienti da FIMM
        cFileName = CercaFileName(_DatiSessione, 199)
        sr = New System.IO.StreamReader(cFileName)
        nLine = 0
        cOffSetChar = ""
        While Not sr.EndOfStream
            cLine = ""
            While sr.Read(chTmp, 0, 1) = 1 And cLine.Length < 156
                If Not (chTmp = vbCr Or chTmp = vbLf) Then
                    cLine = String.Concat(cLine, chTmp)
                End If
            End While

            If cLine.Length = 156 Then
                If Not (nLine = cLine.Substring(150, 6)) Then
                    cBlocco = cLine.Substring(0, 3)
                    If IsNumeric(cBlocco.Trim) Then
                        nBlocco = cBlocco.Trim
                    Else
                        cBlocco = cBlocco.Trim
                    End If
                    nLine += 1
                Else
                    cLine = ""
                End If
                If cLine > "" Then
                    If (nBlocco = 1 And cBlocco > "" And aBlocchi(0) > "") Then
                        cErrorMessage = SuddividiBlocchiDati(aBlocchi)
                        AggiungiRecordQD(xdWork, cErrorMessage)
                        aBlocchi = New String(3) {cLine, "", "", ""}
                    Else
                        aBlocchi(nBlocco - 1) = String.Concat(aBlocchi(nBlocco - 1), cLine)
                        cLine = ""
                    End If
                End If
            End If
        End While
        cErrorMessage = SuddividiBlocchiDati(aBlocchi)
        AggiungiRecordQD(xdWork, cErrorMessage)

        sr.Close()

        _MessaggiAvanzamento(5) = "Caricamento dati trasparenza in corso..."
        ' Carica il primo file con i dati della trasparenza
        cFileName = CercaFileName(_DatiSessione, 198)
        sr = New System.IO.StreamReader(cFileName)
        cHeader = sr.ReadLine()
        aHeader = cHeader.Split(";")
        cBolletta = ""
        While Not sr.EndOfStream
            ' legge la riga e la inserisce nel dataset.
            cLine = sr.ReadLine()
            Try
                If cLine > "" Then
                    aTemp = cLine.Split(";")
                    If cBolletta <> aTemp(Array.IndexOf(aHeader, "BOLLETTA")) Then
                        cBolletta = aTemp(Array.IndexOf(aHeader, "BOLLETTA"))
                        If data_import.TBL_dati_fatt.Select(String.Concat("DFA_num_boll = ", cBolletta)).Length = 1 Then
                            dr_dfa = CType(data_import.TBL_dati_fatt.Select(String.Concat("DFA_num_boll = ", cBolletta))(0), DS_import.TBL_dati_fattRow)
                            nCodFatt = CType(data_import.TBL_dati_fatt.Select(String.Concat("DFA_num_boll = ", cBolletta))(0), DS_import.TBL_dati_fattRow).DFA_cod_mai
                            dr_dfa.DFA_serv_rete_des = " "
                            dr_dfa.DFA_serv_rete_imp = 0
                            dr_dfa.DFA_serv_vend_des = ""
                            dr_dfa.DFA_serv_vend_imp = 0
                            dr_dfa.DFA_tot_forn_ener_des = ""
                            dr_dfa.DFA_tot_forn_ener = 0
                            dr_dfa.DFA_imp_des = " "
                            dr_dfa.DFA_imp_imp = 0
                            dr_dfa.DFA_sconto_enel_des = ""
                            dr_dfa.DFA_sconto_enel = 0
                        Else
                            nCodFatt = -1
                        End If
                    End If
                    If nCodFatt > -1 Then
                        nTmp = aTemp(Array.IndexOf(aHeader, "RIGA")).Substring(1)
                        Select Case nTmp
                            Case Is = 0
                                dr_dfa.DFA_serv_rete_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                                dr_dfa.DFA_serv_rete_imp = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                                nTmp = -1
                            Case 1 To 10
                                nSezione = 1
                            Case Is = 11
                                dr_dfa.DFA_serv_vend_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                                dr_dfa.DFA_serv_vend_imp = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                                nTmp = -1
                            Case 12 To 21
                                nSezione = 2
                            Case Is = 22
                                dr_dfa.DFA_imp_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                                dr_dfa.DFA_imp_imp = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                                nTmp = -1
                            Case 23 To 25
                                nSezione = 3
                            Case 27 To 29
                                nSezione = 4
                            Case Else
                                nSezione = -1
                        End Select

                        If nSezione > -1 And nTmp > -1 Then
                            ' Riempie il record coni valori estratti dalla riga letta dal file.
                            dr_tra = data_import.TBL_trasparenza.NewRow

                            With dr_tra
                                '.TRA_fattura = aTemp(Array.IndexOf(aHeader, "BOLLETTA")).......
                                .TRA_cod_mai = nCodFatt
                                .TRA_cod_sez = nSezione
                                .TRA_tipo_rec = aTemp(Array.IndexOf(aHeader, "RIGA"))
                                .TRA_progr_voc = aTemp(Array.IndexOf(aHeader, "PROGRESSIVO_VOCE"))
                                .TRA_riga_parz = (.TRA_tipo_rec = "T10") Or (.TRA_tipo_rec = "T21") Or (.TRA_tipo_rec = "T25")
                                .TRA_descr_voce = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                                .TRA_dal = aTemp(Array.IndexOf(aHeader, "DAL"))
                                .TRA_al = aTemp(Array.IndexOf(aHeader, "AL"))

                                cTmp = aTemp(Array.IndexOf(aHeader, "GIORNI"))
                                If IsNumeric(cTmp) Then
                                    .TRA_giorni = CType(cTmp, String)
                                Else
                                    .TRA_giorni = -1
                                End If

                                .TRA_scaglione = aTemp(Array.IndexOf(aHeader, "SCAGLIONE"))
                                .TRA_fascia = aTemp(Array.IndexOf(aHeader, "FASCIA"))

                                '  impostazione delle unit� di misura
                                Select Case nTmp
                                    Case Is = 1, 2, 8, 12, 18
                                        .TRA_unita_mis = "�/cliente/mese"
                                    Case Is = 3, 4, 5, 6, 7, 9, 16, 20, 23, 24
                                        .TRA_unita_mis = "�/kWh"
                                    Case Is = 13
                                        .TRA_unita_mis = "�/cliente/giorno"
                                    Case Is = 14, 15, 19
                                        .TRA_unita_mis = "�/kW/mese"
                                    Case Is = 17
                                        .TRA_unita_mis = "�/kWh"
                                    Case Else
                                        .TRA_unita_mis = ""
                                End Select

                                cTmp = aTemp(Array.IndexOf(aHeader, "CORR_UNIT"))
                                If IsNumeric(cTmp) Then
                                    .TRA_corr_uni = cTmp
                                Else
                                    .TRA_corr_uni = -1
                                End If

                                cTmp = aTemp(Array.IndexOf(aHeader, "QUANTITA"))
                                If IsNumeric(cTmp) Then
                                    .TRA_quantita = cTmp
                                Else
                                    .TRA_quantita = -1
                                End If

                                .TRA_totale = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                                cTmp = aTemp(Array.IndexOf(aHeader, "ALI_IVA"))
                                If IsNumeric(cTmp) Then
                                    .TRA_aliq_iva = CType(cTmp, String)
                                    If (dr_dfa.DFA_iva_per > 0) And (dr_dfa.DFA_iva_per <> .TRA_aliq_iva) And (.TRA_aliq_iva > 0) Then
                                        AddErroreLieve("Aliquote IVA differenti")
                                    ElseIf dr_dfa.DFA_iva_per = -1 Then
                                        dr_dfa.DFA_iva_per = .TRA_aliq_iva
                                    End If
                                Else
                                    .TRA_aliq_iva = -1
                                End If

                            End With
                            data_import.TBL_trasparenza.Rows.Add(dr_tra)
                        ElseIf nTmp = 26 Then
                            dr_dfa.DFA_tot_forn_ener_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                            dr_dfa.DFA_tot_forn_ener = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                        ElseIf nTmp = 30 Then
                            dr_dfa.DFA_one_div_iva_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                            dr_dfa.DFA_one_div_iva = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                        ElseIf nTmp = 31 Then
                            dr_dfa.DFA_one_div_no_iva_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                            dr_dfa.DFA_one_div_no_iva = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                        ElseIf nTmp = 32 Then
                            dr_dfa.DFA_tot_one_div_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                            dr_dfa.DFA_tot_one_div_imp = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                        ElseIf nTmp = 34 Then
                            dr_dfa.DFA_sconto_enel_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                            dr_dfa.DFA_sconto_enel = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                        ElseIf nTmp = 33 Then
                            dr_dfa.DFA_tot_netto_iva_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                            dr_dfa.DFA_tot_netto_iva_imp = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                        ElseIf nTmp = 35 Then                                                              '  indicazione dell'imponibile
                            dr_dfa.DFA_imponibile_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                            dr_dfa.DFA_imponibile = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                        ElseIf nTmp = 36 Then                                                              '  indicazione dell'importo dell'iva
                            dr_dfa.DFA_iva_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                            dr_dfa.DFA_iva = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                        ElseIf nTmp = 37 Then                                                              '  indicazione del totale della fattura
                            dr_dfa.DFA_totale_fatt_tra_des = aTemp(Array.IndexOf(aHeader, "D_VOCE"))
                            dr_dfa.DFA_totale_fatt_tra = aTemp(Array.IndexOf(aHeader, "TOTALE"))
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try

        End While
        sr.Close()

        _MessaggiAvanzamento(5) = "Ordinamento dei record in corso..."
        ' FC_SortFile.SortFileIndiceXML(xdWork, 0, xdWork.SelectSingleNode("FATTURA").ChildNodes.Count - 1)

        ' Rimuove il campo utilizzato per l'ordinamento
        nRecord = 1
        For Each xnWork1 As Xml.XmlNode In xdWork.SelectNodes("FATTURA/FATTURA_ROW/SORTFIELD")
            xnWork2 = xnWork1.ParentNode
            xnWork2.RemoveChild(xnWork1)
            SetRecordIDX(xdWork, xnWork2, "MAI_codice", nRecord)
            nRecord += 1
        Next

        _MessaggiAvanzamento(5) = "Salvataggio dati in corso..."
        xdWork.Save(String.Concat(_SessionePath, "\dati\indice.xml"))
        'xdViaMail.Save(String.Concat(_SessionePath, "\dati\SendByMail.xml"))
        _ResultAL.Add(New String() {"filename", "dati\indice.xml", "0", "0", "1"})

        SalvaBaseDati(data_import)

    End Sub

    Private Function SuddividiBlocchiDati(ByVal aBlocchi As String()) As String
        Dim dr_com As DS_import.TBL_comunicazioniRow
        Dim dr_dfa As DS_import.TBL_dati_fattRow
        Dim dr_daf As DS_import.TBL_dati_fornRow
        Dim dr_con As DS_import.TBL_consumiRow
        Dim dr_mai As DS_import.TBL_mainRow
        Dim dr_rco As DS_import.TBL_riep_consRow
        Dim dr_rci As DS_import.TBL_riep_cons_indRow
        Dim dr_tmp As DS_import.TBL_tmpRow

        Dim nBlocco As Integer
        Dim cBlocco As String
        Dim nLine As Integer
        Dim cLine As String
        Dim cTmp As String
        Dim ts As TimeSpan
        Dim cErrorMessage As String

        cErrorMessage = ""

        dr_mai = data_import.TBL_main.NewRow
        dr_daf = data_import.TBL_dati_forn.NewRow
        dr_dfa = data_import.TBL_dati_fatt.NewRow
        dr_con = data_import.TBL_consumi.NewRow
        dr_rco = data_import.TBL_riep_cons.NewRow
        dr_rci = data_import.TBL_riep_cons_ind.NewRow
        dr_tmp = data_import.TBL_tmp.NewRow

        dr_daf.DAF_cod_mai = dr_mai.MAI_codice
        dr_dfa.DFA_cod_mai = dr_mai.MAI_codice
        dr_con.CON_cod_mai = dr_mai.MAI_codice
        dr_con.CON_mat_cont = ""
        dr_rco.RCO_ult_per_cong = ""
        dr_rco.RCO_con_med_ug = 0
        dr_rco.RCO_pen_per_cong = ""
        dr_rco.RCO_con_med_pc = 0
        dr_rco.RCO_con_med_ug_f1 = 0
        dr_rco.RCO_con_med_ug_f2 = 0
        dr_rco.RCO_con_med_ug_f3 = 0

        dr_rco.RCO_lett_ini_f1 = 0
        dr_rco.RCO_lett_fin_f1 = 0
        dr_rco.RCO_lett_ini_f2 = 0
        dr_rco.RCO_lett_fin_f2 = 0
        dr_rco.RCO_lett_ini_f3 = 0
        dr_rco.RCO_lett_fin_f3 = 0

        dr_rco.RCO_cod_mai = dr_mai.MAI_codice
        dr_rci.RCI_cod_mai = dr_mai.MAI_codice
        data_import.TBL_main.Rows.Add(dr_mai)
        data_import.TBL_dati_forn.Rows.Add(dr_daf)
        data_import.TBL_dati_fatt.Rows.Add(dr_dfa)
        data_import.TBL_consumi.Rows.Add(dr_con)
        data_import.TBL_riep_cons.Rows.Add(dr_rco)
        data_import.TBL_tmp.Rows.Add(dr_tmp)

        dr_rco.RCO_tot_acc_mon = 0
        If aBlocchi(0).Length < 156 Then
            cErrorMessage = "Blocco 1 non presente"
        End If
        If aBlocchi(1).Length < 156 Then
            cErrorMessage = "Blocco 2 non presente"
        End If
        If aBlocchi(2).Length < 156 Then
            cErrorMessage = "Blocco 3 non presente"
        End If
        If aBlocchi(3).Length < 156 Then
            cErrorMessage = "Blocco 4 non presente"
        End If
        Try
            nBlocco = 0
            While nBlocco < 4
                ' Recupera il blocco da analizzare
                cBlocco = aBlocchi(nBlocco)

                If nBlocco + 1 = 1 Then
                    nLine = 1
                    While cBlocco > ""
                        cLine = cBlocco.Substring(0, 156)
                        cBlocco = cBlocco.Substring(156)
                        If nLine = 1 Then
                            With dr_mai
                                .MAI_cod_cli = cLine.Substring(3, 8).Trim
                                .MAI_nominativo = cLine.Substring(11, 30).Trim
                                .MAI_int_sito_1 = cLine.Substring(41, 30).Trim
                                .MAI_int_sito_2 = cLine.Substring(71, 21).Trim
                                .MAI_int_sito_3 = cLine.Substring(92, 30).Trim
                                .MAI_int_sito_4 = cLine.Substring(122, 5).Trim
                                .MAI_int_sito_5 = cLine.Substring(127, 2).Trim
                                .MAI_cfpi = cLine.Substring(129, 16).Trim
                            End With
                        End If
                        If nLine = 2 Then
                            dr_dfa.DFA_progr_boll = cLine.Substring(3, 7).Trim
                            dr_dfa.DFA_data_emis = cLine.Substring(10, 8).Trim
                            dr_dfa.DFA_data_scad = cLine.Substring(18, 8).Trim
                            dr_dfa.DFA_num_fatt = cLine.Substring(26, 8).Trim
                            dr_mai.MAI_banca = cLine.Substring(34, 40).Replace("99", "").Replace("01", "").Trim
                            dr_daf.DAF_ppr_sito_1 = cLine.Substring(90, 60).Trim
                            ' i dati dell'ubicazione del sito vengono banalmentre ripresi da
                            ' quelli della residenza del titolare
                            dr_daf.DAF_ppr_sito_2 = ""
                            dr_daf.DAF_ppr_sito_3 = dr_mai.MAI_int_sito_3
                            dr_daf.DAF_ppr_sito_4 = dr_mai.MAI_int_sito_4
                            dr_daf.DAF_ppr_sito_5 = dr_mai.MAI_int_sito_5

                            ' Se l'indicazione della citt� � gi� presente nell'indirizzo dell'ubicazione la rimuovo.
                            If dr_daf.DAF_ppr_sito_1.IndexOf(dr_daf.DAF_ppr_sito_3) > -1 Then
                                dr_daf.DAF_ppr_sito_1 = dr_daf.DAF_ppr_sito_1.Replace(dr_daf.DAF_ppr_sito_3, "")
                            End If
                        End If
                        If nLine = 3 Then
                            cTmp = cLine.Substring(3, 19)
                            dr_dfa.DFA_ocr = String.Concat("0", cTmp.Substring(1, 17))
                            dr_dfa.DFA_num_boll = cTmp.Substring(5, 8)

                            cTmp = cLine.Substring(29, 59)
                            dr_mai.MAI_dom_ban = cTmp.StartsWith("ADDEBITATO")
                            dr_dfa.DFA_tipo_bolletta = cLine.Substring(114, 1).Trim
                            dr_dfa.DFA_totale_fatt = cLine.Substring(131, 15).Trim
                            dr_tmp.TMP_print_boll = cLine.Substring(88, 15) <> "***************"
                        End If
                        If nLine = 4 Then
                            With dr_mai
                                .MAI_uti_nome = cLine.Substring(3, 30).Trim
                                .MAI_rec_nome = cLine.Substring(33, 30).Trim

                                If .MAI_nominativo = .MAI_rec_nome Then
                                    .MAI_rec_nome = ""
                                End If
                                .MAI_rec_sito_1 = cLine.Substring(63, 30).Trim
                                .MAI_rec_sito_2 = cLine.Substring(93, 21).Trim
                                .MAI_rec_sito_3 = cLine.Substring(114, 29).Trim
                                .MAI_rec_sito_4 = cLine.Substring(143, 5).Trim
                                .MAI_rec_sito_5 = cLine.Substring(148, 2).Trim
                            End With
                        End If
                        nLine += 1
                    End While
                End If
                If nBlocco + 1 = 2 Then
                    nLine = 1
                    While cBlocco > ""
                        cLine = cBlocco.Substring(0, 156)
                        cBlocco = cBlocco.Substring(156)
                        If nLine = 1 Then
                            dr_dfa.DFA_fatt_ora = cLine.Substring(3, 1) = "S"
                            dr_daf.DAF_POD = cLine.Substring(4, 14)
                            dr_daf.DAF_cod_contr = cLine.Substring(18, 8)
                            dr_daf.DAF_tipo_contr = cLine.Substring(26, 50)
                            dr_daf.DAF_opz_tar = cLine.Substring(76, 4)
                            dr_daf.DAF_denom_tar = cLine.Substring(80, 40)
                            dr_daf.DAF_tens_alim = cLine.Substring(120, 5)
                            dr_daf.DAF_inizio_forn = cLine.Substring(125, 10)
                            dr_mai.MAI_tipo_all = cLine.Substring(136, 5)
                        End If
                        If nLine = 2 Then
                            dr_dfa.DFA_tipo_bollettazione = cLine.Substring(3, 1)
                            dr_dfa.DFA_per_rif = cLine.Substring(4, 27)
                            dr_daf.DAF_pot_imp = cLine.Substring(31, 10)
                            dr_daf.DAF_pot_dis = cLine.Substring(41, 10)
                            dr_daf.DAF_dep_cauz = cLine.Substring(91, 12)
                            dr_daf.DAF_stat_cauz = cLine.Substring(103, 1)
                            dr_rco.RCO_con_anno_f1 = GetValueFromText(cLine, 104, 8, 0)
                            dr_rco.RCO_con_anno_f2 = GetValueFromText(cLine, 112, 8, 0)
                            dr_rco.RCO_con_anno_f3 = GetValueFromText(cLine, 120, 8, 0)
                            dr_rco.RCO_con_anno_tot = dr_rco.RCO_con_anno_f1 + dr_rco.RCO_con_anno_f2 + dr_rco.RCO_con_anno_f3

                            cTmp = GetValueFromText(cLine, 128, 7, 0).Replace("%", "")
                            If IsNumeric(cTmp) Then
                                dr_rco.RCO_per_f1 = cTmp
                            Else
                                dr_rco.RCO_per_f1 = 0
                            End If
                            cTmp = GetValueFromText(cLine, 135, 7, 0).Replace("%", "")
                            If IsNumeric(cTmp) Then
                                dr_rco.RCO_per_f2_f3 = cTmp
                            Else
                                dr_rco.RCO_per_f2_f3 = 0
                            End If
                        End If
                        If nLine = 3 Then
                            dr_con.CON_sti_pen_let = cLine.Substring(3, 8).Replace("//", "").Trim
                            dr_con.CON_sti_ult_let = cLine.Substring(11, 8).Replace("//", "").Trim

                            If (dr_con.CON_sti_pen_let > "") And (dr_con.CON_sti_ult_let > "") Then
                                ts = CDate(dr_con.CON_sti_ult_let).Subtract(CDate(dr_con.CON_sti_pen_let))
                                dr_con.CON_sti_gg = ts.Days
                            Else
                                dr_con.CON_sti_gg = -1
                            End If
                            dr_rco.RCO_acc_f1 = GetValueFromText(cLine, 27, 8, 0)
                            dr_rco.RCO_acc_f2 = GetValueFromText(cLine, 35, 8, 0)
                            dr_rco.RCO_acc_f3 = GetValueFromText(cLine, 43, 8, 0)
                            dr_con.CON_eff_pen_let = cLine.Substring(51, 8).Replace("//", "").Trim
                            dr_con.CON_eff_ult_let = cLine.Substring(59, 8).Replace("//", "").Trim

                            If (dr_con.CON_eff_pen_let > "") And (dr_con.CON_eff_ult_let > "") Then
                                ts = CDate(dr_con.CON_eff_ult_let).Subtract(CDate(dr_con.CON_eff_pen_let))
                                dr_con.CON_eff_gg = ts.Days
                            Else
                                dr_con.CON_eff_gg = -1
                            End If
                            dr_rco.RCO_lett_ini_f1 = GetValueFromText(cLine, 67, 8, "0")
                            dr_rco.RCO_lett_fin_f1 = GetValueFromText(cLine, 75, 8, "0")
                            dr_rco.RCO_con_f1 = GetValueFromText(cLine, 83, 8, "0")
                            dr_rco.RCO_tot_con_mon = GetValueFromText(cLine, 83, 8, 0)
                            dr_rco.RCO_tot_acc_mon = GetValueFromText(cLine, 91, 8, 0)
                            dr_rco.RCO_tot_diff_mon = GetValueFromText(cLine, 99, 8, 0)
                            'dr_rco.RCO_acc_f1 = GetValueFromText(cLine, 91, 8, "0")
                            'dr_rco.RCO_tot_f1 = GetValueFromText(cLine, 99, 8, "0")
                        End If
                        If nLine = 4 Then
                            dr_rci.RCI_lett_ini_rea_f1 = GetValueFromText(cLine, 27, 8, "-1")
                            dr_rci.RCI_lett_fin_rea_f1 = GetValueFromText(cLine, 35, 8, "-1")
                            dr_rci.RCI_con_rea_f1 = GetValueFromText(cLine, 43, 8, "-1")
                            dr_rci.RCI_lett_ini_ind_f1 = GetValueFromText(cLine, 51, 8, "-1")
                            dr_rci.RCI_lett_fin_ind_f1 = GetValueFromText(cLine, 59, 8, "-1")
                            dr_rci.RCI_con_ind_f1 = GetValueFromText(cLine, 67, 8, "-1")
                            dr_rco.RCO_lett_ini_f2 = GetValueFromText(cLine, 75, 8, "0")
                            dr_rco.RCO_lett_fin_f2 = GetValueFromText(cLine, 83, 8, "0")
                            dr_rco.RCO_con_f2 = GetValueFromText(cLine, 91, 8, "0")
                            dr_rci.RCI_lett_ini_rea_f2 = GetValueFromText(cLine, 99, 8, "-1")
                            dr_rci.RCI_lett_fin_rea_f2 = GetValueFromText(cLine, 107, 8, "-1")
                            dr_rci.RCI_con_rea_f2 = GetValueFromText(cLine, 115, 8, "-1")
                            dr_rci.RCI_lett_ini_ind_f2 = GetValueFromText(cLine, 123, 8, "-1")
                            dr_rci.RCI_lett_fin_ind_f2 = GetValueFromText(cLine, 131, 8, "-1")
                            dr_rci.RCI_con_ind_f2 = GetValueFromText(cLine, 139, 8, "-1")
                        End If
                        If nLine = 5 Then
                            dr_rco.RCO_lett_ini_f3 = GetValueFromText(cLine, 3, 8, "0")
                            dr_rco.RCO_lett_fin_f3 = GetValueFromText(cLine, 11, 8, "0")
                            dr_rco.RCO_con_f3 = GetValueFromText(cLine, 19, 8, "0")
                            dr_rci.RCI_lett_ini_rea_f3 = GetValueFromText(cLine, 27, 8, "-1")
                            dr_rci.RCI_lett_fin_rea_f3 = GetValueFromText(cLine, 35, 8, "-1")
                            dr_rci.RCI_con_rea_f3 = GetValueFromText(cLine, 43, 8, "-1")
                            dr_rci.RCI_lett_ini_ind_f3 = GetValueFromText(cLine, 51, 8, "-1")
                            dr_rci.RCI_lett_fin_ind_f3 = GetValueFromText(cLine, 59, 8, "-1")
                            dr_rci.RCI_con_ind_f3 = GetValueFromText(cLine, 67, 8, "-1")
                        End If
                        If nLine = 6 Then
                            dr_con.CON_mat_cont = cLine.Substring(3, 12)
                            dr_rco.RCO_ult_per_cong = cLine.Substring(111, 21)
                            dr_rco.RCO_con_med_ug = GetValueFromText(cLine, 132, 9, "-1")
                        End If
                        If nLine = 7 Then
                            dr_rco.RCO_pen_per_cong = cLine.Substring(3, 21)
                            dr_rco.RCO_con_med_pc = GetValueFromText(cLine, 24, 9, "-1")
                            dr_rco.RCO_con_med_ug_f1 = GetValueFromText(cLine, 33, 9, "-1")
                            dr_rco.RCO_con_med_ug_f2 = GetValueFromText(cLine, 42, 9, "-1")
                            dr_rco.RCO_con_med_ug_f3 = GetValueFromText(cLine, 51, 9, "-1")

                            If dr_dfa.DFA_tipo_bollettazione = "A" Then
                                dr_rco.RCO_lett_ini_f1 = GetValueFromText(cLine, 87, 8, "0")
                                dr_rco.RCO_lett_fin_f1 = GetValueFromText(cLine, 95, 8, "0")
                                dr_rco.RCO_lett_ini_f2 = GetValueFromText(cLine, 103, 8, "0")
                                dr_rco.RCO_lett_fin_f2 = GetValueFromText(cLine, 111, 8, "0")
                                dr_rco.RCO_lett_ini_f3 = GetValueFromText(cLine, 119, 8, "0")
                                dr_rco.RCO_lett_fin_f3 = GetValueFromText(cLine, 127, 8, "0")
                            End If
                        End If
                        nLine += 1
                    End While
                End If
                If nBlocco + 1 = 4 Then
                    nLine = 1
                    While cBlocco > ""
                        cLine = cBlocco.Substring(0, 156)
                        cBlocco = cBlocco.Substring(156)
                        If cLine.Substring(3, 147).Trim > "" Then
                            dr_com = data_import.TBL_comunicazioni.NewRow
                            dr_com.COM_cod_mai = dr_mai.MAI_codice
                            dr_com.COM_comunicazione = cLine.Substring(3, 147)
                            If nLine = 1 Then
                                dr_dfa.DFA_com_mpa = dr_com.COM_comunicazione.ToLower.StartsWith("risulta")
                            End If
                            data_import.TBL_comunicazioni.Rows.Add(dr_com)
                        End If
                        nLine += 1
                    End While
                End If
                nBlocco += 1
            End While
            If dr_dfa.DFA_fatt_ora Then
                dr_rco.RCO_tot_f1 = dr_rco.RCO_con_f1 - dr_rco.RCO_acc_f1
                dr_rco.RCO_tot_f2 = dr_rco.RCO_con_f2 - dr_rco.RCO_acc_f2
                dr_rco.RCO_tot_f3 = dr_rco.RCO_con_f3 - dr_rco.RCO_acc_f3

                dr_rco.RCO_con_f2_f3 = dr_rco.RCO_con_f2 + dr_rco.RCO_con_f3
                dr_rco.RCO_acc_f2_f3 = dr_rco.RCO_acc_f2 + dr_rco.RCO_acc_f3
                dr_rco.RCO_tot_f2_f3 = dr_rco.RCO_con_f2_f3 - dr_rco.RCO_acc_f2_f3

                dr_rco.RCO_tot_con_ora = dr_rco.RCO_con_f1 + dr_rco.RCO_con_f2 + dr_rco.RCO_con_f3
                dr_rco.RCO_tot_acc_ora = dr_rco.RCO_acc_f1 + dr_rco.RCO_acc_f2 + dr_rco.RCO_acc_f3
                dr_rco.RCO_tot_diff_ora = dr_rco.RCO_tot_con_ora - dr_rco.RCO_tot_acc_ora
            Else
                'dr_rco.RCO_tot_diff_mon = dr_rco.RCO_tot_con_mon - dr_rco.RCO_tot_acc_mon
            End If

            dr_dfa.DFA_fatt_ind = dr_daf.DAF_pot_imp > 15
            If dr_dfa.DFA_fatt_ind Then
                data_import.TBL_riep_cons_ind.Rows.Add(dr_rci)
            End If
        Catch ex As Exception
            If cErrorMessage = "" Then
                cErrorMessage = ex.Message
            End If
        End Try
        Return cErrorMessage

    End Function

    Private Sub AggiungiRecordQD(ByVal xdWork2 As Xml.XmlDocument, ByVal cErrorMessage As String)
        Dim dr_dfa As DS_import.TBL_dati_fattRow
        Dim dr_mai As DS_import.TBL_mainRow
        Dim dr_tmp As DS_import.TBL_tmpRow
        Dim xiRecord As Xml.XmlNode
        Dim cSortValue As String

        dr_mai = data_import.TBL_main.Rows(data_import.TBL_main.Rows.Count - 1)
        dr_dfa = data_import.TBL_dati_fatt(data_import.TBL_main.Rows.Count - 1)
        dr_tmp = data_import.TBL_tmp(data_import.TBL_main.Rows.Count - 1)
        xiRecord = xdWork2.CreateNode(Xml.XmlNodeType.Element, "FATTURA_ROW", "")

        SetRecordIDX(xdWork2, xiRecord, "MAI_num_utente", dr_mai.MAI_cod_cli)
        SetRecordIDX(xdWork2, xiRecord, "MAI_num_fattura", dr_dfa.DFA_num_fatt)
        SetRecordIDX(xdWork2, xiRecord, "MAI_nominativo", dr_mai.MAI_nominativo)
        SetRecordIDX(xdWork2, xiRecord, "MAI_indirizzo_recapito", String.Concat(dr_mai.MAI_rec_sito_1, " ", dr_mai.MAI_rec_sito_2))
        SetRecordIDX(xdWork2, xiRecord, "MAI_citta_recapito", dr_mai.MAI_rec_sito_3)
        SetRecordIDX(xdWork2, xiRecord, "MAI_cap_recapito", dr_mai.MAI_rec_sito_4)
        SetRecordIDX(xdWork2, xiRecord, "MAI_provincia_recapito", dr_mai.MAI_rec_sito_5)
        If dr_tmp.TMP_print_boll Then
            SetRecordIDX(xdWork2, xiRecord, "MAI_print_bol", "si")
        Else
            SetRecordIDX(xdWork2, xiRecord, "MAI_print_bol", "no")
        End If


        SetRecordIDX(xdWork2, xiRecord, "MAI_print_all", "si")
        If cErrorMessage = "" Then
            SetRecordIDX(xdWork2, xiRecord, "DEF_errcode", "0")
            SetRecordIDX(xdWork2, xiRecord, "DEF_errdesc", "")
        Else
            SetRecordIDX(xdWork2, xiRecord, "DEF_errcode", "-1")
            SetRecordIDX(xdWork2, xiRecord, "DEF_errdesc", cErrorMessage)
        End If
        SetRecordIDX(xdWork2, xiRecord, "DEF_sendbymail", "")
        SetRecordIDX(xdWork2, xiRecord, "MAI_filedati", dr_mai.MAI_codice)

        ' impostazione della modalit� di ordinamento.
        ' in partenza si era preferito ordinare per numero fattura.
        ' la richiesta di bonato � stata invece quella di ordinare per:
        ' CAP - Via - NumeroCivico Pari o dispari - numero civico.

        'cSortValue = xmlRecord.SelectSingleNode("MAI_num_fattura").InnerText

        ' Inizializzo la variabile di ordinamento come indicato sopra
        cSortValue = dr_mai.MAI_rec_sito_3
        cSortValue = String.Concat(cSortValue, dr_mai.MAI_rec_sito_1, dr_mai.MAI_rec_sito_2.PadLeft(21, "0"))
        cSortValue = String.Concat(cSortValue, dr_mai.MAI_nominativo).Replace(" ", "")
        SetRecordIDX(xdWork2, xiRecord, "SORTFIELD", cSortValue)
        xdWork2.SelectSingleNode("FATTURA").AppendChild(xiRecord)

    End Sub

    Private Sub SalvaBaseDati(ByVal data_import As DS_import)
        Dim cConnACCStr As String
        Dim cDBFileName As String
        Dim oComBuil As OleDb.OleDbCommandBuilder
        Dim oDa As OleDb.OleDbDataAdapter
        Dim oCmd As OleDb.OleDbCommand

        Try
            cDBFileName = _SessioneDati.SelectSingleNode("filename[@tipo='103']").InnerText
            cDBFileName = String.Concat(_SessionePath, cDBFileName)
            cConnACCStr = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cDBFileName, ";")

            ' Salvataggio della tabella TBL_main
            _MessaggiAvanzamento(5) = "Salvataggio dati Tabella Principale in corso..."
            oCmd = New OleDb.OleDbCommand("SELECT * FROM TBL_main", New OleDb.OleDbConnection(cConnACCStr))
            oDa = New OleDb.OleDbDataAdapter(oCmd)
            oComBuil = New OleDb.OleDbCommandBuilder(oDa)
            oDa.Update(data_import.TBL_main)
            oCmd.Connection.Close()

            ' Salvataggio della tabella TBL_dati_fatt
            _MessaggiAvanzamento(5) = "Salvataggio dati Tabella Dati Fatturazione in corso..."
            oCmd = New OleDb.OleDbCommand("SELECT * FROM TBL_dati_fatt", New OleDb.OleDbConnection(cConnACCStr))
            oDa = New OleDb.OleDbDataAdapter(oCmd)
            oComBuil = New OleDb.OleDbCommandBuilder(oDa)
            oDa.Update(data_import.TBL_dati_fatt)
            oCmd.Connection.Close()

            ' Salvataggio della tabella TBL_dati_forn
            _MessaggiAvanzamento(5) = "Salvataggio dati Tabella Dati Fornitura in corso..."
            oCmd = New OleDb.OleDbCommand("SELECT * FROM TBL_dati_forn", New OleDb.OleDbConnection(cConnACCStr))
            oDa = New OleDb.OleDbDataAdapter(oCmd)
            oComBuil = New OleDb.OleDbCommandBuilder(oDa)
            oDa.Update(data_import.TBL_dati_forn)
            oCmd.Connection.Close()

            ' Salvataggio della tabella TBL_trasparenza
            _MessaggiAvanzamento(5) = "Salvataggio dati Tabella Trasparenza in corso..."
            oCmd = New OleDb.OleDbCommand("SELECT * FROM TBL_trasparenza", New OleDb.OleDbConnection(cConnACCStr))
            oDa = New OleDb.OleDbDataAdapter(oCmd)
            oComBuil = New OleDb.OleDbCommandBuilder(oDa)
            oDa.Update(data_import.TBL_trasparenza)
            oCmd.Connection.Close()

            ' Salvataggio della tabella TBL_comunicazioni
            _MessaggiAvanzamento(5) = "Salvataggio dati Tabella Comunicazioni in corso..."
            oCmd = New OleDb.OleDbCommand("SELECT * FROM TBL_comunicazioni", New OleDb.OleDbConnection(cConnACCStr))
            oDa = New OleDb.OleDbDataAdapter(oCmd)
            oComBuil = New OleDb.OleDbCommandBuilder(oDa)
            oDa.Update(data_import.TBL_comunicazioni)
            oCmd.Connection.Close()

            ' Salvataggio della tabella TBL_consumi
            _MessaggiAvanzamento(5) = "Salvataggio dati Tabella Consumi in corso..."
            oCmd = New OleDb.OleDbCommand("SELECT * FROM TBL_consumi", New OleDb.OleDbConnection(cConnACCStr))
            oDa = New OleDb.OleDbDataAdapter(oCmd)
            oComBuil = New OleDb.OleDbCommandBuilder(oDa)
            oDa.Update(data_import.TBL_consumi)
            oCmd.Connection.Close()

            ' Salvataggio della tabella TBL_riep_cons
            _MessaggiAvanzamento(5) = "Salvataggio dati Tabella Riepilogo Consumi in corso..."
            oCmd = New OleDb.OleDbCommand("SELECT * FROM TBL_riep_cons", New OleDb.OleDbConnection(cConnACCStr))
            oDa = New OleDb.OleDbDataAdapter(oCmd)
            oComBuil = New OleDb.OleDbCommandBuilder(oDa)
            oDa.Update(data_import.TBL_riep_cons)
            oCmd.Connection.Close()

            ' Salvataggio della tabella TBL_riep_cons
            oCmd = New OleDb.OleDbCommand("SELECT * FROM TBL_riep_cons_ind", New OleDb.OleDbConnection(cConnACCStr))
            oDa = New OleDb.OleDbDataAdapter(oCmd)
            oComBuil = New OleDb.OleDbCommandBuilder(oDa)
            oDa.Update(data_import.TBL_riep_cons_ind)
            oCmd.Connection.Close()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Function CercaFileName(ByVal _DatiSessione As ArrayList, ByVal nTipo As Integer) As String
        Dim cTmp As String

        cTmp = ""
        For Each aValue As String() In _DatiSessione
            If aValue(0) = nTipo Then
                cTmp = aValue(1)
            End If
        Next
        Return cTmp

    End Function

    Sub ImportaFileInSessione(ByVal nTipoFile As Integer, ByVal cFileName As String)
        Dim cDstFile As String

        ' Nel caso di tipo file 1 allora si tratta di un file xml, indipendentemente dall'estensione 
        ' da suddividere eventualmente in file di pi� piccole dimensioni.
        If nTipoFile = 102 Then
            _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
            cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

            System.IO.File.Copy(cFileName, cDstFile)
            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
            _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
        End If

    End Sub

#End Region

    Public Sub LoadQuickDataset() Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.LoadQuickDataset
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow

        _QuickDataset = New DS_QD_UE_ele
        _xmlDati_qd = New Xml.XmlDocument
        _xmlDati_qd.Load(QuickDataSet_IndexFile)
        ImpostaMessaggi(New String() {"", "", "", "0", _xmlDati_qd.SelectNodes(Costanti.itm_qd_Fattura).Count, "Caricamento dei dati...."})

        For Each xmlItem As Xml.XmlNode In _xmlDati_qd.SelectNodes(Costanti.itm_qd_Fattura)
            dr_mai_qd = _QuickDataset.TBL_main.NewRow
            Try
                dr_mai_qd.MAI_codice = GetValueFromXML(xmlItem, "MAI_codice")
                dr_mai_qd.DEF_toprint = False
                dr_mai_qd.DEF_errcode = GetValueFromXML(xmlItem, "DEF_errcode")
                dr_mai_qd.DEF_errdesc = GetValueFromXML(xmlItem, "DEF_errdesc")
                dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
                dr_mai_qd.DEF_sendbymail = GetValueFromXML(xmlItem, "DEF_sendbymail").ToLower = "si"
                dr_mai_qd.MAI_num_utente = GetValueFromXML(xmlItem, "MAI_num_utente")
                dr_mai_qd.MAI_num_fattura = GetValueFromXML(xmlItem, "MAI_num_fattura")
                dr_mai_qd.MAI_nominativo = GetValueFromXML(xmlItem, "MAI_nominativo")
                dr_mai_qd.MAI_indirizzo_recapito = GetValueFromXML(xmlItem, "MAI_indirizzo_recapito")
                dr_mai_qd.MAI_citta_recapito = GetValueFromXML(xmlItem, "MAI_citta_recapito")
                dr_mai_qd.MAI_cap_recapito = GetValueFromXML(xmlItem, "MAI_cap_recapito")
                dr_mai_qd.MAI_provincia_recapito = GetValueFromXML(xmlItem, "MAI_provincia_recapito")
                dr_mai_qd.MAI_nazione_recapito = GetValueFromXML(xmlItem, "MAI_nazione_recapito")
                dr_mai_qd.MAI_print_bol = GetValueFromXML(xmlItem, "MAI_print_bol").ToLower = "si"
                dr_mai_qd.MAI_print_all = GetValueFromXML(xmlItem, "MAI_print_all").ToLower = "si"
                dr_mai_qd.MAI_filedati = GetValueFromXML(xmlItem, "MAI_filedati")
            Catch ex As Exception
                dr_mai_qd.DEF_errcode = -1
                dr_mai_qd.DEF_errdesc = ex.Message
            End Try
            ImpostaMessaggi(New String() {"", "", "", CInt(_MessaggiAvanzamento(3)) + 1, "", ""})
            _QuickDataset.TBL_main.Rows.Add(dr_mai_qd)
        Next
        LoadFileMdb()

    End Sub

#Region "Caricamento del PRINTDATASET"

    ' ************************************************************************** '
    ' Caricamento del DataSet da utilizzare per la stampa.                       '
    ' ************************************************************************** '
    Public Sub LoadPrintDataset(ByVal nRecordToLoad As Integer) Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.LoadPrintDataset
        Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow
        Dim nRecordToPrint As Integer

        ' ************************************************************************** '
        ' Creazione del Dataset e azzeramento del contatore dei record da stampare.  '
        ' ************************************************************************** '
        nRecordToPrint = 0
        Select Case _CodiceModulo
            Case Is = ListaModuli.MOD_fattura
                _PrintFatturaDataset = New DS_PF_UE_ele
            Case Is = ListaModuli.MOD_bollettino
                _PrintBollettinoDataset = New DS_PB_UE_ele
        End Select

        ' ************************************************************************** '
        ' Caricamento del DataSet da utilizzare per la stampa.                       '
        ' ************************************************************************** '
        _CodiceProgressivo = 0
        While _RecordCount < _QuickDataset.TBL_main.Rows.Count And nRecordToPrint < nRecordToLoad
            dr_mai_qd = _QuickDataset.TBL_main.Rows(_RecordCount)
            If dr_mai_qd.DEF_toprint Then
                Try
                    _ErroriLievi = ""
                    AddRecordToPrint(dr_mai_qd)
                    If _ErroriLievi > "" Then
                        dr_mai_qd.DEF_errcode = -2
                        dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori lievi: ", _ErroriLievi)
                    End If
                Catch ex As Exception
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = ex.Message
                End Try
                nRecordToPrint += 1
            End If
            _RecordCount += 1
        End While

        ' ************************************************************************** '
        ' Consolidamento del DataSet da utilizzare per la stampa.                    '
        ' ************************************************************************** '
        Select Case _CodiceModulo
            Case Is = ListaModuli.MOD_fattura
                _PrintFatturaDataset.AcceptChanges()
            Case Is = ListaModuli.MOD_bollettino
                _PrintBollettinoDataset.AcceptChanges()
        End Select

    End Sub

    Private Sub AddRecordToPrint(ByVal dr_mai_qd As DS_QD_UE_ele.TBL_mainRow)

        Try
            Select Case _CodiceModulo
                Case Is = ListaModuli.MOD_fattura
                    GetRecordFatturaToPrint(dr_mai_qd.MAI_filedati, dr_mai_qd.MAI_codice, GetFeedString(dr_mai_qd))
                Case Is = ListaModuli.MOD_bollettino
                    GetRecordBollettiniToPrint(dr_mai_qd.MAI_filedati, dr_mai_qd.MAI_codice, GetFeedString(dr_mai_qd))
            End Select
        Catch ex As Exception
            Dim cErrorString As String
            If ex.Message = "" Then
                cErrorString = "Errore caricamento dati, mentra si caricava il dato: SCONOSCIUTO"
            Else
                cErrorString = String.Concat("Errore caricamento dati, mentra si caricava il dato: ", ex.Message)
            End If
            Throw New Exception(cErrorString, ex)
        End Try

    End Sub

    Private Function GetFeedString(ByVal dr_mai_qd As DS_QD_UE_ele.TBL_mainRow) As String
        Dim lAllModuli As Boolean = True
        Dim cRtn As String

        cRtn = ""
        If _FeedString > "" Then
            For i As Integer = 1 To _FeedString.Split(";").Length - 1 Step 2
                If dr_mai_qd.Item(_FeedString.Split(";")(i)) Then
                    cRtn = _FeedString.Split(";")(i + 1)
                Else
                    lAllModuli = False
                End If
            Next
            If lAllModuli Then
                cRtn = _FeedString.Split(";")(0)
            End If
        End If
        Return cRtn

    End Function

#End Region

#Region "Caricamento del PRINTDATSET della fattura"

    Private Sub GetRecordFatturaToPrint(ByVal nCodice As Integer, ByVal nLinkQD As Integer, ByVal cFeedString As String)
        Dim dr_mai_tra As DS_import.TBL_mainRow
        Dim dr_dfa As DS_PF_UE_ele.TBL_dati_fattRow
        Dim dr_daf As DS_PF_UE_ele.TBL_dati_fornRow

        dr_mai_tra = data_import.TBL_main.FindByMAI_codice(nCodice)
        If Not dr_mai_tra Is Nothing Then
            ' Carico i dati che occorrono cos� come sono memorizzati nel database di transito.
            _PrintFatturaDataset.TBL_main.ImportRow(dr_mai_tra)
            With CType(_PrintFatturaDataset.TBL_main.Rows(_PrintFatturaDataset.TBL_main.Rows.Count - 1), DS_PF_UE_ele.TBL_mainRow)
                .MAI_linkQD = nLinkQD
                .DEF_alimimb = cFeedString
                .DEF_raccolta = 9
            End With

            For Each dr_dfa_imp As DS_import.TBL_dati_fattRow In dr_mai_tra.GetChildRows("TBL_main_TBL_dati_fatt")
                _PrintFatturaDataset.TBL_dati_fatt.ImportRow(dr_dfa_imp)
                dr_dfa = _PrintFatturaDataset.TBL_dati_fatt.Rows(_PrintFatturaDataset.TBL_dati_fatt.Rows.Count - 1)

                dr_dfa.DFA_sv_des_bre = dr_dfa.DFA_serv_vend_des
                If dr_dfa.DFA_sv_des_bre.Contains(")") Then
                    dr_dfa.DFA_sv_des_bre = dr_dfa.DFA_sv_des_bre.Substring(dr_dfa.DFA_sv_des_bre.IndexOf(")") + 1).Trim
                End If
                dr_dfa.DFA_sr_des_bre = dr_dfa.DFA_serv_rete_des
                If dr_dfa.DFA_sr_des_bre.Contains(")") Then
                    dr_dfa.DFA_sr_des_bre = dr_dfa.DFA_sr_des_bre.Substring(dr_dfa.DFA_sr_des_bre.IndexOf(")") + 1).Trim
                End If
                dr_dfa.DFA_imp_des_bre = dr_dfa.DFA_imp_des
                If dr_dfa.DFA_imp_des_bre.Contains(")") Then
                    dr_dfa.DFA_imp_des_bre = dr_dfa.DFA_imp_des_bre.Substring(dr_dfa.DFA_imp_des_bre.IndexOf(")") + 1).Trim
                End If

                dr_dfa.DFA_tfe_des_bre = dr_dfa.DFA_tot_forn_ener_des
                If dr_dfa.DFA_tfe_des_bre.Contains(")") Then
                    dr_dfa.DFA_tfe_des_bre = dr_dfa.DFA_tfe_des_bre.Substring(dr_dfa.DFA_tfe_des_bre.IndexOf(")") + 1).Trim
                End If
                If dr_dfa.DFA_tfe_des_bre.Contains("(") And dr_dfa.DFA_tfe_des_bre.Contains(")") Then
                    dr_dfa.DFA_tfe_des_bre = dr_dfa.DFA_tfe_des_bre.Remove(dr_dfa.DFA_tfe_des_bre.IndexOf("("), dr_dfa.DFA_tfe_des_bre.IndexOf(")") - dr_dfa.DFA_tfe_des_bre.IndexOf("(") + 1)
                End If
                dr_dfa.DFA_tot_od_bre = dr_dfa.DFA_tot_one_div_des
            Next

            For Each dr_daf_imp As DS_import.TBL_dati_fornRow In dr_mai_tra.GetChildRows("TBL_main_TBL_dati_forn")
                _PrintFatturaDataset.TBL_dati_forn.ImportRow(dr_daf_imp)
                dr_daf = _PrintFatturaDataset.TBL_dati_forn.Rows(_PrintFatturaDataset.TBL_dati_forn.Rows.Count - 1)
                dr_daf.DAF_uten_dom = dr_daf.DAF_tipo_contr.Trim.ToLower = "utenza domestica"
            Next

            For Each dr_tra_imp As DS_import.TBL_trasparenzaRow In dr_mai_tra.GetChildRows("TBL_main_TBL_trasparenza")
                _PrintFatturaDataset.TBL_trasparenza.ImportRow(dr_tra_imp)
            Next

            For Each dr_com_imp As DS_import.TBL_comunicazioniRow In dr_mai_tra.GetChildRows("TBL_main_TBL_comunicazioni")
                _PrintFatturaDataset.TBL_comunicazioni.ImportRow(dr_com_imp)
            Next

            For Each dr_con_imp As DS_import.TBL_consumiRow In dr_mai_tra.GetChildRows("TBL_main_TBL_consumi")
                _PrintFatturaDataset.TBL_consumi.ImportRow(dr_con_imp)
            Next

            For Each dr_rco_imp As DS_import.TBL_riep_consRow In dr_mai_tra.GetChildRows("TBL_main_TBL_riep_cons")
                _PrintFatturaDataset.TBL_riep_cons.ImportRow(dr_rco_imp)
            Next

            For Each dr_rci_imp As DS_import.TBL_riep_cons_indRow In dr_mai_tra.GetChildRows("TBL_main_TBL_riep_cons_ind")
                _PrintFatturaDataset.TBL_riep_cons_ind.ImportRow(dr_rci_imp)
            Next

            GetCredenzialiWeb()
        End If

    End Sub

    Private Sub LoadFileMdb()
        Dim oComBuil As OleDb.OleDbCommandBuilder
        Dim oDa As OleDb.OleDbDataAdapter
        Dim oCmd As OleDb.OleDbCommand
        Dim cDBFileName As String
        Dim cConnACCStr As String

        data_import = New DS_import
        cDBFileName = _SessioneDati.SelectSingleNode("filename[@tipo='103']").InnerText
        cDBFileName = String.Concat(_SessionePath, cDBFileName)
        cConnACCStr = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cDBFileName, ";")

        oCmd = New OleDb.OleDbCommand
        oCmd.Connection = New OleDb.OleDbConnection(cConnACCStr)

        ' Salvataggio della tabella TBL_main
        oCmd.CommandText = "SELECT * FROM TBL_main"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(data_import.TBL_main)

        ' Salvataggio della tabella TBL_main
        oCmd.CommandText = "SELECT * FROM TBL_dati_fatt"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(data_import.TBL_dati_fatt)

        ' Salvataggio della tabella TBL_main
        oCmd.CommandText = "SELECT * FROM TBL_dati_forn"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(data_import.TBL_dati_forn)

        ' Salvataggio della tabella TBL_main
        oCmd.CommandText = "SELECT * FROM TBL_trasparenza ORDER BY TRA_codice"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(data_import.TBL_trasparenza)

        ' Salvataggio della tabella TBL_main
        oCmd.CommandText = "SELECT * FROM TBL_comunicazioni"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(data_import.TBL_comunicazioni)

        ' Salvataggio della tabella TBL_consumi
        oCmd.CommandText = "SELECT * FROM TBL_consumi"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(data_import.TBL_consumi)

        ' Salvataggio della tabella TBL_riep_cons
        oCmd.CommandText = "SELECT * FROM TBL_riep_cons"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(data_import.TBL_riep_cons)

        ' Salvataggio della tabella TBL_riep_cons_ind
        oCmd.CommandText = "SELECT * FROM TBL_riep_cons_ind"
        oDa = New OleDb.OleDbDataAdapter(oCmd)
        oComBuil = New OleDb.OleDbCommandBuilder(oDa)
        oDa.Fill(data_import.TBL_riep_cons_ind)

    End Sub

    Private Sub CheckFieldValue(ByVal cValue As String, ByVal cMsgError As String)

        If cValue = "" And cMsgError > "" Then
            AddErroreLieve(cMsgError)
        End If

    End Sub

    Private Function SetDataScadenza(ByVal cFlagTipoPag As String, ByVal dDataForzata As String) As String
        Dim cCondizioneForzaScadenza As String

        cCondizioneForzaScadenza = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "17")
        If (cCondizioneForzaScadenza.Length > 0) And (cCondizioneForzaScadenza.ToUpper.Contains(cFlagTipoPag.ToUpper)) Then
            dDataForzata = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "16")
        End If
        Return dDataForzata

    End Function

#End Region

#Region "Caricamento del PRINTDATSET del bollettino."

    Private Sub GetRecordBollettiniToPrint(ByVal nCodice As Integer, ByVal nLinkQD As Integer, ByVal cFeedString As String)
        Dim dr_fxr As DS_PB_UE_ele.TBL_fat_x_ratRow
        Dim dr_rdo As DS_PB_UE_ele.TBL_rich_domRow
        Dim dr_fat As DS_PB_UE_ele.TBL_fattureRow
        Dim dr_rat As DS_PB_UE_ele.TBL_rateRow
        Dim dr_mai_tra As DS_import.TBL_mainRow
        Dim dr_dfa_tra As DS_import.TBL_dati_fattRow
        Dim dr_daf_tra As DS_import.TBL_dati_fornRow
        Dim nRate As Integer

        dr_mai_tra = data_import.TBL_main.FindByMAI_codice(nCodice)
        dr_dfa_tra = dr_mai_tra.GetChildRows("TBL_main_TBL_dati_fatt")(0)

        ' Carico per ciascuna fattura un record sulla tabella delle fatture e tanti record su quella dei bollettini.
        ' cos� posso far guidare la stampa dei bollettini da una tabella che originer� successivamente.
        dr_fat = _PrintBollettinoDataset.TBL_fatture.NewRow
        With dr_fat
            .FAT_codice = _PrintBollettinoDataset.TBL_fatture.Rows.Count + 1
            '.FAT_prot_reg_iva = GetValueFromXML(xmlFattura, Costanti.itm_prot_reg_iva)
            '.FAT_num_fattura = GetValueFromXML(xmlFattura, Costanti.itm_num_fattura)
            '.FAT_data_fattura = GetValueFromXML(xmlFattura, Costanti.itm_data_emiss)
            .FAT_nominativo = dr_mai_tra.MAI_nominativo
            .FAT_ind_rec_1 = dr_mai_tra.MAI_int_sito_1
            .FAT_ind_rec_2 = dr_mai_tra.MAI_int_sito_2
            .FAT_ind_rec_3 = dr_mai_tra.MAI_int_sito_3
            .FAT_ind_rec_4 = dr_mai_tra.MAI_int_sito_4
            .FAT_ind_rec_5 = dr_mai_tra.MAI_int_sito_5
        End With
        _PrintBollettinoDataset.TBL_fatture.Rows.Add(dr_fat)

        ' Carico i record nella tabella delle rate.
        nRate = dr_mai_tra.GetChildRows("TBL_main_TBL_dati_fatt").Length
        For i As Integer = 0 To nRate - 1
            dr_dfa_tra = dr_mai_tra.GetChildRows("TBL_main_TBL_dati_fatt")(i)
            dr_daf_tra = dr_mai_tra.GetChildRows("TBL_main_TBL_dati_forn")(i)
            dr_rat = _PrintBollettinoDataset.TBL_rate.NewRow
            dr_fat.FAT_cod_cli = dr_mai_tra.MAI_cod_cli
            dr_fat.FAT_num_fattura = dr_dfa_tra.DFA_num_fatt
            dr_fat.FAT_data_fattura = dr_dfa_tra.DFA_data_emis
            With dr_rat
                .RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count + 1
                .RAT_num_rata = ""
                .RAT_scadenza = dr_dfa_tra.DFA_data_scad
                .RAT_emissione = dr_dfa_tra.DFA_data_emis
                .RAT_per_rif = dr_dfa_tra.DFA_per_rif
                .RAT_matricola = dr_mai_tra.MAI_cod_cli
                .RAT_importo = dr_dfa_tra.DFA_totale_fatt
                .RAT_ocr = dr_dfa_tra.DFA_ocr
            End With
            _PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat)

            ' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
            dr_fxr = _PrintBollettinoDataset.TBL_fat_x_rat.NewRow
            dr_fxr.FXR_codice = _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Count + 1
            dr_fxr.DEF_alimimb = cFeedString
            ' per il bollettino non dovrebbero esserci problemi di raccolta.
            ' Al momento il campo viene valorizzato con questi valori per le fasi successive si vedr�.
            dr_fxr.DEF_raccolta = 3
            dr_fxr.FXR_linkQD = nLinkQD
            dr_fxr.FXR_cod_fat = dr_fat.FAT_codice
            dr_fxr.FXR_cod_rat = dr_rat.RAT_codice

            ' Mi serve per controllare il codice dell'imbustatrice.
            If i = nRate - 1 Then
                dr_fxr.FXR_ultimo = 1
            End If
            _PrintBollettinoDataset.TBL_fat_x_rat.Rows.Add(dr_fxr)
        Next

        ' Carico i record nella richiesta di domiciliazione 
        dr_rdo = _PrintBollettinoDataset.TBL_rich_dom.NewRow
        With dr_rdo
            .RDO_codice = _PrintBollettinoDataset.TBL_rich_dom.Rows.Count + 1
            .RDO_cod_fat = dr_fat.FAT_codice
            .RDO_cod_sia = GetValueDatiFissi("FixBol", "6")
            .RDO_tipo_serv = GetValueDatiFissi("FixBol", "7")
            .RDO_cod_domic = ""
        End With
        _PrintBollettinoDataset.TBL_rich_dom.Rows.Add(dr_rdo)

    End Sub

#End Region

    Private Sub GetCredenzialiWeb()
        Dim strCredWeb As System.IO.StreamReader
        Dim nColPwd As Integer = 0
        Dim nColCF As Integer = 0
        Dim nColPI As Integer = 0
        Dim cFileName As String
        Dim cPassword As String
        Dim i As Integer = 0
        Dim cLine As String
        Dim dr_mai As DS_PF_UE_ele.TBL_mainRow

        Try
            dr_mai = _PrintFatturaDataset.TBL_main.Rows(_PrintFatturaDataset.TBL_main.Rows.Count - 1)
            cFileName = String.Concat(_SessionePath, _SessioneDati.SelectSingleNode(String.Concat("filename[@tipo=", "", 102, "", "]")).InnerText)
            strCredWeb = New System.IO.StreamReader(cFileName)

            ' Dalla prima riga legge i nomi della colonna.
            cLine = strCredWeb.ReadLine

            ' recupera il numero di colonna della PASSWORD, del Codice Fiscale, della Partita IVA
            For Each cTmp As String In cLine.Split(";")
                'If cTmp.ToLower = "codice_fiscale" Then
                ' nColCF = i
                ' End If
                ' If cTmp.ToLower = "partita_iva" Then
                ' nColPI = i
                ' End If
                'If cTmp.ToLower = "password" Then
                'nColPwd = i
                'End If
                If cTmp.ToLower = "nomeutente" Then
                    nColCF = i
                    nColPI = i
                End If
                If cTmp.ToLower = "sito_pw_iniziale" Then
                    nColPwd = i
                End If
                i += 1
            Next
            cPassword = ""
            If (nColCF * nColPwd) > 0 Then
                While Not strCredWeb.EndOfStream
                    cLine = strCredWeb.ReadLine
                    If dr_mai.MAI_cfpi > "" And cLine.Split(";")(nColPI) = dr_mai.MAI_cfpi Then
                        cPassword = cLine.Split(";")(nColPwd)
                    End If
                End While
            End If
            strCredWeb.Close()
        Catch ex As Exception
            cPassword = ""
            AddErroreLieve("Recupero credenziali accesso web")
        End Try
        dr_mai.MAI_wa_username = "NON UTILIZZATO"
        dr_mai.MAI_wa_password = cPassword
        dr_mai.MAI_web_access = dr_mai.MAI_wa_password > ""

    End Sub

    Private Function GetValueFromXML(ByVal xmlItem As Xml.XmlNode, ByVal xmlPath As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cDefault
            If Not xmlItem.SelectSingleNode(xmlPath) Is Nothing Then
                cValue = xmlItem.SelectSingleNode(xmlPath).InnerText
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori del file XML.", vbLf, "percorso da cui si tenta il recupero:", vbLf, xmlPath), ex)
        End Try
        Return cValue

    End Function

    Private Function GetValueFromText(ByVal cText As String, ByVal nStart As Integer, ByVal nLength As Integer, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cText.Substring(nStart, nLength)
            If cValue.Trim = "" And cDefault > "" Then
                cValue = cDefault
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori da un testo.", vbLf, "Dati per il recupero: ", vbLf, "- inizio ", nStart, vbLf, "- fine ", nLength), ex)
        End Try
        Return cValue

    End Function

    Private Sub AddErroreLieve(ByVal cMsg As String)

        If _ErroriLievi = "" Then
            _ErroriLievi = cMsg
        Else
            _ErroriLievi = String.Concat(_ErroriLievi, "; ", cMsg)
        End If

    End Sub

    Private Sub ImpostaMessaggi(ByVal aValue As String())
        Dim i As Integer

        For i = 0 To 5
            If aValue(i) > "" Then
                _MessaggiAvanzamento(i) = aValue(i)
            End If
        Next

    End Sub

    Private Function GetTrasformazioni(ByVal cTable As String, ByVal cCodice As String, ByVal nTipo As Integer) As String
        Dim xdTrasf As Xml.XmlDocument
        Dim xiWork As Xml.XmlNode
        Dim cRtn As String

        Try
            xdTrasf = New Xml.XmlDocument
            xdTrasf.Load(_SessioneTrasformazioniFileName)
            xiWork = xdTrasf.SelectSingleNode(String.Concat("translation/", cTable, "/item[codice = '", cCodice, "']"))
            cRtn = ""
            If Not xiWork Is Nothing Then
                If nTipo = 1 Then
                    cRtn = xiWork.SelectSingleNode("descr_breve").InnerText
                End If
                If nTipo = 2 Then
                    cRtn = xiWork.SelectSingleNode("descrizione").InnerText
                End If
            End If
            xdTrasf = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    Public Sub PostPreStampa(ByVal aLista As ArrayList) Implements PLUGIN_interfaceV1_3.IPLG_dati_lib.PostPreStampa
        'Dim dr_elp As DS_PF_UE_ele.TBL_elenco_podRow
        'Dim nPagine As Integer
        'Dim nElencoPod As Integer
        'Dim nStatusRaccolta As Integer
        'Dim i As Integer

        'If Not _PrintFatturaDataset Is Nothing Then
        '    nElencoPod = -1
        '    For i = 0 To aLista.Count - 1
        '        nStatusRaccolta = CType(_PrintFatturaDataset.TBL_main.Rows(i), DS_PF_UE_ele.TBL_mainRow).DEF_raccolta
        '        Select Case nStatusRaccolta
        '            Case Is = 1
        '                nPagine = aLista(i)
        '            Case Is = 2, 3
        '                nElencoPod += 1
        '                dr_elp = _PrintFatturaDataset.TBL_elenco_pod.Rows(nElencoPod)
        '                dr_elp.ELP_page_num = nPagine + 1
        '                dr_elp.AcceptChanges()
        '                nPagine += aLista(i)
        '        End Select
        '    Next
        'End If

    End Sub

    Private Function GetDataSendByMail() As Xml.XmlNode
        'Dim xnWork As Xml.XmlNode
        'Dim xdWork As Xml.XmlDocument

        'xdWork = New Xml.XmlDocument
        'xnWork = xdWork.CreateNode(Xml.XmlNodeType.Element, "SendByMail", "")
        'Select Case _CodiceModulo
        '    Case Is = ListaModuli.MOD_fattura
        '        With CType(_PrintFatturaDataset.TBL_main.Rows(0), DS_PF_UE_ele.TBL_mainRow)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_codice", .MAI_linkQD)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_cod_utente", .MAI_cod_cli)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_nominativo", .MAI_nome)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_mail_address", "lproietti@gmail.com")
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_codice", .MAI_linkQD.ToString))
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_cod_utente", .MAI_cod_cli.ToString))
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_nominativo", .MAI_nome.ToString))
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_mail_address", "lproietti@gmail.com"))
        '        End With
        'End Select
        'Return xnWork

    End Function

    Private Sub CreateXmlNode(ByVal xdDoc As Xml.XmlDocument, ByVal xnNode As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xdDoc.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xnNode.AppendChild(xiTmp)

    End Sub

#Region "Elenco procedure Generiche"

    Private Function SetExceptionItem(ByVal cProcError As String, ByVal ex As Exception) As Exception
        Dim ex_rtn As Exception

        If ex.InnerException Is Nothing Then
            ex_rtn = New Exception(String.Concat("Errore (", cProcError, "): ", ex.Message), ex)
        Else
            ex_rtn = ex
        End If
        Return ex_rtn

    End Function

#End Region

End Class

