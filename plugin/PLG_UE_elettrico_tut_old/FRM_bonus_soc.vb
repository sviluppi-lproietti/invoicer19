Imports System.Windows.Forms

Public Class FRM_bonus_soc

    Private lModified As Boolean

    Private Sub FRM_autolettura_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lModified = False

    End Sub

    Private Sub BTN_salva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_salva.Click

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()

    End Sub

    Private Sub BTN_ignora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTN_ignora.Click
        Dim lClose As Boolean

        lClose = Not lModified
        If Not lClose Then
            lClose = MessageBox.Show("Attenzione. Le modifiche apportate al testo andranno perse. Proseguo comunque?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes
        End If
        If lClose Then
            Me.DialogResult = Windows.Forms.DialogResult.Ignore
            Me.Close()
        End If

    End Sub

    Private Sub TXB_autolettura_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TXB_bonus_sociale.TextChanged

        lModified = True

    End Sub

End Class