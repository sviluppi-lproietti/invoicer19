Public Class Costanti

    Public Const DatiFissi_Fattura = "FixFat"
    Public Const KEY_DF_Periodo_fatturazione = 14
    Public Const KEY_DF_autolettura_stampa = 10
    Public Const KEY_DF_autolettura_testo = 11
    Public Const KEY_DF_bonus_soc_de = 20
    Public Const KEY_DF_box_commerciale = 22
    Public Const KEY_DF_forza_scacciapensieri = 23

    Public Const SEZ_no_sezione = -1
    Public Const SEZ_SezioneVendita = 7
    Public Const SEZ_Sezionerete = 6
    Public Const SEZ_Imposte = 8
    Public Const SEZ_OneriDiversi = 9
    Public Const SEZ_Rielabora = 1000

    Public Const itm_qd_Fattura = "FATTURA/FATTURA_ROW"

    Public Const itm_Fattura = "EMISSIONE/EMISSIONE_ROW"

    Public Const itm_SEZ_fatt = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW"
    Public Const itm_Testata_Fattura_row = "FATTURA/TESTATA_FATTURA/TESTATA_FATTURA_ROW"
    Public Const itm_COM_Testata_Fattura_row = itm_Fattura + "/" + itm_Testata_Fattura_row
    Public Const itm_POD_fatturati = itm_Testata_Fattura_row + "/RIEPILOGO_X_POD/RIEPILOGOXPOD_ROW"
    Public Const itm_stm_dal = "DAL"
    Public Const itm_stm_al = "AL"
    ' Dati dell'intestatario della fattura
    Public Const itm_nome = itm_Testata_Fattura_row + "/RAGIONE_SOCIALE_INTESTATARIO"
    Public Const itm_num_ute = itm_Testata_Fattura_row + "/ID_INTESTATARIO"
    Public Const itm_num_fattura = itm_Testata_Fattura_row + "/NUMERO_FATTURA"
    Public Const itm_rec_nome = itm_Testata_Fattura_row + "/RECAPITO_FATTURA"
    Public Const itm_rec_sito_1 = itm_Testata_Fattura_row + "/TIPO_ELETOPO_SPEDIZIONE"
    Public Const itm_rec_sito_2 = itm_Testata_Fattura_row + "/ELETOPO_SPEDIZIONE"
    Public Const itm_rec_sito_3 = itm_Testata_Fattura_row + "/NUMERO_SPEDIZIONE"
    Public Const itm_rec_sito_4 = itm_Testata_Fattura_row + "/SUFFISSO_SPEDIZIONE"
    Public Const itm_rec_sito_5 = itm_Testata_Fattura_row + "/LOCALITA_SPEDIZIONE"
    Public Const itm_rec_sito_6 = itm_Testata_Fattura_row + "/COMUNE_SPEDIZIONE"
    Public Const itm_rec_sito_7 = itm_Testata_Fattura_row + "/CAP_SPEDIZIONE"
    Public Const itm_rec_sito_8 = itm_Testata_Fattura_row + "/PROVINCIA_SIGLA_SPEDIZIONE"
    Public Const itm_rec_nazione = itm_Testata_Fattura_row + "/NAZIONE_SPEDIZIONE"
    Public Const itm_email_spedizione = itm_Testata_Fattura_row + "/EMAIL_SPEDIZIONE"
    Public Const itm_prot_reg_iva = itm_Testata_Fattura_row + "/LABEL_PROTOCOLLO_REG_IVA"

    Public Const itm_cf = itm_Testata_Fattura_row + "/CODICE_FISCALE_INTESTATARIO"
    Public Const itm_piva = itm_Testata_Fattura_row + "/PARTITA_IVA_INTESTATARIO"

    'Public Const itm_cod_cli = itm_Testata_Fattura_row + "/ID_INTESTATARIO"
    Public Const itm_ind_SedeLeg_1 = itm_Testata_Fattura_row + "/TIPO_ELETOPO_SEDELEGALE"
    Public Const itm_ind_SedeLeg_2 = itm_Testata_Fattura_row + "/ELETOPO_SEDELEGALE"
    Public Const itm_ind_SedeLeg_3 = itm_Testata_Fattura_row + "/NUMERO_SEDELEGALE"
    Public Const itm_ind_SedeLeg_4 = itm_Testata_Fattura_row + "/SUFFISSO_SEDELEGALE"
    Public Const itm_ind_SedeLeg_5 = itm_Testata_Fattura_row + "/LOCALITA_SEDELEGALE"
    Public Const itm_ind_SedeLeg_6 = itm_Testata_Fattura_row + "/COMUNE_SEDELEGALE"
    Public Const itm_ind_SedeLeg_7 = itm_Testata_Fattura_row + "/CAP_SEDELEGALE"
    Public Const itm_ind_SedeLeg_8 = itm_Testata_Fattura_row + "/PROVINCIA_SIGLA_SEDELEGALE"
    Public Const itm_int_nazione = itm_Testata_Fattura_row + "/NAZIONE_SEDELEGALE"

    Public Const itm_SEZ_msg_contr = "MESSAGGI_CONTRATTO/MESSAGGI_CONTRATTO_ROW"
    Public Const itm_SEZ_msg_raggr = itm_Testata_Fattura_row + "/MESSAGGI_RAGGRUPPAMENTO/MESSAGGI_RAGGRUPPAMENTO_ROW"

    Public Const SEZ_elenco_distrib = itm_Testata_Fattura_row + "/ELENCO_DISTRIBUTORI/ELENCO_DISTRIBUTORI_ROW"

    Public Const itm_DIS_nome = "RAGIONE_SOCIALE_DISTRIBUTORE"
    Public Const itm_DIS_telefono = "NUMERO_VERDE_DISTRIBUTORE"
    Public Const itm_COM_DIS_nome = SEZ_elenco_distrib + "/" + itm_DIS_nome
    Public Const itm_COM_DIS_telefono = SEZ_elenco_distrib + "/" + itm_DIS_telefono

    'Public Const itm_DIS_nome = SEZ_elenco_distrib + "/RAGIONE_SOCIALE_DISTRIBUTORE"
    'Public Const itm_DIS_telefono = SEZ_elenco_distrib + "/NUMERO_VERDE_DISTRIBUTORE"

    Public Const itm_GruppiMisura = "CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/GRUPPO_MISURA_PUNTO"

    Public Const itm_print_bol = itm_Testata_Fattura_row + "/STAMPA_BOLLETTINO"
    Public Const itm_SEZ_fatt_com = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW"

    ' ************************************************************************************ '
    ' Tag per il recupero delle informazioni dalla sezione del POD.                        '
    ' ************************************************************************************ '
    Public Const itm_POD_idubicazione = "ID_UBICAZIONE"
    Public Const itm_POD_numero = "POD_PDR"
    Public Const itm_descr_contratto = "CONTRATTO/CONTRATTO_ROW/DESCR_TIPO_CONTRATTO"
    Public Const itm_anno_contratto = "CONTRATTO/CONTRATTO_ROW/ANNO_CONTRATTO"
    Public Const itm_num_contratto = "CONTRATTO/CONTRATTO_ROW/ID_CONTRATTO"

    Public Const itm_opt_param = "PARAMETRI_OT/PARAMETRI_OT_ROW"
    Public Const itm_campi_contr = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/CAMPI_CONTRATTO/CAMPI_CONTRATTO_ROW"
    Public Const itm_opz_tar = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/OPZIONI_TARIFFARIE/OPZIONI_TARIFFARIE_ROW" 'DATA_INIZIO_CONSUMO"
    Public Const itm_campi_punto_forn = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/PUNTO_FORNITURA/PUNTO_FORNITURA_ROW/CAMPI_PUNTO_FORNITURA/CAMPI_PUNTO_FORNITURA_ROW"

    Public Const itm_data_ini_fat = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/DATE_COMPETENZA/DATE_COMPETENZA_ROW/INIZIO_RIPETITIVI" 'DATA_INIZIO_CONSUMO"
    Public Const itm_data_fin_fat = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/DATE_COMPETENZA/DATE_COMPETENZA_ROW/FINE_RIPETITIVI"    'DATA_FINE_CONSUMO"
    Public Const itm_punto_fornitura = itm_Testata_Fattura_row + "/SEZIONE_FATTURA/SEZIONE_FATTURA_ROW/CONTRATTO/CONTRATTO_ROW/ID_PUNTO_FORNITURA"

    Public Const itm_COM_banca_appoggio = itm_Testata_Fattura_row + "/BANCA_APPOGGIO/BANCA_APPOGGIO_ROW"
    Public Const itm_nome_banca_appoggio = itm_COM_banca_appoggio + "/NOME_BANCA"
    Public Const itm_iban_appoggio = itm_COM_banca_appoggio + "/CODICE_IBAN"

    Public Const itm_COM_banca_cliente = itm_Testata_Fattura_row + "/BANCA_CLIENTE/BANCA_CLIENTE_ROW"
    Public Const itm_nome_banca_cliente = itm_COM_banca_cliente + "/NOME_BANCA"
    Public Const itm_iban_cliente = itm_COM_banca_cliente + "/CODICE_IBAN"

    Public Const itm_anno_fattura = itm_Testata_Fattura_row + "/ANNO_FATTURA"
    Public Const itm_data_emiss = itm_Testata_Fattura_row + "/DATA_FATTURA"
    Public Const itm_tbs_totale = itm_Testata_Fattura_row + "/TOTALE_FATTURA"
    Public Const itm_flag_tipo_pag = itm_Testata_Fattura_row + "/FLAG_TIPO_PAGAMENTO"
    Public Const itm_descr_tipo_pag = itm_Testata_Fattura_row + "/TIPO_PAGAMENTO"
    Public Const itm_tot_imponibile = itm_Testata_Fattura_row + "/PIEDE_FATTURA/PIEDE_FATTURA_ROW/IMPORTO_IMPONIBILE"
    Public Const itm_tot_iva = itm_Testata_Fattura_row + "/PIEDE_FATTURA/PIEDE_FATTURA_ROW/IMPORTO_IVA"
    Public Const itm_scadenza = itm_Testata_Fattura_row + "/RATA/RATA_ROW/DATA_SCADENZA"

    Public Const itm_GruppoMisura = "GRUPPO_MISURA_PUNTO_ROW"
    Public Const itm_insoluti = itm_Testata_Fattura_row + "/INSOLUTI/INSOLUTI_ROW"

    Private Const SEZ_dati_ub_pf = "CONTRATTO/CONTRATTO_ROW/SITO_FORNITURA/SITO_FORNITURA_ROW/"
    Private Const EST_ub_pf = "_SITO"
    Public Const itm_pf_descr = SEZ_dati_ub_pf + "DESCRIZIONE_SITO"
    Public Const itm_pf_sito_1 = SEZ_dati_ub_pf + "TIPO_ELETOPO" + EST_ub_pf
    Public Const itm_pf_sito_2 = SEZ_dati_ub_pf + "ELETOPO" + EST_ub_pf
    Public Const itm_pf_sito_3 = SEZ_dati_ub_pf + "NUMERO" + EST_ub_pf
    Public Const itm_pf_sito_4 = SEZ_dati_ub_pf + "SUFFISSO" + EST_ub_pf
    Public Const itm_pf_sito_5 = SEZ_dati_ub_pf + "CAP" + EST_ub_pf
    Public Const itm_pf_sito_6 = SEZ_dati_ub_pf + "COMUNE" + EST_ub_pf
    Public Const itm_pf_sito_7 = SEZ_dati_ub_pf + "PROVINCIA_SIGLA" + EST_ub_pf
    Public Const itm_pf_sito_8 = SEZ_dati_ub_pf + "NAZIONE" + EST_ub_pf

    Public Const itm_SEZ_contratto = "CONTRATTO/CONTRATTO_ROW/"
    Public Const itm_inizio_gestione = itm_SEZ_contratto + "DATA_ATTIVAZIONE_FORNITURA"


    ' Righe di fatturazione
    Public Const itm_SEZ_righe_fatt = "SEZ_STAMPA_RFATTURA/SEZ_STAMPA_RFATTURA_ROW"
    Public Const itm_COM_sezione_consumi = "SEZIONE_LETTURE_CONSUMI/SEZIONE_LETTURE_CONSUMI_ROW"
    Public Const itm_fattura_multi = "MULTIPOS"
    Public Const TipoFattura_NoTi = 0
    Public Const TipoFattura_Cong = 1
    Public Const TipoFattura_Acco = 2
    Public Const TipoFattura_CoAc = 3

    Public Const OFF_scacciapensieri = "scacciapensieri"

    Public Const itm_ID_Voce_rata_costante_scacciapensieri = 456          '  identificativo della voce RATA ACCONTO COSTANTE SCACCIAPENSIERI

End Class
