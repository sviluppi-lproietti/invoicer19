﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication6.Entity
{
    public class row_030_20Entity : rowBase
    {
        public row_030_20Entity(string cLinea) : base(cLinea)
        {

        }

        public string codice_fattura { get; set; }
        public string descrizione_totale { get; set; }
        public string unita_misura { get; set; }
        public decimal importo_utenza { get; set; }
    }
}
