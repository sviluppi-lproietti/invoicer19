Public Class Costanti

    Public Const DatiFissi_Fattura = "FixFat"

    Public Const DatiFissi_F24 = "FixBol"
    Public Const KEY_DF24_anno = 16
    
    Public Const SEZ_no_sezione = -1
    Public Const SEZ_SezioneVendita = 7
    Public Const SEZ_Sezionerete = 6
    Public Const SEZ_Imposte = 8
    Public Const SEZ_OneriDiversi = 9
    Public Const SEZ_Rielabora = 1000

    Public Const itm_qd_Fattura = "FATTURA/FATTURA_ROW"

End Class
