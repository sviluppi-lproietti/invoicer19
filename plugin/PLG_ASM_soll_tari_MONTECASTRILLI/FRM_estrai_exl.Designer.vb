<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FRM_estrai_exl
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BTN_genera = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.TXB_num_lotto = New System.Windows.Forms.TextBox
        Me.BTN_ignora = New System.Windows.Forms.Button
        Me.SFD_filename = New System.Windows.Forms.SaveFileDialog
        Me.SuspendLayout()
        '
        'BTN_genera
        '
        Me.BTN_genera.Location = New System.Drawing.Point(37, 40)
        Me.BTN_genera.Name = "BTN_genera"
        Me.BTN_genera.Size = New System.Drawing.Size(75, 23)
        Me.BTN_genera.TabIndex = 0
        Me.BTN_genera.Text = "Salva"
        Me.BTN_genera.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Numero lotto"
        '
        'TXB_num_lotto
        '
        Me.TXB_num_lotto.Location = New System.Drawing.Point(165, 6)
        Me.TXB_num_lotto.Multiline = True
        Me.TXB_num_lotto.Name = "TXB_num_lotto"
        Me.TXB_num_lotto.Size = New System.Drawing.Size(89, 21)
        Me.TXB_num_lotto.TabIndex = 2
        '
        'BTN_ignora
        '
        Me.BTN_ignora.Location = New System.Drawing.Point(154, 40)
        Me.BTN_ignora.Name = "BTN_ignora"
        Me.BTN_ignora.Size = New System.Drawing.Size(75, 23)
        Me.BTN_ignora.TabIndex = 7
        Me.BTN_ignora.Text = "Ignora"
        Me.BTN_ignora.UseVisualStyleBackColor = True
        '
        'FRM_estrai_exl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(264, 76)
        Me.Controls.Add(Me.BTN_ignora)
        Me.Controls.Add(Me.TXB_num_lotto)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BTN_genera)
        Me.Name = "FRM_estrai_exl"
        Me.Text = "Estrazione del file riassuntivo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BTN_genera As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TXB_num_lotto As System.Windows.Forms.TextBox
    Friend WithEvents BTN_ignora As System.Windows.Forms.Button
    Friend WithEvents SFD_filename As System.Windows.Forms.SaveFileDialog
End Class
