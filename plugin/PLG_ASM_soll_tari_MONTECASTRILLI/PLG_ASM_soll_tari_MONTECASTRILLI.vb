Imports System.Windows.Forms
Imports PLG_ASM_soll_tari_MONTECASTRILLI.Costanti

Public Class PLG_ASM_soll_tari_MONTECASTRILLI
    Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib

    Const UpperFileSize = 3000000
    Const LowerFileSize = 2000000


    Const cns_tsm_sel_contr_attivo = "TSM_sel_contr_attivo"
    Const cns_tsm_sel_contr_chiuso = "TSM_sel_contr_chiuso"

    Private _CodiceProgressivo As Integer
    Private _SessioneDati As Xml.XmlNode
    Private _SessioneTrasformazioniFileName As String
    Private _SessioneNuova As Boolean
    Private _SessionePath As String
    Private _CodiceModulo As Integer
    Private _FeedString As String
    Private _QuickDataset As DS_QD_ASM_sol
    Private _PrintSollecitoDataset As DS_PS_ASM_sol
    'Private _PrintBollettinoDataset As DS_PB_ASM_sol
    Private _PrintF24Dataset As DS_PF24_ASM_sol
    'Private _PrintReportNoPrint As DS_PR1_ASM_sol
    Private _PrintReportSaldoPrint As DS_PR3_ASM_sol
    Private _PrintReportSolleciti As DS_PR2_ASM_sol
    Private _RecordCount As Integer
    Private _xmlDati_qd As Xml.XmlDocument
    Private _MessaggiAvanzamento(5) As String
    Private _ErroriLievi As String
    Private _SessioneDatiFissiFileName As String
    Private _SessioneDatiPath As String
    Private _LogDS As DataSet
    Private _ResultAL As ArrayList
    Private _MasterRecord As Integer
    Private _NuoviToponimi As Boolean
    Private _DbGiriConnString As String
    Private _ListaGiri As ArrayList
    Private _TipoCaricamento As Integer

    Enum ListaModuli
        MOD_quickdataset = 0
        MOD_sollecito = 1
        MOD_bollettino = 2
        MOD_reportsolleciti = 3
        MOD_reportsaldoprint = 4
        MOD_reporteccedprint = 5
    End Enum

    ReadOnly Property ResultAL() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ResultAL

        Get
            Return _ResultAL
        End Get

    End Property

    WriteOnly Property SessioneDatiPath() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneDatiPath

        Set(ByVal value As String)
            _SessioneDatiPath = value
        End Set

    End Property

    WriteOnly Property SessioneTrasformazioniFileName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneTrasformazioniFileName

        Set(ByVal value As String)
            _SessioneTrasformazioniFileName = value
        End Set

    End Property

    WriteOnly Property SessioneDatiFissiFileName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneDatiFissiFileName

        Set(ByVal value As String)
            _SessioneDatiFissiFileName = value
        End Set

    End Property

    WriteOnly Property SessioneFld() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneFld

        Set(ByVal value As String)
            _SessionePath = value
        End Set

    End Property

    WriteOnly Property SezioneDati() As Xml.XmlNode Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SezioneDati

        Set(ByVal value As Xml.XmlNode)
            _SessioneDati = value
        End Set

    End Property

    WriteOnly Property CodiceModulo() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CodiceModulo

        Set(ByVal value As Integer)
            _CodiceModulo = value
        End Set

    End Property

    ReadOnly Property NomePlugIn() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.NomePlugIn

        Get
            Return "Plug-in per la stampa dei solleciti di pagamento per UmbriaEnergy."
        End Get

    End Property

    'ReadOnly Property VersionePlugIn() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.VersionePlugIn

    '    ' 1.0.0 Versione iniziale
    '    ' 1.0.1 Corretto alcuni problemi di stampa e sistemato il plugin.
    '    ' 1.0.2 Sistemato il bollettino di Stampa e gli oggetti collegati.
    '    Get
    '        Return "1.2.0"
    '    End Get

    'End Property

    Property MessaggiAvanzamento() As String() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.MessaggiAvanzamento

        Get
            Return _MessaggiAvanzamento
        End Get
        Set(ByVal value As String())
            _MessaggiAvanzamento = value
        End Set

    End Property

    'ReadOnly Property AlertPlugIn() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.AlertPlugIn

    '    Get
    '        Return String.Concat("Attenzione!! Ricordiamo che prima di procedere con la stampa dei moduli selezionati", vbCr, "� necessario verificare i seguenti dati:", vbCr, "- numero del protocollo;", vbCr, "- data del protocollo.", vbLf, vbLf, "Posso procedere con la stampa di quanto selezionato?")
    '    End Get

    'End Property

    ReadOnly Property ListaControlliForm() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ListaControlliFormNuovaSessione

        Get
            Dim aCtr As ArrayList

            aCtr = New ArrayList
            aCtr.Add(New String() {"LBL_input_1", "File contenente i solleciti ...:", "3;16", "155;13"})
            aCtr.Add(New String() {"TXB_input_1", "&1", "160;12", "610;20", "102"})
            aCtr.Add(New String() {"BTN_fil_input_1", "", "775;10", "27;23"})
            aCtr.Add(New String() {"LBL_output_998", "Cartella di destinazione .:", "3;42", "155;13"})
            aCtr.Add(New String() {"TXB_output_998", "&998", "160;38", "610;20", "201"})
            aCtr.Add(New String() {"BTN_fld_output_998", "", "775;36", "27;23"})
            aCtr.Add(New String() {"LBL_output_999", "Descrizione archivio ......:", "3;68", "155;13"})
            aCtr.Add(New String() {"TXB_output_999", "", "160;64", "M;640;122", "202"})
            Return aCtr
        End Get

    End Property

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckDatiSessione
        Dim _lObb As Boolean
        Dim lRtn As Boolean

        _lObb = False
        For Each aValue As String() In aDati
            Select Case aValue(0)
                Case Is = 102
                    _lObb = True
            End Select
        Next
        lRtn = True
        If _SessioneNuova Then
            If Not _lObb Then
                MessageBox.Show("Attenzione! Non sono � stato indicato nessun file contenente i dati. Non posso procedere con la creazione della sessione.")
                lRtn = False
            End If
        End If
        Return lRtn

    End Function

    Private Sub SetRecordIDX(ByVal xmlIndice As Xml.XmlDocument, ByVal xmlRecord As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xmlIndice.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xmlRecord.AppendChild(xiTmp)

    End Sub

    ReadOnly Property LinkField_QuickDS_FullDS() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LinkField_QuickDS_FullDS

        Get
            Select Case _CodiceModulo
                Case Is = ListaModuli.MOD_sollecito
                    Return "MAI_linkQD"
                Case Is = ListaModuli.MOD_bollettino
                    'Return "SXR_linkQD"
                    Return "FXR_linkQD"
                Case Is = ListaModuli.MOD_reportsolleciti
                    Return ""
                Case Is = ListaModuli.MOD_reportsaldoprint
                    Return ""
                Case Is = ListaModuli.MOD_reporteccedprint
                    Return ""
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    Public ReadOnly Property QuickDataset() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset

        Get
            Return _QuickDataset
        End Get

    End Property

    Public ReadOnly Property QuickDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset_MainTableKey

        Get
            Return "MAI_codice"
        End Get

    End Property

    Public ReadOnly Property QuickDataset_MainTableName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset_MainTableName

        Get
            Return "TBL_main"
        End Get

    End Property

    Public ReadOnly Property FullDataset() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataset

        Get
            Select Case _CodiceModulo
                Case Is = ListaModuli.MOD_sollecito
                    Return _PrintSollecitoDataset
                Case Is = ListaModuli.MOD_bollettino
                    Return _PrintF24Dataset
                Case Is = ListaModuli.MOD_reportsolleciti
                    Return _PrintReportSolleciti
                Case Is = ListaModuli.MOD_reportsaldoprint
                    Return _PrintReportSolleciti
                Case Is = ListaModuli.MOD_reporteccedprint
                    Return _PrintReportSaldoPrint
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataset_MainTableName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataSet_MainTableName

        Get
            Select Case _CodiceModulo
                Case Is = ListaModuli.MOD_sollecito
                    Return "TBL_main"
                Case Is = ListaModuli.MOD_bollettino
                    'Return "TBL_sol_x_rat"
                    Return "TBL_fat_x_rat"
                Case Is = ListaModuli.MOD_reportsolleciti
                    Return "TBL_main"
                Case Is = ListaModuli.MOD_reportsaldoprint
                    Return "TBL_main"
                Case Is = ListaModuli.MOD_reporteccedprint
                    Return "TBL_main"
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    ReadOnly Property CurrentRecordNumber() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CurrentRecordNumber

        Get
            Return _RecordCount
        End Get

    End Property

    Sub GoToRecordNumber(ByVal nRecord As Integer) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.GoToRecordNumber

        _RecordCount = nRecord

    End Sub

    WriteOnly Property FeedString() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FeedString

        Set(ByVal value As String)
            _FeedString = value
        End Set

    End Property

#Region "SetUp per Datagrid di visualizzazione delle righe da stampare"

    Sub ImpostaDGVDati(ByVal DGV As DataGridView) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ImpostaDGVDati

        With DGV.Columns
            .Clear()
            .Add(Col_DaStampare)
            .Add(Col_Errore_code)
            .Add(Col_Errore_desc)
            .Add(Col_Progressivo)
            .Add(Col_NumeroUtente)
            .Add(COL_Contr_Attivo)
            .Add(Col_Nominativo)
            .Add(COL_Indirizzo_Recapito)
            .Add(COL_CAP_Recapito)
            .Add(COL_Citta_Recapito)
            .Add(COL_Provincia_Recapito)
            .Add(COL_Nazione_Recapito)
            .Add(COL_Print_Bollettino)
            .Add(COL_Print_allegato)
        End With

    End Sub

    Private Function Col_DaStampare() As System.Windows.Forms.DataGridViewCheckBoxColumn
        Dim col As System.Windows.Forms.DataGridViewCheckBoxColumn

        col = New System.Windows.Forms.DataGridViewCheckBoxColumn
        With col
            .HeaderText = "Stampa"
            .DataPropertyName = "DEF_toprint"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .Width = 50
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Errore_code() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errcode"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Errore_desc() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errdesc"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Progressivo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Progressivo"
            .DataPropertyName = "MAI_codice"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 70
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroUtente() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Utente"
            .DataPropertyName = "MAI_num_utente"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 90
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Contr_Attivo()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Stato Contr."
            .DataPropertyName = "MAI_attivo"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "Chiuso;Attivo"
        End With
        Return col

    End Function

    Private Function Col_Nominativo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nominativo"
            .DataPropertyName = "MAI_nominativo"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Indirizzo_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Indirizzo Recapito"
            .DataPropertyName = "MAI_indirizzo_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_CAP_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "CAP"
            .DataPropertyName = "MAI_cap_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Citta_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Citt�"
            .DataPropertyName = "MAI_citta_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Provincia_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Prov."
            .DataPropertyName = "MAI_provincia_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Nazione_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nazione"
            .DataPropertyName = "MAI_nazione_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Print_Bollettino()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Bollettino"
            .DataPropertyName = "MAI_print_bol"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_Print_allegato()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Allegato"
            .DataPropertyName = "MAI_print_all"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

#End Region

    ReadOnly Property ElementiMenuDedicati() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ElementiMenuDedicati
        Get
            Dim aMenuItem As ArrayList
            Dim MenuEntry As ToolStripMenuItem
            Dim SubMenuEntry As ToolStripMenuItem

            aMenuItem = New ArrayList
            MenuEntry = New ToolStripMenuItem("Stampa Solleciti")

            SubMenuEntry = New ToolStripMenuItem("Dati protocollo")
            AddHandler SubMenuEntry.Click, AddressOf DatiProtocollo_click
            MenuEntry.DropDownItems.Add(SubMenuEntry)

            SubMenuEntry = New ToolStripMenuItem("Data sollecito")
            AddHandler SubMenuEntry.Click, AddressOf DataSollecito_click
            MenuEntry.DropDownItems.Add(SubMenuEntry)

            SubMenuEntry = New ToolStripMenuItem("Testo sollecito Energia Elettrica")
            AddHandler SubMenuEntry.Click, AddressOf TestoSollecito_ele_click
            MenuEntry.DropDownItems.Add(SubMenuEntry)

            SubMenuEntry = New ToolStripMenuItem("Testo sollecito GAS")
            AddHandler SubMenuEntry.Click, AddressOf TestoSollecito_gas_click
            MenuEntry.DropDownItems.Add(SubMenuEntry)

            SubMenuEntry = New ToolStripMenuItem("Esporta dati solleciti")
            AddHandler SubMenuEntry.Click, AddressOf DatiSolleciti_click
            MenuEntry.DropDownItems.Add(SubMenuEntry)

            SubMenuEntry = New ToolStripMenuItem("Carica record annullare Imp. Eccedent.")
            AddHandler SubMenuEntry.Click, AddressOf SwImpEcc_click
            MenuEntry.DropDownItems.Add(SubMenuEntry)

            aMenuItem.Add(New Object() {"n", "TSM_sessione_stampa", MenuEntry})

            aMenuItem.Add(New Object() {"a", "TSM_selezione", New ToolStripSeparator()})

            ' Seleziona tutti i record con bollettino
            SubMenuEntry = New ToolStripMenuItem("Con Contratti attivi")
            SubMenuEntry.Name = cns_tsm_sel_contr_attivo
            aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

            SubMenuEntry = New ToolStripMenuItem("Con Contratti Chiusi")
            SubMenuEntry.Name = cns_tsm_sel_contr_chiuso
            aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

            Return aMenuItem
        End Get
    End Property

    ' ************************************************************************** '
    ' Imposto correttamente la parte di selezione per la stampa dei record. Se   '
    ' non dobbiamo mantenere la selezione o se la dobbiamo mantenere ma il re-   ' 
    ' cord non � stampabile metto a "NON STAMPABILE (False)" il campo .          '
    ' ************************************************************************** '
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetSelectable
        Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_ASM_sol.TBL_mainRow)
        dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
        If cFilter > "" Then
            dr_mai_qd.DEF_selectable = dr_mai_qd.DEF_selectable And dr_mai_qd(cFilter)
        End If

        ' ************************************************************************** '
        ' Pulisce i campi relativi agli errori in quanto ci stiamo occupando di un   '
        ' nuovo modulo.                                                              '
        ' ************************************************************************** '
        If dr_mai_qd.DEF_selectable Then
            dr_mai_qd.Item("DEF_errcode") = 0
            dr_mai_qd.Item("DEF_errdesc") = ""
        End If

    End Sub

    Private Function GetValueDatiFissi(ByVal cTable As String, ByVal cField As String) As String
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim cRtn As String

        Try
            xmlDF.Load(_SessioneDatiFissiFileName)
            xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))

            cRtn = xmlTmp.InnerText
            cRtn = cRtn.Replace("{\TBL_main;MAI_tot_sol;2;#,##0.00/}", "&IMPORTO_TOTALE_SOLL&")
            xmlDF = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    Private Sub SetValueDatiFissi(ByVal cTable As String, ByVal cField As String, ByVal cValue As String)
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim xaTmp As Xml.XmlAttribute
        Dim xnWork2 As Xml.XmlNode

        cValue = cValue.Replace("&IMPORTO_TOTALE_SOLL&", "{\TBL_main;MAI_tot_sol;2;#,##0.00/}")

        xmlDF.Load(_SessioneDatiFissiFileName)
        xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))
        If Not xmlTmp Is Nothing Then
            xmlTmp = xmlTmp.SelectSingleNode("valore")
            xmlTmp.InnerText = cValue
        Else
            'Crea il nodo
            xmlTmp = xmlDF.CreateElement("item")
            xaTmp = xmlDF.CreateAttribute("codice")
            xaTmp.Value = cField
            xmlTmp.Attributes.Append(xaTmp)
            xnWork2 = xmlDF.CreateElement("valore")
            xnWork2.InnerText = cValue
            xmlTmp.AppendChild(xnWork2)
            xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable)).AppendChild(xmlTmp)
        End If
        xmlDF.Save(_SessioneDatiFissiFileName)
        xmlDF = Nothing

    End Sub

    Private Sub DatiProtocollo_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As FRM_dati_protocollo

        Try
            frm = New FRM_dati_protocollo
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_num_prot.Text = GetValueDatiFissi("FixSol", "1")
            frm.TXB_data_prot.Text = GetValueDatiFissi("FixSol", "2")
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi("FixSol", "1", frm.TXB_num_prot.Text)
                SetValueDatiFissi("FixSol", "2", frm.TXB_data_prot.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il valore richiesto.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub DataSollecito_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As FRM_data_sollecito

        Try
            frm = New FRM_data_sollecito
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_data_soll.Text = GetValueDatiFissi("FixSol", "5")
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi("FixSol", "5", frm.TXB_data_soll.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il valore richiesto.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TestoSollecito_ele_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_testo_sollecito

        Try
            frm = New FRM_testo_sollecito
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_testo_sollecito.Text = GetValueDatiFissi("FixSol", "3")
            frm.Text = "Testo sollecito per le forniture di Energia Elettrica"
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi("FixSol", "3", frm.TXB_testo_sollecito.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo dell'autolettura", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TestoSollecito_gas_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_testo_sollecito

        Try
            frm = New FRM_testo_sollecito
            frm.StartPosition = FormStartPosition.CenterParent
            frm.TXB_testo_sollecito.Text = GetValueDatiFissi("FixSol", "4")
            frm.Text = "Testo sollecito per le forniture di GAS"
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi("FixSol", "4", frm.TXB_testo_sollecito.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo dell'autolettura", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub DatiSolleciti_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_estrai_exl

        Try
            frm = New FRM_estrai_exl
            frm.StartPosition = FormStartPosition.CenterParent
            If frm.ShowDialog = DialogResult.OK Then
                SetValueDatiFissi("FixSol", "5", frm.TXB_num_lotto.Text)
            End If
        Catch ex As Exception
            MessageBox.Show("Errore di scrittura del file di dati del numero lotto", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub SwImpEcc_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument
        Dim frm As New FRM_SwitchEcc

        Try
            frm = New FRM_SwitchEcc
            frm.Soll_FileName = PrintDataSet_DataFile(1)
            frm.QuickDataset_FileName = QuickDataSet_IndexFile
            frm.StartPosition = FormStartPosition.CenterParent
            If frm.ShowDialog = DialogResult.OK Then

            End If
        Catch ex As Exception
        End Try

    End Sub

    Sub RemovePRNDSErrorRecord() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.RemovePRNDSErrorRecord
        Dim nLinkCode As Integer

        Select Case _CodiceModulo
            Case Is = ListaModuli.MOD_sollecito
                For Each dr_mai_pd As DS_PS_ASM_sol.TBL_mainRow In _PrintSollecitoDataset.TBL_main.Rows
                    nLinkCode = dr_mai_pd(LinkField_QuickDS_FullDS)
                    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                        dr_mai_pd.Delete()
                    End If
                Next
                _PrintSollecitoDataset.TBL_main.AcceptChanges()
            Case Is = ListaModuli.MOD_bollettino
                'For Each dr_mai_pd As DS_PB_ASM_sol.TBL_sol_x_ratRow In _PrintF24Dataset.TBL_sol_x_rat.Rows
                '    nLinkCode = dr_mai_pd(LinkField_QuickDS_FullDS)
                '    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                '        dr_mai_pd.Delete()
                '    End If
                'Next
                '_PrintF24Dataset.TBL_sol_x_rat.AcceptChanges()
        End Select

    End Sub

    'Property LogDS() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LogDSStampa

    '    Get
    '        LoadDSLog()
    '        Return _LogDS
    '    End Get
    '    Set(ByVal value As DataSet)
    '        _LogDS = value
    '    End Set

    'End Property

    Private Sub LoadDSLog()
        Dim dr As DataRow

        For Each dr_qds As DS_QD_ASM_sol.TBL_mainRow In _QuickDataset.TBL_main.Rows
            If dr_qds.DEF_errcode < 0 Then
                dr = _LogDS.Tables(0).NewRow
                dr("LOG_desc_rec_1") = String.Concat("Progressivo: ", dr_qds.MAI_codice, " Nominativo: ", dr_qds.MAI_nominativo, " Numero Fattura: ", dr_qds.MAI_num_sollecito)
                dr("LOG_stato") = dr_qds.DEF_errcode
                dr("LOG_desc_err") = dr_qds.DEF_errdesc
                _LogDS.Tables(0).Rows.Add(dr)
            End If
        Next

    End Sub

    'Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetPrintable
    '    Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow

    '    dr_mai_qd = CType(dr, DS_QD_ASM_sol.TBL_mainRow)
    '    Select Case _SelectFromPlugInType
    '        Case Is = cns_tsm_sel_contr_attivo.ToLower
    '            dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_attivo
    '        Case Is = cns_tsm_sel_contr_chiuso.ToLower
    '            dr_mai_qd.DEF_toprint = Not dr_mai_qd.MAI_attivo
    '    End Select

    'End Sub

    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.GetSelectedRow
        Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow
        Dim aResult As String()
        Dim cFilter As String

        Select Case _SelectFromPlugInType
            Case Is = "tsm_ric_progressivo"
                cFilter = "MAI_codice"
        End Select
        aResult = New String() {"MAI_codice", ""}
        For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
            If dr_mai_qd(cFilter) = cValue Then
                aResult(1) = dr_mai_qd.MAI_codice
            End If
        Next
        Return aResult

    End Function

    WriteOnly Property FiltroSelezione() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FiltroSelezione

        Set(ByVal value As String)

        End Set

    End Property

    'Public Sub ClearPrintDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ClearPrintDataset

    '    Select Case _CodiceModulo
    '        Case Is = ListaModuli.MOD_sollecito
    '            _PrintSollecitoDataset = Nothing
    '        Case Is = ListaModuli.MOD_bollettino
    '            _PrintBollettinoDataset = Nothing
    '        Case Is = ListaModuli.MOD_reportsolleciti
    '            _PrintReportSolleciti = Nothing
    '        Case Is = ListaModuli.MOD_reportsaldoprint
    '            _PrintReportSolleciti = Nothing
    '        Case Is = ListaModuli.MOD_reporteccedprint
    '            _PrintReportSaldoPrint = Nothing
    '    End Select

    'End Sub

#Region "Procedure utili per la creazione della sessione di stampa"

    Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CreaDataBaseSessione
        Dim aValue As String()
        Dim xdWork1 As Xml.XmlDocument
        Dim xdWork2 As Xml.XmlDocument
        Dim nRecord As Integer
        Dim xiWork1 As Xml.XmlNode
        Dim xiWork2 As Xml.XmlNode
        Dim xiRecord As Xml.XmlNode
        Dim cSortValue As String
        Dim cTmp As String

        Dim nTotale As Decimal
        Dim ds_src As DataSet
        Dim dr_sol As DataRow

        _ResultAL = New ArrayList
        ' Copia i file dei dati dalla cartella dove sono attualmente 
        ' nella cartella dati della sessione
        ImpostaMessaggi(New String() {"", "", "", "0", _DatiSessione.Count, "Elaborazione dei file da caricare...."})

        ' Copia dei file indicati nella loro posizione
        For Each aValue In _DatiSessione
            ImportaFileInSessione(aValue(0), aValue(1))
        Next

        ImpostaMessaggi(New String() {"", "", "", "", CInt(_MessaggiAvanzamento(4)) + _ResultAL.Count, "Elaborazione dei file della cartella di archiviazione..."})
        xdWork2 = New Xml.XmlDocument
        xdWork2.AppendChild(xdWork2.CreateNode(Xml.XmlNodeType.Element, "SOLLECITATO", ""))
        For Each aValue In _ResultAL
            If (aValue(3) = 102) Then
                Try
                    Try
                        ElaboraFileSolleciti(String.Concat(_SessionePath, aValue(1)))
                        ds_src = caricaFileDati(String.Concat(_SessionePath, aValue(1)))
                        aValue(4) = 0
                    Catch ex As Exception
                        MessageBox.Show(String.Concat("Attenzione il file ", String.Concat(_SessionePath, aValue(1)), " non � correttamente formattato.", vbCr, "L'errore � riportato di seguito:", vbCr, ex.Message), "Errore di caricamento di un file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        aValue(4) = -1
                    End Try
                    If aValue(4) = 0 Then
                        For Each dr_sol In ds_src.Tables("Sollecito").Rows
                            xiRecord = xdWork2.CreateNode(Xml.XmlNodeType.Element, "SOLLECITATO_ROW", "")
                            SetRecordIDX(xdWork2, xiRecord, "MAI_num_utente", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "IDSoggetto"))
                            SetRecordIDX(xdWork2, xiRecord, "MAI_num_sollecito", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "IDSollecito"))
                            SetRecordIDX(xdWork2, xiRecord, "MAI_nominativo", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "RagioneSociale"))
                            cTmp = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "TipoElementoTopologico")
                            cTmp = String.Concat(cTmp, " ", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ElementoTopologico"))
                            cTmp = String.Concat(cTmp, " ", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "NumeroCivico"))
                            cTmp = String.Concat(cTmp, " ", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "SuffissoCivico"))
                            SetRecordIDX(xdWork2, xiRecord, "MAI_indirizzo_recapito", cTmp.Trim)
                            SetRecordIDX(xdWork2, xiRecord, "MAI_citta_recapito", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "Comune"))
                            SetRecordIDX(xdWork2, xiRecord, "MAI_cap_recapito", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "CAP"))
                            SetRecordIDX(xdWork2, xiRecord, "MAI_provincia_recapito", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ProvinciaSigla"))
                            SetRecordIDX(xdWork2, xiRecord, "MAI_nazione_recapito", "")
                            SetRecordIDX(xdWork2, xiRecord, "MAI_print_bol", "si")
                            SetRecordIDX(xdWork2, xiRecord, "MAI_attivo", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "StatoContratto").ToString.ToUpper = "ATTIVO")
                            SetRecordIDX(xdWork2, xiRecord, "MAI_filedati", 1)
                            SetRecordIDX(xdWork2, xiRecord, "DEF_errcode", "0")
                            SetRecordIDX(xdWork2, xiRecord, "DEF_errdesc", "")
                            SetRecordIDX(xdWork2, xiRecord, "MAI_totale", 0)
                            SetRecordIDX(xdWork2, xiRecord, "MAI_switchecced", 0)
                            If RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "CodiceFiscale") > "" Then
                                SetRecordIDX(xdWork2, xiRecord, "MAI_cf", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "CodiceFiscale"))
                            ElseIf RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "Partitaiva") > "" Then
                                SetRecordIDX(xdWork2, xiRecord, "MAI_cf", RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "Partitaiva"))
                            End If

                            cTmp = String.Concat(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "AnnoContratto"), "/", _
                                                 RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "NumeroContratto").PadLeft(6, "0"))
                            If cTmp = "/000000" Then
                                xiRecord.SelectSingleNode("DEF_errcode").InnerText = "-1"
                                xiRecord.SelectSingleNode("DEF_errdesc").InnerText = "Codice contratto non valorizzato"
                                cTmp = ""
                            End If
                            SetRecordIDX(xdWork2, xiRecord, "MAI_cod_contratto", cTmp)
                            ' impostazione della modalit� di ordinamento.
                            ' in partenza si era preferito ordinare per numero fattura.
                            ' la richiesta di bonato � stata invece quella di ordinare per:
                            ' CAP - Via - NumeroCivico Pari o dispari - numero civico.

                            ' Inizializzo la variabile di ordinamento come indicato sopra
                            cSortValue = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "Comune")

                            cSortValue = String.Concat(cSortValue, cTmp)
                            cTmp = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "NumeroCivico")
                            If IsNumeric(cTmp) Then
                                If (cTmp Mod 2) = 0 Then
                                    cSortValue = String.Concat(cSortValue, "P")
                                Else
                                    cSortValue = String.Concat(cSortValue, "D")
                                End If
                                ' Inverte l'ordinamento per numero civico, dal pi� grande al pi� piccolo.
                                cTmp = 1000000000 - Integer.Parse(cTmp)
                            Else
                                cSortValue = String.Concat(cSortValue, "N")
                            End If
                            cTmp = ""
                            ' vecchia modalit� di ordinamento
                            '                            cSortValue = ""
                            '                           cSortValue = String.Concat(cSortValue, cTmp.PadLeft(10, "0"))
                            '                          cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "RagioneSociale")).Replace(" ", "")

                            cSortValue = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "Comune")
                            cTmp = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "NumeroCivico")
                            cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "TipoElementoTopologico"), _
                                                                   RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ElementoTopologico"))
                            If IsNumeric(cTmp) Then
                                If (cTmp Mod 2) = 0 Then
                                    cSortValue = String.Concat(cSortValue, "P")
                                Else
                                    cSortValue = String.Concat(cSortValue, "D")
                                End If
                                ' Inverte l'ordinamento per numero civico, dal pi� grande al pi� piccolo.
                                cTmp = 1000000000 - Integer.Parse(cTmp)
                            Else
                                cSortValue = String.Concat(cSortValue, "N")
                            End If
                            cSortValue = String.Concat(cSortValue, cTmp.PadLeft(10, "0"))
                            cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "SuffissoCivico").PadLeft(6, " "))
                            cSortValue = String.Concat(cSortValue, RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "RagioneSociale")).Replace(" ", "")
                            SetRecordIDX(xdWork2, xiRecord, "SORTFIELD", cSortValue)
                            xdWork2.SelectSingleNode("SOLLECITATO").AppendChild(xiRecord)
                            nTotale = CDec(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleSollecito", 0).ToString.Replace(".", ",")) '- _
                            '          (CDec(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleAnticipazioneRaggrupp", 0).ToString.Replace(".", ",")) + _
                            '          GetValueEccedentari(ds_src, dr_sol))
                            xiRecord.SelectSingleNode("MAI_totale").InnerText = nTotale.ToString
                            If xiRecord.SelectSingleNode("DEF_errcode").InnerText = "0" Then
                                If Not nTotale > cnt_ThresholdImporto Then
                                    xiRecord.SelectSingleNode("DEF_errcode").InnerText = "1"
                                    xiRecord.SelectSingleNode("DEF_errdesc").InnerText = String.Concat("Il totale dei solleciti � uguale o inferiore a � ", cnt_ThresholdImporto, " per cui non � necessario stampare alcunch�. Totale= � ", Format(nTotale, "#,##0.00"))
                                Else
                                    xiRecord.SelectSingleNode("DEF_errcode").InnerText = "0"
                                    xiRecord.SelectSingleNode("DEF_errdesc").InnerText = ""
                                End If
                            End If
                        Next
                        aValue(4) = 1
                    End If
                Catch ex As Exception
                    aValue(4) = -1
                Finally
                    xdWork1 = Nothing
                End Try
            Else
                aValue(4) = 1
            End If
            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
        Next
        _MessaggiAvanzamento(5) = "Ordinamento dei record in corso..."
        SortFileIndice(xdWork2, 0, xdWork2.SelectSingleNode("SOLLECITATO").ChildNodes.Count - 1)

        ' Rimuove il campo utilizzato per l'ordinamento
        nRecord = 1
        For Each xiWork1 In xdWork2.SelectNodes("SOLLECITATO/SOLLECITATO_ROW/SORTFIELD")
            xiWork2 = xiWork1.ParentNode
            xiWork2.RemoveChild(xiWork1)
            SetRecordIDX(xdWork2, xiWork2, "MAI_codice", nRecord)
            nRecord += 1
        Next

        ' Salva il file
        xdWork2.Save(String.Concat(_SessionePath, "\dati\indice.xml"))
        _ResultAL.Add(New String() {"filename", "dati\indice.xml", "0", "0", "1"})
        xdWork2 = Nothing

    End Sub

    Private Sub ElaboraFileSolleciti(ByVal cFileName As String)
        Dim xdSoll As Xml.XmlDocument
        Dim xiSogg As Xml.XmlNode
        Dim xiSoll As Xml.XmlNode
        Dim xiContr As Xml.XmlNodeList
        Dim aSoggID As ArrayList
        Dim nEccedentario As Decimal
        Dim nSolValue As Decimal
        Dim nEccValue As Decimal
        Dim nPlusValue As Decimal
        Dim nTotRaggValue As Decimal

        xdSoll = New Xml.XmlDocument
        xdSoll = caricaFileDatiXML(cFileName)
        aSoggID = New ArrayList
        For Each xiSogg In xdSoll.SelectNodes("EmissioneSolleciti/Sollecito/Cliente/IDSoggetto")
            xiContr = xdSoll.SelectNodes(String.Concat("EmissioneSolleciti/Sollecito/Cliente/IDSoggetto[.='", xiSogg.InnerText, "']"))
            nEccedentario = -1

            If xiContr.Count > 1 And Not aSoggID.Contains(xiSogg.InnerText) Then
                aSoggID.Add(xiSogg.InnerText)
                ' Verifico la presenza di importi eccedentari. coincidenti per tutti i contratti del cliente.
                nPlusValue = 0
                For Each xiSoll In xiContr
                    xiSoll = xiSoll.ParentNode.ParentNode

                    If xiSoll.SelectSingleNode("TotaleSollecito").InnerText.StartsWith(".") Then
                        xiSoll.SelectSingleNode("TotaleSollecito").InnerText = String.Concat("0", xiSoll.SelectSingleNode("TotaleSollecito").InnerText)
                    End If
                    nSolValue = CDec(xiSoll.SelectSingleNode("TotaleSollecito").InnerText.Replace(".", ","))

                    'If xiSoll.SelectSingleNode("TotaleAnticipazioneRaggrupp").InnerText.StartsWith(".") Then
                    '    xiSoll.SelectSingleNode("TotaleAnticipazioneRaggrupp").InnerText = String.Concat("0", xiSoll.SelectSingleNode("TotaleAnticipazioneRaggrupp").InnerText)
                    'End If
                    'nTotRaggValue = CDec(xiSoll.SelectSingleNode("TotaleAnticipazioneRaggrupp").InnerText.Replace(".", ","))

                    'If xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText.StartsWith(".") Then
                    '    xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText = String.Concat("0", xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText)
                    'End If

                    'nEccValue = CDec(xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText.Replace(".", ","))

                    'If nEccedentario = -1 Then
                    '    nEccedentario = nEccValue
                    '    If nEccValue > 0 Then
                    '        If nSolValue < nTotRaggValue Then
                    '            xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText = 0
                    '            ' nPlusValue = nEccValue + (nTotRaggValue - nSolValue)
                    '        Else
                    '            If nSolValue - (nTotRaggValue + nEccValue) < 0 Then
                    '                xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText = nSolValue - nTotRaggValue
                    '                nPlusValue = nEccValue - (nSolValue - nTotRaggValue)
                    '            End If
                    '        End If
                    '    End If
                    'Else
                    '    If nEccedentario > 0 And nEccedentario = nEccValue Then
                    '        If nPlusValue > 0 Then
                    '            If nSolValue < nTotRaggValue Then
                    '                xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText = 0
                    '                '      nPlusValue = nPlusValue + (nTotRaggValue - nSolValue)
                    '            Else
                    '                If nSolValue - (nTotRaggValue + nPlusValue) < 0 Then
                    '                    xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText = nSolValue - nTotRaggValue
                    '                    nPlusValue = nPlusValue - (nSolValue - nTotRaggValue)
                    '                Else
                    '                    xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText = nPlusValue
                    '                    nPlusValue = 0
                    '                End If
                    '            End If
                    '        Else
                    '            xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText = 0
                    '        End If
                    '    End If
                    'End If
                Next

                If nPlusValue > 0 Then
                    xiSoll = xiContr(0).ParentNode.ParentNode
                    xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText = CDec(xiSoll.SelectSingleNode("TotaleIncassiEccedentariSog").InnerText) + nPlusValue
                End If
            End If
        Next

        SalvaFileDatiXML(cFileName, xdSoll)

    End Sub

    ' ************************************************************************** '
    ' Questa funzione serve a leggere un file XML con la codifica WINDOWS-1252 e '
    ' a caricarlo in un XMLDocument.                                             '
    ' ************************************************************************** '
    Private Function caricaFileDatiXML(ByVal cFileName As String) As Xml.XmlDocument
        Dim myEncoding As System.Text.Encoding
        Dim sr_xml As System.IO.StreamReader
        Dim fil_xml As Xml.XmlDocument

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)    ' Windows-1252
            sr_xml = New System.IO.StreamReader(cFileName, myEncoding)
            fil_xml = New Xml.XmlDocument
            fil_xml.Load(sr_xml)
            sr_xml.Close()
        Catch ex As Exception
            fil_xml = Nothing
            Throw ex
        End Try
        Return fil_xml

    End Function

    ' ************************************************************************** '
    ' Questa funzione serve a scrivere un file XML con la codifica WINDOWS-1252. '
    ' ************************************************************************** '
    Private Sub SalvaFileDatiXML(ByVal cFileName As String, ByVal xdFile As Xml.XmlDocument)
        Dim myEncoding As System.Text.Encoding
        Dim sw_xml As System.IO.StreamWriter

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252)
            sw_xml = New System.IO.StreamWriter(cFileName, False, myEncoding)
            xdFile.Save(sw_xml)
            sw_xml.Close()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Function CaricaFileDatiToDS_excel(ByVal cFileName As String) As DataSet
        Dim dsExcel As DataSet
        Dim dbConn As String
        Dim oConn As OleDb.OleDbConnection
        Dim oCmd As OleDb.OleDbCommand
        Dim oDA As OleDb.OleDbDataAdapter

        dbConn = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cFileName, ";Extended Properties=""Excel 8.0;HDR=YES;IMEX=1"";")

        oConn = New OleDb.OleDbConnection(dbConn)
        oConn.Open()
        oCmd = New OleDb.OleDbCommand("SELECT * FROM [Foglio1$]", oConn)
        oCmd.CommandType = CommandType.Text
        oDA = New OleDb.OleDbDataAdapter
        oDA.SelectCommand = oCmd

        dsExcel = New DataSet()
        oDA.Fill(dsExcel)
        oConn.Close()
        Return dsExcel

    End Function

    'Private Function CaricaFileDatiToDS1(ByVal cFileName As String) As DS_excel
    '    Dim dsExcel As DS_excel
    '    Dim dbConn As String
    '    Dim oConn As OleDb.OleDbConnection
    '    Dim oCmd As OleDb.OleDbCommand
    '    Dim oDA As OleDb.OleDbDataAdapter

    '    dbConn = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cFileName, ";Extended Properties=""Excel 8.0;HDR=YES;IMEX=1"";")

    '    oConn = New OleDb.OleDbConnection(dbConn)
    '    oConn.Open()
    '    oCmd = New OleDb.OleDbCommand("SELECT * FROM [Foglio1$]", oConn)
    '    oCmd.CommandType = CommandType.Text
    '    oDA = New OleDb.OleDbDataAdapter
    '    oDA.SelectCommand = oCmd

    '    dsExcel = New DS_excel()
    '    oDA.Fill(dsExcel, "TBL_excel")
    '    oConn.Close()
    '    Return dsExcel

    'End Function

    Sub ImportaFileInSessione(ByVal nTipoFile As Integer, ByVal cFileName As String)
        Dim nRecordtoAnalized As Integer
        Dim xdWork1 As Xml.XmlDocument
        Dim xdWork2 As Xml.XmlDocument
        Dim nRecordXFile As Integer
        Dim xiWork As Xml.XmlNode
        Dim cDstFile As String
        Dim nRecord As Integer
        Dim nFile As Integer
        Dim nDim As Integer

        ' Nel caso di tipo file 1 allora si tratta di un file xml, indipendentemente dall'estensione 
        ' da suddividere eventualmente in file di pi� piccole dimensioni.
        If nTipoFile = 1 Then
            Try
                xdWork1 = New Xml.XmlDocument
                xdWork1.Load(cFileName)
                nDim = New System.IO.FileInfo(cFileName).Length
                If (nDim < UpperFileSize) Then
                    _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
                    cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

                    System.IO.File.Copy(cFileName, cDstFile)
                    _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                    _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
                Else
                    _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                    _MessaggiAvanzamento(5) = "Suddivizione del file nella cartella di archiviazione..."

                    nRecordtoAnalized = xdWork1.SelectNodes(Costanti.itm_Fattura).Count
                    nRecordXFile = Int(LowerFileSize / (nDim / nRecordtoAnalized)) + 1
                    _MessaggiAvanzamento(4) = CInt(_MessaggiAvanzamento(4)) + Int(nRecordtoAnalized / nRecordXFile) + 1
                    nRecord = 0
                    nFile = 1
                    For Each xiWork In xdWork1.SelectNodes(Costanti.itm_Fattura)
                        If nRecord = 0 Then
                            xdWork2 = New Xml.XmlDocument
                            xdWork2.AppendChild(xdWork2.CreateNode(Xml.XmlNodeType.Element, "EMISSIONE", ""))
                        End If
                        xdWork2.SelectSingleNode("EMISSIONE").AppendChild(xdWork2.ImportNode(xiWork, True))
                        nRecord += 1
                        nRecordtoAnalized -= 1
                        If (nRecord = nRecordXFile) Or (nRecordtoAnalized = 0) Then
                            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
                            cDstFile = cFileName.Substring(cFileName.LastIndexOf("\") + 1)
                            cDstFile = String.Concat(cDstFile.Substring(0, cDstFile.IndexOf(".")), "_", nFile.ToString.PadLeft(5, "0"), cDstFile.Substring(cDstFile.IndexOf(".")))
                            cDstFile = String.Concat(_SessioneDatiPath, cDstFile)
                            xdWork2.Save(cDstFile)
                            xdWork2 = Nothing
                            _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
                            nRecord = 0
                            nFile += 1
                        End If
                    Next
                    xdWork1 = Nothing
                End If
            Catch ex As Exception
                cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))
                System.IO.File.Copy(cFileName, cDstFile)
                _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, -1})
            End Try
        Else
            _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
            cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

            System.IO.File.Copy(cFileName, cDstFile)
            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
            _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
        End If

    End Sub

    Private Sub SortFileIndice(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer)
        Dim nPivot As Integer
        Dim k As Integer

        If nFst < nEnd Then
            nPivot = (nFst + nEnd) / 2
            k = dividi(xiWork, nFst, nEnd, nPivot)
            SortFileIndice(xiWork, nFst, k - 1)
            SortFileIndice(xiWork, k + 1, nEnd)
        End If

    End Sub

    Function dividi(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer, ByVal nPivot As Integer) As Integer
        Dim i As Integer = nFst
        Dim j As Integer = nEnd
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim nPivotPlace As Integer

        If nPivot < nEnd Then
            ' sposto il valore del pivot alla fine dell file xml
            swapItem(xiWork, nPivot, nEnd)
        End If

        xiWork_1 = xiWork.SelectSingleNode("SOLLECITATO").ChildNodes(nEnd).SelectSingleNode("SORTFIELD")
        nPivotPlace = nFst
        For i = nFst To nEnd - 1
            xiWork_2 = xiWork.SelectSingleNode("SOLLECITATO").ChildNodes(i).SelectSingleNode("SORTFIELD")
            If (xiWork_2.InnerText <= xiWork_1.InnerText) Then
                If i <> nPivotPlace Then
                    swapItem(xiWork, nPivotPlace, i)
                End If
                nPivotPlace += 1
            End If
        Next
        If nPivotPlace < nEnd Then
            swapItem(xiWork, nPivotPlace, nEnd)
        End If
        Return nPivotPlace

    End Function

    Private Sub swapItem(ByVal xiWork As Xml.XmlDocument, ByVal nIdx1 As Integer, ByVal nIdx2 As Integer)
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim xiWork_3 As Xml.XmlNode

        xiWork_1 = xiWork.SelectSingleNode("SOLLECITATO").ChildNodes(nIdx1)
        xiWork_2 = xiWork.SelectSingleNode("SOLLECITATO").ChildNodes(nIdx2)
        xiWork.SelectSingleNode("SOLLECITATO").RemoveChild(xiWork_1)
        xiWork.SelectSingleNode("SOLLECITATO").RemoveChild(xiWork_2)
        xiWork_3 = xiWork.SelectSingleNode("SOLLECITATO").ChildNodes(nIdx1 - 1)
        xiWork.SelectSingleNode("SOLLECITATO").InsertAfter(xiWork_2, xiWork_3)
        xiWork_3 = xiWork.SelectSingleNode("SOLLECITATO").ChildNodes(nIdx2 - 1)
        xiWork.SelectSingleNode("SOLLECITATO").InsertAfter(xiWork_1, xiWork_3)

    End Sub

#End Region

    Private ReadOnly Property QuickDataSet_IndexFile() As String

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode("filename[@tipo=""0""]").InnerText)
        End Get

    End Property

    Public Sub LoadQuickDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LoadQuickDataset
        Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow

        _QuickDataset = New DS_QD_ASM_sol
        _xmlDati_qd = New Xml.XmlDocument
        _xmlDati_qd.Load(QuickDataSet_IndexFile)
        ImpostaMessaggi(New String() {"", "", "", "0", _xmlDati_qd.SelectNodes(Costanti.itm_qd_Sollecitato).Count, "Caricamento dei dati...."})

        For Each xmlItem As Xml.XmlNode In _xmlDati_qd.SelectNodes(Costanti.itm_qd_Sollecitato)
            dr_mai_qd = _QuickDataset.TBL_main.NewRow
            Try
                dr_mai_qd.MAI_codice = GetValueFromXML(xmlItem, "MAI_codice")
                dr_mai_qd.DEF_toprint = False
                dr_mai_qd.DEF_errcode = GetValueFromXML(xmlItem, "DEF_errcode")
                dr_mai_qd.DEF_errdesc = GetValueFromXML(xmlItem, "DEF_errdesc")
                dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
                dr_mai_qd.MAI_num_utente = GetValueFromXML(xmlItem, "MAI_num_utente")
                dr_mai_qd.MAI_num_sollecito = GetValueFromXML(xmlItem, "MAI_num_sollecito")
                dr_mai_qd.MAI_nominativo = GetValueFromXML(xmlItem, "MAI_nominativo")
                dr_mai_qd.MAI_indirizzo_recapito = GetValueFromXML(xmlItem, "MAI_indirizzo_recapito")
                dr_mai_qd.MAI_citta_recapito = GetValueFromXML(xmlItem, "MAI_citta_recapito")
                dr_mai_qd.MAI_cap_recapito = GetValueFromXML(xmlItem, "MAI_cap_recapito")
                dr_mai_qd.MAI_provincia_recapito = GetValueFromXML(xmlItem, "MAI_provincia_recapito")
                dr_mai_qd.MAI_nazione_recapito = GetValueFromXML(xmlItem, "MAI_nazione_recapito")
                dr_mai_qd.MAI_print_bol = GetValueFromXML(xmlItem, "MAI_print_bol").ToLower = "si"
                dr_mai_qd.MAI_print_all = GetValueFromXML(xmlItem, "MAI_print_all").ToLower = "si"
                dr_mai_qd.MAI_totale = GetValueFromXML(xmlItem, "MAI_totale", "0")
                dr_mai_qd.MAI_cod_contratto = GetValueFromXML(xmlItem, "MAI_cod_contratto")
                dr_mai_qd.MAI_cf = GetValueFromXML(xmlItem, "MAI_cf")
                dr_mai_qd.MAI_filedati = GetValueFromXML(xmlItem, "MAI_filedati")
                dr_mai_qd.MAI_attivo = GetValueFromXML(xmlItem, "MAI_attivo")
                dr_mai_qd.DEF_sendbymail = False
                dr_mai_qd.DEF_onlyarchott = False
            Catch ex As Exception
                dr_mai_qd.DEF_errcode = -1
                dr_mai_qd.DEF_errdesc = ex.Message
            End Try
            ImpostaMessaggi(New String() {"", "", "", CInt(_MessaggiAvanzamento(3)) + 1, "", ""})
            _QuickDataset.TBL_main.Rows.Add(dr_mai_qd)
        Next

    End Sub

#Region "Caricamento del PRINTDATASET"

    ' ************************************************************************** '
    ' Caricamento del DataSet da utilizzare per la stampa.                       '
    ' ************************************************************************** '
    Public Sub LoadFullDataset(ByVal nRecordToLoad As Integer) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LoadFullDataset
        Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow
        'Dim myEncoding As System.Text.Encoding
        Dim nRecordToPrint As Integer
        Dim ds_src As DataSet

        'myEncoding = System.Text.Encoding.GetEncoding(1252) ' Windows-1252

        ' ************************************************************************** '
        ' Creazione del Dataset e azzeramento del contatore dei record da stampare.  '
        ' ************************************************************************** '
        nRecordToPrint = 0
        Select Case _CodiceModulo
            Case Is = ListaModuli.MOD_sollecito
                _PrintSollecitoDataset = New DS_PS_ASM_sol
            Case Is = ListaModuli.MOD_bollettino
                _PrintF24Dataset = New DS_PF24_ASM_sol
            Case Is = ListaModuli.MOD_reportsolleciti
                _PrintReportSolleciti = New DS_PR2_ASM_sol
            Case Is = ListaModuli.MOD_reportsaldoprint
                _PrintReportSolleciti = New DS_PR2_ASM_sol
            Case Is = ListaModuli.MOD_reporteccedprint
                _PrintReportSaldoPrint = New DS_PR3_ASM_sol
        End Select


        ' ************************************************************************** '
        ' Caricamento del DataSet da utilizzare per la stampa.                       '
        ' ************************************************************************** '
        If _CodiceModulo = ListaModuli.MOD_reportsolleciti Then
            GetRecordReportSolleciti()
        ElseIf _CodiceModulo = ListaModuli.MOD_reportsaldoprint Then
            GetRecordReportEccedPrint()
        ElseIf _CodiceModulo = ListaModuli.MOD_reporteccedprint Then
            ' GetRecordReportSaldoPrint()
        Else
            _CodiceProgressivo = 0
            ds_src = caricaFileDati(PrintDataSet_DataFile(1))

            While _RecordCount < _QuickDataset.TBL_main.Rows.Count And nRecordToPrint < nRecordToLoad
                dr_mai_qd = _QuickDataset.TBL_main.Rows(_RecordCount)
                If dr_mai_qd.DEF_toprint Then
                    Try
                        _ErroriLievi = ""
                        AddRecordToPrint(dr_mai_qd, ds_src.Tables(0))
                        If _ErroriLievi > "" Then
                            dr_mai_qd.DEF_errcode = -2
                            dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori Lievi: ", _ErroriLievi)
                        End If
                    Catch ex As Exception
                        dr_mai_qd.DEF_errcode = -1
                        dr_mai_qd.DEF_errdesc = ex.Message
                    End Try
                    nRecordToPrint += 1
                End If
                _RecordCount += 1
            End While
        End If

        ' ************************************************************************** '
        ' Consolidamento del DataSet da utilizzare per la stampa.                    '
        ' ************************************************************************** '
        Select Case _CodiceModulo
            Case Is = ListaModuli.MOD_sollecito
                _PrintSollecitoDataset.AcceptChanges()
            Case Is = ListaModuli.MOD_bollettino
                _PrintF24Dataset.AcceptChanges()
            Case Is = ListaModuli.MOD_reportsolleciti
                _PrintReportSolleciti.AcceptChanges()
            Case Is = ListaModuli.MOD_reportsaldoprint
                _PrintReportSolleciti.AcceptChanges()
            Case Is = ListaModuli.MOD_reporteccedprint
                _PrintReportSaldoPrint.AcceptChanges()
        End Select

    End Sub

    Private Sub AddRecordToPrint(ByVal dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow, ByVal dtExcel As DataTable)
        Dim myEncoding As System.Text.Encoding
        Dim ds_src As DataSet
        Dim ds_exc As DataSet
        Dim dr_sol As DataRow

        Try
            ds_src = caricaFileDati(PrintDataSet_DataFile(dr_mai_qd.MAI_filedati))
            'ds_exc = CaricaFileDatiToDS_excel("D:\Invoicer_svi\Archivi\SOLLECITI PROVA_NARNI\dati\20150824_DATI_SOG_SOLLECITI.xls")
            'ds_src = New DataSet
            'myEncoding = System.Text.Encoding.GetEncoding(1252) ' Windows-1252

            ''xdSoll.Load(New System.IO.StreamReader("EXP_EMISOL_01_56_39741.XML", myEncoding))
            ''ds_src.ReadXml(PrintDataSet_DataFile(dr_mai_qd.MAI_filedati))
            'ds_src.ReadXml(New System.IO.StreamReader(PrintDataSet_DataFile(dr_mai_qd.MAI_filedati), myEncoding))
            If ds_src.Tables("Sollecito").Select(String.Concat("IDSollecito = '", dr_mai_qd.MAI_num_sollecito, "'")).Length > 0 Then
                dr_sol = ds_src.Tables("Sollecito").Select(String.Concat("IDSollecito = '", dr_mai_qd.MAI_num_sollecito, "'"))(0)
                Select Case _CodiceModulo
                    Case Is = ListaModuli.MOD_sollecito
                        GetRecordSollecitoToPrint(dr_mai_qd.MAI_codice, ds_src, dr_sol, GetFeedString(dr_mai_qd))
                    Case Is = ListaModuli.MOD_bollettino
                        'GetRecordBollettiniToPrint(dr_mai_qd.MAI_codice, ds_src, dr_sol, GetFeedString(dr_mai_qd))
                        GetRecordF24ToPrint(dr_mai_qd.MAI_codice, ds_src, dr_sol, GetFeedString(dr_mai_qd))
                End Select
            End If
            ds_src = Nothing
        Catch ex As Exception
            Dim cErrorString As String
            If ex.Message = "" Then
                cErrorString = "Errore caricamento dati, mentra si caricava il dato: SCONOSCIUTO"
            Else
                cErrorString = String.Concat("Errore caricamento dati, mentra si caricava il dato: ", ex.Message)
            End If
            Throw New Exception(cErrorString, ex)
        End Try

    End Sub

    Private Function GetFeedString(ByVal dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow) As String
        Dim lAllModuli As Boolean = True
        Dim cRtn As String

        cRtn = ""
        If _FeedString > "" Then
            For i As Integer = 1 To _FeedString.Split(";").Length - 1 Step 2
                If dr_mai_qd.Item(_FeedString.Split(";")(i)) Then
                    cRtn = _FeedString.Split(";")(i + 1)
                Else
                    lAllModuli = False
                End If
            Next
            If lAllModuli Then
                cRtn = _FeedString.Split(";")(0)
            End If
        End If
        Return cRtn

    End Function

    Private ReadOnly Property PrintDataSet_DataFile(ByVal _FileIdx As Integer) As String

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode(String.Concat("filename[@codice=", "", _FileIdx, "", "]")).InnerText)
        End Get

    End Property

#End Region

#Region "Caricamento del PRINTDATSET del sollecito"

    Private Sub GetRecordSollecitoToPrint(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_sol As DataRow, ByVal cFeedString As String)
        Dim dt As DS_PS_ASM_sol.TBL_rate_sollDataTable
        Dim dr_ras As DS_PS_ASM_sol.TBL_rate_sollRow
        Dim dr_mai_ps As DS_PS_ASM_sol.TBL_mainRow
        Dim dr_pop As DS_PS_ASM_sol.TBL_pod_pdrRow
        Dim nAccred As Decimal
        Dim nPlus As Decimal
        Dim nRata As Integer
        Dim dv As DataView
        Dim i As Integer

        ' ************************************************************************** '
        ' Creazione del record guida della stampa della fattura.                     '
        ' ************************************************************************** '
        dr_mai_ps = _PrintSollecitoDataset.TBL_main.NewRow
        _CodiceProgressivo += 1
        dr_mai_ps.MAI_codice = _CodiceProgressivo
        _MasterRecord = _CodiceProgressivo
        dr_mai_ps.MAI_linkQD = nCodice
        dr_mai_ps.DEF_alimimb = cFeedString

        ' ************************************************************************** '
        ' Recuperiamo il numero dei pod per i quli si stamper� la fattura. Il campo  '
        ' di default DEF_raccolta indica se il record attuale fa parte di una raccol '
        ' ta. I valori che pu� assumere sono:                                        '
        ' ---- In caso di raccolta da pi� di un record:                              '
        '      - 1 primo record di una raccolta;                                     '
        '      - 2 record successivi della raccolta                                  '
        '      - 3 ultimo record della raccolta.                                     '
        ' ---- In caso di raccolta da pi� di un record:                              '
        '      - 9 raccolta costituita da un solo record                             '
        ' ************************************************************************** '
        dr_mai_ps.DEF_raccolta = 9

        ' ************************************************************************** '
        ' Dati dell'intestatario della fattura.                                      ' 
        ' ************************************************************************** '
        dr_mai_ps.MAI_cod_cli = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "IDSoggetto")
        dr_mai_ps.MAI_nome = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "RagioneSociale")
        If RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "PartitaIVA") = "" Then
            dr_mai_ps.MAI_cod_fis = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "CodiceFiscale")
        Else
            dr_mai_ps.MAI_cod_fis = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "PartitaIVA")
        End If


        '_dr_mai_pf.MAI_par_iva = GetValueFromXML(xiWork, Costanti.itm_piva)

        ' ************************************************************************** '
        ' Indirizzo del recapito della fattura.                                      '
        ' ************************************************************************** '
        dr_mai_ps.MAI_rec_nome = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "RecapitoFattura", dr_mai_ps.MAI_nome)
        If dr_mai_ps.MAI_rec_nome = dr_mai_ps.MAI_nome Then
            dr_mai_ps.MAI_rec_nome = ""
        End If

        dr_mai_ps.MAI_rec_sito_1 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "TipoElementoTopologico")
        dr_mai_ps.MAI_rec_sito_2 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ElementoTopologico")
        dr_mai_ps.MAI_rec_sito_3 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "NumeroCivico")
        dr_mai_ps.MAI_rec_sito_4 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "SuffissoCivico")
        dr_mai_ps.MAI_rec_sito_5 = ""
        dr_mai_ps.MAI_rec_sito_6 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "CAP")
        dr_mai_ps.MAI_rec_sito_7 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "Comune")
        dr_mai_ps.MAI_rec_sito_8 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ProvinciaSigla")
        dr_mai_ps.MAI_rec_nazione = ""
        dr_mai_ps.MAI_con_anno = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "AnnoContratto")
        dr_mai_ps.MAI_con_num = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "NumeroContratto")

        dr_mai_ps.MAI_tot_sol = CDec(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleSollecito", 0).ToString.Replace(".", ","))
        'dr_mai_ps.MAI_tot_sol = CDec(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleSollecito", 0).ToString.Replace(".", ",")) - _
        '                        (CDec(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleAnticipazioneRaggrupp", 0).ToString.Replace(".", ",")) + _
        '                        GetValueEccedentari(ds_src, dr_sol))
        'CDec(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleIncassiEccedentariSog", 0).ToString.Replace(".", ",")))
        dr_mai_ps.MAI_cod_tipo_ser = TipoServizio(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "DatiIdentificativiFattura", "Protocollo"))
        dr_mai_ps.MAI_num_sollecito = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "IDSollecito")
        _PrintSollecitoDataset.TBL_main.Rows.Add(dr_mai_ps)

        If dr_mai_ps.MAI_cod_tipo_ser = 1 Then
            dr_mai_ps.MAI_tipo_sollecito = "POD"
        ElseIf dr_mai_ps.MAI_cod_tipo_ser = 2 Then
            dr_mai_ps.MAI_tipo_sollecito = "PDR"
        ElseIf dr_mai_ps.MAI_cod_tipo_ser = -1 Then
            Throw New Exception("Descrizione del servizio non associabile.")
        End If

        'If aDr_exc.Length > 0 Then
        dr_pop = _PrintSollecitoDataset.TBL_pod_pdr.NewRow
        dr_pop.POP_cod_mai = dr_mai_ps.MAI_codice
        If dr_mai_ps.MAI_cod_tipo_ser = 1 Then
            dr_pop.POP_pod_pdr = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "POD")
        ElseIf dr_mai_ps.MAI_cod_tipo_ser = 2 Then
            dr_pop.POP_pod_pdr = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "POD")
        ElseIf dr_mai_ps.MAI_cod_tipo_ser = -1 Then
            Throw New Exception("Codice apparato non trovato.")
        End If

        dr_pop.POP_rec_for_1 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornTipoElementoTopologico")
        dr_pop.POP_rec_for_2 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornElementoTopologico")
        dr_pop.POP_rec_for_3 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornNumeroCivico")
        dr_pop.POP_rec_for_4 = ""
        dr_pop.POP_rec_for_5 = ""
        dr_pop.POP_rec_for_6 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornCAP")
        dr_pop.POP_rec_for_7 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornComune")
        dr_pop.POP_rec_for_8 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornProvinciaSigla")
        'dr_pop.POP_rec_for_1 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "TipoElementoTopologico")
        'dr_pop.POP_rec_for_2 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ElementoTopologico")
        'dr_pop.POP_rec_for_3 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "NumeroCivico")
        'dr_pop.POP_rec_for_4 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "SuffissoCivico")
        'dr_pop.POP_rec_for_5 = ""
        'dr_pop.POP_rec_for_6 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "CAP")
        'dr_pop.POP_rec_for_7 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "Comune")
        'dr_pop.POP_rec_for_8 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ProvinciaSigla")
        'dr_mai_ps.MAI_rec_nazione = ""
        _PrintSollecitoDataset.TBL_pod_pdr.Rows.Add(dr_pop)
        'End If
        dt = New DS_PS_ASM_sol.TBL_rate_sollDataTable


        nRata = 0
        For Each dr As DataRow In dr_sol.GetChildRows("Sollecito_RataSollecitata")
            dr_ras = dt.NewRow
            dr_ras.RAS_codice = (dt.Rows.Count + 1) * -1
            dr_ras.RAS_cod_mai = dr_mai_ps.MAI_codice
            dr_ras.RAS_num_doc = String.Concat(RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiIdentificativiFattura", "AnnoFattura"), "/", _
                                               RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiIdentificativiFattura", "Protocollo"), "/", _
                                               RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiIdentificativiFattura", "NumeroFattura"))
            dr_ras.RAS_data_emis = RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiIdentificativiFattura", "DataFattura")
            dr_ras.RAS_data_scad = RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiRata", "DataScadenza")
            dr_ras.RAS_imp_totale = RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiRata", "ImportoRata", 0).ToString.Replace(".", ",")
            dr_ras.RAS_imp_residuo = dr_ras.RAS_imp_totale
            If dr_ras.RAS_imp_residuo > 0 Then
                dt.Rows.Add(dr_ras)
            End If
            nRata += 1
        Next
        'nAccred = CDec(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleAnticipazioneRaggrupp", 0).ToString.Replace(".", ",")) + _
        ' GetValueEccedentari(ds_src, dr_sol)
        'CDec(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleIncassiEccedentariSog", 0).ToString.Replace(".", ",")))
        nAccred = 0
        dv = New DataView(dt)
        dv.Sort = "RAS_imp_residuo"
        i = 0
        While (i < dv.Count) And (nAccred > 0)
            dr_ras = CType(dv(i).Row, DS_PS_ASM_sol.TBL_rate_sollRow)
            dr_ras.RAS_imp_residuo -= nAccred
            If (dr_ras.RAS_imp_residuo < 0) Then
                nAccred = (dr_ras.RAS_imp_residuo * -1)
                dr_ras.RAS_imp_residuo = 0
                dr_ras.AcceptChanges()
            Else
                nAccred = 0
            End If
            i += 1
        End While

        dv = New DataView(dt)
        dv.Sort = "RAS_data_emis"
        For i = 0 To dv.Count - 1
            dr_ras = CType(dv(i).Row, DS_PS_ASM_sol.TBL_rate_sollRow)
            _PrintSollecitoDataset.TBL_rate_soll.ImportRow(dr_ras)
            dr_ras = _PrintSollecitoDataset.TBL_rate_soll.Rows(_PrintSollecitoDataset.TBL_rate_soll.Rows.Count - 1)
            dr_ras.RAS_codice = _PrintSollecitoDataset.TBL_rate_soll.Rows.Count
            dr_ras.AcceptChanges()
        Next

    End Sub

#End Region

#Region "Caricamento del PRINTDATSET del bollettino."

    'Private Sub GetRecordBollettiniToPrint(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_sol As DataRow, ByVal cFeedString As String)
    '    Dim dr_sxr As DS_PB_ASM_sol.TBL_sol_x_ratRow
    '    Dim dr_sol_pb As DS_PB_ASM_sol.TBL_sollecitoRow
    '    Dim dt As DS_PB_ASM_sol.TBL_rateDataTable
    '    Dim dr_rat As DS_PB_ASM_sol.TBL_rateRow
    '    Dim nPlus As Decimal
    '    Dim dv As DataView
    '    Dim dr As DataRow
    '    Dim i As Integer
    '    Dim nRata As Integer
    '    Dim nAccred As Double

    '    ' Carico per ciascuna fattura un record sulla tabella delle fatture e tanti record su quella dei bollettini.
    '    ' cos� posso far guidare la stampa dei bollettini da una tabella che originer� successivamente.
    '    dr_sol_pb = _PrintBollettinoDataset.TBL_sollecito.NewRow
    '    With dr_sol_pb
    '        .SOL_codice = _PrintBollettinoDataset.TBL_sollecito.Rows.Count + 1
    '        .SOL_num_sollecito = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "IDSollecito")
    '        .SOL_data_sollecito = GetValueDatiFissi("FixSol", "5")
    '        .SOL_nominativo = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "RagioneSociale")
    '        .SOL_ind_rec_1 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ElementoTopologico")
    '        .SOL_ind_rec_2 = ""
    '        .SOL_ind_rec_3 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "NumeroCivico")
    '        .SOL_ind_rec_4 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "SuffissoCivico")
    '        .SOL_ind_rec_5 = ""
    '        .SOL_ind_rec_6 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "CAP")
    '        .SOL_ind_rec_7 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "Comune")
    '        .SOL_ind_rec_8 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ProvinciaSigla")
    '        .SOL_cod_utente = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "IDSoggetto")
    '    End With
    '    _PrintBollettinoDataset.TBL_sollecito.Rows.Add(dr_sol_pb)

    '    ' Carico i record nella tabella delle rate.

    '    dt = New DS_PB_ASM_sol.TBL_rateDataTable
    '    nRata = 0
    '    For Each dr In dr_sol.GetChildRows("Sollecito_RataSollecitata")
    '        'dr_rat = _PrintBollettinoDataset.TBL_rate.NewRow
    '        dr_rat = dt.NewRow
    '        With dr_rat
    '            .RAT_codice = (dt.Rows.Count + 1) * -1
    '            .RAT_num_rata = RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiIdentificativiFattura", "DataFattura")

    '            'LP 20110125 Modificata su richiesta di Andrea Bonato. Il valore preso in precedenza era la data del sollecito.
    '            '.RAT_scadenza = dr.Item("DATA_SOLLECITO")
    '            .RAT_scadenza = RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiRata", "DataScadenza")
    '            .RAT_importo = RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiRata", "ImportoRata", 0).ToString.Replace(".", ",")
    '            .RAT_ocr = RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiRata", "OCR")
    '        End With
    '        If dr_rat.RAT_importo > 0 Then
    '            dt.Rows.Add(dr_rat)
    '        End If
    '        '    _PrintBollettinoDataset.TBL_rate.Rows.Add(dr_rat)

    '        nRata += 1
    '    Next
    '    'nAccred = CDbl(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleAnticipazioneRaggrupp", 0).ToString.Replace(".", ",")) + _
    '    '          GetValueEccedentari(ds_src, dr_sol)
    '    ''CDbl(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleIncassiEccedentariSog", 0).ToString.Replace(".", ","))
    '    'If (dt.Rows.Count > 0) And (nAccred > 0) Then
    '    '    dr_rat = dt.Rows(0)
    '    '    dr_rat.RAT_importo = dr_rat.RAT_importo - nAccred
    '    '    dr_rat.AcceptChanges()
    '    'End If

    '    'dv = New DataView(dt)
    '    'dv.Sort = "RAT_importo"
    '    'i = 0
    '    'nPlus = 0
    '    'While i < dv.Count
    '    '    dr_rat = CType(dv(i).Row, DS_PB_UE_sol.TBL_rateRow)
    '    '    If dr_rat.RAT_importo <= 0 Then
    '    '        nPlus += (dr_rat.RAT_importo * -1)
    '    '        dr_rat.Delete()
    '    '        i -= 1
    '    '    Else
    '    '        If nPlus > 0 Then
    '    '            dr_rat.RAT_importo -= nPlus
    '    '            nPlus = 0
    '    '            dr_rat.AcceptChanges()
    '    '            i -= 1
    '    '        End If
    '    '    End If
    '    '    i += 1
    '    'End While

    '    dv = New DataView(dt)
    '    dv.Sort = "RAT_num_rata"

    '    For i = 0 To dv.Count - 1
    '        dr_rat = CType(dv(i).Row, DS_PB_ASM_sol.TBL_rateRow)
    '        _PrintBollettinoDataset.TBL_rate.ImportRow(dr_rat)
    '        dr_rat = _PrintBollettinoDataset.TBL_rate.Rows(_PrintBollettinoDataset.TBL_rate.Rows.Count - 1)
    '        dr_rat.RAT_codice = _PrintBollettinoDataset.TBL_rate.Rows.Count
    '        ' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
    '        dr_sxr = _PrintBollettinoDataset.TBL_sol_x_rat.NewRow
    '        dr_sxr.SXR_codice = _PrintBollettinoDataset.TBL_sol_x_rat.Rows.Count + 1
    '        dr_sxr.DEF_alimimb = cFeedString
    '        ' per il bollettino non dovrebbero esserci problemi di raccolta.
    '        ' Al momento il campo viene valorizzato con questi valori per le fasi successive si vedr�.
    '        If dv.Count = 1 Then
    '            dr_sxr.DEF_raccolta = 9
    '        Else
    '            If i = 0 Then
    '                dr_sxr.DEF_raccolta = 1
    '            Else
    '                If i = dv.Count - 1 Then
    '                    dr_sxr.DEF_raccolta = 3
    '                Else
    '                    dr_sxr.DEF_raccolta = 2
    '                End If
    '            End If
    '        End If

    '        dr_sxr.SXR_linkQD = nCodice
    '        dr_sxr.SXR_cod_sol = dr_sol_pb.SOL_codice()
    '        dr_sxr.SXR_cod_rat = dr_rat.RAT_codice
    '        dr_sxr.SXR_cod_tipo_serv = TipoServizio(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "DatiIdentificativiFattura", "Protocollo"))

    '        ' Mi serve per controllare il codice dell'imbustatrice.
    '        If i = dv.Count - 1 Then
    '            dr_sxr.SXR_ultimo = 1
    '        End If
    '        _PrintBollettinoDataset.TBL_sol_x_rat.Rows.Add(dr_sxr)
    '    Next

    'End Sub

#End Region

#Region "Caricamento del PRINTDATASET dell'F24 semplificato"

    Private Sub GetRecordF24ToPrint(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_sol As DataRow, ByVal cFeedString As String)
        '  Dim dr_dfa_tra As DS_import.TBL_dati_fattRow
        ' Dim dr_daf_tra As DS_import.TBL_dati_fornRow
        Dim dr_exc As DataRow
        Dim dr_fxr As DS_PF24_ASM_sol.TBL_fat_x_ratRow
        Dim dr_rdo As DS_PF24_ASM_sol.TBL_rich_domRow
        Dim dr_fat As DS_PF24_ASM_sol.TBL_fattureRow
        Dim dr_rat As DS_PF24_ASM_sol.TBL_rateRow
        Dim nRata As Integer
        Dim nIdSog As Integer
        Dim dt As DataTable

        nIdSog = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "IDSoggetto")

        'If ds_exc.Tables(0).Select(String.Concat("sog_id = '", nIdSog, "'")).Length = 1 Then
        '    dr_exc = ds_exc.Tables(0).Select(String.Concat("sog_id = '", nIdSog, "'"))(0)
        'Else
        '    dr_exc = Nothing
        'End If

        dr_fat = _PrintF24Dataset.TBL_fatture.NewRow
        With dr_fat

            .FAT_codice = _PrintF24Dataset.TBL_fatture.Rows.Count + 1
            .FAT_anno_fattura = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "DatiIdentificativiFattura", "AnnoFattura")
            .FAT_num_fattura = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "IDSollecito")
            .FAT_num_sollecito = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "IDSollecito")
            'RecuperaValoreDS_UE(ds_src, dr_sol, 0, "DatiIdentificativiFattura", "NumeroFattura")
            'If dr_exc Is Nothing Then
            '                .FAT_cognome = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IntestatarioFattura", "RagioneSociale")
            '                .FAT_nome = ""
            '                .FAT_codice_fiscale = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IntestatarioFattura", "PartitaIva")
            '                .FAT_data_nascita = ""
            '                .FAT_sesso = ""
            '                .FAT_comune_nasc = ""
            '                .FAT_prov_nasc = ""
            '            Else
            '                ' sog_id	sog_ragsoc	sog_codfis	sog_pariva	sog_nome	sog_cogn	sog_flsesso	sog_dtnas	naz_desnas	com_desnas	pro_desnas

            '                .FAT_cognome = dr_exc("sog_cogn")
            '                .FAT_nome = dr_exc("sog_nome")
            '                .FAT_codice_fiscale = dr_exc("sog_codfis")
            '                .FAT_data_nascita = CDate(dr_exc("sog_dtnas")).ToString("ddMMyyyy")
            '                .FAT_sesso = dr_exc("sog_flsesso")
            '                If dr_exc("naz_desnas") = "ITALIA" Then
            '                    .FAT_comune_nasc = dr_exc("com_desnas")
            '                    .FAT_prov_nasc = dr_exc("pro_desnas")
            '                Else
            '                    .FAT_comune_nasc = dr_exc("naz_desnas")
            '                    .FAT_prov_nasc = "EE"
            '                End If
            '            End If            
            If RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "DataNascita") = "" Then
                .FAT_cognome = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "RagioneSociale")
                .FAT_nome = ""
                .FAT_codice_fiscale = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "PartitaIva")
                .FAT_data_nascita = ""
                .FAT_sesso = ""
                .FAT_comune_nasc = ""
                .FAT_prov_nasc = ""
            Else
                .FAT_cognome = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "Cognome")
                .FAT_nome = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "Nome")
                .FAT_codice_fiscale = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "CodiceFiscale")
                .FAT_data_nascita = CDate(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "DataNascita")).ToString("ddMMyyyy")
                .FAT_sesso = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "Sesso")
                If RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "NazioneNascita") = "ITALIA" Then
                    .FAT_comune_nasc = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "ComuneNascita")
                    .FAT_prov_nasc = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "ProvinciaNascita")
                Else
                    .FAT_comune_nasc = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "NazioneNascita")
                    .FAT_prov_nasc = "EE"
                End If
            End If
            If RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Rate", "ImportoRata") > "" Then
                .FAT_importo_totale = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Rate", "ImportoRata").ToString().Replace(".", ",")
            Else
                .FAT_importo_totale = 0
            End If
        End With
        _PrintF24Dataset.TBL_fatture.Rows.Add(dr_fat)

        '
        ' Aggiungo un record per ogni rata da sollecitare.
        '
        dt = New DS_PF24_ASM_sol.TBL_rateDataTable
        nRata = 0
        For Each dr As DataRow In dr_sol.GetChildRows("Sollecito_RataSollecitata")
            dr_rat = dt.NewRow
            With dr_rat
                .RAT_num_rata = nRata
                .RAT_codice = (dt.Rows.Count + 1) * -1
                .RAT_sezione = "EL"
                .RAT_cod_tributo = "3944"
                .RAT_codice_ente = "I981"
                .RAT_num_immobili = ""
                .RAT_rateizzazione = "0101"
                .RAT_anno_riferimento = GetValueDatiFissi(Costanti.DatiFissi_F24, Costanti.KEY_DF24_anno)
                .RAT_codice_identificativo = ""
                .RAT_scadenza = ""
                If RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiRata", "ImportoRata") > "" Then
                    .RAT_codice_identificativo = RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiRata", "OCR")
                    .RAT_importo = RecuperaValoreDS_UE(ds_src, dr_sol, nRata, "DatiRata", "ImportoRata").ToString().Replace(".", ",")
                Else
                    .RAT_importo = 0
                End If
            End With
            If dr_rat.RAT_importo > 0 Then
                dt.Rows.Add(dr_rat)
            End If
            nRata += 1
        Next

        Dim dv As DataView
        Dim i As Integer

        ' Carico i record nella tabella delle rate.5
        dv = New DataView(dt)
        dv.Sort = "RAT_num_rata"

        For i = 0 To dv.Count - 1
            dr_rat = CType(dv(i).Row, DS_PF24_ASM_sol.TBL_rateRow)
            _PrintF24Dataset.TBL_rate.ImportRow(dr_rat)
            dr_rat = _PrintF24Dataset.TBL_rate.Rows(_PrintF24Dataset.TBL_rate.Rows.Count - 1)
            dr_rat.RAT_codice = _PrintF24Dataset.TBL_rate.Rows.Count
            ' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
            dr_fxr = _PrintF24Dataset.TBL_fat_x_rat.NewRow
            dr_fxr.FXR_codice = _PrintF24Dataset.TBL_fat_x_rat.Rows.Count + 1
            dr_fxr.DEF_alimimb = cFeedString
            ' per il bollettino non dovrebbero esserci problemi di raccolta.
            ' Al momento il campo viene valorizzato con questi valori per le fasi successive si vedr�.
            If dv.Count = 1 Then
                dr_fxr.DEF_raccolta = 9
            Else
                If i = 0 Then
                    dr_fxr.DEF_raccolta = 1
                Else
                    If i = dv.Count - 1 Then
                        dr_fxr.DEF_raccolta = 3
                    Else
                        dr_fxr.DEF_raccolta = 2
                    End If
                End If
            End If

            dr_fxr.FXR_linkQD = nCodice
            dr_fxr.FXR_cod_fat = dr_fat.FAT_codice()
            dr_fxr.FXR_cod_rat = dr_rat.RAT_codice
            'dr_fxr.fXR_cod_tipo_serv = TipoServizio(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "DatiIdentificativiFattura", "Protocollo"))

            ' Mi serve per controllare il codice dell'imbustatrice.
            If i = dv.Count - 1 Then
                dr_fxr.FXR_ultimo = 1
            End If
            _PrintF24Dataset.TBL_fat_x_rat.Rows.Add(dr_fxr)
        Next
        '' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
        'dr_fxr = _PrintF24Dataset.TBL_fat_x_rat.NewRow
        'dr_fxr.FXR_codice = _PrintF24Dataset.TBL_fat_x_rat.Rows.Count + 1
        'dr_fxr.DEF_alimimb = cFeedString
        'dr_fxr.DEF_raccolta = 3
        'dr_fxr.FXR_linkQD = nCodice
        'dr_fxr.FXR_cod_fat = dr_fat.FAT_codice
        'dr_fxr.FXR_cod_rat = dr_rat.RAT_codice
        '_PrintF24Dataset.TBL_fat_x_rat.Rows.Add(dr_fxr)

        '' Carico i record nella richiesta di domiciliazione 
        'dr_rdo = _PrintF24Dataset.TBL_rich_dom.NewRow
        'With dr_rdo
        '    .RDO_codice = _PrintF24Dataset.TBL_rich_dom.Rows.Count + 1
        '    .RDO_cod_fat = dr_fat.FAT_codice
        '    .RDO_cod_sia = GetValueDatiFissi("FixBol", "6")
        '    .RDO_tipo_serv = GetValueDatiFissi("FixBol", "7")
        '    .RDO_cod_domic = ""
        'End With
        '_PrintF24Dataset.TBL_rich_dom.Rows.Add(dr_rdo)

    End Sub

#End Region

#Region "caricamento del printdataset del report dei solleciti non stampati"

    Private Sub GetRecordReportNoPrint()
        Dim dt As DS_PS_ASM_sol.TBL_rate_sollDataTable
        Dim dr_mai As DS_PR1_ASM_sol.TBL_mainRow
        Dim dr_mai_rec As DS_PR1_ASM_sol.TBL_main_recRow
        Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow
        Dim dr_mai_ps As DS_PS_ASM_sol.TBL_mainRow
        Dim dr_pop As DS_PS_ASM_sol.TBL_pod_pdrRow
        Dim nPlus As Decimal
        Dim dv As DataView
        Dim i As Integer
        Dim dc As DataColumn

        ' ************************************************************************** '
        ' Creazione del record guida della stampa del report.                        '
        ' ************************************************************************** '
        dr_mai = _PrintReportSolleciti.TBL_main.NewRow
        dr_mai.MAI_codice = 1
        dr_mai.DEF_raccolta = 9
        _PrintReportSolleciti.TBL_main.Rows.Add(dr_mai)

        For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
            If dr_mai_qd.DEF_errcode = 1 Then
                dr_mai_rec = _PrintReportSolleciti.TBL_main_rec.NewRow
                dr_mai_rec.MAI_cod_mai = 1
                For Each dc In _QuickDataset.TBL_main.Columns
                    If _PrintReportSolleciti.TBL_main_rec.Columns.IndexOf(dc.ColumnName) > -1 Then
                        dr_mai_rec(dc.ColumnName) = dr_mai_qd(dc.ColumnName)
                    End If
                Next
                _PrintReportSolleciti.TBL_main_rec.Rows.Add(dr_mai_rec)
            End If
        Next

    End Sub

#End Region

#Region "caricamento del printdataset del report dei saldi dei solleciti"

    Private Sub GetRecordReportSolleciti()
        Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow
        Dim dr_mai As DS_PR2_ASM_sol.TBL_mainRow
        Dim ds_src As DataSet
        Dim dr_sol As DataRow

        Try
            'myEncoding = System.Text.Encoding.GetEncoding(1252)
            ' ************************************************************************** '
            ' Creazione del record guida della stampa del report.                        '
            ' ************************************************************************** '
            dr_mai = _PrintReportSolleciti.TBL_main.NewRow
            dr_mai.MAI_codice = 1
            dr_mai.DEF_raccolta = 9
            _PrintReportSolleciti.TBL_main.Rows.Add(dr_mai)

            ds_src = caricaFileDati(PrintDataSet_DataFile(1))
            For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
                dr_sol = ds_src.Tables("Sollecito").Select(String.Concat("IDSollecito = '", dr_mai_qd.MAI_num_sollecito, "'"))(0)
                GetRecordSollecito(dr_mai.MAI_codice, ds_src, dr_sol, GetFeedString(dr_mai_qd), dr_mai_qd.DEF_toprint, dr_mai_qd.DEF_errcode = 0, dr_mai_qd.DEF_errdesc)
            Next
            ds_src = Nothing
        Catch ex As Exception
            Dim cErrorString As String
            If ex.Message = "" Then
                cErrorString = "Errore caricamento dati, mentra si caricava il dato: SCONOSCIUTO"
            Else
                cErrorString = String.Concat("Errore caricamento dati, mentra si caricava il dato: ", ex.Message)
            End If
            Throw New Exception(cErrorString, ex)
        End Try

    End Sub

    Private Sub GetRecordSollecito(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_sol As DataRow, ByVal cFeedString As String, ByVal lSelected As Boolean, ByVal lprinted As Boolean, ByVal cMotivo As String)
        Dim dr_mai_rec As DS_PR2_ASM_sol.TBL_main_recRow

        '        Cod(cliente)
        'Rag sociale/nome cognome
        '        Tot(sollecitato)
        '        Matricola()
        '        Anno(contratto)
        '        Num(contratto)
        'Pdr/pod
        'Stato contratto attivo/cessato
        '        Indirizzo(fornitura)
        dr_mai_rec = _PrintReportSolleciti.TBL_main_rec.NewRow
        dr_mai_rec.MAI_cod_mai = nCodice
        dr_mai_rec.DEF_toprint = lSelected
        dr_mai_rec.MAI_num_utente = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "IDSoggetto")
        dr_mai_rec.MAI_nominativo = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "RagioneSociale")
        dr_mai_rec.MAI_num_sollecito = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "IDSollecito")
        If RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "PartitaIVA") = "" Then
            dr_mai_rec.MAI_cod_fis = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "CodiceFiscale")
        Else
            dr_mai_rec.MAI_cod_fis = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "PartitaIVA")
        End If
        dr_mai_rec.MAI_totale_soll = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleSollecito", 0).ToString.Replace(".", ",")
        dr_mai_rec.MAI_totale_ecc = 0   'GetValueEccedentari(ds_src, dr_sol)  'RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleIncassiEccedentariSog", 0).ToString.Replace(".", ",")
        dr_mai_rec.MAI_totale_ant = 0   'RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleAnticipazioneRaggrupp", 0).ToString.Replace(".", ",")
        dr_mai_rec.MAI_matricola = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "MatricolaContatore")
        dr_mai_rec.MAI_pod = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "POD")
        dr_mai_rec.MAI_contr_anno = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "AnnoContratto")
        dr_mai_rec.MAI_contr_numero = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "NumeroContratto")
        If RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Contratto", "StatoContratto").ToString.ToUpper = "ATTIVO" Then
            dr_mai_rec.MAI_stato = "Attivo"
        Else
            dr_mai_rec.MAI_stato = "Chiuso"
        End If
        dr_mai_rec.MAI_rec_for_1 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornTipoElementoTopologico")
        dr_mai_rec.MAI_rec_for_2 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornElementoTopologico")
        dr_mai_rec.MAI_rec_for_3 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornNumeroCivico")
        dr_mai_rec.MAI_rec_for_4 = ""
        dr_mai_rec.MAI_rec_for_5 = ""
        dr_mai_rec.MAI_rec_for_6 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornCAP")
        dr_mai_rec.MAI_rec_for_7 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornComune")
        dr_mai_rec.MAI_rec_for_8 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "UbicazioneFornitura", "FornProvinciaSigla")
        dr_mai_rec.MAI_rec_1 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "TipoElementoTopologico")
        dr_mai_rec.MAI_rec_2 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ElementoTopologico")
        dr_mai_rec.MAI_rec_3 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "NumeroCivico")
        dr_mai_rec.MAI_rec_4 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "SuffissoCivico")
        dr_mai_rec.MAI_rec_5 = ""
        dr_mai_rec.MAI_rec_6 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "CAP")
        dr_mai_rec.MAI_rec_7 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "Comune")
        dr_mai_rec.MAI_rec_8 = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "IndirizzoSpedizione", "ProvinciaSigla")
        dr_mai_rec.MAI_saldo = dr_mai_rec.MAI_totale_soll - (dr_mai_rec.MAI_totale_ecc + dr_mai_rec.MAI_totale_ant)

        If lprinted Then
            dr_mai_rec.MAI_stampato = "Stampato"
        Else
            dr_mai_rec.MAI_stampato = "Non stampato"
        End If
        dr_mai_rec.MAI_motivo = cMotivo
        _PrintReportSolleciti.TBL_main_rec.Rows.Add(dr_mai_rec)

    End Sub

    Private Sub GetRecordSaldo(ByVal nCodice As Integer, ByVal ds_src As DataSet, ByVal dr_sol As DataRow, ByVal cFeedString As String, ByVal lprint As Boolean, ByVal cDescr As String)
        Dim dr_mai_rec As DS_PR2_ASM_sol.TBL_main_recRow

        dr_mai_rec = _PrintReportSaldoPrint.TBL_main_rec.NewRow
        dr_mai_rec.MAI_cod_mai = nCodice
        dr_mai_rec.MAI_num_utente = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "IDSoggetto")
        dr_mai_rec.MAI_nominativo = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "Cliente", "RagioneSociale")
        dr_mai_rec.MAI_num_sollecito = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "IDSollecito")
        dr_mai_rec.MAI_totale_soll = RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleSollecito", 0).ToString.Replace(".", ",")
        dr_mai_rec.MAI_totale_ecc = 0  'GetValueEccedentari(ds_src, dr_sol)       'RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleIncassiEccedentariSog", 0).ToString.Replace(".", ",")
        dr_mai_rec.MAI_totale_ant = 0  'RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleAnticipazioneRaggrupp", 0).ToString.Replace(".", ",")
        dr_mai_rec.MAI_saldo = dr_mai_rec.MAI_totale_soll - (dr_mai_rec.MAI_totale_ecc + dr_mai_rec.MAI_totale_ant)
        _PrintReportSaldoPrint.TBL_main_rec.Rows.Add(dr_mai_rec)

    End Sub

#End Region

#Region "caricamento del printdataset del report dei saldi dei solleciti con importi eccedentari"

    Private Sub GetRecordReportEccedPrint()
        Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow
        Dim dr_mai As DS_PR2_ASM_sol.TBL_mainRow
        Dim ds_src As DataSet
        Dim dr_sol As DataRow

        Try
            'myEncoding = System.Text.Encoding.GetEncoding(1252)
            ' ************************************************************************** '
            ' Creazione del record guida della stampa del report.                        '
            ' ************************************************************************** '
            dr_mai = _PrintReportSolleciti.TBL_main.NewRow
            dr_mai.MAI_codice = 1
            dr_mai.DEF_raccolta = 9
            _PrintReportSolleciti.TBL_main.Rows.Add(dr_mai)

            ds_src = caricaFileDati(PrintDataSet_DataFile(1))
            'ds_src = New DataSet
            'ds_src.ReadXml(New System.IO.StreamReader(PrintDataSet_DataFile(1), myEncoding))

            For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
                If dr_mai_qd.DEF_errcode < 1 Then
                    If ds_src.Tables("Sollecito").Select(String.Concat("IDSollecito = '", dr_mai_qd.MAI_num_sollecito, "'")).Length > 0 Then
                        dr_sol = ds_src.Tables("Sollecito").Select(String.Concat("IDSollecito = '", dr_mai_qd.MAI_num_sollecito, "'"))(0)
                        '                        GetRecordSaldoEcced(dr_mai.MAI_codice, ds_src, dr_sol, GetFeedString(dr_mai_qd))
                    End If
                End If
            Next
            ds_src = Nothing
        Catch ex As Exception
            Dim cErrorString As String
            If ex.Message = "" Then
                cErrorString = "Errore caricamento dati, mentra si caricava il dato: SCONOSCIUTO"
            Else
                cErrorString = String.Concat("Errore caricamento dati, mentra si caricava il dato: ", ex.Message)
            End If
            Throw New Exception(cErrorString, ex)
        End Try

    End Sub


#End Region

    Private Function TipoServizio(ByVal cDesServizio As String) As Integer
        Dim nTmp As Integer

        nTmp = -1
        If cDesServizio = "CF" Then
            nTmp = 1
        End If
        Return nTmp

    End Function

    Private Function GetValueFromXML(ByVal xmlItem As Xml.XmlNode, ByVal xmlPath As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cDefault
            If Not xmlItem.SelectSingleNode(xmlPath) Is Nothing Then
                cValue = xmlItem.SelectSingleNode(xmlPath).InnerText
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori del file XML.", vbLf, "percorso da cui si tenta il recupero:", vbLf, xmlPath), ex)
        End Try
        Return cValue

    End Function

    Private Function GetValueFromDR(ByVal dr As DataRow, ByVal cField As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cDefault
            If Not IsDBNull(dr.Item(cField)) Then
                cValue = dr.Item(cField)
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori dal record.", vbLf, "percorso da cui si tenta il recupero:", vbLf, cField), ex)
        End Try
        Return cValue

    End Function

    Private Sub AddErroreLieve(ByVal cMsg As String)

        If _ErroriLievi = "" Then
            _ErroriLievi = cMsg
        Else
            _ErroriLievi = String.Concat(_ErroriLievi, "; ", cMsg)
        End If

    End Sub

    Private Sub ImpostaMessaggi(ByVal aValue As String())
        Dim i As Integer

        For i = 0 To 5
            If aValue(i) > "" Then
                _MessaggiAvanzamento(i) = aValue(i)
            End If
        Next

    End Sub

    'Public Sub PostPreStampa(ByVal aLista As ArrayList) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.PostPreStampa

    '    ' Non ci sono azioni da compiere in questo caso.

    'End Sub

    Private Sub EsportaDati(ByVal cFileName As String, ByVal cLotto As String)
        Dim dtExport As DS_export.Export_SollecitiDataTable
        Dim dr_exp As DS_export.Export_SollecitiRow
        Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow
        Dim sw As System.IO.StreamWriter
        Dim cValue As String
        Dim cRow As String
        Dim i As Integer

        dtExport = New DS_export.Export_SollecitiDataTable
        For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
            If dr_mai_qd("DEF_errcode") = 0 Then
                dr_exp = dtExport.NewRow

                dr_exp("NLotto") = cLotto
                dr_exp("nrbusta") = dr_mai_qd.MAI_codice
                dr_exp("CodCartella_Carico") = dr_mai_qd.MAI_cod_contratto
                dr_exp("Titolo") = ""
                dr_exp("Nominativo") = dr_mai_qd.MAI_nominativo
                dr_exp("indirizzo") = dr_mai_qd.MAI_indirizzo_recapito
                dr_exp("cap") = dr_mai_qd.MAI_cap_recapito
                dr_exp("Localita") = dr_mai_qd.MAI_citta_recapito
                dr_exp("gstampa") = ""
                dr_exp("tspedizione") = ""
                dr_exp("cf") = dr_mai_qd.MAI_cf
                dr_exp("importo") = dr_mai_qd.MAI_totale
                dr_exp("tipografia") = ""
                dtExport.Rows.Add(dr_exp)
            End If
        Next

        ' Apri lo stream di output
        sw = New System.IO.StreamWriter(cFileName)
        cRow = ""

        For i = 0 To dtExport.Columns.Count - 1
            cValue = dtExport.Columns(i).ColumnName
            If cRow = "" Then
                cRow = cValue
            Else
                cRow = String.Concat(cRow, ";", cValue)
            End If
        Next

        sw.WriteLine(cRow)
        For Each dr_exp In dtExport.Rows

            ' Carica le righe successive con i dati estratti sempre che ce ne siano.
            cRow = ""
            For i = 0 To dtExport.Columns.Count - 1
                If IsDBNull(dr_exp(dtExport.Columns(i).ColumnName)) Then
                    cValue = " "
                Else
                    cValue = Trim(dr_exp(dtExport.Columns(i).ColumnName))
                End If

                If i = 0 Then
                    cRow = cValue
                Else
                    cRow = String.Concat(cRow, ";", cValue)
                End If
            Next
            If cRow.Trim.Length > 0 Then
                sw.WriteLine(cRow)
            End If
        Next
        ' Chiudi lo stream di output
        sw.Close()
        sw.Dispose()

    End Sub

    Private Function RecuperaValoreDS_UE(ByVal ds_src As DataSet, ByVal dr_sol As DataRow, ByVal nRataID As Integer, ByVal cTable As String, ByVal cField As String, Optional ByVal cDefault As String = "")
        Dim dr_tmp As DataRow
        Dim cRtnValue As String
        Dim lFieldExist As Boolean

        cRtnValue = cDefault
        lFieldExist = ds_src.Tables.Contains(cTable)
        If lFieldExist Then
            lFieldExist = ds_src.Tables(cTable).Columns.Contains(cField)
        End If
        If lFieldExist Or cTable = "" Then
            Select Case cTable.ToLower
                Case Is = "".ToLower
                    If IsDBNull(dr_sol(cField)) Then
                        cRtnValue = cDefault
                    Else
                        cRtnValue = dr_sol(cField)
                    End If
                Case Is = "Cliente".ToLower
                    If dr_sol.GetChildRows("Sollecito_Cliente").Length > 0 Then
                        dr_tmp = dr_sol.GetChildRows("Sollecito_Cliente")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "IndirizzoSpedizione".ToLower
                    If dr_sol.GetChildRows("Sollecito_IndirizzoSpedizione").Length > 0 Then
                        dr_tmp = dr_sol.GetChildRows("Sollecito_IndirizzoSpedizione")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                Case Is = "Contratto".ToLower
                    If dr_sol.GetChildRows("Sollecito_RataSollecitata").Length > 0 Then
                        dr_tmp = dr_sol.GetChildRows("Sollecito_RataSollecitata")(0)
                        If dr_tmp.GetChildRows("RataSollecitata_SezioniDocumenti").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("RataSollecitata_SezioniDocumenti")(0)
                            If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                Case Is = "DatiIdentificativiFattura".ToLower
                    If dr_sol.GetChildRows("Sollecito_RataSollecitata").Length > 0 Then
                        dr_tmp = dr_sol.GetChildRows("Sollecito_RataSollecitata")(nRataID)
                        If dr_tmp.GetChildRows("RataSollecitata_DatiIdentificativiFattura").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("RataSollecitata_DatiIdentificativiFattura")(0)
                            If IsDBNull(dr_tmp(cField)) Then
                                cRtnValue = cDefault
                            Else
                                cRtnValue = dr_tmp(cField)
                            End If
                        End If
                    End If
                Case Is = "UbicazioneFornitura".ToLower
                    If dr_sol.GetChildRows("Sollecito_RataSollecitata").Length > 0 Then
                        dr_tmp = dr_sol.GetChildRows("Sollecito_RataSollecitata")(0)
                        If dr_tmp.GetChildRows("RataSollecitata_SezioniDocumenti").Length > 0 Then
                            dr_tmp = dr_tmp.GetChildRows("RataSollecitata_SezioniDocumenti")(0)
                            If dr_tmp.GetChildRows("SezioniDocumenti_UbicazioneFornitura").Length > 0 Then
                                dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_UbicazioneFornitura")(0)
                                If IsDBNull(dr_tmp(cField)) Then
                                    cRtnValue = cDefault
                                Else
                                    cRtnValue = dr_tmp(cField)
                                End If
                            End If
                        End If
                    End If
                    '    If dr_fat.GetChildRows("Fattura_IndirizzoSedeLegale").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_IndirizzoSedeLegale")(0)
                    '        If IsDBNull(dr_tmp(cField)) Then
                    '            cRtnValue = cDefault
                    '        Else
                    '            cRtnValue = dr_tmp(cField)
                    '        End If
                    '    End If
                    'Case Is = "DatiSpedizione".ToLower
                    '    If dr_fat.GetChildRows("Fattura_DatiSpedizione").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_DatiSpedizione")(0)
                    '        If IsDBNull(dr_tmp(cField)) Then
                    '            cRtnValue = cDefault
                    '        Else
                    '            cRtnValue = dr_tmp(cField)
                    '        End If
                    '    End If
                    'Case Is = "IndirizzoSpedizione".ToLower
                    '    If dr_fat.GetChildRows("Fattura_DatiSpedizione").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_DatiSpedizione")(0)
                    '        If cField = "IndirizzoEmailInvioFattura" Then
                    '            If IsDBNull(dr_tmp(cField)) Then
                    '                cRtnValue = cDefault
                    '            Else
                    '                cRtnValue = dr_tmp(cField)
                    '            End If
                    '        Else
                    '            If dr_tmp.GetChildRows("DatiSpedizione_IndirizzoSpedizione").Length > 0 Then
                    '                dr_tmp = dr_tmp.GetChildRows("DatiSpedizione_IndirizzoSpedizione")(0)
                    '                If IsDBNull(dr_tmp(cField)) Then
                    '                    cRtnValue = cDefault
                    '                Else
                    '                    cRtnValue = dr_tmp(cField)
                    '                End If
                    '            End If
                    '        End If
                    '    End If
                    'Case Is = "PuntoDiFornitura".ToLower
                    '    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                    '        If dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio").Length > 0 Then
                    '            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio")(0)
                    '            If dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura").Length > 0 Then
                    '                dr_tmp = dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura")(0)
                    '                If IsDBNull(dr_tmp(cField)) Then
                    '                    cRtnValue = cDefault
                    '                Else
                    '                    cRtnValue = dr_tmp(cField)
                    '                End If
                    '            End If
                    '        End If
                    '    End If
                    'Case Is = "Dilazione".ToLower
                    '    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                    '        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                    '            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                    '            If IsDBNull(dr_tmp(cField)) Then
                    '                cRtnValue = cDefault
                    '            Else
                    '                cRtnValue = dr_tmp(cField)
                    '            End If
                    '        End If
                    '    End If
                    'Case Is = "OT".ToLower
                    '    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                    '        If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                    '            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                    '            If dr_tmp.GetChildRows("Contratto_OT").Length > 0 Then
                    '                dr_tmp = dr_tmp.GetChildRows("Contratto_OT")(0)
                    '                If IsDBNull(dr_tmp(cField)) Then
                    '                    cRtnValue = cDefault
                    '                Else
                    '                    cRtnValue = dr_tmp(cField)
                    '                End If
                    '            End If
                    '        End If
                    '    End If
                    'Case Is = "DatiRichiestaDomiciliazione".ToLower
                    '    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                    '        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                    '            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                    '            If dr_tmp.GetChildRows("Dilazione_DatiRichiestaDomiciliazione").Length > 0 Then
                    '                dr_tmp = dr_tmp.GetChildRows("Dilazione_DatiRichiestaDomiciliazione")(0)
                    '                If IsDBNull(dr_tmp(cField)) Then
                    '                    cRtnValue = cDefault
                    '                Else
                    '                    cRtnValue = dr_tmp(cField)
                    '                End If
                    '            End If
                    '        End If
                    '    End If
                    'Case Is = "Rate".ToLower
                    '    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                    '        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                    '            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                    '            If dr_tmp.GetChildRows("Dilazione_Rate").Length > 0 Then
                    '                dr_tmp = dr_tmp.GetChildRows("Dilazione_Rate")(0)
                    '                If IsDBNull(dr_tmp(cField)) Then
                    '                    cRtnValue = cDefault
                    '                Else
                    '                    cRtnValue = dr_tmp(cField)
                    '                End If
                    '            End If
                    '        End If
                    '    End If
                    'Case Is = "BancaCliente".ToLower
                    '    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
                    '        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
                    '            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
                    '            If dr_tmp.GetChildRows("Dilazione_DatiDomiciliazione").Length > 0 Then
                    '                dr_tmp = dr_tmp.GetChildRows("Dilazione_DatiDomiciliazione")(0)
                    '                If dr_tmp.GetChildRows("DatiDomiciliazione_BancaCliente").Length > 0 Then
                    '                    dr_tmp = dr_tmp.GetChildRows("DatiDomiciliazione_BancaCliente")(0)
                    '                    If IsDBNull(dr_tmp(cField)) Then
                    '                        cRtnValue = cDefault
                    '                    Else
                    '                        cRtnValue = dr_tmp(cField)
                    '                    End If
                    '                End If
                    '            End If
                    '        End If
                    '    End If
                    'Case Is = "RiepilogoIVA".ToLower
                    '    If dr_fat.GetChildRows("Fattura_RiepilogoIVA").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_RiepilogoIVA")(0)
                    '        If IsDBNull(dr_tmp(cField)) Then
                    '            cRtnValue = cDefault
                    '        Else
                    '            cRtnValue = dr_tmp(cField)
                    '        End If
                    '    End If
                    'Case Is = "Contratto".ToLower
                    '    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
                    '        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
                    '        If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
                    '            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
                    '            If IsDBNull(dr_tmp(cField)) Then
                    '                cRtnValue = cDefault
                    '            Else
                    '                cRtnValue = dr_tmp(cField)
                    '            End If
                    '        End If
                    '    End If
            End Select
        ElseIf cTable.ToLower = "DatiRata".ToLower Then
            If dr_sol.GetChildRows("Sollecito_RataSollecitata").Length > 0 Then
                dr_tmp = dr_sol.GetChildRows("Sollecito_RataSollecitata")(nRataID)
                If dr_tmp.GetChildRows("RataSollecitata_Rata").Length > 0 Then
                    dr_tmp = dr_tmp.GetChildRows("RataSollecitata_Rata")(0)
                    If dr_tmp.GetChildRows("Rata_Rata").Length > 0 Then
                        dr_tmp = dr_tmp.GetChildRows("Rata_Rata")(0)
                        If IsDBNull(dr_tmp(cField)) Then
                            cRtnValue = cDefault
                        Else
                            cRtnValue = dr_tmp(cField)
                        End If
                    End If
                End If
            End If
        ElseIf cTable.ToLower = "TipoPagamento".ToLower Then
            '    If dr_fat.GetChildRows("Fattura_DatiPagamento").Length > 0 Then
            '        dr_tmp = dr_fat.GetChildRows("Fattura_DatiPagamento")(0)
            '        If dr_tmp.GetChildRows("DatiPagamento_Dilazione").Length > 0 Then
            '            dr_tmp = dr_tmp.GetChildRows("DatiPagamento_Dilazione")(0)
            '            If cField = "BollettinoPostale" Then
            '                If dr_tmp("MezzoPagamento").ToString.ToLower.Replace(" ", "") = "rimessadiretta" Then
            '                    cRtnValue = "si"
            '                Else
            '                    cRtnValue = "no"
            '                End If
            '            End If
            '        End If
            '    End If
            'ElseIf cTable.ToLower = "NumeroPOD".ToLower Then
            '    If cField.ToLower = "NumeroPOD".ToLower Then
            '        cRtnValue = dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length
            '    End If
            'ElseIf cTable.ToLower = "IndirizzoPuntoDiFornitura".ToLower Then
            '    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
            '        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
            '        If dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio").Length > 0 Then
            '            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_DatiServizio")(0)
            '            If dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura").Length > 0 Then
            '                dr_tmp = dr_tmp.GetChildRows("DatiServizio_PuntoDiFornitura")(0)
            '                If dr_tmp.GetChildRows("PuntoDiFornitura_Indirizzo").Length > 0 Then
            '                    dr_tmp = dr_tmp.GetChildRows("PuntoDiFornitura_Indirizzo")(0)
            '                    Try
            '                        If IsDBNull(dr_tmp(cField)) Then
            '                            cRtnValue = cDefault
            '                        Else
            '                            cRtnValue = dr_tmp(cField)
            '                        End If
            '                    Catch ex As Exception
            '                        cRtnValue = cDefault
            '                    End Try
            '                End If
            '            End If
            '        End If
            '    End If
            'ElseIf cField.ToLower = "IndirizzoEmailInvioFattura".ToLower Then
            '    If dr_fat.GetChildRows("Fattura_DatiSpedizione").Length > 0 Then
            '        dr_tmp = dr_fat.GetChildRows("Fattura_DatiSpedizione")(0)
            '        If cField = "IndirizzoEmailInvioFattura" Then
            '            Try
            '                If IsDBNull(dr_tmp(cField)) Then
            '                    cRtnValue = cDefault
            '                Else
            '                    cRtnValue = dr_tmp(cField)
            '                End If
            '            Catch ex As Exception
            '                cRtnValue = ""
            '            End Try
            '        End If
            '    End If
            'ElseIf cTable.ToLower = "TipoStampa".ToLower Then
            '    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
            '        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
            '        If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
            '            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
            '            For Each dr As DataRow In dr_tmp.GetChildRows("Contratto_ValoreCampoContratto")
            '                If dr("Campo").tolower = cField.ToLower Then
            '                    cRtnValue = dr("ValoreCampo")
            '                End If
            '            Next
            '        End If
            '    End If
            'ElseIf cTable.ToLower = "CodiceConsorzioAcquisto".ToLower Then
            '    If dr_fat.GetChildRows("Fattura_SezioniDocumenti").Length > 0 Then
            '        dr_tmp = dr_fat.GetChildRows("Fattura_SezioniDocumenti")(nPodiD)
            '        If dr_tmp.GetChildRows("SezioniDocumenti_Contratto").Length > 0 Then
            '            dr_tmp = dr_tmp.GetChildRows("SezioniDocumenti_Contratto")(0)
            '            If dr_tmp.GetChildRows("Contratto_AltriSoggettiCorrelati").Length > 0 Then
            '                For Each dr As DataRow In dr_tmp.GetChildRows("Contratto_AltriSoggettiCorrelati")
            '                    If dr("Ruolo").tolower = cField.ToLower Then
            '                        cRtnValue = dr.GetChildRows("AltriSoggettiCorrelati_Soggetto")(0)("IDSoggetto")
            '                    End If
            '                Next
            '            End If
            '        End If
            '    End If
        End If
        Return cRtnValue

    End Function

    Private Function caricaFileDati(ByVal cFileName As String) As DataSet
        Dim myEncoding As System.Text.Encoding
        Dim ds_src As DataSet

        Try
            myEncoding = System.Text.Encoding.GetEncoding(1252) ' Windows-1252
            ds_src = New DataSet
            ds_src.ReadXml(New System.IO.StreamReader(cFileName, myEncoding))
        Catch ex As Exception
            ds_src = Nothing
        End Try
        Return ds_src

    End Function

    Private Function GetValueEccedentari(ByVal ds_src As DataSet, ByVal dr_sol As DataRow) As Decimal
        Dim lSwitchEccedentari As Boolean
        Dim nValue As Decimal

        If dr_sol.Table.Columns.IndexOf("SwitchEccedentario") > -1 Then
            If Not IsDBNull(dr_sol("SwitchEccedentario")) Then
                lSwitchEccedentari = dr_sol("SwitchEccedentario")
            Else
                lSwitchEccedentari = True
            End If
        Else
            lSwitchEccedentari = True
        End If

        If lSwitchEccedentari Then
            nValue = CDec(RecuperaValoreDS_UE(ds_src, dr_sol, 0, "", "TotaleIncassiEccedentariSog", 0).ToString.Replace(".", ","))
        Else
            nValue = 0
        End If
        Return nValue

    End Function

    Public Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckCondPSA
        Dim cValue As String
        Dim lCond As Boolean

        cValue = ""
        lCond = False
        Return lCond

    End Function

    Public Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckCondAPI
        Dim cValue As String
        Dim lCond As Boolean


        lCond = False

        Return lCond

    End Function

    ReadOnly Property CapacitaInvioMail() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CapacitaInvioMail

        Get
            Return True
        End Get

    End Property

    ReadOnly Property DataSendByMail() As Xml.XmlNode Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DataSendByMail

        Get
            Return GetDataSendByMail()
        End Get

    End Property

    Private Sub AzioniPostStampa_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xmlDF As New Xml.XmlDocument

    End Sub

    Private Function GetDataSendByMail() As Xml.XmlNode
        Dim xnWork As Xml.XmlNode

        Return Nothing

    End Function

    ReadOnly Property DatiAzioniPostStampa() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DatiAzioniPostStampa

        Get
            Dim tmpArrList As ArrayList

            tmpArrList = New ArrayList

            Return tmpArrList
        End Get

    End Property

    ReadOnly Property DatiAzioniPreInvio() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DatiAzioniPreInvio

        Get
            Dim tmpArrList As ArrayList

            tmpArrList = New ArrayList
            Return tmpArrList
        End Get

    End Property

    ReadOnly Property FullDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataset_MainTableKey

        Get
            Select Case _CodiceModulo
                Case Is = ListaModuli.MOD_sollecito
                    Return "MAI_codice"
                Case Is = ListaModuli.MOD_bollettino
                    'Return "SXR_codice"
                    Return "FXR_codice"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    ReadOnly Property LogDS() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LogDSStampa

        Get
            LoadDSLog()
            Return _LogDS
        End Get

    End Property

    ReadOnly Property MessaggioAvviso() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.MessaggioAvviso

        Get
            Dim cValue As String

            Select Case _CodiceModulo
                Case Is = ListaModuli.MOD_sollecito, ListaModuli.MOD_bollettino
                    cValue = String.Concat("Attenzione!! Ricordiamo che prima di procedere con la stampa dei moduli selezionati", vbCr, "� necessario verificare i seguenti dati:", vbCr, "- numero del protocollo;", vbCr, "- data del protocollo.", vbLf, vbLf, "Posso procedere con la stampa di quanto selezionato?")
                Case Else
                    cValue = ""
            End Select
            Return cValue
        End Get

    End Property

    ReadOnly Property NuoviToponimi() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.NuoviToponimi

        Get
            Return _NuoviToponimi
        End Get

    End Property

    ReadOnly Property StampaInProprio() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.StampaInProprio

        Get
            Return True
        End Get

    End Property

    Public Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ExecActionPSA


    End Sub

    Public Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ExecActionAPI

    End Sub

    Sub PostAnteprima(ByVal aLista As ArrayList) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.PostAnteprima


    End Sub

    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String, ByVal cParamSelez As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetPrintable
        Dim dr_mai_qd As DS_QD_ASM_sol.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_ASM_sol.TBL_mainRow)
        Select Case _SelectFromPlugInType
            Case Is = cns_tsm_sel_contr_attivo.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_bol
            Case Is = cns_tsm_sel_contr_chiuso.ToLower
                dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_all
        End Select

    End Sub

    Sub UpdateQuickDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.UpdateQuickDatasetFile
        Dim dr_prd As DS_PS_ASM_sol.TBL_mainRow
        Dim xnTmp As Xml.XmlNode
        Dim nLinkCode As Integer

        If _CodiceModulo = ListaModuli.MOD_sollecito Then
            _xmlDati_qd = New Xml.XmlDocument
            _xmlDati_qd.Load(QuickDataSet_IndexFile)
            For Each dr_prd In _PrintSollecitoDataset.TBL_main
                nLinkCode = dr_prd(LinkField_QuickDS_FullDS)
                xnTmp = _xmlDati_qd.SelectSingleNode(String.Concat(Costanti.itm_qd_Sollecitato, "/MAI_codice[.=", _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).MAI_codice, "]")).ParentNode
                xnTmp.SelectSingleNode("DEF_errcode").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode
                xnTmp.SelectSingleNode("DEF_errdesc").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errdesc
            Next
            _xmlDati_qd.Save(QuickDataSet_IndexFile)
            _xmlDati_qd.RemoveAll()
            GC.Collect()
            _xmlDati_qd = Nothing
        End If
    End Sub

    WriteOnly Property DBGiriConnectionString() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DBGiriConnectionString

        Set(ByVal value As String)
            _DbGiriConnString = value
        End Set

    End Property

    WriteOnly Property ListaGiri() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ListaGiri

        Set(ByVal value As ArrayList)
            _ListaGiri = value
        End Set

    End Property

    WriteOnly Property SessioneNuova() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneNuova

        Set(ByVal value As Boolean)
            _SessioneNuova = value
        End Set

    End Property

    WriteOnly Property TipoCaricamento() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.TipoCaricamento

        Set(ByVal value As Integer)
            _TipoCaricamento = value
        End Set

    End Property

End Class
