Imports System.Windows.Forms
Imports TipiComuni2.TC2_utilita

Public Class PLG_ANE_tasi
    Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib

    Const cns_tsm_sel_con_bollettino = "TSM_sel_con_bollettino"
    Const cns_tsm_sel_con_allegato = "TSM_sel_con_allegato"

    Const nLunghezzaLinea = 156
    ' Costante indicante il corretto registro IVA della parte UmbriaEnergy Elettricit�
    'Const cns_prot_reg_iva_corr = "UEV"
    Const Cod_consegna_certa = "036"

    Private _ListaGiri As ArrayList
    Private _TipoCaricamento As Integer
    Private _DbGiriConnString As String
    Private _NuoviToponimi As Boolean
    ' Private _dataImport As DS_import_AE
    Private _data_avvisi As DataSet
    Private _data_avvisiFileName As String
    Private _CodiceProgressivo As Integer
    Private _SessioneDati As Xml.XmlNode
    Private _SessioneTrasformazioniFileName As String
    Private _SessioneNuova As Boolean
    Private _SessionePath As String
    Private _CodiceModulo As Integer
    Private _FeedString As String
    Private _QuickDataset As DS_QD_ANE_tasi
    Private _PrintBollettaDataset As DS_PF_ANE_tasi
    Private _PrintF24Dataset As DS_PF24_ANE_tasi
    Private _RecordCount As Integer
    Private _xmlDati_qd As Xml.XmlDocument
    Private _MessaggiAvanzamento(5) As String
    Private _ErroriLievi As String
    Private _SessioneDatiFissiFileName As String
    Private _SessioneDatiPath As String
    Private _LogDS As DataSet
    Private _ResultAL As ArrayList
    '' Private _dr_g20 As DS_import_AE.TBL_g20Row
    '  Private _dr_g40 As DS_import_AE.TBL_g40Row

    Enum ListaDataset
        DAS_quick = 0
        DAS_fattura = 1
        DAS_f24s = 3
        DAS_send_email = 999
    End Enum

#Region "Elenco delle property del tipo READONLY public"

    ReadOnly Property AlertPlugIn_PRN() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.MessaggioAvviso

        Get
            Dim cValue As String

            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura, ListaDataset.DAS_f24s
                    cValue = String.Concat("Attenzione!! Ricordiamo che prima di procedere con la stampa dei moduli selezionati", vbCr, "� necessario verificare i seguenti dati:", vbCr, "- Periodo di riferimento (Bimestre);", vbCr, "- Testo dell'autolettura.", vbLf, vbLf, "Posso procedere con la stampa di quanto selezionato?")
                Case Else
                    cValue = ""
            End Select
            Return cValue
        End Get

    End Property

    ReadOnly Property DataSendByMail() As Xml.XmlNode Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DataSendByMail

        Get
            Return GetDataSendByMail()
        End Get

    End Property

    'ReadOnly Property LinkField_QuickDS_PrintDS() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LinkField_QuickDS_PrintDS

    '    Get
    '        Select Case _CodiceModulo
    '            Case Is = ListaDataset.DAS_fattura
    '                Return "MAI_linkQD"
    '            Case Is = ListaDataset.DAS_bollettino
    '                Return "FXR_linkQD"
    '            Case Else
    '                Return Nothing
    '        End Select
    '    End Get

    'End Property

    ReadOnly Property ListaControlliFormNuovaSessione() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ListaControlliFormNuovaSessione

        Get
            Dim aCtr As ArrayList

            aCtr = New ArrayList
            aCtr.Add(New String() {"LBL_input_1", "Folder Bollette......:", "3;16", "155;13"})
            aCtr.Add(New String() {"TXB_input_1", "&1", "160;12", "610;20", "*1"})
            aCtr.Add(New String() {"BTN_fld_input_1", "", "775;10", "27;23"})
            aCtr.Add(New String() {"LBL_output_998", "Cartella di destinazione .:", "3;42", "155;13"})
            aCtr.Add(New String() {"TXB_output_998", "&998", "160;38", "610;20", "201"})
            aCtr.Add(New String() {"BTN_fld_output_998", "", "775;38", "27;23"})
            aCtr.Add(New String() {"LBL_output_999", "Descrizione archivio ......:", "3;68", "155;13"})
            aCtr.Add(New String() {"TXB_output_999", "", "160;62", "M;640;122", "202"})
            Return aCtr
        End Get

    End Property

    ReadOnly Property NomePlugIn() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.NomePlugIn

        Get
            Return "Plug-in per la stampa delle bollette della TASI del comune di Ancona (Ancona Entrate)."
        End Get

    End Property

    ReadOnly Property QuickDataset() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset

        Get
            Return _QuickDataset
        End Get

    End Property

    ReadOnly Property QuickDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset_MainTableKey

        Get
            Return "MAI_codice"
        End Get

    End Property

    ReadOnly Property QuickDataset_MainTableName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.QuickDataset_MainTableName

        Get
            Return "TBL_main"
        End Get

    End Property

    ReadOnly Property ResultAL() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ResultAL

        Get
            Return _ResultAL
        End Get

    End Property

#End Region

#Region "Elenco delle property del tipo READONLY Private"

    Private ReadOnly Property PrintDataSet_DataFile(ByVal _FileIdx As Integer)

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode(String.Concat("filename[@codice=", "", _FileIdx, "", "]")).InnerText)
        End Get

    End Property

    Private ReadOnly Property QuickDataSet_IndexFile() As String

        Get
            Return String.Concat(_SessionePath, _SessioneDati.SelectSingleNode("filename[@tipo=""0""]").InnerText)
        End Get

    End Property

#End Region

#Region "Elenco delle property del tipo WRITEONLY"

    WriteOnly Property CodiceModulo() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CodiceModulo

        Set(ByVal value As Integer)
            _CodiceModulo = value
        End Set

    End Property

    WriteOnly Property FeedString() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FeedString

        Set(ByVal value As String)
            _FeedString = value
        End Set

    End Property

    WriteOnly Property SessioneDatiPath() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneDatiPath

        Set(ByVal value As String)
            _SessioneDatiPath = value
        End Set

    End Property

    WriteOnly Property SessioneTrasformazioniFileName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneTrasformazioniFileName

        Set(ByVal value As String)
            _SessioneTrasformazioniFileName = value
        End Set

    End Property

    WriteOnly Property SessioneDatiFissiFileName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneDatiFissiFileName

        Set(ByVal value As String)
            _SessioneDatiFissiFileName = value
        End Set

    End Property

    WriteOnly Property SessioneFld() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneFld

        Set(ByVal value As String)
            _SessionePath = value
        End Set

    End Property

    WriteOnly Property SezioneDati() As Xml.XmlNode Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SezioneDati

        Set(ByVal value As Xml.XmlNode)
            _SessioneDati = value
        End Set

    End Property

#End Region

#Region "Elenco delle property del tipo READ e WRITE"

    Property MessaggiAvanzamento() As String() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.MessaggiAvanzamento

        Get
            Return _MessaggiAvanzamento
        End Get
        Set(ByVal value As String())
            _MessaggiAvanzamento = value
        End Set

    End Property

#End Region

#Region "Elenco dei metodi Pubblici"

#End Region

    Function CheckDatiSessione(ByVal aDati As ArrayList) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckDatiSessione
        Dim _lObb As Boolean = False
        Dim _l101 As Boolean = False
        Dim _l102 As Boolean = False
        Dim lRtn As Boolean = True

        For Each aValue As String() In aDati
            Select Case aValue(0)
                Case Is = 1
                    _lObb = True
                Case Is = 102
                    _l102 = True
            End Select
        Next
        If _SessioneNuova Then
            If Not _lObb Then
                MessageBox.Show("Attenzione! Non � stata indicata nessuna cartella contenente i dati. Non posso procedere con la creazione della sessione.")
                lRtn = False
            End If
        End If
        Return lRtn

    End Function

    Private Sub SetRecordIDX(ByVal xmlIndice As Xml.XmlDocument, ByVal xmlRecord As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xmlIndice.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xmlRecord.AppendChild(xiTmp)

    End Sub

    ReadOnly Property CurrentRecordNumber() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CurrentRecordNumber

        Get
            Return _RecordCount
        End Get

    End Property

    Sub GoToRecordNumber(ByVal nRecord As Integer) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.GoToRecordNumber

        _RecordCount = nRecord

    End Sub


#Region "SetUp per Datagrid di visualizzazione delle righe da stampare"

    Sub ImpostaDGVDati(ByVal DGV As DataGridView) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ImpostaDGVDati

        With DGV.Columns
            .Clear()
            .Add(Col_DaStampare)
            .Add(Col_Errore_code)
            .Add(Col_Errore_desc)
            .Add(Col_Progressivo)
            '.Add(Col_NumeroUtente)
            '.Add(Col_NumeroFattura)
            .Add(Col_Nominativo)
            .Add(COL_Indirizzo_Recapito)
            .Add(COL_CAP_Recapito)
            .Add(COL_Citta_Recapito)
            '.Add(COL_Provincia_Recapito)
            '.Add(COL_Nazione_Recapito)
            .Add(COL_Print_F24s)
            .Add(COL_Precompilato)
            '.Add(COL_Print_allegato)
            '.Add(COL_SpedByMail)
        End With

    End Sub

    Private Function Col_DaStampare() As System.Windows.Forms.DataGridViewCheckBoxColumn
        Dim col As System.Windows.Forms.DataGridViewCheckBoxColumn

        col = New System.Windows.Forms.DataGridViewCheckBoxColumn
        With col
            .HeaderText = "Stampa"
            .DataPropertyName = "DEF_toprint"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .Width = 50
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Errore_code() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errcode"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Errore_desc() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Errore"
            .DataPropertyName = "DEF_errdesc"
            .Visible = False
        End With
        Return col

    End Function

    Private Function Col_Progressivo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Progressivo"
            .DataPropertyName = "MAI_codice"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 70
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroUtente() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Utente"
            .DataPropertyName = "MAI_num_utente"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 90
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_NumeroFattura() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .HeaderText = "Num. Sollecito"
            .DataPropertyName = "MAI_num_sollecito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleRight
            .Width = 200
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function Col_Nominativo() As System.Windows.Forms.DataGridViewTextBoxColumn
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nominativo"
            .DataPropertyName = "MAI_nominativo"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Indirizzo_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Indirizzo Recapito"
            .DataPropertyName = "MAI_indirizzo_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_CAP_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "CAP"
            .DataPropertyName = "MAI_cap_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleCenter
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Citta_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Citt�"
            .DataPropertyName = "MAI_citta_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Provincia_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Prov."
            .DataPropertyName = "MAI_provincia_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Nazione_Recapito()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "Nazione"
            .DataPropertyName = "MAI_nazione_recapito"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .ReadOnly = True
        End With
        Return col

    End Function

    Private Function COL_Print_F24s()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "F24 Semp."
            .DataPropertyName = "MAI_print_f24s"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    Private Function COL_Precompilato()
        Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

        col = New System.Windows.Forms.DataGridViewTextBoxColumn
        With col
            .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
            .HeaderText = "F24 Precompilato"
            .DataPropertyName = "MAI_precompilato"
            .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            .Width = 120
            .ReadOnly = True
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Tag = "boolean"
        End With
        Return col

    End Function

    'Private Function COL_SpedByMail()
    '    Dim col As System.Windows.Forms.DataGridViewTextBoxColumn

    '    col = New System.Windows.Forms.DataGridViewTextBoxColumn
    '    With col
    '        .AutoSizeMode = Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
    '        .HeaderText = "Sped. Email"
    '        .DataPropertyName = "DEF_sendbymail"
    '        .DefaultCellStyle.Alignment = Windows.Forms.DataGridViewContentAlignment.MiddleLeft
    '        .Width = 120
    '        .ReadOnly = True
    '        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
    '        .Tag = "boolean"
    '    End With
    '    Return col

    'End Function

#End Region

    'Private Function RecuperaPeriodoRiferimento1(ByVal xiWork As Xml.XmlNode) As String
    '    Dim dTmp1 As DateTime
    '    Dim dTmp2 As DateTime
    '    Dim cRtn As String

    '    cRtn = ""
    '    dTmp1 = GetValueFromXML(xiWork, Costanti.itm_data_ini_fat)
    '    dTmp2 = GetValueFromXML(xiWork, Costanti.itm_data_fin_fat)
    '    If dTmp1.Year = dTmp2.Year And dTmp1.Month = dTmp2.Month Then
    '        cRtn = String.Concat(TrasformaMese(dTmp1.Month), " ", dTmp1.Year)
    '    Else
    '        If dTmp1.Year = dTmp2.Year Then
    '            cRtn = String.Concat(TrasformaMese(dTmp1.Month), " ", TrasformaMese(dTmp2.Month), " ", dTmp1.Year)
    '        Else
    '            cRtn = String.Concat(TrasformaMese(dTmp1.Month), " ", dTmp1.Year, " ", TrasformaMese(dTmp2.Month), " ", dTmp2.Year)
    '        End If
    '    End If
    '    Return cRtn

    'End Function

    Private Function TrasformaMese(ByVal nMese As Integer) As String
        Dim cvalue As String

        Select Case nMese
            Case Is = 1
                cvalue = "gennaio"
            Case Is = 2
                cvalue = "febbraio"
            Case Is = 3
                cvalue = "marzo"
            Case Is = 4
                cvalue = "aprile"
            Case Is = 5
                cvalue = "maggio"
            Case Is = 6
                cvalue = "giugno"
            Case Is = 7
                cvalue = "luglio"
            Case Is = 8
                cvalue = "agosto"
            Case Is = 9
                cvalue = "settembre"
            Case Is = 10
                cvalue = "ottobre"
            Case Is = 11
                cvalue = "novembre"
            Case Is = 12
                cvalue = "dicembre"
            Case Else
                cvalue = ""
        End Select
        Return cvalue

    End Function

    ReadOnly Property ElementiMenuDedicati() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ElementiMenuDedicati

        Get
            Dim SubMenuEntry As ToolStripMenuItem
            Dim MenuEntry As ToolStripMenuItem
            Dim aMenuItem As ArrayList

            aMenuItem = New ArrayList
            MenuEntry = New ToolStripMenuItem("Stampa TASI Ancona Entrate")

            'SubMenuEntry = New ToolStripMenuItem("Esporta dati solleciti")
            'AddHandler SubMenuEntry.Click, AddressOf DatiSolleciti_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Codice del Gruppo")
            'AddHandler SubMenuEntry.Click, AddressOf CodiceGruppo_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Testo autolettura")
            'AddHandler SubMenuEntry.Click, AddressOf TestoAutolettura_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Stampa autolettura")
            'SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_autolettura_stampa).ToLower = "si"
            'AddHandler SubMenuEntry.Click, AddressOf StampaAutolettura_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Testo Bonus Sociale")
            'AddHandler SubMenuEntry.Click, AddressOf TestoBonusSociale_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Stampa Bonus Sociale")
            'SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_bonus_sociale_stampa).ToLower = "si"
            'AddHandler SubMenuEntry.Click, AddressOf StampaBonusSociale_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'SubMenuEntry = New ToolStripMenuItem("Stampa Box Commerciale")
            'SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_box_commerciale).ToLower = "si"
            'AddHandler SubMenuEntry.Click, AddressOf StampaBoxCommerciale_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            ''SubMenuEntry = New ToolStripMenuItem("Forza offerta scacciapensieri")
            ''SubMenuEntry.Checked = GetValueDatiFissi(Costanti.DatiFissi_Fattura, Costanti.KEY_DF_forza_scacciapensieri).ToLower = "si"
            ''AddHandler SubMenuEntry.Click, AddressOf ForzaOffertaScacciapensieri_TSM_click
            ''MenuEntry.DropDownItems.Add(SubMenuEntry)
            'MenuEntry.DropDownItems.Add(New ToolStripSeparator())

            'SubMenuEntry = New ToolStripMenuItem("Dati Fissi")
            'AddHandler SubMenuEntry.Click, AddressOf DatiFissi_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            'MenuEntry.DropDownItems.Add(New ToolStripSeparator())

            '' Imposta tutti i record con allegato
            'SubMenuEntry = New ToolStripMenuItem("Allegato su tutti")
            'AddHandler SubMenuEntry.Click, AddressOf AllegatiAll_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            '' Imposta nessun record con allrgato
            'SubMenuEntry = New ToolStripMenuItem("Allegato su Nessuno")
            'AddHandler SubMenuEntry.Click, AddressOf AllegatiNot_click
            'MenuEntry.DropDownItems.Add(SubMenuEntry)

            aMenuItem.Add(New Object() {"n", "TSM_sessione_stampa", MenuEntry})

            'aMenuItem.Add(New Object() {"a", "TSM_selezione", New ToolStripSeparator()})

            '' Seleziona tutti i record con bollettino
            'SubMenuEntry = New ToolStripMenuItem("Con bollettino")
            'SubMenuEntry.Name = cns_tsm_sel_con_bollettino
            'aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

            '' Seleziona tutti i record con allegato
            'SubMenuEntry = New ToolStripMenuItem("Con allegato")
            'SubMenuEntry.Name = cns_tsm_sel_con_allegato
            'aMenuItem.Add(New Object() {"a", "TSM_selezione", SubMenuEntry})

            'tsb = New System.Windows.Forms.ToolStripButton
            'tsb.AutoSize = True
            'tsb.ImageTransparentColor = System.Drawing.Color.Magenta
            'AddHandler tsb.Click, AddressOf ForzaOffertaScacciapensieri_TSB_click
            'tsb.Name = "TSB_forza_scacciapensieri"
            'tsb.Text = "Forzatura scacciapensieri"
            'SetButtonForzaturaScacciapensieri(tsb)

            'aMenuItem.Add(New Object() {"a", "TOS_menu", tsb})
            Return aMenuItem
        End Get

    End Property

    'Private Sub BimestreFatturazione_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim xmlDF As New Xml.XmlDocument
    '    Dim frm As FRM_bimestre

    '    Try
    '        frm = New FRM_bimestre
    '        frm.StartPosition = FormStartPosition.CenterParent
    '        frm.TXB_bimestre.Text = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "14")
    '        If frm.ShowDialog = DialogResult.OK Then
    '            SetValueDatiFissi(Costanti.DatiFissi_Fattura, "14", frm.TXB_bimestre.Text)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il valore richiesto.", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Sub

    ' ************************************************************************** '
    ' Imposto correttamente la parte di selezione per la stampa dei record. Se   '
    ' non dobbiamo mantenere la selezione o se la dobbiamo mantenere ma il re-   ' 
    ' cord non � stampabile metto a "NON STAMPABILE (False)" il campo .          '
    ' ************************************************************************** '
    Sub SetSelectable(ByVal dr As DataRow, ByVal cFilter As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetSelectable
        Dim dr_mai_qd As DS_QD_ANE_tasi.TBL_mainRow

        dr_mai_qd = CType(dr, DS_QD_ANE_tasi.TBL_mainRow)
        dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
        If cFilter > "" Then
            dr_mai_qd.DEF_selectable = dr_mai_qd.DEF_selectable And dr_mai_qd(cFilter)
        End If

        ' ************************************************************************** '
        ' Pulisce i campi relativi agli errori in quanto ci stiamo occupando di un   '
        ' nuovo modulo.                                                              '
        ' ************************************************************************** '
        If dr_mai_qd.DEF_selectable Then
            dr_mai_qd.Item("DEF_errcode") = 0
            dr_mai_qd.Item("DEF_errdesc") = ""
        End If

    End Sub

    Private Function GetValueDatiFissi(ByVal cTable As String, ByVal cField As String) As String
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim cRtn As String

        Try
            xmlDF.Load(_SessioneDatiFissiFileName)
            xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))

            cRtn = xmlTmp.InnerText
            cRtn = cRtn.Replace("{\TBL_main;MAI_punto_fornitura/}", "&PUNTO_FORNITURA&")
            cRtn = cRtn.Replace("{\TBL_strum_misura;STM_matricola/}", "&MATRICOLA_CONT&")
            xmlDF = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    Private Sub SetValueDatiFissi(ByVal cTable As String, ByVal cField As String, ByVal cValue As String)
        Dim xmlDF As New Xml.XmlDocument
        Dim xmlTmp As Xml.XmlNode
        Dim xaTmp As Xml.XmlAttribute
        Dim xnWork2 As Xml.XmlNode

        cValue = cValue.Replace("&PUNTO_FORNITURA&", "{\TBL_main;MAI_punto_fornitura/}")
        cValue = cValue.Replace("&MATRICOLA_CONT&", "{\TBL_strum_misura;STM_matricola/}")

        xmlDF.Load(_SessioneDatiFissiFileName)
        xmlTmp = xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable, "/item[@codice=", cField, "]"))
        If Not xmlTmp Is Nothing Then
            xmlTmp = xmlTmp.SelectSingleNode("valore")
            xmlTmp.InnerText = cValue
        Else
            'Crea il nodo
            xmlTmp = xmlDF.CreateElement("item")
            xaTmp = xmlDF.CreateAttribute("codice")
            xaTmp.Value = cField
            xmlTmp.Attributes.Append(xaTmp)
            xnWork2 = xmlDF.CreateElement("valore")
            xnWork2.InnerText = cValue
            xmlTmp.AppendChild(xnWork2)
            xmlDF.SelectSingleNode(String.Concat("DatiFissi/", cTable)).AppendChild(xmlTmp)
        End If
        xmlDF.Save(_SessioneDatiFissiFileName)
        xmlDF = Nothing

    End Sub

    Sub RemovePRNDSErrorRecord() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.RemovePRNDSErrorRecord
        Dim nLinkCode As Integer

        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                'For Each dr_mai_pd As DS_QD_ASM_tia_tares.TBL_mainRow In _PrintFatturaDataset.TBL_main.Rows
                '    nLinkCode = dr_mai_pd(LinkField_QuickDS_FullDS)
                '    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                '        dr_mai_pd.Delete()
                '    End If
                'Next
                '_PrintFatturaDataset.TBL_main.AcceptChanges()
            Case Is = ListaDataset.DAS_f24s
                'For Each dr_mai_pd As DS_QD_ASM_tia_tares.TBL_fat_x_ratRow In _PrintBollettinoDataset.TBL_fat_x_rat.Rows
                '    nLinkCode = dr_mai_pd(LinkField_QuickDS_FullDS)
                '    If Not ((_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = 0) Or (_QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode = -2)) Then
                '        dr_mai_pd.Delete()
                '    End If
                'Next
                '_PrintF24Dataset.TBL_fat_x_rat.AcceptChanges()
        End Select

    End Sub

    ReadOnly Property LogDSStampa() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LogDSStampa

        Get
            LoadDSLog()
            Return _LogDS
        End Get

    End Property

    Private Sub LoadDSLog()
        Dim dr As DataRow

        For Each dr_qds As DS_QD_ANE_tasi.TBL_mainRow In _QuickDataset.TBL_main.Rows
            If dr_qds.DEF_errcode < 0 Then
                dr = _LogDS.Tables(0).NewRow
                '  dr("LOG_desc_rec_1") = String.Concat("Progressivo: ", dr_qds.MAI_codice, " Nominativo: ", dr_qds.MAI_nominativo, " Numero Fattura: ", dr_qds.MAI_num_bolletta)
                dr("LOG_stato") = dr_qds.DEF_errcode
                dr("LOG_desc_err") = dr_qds.DEF_errdesc
                _LogDS.Tables(0).Rows.Add(dr)
            End If
        Next

    End Sub

    'Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetPrintable
    '    Dim dr_mai_qd As DS_QD_ASM_tia_tares.TBL_mainRow

    '    dr_mai_qd = CType(dr, DS_QD_ASM_tia_tares.TBL_mainRow)
    '    Select Case _SelectFromPlugInType
    '        Case Is = cns_tsm_sel_con_bollettino.ToLower
    '            dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_bol
    '        Case Is = cns_tsm_sel_con_allegato.ToLower
    '            dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_all
    '    End Select

    'End Sub

    Function GetSelectedRow(ByVal _SelectFromPlugInType As String, ByVal cValue As String) As String() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.GetSelectedRow
        Dim dr_mai_qd As DS_QD_ANE_tasi.TBL_mainRow
        Dim aResult As String()
        Dim cFilter As String

        Select Case _SelectFromPlugInType
            Case Is = "tsm_ric_progressivo"
                cFilter = "MAI_codice"
            Case Else
                cFilter = ""
        End Select
        aResult = New String() {"MAI_codice", ""}
        For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
            If dr_mai_qd(cFilter) = cValue Then
                aResult(1) = dr_mai_qd.MAI_codice
            End If
        Next
        Return aResult

    End Function

    WriteOnly Property FiltroSelezione() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FiltroSelezione

        Set(ByVal value As String)

        End Set

    End Property

    'Public Sub ClearPrintDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ClearPrintDataset

    '    Select Case _CodiceModulo
    '        Case Is = ListaModuli.MOD_fattura
    '            _PrintFatturaDataset = Nothing
    '        Case Is = ListaModuli.MOD_bollettino
    '            _PrintBollettinoDataset = Nothing
    '    End Select

    'End Sub

#Region "Procedure utili per la creazione della sessione di stampa"

    Public Sub CreaDataBaseSessione(ByVal _DatiSessione As ArrayList) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CreaDataBaseSessione
        Dim xdWork As Xml.XmlDocument
        Dim xnWork2 As Xml.XmlNode
        Dim cFileName As String
        Dim nRecord As Integer
        Dim nBlocco As String
        Dim cBlocco As String
        Dim chTmp(0) As Char
        Dim nLine As Integer
        Dim cLine As String
        Dim aTemp As String()
        Dim aHeader As String()
        Dim cHeader As String
        Dim nTmp As Integer
        Dim nCodFatt As Integer
        Dim cBolletta As String
        Dim cOffSetChar As String
        Dim cErrorMessage As String
        Dim aValue As String()
        Dim nIntesta As Integer
        Dim cProgressivoRiga As String
        Dim lOnly199 As Boolean
        Dim i As Integer
        Dim cTmp As String
        'Dim dr_uxx As DS_import.TBL_record_UXXRow
        Dim xiRecord As Xml.XmlNode
        Dim cSortValue As String
        Dim dsTMp As DataSet

        _ResultAL = New ArrayList
        ImpostaMessaggi(New String() {"", "", "", "0", _DatiSessione.Count, "Elaborazione dei file da caricare...."})

        ' Copia dei file indicati nella loro posizione''
        For Each aValue In _DatiSessione
            ImportaFileInSessione(aValue(0), aValue(1))
        Next

        ' _dataImport = New DS_import_AE
        nLine = 0
        xdWork = New Xml.XmlDocument
        xdWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "FATTURE", ""))

        _MessaggiAvanzamento(5) = "Caricamento dati fatturazione in corso..."
        For Each aValue In _ResultAL
            If (aValue(3) = 1) And (aValue(4) = 0) Then
                Try
                    '
                    ' caricamento del file dal file CSV
                    '
                    'dsTMp = New DataSet
                    'dsTMp.Tables.Add(CaricaCSVToDataset(ConcatenaFolderFileValue(_SessionePath, aValue(1))))
                    dsTMp = CaricaFileDatiToDS_excel(ConcatenaFolderFileValue(_SessionePath, aValue(1)))
                    For Each dr As DataRow In dsTMp.Tables(0).Rows
                        xiRecord = xdWork.CreateNode(Xml.XmlNodeType.Element, "FATTURA_ROW", "")
                        SetRecordIDX(xdWork, xiRecord, "MAI_num_fattura", dr("CodiceACS"))
                        SetRecordIDX(xdWork, xiRecord, "MAI_nominativo", dr("Denominazione").replace("/", " "))

                        '"Via";"Civico";"EsponenteCivico"
                        cTmp = String.Concat(dr("Via"), " ", dr("Civico"))
                        If dr("EsponenteCivico").trim > "" Then
                            cTmp = String.Concat(cTmp, "/", dr("EsponenteCivico"))
                        End If
                        SetRecordIDX(xdWork, xiRecord, "MAI_indirizzo_recapito", cTmp)
                        SetRecordIDX(xdWork, xiRecord, "MAI_citta_recapito", dr("Comune"))
                        SetRecordIDX(xdWork, xiRecord, "MAI_provincia_recapito", dr("Provincia"))
                        SetRecordIDX(xdWork, xiRecord, "MAI_cap_recapito", dr("CAP"))
                        SetRecordIDX(xdWork, xiRecord, "MAI_filedati", aValue(2))
                        SetRecordIDX(xdWork, xiRecord, "DEF_errcode", "0")
                        SetRecordIDX(xdWork, xiRecord, "DEF_errdesc", "")

                        ' impostazione della modalit� di ordinamento.
                        ' in partenza si era preferito ordinare per numero fattura.
                        ' la richiesta di bonato � stata invece quella di ordinare per:
                        ' CAP - Via - NumeroCivico Pari o dispari - numero civico.
                        If dr("CAP") = "60123" Then
                            cSortValue = "0"
                        Else
                            cSortValue = "1"
                        End If
                        cSortValue = String.Concat(cSortValue, dr("Provincia"), dr("Comune"), dr("Via"), dr("Civico").ToString().PadLeft(10, "0"))
                        cSortValue = String.Concat(cSortValue, dr("Denominazione"))
                        SetRecordIDX(xdWork, xiRecord, "SORTFIELD", cSortValue)

                        '
                        ' La distinzione tra le due tipologie di stampa � fatta sul numero di colonne che ci vengono passate tramite file CSV.
                        '
                        If Not IsDBNull(dr("AbiCategoria")) Then
                            If dr("AbiCategoria") > "" Then
                                SetRecordIDX(xdWork, xiRecord, "MAI_precompilato", "SI")
                            Else
                                SetRecordIDX(xdWork, xiRecord, "MAI_precompilato", "NO")
                            End If
                        Else
                            SetRecordIDX(xdWork, xiRecord, "MAI_precompilato", "NO")
                        End If
                        SetRecordIDX(xdWork, xiRecord, "MAI_print_f24s", "SI")
                        xdWork.SelectSingleNode("FATTURE").AppendChild(xiRecord)
                    Next
                Catch ex As Exception

                End Try
            End If
        Next
        _MessaggiAvanzamento(5) = "Ordinamento dei record in corso..."
        'SortFileIndice(xdWork, 0, xdWork.SelectSingleNode("FATTURE").ChildNodes.Count - 1)
        xdWork.Save(String.Concat(_SessionePath, "\dati\indice_ns.xml"))
        FastSortFile(String.Concat(_SessionePath, "\dati\indice_ns.xml"), String.Concat(_SessionePath, "\dati\indice.xml"), String.Concat(_SessioneDatiPath, "\SortParameter.xslt"))

        System.IO.File.Delete(String.Concat(_SessionePath, "\dati\indice_ns.xml"))

        xdWork.Load(String.Concat(_SessionePath, "\dati\indice.xml"))
        ' Rimuove il campo utilizzato per l'ordinamento
        nRecord = 1
        For Each xnWork1 As Xml.XmlNode In xdWork.SelectNodes("FATTURE/FATTURA_ROW/SORTFIELD")
            xnWork2 = xnWork1.ParentNode
            xnWork2.RemoveChild(xnWork1)
            SetRecordIDX(xdWork, xnWork2, "MAI_codice", nRecord)
            nRecord += 1
        Next

        _MessaggiAvanzamento(5) = "Salvataggio dati in corso..."
        xdWork.Save(String.Concat(_SessionePath, "\dati\indice.xml"))
        _ResultAL.Add(New String() {"filename", "dati\indice.xml", "0", "0", "1"})

    End Sub

    Private Function CercaFileName(ByVal _DatiSessione As ArrayList, ByVal nTipo As Integer) As String
        Dim cTmp As String

        cTmp = ""
        For Each aValue As String() In _DatiSessione
            If aValue(0) = nTipo Then
                cTmp = aValue(1)
            End If
        Next
        Return cTmp

    End Function

    Sub ImportaFileInSessione(ByVal nTipoFile As Integer, ByVal cFileName As String)
        Dim cDstFile As String

        If nTipoFile = 1 Then
            _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
            cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

            System.IO.File.Copy(cFileName, cDstFile)
            _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
            _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
        End If
        '' Nel caso di tipo file 1 allora si tratta di un file xml, indipendentemente dall'estensione 
        '' da suddividere eventualmente in file di pi� piccole dimensioni.
        'If nTipoFile = 102 Then
        '    _MessaggiAvanzamento(5) = "Copia del file nella cartella di archiviazione..."
        '    cDstFile = String.Concat(_SessioneDatiPath, cFileName.Substring(cFileName.LastIndexOf("\") + 1))

        '    System.IO.File.Copy(cFileName, cDstFile)
        '    _MessaggiAvanzamento(3) = CInt(_MessaggiAvanzamento(3)) + 1
        '    _ResultAL.Add(New String() {"filename", cDstFile.Replace(_SessionePath, ""), _MessaggiAvanzamento(3), nTipoFile, 0})
        'End If

    End Sub

    Private Function GetRecordValue(ByVal cLine As String, ByVal aRecordDef As Integer()) As String()
        Dim aValue As String()
        Dim nIdx As Integer
        Dim i As Integer

        ReDim aValue(aRecordDef.Length - 1)
        i = 0
        nIdx = 0
        For Each nPos As Integer In aRecordDef
            aValue(i) = Trim(cLine.Substring(nIdx, aRecordDef(i)))
            nIdx += aRecordDef(i)
            i += 1
        Next
        Return aValue

    End Function

    Private Function String2Number(ByVal cText As String, ByVal nPrec As Integer) As Double
        Dim nRtn As Double

        If cText = "" Then
            nRtn = 0
        Else
            nRtn = cText
        End If
        Return nRtn

    End Function

    Private Function DateDefault(ByVal cText As String) As DateTime

        If cText = "" Then
            cText = "01/01/1900"
        End If
        Return CDate(cText)

    End Function

#End Region

#Region "Caricamento del PRINTDATASET"

    ' ************************************************************************** '
    ' Caricamento del DataSet da utilizzare per la stampa.                       '
    ' ************************************************************************** '
    Public Sub LoadPrintDataset(ByVal nRecordToLoad As Integer) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LoadFullDataset
        Dim dr_mai_qd As DS_QD_ANE_tasi.TBL_mainRow
        Dim nRecordToPrint As Integer

        ' ************************************************************************** '
        ' Creazione del Dataset e azzeramento del contatore dei record da stampare.  '
        ' ************************************************************************** '
        nRecordToPrint = 0
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                _PrintBollettaDataset = New DS_PF_ANE_tasi
            Case Is = ListaDataset.DAS_f24s
                _PrintF24Dataset = New DS_PF24_ANE_tasi
        End Select

        ' ************************************************************************** '
        ' Caricamento del DataSet da utilizzare per la stampa.                       '
        ' ************************************************************************** '
        _CodiceProgressivo = 0
        While _RecordCount < _QuickDataset.TBL_main.Rows.Count And nRecordToPrint < nRecordToLoad
            dr_mai_qd = _QuickDataset.TBL_main.Rows(_RecordCount)
            If dr_mai_qd.DEF_toprint Then
                Try
                    _ErroriLievi = ""
                    AddRecordToPrint(dr_mai_qd)
                    If _ErroriLievi > "" Then
                        dr_mai_qd.DEF_errcode = -2
                        dr_mai_qd.DEF_errdesc = String.Concat("Rilevati errori lievi: ", _ErroriLievi)
                    End If
                Catch ex As Exception
                    dr_mai_qd.DEF_errcode = -1
                    dr_mai_qd.DEF_errdesc = ex.Message
                End Try
                nRecordToPrint += 1
            End If
            _RecordCount += 1
        End While

        ' ************************************************************************** '
        ' Consolidamento del DataSet da utilizzare per la stampa.                    '
        ' ************************************************************************** '
        Select Case _CodiceModulo
            Case Is = ListaDataset.DAS_fattura
                _PrintBollettaDataset.AcceptChanges()
            Case Is = ListaDataset.DAS_f24s
                _PrintF24Dataset.AcceptChanges()
        End Select

    End Sub

    Private Sub AddRecordToPrint(ByVal dr_mai_qd As DS_QD_ANE_tasi.TBL_mainRow)
        ' Dim ds_src As DataSet
        Dim dr_fat As DataRow
        Dim aDR_fat As DataRow()

        Try
            ' ds_src = New DataSet
            ' ds_src.Tables.Add(CaricaCSVToDataset(PrintDataSet_DataFile(dr_mai_qd.MAI_filedati)))
            If _data_avvisiFileName <> PrintDataSet_DataFile(dr_mai_qd.MAI_filedati) Then
                _data_avvisiFileName = PrintDataSet_DataFile(dr_mai_qd.MAI_filedati)
                _data_avvisi = CaricaFileDatiToDS_excel(_data_avvisiFileName)
            End If

            aDR_fat = _data_avvisi.Tables(0).Select(String.Concat("CodiceACS = '", dr_mai_qd.MAI_num_fattura, "'"))
            If aDR_fat.Length > 0 Then
                dr_fat = aDR_fat(0)
                Select Case _CodiceModulo
                    Case Is = ListaDataset.DAS_fattura
                        GetRecordFatturaToPrint(dr_mai_qd.MAI_codice, dr_mai_qd.MAI_precompilato, _data_avvisi, dr_fat, GetFeedString(dr_mai_qd))
                    Case Is = ListaDataset.DAS_f24s
                        GetRecordF24ToPrint(dr_mai_qd.MAI_codice, dr_mai_qd.MAI_precompilato, _data_avvisi, dr_fat, GetFeedString(dr_mai_qd))
                End Select
            End If
            '_data_avvisi = Nothing
        Catch ex As Exception
            Dim cErrorString As String
            If ex.Message = "" Then
                cErrorString = "Errore caricamento dati, mentra si caricava il dato: SCONOSCIUTO"
            Else
                cErrorString = String.Concat("Errore caricamento dati, mentra si caricava il dato: ", ex.Message)
            End If
            Throw New Exception(cErrorString, ex)
        End Try

    End Sub

    Private Function GetFeedString(ByVal dr_mai_qd As DS_QD_ANE_tasi.TBL_mainRow) As String
        Dim lAllModuli As Boolean = True
        Dim cRtn As String

        cRtn = ""
        If _FeedString > "" Then
            For i As Integer = 1 To _FeedString.Split(";").Length - 1 Step 2
                If dr_mai_qd.Item(_FeedString.Split(";")(i)) Then
                    cRtn = _FeedString.Split(";")(i + 1)
                Else
                    lAllModuli = False
                End If
            Next
            If lAllModuli Then
                cRtn = _FeedString.Split(";")(0)
            End If
        End If
        Return cRtn

    End Function

#End Region

#Region "Caricamento del PRINTDATSET della fattura"

    Private Sub GetRecordFatturaToPrint(ByVal nCodice As Integer, ByVal lPrecompilato As Boolean, ByVal ds_src As DataSet, ByVal dr_fat_imp As DataRow, ByVal cFeedString As String)
        Dim dt_mai As DS_PF_ANE_tasi.TBL_mainDataTable
        Dim dr_mai As DS_PF_ANE_tasi.TBL_mainRow
        Dim dr_daf As DS_PF_ANE_tasi.TBL_dati_fornRow
        Dim dr_dfa As DS_PF_ANE_tasi.TBL_dati_fattRow
        Dim nPodFatturati As Integer
        Dim nIdPuntoForn As Integer
        Dim lOdl As Boolean
        Dim nRow As Integer

        ' ************************************************************************** '
        ' Creazione del record guida della stampa della fattura.                     '
        ' ************************************************************************** '
        dr_mai = _PrintBollettaDataset.TBL_main.NewRow
        _CodiceProgressivo += 1
        dr_mai.MAI_codice = _CodiceProgressivo
        dr_mai.MAI_linkQD = nCodice
        dr_mai.DEF_alimimb = cFeedString

        ' ************************************************************************** '
        ' Recuperiamo il numero dei pod per i quli si stamper� la fattura. Il campo  '
        ' di default DEF_raccolta indica se il record attuale fa parte di una raccol '
        ' ta. I valori che pu� assumere sono:                                        '
        ' ---- In caso di raccolta da pi� di un record:                              '
        '      - 1 primo record di una raccolta;                                     '
        '      - 2 record successivi della raccolta                                  '
        '      - 3 ultimo record della raccolta.                                     '
        ' ---- In caso di raccolta da pi� di un record:                              '
        '      - 9 raccolta costituita da un solo record                             '
        ' ************************************************************************** '
        dr_mai.DEF_raccolta = 9

        ' ************************************************************************** '
        ' Dati dell'intestatario della fattura.                                      ' 
        ' ************************************************************************** '
        dr_mai.MAI_progressivo = nCodice
        dr_mai.MAI_cod_cli = dr_fat_imp("CodiceACS")
        dr_mai.MAI_nome = dr_fat_imp("Denominazione").ToString().Replace("/", " ")
        If dr_fat_imp("CodiceFiscale").ToString.Length = 16 Then
            dr_mai.MAI_cod_fis = dr_fat_imp("CodiceFiscale")
        Else
            dr_mai.MAI_par_iva = dr_fat_imp("CodiceFiscale")
        End If

        ' ************************************************************************** '
        ' Indirizzo del recapito della fattura.                                      '
        ' ************************************************************************** '
        dr_mai.MAI_rec_sito_1 = String.Concat(dr_fat_imp("Via"), " ", dr_fat_imp("Civico"))
        If dr_fat_imp("EsponenteCivico") > "" Then
            dr_mai.MAI_rec_sito_1 = String.Concat(dr_mai.MAI_rec_sito_1, " ", dr_fat_imp("EsponenteCivico"))
        End If
        dr_mai.MAI_rec_sito_6 = dr_fat_imp("Comune")
        dr_mai.MAI_rec_sito_7 = dr_fat_imp("CAP")
        dr_mai.MAI_rec_sito_8 = dr_fat_imp("Provincia")
        dr_mai.MAI_precompilato = Not IsDBNull(dr_fat_imp("AbiCategoria"))  ' > ""
        _PrintBollettaDataset.TBL_main.Rows.Add(dr_mai)

        If lPrecompilato Then
            dr_dfa = _PrintBollettaDataset.TBL_dati_fatt.NewRow
            dr_dfa.DFA_cod_mai = dr_mai.MAI_codice
            dr_dfa.DFA_totale = dr_fat_imp("F24_3_ImportoDebito")
            dr_dfa.DFA_detrazioni = dr_fat_imp("f24_3_importodetrazione")   'dr_fat_imp("AbiDetrazione")
            _PrintBollettaDataset.TBL_dati_fatt.Rows.Add(dr_dfa)

            dr_daf = _PrintBollettaDataset.TBL_dati_forn.NewRow
            dr_daf.DAF_cod_mai = dr_mai.MAI_codice
            dr_daf.DAF_abi_tipo = "Abitazione"    ' dr_fat_imp("AbiTipo")
            dr_daf.DAF_abi_categoria = dr_fat_imp("AbiCategoria")
            dr_daf.DAF_abi_classe = dr_fat_imp("AbiClasse")
            dr_daf.DAF_abi_foglio = dr_fat_imp("AbiFoglio")
            dr_daf.DAF_abi_numero = dr_fat_imp("AbiNumero")
            dr_daf.DAF_abi_subalterno = dr_fat_imp("AbiSub")
            dr_daf.DAF_abi_possesso = dr_fat_imp("abipos")
            dr_daf.DAF_abi_mesi = dr_fat_imp("AbiMesi")
            dr_daf.DAF_abi_rendita = dr_fat_imp("AbiRendita")
            dr_daf.DAF_abi_valore = dr_fat_imp("AbiValore")
            dr_daf.DAF_pertinenza = Not IsDBNull(dr_fat_imp("PertTipo"))    ' > ""
            If dr_daf.DAF_pertinenza Then
                dr_daf.DAF_per_tipo = dr_fat_imp("PertTipo")
                dr_daf.DAF_per_categoria = dr_fat_imp("PertCategoria")
                dr_daf.DAF_per_classe = dr_fat_imp("PertClasse")
                dr_daf.DAF_per_foglio = dr_fat_imp("PertFoglio")
                dr_daf.DAF_per_numero = dr_fat_imp("PertNumero")
                dr_daf.DAF_per_subalterno = dr_fat_imp("PertSub")
                dr_daf.DAF_per_possesso = dr_fat_imp("PertPossesso")
                dr_daf.DAF_per_mesi = dr_fat_imp("PertMesi")
                dr_daf.DAF_per_rendita = dr_fat_imp("PertRendita")
                dr_daf.DAF_per_valore = dr_fat_imp("PertValore")
            End If
            dr_daf.DAF_pertinenza2 = Not IsDBNull(dr_fat_imp("Pert2Tipo"))    ' > ""
            If dr_daf.DAF_pertinenza2 Then
                dr_daf.DAF_per_tipo2 = dr_fat_imp("Pert2Tipo")
                dr_daf.DAF_per_categoria2 = dr_fat_imp("Pert2Categoria")
                dr_daf.DAF_per_classe2 = dr_fat_imp("Pert2Classe")
                dr_daf.DAF_per_foglio2 = dr_fat_imp("Pert2Foglio")
                dr_daf.DAF_per_numero2 = dr_fat_imp("Pert2Numero")
                dr_daf.DAF_per_subalterno2 = dr_fat_imp("Pert2Sub")
                dr_daf.DAF_per_possesso2 = dr_fat_imp("Pert2Possesso")
                dr_daf.DAF_per_mesi2 = dr_fat_imp("Pert2Mesi")
                dr_daf.DAF_per_rendita2 = dr_fat_imp("Pert2Rendita")
                dr_daf.DAF_per_valore2 = dr_fat_imp("Pert2Valore")
            End If
            _PrintBollettaDataset.TBL_dati_forn.Rows.Add(dr_daf)
        End If
        'nRow = 1
        '_dr_dfa_pf = Nothing
        'For i As Integer = 0 To nSezDocumenti - 1

        '    Carica_DatiFattura(ds_src, dr_fat, i)
        '    Carica_DatiFornitura(ds_src, dr_fat, i, nPodFatturati)

        '    Carica_RigheFatturazione(ds_src, dr_fat, i, 1)
        'Next

    End Sub

    Private Sub CheckFieldValue(ByVal cValue As String, ByVal cMsgError As String)

        If cValue = "" And cMsgError > "" Then
            AddErroreLieve(cMsgError)
        End If

    End Sub

    Private Function SetDataScadenza(ByVal cFlagTipoPag As String, ByVal dDataForzata As String) As String
        Dim cCondizioneForzaScadenza As String

        cCondizioneForzaScadenza = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "17")
        If (cCondizioneForzaScadenza.Length > 0) And (cCondizioneForzaScadenza.ToUpper.Contains(cFlagTipoPag.ToUpper)) Then
            dDataForzata = GetValueDatiFissi(Costanti.DatiFissi_Fattura, "16")
        End If
        Return dDataForzata

    End Function

#End Region

#Region "Caricamento del PRINTDATSET del modulo F24."

    Private Sub GetRecordF24ToPrint(ByVal nCodice As Integer, ByVal lPrecompilato As Boolean, ByVal ds_src As DataSet, ByVal dr_fat_imp As DataRow, ByVal cFeedString As String)
        Dim dr_fxr As DS_PF24_ANE_tasi.TBL_fat_x_ratRow
        Dim dr_fat As DS_PF24_ANE_tasi.TBL_fattureRow
        Dim dr_rat As DS_PF24_ANE_tasi.TBL_rateRow
        Dim nModuliF24 As Integer
        Dim i As Integer

        '
        ' Se il modulo F24 sar� spedito precompilato allora ne dovranno essere stampati 3.
        ' altrimenti 1 solo foglio.
        '
        If lPrecompilato Then
            nModuliF24 = 3
        Else
            nModuliF24 = 1
        End If

        For i = 1 To nModuliF24
            dr_fat = _PrintF24Dataset.TBL_fatture.NewRow
            With dr_fat
                .FAT_codice = _PrintF24Dataset.TBL_fatture.Rows.Count + 1
                .FAT_progressivo = nCodice
                .FAT_cognome = dr_fat_imp("Cognome")
                .FAT_nome = dr_fat_imp("Nome")
                .FAT_codice_fiscale = dr_fat_imp("CodiceFiscale")
                .FAT_data_nascita = dr_fat_imp("DataNascita")
                .FAT_data_nascita = String.Concat(.FAT_data_nascita.Substring(6, 2), .FAT_data_nascita.Substring(4, 2), .FAT_data_nascita.Substring(0, 4))
                .FAT_sesso = dr_fat_imp("Sesso")
                .FAT_comune_nasc = dr_fat_imp("ComuneStatoNascita")
                If dr_fat_imp("ProvinciaNascita").ToString.Trim() = "" Then
                    .FAT_prov_nasc = "EE"
                Else
                    .FAT_prov_nasc = dr_fat_imp("ProvinciaNascita")
                End If
                If i = 1 And lPrecompilato Then
                    .FAT_identificativoOperazione = dr_fat_imp("F24_1_IdentificativoOperazione")
                ElseIf i = 1 And Not lPrecompilato Then
                    ' .FAT_identificativoOperazione = dr_fat_imp("IdentificativoOperazione")
                ElseIf i = 2 Then
                    .FAT_identificativoOperazione = dr_fat_imp("F24_2_IdentificativoOperazione")
                ElseIf i = 3 Then
                    .FAT_identificativoOperazione = dr_fat_imp("F24_3_IdentificativoOperazione")
                End If

            End With
            _PrintF24Dataset.TBL_fatture.Rows.Add(dr_fat)

            dr_rat = _PrintF24Dataset.TBL_rate.NewRow
            With dr_rat
                .RAT_codice = _PrintF24Dataset.TBL_rate.Rows.Count + 1
                .RAT_sezione = ""
                .RAT_cod_tributo = ""
                .RAT_codice_ente = ""
                If lPrecompilato Then
                    If i = 1 Then
                        ' .RAT_identificativoOperazione = dr_fat_imp("F24_1_IdentificativoOperazione")
                        .RAT_acconto = True
                        .RAT_saldo = False
                        .RAT_rateizzazione = "0102"
                        .RAT_num_immobili = dr_fat_imp("F24_1_NumeroImmobili")
                        .RAT_importo = dr_fat_imp("F24_1_ImportoDebito")
                        .RAT_detrazioni = 0
                        '"F24_1_IdentificativoOperazione";"F24_1_ImportoDebito";"F24_1_NumeroImmobili";"f24_1_ImportoDetrazione";"F24_2_IdentificativoOperazione";"F24_2_ImportoDebito";"F24_2_NumeroImmobili";"f24_2_ImportoDetrazione";"F24_3_IdentificativoOperazione";"F24_3_ImportoDebito";"F24_3_NumeroImmobili";"f24_3_ImportoDetrazione"
                    ElseIf i = 2 Then
                        '  .RAT_identificativoOperazione = dr_fat_imp("F24_2_IdentificativoOperazione")
                        .RAT_acconto = False
                        .RAT_saldo = True
                        .RAT_rateizzazione = "0202"
                        .RAT_num_immobili = dr_fat_imp("F24_2_NumeroImmobili")
                        .RAT_importo = dr_fat_imp("F24_2_ImportoDebito")
                        .RAT_detrazioni = 0
                    ElseIf i = 3 Then
                        ' .RAT_identificativoOperazione = dr_fat_imp("F24_1_IdentificativoOperazione")
                        .RAT_acconto = True
                        .RAT_saldo = True
                        .RAT_rateizzazione = "0101"
                        .RAT_num_immobili = dr_fat_imp("F24_3_NumeroImmobili")
                        .RAT_importo = dr_fat_imp("F24_3_ImportoDebito")
                        .RAT_detrazioni = dr_fat_imp("f24_3_ImportoDetrazione")
                    End If
                    dr_fat.FAT_importo_totale = .RAT_importo
                    '  .RAT_anno_riferimento = "2014"
                End If
            End With
            _PrintF24Dataset.TBL_rate.Rows.Add(dr_rat)
            ' Carico una record per ogni bollettino che debbo stampare nella tabella che aggrega le fatture e i bollettini.
            dr_fxr = _PrintF24Dataset.TBL_fat_x_rat.NewRow
            dr_fxr.FXR_codice = _PrintF24Dataset.TBL_fat_x_rat.Rows.Count + 1
            dr_fxr.FXR_ultimo = 0
            If i = nModuliF24 Then
                dr_fxr.FXR_ultimo = 1
            End If
            dr_fxr.DEF_alimimb = cFeedString
            If nModuliF24 = 1 Then
                dr_fxr.DEF_raccolta = 9
            ElseIf i = 1 Then
                dr_fxr.DEF_raccolta = 1
            ElseIf i = nModuliF24 Then
                dr_fxr.DEF_raccolta = 3
            Else
                dr_fxr.DEF_raccolta = 2
            End If

            dr_fxr.FXR_linkQD = nCodice
            dr_fxr.FXR_cod_fat = dr_fat.FAT_codice
            dr_fxr.FXR_cod_rat = dr_rat.RAT_codice
            _PrintF24Dataset.TBL_fat_x_rat.Rows.Add(dr_fxr)
        Next

    End Sub

#End Region

    Private Function GetValueFromXML(ByVal xmlItem As Xml.XmlNode, ByVal xmlPath As String, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cDefault
            If Not xmlItem.SelectSingleNode(xmlPath) Is Nothing Then
                cValue = xmlItem.SelectSingleNode(xmlPath).InnerText
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori del file XML.", vbLf, "percorso da cui si tenta il recupero:", vbLf, xmlPath), ex)
        End Try
        Return cValue

    End Function

    Private Function GetValueFromText(ByVal cText As String, ByVal nStart As Integer, ByVal nLength As Integer, Optional ByVal cDefault As String = "", Optional ByVal cMsgError As String = "") As String
        Dim cValue As String

        cValue = ""
        Try
            cValue = cText.Substring(nStart, nLength)
            If cValue.Trim = "" And cDefault > "" Then
                cValue = cDefault
            End If
            If cValue = "" And cMsgError > "" Then
                AddErroreLieve(cMsgError)
            End If
        Catch ex As Exception
            Throw New Exception(String.Concat("Errore nella procedura di recupero dei valori da un testo.", vbLf, "Dati per il recupero: ", vbLf, "- inizio ", nStart, vbLf, "- fine ", nLength), ex)
        End Try
        Return cValue

    End Function

    Private Sub AddErroreLieve(ByVal cMsg As String)

        If _ErroriLievi = "" Then
            _ErroriLievi = cMsg
        Else
            _ErroriLievi = String.Concat(_ErroriLievi, "; ", cMsg)
        End If

    End Sub

    Private Sub ImpostaMessaggi(ByVal aValue As String())
        Dim i As Integer

        For i = 0 To 5
            If aValue(i) > "" Then
                _MessaggiAvanzamento(i) = aValue(i)
            End If
        Next

    End Sub

    Private Function GetTrasformazioni(ByVal cTable As String, ByVal cCodice As String, ByVal nTipo As Integer) As String
        Dim xdTrasf As Xml.XmlDocument
        Dim xiWork As Xml.XmlNode
        Dim cRtn As String

        Try
            xdTrasf = New Xml.XmlDocument
            xdTrasf.Load(_SessioneTrasformazioniFileName)
            xiWork = xdTrasf.SelectSingleNode(String.Concat("translation/", cTable, "/item[codice = '", cCodice, "']"))
            cRtn = ""
            If Not xiWork Is Nothing Then
                If nTipo = 1 Then
                    cRtn = xiWork.SelectSingleNode("descr_breve").InnerText
                End If
                If nTipo = 2 Then
                    cRtn = xiWork.SelectSingleNode("descrizione").InnerText
                End If
            End If
            xdTrasf = Nothing
        Catch ex As Exception
            cRtn = ""
        End Try
        Return cRtn

    End Function

    Private Function GetDataSendByMail() As Xml.XmlNode
        'Dim xnWork As Xml.XmlNode
        'Dim xdWork As Xml.XmlDocument

        'xdWork = New Xml.XmlDocument
        'xnWork = xdWork.CreateNode(Xml.XmlNodeType.Element, "SendByMail", "")
        'Select Case _CodiceModulo
        '    Case Is = ListaModuli.MOD_fattura
        '        With CType(_PrintFatturaDataset.TBL_main.Rows(0), DS_PF_UE_ele.TBL_mainRow)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_codice", .MAI_linkQD)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_cod_utente", .MAI_cod_cli)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_nominativo", .MAI_nome)
        '            CreateXmlNode(xdWork, xnWork, "SND_EML_mail_address", "lproietti@gmail.com")
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_codice", .MAI_linkQD.ToString))
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_cod_utente", .MAI_cod_cli.ToString))
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_nominativo", .MAI_nome.ToString))
        '            'xnWork.AppendChild(xdWork.CreateNode(Xml.XmlNodeType.Element, "SND_EML_mail_address", "lproietti@gmail.com"))
        '        End With
        'End Select
        'Return xnWork

    End Function

    Private Sub CreateXmlNode(ByVal xdDoc As Xml.XmlDocument, ByVal xnNode As Xml.XmlNode, ByVal cField As String, ByVal cValue As String)
        Dim xiTmp As Xml.XmlNode

        xiTmp = xdDoc.CreateNode(Xml.XmlNodeType.Element, cField, "")
        xiTmp.InnerText = cValue
        xnNode.AppendChild(xiTmp)

    End Sub

#Region "Elenco procedure Generiche"

    Private Function SetExceptionItem(ByVal cProcError As String, ByVal ex As Exception) As Exception
        Dim ex_rtn As Exception

        If ex.InnerException Is Nothing Then
            ex_rtn = New Exception(String.Concat("Errore (", cProcError, "): ", ex.Message), ex)
        Else
            ex_rtn = ex
        End If
        Return ex_rtn

    End Function

#End Region

    Private Function GetFieldValue(ByVal aField As Array, ByVal cField As String, ByVal aValue As String(), Optional ByVal cDefault As String = "") As String
        Dim nIdx As Integer
        Dim cRtn As String

        nIdx = Array.IndexOf(aField, cField)
        If (nIdx > -1) And nIdx < aValue.Length Then
            cRtn = aValue(nIdx)
        Else
            cRtn = cDefault
        End If
        Return cRtn

    End Function

#Region "Caricamento dei dati per la generazione della sessione"

    Public Sub LoadQuickDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LoadQuickDataset
        Dim dr_mai_qd As DS_QD_ANE_tasi.TBL_mainRow

        _QuickDataset = New DS_QD_ANE_tasi
        _xmlDati_qd = New Xml.XmlDocument
        _xmlDati_qd.Load(QuickDataSet_IndexFile)
        ImpostaMessaggi(New String() {"", "", "", "0", _xmlDati_qd.SelectNodes(Costanti.itm_qd_bolletta).Count, "Caricamento dei dati...."})

        For Each xmlItem As Xml.XmlNode In _xmlDati_qd.SelectNodes(Costanti.itm_qd_bolletta)
            dr_mai_qd = _QuickDataset.TBL_main.NewRow
            Try
                dr_mai_qd.MAI_codice = GetValueFromXML(xmlItem, "MAI_codice")
                dr_mai_qd.DEF_toprint = False
                dr_mai_qd.DEF_errcode = GetValueFromXML(xmlItem, "DEF_errcode")
                dr_mai_qd.DEF_errdesc = GetValueFromXML(xmlItem, "DEF_errdesc")
                dr_mai_qd.DEF_selectable = (dr_mai_qd.DEF_errcode = 0)
                dr_mai_qd.DEF_sendbymail = GetValueFromXML(xmlItem, "DEF_sendbymail").ToLower = "si"
                dr_mai_qd.DEF_onlyarchott = GetValueFromXML(xmlItem, "DEF_onlyarchott").ToLower = "si"
                dr_mai_qd.MAI_num_utente = GetValueFromXML(xmlItem, "MAI_num_utente")
                dr_mai_qd.MAI_num_fattura = GetValueFromXML(xmlItem, "MAI_num_fattura")
                dr_mai_qd.MAI_nominativo = GetValueFromXML(xmlItem, "MAI_nominativo")
                dr_mai_qd.MAI_indirizzo_recapito = GetValueFromXML(xmlItem, "MAI_indirizzo_recapito")
                dr_mai_qd.MAI_citta_recapito = GetValueFromXML(xmlItem, "MAI_citta_recapito")
                dr_mai_qd.MAI_cap_recapito = GetValueFromXML(xmlItem, "MAI_cap_recapito")
                dr_mai_qd.MAI_provincia_recapito = GetValueFromXML(xmlItem, "MAI_provincia_recapito")
                dr_mai_qd.MAI_print_f24s = GetValueFromXML(xmlItem, "MAI_print_f24s").ToLower = "si"
                'dr_mai_qd.MAI_nazione_recapito = GetValueFromXML(xmlItem, "MAI_nazione_recapito")
                'dr_mai_qd.MAI_print_bol = GetValueFromXML(xmlItem, "MAI_print_bol").ToLower = "si"
                'dr_mai_qd.MAI_print_all = GetValueFromXML(xmlItem, "MAI_print_all").ToLower = "si"
                dr_mai_qd.MAI_filedati = GetValueFromXML(xmlItem, "MAI_filedati")
                dr_mai_qd.MAI_precompilato = GetValueFromXML(xmlItem, "MAI_precompilato").ToLower = "si"
                'dr_mai_qd.MAI_bc_data_certa = GetValueFromXML(xmlItem, "MAI_bc_data_certa")
                'dr_mai_qd.MAI_cf = GetValueFromXML(xmlItem, "MAI_cf")
                'dr_mai_qd.MAI_totale = GetValueFromXML(xmlItem, "MAI_totale")
            Catch ex As Exception
                dr_mai_qd.DEF_errcode = -1
                dr_mai_qd.DEF_errdesc = ex.Message
            End Try
            ImpostaMessaggi(New String() {"", "", "", CInt(_MessaggiAvanzamento(3)) + 1, "", ""})
            _QuickDataset.TBL_main.Rows.Add(dr_mai_qd)
        Next
        ' LoadFileMdb()

    End Sub

#End Region

    Public Function CheckCondPSA(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckCondPSA
        Dim cValue As String
        Dim lCond As Boolean

        cValue = ""
        lCond = False
        Return lCond

    End Function

    Public Function CheckCondAPI(ByVal nCond2Check As Integer, ByVal cValue2Check As String) As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CheckCondAPI
        Dim cValue As String
        Dim lCond As Boolean

        cValue = ""
        lCond = False
        Return lCond

    End Function

    Public Sub ExecActionPSA(ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ExecActionPSA

    End Sub

    Public Sub ExecActionAPI(ByVal cTipoExecAPI As String, ByVal nAct2Exec As Integer, ByVal cValue2Exec As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ExecActionAPI

    End Sub

    ReadOnly Property CapacitaInvioMail() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.CapacitaInvioMail

        Get
            Return True
        End Get

    End Property

    ReadOnly Property DatiAzioniPostStampa() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DatiAzioniPostStampa

        Get
            Dim tmpArrList As ArrayList

            tmpArrList = New ArrayList
            tmpArrList.Add(New String() {"C", "1001", "Consorzio di acquisto"})
            tmpArrList.Add(New String() {"A", "1001", "Estrai file porzione file xml"})

            Return tmpArrList
        End Get

    End Property

    ReadOnly Property DatiAzioniPreInvio() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DatiAzioniPreInvio

        Get
            Dim tmpArrList As ArrayList

            tmpArrList = New ArrayList
            tmpArrList.Add(New String() {"C", "1001", "Consorzio di acquisto"})
            tmpArrList.Add(New String() {"A", "1001", "Allega Porzione di file xml alla mail"})

            Return tmpArrList
        End Get

    End Property

    ReadOnly Property StampaInProprio() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.StampaInProprio

        Get
            Return True
        End Get

    End Property

    Public ReadOnly Property FullDataset() As DataSet Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataset

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return _PrintBollettaDataset
                Case Is = ListaDataset.DAS_f24s
                    Return _PrintF24Dataset
                Case Else
                    Return Nothing
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataSet_MainTableName() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataSet_MainTableName

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "TBL_main"
                Case Is = ListaDataset.DAS_f24s
                    Return "TBL_fat_x_rat"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    Public ReadOnly Property FullDataset_MainTableKey() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.FullDataset_MainTableKey

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "MAI_codice"  '                    Return _PrintFatturaDataset
                Case Is = ListaDataset.DAS_f24s
                    Return "FXR_codice"  '                    Return _PrintBollettinoDataset
                Case Is = ListaDataset.DAS_send_email
                    Return "MAI_codice"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    ReadOnly Property LinkField_QuickDS_FullDS() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.LinkField_QuickDS_FullDS

        Get
            Select Case _CodiceModulo
                Case Is = ListaDataset.DAS_fattura
                    Return "MAI_linkQD"
                Case Is = ListaDataset.DAS_f24s
                    Return "FXR_linkQD"
                Case Else
                    Return ""
            End Select
        End Get

    End Property

    ReadOnly Property NuoviToponimi() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.NuoviToponimi

        Get
            Return _NuoviToponimi
        End Get

    End Property

    Public Sub PostPostAnteprima(ByVal aLista As ArrayList) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.PostAnteprima
        'Dim dr_elp As DS_PF_UE_ele.TBL_elenco_podRow
        'Dim nPagine As Integer
        'Dim nElencoPod As Integer
        'Dim nStatusRaccolta As Integer
        'Dim i As Integer

        'If Not _PrintFatturaDataset Is Nothing Then
        '    nElencoPod = -1
        '    For i = 0 To aLista.Count - 1
        '        nStatusRaccolta = CType(_PrintFatturaDataset.TBL_main.Rows(i), DS_PF_UE_ele.TBL_mainRow).DEF_raccolta
        '        Select Case nStatusRaccolta
        '            Case Is = 1
        '                nPagine = aLista(i)
        '            Case Is = 2, 3
        '                nElencoPod += 1
        '                dr_elp = _PrintFatturaDataset.TBL_elenco_pod.Rows(nElencoPod)
        '                dr_elp.ELP_page_num = nPagine + 1
        '                dr_elp.AcceptChanges()
        '                nPagine += aLista(i)
        '        End Select
        '    Next
        'End If

    End Sub

    Sub SetPrintable(ByVal dr As DataRow, ByVal _SelectFromPlugInType As String, ByVal cParamSelez As String) Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SetPrintable
        'Dim dr_mai_qd As DS_QD_UE_ele.TBL_mainRow

        'dr_mai_qd = CType(dr, DS_QD_UE_ele.TBL_mainRow)
        'Select Case _SelectFromPlugInType
        '    Case Is = cns_tsm_sel_con_bollettino.ToLower
        '        dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_bol
        '    Case Is = cns_tsm_sel_con_allegato.ToLower
        '        dr_mai_qd.DEF_toprint = dr_mai_qd.MAI_print_all
        'End Select

    End Sub


    Public Sub UpdateQuickDataset() Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.UpdateQuickDatasetFile
        'Dim dr_prd As DS_PF_UE_ele.TBL_mainRow
        'Dim xnTmp As Xml.XmlNode
        'Dim nLinkCode As Integer

        'If _CodiceModulo = ListaDataset.DAS_fattura Then
        '    _xmlDati_qd = New Xml.XmlDocument
        '    _xmlDati_qd.Load(QuickDataSet_IndexFile)
        '    For Each dr_prd In _PrintFatturaDataset.TBL_main
        '        nLinkCode = dr_prd(LinkField_QuickDS_FullDS)
        '        xnTmp = _xmlDati_qd.SelectSingleNode(String.Concat(Costanti.itm_qd_Fattura, "/MAI_codice[.=", _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).MAI_codice, "]")).ParentNode
        '        xnTmp.SelectSingleNode("DEF_errcode").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errcode
        '        xnTmp.SelectSingleNode("DEF_errdesc").InnerText = _QuickDataset.TBL_main.FindByMAI_codice(nLinkCode).DEF_errdesc
        '    Next
        '    _xmlDati_qd.Save(QuickDataSet_IndexFile)
        'End If

    End Sub

    WriteOnly Property DBGiriConnectionString() As String Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.DBGiriConnectionString

        Set(ByVal value As String)
            _DbGiriConnString = value
        End Set

    End Property

    WriteOnly Property ListaGiri() As ArrayList Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.ListaGiri

        Set(ByVal value As ArrayList)
            _ListaGiri = value
        End Set

    End Property

    WriteOnly Property SessioneNuova() As Boolean Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.SessioneNuova

        Set(ByVal value As Boolean)
            _SessioneNuova = value
        End Set

    End Property

    WriteOnly Property TipoCaricamento() As Integer Implements PLUGIN_interfaceV1_5_0.IPLG_dati_lib.TipoCaricamento

        Set(ByVal value As Integer)
            _TipoCaricamento = value
        End Set

    End Property

#Region "Procedure per il caricamento dei dati nel DB"

    Private Sub ManageRecordG00(ByVal cLine As String)
        Dim aValue As String()

        ' aValue = ExtractRecord(cLine, New Integer() {3, 9, 12, 3, 5, 8, 8, 150})

    End Sub

    Private Sub ManageRecordG10(ByVal cLine As String)
        Dim aValue As String()

        'aValue = ExtractRecord(cLine, New Integer() {3, 9, 2, 8})

    End Sub

    Private Function ExtractRecord(ByRef dr As DataRow, ByVal cLine As String, ByVal aFields As ArrayList) As DataRow
        Dim aField As String()
        Dim nPos As Integer
        Dim cTmp As String
        Dim i As Integer
        Dim nTmp As Decimal

        ' i = 0
        nPos = 1
        For Each aField In aFields
            cTmp = cLine.Substring(nPos - 1, aField(2)).Trim
            If (aField(0) > "") And ((aField(1) = "A") Or (cTmp.Trim > "" And aField(1) <> "A")) Then
                Select Case aField(1)
                    Case Is = "GGMMAAAA"
                        dr(aField(0)) = CDate(String.Concat(cTmp.Substring(0, 2), "/", cTmp.Substring(2, 2), "/", cTmp.Substring(4, 4)))
                    Case Is = "A"
                        dr(aField(0)) = cTmp
                    Case Is = "EC"
                        dr(aField(0)) = CDec(cTmp) / 100
                    Case Is = "I"
                        dr(aField(0)) = cTmp
                    Case Is = "ECS"
                        cTmp = cTmp.Trim
                        nTmp = cTmp.Trim.Substring(0, cTmp.Length - 1)
                        nTmp = nTmp / 100

                        dr(aField(0)) = nTmp
                    Case Is = "FC6"
                        cTmp = cTmp.Trim
                        i = aField(1).Substring(2, 1)
                        dr(aField(0)) = cTmp.Insert(cTmp.Length - i, ",")
                    Case Is = ""
                    Case Else
                        Throw New Exception("Formato campo non ricomosciuto")
                End Select
            End If
            nPos += aField(2)
            ' i += 1
        Next
        Return dr

    End Function

#End Region

#Region "procedura di ordinamento, da sistemare in apposita libreria"

    Private Sub SortFileIndice(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer)
        Dim nPivot As Integer
        Dim k As Integer

        If nFst < nEnd Then
            nPivot = (nFst + nEnd) / 2
            k = dividi(xiWork, nFst, nEnd, nPivot)
            SortFileIndice(xiWork, nFst, k - 1)
            SortFileIndice(xiWork, k + 1, nEnd)
        End If

    End Sub

    Function dividi(ByVal xiWork As Xml.XmlDocument, ByVal nFst As Integer, ByVal nEnd As Integer, ByVal nPivot As Integer) As Integer
        Dim i As Integer = nFst
        Dim j As Integer = nEnd
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim nPivotPlace As Integer

        If nPivot < nEnd Then
            ' sposto il valore del pivot alla fine dell file xml
            swapItem(xiWork, nPivot, nEnd)
        End If

        xiWork_1 = xiWork.SelectSingleNode("FATTURE").ChildNodes(nEnd).SelectSingleNode("SORTFIELD")
        nPivotPlace = nFst
        For i = nFst To nEnd - 1
            xiWork_2 = xiWork.SelectSingleNode("FATTURE").ChildNodes(i).SelectSingleNode("SORTFIELD")
            If (xiWork_2.InnerText <= xiWork_1.InnerText) Then
                If i <> nPivotPlace Then
                    swapItem(xiWork, nPivotPlace, i)
                End If
                nPivotPlace += 1
            End If
        Next
        If nPivotPlace < nEnd Then
            swapItem(xiWork, nPivotPlace, nEnd)
        End If
        Return nPivotPlace

    End Function

    Private Sub swapItem(ByVal xiWork As Xml.XmlDocument, ByVal nIdx1 As Integer, ByVal nIdx2 As Integer)
        Dim xiWork_1 As Xml.XmlNode
        Dim xiWork_2 As Xml.XmlNode
        Dim xiWork_3 As Xml.XmlNode

        xiWork_1 = xiWork.SelectSingleNode("FATTURE").ChildNodes(nIdx1)
        xiWork_2 = xiWork.SelectSingleNode("FATTURE").ChildNodes(nIdx2)
        xiWork.SelectSingleNode("FATTURE").RemoveChild(xiWork_1)
        xiWork.SelectSingleNode("FATTURE").RemoveChild(xiWork_2)
        xiWork_3 = xiWork.SelectSingleNode("FATTURE").ChildNodes(nIdx1 - 1)
        xiWork.SelectSingleNode("FATTURE").InsertAfter(xiWork_2, xiWork_3)
        xiWork_3 = xiWork.SelectSingleNode("FATTURE").ChildNodes(nIdx2 - 1)
        xiWork.SelectSingleNode("FATTURE").InsertAfter(xiWork_1, xiWork_3)

    End Sub

#End Region

    'Private Sub DatiSolleciti_click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim xmlDF As New Xml.XmlDocument
    '    Dim frm As New FRM_estrai_exl

    '    Try
    '        frm = New FRM_estrai_exl
    '        frm.StartPosition = FormStartPosition.CenterParent
    '        If frm.ShowDialog = DialogResult.OK Then
    '            EsportaDati(frm.TXB_filename.Text, frm.TXB_num_lotto.Text)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Errore di lettura del file dei dati Fissi. Impossibile recuperare il testo dell'autolettura", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Sub

    Private Sub EsportaDati(ByVal cFileName As String, ByVal cLotto As String)
        'Dim dtExport As DS_export.Export_SollecitiDataTable
        'Dim dr_exp As DS_export.Export_SollecitiRow
        'Dim dr_mai_qd As DS_QD_ANE_tasi.TBL_mainRow
        'Dim sw As System.IO.StreamWriter
        'Dim cValue As String
        'Dim cRow As String
        'Dim i As Integer

        'dtExport = New DS_export.Export_SollecitiDataTable
        'For Each dr_mai_qd In _QuickDataset.TBL_main.Rows
        '    If dr_mai_qd("DEF_errcode") = 0 Then
        '        dr_exp = dtExport.NewRow

        '        dr_exp("NLotto") = cLotto
        '        dr_exp("nrbusta") = dr_mai_qd.MAI_codice
        '        'dr_exp("CodCartella_Carico") = dr_mai_qd.MAI_bc_data_certa
        '        dr_exp("Titolo") = ""
        '        dr_exp("Nominativo") = dr_mai_qd.MAI_nominativo
        '        dr_exp("indirizzo") = dr_mai_qd.MAI_indirizzo_recapito
        '        dr_exp("cap") = dr_mai_qd.MAI_cap_recapito
        '        dr_exp("Localita") = dr_mai_qd.MAI_citta_recapito
        '        dr_exp("gstampa") = ""
        '        dr_exp("tspedizione") = ""
        '        'dr_exp("cf") = dr_mai_qd.MAI_cf
        '        ' dr_exp("importo") = dr_mai_qd.MAI_totale
        '        dr_exp("tipografia") = ""
        '        dtExport.Rows.Add(dr_exp)
        '    End If
        'Next

        '' Apri lo stream di output
        'sw = New System.IO.StreamWriter(cFileName)
        'cRow = ""

        'For i = 0 To dtExport.Columns.Count - 1
        '    cValue = dtExport.Columns(i).ColumnName
        '    If cRow = "" Then
        '        cRow = cValue
        '    Else
        '        cRow = String.Concat(cRow, ";", cValue)
        '    End If
        'Next

        'sw.WriteLine(cRow)
        'For Each dr_exp In dtExport.Rows

        '    ' Carica le righe successive con i dati estratti sempre che ce ne siano.
        '    cRow = ""
        '    For i = 0 To dtExport.Columns.Count - 1
        '        If IsDBNull(dr_exp(dtExport.Columns(i).ColumnName)) Then
        '            cValue = " "
        '        Else
        '            cValue = Trim(dr_exp(dtExport.Columns(i).ColumnName))
        '        End If

        '        If i = 0 Then
        '            cRow = cValue
        '        Else
        '            cRow = String.Concat(cRow, ";", cValue)
        '        End If
        '    Next
        '    If cRow.Trim.Length > 0 Then
        '        sw.WriteLine(cRow)
        '    End If
        'Next
        '' Chiudi lo stream di output
        'sw.Close()
        'sw.Dispose()

    End Sub

    Private Function CaricaCSVToDataset(ByVal cFileName As String) As DataTable
        Const separator = """;"""

        Dim lIntesta As Boolean
        Dim aLines As String()
        Dim tbl As DataTable
        Dim cTmp2 As String
        Dim cTmp1 As String
        Dim dr As DataRow
        Dim i As Integer

        '
        ' carica le righe della tabella.
        '
        aLines = IO.File.ReadAllLines(cFileName)
        tbl = New DataTable
        lIntesta = True
        For Each cTmp1 In aLines
            i = 0
            cTmp1 = String.Concat(cTmp1, ";""")
            If Not lIntesta Then
                dr = tbl.NewRow
            End If

            Do
                cTmp2 = cTmp1.Substring(0, cTmp1.IndexOf(separator))
                cTmp1 = cTmp1.Substring(cTmp1.IndexOf(separator) + separator.Length)
                If cTmp2.StartsWith("""") Then
                    cTmp2 = cTmp2.Substring(1)
                End If
                If cTmp2.EndsWith("""") Then
                    cTmp2 = cTmp2.Substring(0, cTmp2.Length - 2)
                End If
                If lIntesta Then
                    tbl.Columns.Add(New DataColumn(cTmp2))
                Else
                    dr(i) = cTmp2
                End If
                i += 1
            Loop Until cTmp1.IndexOf(separator) = -1
            If Not lIntesta Then
                tbl.Rows.Add(dr)
            End If
            lIntesta = False
        Next
        Return tbl

    End Function

    Public Sub FastSortFile(ByVal cSourceFile As String, ByVal cDestFile As String, ByVal cSortFile As String)
        Dim xslt As System.Xml.Xsl.XslCompiledTransform

        Try
            xslt = New System.Xml.Xsl.XslCompiledTransform()
            xslt.Load(cSortFile)

            xslt.Transform(cSourceFile, cDestFile)
        Catch ex As Exception

        End Try

    End Sub

    Private Function CaricaFileDatiToDS_excel(ByVal cFileName As String) As DataSet
        Dim dsExcel As DataSet
        Dim dbConn As String
        Dim oConn As OleDb.OleDbConnection
        Dim oCmd As OleDb.OleDbCommand
        Dim oDA As OleDb.OleDbDataAdapter
        Dim cFoglionome As String

        If cFileName > "" Then
            cFoglionome = "Foglio1"
            dbConn = String.Concat("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=", cFileName, ";Extended Properties=""Excel 8.0;HDR=YES;IMEX=1"";")

            oConn = New OleDb.OleDbConnection(dbConn)
            oConn.Open()
            oCmd = New OleDb.OleDbCommand(String.Concat("SELECT * FROM [", cFoglionome, "$]"), oConn)
            oCmd.CommandType = CommandType.Text
            oDA = New OleDb.OleDbDataAdapter
            oDA.SelectCommand = oCmd

            dsExcel = New DataSet()
            oDA.Fill(dsExcel)
            oConn.Close()
        Else
            dsExcel = Nothing
        End If
        Return dsExcel

    End Function

End Class
