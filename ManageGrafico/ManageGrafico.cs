﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;
using System.Xml;
using System.Data;

namespace ManageGrafico
{
    public class Grafico
    {
        private SeriesChartType _TipoGrafico;
        private int _Altezza;
        private int _Larghezza;
        private XmlNode _Series;
        private ChartColorPalette _Palette;

        public XmlNode Series
        {
            set
            {
                _Series = value;
            }
        }

        public Boolean UseCustomPalette;

        public string CustomPalette;

        public DataTable Tabella;

        public int TipoLegendaGrafico;

        public string Palette
        {
            set
            {
                _Palette = (ChartColorPalette)Enum.Parse(typeof(ChartColorPalette), value);
            }
        }

        public string TipoGrafico
        {
            set
            {
                _TipoGrafico = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), value);
            }
        }

        public int Altezza
        {
            set { _Altezza = value; }
        }

        public int Larghezza
        {
            set { _Larghezza = value; }
        }

        public System.Drawing.Image EncodeBmpGrafico()
        {
            if (this.TipoLegendaGrafico == 1)
            {
                return EncodeBmpGrafico1();
            }
            else if (this.TipoLegendaGrafico == 2)
            {
                return EncodeBmpGrafico2();
            }
            else
                return null;

        }

        private System.Drawing.Image EncodeBmpGrafico1()
        {
            string CustomProperty;
            var stream = new System.IO.MemoryStream();

            // create the chart
            var chart = new Chart();
            chart.Size = new Size(_Larghezza, _Altezza);

            var chartArea = new ChartArea();
            chartArea.AxisX.LabelStyle.Format = "MMM-yyyy";
            chartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
            chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
            chartArea.AxisX.LabelStyle.Font = new Font("Calibri", 7);
            chartArea.AxisY.LabelStyle.Font = new Font("Calibri", 7);
            chart.ChartAreas.Add(chartArea);

            //
            // imposta i colori del grafico
            //
            if (UseCustomPalette)
            {
                chart.Palette = ChartColorPalette.None;
                string[] aColor = CustomPalette.Split(';');

                Color[] custPalette = new Color[1];
                Array.Resize(ref custPalette, aColor.Length);

                for (int i = 0; i <= aColor.Length - 1; i++)
                {
                    if (aColor[i].Contains(","))
                    {
                        int nRed = int.Parse(aColor[i].Split(',')[0]);
                        int nGre = int.Parse(aColor[i].Split(',')[1]);
                        int nBlu = int.Parse(aColor[i].Split(',')[2]);
                        custPalette[i] = Color.FromArgb(nRed, nGre, nBlu);
                    }
                    else
                    {
                        custPalette[i] = Color.FromName(aColor[i]);
                    }
                }
                chart.PaletteCustomColors = custPalette;
            }
            else
                chart.Palette = _Palette;
            if (Tabella == null)
                foreach (XmlNode xm in _Series.SelectNodes("serie"))
                {
                    var series = new Series();
                    var xvals = xm.SelectSingleNode("xValue").InnerText.Split(';');
                    var yvals = Array.ConvertAll(xm.SelectSingleNode("yValue").InnerText.Split(';'), Decimal.Parse);

                    CustomProperty = xm.SelectSingleNode("customproperties").InnerText;
                    series.Name = xm.SelectSingleNode("nome").InnerText;
                    series.ChartType = _TipoGrafico;
                    series.XValueType = ChartValueType.String;

                    chart.Series.Add(series);

                    //
                    // bind the datapoints
                    //
                    chart.Series[series.Name].Points.DataBindXY(xvals, yvals);
                    foreach (DataPoint item in chart.Series[series.Name].Points)
                        foreach (string cTmp1 in CustomProperty.Split(','))
                        {
                            item[cTmp1.Split('=')[0]] = cTmp1.Split('=')[1];
                        }


                    chart.Legends.Add(new Legend("Legend1"));

                    // Set title
                    chart.Legends["Legend1"].Alignment = StringAlignment.Center;
                    chart.Legends["Legend1"].Docking = Docking.Left;
                    chart.Legends["Legend1"].TextWrapThreshold = 0;

                    // Assign the legend to Series1.
                    chart.Series[series.Name].Legend = "Legend1";
                    chart.Series[series.Name].IsVisibleInLegend = false;
                }
            else
            {
                //  chart.Series.RemoveAt(0);
                foreach (XmlNode xm in _Series.SelectNodes("serie"))
                {
                    var series = new Series();
                    CustomProperty = xm.SelectSingleNode("customproperties").InnerText;
                    series.Name = xm.SelectSingleNode("nome").InnerText;
                    series.ChartType = _TipoGrafico;

                    series.XValueType = (ChartValueType)Enum.Parse(typeof(ChartValueType), Tabella.Columns[xm.SelectSingleNode("xValueF").InnerText].DataType.ToString().Split('.')[1]);   //charttype ChartValueType.String;

                    series.XValueMember = xm.SelectSingleNode("xValueF").InnerText;
                    series.YValueMembers = xm.SelectSingleNode("yValueF").InnerText;
                    series.CustomProperties = CustomProperty;
                    chart.Series.Add(series);
                }
                foreach (Series cs in chart.Series)
                {
                    cs.ChartType = _TipoGrafico;
                }
                chart.DataSource = Tabella; //InvertiRigheTabella(); //
            }
            chart.Invalidate();

            // write out a file
            chart.SaveImage(stream, ChartImageFormat.Png);
            return System.Drawing.Image.FromStream(stream);
        }

        private System.Drawing.Image EncodeBmpGrafico2()
        {
            string CustomProperty;
            var stream = new System.IO.MemoryStream();

            // create the chart
            var chart = new Chart();
            chart.Size = new Size(_Larghezza, _Altezza);

            ChartArea chartArea = new ChartArea();
            chartArea.AxisX.LabelStyle.Font = new Font("Arial", 7);
            chartArea.AxisX.LabelStyle.Format = "MMM-yyyy";
            chartArea.AxisX.TextOrientation = TextOrientation.Rotated90;
            chartArea.AxisX.MajorGrid.LineColor = Color.LightGray;

            chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
            chartArea.AxisY.LabelStyle.Font = new Font("Arial", 7);
            chart.ChartAreas.Add(chartArea);

            //
            // imposta i colori del grafico
            //
            if (UseCustomPalette)
            {
                chart.Palette = ChartColorPalette.None;
                string[] aColor = CustomPalette.Split(';');

                Color[] custPalette = new Color[1];
                Array.Resize(ref custPalette, aColor.Length);

                for (int i = 0; i <= aColor.Length - 1; i++)
                {
                    if (aColor[i].Contains(","))
                    {
                        int nRed = int.Parse(aColor[i].Split(',')[0]);
                        int nGre = int.Parse(aColor[i].Split(',')[1]);
                        int nBlu = int.Parse(aColor[i].Split(',')[2]);
                        custPalette[i] = Color.FromArgb(nRed, nGre, nBlu);
                    }
                    else
                    {
                        custPalette[i] = Color.FromName(aColor[i]);
                    }
                }
                chart.PaletteCustomColors = custPalette;
            }
            else
                chart.Palette = _Palette;
            if (Tabella == null)
                foreach (XmlNode xm in _Series.SelectNodes("serie"))
                {
                    var series = new Series();
                    var xvals = xm.SelectSingleNode("xValue").InnerText.Split(';');
                    var yvals = Array.ConvertAll(xm.SelectSingleNode("yValue").InnerText.Split(';'), Decimal.Parse);

                    CustomProperty = xm.SelectSingleNode("customproperties").InnerText;
                    series.Name = xm.SelectSingleNode("nome").InnerText;
                    series.ChartType = _TipoGrafico;
                    series.XValueType = ChartValueType.String;

                    chart.Series.Add(series);

                    //
                    // bind the datapoints
                    //
                    chart.Series[series.Name].Points.DataBindXY(xvals, yvals);
                    foreach (DataPoint item in chart.Series[series.Name].Points)
                        foreach (string cTmp1 in CustomProperty.Split(','))
                        {
                            item[cTmp1.Split('=')[0]] = cTmp1.Split('=')[1];
                        }

                    chart.Legends.Add(new Legend("Legend1"));

                    // Set title
                    chart.Legends["Legend1"].Alignment = StringAlignment.Center;
                    chart.Legends["Legend1"].Docking = Docking.Left;
                    chart.Legends["Legend1"].TextWrapThreshold = 0;

                    // Assign the legend to Series1.
                    chart.Series[series.Name].Legend = "Legend1";
                    chart.Series[series.Name].IsVisibleInLegend = false;
                }
            else
            {
                foreach (XmlNode xm in _Series.SelectNodes("serie"))
                {
                    DataView dv;
                    dv = new DataView(Tabella);
                    chart.DataBindCrossTable(dv, xm.SelectSingleNode("xValueF").InnerText, xm.SelectSingleNode("x2ValueF").InnerText, xm.SelectSingleNode("yValueF").InnerText, "Label=" + xm.SelectSingleNode("yValueF").InnerText);
                }

                //
                // Sistemo la formattazione delle SERIE
                //
                foreach (Series cs in chart.Series)
                {
                    cs.ChartType = SeriesChartType.StackedColumn;
                    if (cs.Name.Contains("TOTALE"))
                    {
                        cs.Color = System.Drawing.Color.Transparent;
                        cs.Font = new Font("Arial", 7);
                        cs.LabelAngle = -90;
                        cs.SmartLabelStyle.Enabled = false;
                        cs.IsVisibleInLegend = false;
                    }
                    else
                    {
                        cs.IsVisibleInLegend = true;
                        foreach (DataPoint dp in cs.Points)
                        {
                            dp.Label = "";
                            dp.IsValueShownAsLabel = false;
                        }
                    }
                }

                //
                // Impostazione della leggenda
                //
                chart.Legends.Add(new Legend("legenda"));
                chart.Legends["legenda"].Alignment = StringAlignment.Center;
                chart.Legends["legenda"].Docking = Docking.Right;
                chart.Legends["legenda"].Font = new Font("Arial", 7);
                chart.Legends["legenda"].TextWrapThreshold = 0;
            }
            chart.Invalidate();

            // write out a file
            chart.SaveImage(stream, ChartImageFormat.Png);
            return System.Drawing.Image.FromStream(stream);
        }

        //
        // Inverte l'ordine delle righe della tabella
        //
        private DataTable InvertiRigheTabella()
        {
            DataTable dt;
            DataRow dr1;
            int i;

            dt = Tabella.Clone();
            dt.Rows.Clear();
            i = Tabella.Rows.Count - 1;

            while (i >= 0)
            {
                dr1 = Tabella.Rows[i];
                dt.ImportRow(dr1);
                i -= 1;
            }
            return dt;
        }
    }
}
