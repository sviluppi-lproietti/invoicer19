﻿Public Class MST_enumeratori

    Enum ePDFCreatorFileType
        FT_PDFGeneric = 0
        FT_JPG = 1
        FT_PDF1A = 9
    End Enum

    Enum eCodImbState
        EmptyCode = 0
        noCodImbusta = 1
        CodPagePar = 2
        CodPageDis = 3
        CodAllPage = 4
        ProdCodImb = 100
    End Enum

    Enum eTipoResetPrintTag
        NoReset = 0
        RstPrtl = 1
        RstGlbl = 2
        RstGlblPrtl = 3
    End Enum

    Enum eTipoGestionePagine
        GeneraPagineStampare = 1
        DuplicaPagine = 2
    End Enum

    Enum eStatoStampa
        STS_00_cancellata = -1
        STS_01_inizializza = 0
        STS_02_prontastampa = 1
        STS_03_inizostampa = 2
        STS_04_stampa_parte_terminata = 3
        STS_05_terminata = 4
    End Enum

    Enum eStatoRaccolta
        SR_Primo = 1
        SR_Intermedio = 2
        SR_Ultimo = 3
        SR_Unico = 9
    End Enum

End Class
