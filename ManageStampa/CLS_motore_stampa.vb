Imports System.Drawing
Imports ISC.LibrerieComuni.InvoicerDomain.Impl
Imports ISC.Invoicer.ManageStampa.MST_enumeratori
Imports ISC.Invoicer.ManageStampa.MST_costanti
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML
Imports ISC.LibrerieComuni.OggettiComuni.LogSystem
Imports Spire.Pdf

Public Class CLS_motore_stampa
    Inherits Printing.PrintDocument

    Public Property DatiPerStampa As ManageStampa.CLS_DatiPerStampa
    Public Property GestioneModulo As GestioneModulo
    Public Property StampaForzataDettaglio As Boolean

    Public Property WorkFLD As String

    ''' <summary>
    ''' Restituisce lo stato della stampa
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property StatoStampa As eStatoStampa

    Private _ModuloInStampa As CLS_ele_modulo
    Private _FeedArray As ArrayList
    Private _logStampa As CLS_logger
    ''' <summary>
    ''' Specifica se si deve proseguire la stampa su di una nuova pagina.
    ''' </summary>
    ''' <remarks></remarks>
    Private _StampaSuNuovaPagina As Boolean

    Private _Posizioni As New CLS_posizioni
    Private _PrintError As Boolean
    Private _StatusPrn As Dictionary(Of Integer, String())
    Private _TraySelected As Integer
    Private _PaginaInStampa As CLS_ele_pagina

    WriteOnly Property FeedArray() As ArrayList

        Set(ByVal value As ArrayList)
            _FeedArray = value
        End Set

    End Property

    ReadOnly Property Cancelled() As Boolean

        Get
            Return StatoStampa = eStatoStampa.STS_03_inizostampa
        End Get

    End Property

    ReadOnly Property Splitted() As Boolean

        Get
            Return StatoStampa = eStatoStampa.STS_04_stampa_parte_terminata
        End Get

    End Property

    Property Margini() As Printing.Margins

        Get
            Return Me.PrinterSettings.DefaultPageSettings.Margins
        End Get
        Set(ByVal value As Printing.Margins)
            Me.PrinterSettings.DefaultPageSettings.Margins = value
        End Set

    End Property

    ReadOnly Property StatusPrn() As Dictionary(Of Integer, String())

        Get
            Return _StatusPrn
        End Get

    End Property

    Public Sub New(ByVal PRN_Name As String, ByVal logsFLD As String, ByVal nLogLevel As Integer)
        ' Sets the file stream
        MyBase.New()

        _logStampa = New ISC.LibrerieComuni.OggettiComuni.LogSystem.CLS_logger(logsFLD, nLogLevel, 1000000, "log_stampa")

        _StatusPrn = New Dictionary(Of Integer, String())
        Me.PrinterSettings.PrinterName = PRN_Name

    End Sub

    Private Sub SettaStampaSinteticoDettaglio()

        If _ModuloInStampa.ConDettaglio Then
            If (DatiPerStampa.LoopSinteticoDettaglio = 1) Then
                If DatiPerStampa.DBStampa.MainTable.Columns.Contains("DEF_fattura_dettaglio") Then
                    DatiPerStampa.DBStampa.RecordMasterRow.Item("DEF_stampa_dettaglio") = DatiPerStampa.DBStampa.RecordMasterRow.Item("DEF_fattura_dettaglio")
                Else
                    DatiPerStampa.DBStampa.RecordMasterRow.Item("DEF_stampa_dettaglio") = DatiPerStampa.DBStampa.RecordMasterRow.Item("DEF_richiesta_dettaglio")
                End If
            ElseIf (DatiPerStampa.LoopSinteticoDettaglio = 2) Then
                DatiPerStampa.DBStampa.RecordMasterRow.Item("DEF_stampa_dettaglio") = DatiPerStampa.DBStampa.RecordMasterRow.Item("DEF_fattura_dettaglio")
            ElseIf (DatiPerStampa.LoopSinteticoDettaglio = 3) Then
                DatiPerStampa.DBStampa.RecordMasterRow.Item("DEF_stampa_dettaglio") = True
            End If
        End If

    End Sub

    Private Sub CLS_motore_stampa_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeginPrint

        _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 0, "", "CLS_printer_BeginPrint", "Inizio processo di stampa.")
        _ModuloInStampa = BuildElement(GestioneModulo.ModuloDaStampare, Nothing, Nothing, Nothing)

        '
        ' Stato Avanzamento
        '
        StatoStampa = eStatoStampa.STS_01_inizializza

        '
        ' Impostazione dei controlli della stampante.
        '
        Me.PrinterSettings.PrintToFile = DatiPerStampa.Anteprima
        Me.PrinterSettings.DefaultPageSettings.Margins = New Printing.Margins(0, 0, 0, 0)
        Me.DefaultPageSettings.PrinterResolution.Kind = Printing.PrinterResolutionKind.High
        If DatiPerStampa.FronteRetro Then
            Me.DefaultPageSettings.Landscape = GestioneModulo.DefaultOrientamento = "o"
            If GestioneModulo.DefaultOrientamento = "o" Then
                Me.DefaultPageSettings.PrinterSettings.Duplex = Printing.Duplex.Horizontal
            ElseIf GestioneModulo.DefaultOrientamento = "v" Then
                Me.DefaultPageSettings.PrinterSettings.Duplex = Printing.Duplex.Vertical
            End If
        Else
            Me.DefaultPageSettings.Landscape = GestioneModulo.DefaultOrientamento = "o"
            Me.DefaultPageSettings.PrinterSettings.Duplex = Printing.Duplex.Simplex
        End If

        '
        ' Imposto il vettore delle posizioni fisse.
        '
        _Posizioni.Fisse = GestioneModulo.PosizioniFisse

        '
        ' Gestione pagine da stampare
        '
        SettaStampaSinteticoDettaglio()
        GestionePagineStampare()
        Me.ImpostaCassettoCarta()
        StatoStampa = eStatoStampa.STS_02_prontastampa

    End Sub

    Private Sub GestionePagineStampare()
        Dim ele_pres As CLS_ele_prestampato
        Dim cTmp As String

        If _ModuloInStampa.ContainsPreStampato Then
            ele_pres = _ModuloInStampa.GetElementoPrestampato()
            _ModuloInStampa.SetNumeroPaginePrestampato(GetPagePdf(DatiPerStampa.AccessiDati.ExtractValues(ele_pres.GetValueTAG, "", 0, "")))
        End If
        If DatiPerStampa.DBStampa.MainTable.Columns.Contains("DEF_richiesta_dettaglio") Then
            _ModuloInStampa.PreparaListaPagineDaStampare(DatiPerStampa.DBStampa.MainTableCurrentRecord.Item("DEF_richiesta_dettaglio"), Not StampaForzataDettaglio)
        Else
            _ModuloInStampa.PreparaListaPagineDaStampare(False, StampaForzataDettaglio)
        End If

    End Sub

    Protected Overrides Sub OnPrintPage(ByVal e As Printing.PrintPageEventArgs)
        Dim lProxDocumento As Boolean
        Dim nRecStatCode As Integer
        Dim cRecStatDesc As String
        Dim nRecWork As Integer

        '
        ' Impostazioni preliminari
        '
        StatoStampa = eStatoStampa.STS_03_inizostampa
        e.Graphics.PageUnit = GraphicsUnit.Millimeter
        _StampaSuNuovaPagina = False
        'If (_ModuloInStampa.ConDettaglio) And (DatiPerStampa.DBStampa.StatoRaccolta <> eStatoRaccolta.SR_Unico) Then
        '    If (DatiPerStampa.DBStampa.StatoRaccolta = eStatoRaccolta.SR_Primo) Then
        '        DatiPerStampa.SwitchStampaDettaglio = DatiPerStampa.DBStampa.MainTableCurrentRecord.Item("DEF_stampa_dettaglio")
        '    Else
        '        DatiPerStampa.DBStampa.MainTableCurrentRecord.Item("DEF_stampa_dettaglio") = DatiPerStampa.SwitchStampaDettaglio
        '        DatiPerStampa.DBStampa.MainTableCurrentRecord.AcceptChanges()
        '    End If
        'End If

        '
        ' Salto dei documenti la cui anteprima e gi� stata caricata
        '
        While Me.DatiPerStampa.Anteprima And DatiPerStampa.ContatoriPagine.GiaEseguitaAnteprima
            DatiPerStampa.DBStampa.MoveRecordNumber(DatiPerStampa.DBStampa.MainTableName)
        End While

        '
        ' Stampa delle pagine del modulo
        '
        Try
            _PrintError = False
            _PaginaInStampa = _ModuloInStampa.FormattaPaginaInStampa(DatiPerStampa.ContatoriPagine.PagineSubDocumento)
            _PaginaInStampa.ImpostaMargineBasso(GetValueFromXML(GestioneModulo.ModuloDaStampare, itm_DefaultMaxBottom, 300))
            'If _ModuloInStampa.ConDettaglio And _PaginaInStampa.SwitchStampaDettaglioExist And Not StampaForzataDettaglio Then
            '    If _PaginaInStampa.SwitchStampaDettaglio Then
            '        DatiPerStampa.DBStampa.MainTableCurrentRecord.Item("DEF_stampa_dettaglio") = True
            '    Else
            '        DatiPerStampa.DBStampa.MainTableCurrentRecord.Item("DEF_stampa_dettaglio") = False
            '    End If
            'End If
            _Posizioni.NamedItem.Clear()
            If Not GestioneModulo.ModuloSenzaSelezioneNecessaria Then
                nRecWork = DatiPerStampa.DBStampa.RecordMasterRow.Item(DatiPerStampa.DBStampa.MainLinkToQD)
            End If
            StampaPagina(e, _PaginaInStampa.XMLItem, _ModuloInStampa.Defaults.ColoreBrush, _ModuloInStampa.Defaults.TipoCarattere, Nothing, DatiPerStampa.DBStampa.MainTableName)

            '
            ' Aggiungo una pagina se necessario.
            '
            If _StampaSuNuovaPagina Then
                _ModuloInStampa.DuplicaPagina(DatiPerStampa.ContatoriPagine.PagineSubDocumento)
            End If

            ' *************************************************************************** '
            ' Verifica per la stampa del codice a barre per il controllo dell'imbustatri- ' 
            ' ce.                                                                         '
            ' *************************************************************************** '
            If Not DatiPerStampa.Anteprima Then
                If Not DatiPerStampa.FronteRetro Or (DatiPerStampa.FronteRetro And (DatiPerStampa.ContatoriPagine.PagineDocumento Mod 2 = 1)) Then
                    _TraySelected = _PaginaInStampa.XMLItem.SelectSingleNode("tray").InnerText.Replace("*", "")
                End If
                If PrintImbCode(DatiPerStampa.ContatoriPagine.PagineDocumento, DatiPerStampa.ContatoriPagine.PagineTotaliDocumento) Then
                    StampaElemento(e, GetXmlNodeFromXML(GestioneModulo.ModuloDaStampare, itm_ItemImb).Clone, _ModuloInStampa.Defaults.ColoreBrush, _ModuloInStampa.Defaults.TipoCarattere)
                End If
            End If
            nRecStatCode = 0
            cRecStatDesc = ""
        Catch ex As Exception
            _PrintError = True
            nRecStatCode = -301
            cRecStatDesc = ex.Message
            If Not (ex.InnerException Is Nothing) Then
                cRecStatDesc = String.Concat(cRecStatDesc, vbLf, ex.InnerException.Message)
            End If
        End Try

        ' *************************************************************************** '
        ' Definizione della necessit� di proseguire la stampa su di una nuova pagina. '
        ' *************************************************************************** '
        lProxDocumento = False
        If _PrintError Then
            lProxDocumento = True
            '
            ' Se la stampa si � conclusa con errore allora devo avanzare al prossimo documento
            '
            DatiPerStampa.FineStampaDocumento()
        Else
            e.HasMorePages = True
            DatiPerStampa.AvanzaPaginaStampata()

            If (DatiPerStampa.ContatoriPagine.PagineSubDocumento <> _ModuloInStampa.NumPagineDaStampare) Then
                _PaginaInStampa = _ModuloInStampa.FormattaPaginaInStampa(DatiPerStampa.ContatoriPagine.PagineSubDocumento)
                If (_PaginaInStampa.StampaSoloSePari) Then
                    If ((DatiPerStampa.ContatoriPagine.PagineSubDocumento + 1) Mod 2) = 1 Then
                        _ModuloInStampa.ListaPagineDaStampare.RemoveAt(DatiPerStampa.ContatoriPagine.PagineSubDocumento)
                    End If
                End If
            End If

            '
            ' Verifico che ho stampato tutte le pagine del sub documento 
            '
            If (DatiPerStampa.ContatoriPagine.PagineSubDocumento = _ModuloInStampa.NumPagineDaStampare) Then
                DatiPerStampa.ContatoriPagine.FineStampaSubDocumento()
                _Posizioni.Fisse = GestioneModulo.PosizioniFisse
                lProxDocumento = True

                '
                ' Controllo se ho stampato tutti i sub documenti del documento
                '
                If DatiPerStampa.DBStampa.StatoRaccolta = eStatoRaccolta.SR_Ultimo Or DatiPerStampa.DBStampa.StatoRaccolta = eStatoRaccolta.SR_Unico Then
                    DatiPerStampa.FineStampaDocumento()
                    lProxDocumento = True
                ElseIf (DatiPerStampa.ArchiviazioneOttica And Not DatiPerStampa.Anteprima) Then
                    If DatiPerStampa.ContatoriPagine.NuovaParte Then
                        DatiPerStampa.ContatoriPagine.NuovaParte = False
                        e.HasMorePages = False
                    End If
                End If
            End If
        End If
        If lProxDocumento Then
            If _PrintError Then
                While (DatiPerStampa.DBStampa.StatoRaccolta = eStatoRaccolta.SR_Primo Or DatiPerStampa.DBStampa.StatoRaccolta = eStatoRaccolta.SR_Intermedio)
                    DatiPerStampa.DBStampa.MoveRecordNumber(DatiPerStampa.DBStampa.MainTableName)
                End While
            Else
                If (DatiPerStampa.DBStampa.RecordNumber(DatiPerStampa.DBStampa.MainTableName) < DatiPerStampa.DBStampa.MainTable.Rows.Count - 1) Then
                    DatiPerStampa.DBStampa.MoveRecordNumber(DatiPerStampa.DBStampa.MainTableName)
                Else
                    e.HasMorePages = False
                End If
            End If
            SettaStampaSinteticoDettaglio()
            GestionePagineStampare()
        End If
        If e.HasMorePages Then
            Me.ImpostaCassettoCarta(e)
        Else
            '
            ' Imposta la modalit� di uscita dalla stampa
            '
            If DatiPerStampa.Anteprima Then
                StatoStampa = eStatoStampa.STS_05_terminata
            Else
                If (DatiPerStampa.ArchiviazioneOttica And DatiPerStampa.ContatoriPagine.PagineDocumento <> DatiPerStampa.ContatoriPagine.PagineTotaliDocumento) Or Not DatiPerStampa.ArchiviazioneOttica Then
                    StatoStampa = eStatoStampa.STS_04_stampa_parte_terminata
                Else
                    StatoStampa = eStatoStampa.STS_05_terminata
                End If
            End If
        End If

    End Sub

    Private Sub CLS_motore_stampa_EndPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.EndPrint

        _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 0, "", "CLS_motore_stampa_BeginPrint", "Fine processo di stampa.")

    End Sub

    ''' <summary>
    ''' Costruisce l'elemento per la stampa.
    ''' </summary>
    ''' <param name="xnItem"></param>
    ''' <param name="upColor"></param>
    ''' <param name="upFont"></param>
    ''' <param name="upPos"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function BuildElement(ByVal xnItem As Xml.XmlNode, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal)) As CLS_elemento_base
        Dim ebr As CLS_elemento_base
        Dim eb As CLS_elemento_base

        Try
            eb = New CLS_elemento_base(xnItem)
            If (eb.TipoElemento = eTipoElemento.TEL_modulo) Then
                ebr = New CLS_ele_modulo(eb)
            ElseIf (eb.TipoElemento = eTipoElemento.TEL_blocco) Then
                ebr = New CLS_ele_blocco(eb, _Posizioni, upColor, upFont, upPos)
            ElseIf (eb.TipoElemento = eTipoElemento.TEL_pagina) Then
                ebr = New CLS_ele_pagina(eb, _Posizioni, upColor, upFont, upPos)
            ElseIf (eb.TipoElemento = eTipoElemento.TEL_item) Then
                If (eb.SottoTipoElemento = eSottoTipoItem.Fincatura) Then
                    ebr = New CLS_ele_fincatura(eb, _Posizioni, upColor, upFont, upPos)
                    CType(ebr, CLS_ele_fincatura).ImageFLD = DatiPerStampa.Folders("ModuliFLD")
                ElseIf (eb.SottoTipoElemento = eSottoTipoItem.Immagine) Then
                    ebr = New CLS_ele_immagine(eb, _Posizioni, upColor, upFont, upPos)
                    CType(ebr, CLS_ele_immagine).ImageFLD = DatiPerStampa.Folders("ModuliFLD")
                ElseIf (eb.SottoTipoElemento = eSottoTipoItem.Linea) Then
                    ebr = New CLS_ele_linea(eb, _Posizioni, upColor, upFont, upPos)
                ElseIf (eb.SottoTipoElemento = eSottoTipoItem.Rettangolo) Then
                    ebr = New CLS_ele_rettangolo(eb, _Posizioni, upColor, upFont, upPos)
                ElseIf (eb.SottoTipoElemento = eSottoTipoItem.Grafico1) Then
                    ebr = New CLS_ele_itm_grafico(eb, _Posizioni, upColor, upFont, upPos)
                ElseIf (eb.SottoTipoElemento = eSottoTipoItem.Grafico2) Then
                    ebr = New CLS_ele_itm_grafico(eb, _Posizioni, upColor, upFont, upPos)
                ElseIf (eb.SottoTipoElemento = eSottoTipoItem.BarCode) Then
                    ebr = New CLS_ele_barcode(eb, _Posizioni, upColor, upFont, upPos)
                ElseIf (eb.SottoTipoElemento = eSottoTipoItem.TestoInvertito) Or (eb.SottoTipoElemento = eSottoTipoItem.Testo) Then
                    ebr = New CLS_ele_testo(eb, _Posizioni, upColor, upFont, upPos)
                ElseIf (eb.SottoTipoElemento = eSottoTipoItem.PreStampato) Then
                    ebr = New CLS_ele_prestampato(eb, _Posizioni, upColor, upFont, upPos)
                Else
                    ebr = New CLS_elemento_base(eb.XMLItem, _Posizioni, upColor, upFont, upPos)
                End If
            ElseIf (eb.TipoElemento = eTipoElemento.TEL_sezione) Then
                ebr = New CLS_ele_sezione(eb, _Posizioni, upColor, upFont, upPos)
            ElseIf (eb.TipoElemento = eTipoElemento.TEL_corpo) Then
                ebr = New CLS_ele_sezione(eb, _Posizioni, upColor, upFont, upPos)
            Else
                ' Throw New Exception("Elemento da stampare sconosciuto")
            End If
            If ebr IsNot Nothing Then
                ebr.CheckCondizioni = AddressOf ControllaCondizioni
            End If
        Catch ex As Exception
            ebr = Nothing
            Throw New Exception("MS-000001: Errore generando l'elemento da stampare")
        End Try
        Return ebr

    End Function

    Private Sub AddStatusRec(ByVal nRecCode As Integer, ByVal nRecStatus As Integer, ByVal cRecDesc As String)

        If Not _StatusPrn.ContainsKey(nRecCode) Then
            _StatusPrn.Add(nRecCode, New String() {nRecStatus, cRecDesc})
        End If

    End Sub

    Private Function PrintImbCode(ByVal _PageDoc_Wrk As Integer, ByVal _PageTot_Wrk As Integer) As Boolean
        Dim _lPrintImb As Boolean

        _lPrintImb = False
        If NodeExist(GestioneModulo.ModuloDaStampare, itm_ItemImb) Then
            Select Case GestioneModulo.ModuloDaStampare.SelectSingleNode(itm_CodiceImbustatrice).InnerText
                Case Is = eCodImbState.CodPagePar
                    _lPrintImb = _PageDoc_Wrk Mod 2 = 0
                Case Is = eCodImbState.CodPageDis
                    _lPrintImb = _PageDoc_Wrk Mod 2 = 1
                Case Is = eCodImbState.CodAllPage
                    _lPrintImb = True
            End Select
        End If
        Return _lPrintImb

    End Function

    '
    ' Gestisce il processo di stampa della pagina. 
    '
    Private Sub StampaPagina(ByVal e As Printing.PrintPageEventArgs, ByVal xnItems As Xml.XmlNode, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal), ByVal cParentTable As String, Optional ByVal _RelationName As String = "")
        Dim eleC As CLS_elemento_base
        Dim eleM As CLS_ele_pagina
        Dim i As Integer

        eleM = Nothing
        Try
            eleM = BuildElement(xnItems, upColor, upFont, upPos)
            i = 0
            While Not _StampaSuNuovaPagina And (i < eleM.XMLItem.ChildNodes.Count)
                eleC = New CLS_elemento_base(eleM.XMLItem.ChildNodes(i))
                If (eleC.IsElementoStampa) And eleC.IsDaStampare Then
                    If Not eleC.JumpPrint Then
                        If eleC.SottoTipoElemento = eSottoTipoItem.PreStampato Then
                            eleC.PaginaPreStampato = eleM.PaginaPreStampato
                        End If
                        Select Case eleC.TipoElemento
                            Case Is = eTipoElemento.TEL_item
                                StampaElemento(e, eleC.XMLItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione, _RelationName)
                            Case Is = eTipoElemento.TEL_sezione
                                StampaSezione(e, eleC.XMLItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione, _RelationName)
                            Case Is = eTipoElemento.TEL_blocco
                                StampaBlocco(e, eleC.XMLItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione, cParentTable, _RelationName)
                        End Select
                    Else
                        eleC.RimuoviAttributoElemento("JumpPrint")
                    End If
                End If
                i += 1
            End While
        Catch ex As Exception
            If eleM Is Nothing Then
                Throw SetExceptionItem("Errore in StampaPagina. Codice Elemento= Non Disponibile", ex)
            Else
                Throw SetExceptionItem(String.Concat("Errore in StampaPagina. Codice Elemento= ", eleM.Codice), ex)
            End If
        End Try

    End Sub

    Private Function StampaSezione(ByVal e As Printing.PrintPageEventArgs, ByVal xnItems As Xml.XmlNode, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal), ByVal cParentTable As String, Optional ByVal _RelationName As String = "") As Decimal
        Dim eleM As CLS_ele_sezione
        Dim eleC As CLS_elemento_base
        Dim i As Integer

        eleM = Nothing
        Try
            eleM = BuildElement(xnItems, upColor, upFont, upPos)
            If eleM.IsDaStampare And DatiPerStampa.AccessiDati.EvaluateCondition(eleM.XMLItem, _RelationName) Then
                If GetAttrFromXML(eleM.XMLItem, "checkBottomMargin", "0") = 1 Then
                    _StampaSuNuovaPagina = eleM.AltezzaMinimaRichiesta + eleM.Posizione("Y") > _PaginaInStampa.MargineBasso
                    If _StampaSuNuovaPagina Then
                        eleM.SegnaDaRiusare(-1)
                    End If
                End If
                If Not _StampaSuNuovaPagina Then
                    i = 0
                    If (eleM.IndiceRiutilizzo > -1) Then
                        eleC = New CLS_elemento_base(eleM.XMLItem.ChildNodes(i))
                        While Not (eleC.IsElementoStampa And (eleC.IndiceRiutilizzo = eleM.IndiceRiutilizzo - 1)) And (i < eleM.XMLItem.ChildNodes.Count)
                            i += 1
                            eleC = New CLS_elemento_base(eleM.XMLItem.ChildNodes(i))
                        End While
                        eleM.RimuoviAttributoElemento("IndiceRiutilizzo")
                    End If
                    While Not _StampaSuNuovaPagina And (i < eleM.XMLItem.ChildNodes.Count)
                        eleC = New CLS_elemento_base(eleM.XMLItem.ChildNodes(i))
                        If (eleC.IsElementoStampa) Then
                            If eleC.IsDaStampare Then
                                Select Case eleC.TipoElemento
                                    Case Is = eTipoElemento.TEL_item
                                        eleM.MaxPosizioneY = StampaElemento(e, eleC.XMLItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione, _RelationName)
                                    Case Is = eTipoElemento.TEL_sezione
                                        eleM.MaxPosizioneY = StampaSezione(e, eleC.XMLItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione, _RelationName)
                                    Case Is = eTipoElemento.TEL_blocco
                                        eleM.MaxPosizioneY = StampaBlocco(e, eleC.XMLItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione, cParentTable, _RelationName)
                                        ManageNamedItem(eleM.Nome, New Decimal() {0, eleM.Posizione("Y"), 0})
                                End Select
                            Else
                                ManageNamedItem(eleM.Nome, New Decimal() {0, eleM.Posizione("Y"), 0})
                            End If
                        End If
                        If eleC.IndiceRiutilizzo > -1 Then
                            eleM.SegnaDaRiusare(eleC.IndiceRiutilizzo)
                        End If
                        i += 1
                    End While
                    If eleM.IsBoxed Then
                        eleM.SetUpBoxedItem(eleM.MaxPosizioneY)
                        eleM.MaxPosizioneY = StampaElemento(e, eleM.BoxItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione)
                        eleM.RipristinaBox()
                    End If
                End If
                ManageNamedItem(eleM.Nome, New Decimal() {0, eleM.MaxPosizioneY, 1})
            Else
                ManageNamedItem(eleM.Nome, New Decimal() {0, eleM.Posizione("Y"), 0})
            End If
        Catch ex As Exception
            If eleM Is Nothing Then
                Throw SetExceptionItem("Errore in StampaSezione. Codice Elemento Non Disponibile", ex)
            Else
                Throw SetExceptionItem(String.Concat("Errore in StampaSezione. Codice Elemento= ", eleM.Codice), ex)
            End If
        End Try
        Return eleM.MaxPosizioneY

    End Function

    Private Function StampaBlocco(ByVal e As Printing.PrintPageEventArgs, ByVal xmlItems As Xml.XmlNode, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal), ByVal cParentTable As String, Optional ByVal _relationName As String = "") As Decimal
        Dim ele As CLS_ele_blocco
        Dim dr_tbl As DataRow
        Dim nMaxPrintedY As Decimal
        Dim nPrintedY As Decimal
        Dim nFstRowkOffSet As Decimal
        Dim nRowOffset As Decimal
        Dim xlGridElem As Xml.XmlNodeList
        Dim nMaxRowXBlock As Integer
        Dim lMoreRow As Boolean
        Dim nMaxRowXBlockAction As Integer
        Dim lHitMax As Boolean
        Dim dr_array As DataRow()
        Dim lToPrint As Boolean
        Dim cRelName As String
        Dim nRowDisplayed As Integer
        Dim nNoMaxBottom As Integer
        Dim nCodiceRiutilizzo As Integer = -1

        ele = Nothing
        Try
            DatiPerStampa.DBStampa.AvanzaRecordMaster(True)
            nMaxPrintedY = Decimal.MinValue

            ele = BuildElement(xmlItems, upColor, upFont, upPos)
            If ele.IndiceRiutilizzo = 0 Then
                ele.RimuoviAttributoElemento("IndiceRiutilizzo")
            End If
            nNoMaxBottom = GetValueFromXML(ele.XMLItem, "NoMaxBottom", 0)
            cRelName = DatiPerStampa.DBStampa.GetRelationName(ele.TabellaBlocco)
            lToPrint = ele.IsDaStampare And DatiPerStampa.AccessiDati.EvaluateCondition(ele.XMLItem, _relationName, cRelName)
            If lToPrint Then
                lToPrint = (ele.AltezzaMinimaRichiesta = Decimal.MaxValue) Or (nNoMaxBottom = 1)
                If Not lToPrint Then
                    lToPrint = (ele.AltezzaMinimaRichiesta + ele.Posizione("Y") < _PaginaInStampa.MargineBasso)
                End If
                If lToPrint Then
                    '    If (ele.AltezzaMinimaRichiesta + ele.Posizione("Y") < _PaginaInStampa.MargineBasso) Or (nNoMaxBottom = 1) Then
                    cRelName = DatiPerStampa.DBStampa.GetRelationName(ele.TabellaBlocco)
                    lToPrint = DatiPerStampa.AccessiDati.EvaluateCondition(ele.XMLItem, _relationName, cRelName)
                Else
                    DatiPerStampa.DBStampa.SetUpRecordNumber(ele.TabellaBlocco)
                    _StampaSuNuovaPagina = GetValueFromXML(ele.XMLItem, "NotInPage", 1) = 1
                    lToPrint = False
                End If
            End If
            If lToPrint Then
                If Not ele.ContinuaDati Then
                    If (ele.TabellaBlocco > "") Then
                        DatiPerStampa.DBStampa.SetUpRecordNumber(ele.TabellaBlocco)
                    End If
                End If

                xlGridElem = ele.XMLItem.SelectNodes("item[@type='gridelemet']")

                '
                ' Stampa l'intestazione
                '
                If ele.ExistIntestazione Then
                    If xlGridElem.Count > 0 Then
                        For Each xnTmp As Xml.XmlNode In xlGridElem
                            If xnTmp.SelectSingleNode("altezza") IsNot Nothing Then
                                xnTmp.SelectSingleNode("Posizione/Y").InnerText = ele.Posizione("Y") + xnTmp.SelectSingleNode("Posizione/Y").Attributes("OffSet").Value
                            End If
                        Next
                    End If
                    nMaxPrintedY = StampaElementiBlocco(e, ele.Intestazione, ele.ColoreBrush, ele.TipoCarattere, ele.Posizione, cParentTable)
                End If
                nFstRowkOffSet = GetValueFromXML(ele.XMLItem, "FstRowOffSet", 0)
                nRowOffset = GetValueFromXML(ele.XMLItem, "RowOffset", 0)
                nMaxRowXBlock = GetValueFromXML(ele.XMLItem, "MaxRowXBlock", -1)
                nMaxRowXBlockAction = GetValueFromXML(ele.XMLItem, "MaxRowXBlockAction", 1)
                ele.Posizione("Y") += nFstRowkOffSet

                nRowDisplayed = GetValueFromXML(ele.XMLItem, "RigheVisualizzate", "0")
                If ele.ExistCorpo Then
                    If (ele.TabellaBlocco > "") Then
                        lHitMax = False
                        lMoreRow = True
                        dr_array = DatiPerStampa.DBStampa.RecordMasterRow.GetChildRows(cRelName)
                        While Not lHitMax And lMoreRow And DatiPerStampa.DBStampa.RecordNumber(ele.TabellaBlocco) <= dr_array.GetUpperBound(0) And Not _StampaSuNuovaPagina
                            dr_tbl = DatiPerStampa.DBStampa.RecordMasterRow.GetChildRows(cRelName)(DatiPerStampa.DBStampa.RecordNumber(ele.TabellaBlocco))
                            If FilterTableRow(dr_array, dr_tbl, ele.XMLItem, nRowDisplayed) Then
                                DatiPerStampa.DBStampa.AddRecordMaster(New Object() {ele.TabellaBlocco, dr_tbl})
                                nPrintedY = StampaElementiBlocco(e, ele.Corpo, ele.ColoreBrush, ele.TipoCarattere, ele.Posizione, cParentTable, cRelName)
                                DatiPerStampa.DBStampa.RemoveRecordMaster()
                                ele.Posizione("Y") = nPrintedY + nRowOffset
                                nMaxPrintedY = Math.Max(nPrintedY, nMaxPrintedY)
                            End If
                            If Not _StampaSuNuovaPagina Then
                                DatiPerStampa.DBStampa.MoveRecordNumber(ele.TabellaBlocco)
                            End If
                            lMoreRow = (nMaxRowXBlock = -1) Or ((nMaxRowXBlock > 0) And (nMaxRowXBlock > DatiPerStampa.DBStampa.RecordNumber(ele.TabellaBlocco)))
                            lHitMax = (nMaxPrintedY > _PaginaInStampa.MargineBasso) And (DatiPerStampa.DBStampa.RecordNumber(ele.TabellaBlocco) < dr_array.Length)
                        End While
                    Else
                        nPrintedY = StampaElementiBlocco(e, ele.Corpo, ele.ColoreBrush, ele.TipoCarattere, ele.Posizione, cParentTable)
                        ele.Posizione("Y") = nPrintedY + nRowOffset
                        nMaxPrintedY = Math.Max(nPrintedY, nMaxPrintedY)
                    End If
                End If
                If lHitMax Then
                    Select Case nMaxRowXBlockAction
                        Case Is = 1
                            _StampaSuNuovaPagina = True
                        Case Is = 2
                            _StampaSuNuovaPagina = False
                    End Select
                End If

                If ele.ExistPiediBlocco And Not _StampaSuNuovaPagina Then
                    nPrintedY = StampaElementiBlocco(e, ele.PieDiBlocco, ele.ColoreBrush, ele.TipoCarattere, ele.Posizione, cParentTable)
                End If
                If ele.IsBoxed Then
                    nMaxPrintedY = Math.Max(nPrintedY, nMaxPrintedY)
                    ele.SetUpBoxedItem(nMaxPrintedY)
                    ele.MaxPosizioneY = StampaElemento(e, ele.BoxItem, ele.ColoreBrush, ele.TipoCarattere, ele.Posizione)
                    ele.RipristinaBox()
                End If
                If xlGridElem.Count > 0 Then
                    nMaxPrintedY = Math.Max(nPrintedY, nMaxPrintedY)
                    For Each xnTmp As Xml.XmlNode In xlGridElem
                        If xnTmp.SelectSingleNode("PrintItemType").InnerXml.ToLower = "line" Then
                            If xnTmp.SelectSingleNode("lunghezza") IsNot Nothing Then
                                Throw New Exception("Situazione non implementata")
                            End If
                            If xnTmp.SelectSingleNode("altezza") IsNot Nothing Then
                                xnTmp.SelectSingleNode("altezza").InnerText = ele.Posizione("Y") + xnTmp.SelectSingleNode("altezza").Attributes("OffSet").Value - xnTmp.SelectSingleNode("Posizione/Y").InnerText
                            End If
                            'ElseIf xnTmp.SelectSingleNode("PrintItemType").InnerXml.ToLower = "rectangle" Then
                            '    xmlBoxedBlock.SelectSingleNode("Dimensioni/X").InnerText = ele.Posizione("X") + xmlBoxedBlock.SelectSingleNode("Dimensioni/X").Attributes("OffSet").Value - xmlBoxedBlock.SelectSingleNode("Posizione/X").InnerText
                            '    xmlBoxedBlock.SelectSingleNode("Dimensioni/Y").InnerText = nMaxPrintedY + xmlBoxedBlock.SelectSingleNode("Dimensioni/Y").Attributes("OffSet").Value - xmlBoxedBlock.SelectSingleNode("Posizione/Y").InnerText
                        End If
                        StampaElemento(e, xnTmp, ele.ColoreBrush, ele.TipoCarattere, ele.Posizione)
                    Next
                End If
                If nMaxPrintedY > Decimal.MinValue Then
                    ManageNamedItem(ele.Nome, New Decimal() {0, nMaxPrintedY, 1})
                Else
                    ManageNamedItem(ele.Nome, New Decimal() {0, ele.Posizione("Y"), 1})
                End If
            Else
                ManageNamedItem(ele.Nome, New Decimal() {0, ele.Posizione("Y"), 0})
            End If
            If _StampaSuNuovaPagina Then
                ele.SegnaDaRiusare(ele.IndiceRiutilizzoCorpo)
            End If
            DatiPerStampa.DBStampa.AvanzaRecordMaster(False)
            ele.RimuoviAttributoElemento("continuaDati")
        Catch ex As Exception
            If ele Is Nothing Then
                Throw SetExceptionItem("Errore in StampaBlocco. Codice Elemento Non Disponibile", ex)
            Else
                Throw SetExceptionItem(String.Concat("Errore in StampaBlocco. Codice Elemento= ", ele.Codice), ex)
            End If
        End Try
        Return nMaxPrintedY

    End Function

    Private Function StampaElementiBlocco(ByVal e As Printing.PrintPageEventArgs, ByVal xnItems As Xml.XmlNode, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal), ByVal cParentTable As String, Optional ByVal _RelationName As String = "") As Decimal
        Dim eleM As CLS_elemento_base
        Dim eleC As CLS_elemento_base
        Dim nPrintedY As Decimal
        Dim i As Integer

        Try
            eleM = BuildElement(xnItems, upColor, upFont, upPos)
            If eleM.IsDaStampare And DatiPerStampa.AccessiDati.EvaluateCondition(eleM.XMLItem, _RelationName) Then
                i = 0

                '
                ' Se si tratta di un blocco nidificato controlla che all'interno non vi siano ulteriori blocchi.
                '
                If eleM.IsElementoStampa(eTipoElemento.TEL_corpo) And eleM.IndiceRiutilizzo > -1 Then    'IsPrintItem(eleM.XMLItem, "corpo") 
                    eleC = New CLS_elemento_base(eleM.XMLItem.ChildNodes(i))
                    While Not (eleC.IsElementoStampa And (eleC.IndiceRiutilizzo = eleM.IndiceRiutilizzo - 1)) And (i < eleM.XMLItem.ChildNodes.Count)
                        i += 1
                        eleC = New CLS_elemento_base(eleM.XMLItem.ChildNodes(i))
                    End While
                    eleM.RimuoviAttributoElemento("IndiceRiutilizzo")
                End If
                While Not _StampaSuNuovaPagina And (i < eleM.XMLItem.ChildNodes.Count)
                    eleC = New CLS_elemento_base(eleM.XMLItem.ChildNodes(i))
                    If eleC.IsElementoStampa Then
                        If eleC.IsDaStampare Then
                            If Not eleC.JumpPrint Then
                                Select Case eleC.TipoElemento
                                    Case Is = eTipoElemento.TEL_item
                                        nPrintedY = StampaElemento(e, eleC.XMLItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione, _RelationName)
                                    Case Is = eTipoElemento.TEL_sezione
                                        nPrintedY = StampaSezione(e, eleC.XMLItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione, _RelationName)
                                    Case Is = eTipoElemento.TEL_blocco
                                        nPrintedY = StampaBlocco(e, eleC.XMLItem, eleM.ColoreBrush, eleM.TipoCarattere, eleM.Posizione, cParentTable, _RelationName)
                                End Select
                                eleM.MaxPosizioneY = nPrintedY
                            Else
                                eleC.RimuoviAttributoElemento("JumpPrint")
                            End If
                        Else
                            ManageNamedItem(eleC.Nome, New Decimal() {0, eleC.Posizione("Y"), 0})
                        End If
                    End If
                    If _StampaSuNuovaPagina Then
                        eleM.SegnaDaRiusare(eleC.IndiceRiutilizzo)
                    End If
                    i += 1
                End While
                ManageNamedItem(eleM.Nome, New Decimal() {0, eleM.MaxPosizioneY, 1})
            Else
                ManageNamedItem(eleM.Nome, New Decimal() {0, eleM.Posizione("Y"), 0})
            End If
        Catch ex As Exception
            Throw SetExceptionItem(String.Concat("Errore in StampaElementiBlocco. Codice Elemento= ", eleM.Codice), ex)
        End Try
        Return eleM.MaxPosizioneY

    End Function

    Private Function StampaElemento(ByVal e As Printing.PrintPageEventArgs, ByVal xmlItem As Xml.XmlNode, ByVal upColor As Brush, ByVal upFont As Font, Optional ByVal upPos As Dictionary(Of String, Decimal) = Nothing, Optional ByVal _RelationName As String = "") As Decimal
        Dim ElementoStampa As CLS_prn_base
        Dim ele As CLS_elemento_base
        Dim nMaxPrintedY As Decimal
        Dim aValue As ArrayList
        Dim nFormatValue As Integer
        Dim cFilterRow As String
        Dim i As Integer

        ele = Nothing
        Try
            ele = BuildElement(xmlItem, upColor, upFont, upPos)

            '
            ' Verifico se � necessario stampare l'ITEM
            '
            If DatiPerStampa.AccessiDati.EvaluateCondition(ele.XMLItem, _RelationName) And ele.DaStampare Then

                '
                ' Verifico se per l'elemento che debbo stampare � necessario recuperare dei dati dalla basedati
                ' In caso affermativo richiamo il metodo della classe opportuna
                '
                If ele.NeedValue Then

                    '
                    ' Recupero il valore per la stampa del barcode
                    '
                    If ele.SottoTipoElemento = eSottoTipoItem.Testo Or ele.SottoTipoElemento = eSottoTipoItem.TestoInvertito Or ele.SottoTipoElemento = eSottoTipoItem.Fincatura Then
                        cFilterRow = GetValueFromXML(ele.XMLItem, "ROWFilter")
                        nFormatValue = GetValueFromXML(ele.XMLItem, "FormatValue", 0)
                        ele.ValoreS = DatiPerStampa.AccessiDati.ExtractValues(ele.GetValueTAG, cFilterRow, nFormatValue, _RelationName)
                    ElseIf (ele.SottoTipoElemento = eSottoTipoItem.PreStampato) Then
                        ele.ValoreS = DatiPerStampa.AccessiDati.ExtractValues(ele.GetValueTAG, cFilterRow, nFormatValue, _RelationName)
                    ElseIf ele.SottoTipoElemento = eSottoTipoItem.BarCode Then
                        If CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_2of5 Then
                            aValue = New ArrayList
                            If ele.XMLItem.SelectNodes("values/valuesItem/value/partcodimb").Count = 0 Then
                                If ele.XMLItem.SelectNodes("values").Count > 0 Then
                                    cFilterRow = GetValueFromXML(ele.XMLItem, "ROWFilter")
                                    nFormatValue = GetValueFromXML(ele.XMLItem, "FormatValue", 0)
                                    aValue.Add(DatiPerStampa.AccessiDati.ExtractValues(ele.GetValueTAG, cFilterRow, nFormatValue, _RelationName))
                                Else
                                    If ele.XMLItem.SelectNodes("value/partcodimb").Count = 0 Then
                                        aValue.Add(DatiPerStampa.AccessiDati.ExtractValues(ele.GetValueTAG, "", nFormatValue, _RelationName))
                                    Else
                                        aValue = CodImbFillValue(ele.XMLItem.SelectNodes("value/partcodimb"))
                                    End If
                                End If
                            Else
                                aValue = CodImbFillValues(ele.XMLItem)
                            End If
                            ele.ValoreS = ""
                            For Each ci As String In aValue
                                ele.ValoreS = String.Concat(ele.ValoreS, ci)
                            Next
                            If ele.RawPrintItem.StartsWith("bc_2of5_tx") Then
                                ele = CambiaPrintItem(ele, eSottoTipoItem.Testo, upColor, upFont, upPos)
                                ele.ValoreS = ""
                                For Each ci As String In aValue
                                    ele.ValoreS = String.Concat(ele.ValoreS, ci)
                                Next
                            End If
                        ElseIf CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_DataMa Or CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_128c Or CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_QRCode Then
                            aValue = DatiPerStampa.AccessiDati.DMFillValues(CType(ele, CLS_ele_barcode).Valori_codice_DataMatrix)
                            If ele.RawPrintItem.EndsWith("_tx") Then
                                ele = CambiaPrintItem(ele, eSottoTipoItem.Testo, upColor, upFont, upPos)
                                ele.ValoreS = ""
                                For Each cTmp As String In aValue
                                    ele.ValoreS = String.Concat(ele.ValoreS, cTmp)
                                Next
                            Else
                                ele.ValoreA = aValue
                            End If
                        ElseIf CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_GS1_128 Then
                            aValue = DatiPerStampa.AccessiDati.AIFillValues(ele.XMLItem)
                            If ele.RawPrintItem.StartsWith("bc_gs1-128_tx") Then
                                ele = CambiaPrintItem(ele, eSottoTipoItem.Testo, upColor, upFont, upPos)
                                ele.ValoreS = ""
                                For Each ai As String() In aValue
                                    ele.ValoreS = String.Concat(ele.ValoreS, "(", ai(0), ")", ai(1), " ")
                                Next
                            Else
                                ele.ValoreA = aValue
                            End If
                        End If
                    End If
                End If
                If ele.SottoTipoElemento = eSottoTipoItem.PreStampato Then
                    CType(ele, CLS_ele_prestampato).FilePaginaPreStampata = ExtractPagePreStampato(ele)
                End If
                ElementoStampa = Nothing

                '
                ' In caso affermativo provvedo a creare l'oggetto necessario.
                '
                If ele.SottoTipoElemento = eSottoTipoItem.Testo Then
                    If CType(ele, CLS_ele_testo).Allineamento = eTipoAllineamento.F24 Then
                        ElementoStampa = New CLS_prn_testo_F24(e, ele)
                    ElseIf CType(ele, CLS_ele_testo).Direzione = eDirezione.Verticale Or CType(ele, CLS_ele_testo).Direzione = eDirezione.Verticale_BassoAlto Then
                        ElementoStampa = New CLS_prn_testo_Verticale(e, ele)
                        'ElementoStampa = New CLS_prn_testo_base(e, ele)
                    Else
                        ElementoStampa = New CLS_prn_testo_base(e, ele)
                    End If
                ElseIf ele.SottoTipoElemento = eSottoTipoItem.TestoInvertito Then
                    ElementoStampa = New CLS_prn_testo_Invertito(e, ele)
                ElseIf ele.SottoTipoElemento = eSottoTipoItem.Fincatura Then
                    '
                    ' La Fincatura va stampata solo in caso di stampa effettiva e non su carta, a meno che venga espressamente richiesto. 
                    ' Inoltre la fincatura va inserita solo se il numero della pagina � pari.
                    '
                    If Not DatiPerStampa.Anteprima And (DatiPerStampa.ArchiviazioneOttica Or DatiPerStampa.InserisciFincatura) And (CType(ele, CLS_ele_fincatura).StampaSempre Or (DatiPerStampa.ContatoriPagine.PagineDocumento + 1) Mod 2 = 1) Then
                        ElementoStampa = New CLS_prn_immagine(e, ele)
                    End If
                ElseIf ele.SottoTipoElemento = eSottoTipoItem.PreStampato Then
                    ElementoStampa = New CLS_prn_immagine(e, ele)
                ElseIf ele.SottoTipoElemento = eSottoTipoItem.Rettangolo Then
                    ElementoStampa = New CLS_prn_rettangolo(e, ele)
                ElseIf ele.SottoTipoElemento = eSottoTipoItem.Linea Then
                    ElementoStampa = New CLS_prn_linea(e, ele)
                ElseIf ele.SottoTipoElemento = eSottoTipoItem.Immagine Then
                    ElementoStampa = New CLS_prn_immagine(e, ele)
                ElseIf ele.SottoTipoElemento = eSottoTipoItem.Grafico1 Or ele.SottoTipoElemento = eSottoTipoItem.Grafico2 Then
                    cFilterRow = GetValueFromXML(ele.XMLItem, "ROWFilter")
                    nFormatValue = GetValueFromXML(ele.XMLItem, "FormatValue", 0)
                    If CType(ele, CLS_ele_itm_grafico).DatiDaTabella Then
                        With CType(ele, CLS_ele_itm_grafico)
                            .DataTable = DatiPerStampa.AccessiDati.RepositoryDati.DBDatiPlugIn.Tables(.NomeTabellaDati).Clone
                            CType(ele, CLS_ele_itm_grafico).DataTable.Rows.Clear()

                            Dim cRelName As String = DatiPerStampa.DBStampa.GetRelationName(.NomeTabellaDati)
                            Dim dr_array As DataRow() = DatiPerStampa.DBStampa.RecordMasterRow.GetChildRows(cRelName)
                            Dim dr As DataRow

                            For i = 0 To dr_array.Length - 1
                                dr = dr_array(i)
                                CType(ele, CLS_ele_itm_grafico).DataTable.ImportRow(dr)
                            Next
                        End With
                    Else
                        For Each xm As Xml.XmlNode In ele.XMLItem.SelectNodes("Series/serie")
                            xm.SelectSingleNode("xValue").InnerText = DatiPerStampa.AccessiDati.ExtractValues(CType(ele, CLS_ele_itm_grafico).GetValueTAGXvalue(xm.SelectSingleNode("xValue").InnerText), cFilterRow, nFormatValue, _RelationName)
                            xm.SelectSingleNode("yValue").InnerText = DatiPerStampa.AccessiDati.ExtractValues(CType(ele, CLS_ele_itm_grafico).GetValueTAGXvalue(xm.SelectSingleNode("yValue").InnerText), cFilterRow, nFormatValue, _RelationName)
                        Next
                    End If
                    ElementoStampa = New CLS_prn_grafico(e, ele)
                ElseIf ele.SottoTipoElemento = eSottoTipoItem.BarCode Then
                    If CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_128c Then
                        ElementoStampa = New CLS_prn_barcode_128c(e, ele)
                    ElseIf CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_2of5 Then
                        ElementoStampa = New CLS_prn_barcode_2of5(e, ele)
                    ElseIf CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_GS1_128 Then
                        ElementoStampa = New CLS_prn_barcode_GS1128(e, ele)
                    ElseIf CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_DataMa Then
                        ElementoStampa = New CLS_prn_barcode_DM(e, ele)
                    ElseIf CType(ele, CLS_ele_barcode).TipoBarcode = eTipoBarcode.BC_QRCode Then
                        ElementoStampa = New CLS_prn_barcode_QR(e, ele)
                    Else
                        Throw New Exception("Tipo barcode non gestito in fase di stampa")
                    End If
                End If

                If (ElementoStampa IsNot Nothing) Then
                    ElementoStampa.FattoreCorrezioneX = DatiPerStampa.FattoreCorrezione.FCS_coord_x
                    ElementoStampa.FattoreCorrezioneY = DatiPerStampa.FattoreCorrezione.FCS_coord_y
                    ElementoStampa.Stampa()
                    ele.SettaMassimaPosizione(0, ElementoStampa.MaxPrintedY)
                    'If ele.SottoTipoElemento <> eSottoTipoItem.Grafico1 And ele.SottoTipoElemento <> eSottoTipoItem.Grafico2 Then
                    nMaxPrintedY = ElementoStampa.MaxPrintedY
                    ManageNamedItem(ele.Nome, New Decimal() {0, ElementoStampa.MaxPrintedY, 1})
                    'End If
                End If
            Else
                ManageNamedItem(ele.Nome, New Decimal() {0, ele.Posizione("Y"), 0})
            End If
        Catch ex As Exception
            If ele Is Nothing Then
                Throw SetExceptionItem("Errore in StampaElemento. Codice Elemento Non Disponibile", ex)
            Else
                Throw SetExceptionItem(String.Concat("Errore in StampaElemento. Codice Elemento= ", ele.Codice), ex)
            End If
        End Try
        Return nMaxPrintedY

    End Function

    Private Function CambiaPrintItem(ByVal ele As CLS_elemento_base, ByVal newTipo As eSottoTipoItem, ByVal upColor As Brush, ByVal upFont As Font, ByVal upPos As Dictionary(Of String, Decimal)) As CLS_elemento_base
        Dim e As CLS_elemento_base

        If newTipo = eSottoTipoItem.Testo Then
            e = New CLS_ele_testo(ele, _Posizioni, upColor, upFont, upPos)
            e.ChangePrintItemTo(eSottoTipoItem.Testo)
        Else
            e = Nothing
        End If
        Return e

    End Function

    Private Sub ManageNamedItem(ByVal cNameItem As String, ByVal aValue As Decimal())

        If cNameItem > "" Then
            If _Posizioni.NamedItem.ContainsKey(cNameItem) Then
                _Posizioni.NamedItem(cNameItem) = aValue
            Else
                _Posizioni.NamedItem.Add(cNameItem, aValue)
            End If
        End If

    End Sub

    Private Function FilterTableRow(ByVal dr_array As DataRow(), ByVal dr_tbl As DataRow, ByVal xmlItem As Xml.XmlNode, Optional ByVal nRowSelected As Integer = 0) As Boolean
        Dim aData As String()
        Dim lRtn As Boolean
        Dim lOkCond As Boolean
        Dim nRowBlk As Integer
        Dim nRowTst As Integer
        Dim nRow As Integer
        Dim lExit As Boolean
        Dim dr_wrk As DataRow
        Dim lOkCond_1 As Boolean

        If xmlItem.SelectSingleNode("BLKFilter") Is Nothing Then
            lRtn = True
        Else
            aData = xmlItem.SelectSingleNode("BLKFilter").InnerText.ToString.Split(";")
            If aData(2).Contains("^") Then
                For Each cTmp As String In aData(2).Split("^")
                    lOkCond = lOkCond Or (dr_tbl.Item(aData(0)) = cTmp)
                Next
            Else
                lOkCond = dr_tbl.Item(aData(0)) = aData(2)
            End If

            If aData(1).ToLower = "ne" Then
                lOkCond = Not lOkCond
            End If
            If lOkCond And (nRowSelected <> 0) Then
                nRowSelected = Math.Abs(nRowSelected)
                ' Abilita per la stampa degli ultimi n record.
                If IsNumeric(nRowSelected) Then
                    nRow = 0
                    lExit = False
                    nRowBlk = 0
                    nRowTst = -1
                    While nRow < dr_array.Length And Not lExit
                        dr_wrk = dr_array(nRow)
                        If aData(2).Contains("^") Then
                            lOkCond_1 = False
                            For Each cTmp As String In aData(2).Split("^")
                                lOkCond_1 = lOkCond_1 Or (dr_tbl.Item(aData(0)) = cTmp)
                            Next
                        Else
                            lOkCond_1 = dr_tbl.Item(aData(0)) = aData(2)
                        End If
                        If lOkCond_1 Then
                            If ((dr_wrk.Item(0) = dr_tbl.Item(0)) And nRowTst = -1) Then
                                nRowTst = nRow
                            End If
                            If (nRowTst > -1) Then
                                nRowBlk += 1
                            End If
                        End If
                        lExit = nRowBlk > nRowSelected
                        nRow += 1
                    End While
                    lOkCond = (nRowBlk <= nRowSelected)
                End If
            End If
            lRtn = lOkCond
        End If
        Return lRtn

    End Function

#Region "Procedure per la gestione del codice di imbustamento"

    Private Function CodImbFillValues(ByVal xmlItems As Xml.XmlNode) As ArrayList
        Dim xnWork As Xml.XmlNode
        Dim xnTmp As Xml.XmlNode
        Dim nNumValue As Integer
        Dim aAIValue As ArrayList

        Try
            aAIValue = New ArrayList
            If Not xmlItems.SelectSingleNode("value") Is Nothing Then
                xnTmp = xmlItems.SelectSingleNode("value").Clone
                xmlItems.RemoveChild(xmlItems.SelectSingleNode("value"))
                xnWork = GestioneModulo.ModuloDaStampare.CreateElement("values")
                xnWork.InnerXml = String.Concat("<valuesItem>", xnTmp.OuterXml, "</valuesItem>")
                xmlItems.AppendChild(xnWork)
            End If
            If NodeExist(xmlItems, "values") Then
                nNumValue = 0
                For Each xnTmp In xmlItems.SelectNodes("values/valuesItem")
                    If DatiPerStampa.AccessiDati.EvaluateCondition(xnTmp, "") Then
                        aAIValue = CodImbFillValue(xnTmp.SelectNodes("value/partcodimb"))
                        nNumValue += 1
                    End If
                Next
            End If
        Catch ex As Exception
            aAIValue = New ArrayList
            Throw New Exception("Errore nell'estrazione del valore.", Nothing)
        End Try
        If nNumValue < 1 Then
            aAIValue = New ArrayList
            Throw New Exception("Errore Non � stato recuperato alcun valore.", Nothing)
        End If
        If nNumValue > 1 Then
            aAIValue = New ArrayList
            Throw New Exception("Errore � stato recuperato pi� di un valore.", Nothing)
        End If
        Return aAIValue

    End Function

    Private Function CodImbFillValue(ByVal xmlItems As Xml.XmlNodeList) As ArrayList
        Dim aValue As New ArrayList
        Dim cTmp1 As String
        Dim cTmp2 As String
        Dim cTmp3 As String
        Dim ci As String()
        Dim _PageDoc_Wrk As Integer
        Dim _PageTot_Wrk As Integer
        Dim nTmp As Integer
        Dim cErrorTmp As String

        Try
            For Each xmlItem_1 As Xml.XmlNode In xmlItems
                ' Da inserire la decodifica dei valori ricavabili dal db.
                cTmp1 = xmlItem_1.InnerText
                cTmp2 = cTmp1
                cTmp2 = cTmp2.Substring(cTmp2.IndexOf(";") + 1)
                cTmp2 = cTmp2.Substring(0, cTmp2.LastIndexOf(";"))
                cTmp3 = DatiPerStampa.AccessiDati.ExtractValue(cTmp2, "", 0, "")
                If cTmp2 <> cTmp3 Then
                    cTmp1 = cTmp1.Replace(cTmp2, cTmp3)
                End If

                ci = cTmp1.Split(";")
                Select Case ci(0)
                    Case Is = "STCOL"
                        _PageDoc_Wrk = DatiPerStampa.ContatoriPagine.PagineDocumento + 1
                        _PageTot_Wrk = DatiPerStampa.ContatoriPagine.PagineTotaliDocumento
                        If DatiPerStampa.FronteRetro Then
                            _PageDoc_Wrk = Int((DatiPerStampa.ContatoriPagine.PagineDocumento + 1) / 2 + 0.5)
                            _PageTot_Wrk = Int(DatiPerStampa.ContatoriPagine.PagineTotaliDocumento / 2)
                        End If
                        nTmp = 0
                        Select Case ci(2)
                            Case Is = 1
                                If _PageDoc_Wrk = _PageTot_Wrk Then
                                    nTmp = 1
                                End If
                            Case Is = 2
                                If _PageTot_Wrk = 1 Then
                                    nTmp = 3
                                Else
                                    If _PageDoc_Wrk = 1 Then
                                        nTmp = 1
                                    End If
                                    If _PageDoc_Wrk = _PageTot_Wrk Then
                                        nTmp = 2
                                    End If
                                End If
                            Case Is = 3
                                nTmp = ci(1)
                        End Select
                        aValue.Add(nTmp.ToString)
                    Case Is = "SEFED"
                        aValue.Add(DatiPerStampa.AccessiDati.ExtractValue(String.Concat("{\", DatiPerStampa.DBStampa.MainTableName, ";DEF_alimimb/}"), "", Nothing, Nothing))
                        If aValue(aValue.Count - 1) = "" Then
                            aValue(aValue.Count - 1) = 0
                        End If
                    Case Is = "MACOD"
                        aValue.Add(ci(1).PadLeft(7, "0"))
                    Case Is = "WRARO"
                        aValue.Add(Int(DatiPerStampa.WrapRound))
                    Case Is = "VALUE"
                        aValue.Add(ci(1))
                End Select
            Next
        Catch ex As Exception
            If xmlItems Is Nothing Then
                cErrorTmp = "Errore su CodImbFillValue: Elemento xmlitem vuoto"
            Else
                cErrorTmp = String.Concat("Errore su CodImbFillValue: ", ex.Message)
            End If
            Throw New Exception(cErrorTmp, ex)
        End Try
        Return aValue

    End Function

#End Region

    ''' <summary>
    ''' Imposta il cassetto da dove prelevare il foglio da stampare.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ImpostaCassettoCarta(Optional ByVal e As Printing.PrintPageEventArgs = Nothing)
        Dim cassettoSelezionato As CassettoEntity
        Dim CassettoCarta As Printing.PaperSource
        Dim nPagina As Integer
        Dim nCassetto As Integer

        _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 1, "", "ImpostaCassettoCarta", "Inizio selezione cassetto di stampa")
        If (Not DatiPerStampa.Anteprima) Then
            nPagina = DatiPerStampa.ContatoriPagine.PagineSubDocumento + 1
            _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "ImpostaCassettoCarta", "Pagina in stampa: " + nPagina.ToString())
            If (Not DatiPerStampa.FronteRetro) Or (DatiPerStampa.FronteRetro And nPagina Mod 2 = 1) Then
                nCassetto = _ModuloInStampa.ListaPagineDaStampare(DatiPerStampa.ContatoriPagine.PagineSubDocumento).CassettoCarta
                _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "ImpostaCassettoCarta", "Cassetto necessario: " + _ModuloInStampa.ListaPagineDaStampare(DatiPerStampa.ContatoriPagine.PagineSubDocumento).CassettoCarta.ToString() + " Codice cassetto: " + nCassetto.ToString())
                If DatiPerStampa.ListaCassettiUtilizzati.ContainsKey(nCassetto) Then
                    cassettoSelezionato = DatiPerStampa.ListaCassettiUtilizzati(nCassetto)
                ElseIf DatiPerStampa.ListaCassettiUtilizzati.ContainsKey(-999) Then
                    cassettoSelezionato = DatiPerStampa.ListaCassettiUtilizzati(-999)
                Else
                    cassettoSelezionato = Nothing
                End If
                If cassettoSelezionato IsNot Nothing Then
                    _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "ImpostaCassettoCarta", "Cassetto selezionato: " + cassettoSelezionato.PRT_sourcename + " " + cassettoSelezionato.PRT_rawkind.ToString())
                    CassettoCarta = New Printing.PaperSource
                    CassettoCarta.RawKind = cassettoSelezionato.PRT_rawkind
                    If e Is Nothing Then
                        Me.DefaultPageSettings.PaperSource = CassettoCarta
                    Else
                        e.PageSettings.PaperSource = CassettoCarta
                    End If
                Else
                    _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "ImpostaCassettoCarta", "Cassetto selezionato: non trovato")
                End If
            Else
                _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "ImpostaCassettoCarta", "Stampa Fronte Retro selezione cassetto non necessaria.")
            End If
        Else
            _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 2, "", "ImpostaCassettoCarta", "Selezione non avvenuta. stampa anteprima.")
        End If
        _logStampa.ScriviMessaggioLog(eLogLevel.LL_debugging, 1, "", "ImpostaCassettoCarta", "Fine selezione cassetto di stampa")

    End Sub

#Region "Elenco delle procedure generiche"

    Private Function SetExceptionItem(ByVal cErrMsg As String, ByVal ex As Exception) As Exception
        Dim ex_rtn As Exception

        ex_rtn = ex
        cErrMsg = String.Concat(cErrMsg, vbLf, ex_rtn.Message)
        While Not ex_rtn.InnerException Is Nothing
            ex_rtn = ex_rtn.InnerException
            cErrMsg = String.Concat(cErrMsg, vbLf, ex_rtn.Message)
        End While
        Return New Exception(cErrMsg, ex_rtn)

    End Function

    'Private Function GetNodoFincatura1(ByVal xnWork1 As Xml.XmlNode) As Xml.XmlNode
    '    Dim xnFinc As Xml.XmlNode
    '    Dim xnWork As Xml.XmlNode

    '    xnFinc = Nothing
    '    xnWork = xnWork1.FirstChild
    '    While Not (xnWork Is Nothing) And (xnFinc Is Nothing)
    '        If GetValueFromXML(xnWork, "PrintItemType") = "form" Then
    '            xnFinc = xnWork
    '        End If
    '        xnWork = xnWork.NextSibling
    '    End While
    '    Return xnFinc

    'End Function

#End Region

    Function ControllaCondizioni(ByVal xnItem As Xml.XmlNode, Optional ByVal cRelNameMaster As String = "", Optional ByVal cRelNameSlave As String = "") As Boolean

        Return DatiPerStampa.AccessiDati.EvaluateCondition(xnItem, cRelNameMaster, cRelNameSlave)

    End Function

    '
    ' Estrae la pagina che dovr� essere utilizzata tipo fincatura.
    '
    Private Function ExtractPagePreStampato(ByVal eleC As CLS_ele_prestampato) As String
        Dim image As System.Drawing.Image
        Dim cSrcPDFFileName As String
        Dim cExpPNGFileName As String
        Dim documemt As PdfDocument

        Try
            cSrcPDFFileName = eleC.ValoreS
            cExpPNGFileName = ConcatenaFolderFileValue(WorkFLD, "Pagina_" + eleC.PaginaPreStampato.ToString().PadLeft(4, "0") + ".png")
            documemt = New PdfDocument()
            documemt.LoadFromFile(cSrcPDFFileName)
            '
            ' il valore della pagina � numerato partendo da 0
            '
            image = documemt.SaveAsImage((eleC.PaginaPreStampato - 1), Spire.Pdf.Graphics.PdfImageType.Bitmap, 400, 400)
            image.Save(cExpPNGFileName)
            documemt.Close()
        Catch ex As Exception
            Throw New Exception("Errore Estraendo la pagina dal prestampato")
        End Try
        Return cExpPNGFileName

    End Function

    Private Function GetPagePdf(ByVal cFileName As String) As Integer
        Dim pdfReader As iTextSharp.text.pdf.PdfReader
        Dim nNumeroPagine As Integer

        Try
            pdfReader = New iTextSharp.text.pdf.PdfReader(cFileName)
            nNumeroPagine = pdfReader.NumberOfPages
        Catch ex As Exception
            Throw New Exception("Errore Estraendo il numero di pagine che compongono il prestampato.")
        End Try

        Return nNumeroPagine

    End Function

End Class
