Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione.Modulo.MMO_enumeratori

Public MustInherit Class CLS_prn_base
    Implements IPrintItem, IDisposable

    '
    ' Elemento da stampare
    '
    Shadows Property ElementoDaStampare As CLS_elemento_base

    '
    '  Posizione dove stampare l'elemento
    '
    Public Property MaxPrintedY As Decimal

    '
    ' Fattore di correzione della stampa relativa alla stampante
    '
    Public Property FattoreCorrezioneX As Decimal
    Public Property FattoreCorrezioneY As Decimal

    'Public Overridable Property ValoreS As String

    'Public Overridable WriteOnly Property ValoreA() As ArrayList

    '    Set(ByVal value As ArrayList)

    '    End Set

    'End Property

    Public ReadOnly Property PosizioneCorr(ByVal cAsse As String) As Decimal

        Get
            If cAsse = "X" Then
                Return ElementoDaStampare.Posizione(cAsse) + FattoreCorrezioneX
            ElseIf cAsse = "Y" Then
                Return ElementoDaStampare.Posizione(cAsse) + FattoreCorrezioneY
            Else
                Throw New Exception("Errore recuperando le coordinate corrette")
            End If
        End Get

    End Property

    '
    ' Oggetto per a gestione della stampa.
    '
    Private _prn As System.Drawing.Printing.PrintPageEventArgs

    '
    ' Oggetto per la stampa. Direttamente dipendente dal precedente per cui lo ricavo da quello.
    '
    Public ReadOnly Property grp As System.Drawing.Graphics

        Get
            Return _prn.Graphics
        End Get

    End Property

    Private _disposedValue As Boolean

    Public Sub New(ByRef ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_elemento_base)

        _disposedValue = False
        _prn = ev
        Me.ElementoDaStampare = ppi

    End Sub

    Public MustOverride Sub Stampa() Implements IPrintItem.Stampa

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me._disposedValue Then
            If disposing Then
                ' TODO: free unmanaged resources when explicitly called
            End If

            _prn = Nothing

        End If
        Me._disposedValue = True

    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)

    End Sub

#End Region

End Class
