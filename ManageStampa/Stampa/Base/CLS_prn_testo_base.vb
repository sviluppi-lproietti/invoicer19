Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo

Public Class CLS_prn_testo_base
    Inherits CLS_prn_base

    Private _ValoreA As ArrayList
    Private _Paragrafi As ArrayList

    Shadows ReadOnly Property Elemento As CLS_ele_testo

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_testo)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_testo)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()

        If Elemento.Allineamento = eTipoAllineamento.Giustificato Then
            StampaGiustificato()
        ElseIf Elemento.Allineamento = eTipoAllineamento.Centro2 Then
            StampaCentrato()
        Else
            StampaAltro()
        End If

    End Sub

    Private Sub StampaAltro()
        Dim sSizeWrk As SizeF
        Dim cValue As String
        Dim sSize As SizeF
        Dim nWidth As Decimal
        Dim nHeight As Decimal
        Dim nMaxPosX As Decimal
        Dim nMaxPosY As Decimal
        Dim CheckSize As String
        Dim lPrint As Boolean
        Dim CheckSizeType As Integer
        Dim rect As Rectangle
        Dim StringFmt As StringFormat
        Dim cEmptySpace As String = ""
        Dim wrkFont1 As Font
        Dim nPos As Integer
        Dim lNewLine As Boolean
        Dim cValueTmp1 As String
        Dim nWidth1 As String
        Dim nPosXStart As Decimal
        Dim nPosYstart As Decimal
        Dim nDimTmp As Decimal

        nWidth = -1
        nHeight = -1

        cValue = Elemento.ValoreS
        If Not (Elemento.XMLItem.SelectSingleNode("dimensioni") Is Nothing) Then
            nHeight = Elemento.XMLItem.SelectSingleNode("dimensioni/altezza").InnerText
            nWidth = Elemento.XMLItem.SelectSingleNode("dimensioni/larghezza").InnerText
        End If
        sSize = grp.MeasureString(cValue, Elemento.TipoCarattere)
        If Not (Elemento.XMLItem.SelectSingleNode("CheckSize") Is Nothing) Then
            CheckSizeType = Elemento.XMLItem.SelectSingleNode("CheckSize").InnerText
            CheckSize = CheckSizeType > 0
            nMaxPosX = Elemento.XMLItem.SelectSingleNode("DimensioneMax/X").InnerText
            nMaxPosY = Elemento.XMLItem.SelectSingleNode("DimensioneMax/Y").InnerText
        Else
            CheckSize = False
        End If
        If CheckSize Then
            Select Case CheckSizeType
                Case Is = 1
                    lPrint = sSize.Width <= nMaxPosX And sSize.Height <= nMaxPosY
                Case Is = 2
                    lPrint = sSize.Width <= nMaxPosX And sSize.Height <= nMaxPosY
                    While Not lPrint
                        cValue = cValue.Substring(0, cValue.Length - 1)
                        sSize = grp.MeasureString(cValue, Elemento.TipoCarattere)
                        lPrint = sSize.Width <= nMaxPosX And sSize.Height <= nMaxPosY
                    End While
            End Select
        Else
            lPrint = True
        End If
        cEmptySpace = "||"
        If Not Elemento.XMLItem.SelectSingleNode("SpazioVuotoX") Is Nothing Then
            nDimTmp = grp.MeasureString(cEmptySpace, Elemento.TipoCarattere).Width + Elemento.XMLItem.SelectSingleNode("SpazioVuotoX").InnerText
            While nDimTmp > grp.MeasureString(cEmptySpace, Elemento.TipoCarattere).Width
                cEmptySpace = cEmptySpace.Insert(1, " ")
            End While
        End If
        cValue = String.Concat(cEmptySpace.Replace("|", ""), cValue).Replace("{CR}", vbCr + vbLf)
        If (cValue.Contains("{B}") Or cValue.Contains("{I}") Or cValue.Contains("{S}")) Then
            nWidth1 = nWidth
            nPosXStart = Me.PosizioneCorr("X")
            nPosYstart = Me.PosizioneCorr("Y")
            cValue = String.Concat(cValue, "{N}")
            wrkFont1 = Elemento.TipoCarattere

            While cValue.Length > 0
                nPos = cValue.IndexOf("{")
                If nPos = 0 Then
                    wrkFont1 = GetFont(Elemento.TipoCarattere, cValue(1))
                    nPos = cValue.IndexOf("}") + 1
                Else
                    cValueTmp1 = cValue.Substring(0, nPos)

                    lNewLine = False
                    If cValueTmp1.Contains(String.Concat(vbCr, vbLf)) Then
                        If cValueTmp1.StartsWith(String.Concat(vbCr, vbLf)) Then
                            lNewLine = True
                            nPos = 0
                            cValue = cValue.Substring(2)
                            cValueTmp1 = ""
                        Else
                            nPos = cValueTmp1.IndexOf(vbCr)
                            cValueTmp1 = cValue.Substring(0, nPos)
                        End If
                    End If

                    If Not lNewLine Then
                        While (grp.MeasureString(cValueTmp1, wrkFont1).Width > nWidth - (nPosXStart - Me.PosizioneCorr("X")))
                            nPos -= (cValueTmp1.Length - cValueTmp1.LastIndexOf(" "))

                            If nPos > 0 Then
                                cValueTmp1 = cValue.Substring(0, nPos)
                            Else
                                cValueTmp1 = ""
                            End If
                            lNewLine = True
                        End While

                        If nPos > 0 Then
                            sSizeWrk = grp.MeasureString(cValueTmp1, Elemento.TipoCarattere)

                            StringFmt = New StringFormat()
                            StringFmt.Alignment = StringAlignment.Near

                            grp.DrawString(cValueTmp1, wrkFont1, Elemento.ColoreBrush, nPosXStart, nPosYstart, StringFmt)
                            nPosXStart += grp.MeasureString(cValueTmp1, wrkFont1).Width
                        Else
                            lNewLine = True
                        End If
                    End If
                    If lNewLine Then
                        If cValueTmp1 = "" Then
                            cValueTmp1 = "a"
                        End If
                        nPosYstart += grp.MeasureString(cValueTmp1, wrkFont1).Height
                        nWidth1 = nWidth
                        nPosXStart = Me.PosizioneCorr("X")
                    End If
                End If
                If nPos > 0 Then
                    cValue = cValue.Substring(nPos)
                End If

                If lNewLine And Not cValue.StartsWith(String.Concat(vbCr, vbLf)) Then
                    cValue = cValue.Trim
                End If
            End While
            Me.MaxPrintedY = nPosYstart + grp.MeasureString(cValueTmp1, wrkFont1).Height
        Else
            sSizeWrk = grp.MeasureString(cValue, Elemento.TipoCarattere)
            If nHeight = -1 And nWidth > -1 Then
                nHeight = grp.MeasureString(cValue, Elemento.TipoCarattere, New System.Drawing.SizeF(nWidth, 0)).Height
            Else
                sSize = grp.MeasureString(cValue, Elemento.TipoCarattere)
                nHeight = sSize.Height
                nWidth = sSize.Width
            End If

            Select Case Elemento.Allineamento
                Case Is = eTipoAllineamento.Sinistra
                    StringFmt = New StringFormat()
                    StringFmt.Alignment = StringAlignment.Near
                Case Is = eTipoAllineamento.Centro
                    StringFmt = New StringFormat()
                    StringFmt.Alignment = StringAlignment.Center
                Case Is = eTipoAllineamento.Destra
                    StringFmt = New StringFormat()
                    StringFmt.Alignment = StringAlignment.Far
            End Select
            If Elemento.Direzione = eDirezione.Verticale Or Elemento.Direzione = eDirezione.Verticale_BassoAlto Then
                If StringFmt Is Nothing Then
                    StringFmt = New StringFormat()
                End If
                StringFmt.FormatFlags = StringFormatFlags.DirectionVertical
            End If
            If CDec(sSizeWrk.Height) = nHeight Then
                grp.DrawString(cValue, Elemento.TipoCarattere, Elemento.ColoreBrush, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), StringFmt)
            Else
                If CType(Elemento, CLS_ele_testo).Allineamento = eTipoAllineamento.Destra Then
                    Elemento.Posizione("X") -= nWidth
                End If
                rect = New Rectangle(Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), nWidth, nHeight)
                grp.DrawString(cValue, Elemento.TipoCarattere, Elemento.ColoreBrush, rect, StringFmt)
            End If
            Me.MaxPrintedY = Elemento.Posizione("Y") + nHeight
            End If

    End Sub

    '
    ' Stampa il valore passato con allineamento giustificato.
    '
    Private Sub StampaGiustificato()
        Dim aRighe As List(Of RigaStampare)
        Dim aPartiParagrafo As ArrayList
        Dim StringFmt As StringFormat
        Dim aRiga As RigaStampare
        Dim nPosXStart As Decimal
        Dim cValueTmp1 As String
        Dim cValueTmp2 As String
        Dim nFstSpace As Integer
        Dim nTmpPosY As Decimal
        Dim aParte1 As String()
        Dim aParte2 As String()
        Dim cOldText As String
        Dim lExit As Boolean
        Dim wrkFont1 As Font
        Dim nTmp As Decimal
        Dim i As Integer
        Dim nOffsetGiust As Decimal
        Dim nOffsetPart As Decimal

        Try
            nPosXStart = Elemento.Posizione("X")
            wrkFont1 = Elemento.TipoCarattere

            '
            ' Suddivido la stringa da stampare in paragrafi in base alla presenza degli "a capo".
            '
            aRighe = New List(Of RigaStampare)
            If Elemento.LarghezzaRiga > 160 Then
                nOffsetGiust = 5
            Else
                nOffsetGiust = 0
            End If
            For Each cParagrafo As String In SuddividiInParagrafi(ElementoDaStampare.ValoreS)
                aPartiParagrafo = SpezzaStringa(cParagrafo)

                aRiga = New RigaStampare
                Elemento.Posizione("X") = 0

                i = 0
                While i < aPartiParagrafo.Count
                    aParte1 = aPartiParagrafo(i)
                    If aRiga.PartiRiga.Count > 0 Then
                        nOffsetPart = 2.5 * aRiga.PartiRiga.Count
                    Else
                        nOffsetPart = 0
                    End If
                    nTmp = CalcolaLarghezzaParte(aParte1, Elemento.TipoCarattere)
                    If Me.PosizioneCorr("X") + nTmp <= Elemento.LarghezzaRiga Then
                        aRiga.PartiRiga.Add(aParte1)
                        aRiga.Dimensioni.Add(nTmp)
                        Elemento.Posizione("X") += nTmp
                    Else
                        aRiga.Giustificare = True
                        cOldText = aParte1(0)
                        While (Me.PosizioneCorr("X") + nTmp + nOffsetPart > Elemento.LarghezzaRiga) And (aParte1(0).LastIndexOf(" ") > -1)
                            aParte1(0) = aParte1(0).Substring(0, aParte1(0).LastIndexOf(" "))
                            nTmp = CalcolaLarghezzaParte(aParte1, Elemento.TipoCarattere)
                        End While

                        If (Me.PosizioneCorr("X") + nTmp + nOffsetPart <= Elemento.LarghezzaRiga) Then
                            aRiga.PartiRiga.Add(aParte1.Clone)
                            aRiga.Dimensioni.Add(nTmp)
                            aRighe.Add(aRiga)
                        Else
                            aRighe.Add(aRiga)
                            aParte1(0) = ""
                        End If
                        aRiga = New RigaStampare
                        Elemento.Posizione("X") = 0
                        aParte1(0) = cOldText.Substring(aParte1(0).Length).Trim
                        i -= 1
                    End If
                    i += 1
                End While
                aRighe.Add(aRiga)
            Next

            '
            ' Controllo che le righe da giustificare siano composte da almeno due elementi
            '
            For Each aRiga In aRighe
                If aRiga.Giustificare And aRiga.PartiRiga.Count = 1 And Not aRiga.test Then
                    '
                    ' Si deve procedere alla divisione della riga in due parti
                    '
                    aParte1 = aRiga.PartiRiga(0).Clone
                    aParte2 = aRiga.PartiRiga(0).Clone

                    cValueTmp1 = aParte1(0)
                    cValueTmp2 = ""

                    '
                    ' Se c'� uno spazio nella stringa procedo alla divisione della stringa in due parti.
                    '
                    If cValueTmp1.Contains(" ") Then
                        nFstSpace = cValueTmp1.Length / 2

                        cValueTmp1 = aParte1(0).Substring(0, nFstSpace)
                        cValueTmp2 = aParte2(0).Substring(nFstSpace - 1)

                        '
                        ' verifico che nella prima parte della stringa ci sia almeno uno spazio
                        '
                        If cValueTmp1.Contains(" ") Then
                            While Not (cValueTmp1(nFstSpace - 1) = " " And cValueTmp2(0) = " ")
                                nFstSpace -= 1
                                cValueTmp1 = aParte1(0).Substring(0, nFstSpace)
                                cValueTmp2 = aParte2(0).Substring(nFstSpace - 1)
                            End While
                        ElseIf cValueTmp2.Contains(" ") Then
                            While Not (cValueTmp2(0) = " ")
                                nFstSpace += 1
                                cValueTmp1 = aParte1(0).Substring(0, nFstSpace)
                                cValueTmp2 = aParte2(0).Substring(nFstSpace)
                            End While
                        End If
                    End If
                    'While Not (cValueTmp1(nFstSpace - 1) = " ") And cValueTmp2(0) = " ")
                    '    nFstSpace = 1 * nSwitch
                    '    cValueTmp1 = aParte1(0).Substring(0, nFstSpace)
                    '    cValueTmp2 = aParte2(0).Substring(nFstSpace - 1)
                    'End While

                    '
                    ' Ricompongo la riga da passare alla stampa
                    '
                    aParte1(0) = cValueTmp1.Trim
                    aParte2(0) = cValueTmp2.Trim
                    aRiga.PartiRiga.Clear()
                    aRiga.Dimensioni.Clear()
                    aRiga.PartiRiga.Add(aParte1)
                    aRiga.Dimensioni.Add(CalcolaLarghezzaParte(aParte1, Elemento.TipoCarattere))
                    aRiga.PartiRiga.Add(aParte2)
                    aRiga.Dimensioni.Add(CalcolaLarghezzaParte(aParte2, Elemento.TipoCarattere))
                End If
            Next

            '
            ' Giustifica le parti delle righe in modo da ottenere un effetto corretto.
            '
            '
            For Each aRiga In aRighe
                If aRiga.Giustificare And Not aRiga.test And Not aRiga.NoSpace Then
                    i = 0
                    lExit = False
                    While (aRiga.LunghezzaRiga < Elemento.LarghezzaRiga + nOffsetGiust) And Not lExit
                        nTmp = aRiga.LunghezzaRiga - aRiga.Dimensioni(i)
                        nTmp += CalcolaLarghezzaParte(aRiga.SimulaAggiungiSpazio(i, (aRiga.PartiRiga.Count = i)), Elemento.TipoCarattere) + (nOffsetGiust / aRiga.PartiRiga.Count)
                        If (nTmp <= Elemento.LarghezzaRiga + nOffsetGiust) Then
                            aRiga.AggiungiSpazio(i, (aRiga.PartiRiga.Count = i))
                            aRiga.Dimensioni(i) = CalcolaLarghezzaParte(aRiga.PartiRiga(i), Elemento.TipoCarattere)
                            i += 1
                            If aRiga.PartiRiga.Count = i Then
                                i = 0
                            End If
                        Else
                            lExit = True
                        End If

                    End While
                End If
            Next

            '
            ' Stampa le righe dei paragrafi
            '
            For Each aRiga In aRighe
                Elemento.Posizione("X") = nPosXStart
                For i = 1 To aRiga.PartiRiga.Count
                    aParte1 = aRiga.PartiRiga(i - 1)
                    wrkFont1 = GetFont(Elemento.TipoCarattere, aParte1(1))
                    StringFmt = New StringFormat()
                    If aRiga.Giustificare Then
                        If i = aRiga.PartiRiga.Count Then
                            StringFmt.Alignment = StringAlignment.Far
                            Elemento.Posizione("X") = nPosXStart + Elemento.LarghezzaRiga
                        Else
                            StringFmt.Alignment = StringAlignment.Near
                        End If
                    Else
                        StringFmt.Alignment = StringAlignment.Near
                    End If

                    grp.DrawString(aParte1(0), wrkFont1, Elemento.ColoreBrush, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), StringFmt)
                    Elemento.Posizione("X") += grp.MeasureString(aParte1(0), wrkFont1).Width
                    nTmpPosY = Math.Max(nTmpPosY, grp.MeasureString(aParte1(0), wrkFont1).Height)
                    nTmp = Elemento.ValoreS.IndexOf(aParte1(0))
                Next
                Elemento.Posizione("Y") += nTmpPosY
            Next
            MaxPrintedY = Elemento.Posizione("Y")
        Catch ex As Exception
            Throw New Exception("PRITESTO-001: Errore stampando il testo.", ex)
        End Try

    End Sub

    '
    ' Stampa il valore passato con allineamento giustificato.
    '
    Private Sub StampaCentrato()
        Dim aRighe As List(Of RigaStampare)
        Dim aPartiParagrafo As ArrayList
        Dim StringFmt As StringFormat
        Dim aRiga As RigaStampare
        Dim nPosXStart As Decimal
        Dim cValueTmp1 As String
        Dim cValueTmp2 As String
        Dim nFstSpace As Integer
        Dim nTmpPosY As Decimal
        Dim aParte1 As String()
        Dim aParte2 As String()
        Dim cOldText As String
        Dim lExit As Boolean
        Dim wrkFont1 As Font
        Dim nTmp As Decimal
        Dim i As Integer
        Dim nOffsetGiust As Decimal
        Dim nOffsetPart As Decimal

        Try
            nPosXStart = Elemento.Posizione("X")
            wrkFont1 = Elemento.TipoCarattere

            '
            ' Suddivido la stringa da stampare in paragrafi in base alla presenza degli "a capo".
            '
            aRighe = New List(Of RigaStampare)
            If Elemento.LarghezzaRiga > 160 Then
                nOffsetGiust = 5
            Else
                nOffsetGiust = 0
            End If
            For Each cParagrafo As String In SuddividiInParagrafi(ElementoDaStampare.ValoreS)
                aPartiParagrafo = SpezzaStringa(cParagrafo)

                aRiga = New RigaStampare
                Elemento.Posizione("X") = 0

                i = 0
                While i < aPartiParagrafo.Count
                    aParte1 = aPartiParagrafo(i)
                    If aRiga.PartiRiga.Count > 0 Then
                        nOffsetPart = 2.5 * aRiga.PartiRiga.Count
                    Else
                        nOffsetPart = 0
                    End If
                    nTmp = CalcolaLarghezzaParte(aParte1, Elemento.TipoCarattere)
                    If Me.PosizioneCorr("X") + nTmp <= Elemento.LarghezzaRiga Then
                        aRiga.PartiRiga.Add(aParte1)
                        aRiga.Dimensioni.Add(nTmp)
                        Elemento.Posizione("X") += nTmp
                    Else
                        aRiga.Giustificare = True
                        cOldText = aParte1(0)
                        While (Me.PosizioneCorr("X") + nTmp + nOffsetPart > Elemento.LarghezzaRiga) And (aParte1(0).LastIndexOf(" ") > -1)
                            aParte1(0) = aParte1(0).Substring(0, aParte1(0).LastIndexOf(" "))
                            nTmp = CalcolaLarghezzaParte(aParte1, Elemento.TipoCarattere)
                        End While

                        If (Me.PosizioneCorr("X") + nTmp + nOffsetPart <= Elemento.LarghezzaRiga) Then
                            aRiga.PartiRiga.Add(aParte1.Clone)
                            aRiga.Dimensioni.Add(nTmp)
                            aRighe.Add(aRiga)
                        Else
                            aRighe.Add(aRiga)
                            aParte1(0) = ""
                        End If
                        aRiga = New RigaStampare
                        Elemento.Posizione("X") = 0
                        aParte1(0) = cOldText.Substring(aParte1(0).Length).Trim
                        i -= 1
                    End If
                    i += 1
                End While
                aRighe.Add(aRiga)
            Next

            '
            ' Stampa le righe dei paragrafi
            '
            For Each aRiga In aRighe
                Elemento.Posizione("X") = nPosXStart
                i = 1
                aParte1 = aRiga.PartiRiga(i - 1)
                wrkFont1 = GetFont(Elemento.TipoCarattere, aParte1(1))
                StringFmt = New StringFormat()
                StringFmt.Alignment = StringAlignment.Center

                grp.DrawString(aParte1(0), wrkFont1, Elemento.ColoreBrush, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), StringFmt)
                Elemento.Posizione("X") += grp.MeasureString(aParte1(0), wrkFont1).Width
                nTmpPosY = Math.Max(nTmpPosY, grp.MeasureString(aParte1(0), wrkFont1).Height)
                nTmp = Elemento.ValoreS.IndexOf(aParte1(0))
                Elemento.Posizione("Y") += nTmpPosY
            Next
            MaxPrintedY = Elemento.Posizione("Y")
        Catch ex As Exception
            Throw New Exception("PRITESTO-001: Errore stampando il testo.", ex)
        End Try

    End Sub
#Region "Funzioni di supporto"

    Private Function CalcolaLarghezzaParte(ByVal aParte As String(), ByVal wrkFont As Font) As Decimal
        Dim wrkFont1 As Font

        wrkFont1 = GetFont(wrkFont, aParte(1))
        Return grp.MeasureString(aParte(0), wrkFont1).Width

    End Function

    Private Function SistemaStringa(ByVal cValue As String, ByVal cAddSpace As String, ByVal lBefore As Boolean) As String
        Dim nFstSpace As Integer
        Dim nNumSpace As Integer
        Dim i As Integer

        i = cValue.Length
        While (cAddSpace.Length > 0)
            If i = cValue.Length Then
                nNumSpace = cValue.Trim.Length - cValue.Replace(" ", "").Trim.Length
                i = (cValue.Length - cValue.Trim.Length)
            End If
            If nNumSpace = 0 Then
                If lBefore Then
                    cValue = String.Concat(cAddSpace, cValue)
                Else
                    cValue = String.Concat(cValue, cAddSpace)
                End If
                cAddSpace = ""
            Else
                nFstSpace = cValue.IndexOf(" ", i)
                If nFstSpace = -1 Then
                    i = cValue.Length
                Else
                    cValue = cValue.Insert(nFstSpace, " ")
                    cAddSpace = cAddSpace.Substring(1)
                    i = nFstSpace + 1
                    i += 1
                End If
            End If
        End While
        Return cValue

    End Function

    Private Function SpezzaStringa(ByVal cValue As String) As ArrayList
        Dim aRtn As ArrayList
        Dim cValueTmp1 As String
        Dim cFormat As String

        aRtn = New ArrayList
        If cValue.Contains("{") Then
            cFormat = ""
            While cValue.Length > 0
                cValueTmp1 = cValue.Clone
                If cValueTmp1.StartsWith("{") Then
                    If cFormat.Contains(cValueTmp1(1)) Then
                        cFormat = cFormat.Replace(cValueTmp1(1), "")
                    Else
                        cFormat = String.Concat(cFormat, cValueTmp1(1))
                    End If
                    cValue = cValue.Substring(cValueTmp1.IndexOf("}") + 1)
                Else
                    If cValueTmp1.IndexOf("{") > -1 Then
                        aRtn.Add(New String() {cValueTmp1.Substring(0, cValueTmp1.IndexOf("{")), cFormat, ""})
                        cValue = cValue.Substring(cValueTmp1.IndexOf("{"))
                    Else
                        aRtn.Add(New String() {cValueTmp1, cFormat, ""})
                        cValue = ""
                    End If
                End If
            End While
        Else
            aRtn.Add(New String() {cValue, "", ""})
        End If

        Return aRtn

    End Function

    '
    ' Questa funzione suddivide la stringa da stampare in paragrafi
    '
    Private Function SuddividiInParagrafi(ByVal cValue As String) As ArrayList
        Dim aParagrafi As ArrayList
        Dim cTmp As String

        '
        ' Standardizza gli a capo con {CR}
        '
        cValue = cValue.Replace(vbLf, "{CR}")
        cValue = String.Concat(cValue, "{CR}")
        aParagrafi = New ArrayList
        While cValue.Contains("{CR}")
            cTmp = cValue.Substring(0, cValue.IndexOf("{CR}"))
            aParagrafi.Add(cTmp)
            cValue = cValue.Substring(cValue.IndexOf("{CR}") + 4)
        End While
        Return aParagrafi

    End Function

    Private Function GetFont(ByVal xmlItem As Xml.XmlNode, ByVal _UpFont As Font) As Font
        Dim tmpFont As Font
        Dim xmlItemTMP As Xml.XmlNode
        Dim xmlFont As Xml.XmlNode
        Dim cFont As String
        Dim nSize As Integer
        Dim FntStyle As New FontStyle
        Dim lBold As Boolean = False
        Dim lItalic As Boolean = False
        Dim lStrikeout As Boolean = False
        Dim lUnderline As Boolean = False

        If (xmlItem.SelectSingleNode("Font") Is Nothing) Then
            tmpFont = _UpFont
        Else
            xmlFont = xmlItem.SelectSingleNode("Font")
            If Not _UpFont Is Nothing Then
                cFont = _UpFont.FontFamily.Name
                nSize = _UpFont.Size
                lBold = _UpFont.Bold
                lItalic = _UpFont.Italic
                lStrikeout = _UpFont.Strikeout
                lUnderline = _UpFont.Underline
            End If

            For Each xmlItemTMP In xmlFont.ChildNodes
                Select Case xmlItemTMP.Name.ToLower
                    Case Is = "nome"
                        cFont = xmlItemTMP.InnerText
                    Case Is = "size"
                        nSize = xmlItemTMP.InnerText
                    Case Is = "bold"
                        lBold = xmlItemTMP.InnerText = 1
                    Case Is = "italic"
                        lItalic = xmlItemTMP.InnerText = 1
                    Case Is = "strikeout"
                        lStrikeout = xmlItemTMP.InnerText = 1
                    Case Is = "underline"
                        lUnderline = xmlItemTMP.InnerText = 1
                End Select
            Next

            ' Imposta lo stile del fnt a normale.
            FntStyle = FontStyle.Regular
            If lBold Then
                FntStyle = FntStyle Or FontStyle.Bold
            End If
            If lItalic Then
                FntStyle = FntStyle Or FontStyle.Italic
            End If
            If lStrikeout Then
                FntStyle = FntStyle Or FontStyle.Strikeout
            End If
            If lUnderline Then
                FntStyle = FntStyle Or FontStyle.Underline
            End If
            tmpFont = New Font(cFont, nSize, FntStyle)
        End If
        Return tmpFont

    End Function

    Private Function GetFont(ByVal _UpFont As Font, ByVal cFontsElem As String) As Font
        Dim tmpFont As Font

        tmpFont = _UpFont
        For Each cFont As String In cFontsElem
            Select Case cFont
                Case Is = "B"
                    If tmpFont.Bold Then
                        tmpFont = New Font(_UpFont.FontFamily, _UpFont.Size, _UpFont.Style And Not FontStyle.Bold)
                    Else
                        tmpFont = New Font(_UpFont.FontFamily, _UpFont.Size, _UpFont.Style Or FontStyle.Bold)
                    End If
                Case Is = "I"
                    If tmpFont.Italic Then
                        tmpFont = New Font(_UpFont.FontFamily, _UpFont.Size, _UpFont.Style And Not FontStyle.Italic)
                    Else
                        tmpFont = New Font(_UpFont.FontFamily, _UpFont.Size, _UpFont.Style Or FontStyle.Italic)
                    End If
                Case Is = "S"
                    If tmpFont.Underline Then
                        tmpFont = New Font(_UpFont.FontFamily, _UpFont.Size, _UpFont.Style And Not FontStyle.Underline)
                    Else
                        tmpFont = New Font(_UpFont.FontFamily, _UpFont.Size, _UpFont.Style Or FontStyle.Underline)
                    End If
            End Select
        Next
        Return tmpFont

    End Function

#End Region

    Private Class RigaStampare

        Property PartiRiga As ArrayList
        Property Dimensioni As ArrayList
        Property Giustificare As Boolean
        Property test As Boolean
        ReadOnly Property NoSpace As Boolean

            Get
                Dim lNoSpace As Boolean

                lNoSpace = True
                For Each cTmp As String() In PartiRiga
                    lNoSpace = lNoSpace And Not cTmp(0).Contains(" ")
                Next
                Return lNoSpace
            End Get

        End Property

        Public Sub New()

            PartiRiga = New ArrayList
            Dimensioni = New ArrayList
            Giustificare = False
            test = False

        End Sub

        Public Function LunghezzaRiga() As Decimal
            Dim nTotale As Decimal
            Dim nTmp As Decimal

            nTotale = 0
            For Each nTmp In Dimensioni
                nTotale += nTmp
            Next
            Return nTotale

        End Function

        Public Function SimulaAggiungiSpazio(ByVal nPosizione As Integer, ByVal lBeforeAfter As Boolean) As String()
            Dim aTmp As String()

            aTmp = PartiRiga(nPosizione)
            aTmp(0) = InserisciSpazio(PartiRiga(nPosizione)(0), lBeforeAfter)
            Return aTmp

        End Function

        Public Sub AggiungiSpazio(ByVal nPosizione As Integer, ByVal lBeforeAfter As Boolean)

            PartiRiga(nPosizione)(0) = InserisciSpazio(PartiRiga(nPosizione)(0), lBeforeAfter)

        End Sub

        Public Function InserisciSpazio(ByVal cAppo As String, ByVal lBeforeAfterSpace As Boolean) As String
            Dim cTmp As String
            Dim cSpazi As String
            Dim nStartIdx As Integer

            cTmp = cAppo
            If cTmp.Contains(" ") Then
                '
                ' Recupera il numero massimo di spazi consecutivi attuali
                '
                cSpazi = " "
                While cTmp.IndexOf(String.Concat(cSpazi, " ")) > -1
                    cSpazi = String.Concat(cSpazi, " ")
                End While

                If cTmp.Substring(cTmp.LastIndexOf(cSpazi) + cSpazi.Length).IndexOf(" ") = -1 Then
                    cSpazi = String.Concat(cSpazi, " ")
                    nStartIdx = cTmp.IndexOf(" ")
                Else
                    nStartIdx = cTmp.LastIndexOf(cSpazi) + cSpazi.Length + cTmp.Substring(cTmp.LastIndexOf(cSpazi) + cSpazi.Length).IndexOf(" ")
                End If
                cTmp = cTmp.Insert(nStartIdx, " ")
            Else
                If lBeforeAfterSpace Then
                    cTmp = String.Concat(" ", cTmp)
                Else
                    cTmp = String.Concat(cTmp, " ")
                End If
            End If
            Return cTmp

        End Function

    End Class

End Class
