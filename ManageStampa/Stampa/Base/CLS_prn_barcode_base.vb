Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione.Modulo.MMO_enumeratori

Public Class CLS_prn_barcode_base
    Inherits CLS_prn_base

    Private _disposedValue As Boolean
    Private _ValoreS As String
    Private _ValoreA As ArrayList

    Shadows Property _BarreDecodeTable As ArrayList

    Shadows ReadOnly Property Elemento As CLS_ele_barcode

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_barcode)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_barcode)
        MyBase.New(ev, ppi)

    End Sub

    Public Shadows Function GetPenna()

        Return New Pen(System.Drawing.Color.Black, Elemento.SpessoreBarCode)

    End Function

    Public Overrides Sub Stampa()

    End Sub

End Class
