Imports System.Drawing
Imports ManageGrafico
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione.Modulo.MMO_enumeratori

Public Class CLS_prn_grafico
    Inherits CLS_prn_base

    Private _BMP_image As Bitmap
    Private _disposedValue As Boolean
    Private _grafico As ManageGrafico.Grafico

    Shadows ReadOnly Property Elemento As CLS_ele_itm_grafico

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_itm_grafico)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_itm_grafico)
        MyBase.New(ev, ppi)

        _grafico = New ManageGrafico.Grafico
        _grafico.UseCustomPalette = ppi.UseCustomPalette
        If ppi.UseCustomPalette Then
            _grafico.CustomPalette = ppi.customPalette
        Else
            _grafico.Palette = ppi.Palette
        End If
        _grafico.TipoLegendaGrafico = ppi.TipoLegendaGrafico
        _grafico.TipoGrafico = ppi.TipoGrafico
        _grafico.Larghezza = ppi.DimensioniX
        _grafico.Altezza = ppi.DimensioniY
        _grafico.Series = ppi.Series
        _grafico.Tabella = ppi.DataTable

    End Sub

    Public Overrides Sub Stampa()
        Dim rect As Rectangle

        rect = New Rectangle(Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), Elemento.DimensioniX, Elemento.DimensioniY)
        _BMP_image = _grafico.EncodeBmpGrafico()
        grp.DrawImage(_BMP_image, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"))
        MaxPrintedY = Me.PosizioneCorr("Y") + 55

    End Sub

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me._disposedValue Then
            If disposing Then
                ' TODO: free unmanaged resources when explicitly called
            End If

            If _BMP_image IsNot Nothing Then
                _BMP_image.Dispose()
                _BMP_image = Nothing
            End If
            'prn.Graphics.Dispose()
        End If
        Me._disposedValue = True

    End Sub

End Class
