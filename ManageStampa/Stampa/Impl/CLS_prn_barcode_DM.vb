Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML
Imports iTextSharp.text
Imports iTextSharp.text.pdf

Public Class CLS_prn_barcode_DM
    Inherits CLS_prn_barcode_base

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_barcode)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()
        Dim options As DataMatrix.net.DmtxImageEncoderOptions
        Dim encoder As DataMatrix.net.DmtxImageEncoder
        Dim barcodeDatamatrix As BarcodeDatamatrix
        Dim BMP_image As System.Drawing.Bitmap
        Dim nDotSize As Integer
        Dim nHeight As Single
        Dim lResize As Boolean
        Dim cValue As String
        Dim nWidth As Single
        Dim cTmp As String

        Try
            cValue = ""
            For Each cTmp In Elemento.BarCodeValue
                cValue = String.Concat(cValue, cTmp)
            Next
            nDotSize = GetValueFromXML(Elemento.XMLItem, "dotsize", "5")
            cTmp = GetValueFromXML(Elemento.XMLItem, "resize", "")
            lResize = cTmp > ""

            If Elemento.Datamatrix_Tipo = "52x52" Then
                barcodeDatamatrix = New BarcodeDatamatrix()
                barcodeDatamatrix.Generate(cValue)
                BMP_image = barcodeDatamatrix.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White)
            Else
                encoder = New DataMatrix.net.DmtxImageEncoder
                If nDotSize <> 5 Then
                    Using bmptmp As Bitmap = encoder.EncodeImage(cValue, nDotSize)
                        BMP_image = bmptmp.Clone
                        bmptmp.Dispose()
                    End Using
                Else
                    options = New DataMatrix.net.DmtxImageEncoderOptions
                    options.ModuleSize = Elemento.Datamatrix_DimensioniModulo
                    options.MarginSize = Elemento.Datamatrix_DimensioniMargini
                    options.BackColor = System.Drawing.Color.White
                    options.ForeColor = System.Drawing.Color.Black
                    Select Case Elemento.Datamatrix_Tipo
                        Case Is = "16x48"
                            options.SizeIdx = DataMatrix.net.DmtxSymbolSize.DmtxSymbol16x48
                        Case Is = "44x44"
                            options.SizeIdx = DataMatrix.net.DmtxSymbolSize.DmtxSymbol44x44
                            'Case Is = "52x52"
                            '    options.SizeIdx = DataMatrix.net.DmtxSymbolSize.DmtxSymbol52x52
                        Case Else
                            options.SizeIdx = DataMatrix.net.DmtxSymbolSize.DmtxSymbol16x16
                    End Select
                    Using bmptmp As Bitmap = encoder.EncodeImage(cValue, options)
                        BMP_image = bmptmp.Clone
                        bmptmp.Dispose()
                    End Using
                End If
                encoder = Nothing
            End If
            If lResize Then
                nWidth = cTmp.Split(";")(0)
                nHeight = cTmp.Split(";")(1)
                grp.DrawImage(BMP_image, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), nWidth, nHeight)
            Else
                grp.DrawImage(BMP_image, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"))
            End If
        Catch ex As Exception
            Throw New Exception("PRIBCDAMA-001: Errore stampando il barcode.", ex)
        End Try

    End Sub

End Class
