Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione.Modulo.MMO_enumeratori

Public Class CLS_prn_linea
    Inherits CLS_prn_base

    Shadows ReadOnly Property Elemento As CLS_ele_linea

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_linea)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_linea)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()
        Dim penna As Pen

        penna = New Pen(Elemento.ColorePenna, Elemento.DimensionePenna)
        grp.DrawLine(penna, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), Me.PosizioneCorr("X") + Elemento.Lunghezza, Me.PosizioneCorr("Y") + Elemento.Altezza)

    End Sub

End Class