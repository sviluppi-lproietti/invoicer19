Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo

Public Class CLS_prn_immagine
    Inherits CLS_prn_base

    Private BMP_image As Bitmap
    Private _ImmaginiFLD As String

    Shadows ReadOnly Property Elemento As CLS_elemento_base

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_elemento_base)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_elemento_base)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()
        Dim IsResized As Boolean
        Dim cImageFil As String
        Dim nResizedX As Single
        Dim nResizedY As Single

        Try
            If (Elemento.GetType().ToString.EndsWith("CLS_ele_fincatura")) Then
                cImageFil = CType(Elemento, CLS_ele_fincatura).FincaturaFile()
                IsResized = False
            ElseIf (Elemento.GetType().ToString.EndsWith("CLS_ele_immagine")) Then
                With CType(Elemento, CLS_ele_immagine)
                    cImageFil = .ImmagineFile
                    IsResized = .IsResized
                    If IsResized Then
                        nResizedX = .ResizedX
                        nResizedY = .ResizedY
                    End If
                End With
            ElseIf (Elemento.GetType().ToString.EndsWith("CLS_ele_prestampato")) Then
                cImageFil = CType(Elemento, CLS_ele_prestampato).FilePaginaPreStampata
            Else
                cImageFil = ""
            End If

            If System.IO.File.Exists(cImageFil) Then
                Using bmptmp As System.Drawing.Image = System.Drawing.Image.FromFile(cImageFil)
                    BMP_image = bmptmp.Clone
                    bmptmp.Dispose()
                End Using

                If IsResized Then
                    grp.DrawImage(BMP_image, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), nResizedX, nResizedY)
                Else
                    grp.DrawImage(BMP_image, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"))
                End If
            End If
        Catch ex As Exception
            Throw New Exception("PRIIMAGE-001: Errore stampando una immagine.", ex)
        End Try

    End Sub

End Class