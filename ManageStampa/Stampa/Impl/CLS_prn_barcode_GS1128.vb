Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione.Modulo.MMO_enumeratori

Public Class CLS_prn_barcode_GS1128
    Inherits CLS_prn_barcode_base

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_barcode)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()

        Try
            Dim aBarCodeValue As New ArrayList
            Dim PositionY As Decimal
            Dim PositionX As Decimal
            Dim lVariable As Boolean
            Dim cTmp As String
            Dim nTmp As Integer
            Dim i As Integer
            Dim ai As String()
            Dim aTmp As String()
            Dim lBlack As Boolean
            Dim Penna As Pen

            '
            '
            '
            Penna = GetPenna()

            '
            '
            '
            BuildBarreDecodeTable()

            '
            ' predisposizione della stringa per il calcolo della check digit:
            '
            aBarCodeValue.Add("SC")
            aBarCodeValue.Add("F1")
            For i = 0 To Elemento.BarCodeValue.Count - 1
                ai = Elemento.BarCodeValue(i)
                lVariable = False
                If ai(0) = 8020 Then
                    lVariable = True
                End If
                If ai(0).Substring(0, 3) = 390 Then
                    lVariable = True
                End If
                nTmp = 0
                cTmp = String.Concat(ai(0), ai(1))
                While nTmp < cTmp.Length
                    aBarCodeValue.Add(cTmp.Substring(nTmp, 2))
                    nTmp += 2
                End While
                If lVariable And i <> Elemento.BarCodeValue.Count - 1 Then
                    aBarCodeValue.Add("F1")
                End If
            Next
            Dim nTotCD As Integer = 0
            Dim nMolt As Integer
            nTmp = 0
            For Each cTmp In aBarCodeValue
                Select Case cTmp
                    Case Is = "F1"
                        nMolt = 102
                    Case Is = "SC"
                        nMolt = 105
                    Case Else
                        nMolt = cTmp
                End Select
                If nTmp = 0 Then
                    nTotCD += nMolt
                Else
                    nTotCD += nMolt * nTmp
                End If
                nTmp += 1
            Next
            aBarCodeValue.Add(nTotCD - Int(nTotCD / 103) * 103)
            aBarCodeValue.Add("ALT")
            For nTmp = 0 To aBarCodeValue.Count - 1
                If aBarCodeValue(nTmp).ToString = "ALT" Then
                    aBarCodeValue(nTmp) = 106
                End If
                If aBarCodeValue(nTmp).ToString = "F1" Then
                    aBarCodeValue(nTmp) = 102
                End If
                If aBarCodeValue(nTmp).ToString = "SC" Then
                    aBarCodeValue(nTmp) = 105
                End If
            Next
            If Elemento.Direzione = eDirezione.Orizzontale Then
                PositionX = Me.PosizioneCorr("X")
                For Each cTmp In aBarCodeValue
                    aTmp = _BarreDecodeTable(CInt(cTmp))
                    lBlack = True
                    For Each nTmp In aTmp
                        For i = 1 To nTmp
                            If lBlack Then
                                grp.DrawLine(Penna, PositionX, Me.PosizioneCorr("Y"), PositionX, Me.PosizioneCorr("Y") + Elemento.AltezzaBarCode)
                            End If
                            PositionX += Elemento.SpessoreBarCode
                        Next
                        lBlack = Not lBlack
                    Next
                Next
            End If
            If Elemento.Direzione = eDirezione.Verticale Then
                PositionY = Me.PosizioneCorr("Y")
                For Each cTmp In aBarCodeValue
                    aTmp = _BarreDecodeTable(CInt(cTmp))
                    lBlack = True
                    For Each nTmp In aTmp
                        For i = 1 To nTmp
                            If lBlack Then
                                'grp.DrawLine(Penna, PositionX, Me.PosizioneCorr("Y"), PositionX, Me.PosizioneCorr("Y") + Elemento.AltezzaBarCode)
                                grp.DrawLine(Penna, Me.PosizioneCorr("X"), PositionY, Me.PosizioneCorr("X") + Elemento.AltezzaBarCode, PositionY)
                            End If
                            PositionY += Elemento.SpessoreBarCode
                        Next
                        lBlack = Not lBlack
                    Next
                Next
            End If

        Catch ex As Exception
            Throw New Exception("PRIBCGS1128-001: Errore stampando il barcode.", ex)
        End Try

    End Sub

    Private Sub BuildBarreDecodeTable()

        _BarreDecodeTable = New ArrayList
        _BarreDecodeTable.Add(New String() {2, 1, 2, 2, 2, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 2, 1, 2, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 2, 2, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 2, 2, 3})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 3, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 3, 1, 2, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 2, 1, 3})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 3, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 3, 2, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 2, 1, 3})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 3, 1, 2})
        _BarreDecodeTable.Add(New String() {2, 3, 1, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 2, 3, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 1, 3, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 2, 3, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 3, 2, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 3, 1, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 3, 2, 2, 1})
        _BarreDecodeTable.Add(New String() {2, 2, 3, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 1, 3, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 2, 3, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 3, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 3, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {3, 1, 2, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 1, 2, 2, 2})
        _BarreDecodeTable.Add(New String() {3, 2, 1, 1, 2, 2})
        _BarreDecodeTable.Add(New String() {3, 2, 1, 2, 2, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 2, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {3, 2, 2, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {3, 2, 2, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 2, 1, 2, 3})
        _BarreDecodeTable.Add(New String() {2, 1, 2, 3, 2, 1})
        _BarreDecodeTable.Add(New String() {2, 3, 2, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 3, 2, 3})
        _BarreDecodeTable.Add(New String() {1, 3, 1, 1, 2, 3})
        _BarreDecodeTable.Add(New String() {1, 3, 1, 3, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 3, 1, 3})
        _BarreDecodeTable.Add(New String() {1, 3, 2, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {1, 3, 2, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 3, 1, 3})
        _BarreDecodeTable.Add(New String() {2, 3, 1, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {2, 3, 1, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 1, 3, 3})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 3, 3, 1})
        _BarreDecodeTable.Add(New String() {1, 3, 2, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 3, 1, 2, 3})
        _BarreDecodeTable.Add(New String() {1, 1, 3, 3, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 3, 3, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 3, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 3, 3, 1})
        _BarreDecodeTable.Add(New String() {2, 3, 1, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 3, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {2, 1, 3, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 3, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 1, 1, 2, 3})
        _BarreDecodeTable.Add(New String() {3, 1, 1, 3, 2, 1})
        _BarreDecodeTable.Add(New String() {3, 3, 1, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 2, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {3, 1, 2, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {3, 3, 2, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 4, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 4, 1, 1})
        _BarreDecodeTable.Add(New String() {4, 3, 1, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 2, 2, 4})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 4, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 1, 2, 4})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 4, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 4, 1, 1, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 4, 1, 2, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 2, 1, 4})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 4, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 1, 1, 4})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 4, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 4, 2, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 4, 2, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 4, 1, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 1, 1, 4})
        _BarreDecodeTable.Add(New String() {4, 1, 3, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 4, 1, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 3, 4, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 2, 4, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 1, 4, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 2, 4, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 4, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 4, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 4, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {4, 1, 1, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {4, 2, 1, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {4, 2, 1, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 2, 1, 4, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 4, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {4, 1, 2, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 1, 4, 3})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 3, 4, 1})
        _BarreDecodeTable.Add(New String() {1, 3, 1, 1, 4, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 4, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {1, 1, 4, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {4, 1, 1, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {4, 1, 1, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 3, 1, 4, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 4, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 1, 1, 4, 1})
        _BarreDecodeTable.Add(New String() {4, 1, 1, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 4, 1, 2})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 2, 1, 4})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 2, 3, 2})
        _BarreDecodeTable.Add(New String() {2, 3, 3, 1, 1, 1, 2})

    End Sub

End Class
