Imports System.Drawing
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione.Modulo.MMO_enumeratori

Public Class CLS_prn_testo_F24
    Inherits CLS_prn_testo_base

    Private _ValoreA As ArrayList
    Private _Paragrafi As ArrayList

    Shadows ReadOnly Property Elemento As CLS_ele_testo

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_testo)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_testo)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()
        Dim nPosti As Integer
        Dim nSize As Decimal
        Dim nWidth As Decimal = -1
        Dim nHeight As Decimal = -1
        Dim StringFmt As StringFormat
        Dim cValueTmp1 As String
        Dim nPosXStart As Decimal
        Dim cValueTmp2 As String
        Dim i As Integer

        cValueTmp1 = GetValueFromXML(Elemento.XMLItem, "F24", "")
        nPosti = cValueTmp1.Split(";")(0)
        nSize = cValueTmp1.Split(";")(1)
        Dim cRemove As String = cValueTmp1.Split(";")(2)
        cValueTmp1 = Elemento.ValoreS
        If cValueTmp1.Length > nPosti Then
            cValueTmp1 = cValueTmp1.Substring(0, nPosti)
        End If

        nPosXStart = Elemento.Posizione("X")
        StringFmt = New StringFormat()
        StringFmt.Alignment = StringAlignment.Near
        cValueTmp2 = ""

        For i = 1 To cValueTmp1.Length
            grp.DrawString(cValueTmp1.Substring(i - 1, 1), Elemento.TipoCarattere, Elemento.ColoreBrush, nPosXStart + Me.FattoreCorrezioneX, Me.PosizioneCorr("Y"), StringFmt)
            nPosXStart += nSize
            If cRemove.Contains(String.Concat(".", i, ".")) Then
                nPosXStart -= 1
            End If
        Next

    End Sub

End Class
