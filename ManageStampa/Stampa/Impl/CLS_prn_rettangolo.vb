Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo

Public Class CLS_prn_rettangolo
    Inherits CLS_prn_base

    Private _disposedValue As Boolean

    Shadows ReadOnly Property Elemento As CLS_ele_rettangolo

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_rettangolo)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_rettangolo)
        MyBase.New(ev, ppi)

        _disposedValue = False

    End Sub

    Public Overrides Sub Stampa()
        Dim penna As Pen

        If Elemento.RettangoloPieno Then
            grp.FillRectangle(New SolidBrush(Elemento.ColorePenna), Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), Elemento.Larghezza, Elemento.Altezza)
        ElseIf Elemento.RettangoloSoloBordi Then
            penna = New Pen(Elemento.ColorePenna, Elemento.DimensionePenna)
            If Elemento.BordiRotondi Then
                '
                ' Disegna rettangolo con angoli arrotondati
                '
                Dim m_intxAxis As Decimal = Me.PosizioneCorr("X")
                Dim m_intyAxis As Decimal = Me.PosizioneCorr("Y")
                Dim m_intWidth As Decimal = Elemento.Larghezza
                Dim m_intHeight As Decimal = Elemento.Altezza
                Dim m_diameter As Decimal = 7
                Dim BaseRect As New RectangleF(m_intxAxis, m_intyAxis, m_intWidth, m_intHeight)
                Dim ArcRect As New RectangleF(BaseRect.Location, New SizeF(m_diameter, m_diameter))

                With grp
                    '
                    ' top left Arc
                    '
                    .DrawArc(penna, ArcRect, 180, 90)
                    .DrawLine(penna, m_intxAxis + (m_diameter / 2), m_intyAxis, m_intxAxis + m_intWidth - (m_diameter / 2), m_intyAxis)

                    '
                    ' top right arc
                    '
                    ArcRect.X = BaseRect.Right - m_diameter
                    .DrawArc(penna, ArcRect, 270, 90)
                    .DrawLine(penna, m_intxAxis + m_intWidth, m_intyAxis + (m_diameter / 2), m_intxAxis + m_intWidth, m_intyAxis + m_intHeight - (m_diameter / 2))

                    '
                    ' bottom right arc
                    '
                    ArcRect.Y = BaseRect.Bottom - m_diameter
                    .DrawArc(penna, ArcRect, 0, 90)
                    .DrawLine(penna, m_intxAxis + (m_diameter / 2), m_intyAxis + m_intHeight, m_intxAxis + m_intWidth - (m_diameter / 2), m_intyAxis + m_intHeight)

                    '
                    ' bottom left arc
                    '
                    ArcRect.X = BaseRect.Left
                    .DrawArc(penna, ArcRect, 90, 90)
                    .DrawLine(penna, m_intxAxis, m_intyAxis + (m_diameter / 2), m_intxAxis, m_intyAxis + m_intHeight - (m_diameter / 2))
                End With
            ElseIf Elemento.BordiSquadrati Then
                grp.DrawRectangle(penna, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), Elemento.Larghezza, Elemento.Altezza)
            End If
        End If
        Me.MaxPrintedY = Elemento.Posizione("Y") + Elemento.Altezza

    End Sub

End Class
