Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo

Public Class CLS_prn_barcode_2of5
    Inherits CLS_prn_barcode_base

    Const nWide = 3
    Const nNarrow = 1

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_barcode)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()

        Try
            Dim aLinee As New ArrayList
            Dim PositionX As Decimal
            Dim PositionY As Decimal
            Dim aTemp_1 As Integer()
            Dim aTemp_2 As Integer()
            Dim i As Integer
            Dim j As Integer
            Dim Penna As Pen

            '
            '
            '
            Penna = GetPenna()

            '
            '
            '
            BuildBarreDecodeTable()

            ' Start Code 
            aLinee.Add(nNarrow)
            aLinee.Add(nNarrow * -1)
            aLinee.Add(nNarrow)
            aLinee.Add(nNarrow * -1)

            For i = 0 To Elemento.BarCodeValue.Count - 1 Step 2
                aTemp_1 = _BarreDecodeTable(Elemento.BarCodeValue(i))
                aTemp_2 = _BarreDecodeTable(Elemento.BarCodeValue(i + 1))
                For j = 0 To 4
                    aLinee.Add(aTemp_1(j))
                    aLinee.Add(aTemp_2(j) * -1)
                Next
            Next

            ' End Code
            aLinee.Add(nWide)
            aLinee.Add(nNarrow * -1)
            aLinee.Add(nNarrow)

            If Elemento.Direzione = eDirezione.Orizzontale Then
                PositionX = Me.PosizioneCorr("X")
                For Each nTmp As Integer In aLinee
                    For i = 1 To Math.Abs(nTmp)
                        If nTmp > 0 Then
                            grp.DrawLine(Penna, PositionX, Me.PosizioneCorr("Y"), PositionX, Me.PosizioneCorr("Y") + Elemento.AltezzaBarCode)
                        End If
                        PositionX += Elemento.SpessoreBarCode
                    Next
                Next
            ElseIf Elemento.Direzione = eDirezione.Verticale Then
                PositionY = Me.PosizioneCorr("Y")
                For Each nTmp As Decimal In aLinee
                    For i = 1 To Math.Abs(nTmp)
                        If nTmp > 0 Then
                            grp.DrawLine(Penna, Me.PosizioneCorr("X"), PositionY, Me.PosizioneCorr("X") + Elemento.AltezzaBarCode, PositionY)
                        End If
                        PositionY += Elemento.SpessoreBarCode
                    Next
                Next
            End If
        Catch ex As Exception
            Throw New Exception("PRIBC2OF5-001: Errore stampando il barcode.", ex)
        End Try

    End Sub

    '
    ' Riempe la tabella di decodifica per le barre
    '
    Private Sub BuildBarreDecodeTable()

        _BarreDecodeTable = New ArrayList
        _BarreDecodeTable.Add(New Integer() {nNarrow, nNarrow, nWide, nWide, nNarrow}) ' conversione dello 0
        _BarreDecodeTable.Add(New Integer() {nWide, nNarrow, nNarrow, nNarrow, nWide}) ' conversione dello 1
        _BarreDecodeTable.Add(New Integer() {nNarrow, nWide, nNarrow, nNarrow, nWide}) ' conversione dello 2
        _BarreDecodeTable.Add(New Integer() {nWide, nWide, nNarrow, nNarrow, nNarrow}) ' conversione dello 3
        _BarreDecodeTable.Add(New Integer() {nNarrow, nNarrow, nWide, nNarrow, nWide}) ' conversione dello 4
        _BarreDecodeTable.Add(New Integer() {nWide, nNarrow, nWide, nNarrow, nNarrow}) ' conversione dello 5
        _BarreDecodeTable.Add(New Integer() {nNarrow, nWide, nWide, nNarrow, nNarrow}) ' conversione dello 6
        _BarreDecodeTable.Add(New Integer() {nNarrow, nNarrow, nNarrow, nWide, nWide}) ' conversione dello 7
        _BarreDecodeTable.Add(New Integer() {nWide, nNarrow, nNarrow, nWide, nNarrow}) ' conversione dello 8
        _BarreDecodeTable.Add(New Integer() {nNarrow, nWide, nNarrow, nWide, nNarrow}) ' conversione dello 9

    End Sub

End Class
