Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo

Public Class CLS_prn_testo_Verticale
    Inherits CLS_prn_testo_base

    Private _ValoreA As ArrayList
    Private _Paragrafi As ArrayList

    Shadows ReadOnly Property Elemento As CLS_ele_testo

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_testo)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_testo)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()
        Dim myPathMatrix As System.Drawing.Drawing2D.Matrix
        Dim oldmyPathMatrix As System.Drawing.Drawing2D.Matrix

        myPathMatrix = New System.Drawing.Drawing2D.Matrix

        If Elemento.Direzione = eDirezione.Verticale_BassoAlto Then
            myPathMatrix.RotateAt(-90, New Point(Me.PosizioneCorr("X"), Me.PosizioneCorr("Y")))
            ' myPathMatrix.RotateAt(45, New Point(Me.PosizioneCorr("X"), Me.PosizioneCorr("Y")))
        Else
            myPathMatrix.RotateAt(90, New Point(Me.PosizioneCorr("X"), Me.PosizioneCorr("Y")))
            '  myPathMatrix.RotateAt(270, New Point(Me.PosizioneCorr("X"), Me.PosizioneCorr("Y")))
        End If
        oldmyPathMatrix = grp.Transform
        grp.Transform = (myPathMatrix)
        grp.DrawString(Elemento.ValoreS, Elemento.TipoCarattere, Elemento.ColoreBrush, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"))
        'grp.ResetTransform()
        grp.Transform = (oldmyPathMatrix)

    End Sub

End Class
