Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione.Modulo.MMO_enumeratori

Public Class CLS_prn_barcode_128c
    Inherits CLS_prn_barcode_base

    Shadows ReadOnly Property Elemento As CLS_ele_barcode

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_barcode)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_barcode)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()

        Try
            Dim PositionX As Decimal
            Dim lBlack As Boolean
            Dim nTotCD As Integer
            Dim cValue As String
            Dim aTmp As String()
            Dim nMolt As Integer
            Dim nTmp As Integer
            Dim cTmp As String
            Dim i As Integer
            Dim Penna As Pen

            Penna = GetPenna()

            '
            '
            '
            BuildBarreDecodeTable()

            '
            ' predisposizione della stringa per il calcolo della check digit:
            '
            cValue = ""
            For Each cTmp In Elemento.BarCodeValue
                cValue = String.Concat(cValue, cTmp)
            Next

            nTotCD = 0
            Elemento.BarCodeValue = New ArrayList
            Elemento.BarCodeValue.Add("SC")
            For i = 1 To 50 Step 2
                Elemento.BarCodeValue.Add(cValue.Substring(i - 1, 2))
            Next
            nTmp = 0
            For Each cTmp In Elemento.BarCodeValue
                Select Case cTmp
                    Case Is = "F1"
                        nMolt = 102
                    Case Is = "SC"
                        nMolt = 105
                    Case Else
                        nMolt = cTmp
                End Select
                If nTmp = 0 Then
                    nTotCD += nMolt
                Else
                    nTotCD += nMolt * nTmp
                End If
                nTmp += 1
            Next
            Elemento.BarCodeValue.Add(nTotCD - Int(nTotCD / 103) * 103)
            Elemento.BarCodeValue.Add("ALT")
            For nTmp = 0 To Elemento.BarCodeValue.Count - 1
                If Elemento.BarCodeValue(nTmp).ToString = "ALT" Then
                    Elemento.BarCodeValue(nTmp) = 106
                End If
                If Elemento.BarCodeValue(nTmp).ToString = "F1" Then
                    Elemento.BarCodeValue(nTmp) = 102
                End If
                If Elemento.BarCodeValue(nTmp).ToString = "SC" Then
                    Elemento.BarCodeValue(nTmp) = 105
                End If
            Next
            PositionX = Me.PosizioneCorr("X")
            For Each cTmp In Elemento.BarCodeValue
                aTmp = _BarreDecodeTable(CInt(cTmp))
                lBlack = True
                For Each nTmp In aTmp
                    For i = 1 To nTmp
                        If lBlack Then
                            grp.DrawLine(Penna, PositionX, Me.PosizioneCorr("Y"), PositionX, Me.PosizioneCorr("Y") + Elemento.AltezzaBarCode)
                        End If
                        PositionX += Elemento.SpessoreBarCode
                    Next
                    lBlack = Not lBlack
                Next
            Next
        Catch ex As Exception
            Throw New Exception("PRIBC128C-001: Errore stampando il barcode.", ex)
        End Try

    End Sub

    '
    ' Riempe la tabella di decodifica per le barre
    '
    Private Sub BuildBarreDecodeTable()

        _BarreDecodeTable = New ArrayList
        _BarreDecodeTable.Add(New String() {2, 1, 2, 2, 2, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 2, 1, 2, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 2, 2, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 2, 2, 3})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 3, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 3, 1, 2, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 2, 1, 3})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 3, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 3, 2, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 2, 1, 3})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 3, 1, 2})
        _BarreDecodeTable.Add(New String() {2, 3, 1, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 2, 3, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 1, 3, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 2, 3, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 3, 2, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 3, 1, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 3, 2, 2, 1})
        _BarreDecodeTable.Add(New String() {2, 2, 3, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 1, 3, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 2, 3, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 3, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {2, 2, 3, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {3, 1, 2, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 1, 2, 2, 2})
        _BarreDecodeTable.Add(New String() {3, 2, 1, 1, 2, 2})
        _BarreDecodeTable.Add(New String() {3, 2, 1, 2, 2, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 2, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {3, 2, 2, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {3, 2, 2, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 2, 1, 2, 3})
        _BarreDecodeTable.Add(New String() {2, 1, 2, 3, 2, 1})
        _BarreDecodeTable.Add(New String() {2, 3, 2, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 3, 2, 3})
        _BarreDecodeTable.Add(New String() {1, 3, 1, 1, 2, 3})
        _BarreDecodeTable.Add(New String() {1, 3, 1, 3, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 3, 1, 3})
        _BarreDecodeTable.Add(New String() {1, 3, 2, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {1, 3, 2, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 3, 1, 3})
        _BarreDecodeTable.Add(New String() {2, 3, 1, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {2, 3, 1, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 1, 3, 3})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 3, 3, 1})
        _BarreDecodeTable.Add(New String() {1, 3, 2, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 3, 1, 2, 3})
        _BarreDecodeTable.Add(New String() {1, 1, 3, 3, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 3, 3, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 3, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 3, 3, 1})
        _BarreDecodeTable.Add(New String() {2, 3, 1, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 3, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {2, 1, 3, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 3, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 1, 1, 2, 3})
        _BarreDecodeTable.Add(New String() {3, 1, 1, 3, 2, 1})
        _BarreDecodeTable.Add(New String() {3, 3, 1, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 2, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {3, 1, 2, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {3, 3, 2, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 4, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 4, 1, 1})
        _BarreDecodeTable.Add(New String() {4, 3, 1, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 2, 2, 4})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 4, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 1, 2, 4})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 4, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 4, 1, 1, 2, 2})
        _BarreDecodeTable.Add(New String() {1, 4, 1, 2, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 2, 1, 4})
        _BarreDecodeTable.Add(New String() {1, 1, 2, 4, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 1, 1, 4})
        _BarreDecodeTable.Add(New String() {1, 2, 2, 4, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 4, 2, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 4, 2, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 4, 1, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 2, 1, 1, 1, 4})
        _BarreDecodeTable.Add(New String() {4, 1, 3, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 4, 1, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 3, 4, 1, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 2, 4, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 1, 4, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 1, 2, 4, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 4, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 4, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {1, 2, 4, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {4, 1, 1, 2, 1, 2})
        _BarreDecodeTable.Add(New String() {4, 2, 1, 1, 1, 2})
        _BarreDecodeTable.Add(New String() {4, 2, 1, 2, 1, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 2, 1, 4, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 4, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {4, 1, 2, 1, 2, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 1, 4, 3})
        _BarreDecodeTable.Add(New String() {1, 1, 1, 3, 4, 1})
        _BarreDecodeTable.Add(New String() {1, 3, 1, 1, 4, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 4, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {1, 1, 4, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {4, 1, 1, 1, 1, 3})
        _BarreDecodeTable.Add(New String() {4, 1, 1, 3, 1, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 3, 1, 4, 1})
        _BarreDecodeTable.Add(New String() {1, 1, 4, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {3, 1, 1, 1, 4, 1})
        _BarreDecodeTable.Add(New String() {4, 1, 1, 1, 3, 1})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 4, 1, 2})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 2, 1, 4})
        _BarreDecodeTable.Add(New String() {2, 1, 1, 2, 3, 2})
        _BarreDecodeTable.Add(New String() {2, 3, 3, 1, 1, 1, 2})

    End Sub

End Class
