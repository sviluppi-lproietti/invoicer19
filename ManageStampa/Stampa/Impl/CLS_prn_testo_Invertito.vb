Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.ManageSessione.Modulo.MMO_enumeratori

Public Class CLS_prn_testo_Invertito
    Inherits CLS_prn_testo_base

    Private _ValoreA As ArrayList
    Private _Paragrafi As ArrayList

    Shadows ReadOnly Property Elemento As CLS_ele_testo

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_testo)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_testo)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()
        Dim StringFmt As StringFormat
        Dim sSize As SizeF

        sSize = grp.MeasureString(Elemento.ValoreS, Elemento.TipoCarattere)

        'Select Case Elemento.Allineamento
        '    Case Is = eTipoAllineamento.Sinistra
        '        StringFmt = New StringFormat()
        '        StringFmt.Alignment = StringAlignment.Near
        '    Case Is = eTipoAllineamento.Centro
        '        StringFmt = New StringFormat()
        '        StringFmt.Alignment = StringAlignment.Center
        '    Case Is = eTipoAllineamento.Destra
        '        StringFmt = New StringFormat()
        '        StringFmt.Alignment = StringAlignment.Far
        'End Select
        'If CType(Elemento, CLS_ele_testo).Allineamento = eTipoAllineamento.Destra Then
        '    Elemento.Posizione("X") -= sSize.Width
        'End If
        '' rect = New Rectangle(Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), nWidth, nHeight)
        grp.FillRectangle(New SolidBrush(Elemento.BackGroundColor), Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), sSize.Width, sSize.Height)
        grp.DrawString(Elemento.ValoreS, Elemento.TipoCarattere, Elemento.ColoreBrush, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"))

    End Sub

End Class
