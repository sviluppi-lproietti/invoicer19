Imports System.Drawing
Imports ISC.LibrerieComuni.ManageSessione.Modulo
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilitaXML
Imports QRCoder

Public Class CLS_prn_barcode_QR
    Inherits CLS_prn_barcode_base

    Shadows ReadOnly Property Elemento As CLS_ele_barcode

        Get
            Return CType(MyBase.ElementoDaStampare, CLS_ele_barcode)
        End Get

    End Property

    Public Sub New(ByVal ev As System.Drawing.Printing.PrintPageEventArgs, ByVal ppi As CLS_ele_barcode)
        MyBase.New(ev, ppi)

    End Sub

    Public Overrides Sub Stampa()
        Dim eccLevel As QRCodeGenerator.ECCLevel
        Dim BMP_image As System.Drawing.Bitmap
        Dim nHeight As Single
        Dim lResize As Boolean
        Dim cValue As String
        Dim nWidth As Single
        Dim cTmp As String

        Try
            cValue = ""
            For Each cTmp In Elemento.BarCodeValue
                cValue = String.Concat(cValue, cTmp)
            Next

            If (Elemento.ECCLevel = "L") Then
                eccLevel = 0
            ElseIf (Elemento.ECCLevel = "M") Then
                eccLevel = 1
            ElseIf (Elemento.ECCLevel = "Q") Then
                eccLevel = 2
            Else
                eccLevel = 3
            End If
            Using qrGenerator As QRCodeGenerator = New QRCodeGenerator()
                Using qrCodeData As QRCodeData = qrGenerator.CreateQrCode(cValue, eccLevel)
                    Using qrCode As QRCode = New QRCode(qrCodeData)
                        BMP_image = qrCode.GetGraphic(3)  ', Color.Black.ToString(), Color.White.ToString())
                        If Elemento.NeedRotation Then
                            BMP_image.RotateFlip(Elemento.RotationType)
                        End If
                        If lResize Then
                            nWidth = cTmp.Split(";")(0)
                            nHeight = cTmp.Split(";")(1)
                            grp.DrawImage(BMP_image, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"), nWidth, nHeight)
                        Else
                            grp.DrawImage(BMP_image, Me.PosizioneCorr("X"), Me.PosizioneCorr("Y"))
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Throw New Exception("PRIBCQRCO-001: Errore stampando il barcode.", ex)
        End Try

    End Sub

End Class
