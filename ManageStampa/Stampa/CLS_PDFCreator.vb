Imports ISC.Invoicer.ManageStampa.MST_enumeratori
Imports ISC.LibrerieComuni.OggettiComuni.OGC_utilita

Public Class CLS_PDFCreator

    Private _PRNArchOtt As PDFCreator.clsPDFCreator
    Private _OldAutoSaveDirectory As String
    Private _OldAutoSaveFileName As String
    Private _OldUseAutoSave As String
    Private _OldUseAutosaveDirectory As String
    Private _OldAutosaveFormat As String
    Private _OldFontsEmbedded As String
    Private _OldDefaultPrinter As String
    Private _PDFSigningPFXFile As String
    Private _PDFSigningPFXFilePassword As String
    Private _FileExtension As String

    Public Sub New(ByVal cPrinterName As String)

        _PRNArchOtt = New PDFCreator.clsPDFCreator
        With _PRNArchOtt
            .cPrinterStop = True
            _OldAutoSaveDirectory = .cOption("AutosaveDirectory")
            _OldAutoSaveFileName = .cOption("AutosaveFilename")
            _OldUseAutoSave = .cOption("UseAutosave")
            _OldUseAutosaveDirectory = .cOption("UseAutosaveDirectory")
            _OldAutosaveFormat = .cOption("AutosaveFormat")
            _OldFontsEmbedded = .cOption("PDFFontsEmbedAll")
            _OldDefaultPrinter = _PRNArchOtt.cDefaultPrinter
            _PDFSigningPFXFile = .cOption("PDFSigningPFXFile")
            _PDFSigningPFXFilePassword = .cOption("PDFSigningPFXFilePassword")
            _PRNArchOtt.cStart("/NoProcessingAtStartup")
            _FileExtension = ".pdf"
        End With

    End Sub

    Public Sub AttivaStampante()

        _PRNArchOtt.cOption("UseAutosave") = 1
        _PRNArchOtt.cOption("UseAutosaveDirectory") = 1
        _PRNArchOtt.cDefaultPrinter = "PDFCreator"
        _PRNArchOtt.cClearCache()
        _PRNArchOtt.cPrinterStop = False

    End Sub

    Public Sub RestorePrinterParam()

        Try
            ' ********************************************************************* '
            ' Ripristino delle impostazioni di PDFCreator.                          '
            ' ********************************************************************* '
            _PRNArchOtt.cPrinterStop = True
            _PRNArchOtt.cOption("AutosaveDirectory") = _OldAutoSaveDirectory
            _PRNArchOtt.cOption("AutosaveFilename") = _OldAutoSaveFileName
            _PRNArchOtt.cOption("UseAutosave") = _OldUseAutoSave
            _PRNArchOtt.cOption("UseAutosaveDirectory") = _OldUseAutosaveDirectory
            _PRNArchOtt.cOption("AutosaveFormat") = _OldAutosaveFormat
            _PRNArchOtt.cDefaultPrinter = _OldDefaultPrinter
            _PRNArchOtt.cPrinterStop = False
        Catch ex As Exception

        End Try
        _PRNArchOtt = Nothing

    End Sub

    WriteOnly Property AutosaveDirectory() As String

        Set(ByVal value As String)
            _OldAutoSaveDirectory = _PRNArchOtt.cOption("AutosaveDirectory")
            _PRNArchOtt.cOption("AutosaveDirectory") = value
        End Set

    End Property

    Property AutosaveFilename() As String

        Set(ByVal value As String)
            _OldAutoSaveFileName = _PRNArchOtt.cOption("AutosaveFilename")
            _PRNArchOtt.cOption("AutosaveFilename") = value
        End Set
        Get
            Return _PRNArchOtt.cOption("AutosaveFilename")
        End Get

    End Property

    WriteOnly Property UseAutosave() As String

        Set(ByVal value As String)
            _OldUseAutoSave = _PRNArchOtt.cOption("UseAutosave")
            _PRNArchOtt.cOption("UseAutosave") = value
        End Set

    End Property

    WriteOnly Property UseAutosaveDirectory() As String

        Set(ByVal value As String)
            _OldUseAutosaveDirectory = _PRNArchOtt.cOption("UseAutosaveDirectory")
            _PRNArchOtt.cOption("UseAutosaveDirectory") = value
        End Set

    End Property

    WriteOnly Property AutosaveFormat() As ePDFCreatorFileType

        Set(ByVal value As ePDFCreatorFileType)
            _OldAutosaveFormat = _PRNArchOtt.cOption("AutosaveFormat")
            _PRNArchOtt.cOption("AutosaveFormat") = value
            If value = ePDFCreatorFileType.FT_JPG Then
                _FileExtension = ".jpg"
            ElseIf value = ePDFCreatorFileType.FT_PDF1A Or value = ePDFCreatorFileType.FT_PDFGeneric Then
                _FileExtension = ".pdf"
            End If
        End Set

    End Property

    WriteOnly Property PDFSigningPFXFile() As String

        Set(ByVal value As String)
            _PRNArchOtt.cOption("PDFSigningSignPDF") = 1
            _PDFSigningPFXFile = _PRNArchOtt.cOption("PDFSigningPFXFile")
            _PRNArchOtt.cOption("PDFSigningPFXFile") = value
        End Set

    End Property

    WriteOnly Property PDFSigningPFXFilePassword() As String

        Set(ByVal value As String)
            _PDFSigningPFXFilePassword = _PRNArchOtt.cOption("PDFSigningPFXFilePassword")
            _PRNArchOtt.cOption("PDFSigningPFXFilePassword") = value
        End Set

    End Property


    WriteOnly Property PDFFontsEmbedAll() As String

        Set(ByVal value As String)
            _OldFontsEmbedded = _PRNArchOtt.cOption("PDFFontsEmbedAll")
            _PRNArchOtt.cOption("PDFFontsEmbedAll") = value
        End Set

    End Property

    Public Sub AttendiProduzioneOttico()
        Dim cTmp As String
        Dim SleepInterval As Integer = 1000
        Dim cFileName As String

        cFileName = String.Concat(StandardFolderValue(_PRNArchOtt.cOption("AutosaveDirectory")), _PRNArchOtt.cOption("AutosaveFilename"), _FileExtension)
        Try
            While Not System.IO.File.Exists(cFileName) 'And Not ExitForced
                System.Threading.Thread.Sleep(SleepInterval)
            End While
            While _PRNArchOtt.cCountOfPrintjobs > 0 'And Not ExitForced
                System.Threading.Thread.Sleep(SleepInterval)
                ' ********************************************************************* '
                ' L'istruzione che segue serve esclusivamente a controllare un abort    ' 
                ' del processo PDFCreator.                                              '
                ' ********************************************************************* '
                cTmp = _PRNArchOtt.cOption("AutosaveFilename")
            End While
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub CambiaNomeFile(ByVal cFileName As String)

        _PRNArchOtt.cPrinterStop = True
        _PRNArchOtt.cOption("AutosaveFilename") = cFileName
        _PRNArchOtt.cPrinterStop = False

    End Sub

    Public ReadOnly Property NumeroJobInStampa() As Integer

        Get
            Return _PRNArchOtt.cCountOfPrintjobs
        End Get

    End Property

End Class
